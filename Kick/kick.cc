// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "kick.hh"

#include "motion/scripts/MotionScript/motionscript.hh"
#include "util/Log/log.hh"
#include "geometry2/Operator/Predicate/Contains/contains.hh"

#include <Eigen/Core>

using namespace bold;
using namespace bold::motion::scripts;
using namespace bold::util;
using namespace bold::geometry2;
using namespace Eigen;
using namespace std;

std::vector<std::shared_ptr<Kick const>> Kick::s_allKicks;

void Kick::loadAll()
{
  // TODO load from config
  // TODO produce kick params experimentally using kick-learner role

  s_allKicks.push_back(allocate_aligned_shared<Kick const>(
    "forward-right",
    "./motionscripts/kick-right.json",
    AlignedBox2d{Vector2d(0, 0), Vector2d(0.1, 0.2)},
    Vector2d{0.0, 1.853},
    Vector2d{0.065, 0.106}
  ));

  s_allKicks.push_back(allocate_aligned_shared<Kick const>(
    "forward-left",
    "./motionscripts/kick-left.json",
    AlignedBox2d{Vector2d(-0.1, 0), Vector2d(0, 0.2)},
    Vector2d{0.0, 1.853},
    Vector2d{-0.065, 0.106}
  ));
}

shared_ptr<Kick const> Kick::getById(string id)
{
  auto it = std::find_if(
    s_allKicks.begin(),
    s_allKicks.end(),
    [id](shared_ptr<Kick const> const& kick) { return kick->getId() == id; });

  if (it == s_allKicks.end())
  {
    Log::error("Kick::getById") << "Requested kick with unknown id: " << id;
    throw runtime_error("Requested kick with unknown id");
  }

  return *it;
}

Kick::Kick(string id, string scriptPath, AlignedBox2d ballBounds, Point2d endPos, Point2d idealBallPos)
  : d_id{id},
    d_motionScript{MotionScript::fromFile(scriptPath)},
    d_ballBounds{ballBounds},
    d_endPos{endPos},
    d_idealBallPos{idealBallPos}
{}

Maybe<Point2d> Kick::estimateEndPos(Point2d const& ballPos) const
{
  // TODO end pos depends upon start pos -- need means of modelling this
  // TODO model probabilistically, not absolutely, to allow learning risk/reward

  // TODO: contains specialisation for Eigen::AlignedBox / Point
  if (!contains(Polygon2d{d_ballBounds}, ballPos))
    return util::Maybe<Point2d>::empty();

  return make_maybe(d_endPos);
}
