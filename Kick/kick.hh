// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <vector>
#include <string>

#include "src/util/Maybe/maybe.hh"
#include "src/geometry2/Point/point.hh"

namespace bold
{
  namespace motion
  {
    namespace scripts
    {
      class MotionScript;
    }
  }

  class Kick
  {
  public:
    static void loadAll();
    static std::vector<std::shared_ptr<Kick const>> const& getAll() { return s_allKicks; }
    static std::shared_ptr<Kick const> getById(std::string id);

    Kick(std::string id, std::string scriptPath,
         Eigen::AlignedBox2d ballBounds, geometry2::Point2d endPos, geometry2::Point2d idealBallPos);

    /**
     * Given a ball position, returns whether the ball can be kicked and
     * where it is likely to end up.
     *
     * All positions are in the agent frame.
     */
    util::Maybe<geometry2::Point2d> estimateEndPos(geometry2::Point2d const& ballPos) const;

    std::string getId() const { return d_id; }

    std::shared_ptr<motion::scripts::MotionScript> getMotionScript() const { return d_motionScript; }

    geometry2::Point2d getIdealBallPos() const { return d_idealBallPos; }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    
  private:
    static std::vector<std::shared_ptr<Kick const>> s_allKicks;

    std::string d_id;
    std::shared_ptr<motion::scripts::MotionScript> d_motionScript;
    Eigen::AlignedBox2d d_ballBounds;
    geometry2::Point2d d_endPos;
    geometry2::Point2d d_idealBallPos;
  };
}
