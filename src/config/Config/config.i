%{
#include <config/Config/config.hh>
#include <util/Range/range.hh>
%}

namespace bold
{
  namespace config
  {
    class Config
    {
    public:
      static bold::config::SettingBase* getSettingBase(std::string path);

      template<typename T>
        static bold::config::Setting<T>* getSetting(std::string path);

      template<typename T>
        static T getValue(std::string path);

      template<typename T>
        static T getStaticValue(std::string path);

      static void initialise(std::string metadataFile, std::string configFile);
      static void initialisationCompleted();
      static bool isInitialising();
    };
  }
}

// Macro to catch an error and convert it to a Python exception
%define GETSETTING_TYPEERROR
  try
  {
    $action
  }
  catch (std::runtime_error& e)
  {
    PyErr_SetString(PyExc_TypeError, const_cast<char*>(e.what()));
    return NULL;
  }
  catch (std::invalid_argument& e)
  {
    PyErr_SetString(PyExc_KeyError, const_cast<char*>(e.what()));
    return NULL;
  }
%enddef

// Putting these into GETSETTING_TEMPLATE does not work
%exception bold::config::Config::getValue<int > { GETSETTING_TYPEERROR }
%exception bold::config::Config::getValue<double > { GETSETTING_TYPEERROR }
%exception bold::config::Config::getValue<bool > { GETSETTING_TYPEERROR }
%exception bold::config::Config::getValue<bold::colour::HSVRange > { GETSETTING_TYPEERROR }
%exception bold::config::Config::getValue<bold::util::Range<double> > { GETSETTING_TYPEERROR }
%exception bold::config::Config::getValue<std::string > { GETSETTING_TYPEERROR }

%define GETSETTING_TEMPLATE(T,N)
  %template(get ## N ## Setting) bold::config::Config::getSetting<T >;
  %template(get ## N ## Value) bold::config::Config::getValue<T >;
  %template(getStatic ## N ## Value) bold::config::Config::getStaticValue<T >;
%enddef

GETSETTING_TEMPLATE(int,Int);
GETSETTING_TEMPLATE(int,Enum);
GETSETTING_TEMPLATE(double,Double);
GETSETTING_TEMPLATE(bool,Bool);
GETSETTING_TEMPLATE(bold::colour::HSVRange,HSVRange);
GETSETTING_TEMPLATE(bold::util::Range<double>,DoubleRange);
GETSETTING_TEMPLATE(std::string,String);

