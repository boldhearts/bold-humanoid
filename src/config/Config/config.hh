// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <functional>
#include <map>
#include <rapidjson/document.h>
#include <sigc++/signal.h>
#include <typeindex>

#include "config/SettingBase/Setting/setting.hh"
#include "util/Log/log.hh"
#include "util/assert.hh"

namespace bold
{
  namespace config
  {
    /// Central store for all configuration data.
    class Config
    {
    public:
      static sigc::signal<void, SettingBase const&> updated;

      static SettingBase* getSettingBase(std::string const& path);

      /// Retrieves a Setting<T> having the specified path.
      template<typename T>
      static Setting<T>* getSetting(std::string const& path)
      {
        auto setting = getSettingBase(path);

        if (setting == nullptr)
          return nullptr;

        std::type_index typeIndex = std::is_enum<T>::value ? typeid(int) : typeid(T);

        if (typeIndex != setting->getTypeIndex())
        {
          util::Log::error("getSetting") << "Attempt to get setting having different type: " << path;
          throw std::runtime_error("Attempt to get setting having different type");
        }

        return (Setting<T>*)setting;
      }

      /// Gets the current value of the specified setting. Throws if setting unknown.
      template<typename T>
      static T getValue(std::string const& path)
      {
        auto setting = getSetting<T>(path);
        if (!setting)
        {
          util::Log::error("getValue") << "No setting exists with path: " << path;
          throw std::invalid_argument("No setting exists for requested path");
        }
        return setting->getValue();
      }

      /// Gets the current value of the specified setting. Throws if setting unknown or setting not readonly.
      template<typename T>
      static T getStaticValue(std::string const& path)
      {
        auto setting = getSetting<T>(path);
        if (!setting)
        {
          util::Log::error("getValue") << "No setting exists with path: " << path;
          throw std::invalid_argument("No setting exists for requested path");
        }
        if (!setting->isReadOnly())
        {
          util::Log::error("getValue") << "Requested static config value, however setting is not readonly: " << path;
          throw std::runtime_error("Requested static config value, however setting is not readonly");
        }
        return setting->getValue();
      }

      /// Adds the specified setting.
      /// If a corresponding value exists in the config document, its value is
      /// retrieved and set on the provided setting.
      static void addSetting(SettingBase* setting);

      static rapidjson::Value const* getConfigJsonValue(std::string const& path);

      static void initialise(std::string const& metadataFile, std::string const& configFile);

      static void initialisationCompleted() { ASSERT(d_isInitialising); d_isInitialising = false; }
      static bool isInitialising() { return d_isInitialising; }

      static std::vector<SettingBase*> getSettings(std::string const& prefix);
      static std::vector<SettingBase*> getAllSettings();

      static std::vector<std::string> getConfigDocumentNames() { return d_configFileNames; }

      static void setPermissible(bool permissible = true) { d_permissible = permissible; }

    private:
      struct TreeNode
      {
        TreeNode()
          : settingByName(),
            subNodeByName()
        {}

        std::map<std::string, SettingBase*> settingByName;
        std::map<std::string, TreeNode> subNodeByName;
      };

      static void processConfigMetaJsonValue(rapidjson::Value const* metaNode, TreeNode* treeNode, std::string const& path, std::string const& name);

      Config() = delete;

      static TreeNode d_root;
      static std::vector<rapidjson::Document> d_configDocuments;
      static std::vector<std::string> d_configFileNames;
      static bool d_isInitialising;
      static bool d_permissible;
    };
  }
}
