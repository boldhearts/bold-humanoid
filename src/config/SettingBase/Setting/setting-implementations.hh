// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "setting.hh"

#include "colour/BGR/bgr.hh"
#include "colour/HSVRange/hsvrange.hh"
#include "util/Range/range.hh"

#include <vector>

namespace bold
{
  namespace colour
  {
    struct BGR;
    struct HSVRange;
  }

  namespace util
  {
    template<typename T>
    class Range;
  }

  namespace config
  {
    /// Models a setting with an integer value.
    class IntSetting final : public VisitableSetting<IntSetting, int>
    {
    public:
      IntSetting(std::string path, int min, int max, bool isReadOnly, std::string description);

      int getMin() const { return d_min; }
      int getMax() const { return d_max; }
    
      bool isValidValue(int const& value) const override;
      std::string getValidationMessage(int const& value) const override;

    private:
      int d_min;
      int d_max;
    };

    /// Models a setting with an integer value selected from a set of valid numbers.
    class EnumSetting final : public VisitableSetting<EnumSetting, int>
    {
    public:
      EnumSetting(std::string path, std::map<int,std::string> pairs, bool isReadOnly, std::string description);

      std::map<int, std::string> const& getPairs() const
      {
        return d_pairs;
      }
    
      bool isValidValue(int const& value) const override;
      std::string getValidationMessage(int const& value) const override;

    private:
      std::map<int, std::string> d_pairs;
    };

    /// Models a setting with a double value.
    class DoubleSetting final : public VisitableSetting<DoubleSetting, double>
    {
    public:
      DoubleSetting(std::string path, double min, double max, bool isReadOnly, std::string description);

      double getMin() const { return d_min; }
      double getMax() const { return d_max; }
    
      bool isValidValue(double const& value) const override;
      std::string getValidationMessage(double const& value) const override;

    private:
      double d_min;
      double d_max;
    };

    /// Models a setting with a boolean value.
    class BoolSetting final : public VisitableSetting<BoolSetting, bool>
    {
    public:
      BoolSetting(std::string path, bool isReadOnly, std::string description);
    };

    /// Models a setting with a colour::HSVRange value.
    class HsvRangeSetting final : public VisitableSetting<HsvRangeSetting, colour::HSVRange>
    {
    public:
      HsvRangeSetting(std::string path, bool isReadOnly, std::string description);

      bool isValidValue(colour::HSVRange const& value) const override;
      std::string getValidationMessage(colour::HSVRange const& value) const override;
    };

    /// Models a setting with a Range<double> value.
    class DoubleRangeSetting final : public VisitableSetting<DoubleRangeSetting, util::Range<double>>
    {
    public:
      DoubleRangeSetting(std::string path, bool isReadOnly, std::string description);

      bool isValidValue(util::Range<double> const& value) const override;
      std::string getValidationMessage(util::Range<double> const& value) const override;
    };

    /// Models a setting with a std::string value.
    class StringSetting final : public VisitableSetting<StringSetting, std::string>
    {
    public:
      StringSetting(std::string path, bool isReadOnly, std::string description);

      bool isValidValue(std::string const& value) const override;
      std::string getValidationMessage(std::string const& value) const override;
    };

    /// Models a setting with a std::string value.
    class StringArraySetting final : public VisitableSetting<StringArraySetting, std::vector<std::string>>
    {
    public:
      StringArraySetting(std::string path, bool isReadOnly, std::string description);

      bool areValuesEqual(std::vector<std::string> const& a, std::vector<std::string> const& b) const override;
    };

    /// Models a setting with a colour::BGR value.
    class BgrColourSetting final : public VisitableSetting<BgrColourSetting, colour::BGR>
    {
    public:
      BgrColourSetting(std::string path, bool isReadOnly, std::string description);
    };
  }
}
