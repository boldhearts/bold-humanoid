// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "setting-implementations.hh"

#include "src/util/Log/log.hh"
#include "colour/BGR/bgr.hh"
#include "colour/HSVRange/hsvrange.hh"
#include "util/Range/range.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace std;

IntSetting::IntSetting(string path, int min, int max, bool isReadOnly, string description)
: VisitableSetting(path, "int", isReadOnly, description),
  d_min(min),
  d_max(max)
{}

bool IntSetting::isValidValue(int const& value) const
{
  return value >= d_min && value <= d_max;
}

string IntSetting::getValidationMessage(int const& value) const
{
  if (value < d_min || value > d_max)
  {
    stringstream msg;
    msg << "Value must be between " << d_min << " and " << d_max;
    return msg.str();
  }
  return "";
}

///////////////////////////////////////////////////////////

EnumSetting::EnumSetting(string path, map<int,string> pairs, bool isReadOnly, string description)
: VisitableSetting(path, "enum", isReadOnly, description),
  d_pairs(pairs)
{}

bool EnumSetting::isValidValue(int const& value) const
{
  return d_pairs.find(value) != d_pairs.end();
}

string EnumSetting::getValidationMessage(int const& value) const
{
  if (d_pairs.find(value) == d_pairs.end())
  {
    stringstream msg;
    msg << "Value " << value << " does not exist in the enumeration";
    return msg.str();
  }
  return "";
}

///////////////////////////////////////////////////////////

DoubleSetting::DoubleSetting(string path, double min, double max, bool isReadOnly, string description)
: VisitableSetting(path, "double", isReadOnly, description),
  d_min(min),
  d_max(max)
{}

bool DoubleSetting::isValidValue(double const& value) const
{
  return value >= d_min && value <= d_max;
}

string DoubleSetting::getValidationMessage(double const& value) const
{
  if (value < d_min || value > d_max)
  {
    stringstream msg;
    msg << "Value must be between " << d_min << " and " << d_max;
    return msg.str();
  }
  return "";
}

///////////////////////////////////////////////////////////

BoolSetting::BoolSetting(string path, bool isReadOnly, string description)
: VisitableSetting(path, "bool", isReadOnly, description)
{}

///////////////////////////////////////////////////////////

HsvRangeSetting::HsvRangeSetting(string path, bool isReadOnly, string description)
: VisitableSetting(path, "hsv-range", isReadOnly, description)
{}

bool HsvRangeSetting::isValidValue(colour::HSVRange const& value) const
{
  return value.isValid();
}

string HsvRangeSetting::getValidationMessage(colour::HSVRange const& value) const
{
  if (!value.isValid())
    return "Sat/Val max values must be greater than min values";
  return "";
}

///////////////////////////////////////////////////////////

DoubleRangeSetting::DoubleRangeSetting(string path, bool isReadOnly, string description)
: VisitableSetting(path, "double-range", isReadOnly, description)
{}

bool DoubleRangeSetting::isValidValue(Range<double> const& value) const
{
  return !value.isEmpty() && value.min() <= value.max();
}

string DoubleRangeSetting::getValidationMessage(Range<double> const& value) const
{
  if (value.isEmpty())
    return "Range may not be empty";
  if (value.min() > value.max())
    return "Range's min may not be greater than its max";
  return "";
}

///////////////////////////////////////////////////////////

StringSetting::StringSetting(string path, bool isReadOnly, string description)
: VisitableSetting(path, "string", isReadOnly, description)
{}

bool StringSetting::isValidValue(string const& value) const
{
  return value.size() > 0;
}

string StringSetting::getValidationMessage(string const& value) const
{
  if (value.size() == 0)
    return "String may not have zero length";
  return "";
}

///////////////////////////////////////////////////////////

StringArraySetting::StringArraySetting(string path, bool isReadOnly, string description)
: VisitableSetting(path, "string[]", isReadOnly, description)
{}

bool StringArraySetting::areValuesEqual(vector<string> const& a, vector<string> const& b) const
{
  if (a.size() != b.size())
    return false;
  for (uint i = 0; i < a.size(); i++)
    if (a[i] != b[i])
      return false;
  return true;
}

///////////////////////////////////////////////////////////

BgrColourSetting::BgrColourSetting(string path, bool isReadOnly, string description)
  : VisitableSetting(path, "bgr-colour", isReadOnly, description)
{}
