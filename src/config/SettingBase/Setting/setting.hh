// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <map>

#include "util/Maybe/maybe.hh"
#include "util/Log/log.hh"
#include "util/Visitor/visitor.hh"

#include "../settingbase.hh"

namespace bold
{
  namespace config
  {
    /// Abstract model of a setting with a particular data type.
    /// Subclasses implement specific validation logic and serialisation.
    template<typename T>
    class Setting : public SettingBase
    {
    public:
      Setting(std::string path, std::string typeName, bool isReadOnly, std::string description)
        : SettingBase(path, typeName, typeid(T), isReadOnly, description),
          d_isInitialValueSet(false)
      {}

      virtual ~Setting() = default;

      const T getValue() const { return d_value; }

      const T getInitialValue() const
      {
        ASSERT(d_isInitialValueSet);
        return d_initialValue;
      }

      bool isInitialValueSet() const { return d_isInitialValueSet; }
      bool isModified() const override { return !areValuesEqual(d_value, d_initialValue); }

      bool setValue(T const& value)
      {
        if (isReadOnly() && !isInitialising())
        {
          bold::util::Log::error("Setting::setValue") << "Attempt to modify readonly setting: " << getPath();
          return false;
        }

        if (!isValidValue(value))
        {
          util::Log::error("Setting::setValue") << "Attempt to set invalid value '" << value
                                                << "' to setting '" << getPath()
                                                << "': " << getValidationMessage(value);
          return false;
        }

        if (!d_isInitialValueSet)
        {
          if (!isInitialising())
          {
            util::Log::error("Setting::setValue") << "Attempt to set initial value after initialisation has completed.";
            throw std::runtime_error("Attempt to set initial value after initialisation has completed.");
          }

          d_initialValue = value;
          d_isInitialValueSet = true;
        }

        if (value == d_value)
          return false;
                
        d_value = value;

        triggerChanged();

        return true;
      }

      /// Fires when the setting's value is changed.
      sigc::signal<void, T const&> changed;

      void triggerChanged() const override
      {
        changed(d_value);
        SettingBase::triggerChanged();
      }

      /// Invokes the provided callback immediately with the current value, and notifies of any subsequent changes.
      void track(std::function<void(T)> callback)
      {
        changed.connect(callback);
        callback(d_value);
      }

      void resetToInitialValue() override
      {
        ASSERT(d_isInitialValueSet);
        setValue(d_initialValue);
      }

      virtual bool areValuesEqual(T const& a, T const& b) const { return a == b; };
      virtual bool isValidValue(T const& value) const { return true; };
      virtual std::string getValidationMessage(T const& value) const { return ""; }

    private:
      T d_value;
      T d_initialValue;
      bool d_isInitialValueSet;
    };

    template<typename Derived, typename T>
    class VisitableSetting : public config::Setting<T>
    {
    public:
      VisitableSetting(std::string path, std::string typeName, bool isReadOnly, std::string description)
        : config::Setting<T>{path, typeName, isReadOnly, description}
      {}
    
      void accept(util::Visitor<AllSettingTypes>& visitor) override
      {
        visitor.visit(static_cast<Derived&>(*this));
      }

      void accept(util::ConstVisitor<AllSettingTypes>& visitor) const override
      {
        visitor.visit(static_cast<Derived const&>(*this));
      }
    };
  }
}
