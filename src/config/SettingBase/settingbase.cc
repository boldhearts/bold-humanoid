// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "settingbase.hh"

#include "config/Config/config.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

SettingBase::SettingBase(string path, string typeName, type_index typeIndex, bool isReadOnly, string description)
: d_path(path),
  d_typeName(typeName),
  d_typeIndex(typeIndex),
  d_isReadOnly(isReadOnly),
  d_description(description)
{
  auto last = d_path.find_last_of('.');
  auto nameStart = last == string::npos
    ? 0
    : last + 1;
  d_name = d_path.substr(nameStart);
}

bool SettingBase::isInitialising()
{
  return Config::isInitialising();
}

void SettingBase::triggerChanged() const
{
  changedBase(*this);
}

