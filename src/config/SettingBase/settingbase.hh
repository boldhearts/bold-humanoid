// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <sigc++/signal.h>
#include <string>
#include <typeindex>

namespace bold
{
  namespace util
  {
    template <typename T>
    struct Visitor;
    template <typename T>
    struct ConstVisitor;
  }

  namespace config
  {
    class IntSetting;
    class EnumSetting;
    class DoubleSetting;
    class BoolSetting;
    class HsvRangeSetting;
    class DoubleRangeSetting;
    class StringSetting;
    class StringArraySetting;
    class BgrColourSetting;

    using AllSettingTypes = std::tuple<
      IntSetting,
      EnumSetting,
      DoubleSetting,
      BoolSetting,
      HsvRangeSetting,
      DoubleRangeSetting,
      StringSetting,
      StringArraySetting,
      BgrColourSetting
      >;

    /// Models data common to all settings, irrespective of data type and other metadata.
    class SettingBase
    {
    public:
      sigc::signal<void, SettingBase const&> changedBase;

      std::string getPath() const { return d_path; }
      std::string getName() const { return d_name; }
      bool isReadOnly() const { return d_isReadOnly; }
      std::string getTypeName() const { return d_typeName; }
      std::type_index getTypeIndex() const { return d_typeIndex; }
      std::string getDescription() const { return d_description; }

      virtual void triggerChanged() const;

      virtual bool isModified() const = 0;
      virtual void resetToInitialValue() = 0;

      virtual void accept(util::Visitor<AllSettingTypes>& visitor) = 0;
      virtual void accept(util::ConstVisitor<AllSettingTypes>& visitor) const = 0;

    protected:
      SettingBase(std::string path, std::string typeName, std::type_index typeIndex, bool isReadOnly, std::string description);

      virtual ~SettingBase() = default;

      static bool isInitialising();

    private:
      std::string d_name;
      std::string d_path;
      std::string d_typeName;
      std::type_index d_typeIndex;
      bool d_isReadOnly;
      std::string d_description;
    };
  }
}
