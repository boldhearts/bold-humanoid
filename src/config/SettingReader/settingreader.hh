// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <rapidjson/document.h>
#include "util/Log/log.hh"
#include "util/Maybe/maybe.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"

namespace bold
{
  namespace config
  {
    struct ValueFromJsonImpl;
    
    /** Knows how to read setting information from JSON
     *
     * Used to read a setting's value from JSON. If the actual
     * setting type is known at compile time, the reader can read the
     * value directly. Else it uses double dispatch with the visitor
     * pattern to deduce the type, which is less efficient.
     */
    class SettingReader
    {
    public:
      template<typename T>
      bool setValueFromJson(T& setting, rapidjson::Value const& jsonValue) const;
    };

    struct ValueFromJsonImpl : public util::Visitor<config::AllSettingTypes>
    {
      ValueFromJsonImpl(rapidjson::Value const& jsonValue)
        : jsonValue{&jsonValue}
      {}
    
      void visit(config::SettingBase& setting)
      {
        setting.accept(*this);
      }

      void visit(config::IntSetting& setting)
      {
        if (!jsonValue->IsInt())
        {
          result = false;
          return;
        }     
        result = setting.setValue(jsonValue->GetInt());
      }

      void visit(config::EnumSetting& setting)
      {
        if (!jsonValue->IsInt())
        {
          result = false;
          return;
        }
        result = setting.setValue(jsonValue->GetInt());
      }
    
      void visit(config::DoubleSetting& setting)
      {
        if (!jsonValue->IsNumber())
        {
          result = false;
          return;
        }
        result = setting.setValue(jsonValue->GetDouble());
      }
    
      void visit(config::BoolSetting& setting)
      {
        if (!jsonValue->IsBool())
        {
          result = false;
          return;
        }
        result = setting.setValue(jsonValue->GetBool());
      }
    
      void visit(config::HsvRangeSetting& setting)
      {
        if (!jsonValue->IsObject())
        {
          util::Log::error("HsvRangeSetting::tryParseObject") << "JSON value must be an object";
          result = false;
          return;
        }

        // {"hue":[44,60],"sat":[158,236],"val":[124,222]}
        using MinMaxPair = std::pair<uint8_t, uint8_t>;
        auto parseChannel = [this](std::string channel) -> util::Maybe<MinMaxPair>
          {
            auto mem = jsonValue->FindMember(channel.c_str());
          
            if (mem == jsonValue->MemberEnd() ||
                !mem->value.IsArray() ||
                mem->value.Size() != 2 ||
                !mem->value[0u].IsInt() ||
                !mem->value[1u].IsInt())
            {
              util::Log::error("HsvRangeSetting::tryParseObject") <<
                "hsv-range value for '" << channel <<
                "' must be an array of two integer values";
              return util::Maybe<MinMaxPair>::empty();
            }

            auto v1 = mem->value[0u].GetInt();
            auto v2 = mem->value[1u].GetInt();

            if (v1 < 0 || v1 > 255 || v2 < 0 || v2 > 255)
            {
              util::Log::error("HsvRangeSetting::tryParseObject") <<
                "hsv-range value for '" << channel <<
                "' must be an array of integers between 0 and 255, inclusive";
              return util::Maybe<MinMaxPair>::empty();
            }

            return std::make_pair(uint8_t(v1), uint8_t(v2));
          };

        for (auto hueRange : parseChannel("hue"))
          for (auto satRange : parseChannel("sat"))
            for (auto valRange : parseChannel("val"))
            {
              if (satRange.first > satRange.second)
              {
                util::Log::error("HsvRangeSetting::tryParseObject") <<
                  "hsv-range value parsed correctly but has invalid saturation range";
                result = false;
                return;
              }
              
              if (valRange.first > valRange.second)
              {
                util::Log::error("HsvRangeSetting::tryParseObject") <<
                  "hsv-range value parsed correctly but has invalid value range";
                result = false;
                return;
              }
              
              auto parsedValue = colour::HSVRange{
                hueRange.first, hueRange.second,
                satRange.first, satRange.second,
                valRange.first, valRange.second
              };
            
              if (!parsedValue.isValid())
              {
                util::Log::error("HsvRangeSetting::tryParseObject") <<
                  "hsv-range value parsed correctly but has invalid data";
                result = false;
              }
              else
                result = setting.setValue(parsedValue);
              return;            
            }

        // One of the channels failed
        result = false;
      }
    
      void visit(config::DoubleRangeSetting& setting)
      {
        if (!jsonValue->IsArray() ||
            jsonValue->Size() != 2 ||
            !(*jsonValue)[0u].IsNumber() || !(*jsonValue)[1u].IsNumber())
        {
          util::Log::error("DoubleRangeSetting::tryParseJsonValue") <<
            "Double range value must be a JSON array of two double values";
          result = false;
          return;
        }

        auto v1 = (*jsonValue)[0u].GetDouble();
        auto v2 = (*jsonValue)[1u].GetDouble();

        if (v1 > v2)
        {
          util::Log::error("DoubleRangeSetting::tryParseJsonValue") <<
            "Double range must have min <= max";
          result = false;
          return;
        }

        result = setting.setValue(util::Range<double>(v1, v2));
      }
    
      void visit(config::StringSetting& setting)
      {
        if (!jsonValue->IsString())
        {
          result = false;
          return;
        }
        result = setting.setValue(jsonValue->GetString());
      }

      void visit(config::StringArraySetting& setting)
      {
        if (!jsonValue->IsArray())
        {
          result = false;
          return;
        }

        auto parsedValue = std::vector<std::string>{};

        for (unsigned i = 0; i < jsonValue->Size(); i++)
        {
          auto const& v = (*jsonValue)[i];
          if (!v.IsString())
          {
            result = false;
            return;
          }

          parsedValue.push_back(v.GetString());
        }

        result = setting.setValue(parsedValue);
      }
    
      void visit(config::BgrColourSetting& setting)
      {
        if (!jsonValue->IsObject())
        {
          result = false;
          return;
        }

        auto bMember = jsonValue->FindMember("b");
        auto gMember = jsonValue->FindMember("g");
        auto rMember = jsonValue->FindMember("r");

        if (bMember == jsonValue->MemberEnd()
            || gMember == jsonValue->MemberEnd()
            || rMember == jsonValue->MemberEnd())
        {
          result = false;
          return;
        }

        if (!bMember->value.IsInt() || !gMember->value.IsInt() || !rMember->value.IsInt())
        {
          result = false;
          return;
        }

        result = setting.setValue(colour::BGR(bMember->value.GetInt(),
                                              gMember->value.GetInt(),
                                              rMember->value.GetInt()));
      }

      rapidjson::Value const* jsonValue;
      bool result;
    };

    template<typename T>
    bool SettingReader::setValueFromJson(T& setting, rapidjson::Value const& jsonValue) const
    {
      auto impl = ValueFromJsonImpl{jsonValue};
      
      impl.visit(setting);
      
      return impl.result;
    }

  }
}
