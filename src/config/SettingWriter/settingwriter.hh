// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "config/SettingBase/Setting/setting.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"
#include "util/assert.hh"
#include "util/Log/log.hh"

#include <iostream>

namespace bold
{
  namespace config
  {
    template<typename JSONWriter>
    struct WriteJsonValueImpl;

    template<typename JSONWriter>
    struct WriteJsonMetadataImpl;

    /** Knows how to write setting information to JSON
     *
     * Used to write a setting's value, its metadata, or full JSON
     * including its description, value, and metadata. If the actual
     * setting type is known at compile time, the writer can write the
     * setting directly. Else it uses double dispatch with the visitor
     * pattern to deduce the type, which is less efficient.
     */
    class SettingWriter
    {
    public:
      template<typename JSONWriter, typename T>
      void writeJsonValue(JSONWriter& jsonWriter, T& setting)
      {
        auto impl = WriteJsonValueImpl<JSONWriter>{jsonWriter};
        impl.visit(setting);
      }

      template<typename JSONWriter, typename T>
      void writeJsonMetadata(JSONWriter& jsonWriter, T& setting)
      {
        auto impl = WriteJsonMetadataImpl<JSONWriter>{jsonWriter};
        impl.visit(setting);
      }

      template<typename JSONWriter, typename T>
      void writeFullJson(JSONWriter& jsonWriter, T& setting)
      {
        jsonWriter.StartObject();
        {
          jsonWriter.String("path");
          jsonWriter.String(setting.getPath().c_str());
          jsonWriter.String("type");
          jsonWriter.String(setting.getTypeName().c_str());
          if (setting.getDescription().size() > 0)
          {
            jsonWriter.String("description");
            jsonWriter.String(setting.getDescription().c_str());
          }
          if (setting.isReadOnly())
          {
            jsonWriter.String("readonly");
            jsonWriter.Bool(true);
          }
          jsonWriter.String("value");
          writeJsonValue(jsonWriter, setting);
          writeJsonMetadata(jsonWriter, setting);
        }
        jsonWriter.EndObject();
      }
    };



    template<typename JSONWriter>
    struct WriteJsonValueImpl : public util::ConstVisitor<config::AllSettingTypes>
    {
      WriteJsonValueImpl(JSONWriter& jsonWriter)
        : jsonWriter{&jsonWriter}
      {}
    
      void visit(config::SettingBase const& setting)
      {
        setting.accept(*this);
      }

      void visit(config::IntSetting const& setting) override
      {
        jsonWriter->Int(setting.getValue());
      }

      void visit(config::EnumSetting const& setting) override
      {
        jsonWriter->Int(setting.getValue());
      }

      void visit(config::DoubleSetting const& setting) override
      {
        jsonWriter->Double(setting.getValue());
      }

      void visit(config::BoolSetting const& setting) override
      {
        jsonWriter->Bool(setting.getValue());
      }

      void visit(config::HsvRangeSetting const& setting) override
      {
        jsonWriter->StartObject();
        {
          jsonWriter->String("hue");
          jsonWriter->StartArray();
          jsonWriter->Uint(setting.getValue().hMin);
          jsonWriter->Uint(setting.getValue().hMax);
          jsonWriter->EndArray();

          jsonWriter->String("sat");
          jsonWriter->StartArray();
          jsonWriter->Uint(setting.getValue().sMin);
          jsonWriter->Uint(setting.getValue().sMax);
          jsonWriter->EndArray();

          jsonWriter->String("val");
          jsonWriter->StartArray();
          jsonWriter->Uint(setting.getValue().vMin);
          jsonWriter->Uint(setting.getValue().vMax);
          jsonWriter->EndArray();
        }
        jsonWriter->EndObject();
      }

      void visit(config::DoubleRangeSetting const& setting) override
      {
        jsonWriter->StartArray();
        jsonWriter->Double(setting.getValue().min());
        jsonWriter->Double(setting.getValue().max());
        jsonWriter->EndArray();
      }

      void visit(config::StringSetting const& setting) override
      {
        jsonWriter->String(setting.getValue().c_str());
      }

      void visit(config::StringArraySetting const& setting) override
      {
        jsonWriter->StartArray();
        {
          for (auto const& s : setting.getValue())
            jsonWriter->String(s.c_str());
        }
        jsonWriter->EndArray();
      }

      void visit(config::BgrColourSetting const& setting) override
      {
        jsonWriter->StartObject();
        {
          jsonWriter->String("r");
          jsonWriter->Int(setting.getValue().r);
          jsonWriter->String("g");
          jsonWriter->Int(setting.getValue().g);
          jsonWriter->String("b");
          jsonWriter->Int(setting.getValue().b);
        }
        jsonWriter->EndObject();
      }

      JSONWriter* jsonWriter;
    };


  
    template<typename JSONWriter>
    struct WriteJsonMetadataImpl : public util::ConstVisitor<config::AllSettingTypes>
    {
      WriteJsonMetadataImpl(JSONWriter& jsonWriter)
        : jsonWriter{&jsonWriter}
      {}

      void visit(config::SettingBase const& setting)
      {
        setting.accept(*this);
      }

      void visit(config::IntSetting const& setting) override
      {
        writeCommonMetadata(setting);

        if (setting.getMin() != -std::numeric_limits<int>::max())
        {
          jsonWriter->String("min");
          jsonWriter->Int(setting.getMin());
        }

        if (setting.getMax() != std::numeric_limits<int>::max())
        {
          jsonWriter->String("max");
          jsonWriter->Int(setting.getMax());
        }
      }

      void visit(config::EnumSetting const& setting) override
      {
        writeCommonMetadata(setting);
        jsonWriter->String("values");
        jsonWriter->StartArray();
        {
          for (auto const& pair : setting.getPairs())
          {
            jsonWriter->StartObject();
            jsonWriter->String("text");
            jsonWriter->String(pair.second.c_str());
            jsonWriter->String("value");
            jsonWriter->Int(pair.first);
            jsonWriter->EndObject();
          }
        }
        jsonWriter->EndArray();
      }

      void visit(config::DoubleSetting const& setting) override
      {
        writeCommonMetadata(setting);

        if (setting.getMin() != -std::numeric_limits<double>::max())
        {
          jsonWriter->String("min");
          jsonWriter->Double(setting.getMin());
        }
        if (setting.getMax() != std::numeric_limits<double>::max())
        {
          jsonWriter->String("max");
          jsonWriter->Double(setting.getMax());
        }
      }

      void visit(config::BoolSetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

      void visit(config::HsvRangeSetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

      void visit(config::DoubleRangeSetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

      void visit(config::StringSetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

      void visit(config::StringArraySetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

      void visit(config::BgrColourSetting const& setting) override
      {
        writeCommonMetadata(setting);
      }

    private:
      template<typename T>
      void writeCommonMetadata(config::Setting<T> const& setting)
      {
        ASSERT(setting.isInitialValueSet() ||
               !(util::Log::error("Setting::writeJsonMetadataInternal") << "Initial value not set for: " << setting.getPath()));

        jsonWriter->String("initial");

        auto valueImpl = WriteJsonValueImpl<JSONWriter>{*jsonWriter};
        valueImpl.visit(setting);
      }
    
      JSONWriter* jsonWriter;
    };
  }
}
