// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionscriptlibrary.hh"

#include <dirent.h>

#include "motion/scripts/MotionScript/motionscript.hh"
#include "util/Log/log.hh"
#include "util/string.hh"

using namespace bold::motion::scripts;
using namespace bold::util;
using namespace bold;

MotionScriptLibrary::MotionScriptLibrary(std::string const& dir)
  : d_motionScriptsDirectory{dir}
{
  loadAll();
}

void MotionScriptLibrary::loadAll()
{
  auto scriptFiles = listScripts();
  for (auto const& fileName : scriptFiles)
  {
    auto filePath = d_motionScriptsDirectory + "/" + fileName;
    auto script = MotionScript::fromFile(filePath);
    if (script != nullptr)
      d_scripts[script->getName()] = std::make_pair(fileName, script);
  }
}

std::vector<std::shared_ptr<MotionScript>> MotionScriptLibrary::getAllScripts() const
{
  auto scriptsVector = std::vector<std::shared_ptr<MotionScript>>{};

  for (auto const& entries : d_scripts)
    scriptsVector.emplace_back(entries.second.second);

  return scriptsVector;
}

void MotionScriptLibrary::reloadAll()
{
  auto scriptFiles = listScripts();
  for (auto const& fileName : scriptFiles) {
    auto filePath = d_motionScriptsDirectory + "/" + fileName;
    auto script = MotionScript::fromFile(filePath);
    if (script != nullptr) {
      auto name = script->getName();

      // If script already exists, overwrite such that any pointers to the script see the update
      if (d_scripts.find(name) != d_scripts.end())
        *d_scripts[name].second = *script;
      // Else insert new entry
      else {
        Log::info("MotionScriptLibrary::reloadAll") << "Found new script: " << name << ", file: " << fileName;
        d_scripts[name] = std::make_pair(fileName, script);
      }
    }
  }
}

std::vector<std::string> MotionScriptLibrary::listScripts() const {
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir(d_motionScriptsDirectory.c_str())) == nullptr)
  {
    util::Log::error("MotionScript::loadAllInPath") << "Unable to open motion scripts directory";
    throw std::runtime_error("Unable to open motion scripts directory");
  }

  auto scriptFiles = std::vector<std::string>{};

  auto extension = std::string{".json"};

  while ((ent = readdir(dir)) != nullptr) {
    // Skip anything that's not a file
    if (ent->d_type != DT_REG)
      continue;

    auto fileName = std::string{ent->d_name};

    // Skip anything that does not have the right extension
    if (!stringutil::endsWith(fileName, extension))
      continue;
    scriptFiles.push_back(fileName);
  }

  closedir(dir);
  return scriptFiles;
}
