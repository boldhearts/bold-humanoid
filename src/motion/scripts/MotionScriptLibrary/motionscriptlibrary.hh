// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <memory>
#include <map>
#include <vector>

namespace bold
{
  namespace motion
  {
    namespace scripts
    {
      class MotionScript;

      /** Repository of motion scripts
       */
      class MotionScriptLibrary
      {
      public:
        /** Loads all scripts in the given directory
         *
         * @param dir The path of the directory, should be relative to the current working directory
         */
        MotionScriptLibrary(std::string const& dir);

        /** Reload all scripts from disk
         *
         * This affects all scripts previously obtained from this library
         */
        void reloadAll();

        /** Get a script by name
         *
         * This is the name as given in the script
         */
        std::shared_ptr<MotionScript> getScript(std::string const& name);

        /** Get all scripts in this library
         */
        std::vector<std::shared_ptr<MotionScript>> getAllScripts() const;
        
      private:
        std::string d_motionScriptsDirectory;

        // Holds all scripts keyed by their name, paired with their filename
        std::map<std::string, std::pair<std::string, std::shared_ptr<MotionScript>>> d_scripts;

        void loadAll();

        std::vector<std::string> listScripts() const;
      };
    }
  }
}

