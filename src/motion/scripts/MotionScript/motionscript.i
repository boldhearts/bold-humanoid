%{
#include <motion/scripts/MotionScript/motionscript.hh>
%}

namespace bold
{
  namespace motion
  {
    namespace scripts
    {
      class MotionScript
      {
      public:
        struct KeyFrame
        {
          KeyFrame();

          uint16_t getValue(uint8_t jointId);
          uint8_t pauseCycles;
          uint8_t moveCycles;
          std::array<uint16_t,(int)bold::JointId::MAX> values;
        };

        struct Stage
        {
          static const uint8_t DEFAULT_SPEED;
          static const uint8_t DEFAULT_P_GAIN;

          Stage();
          uint8_t getPGain(uint8_t jointId) const;
          unsigned speed;
          unsigned repeatCount;
          std::array<uint8_t,(int)bold::JointId::MAX> pGains;
          std::vector<KeyFrame> keyFrames;
        };

        static std::shared_ptr<MotionScript> fromFile(std::string fileName);

        std::string getName() const { return d_name; }
        void setName(std::string name) { d_name = name; }

        std::shared_ptr<MotionScript> getMirroredScript(std::string name) const;

        bool writeJsonFile(std::string fileName) const;

        std::vector<std::shared_ptr<Stage>> const& getStages() const { return d_stages; }
        KeyFrame const& getFinalKeyFrame() const;
      };
    }
  }
}
