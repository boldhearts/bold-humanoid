// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "JointId/jointid.hh"
#include "MX28/mx28.hh"
#include "util/Log/log.hh"
#include "util/string.hh"

#include <rapidjson/document.h>
#include <rapidjson/error/en.h>

#include <algorithm>
#include <iosfwd>
#include <vector>
#include <memory>
#include <array>
#include <cstdint>

namespace bold
{
  namespace motion
  {
    namespace scripts
    {
      /** Describes a playable motion sequence defined using key frames.
       *
       * Motion scripts are divided across several levels:
       *
       *     Motion Script -> Stages -> Key Frames -> Sections -> Steps
       *
       * Motion Scripts
       *
       * - have a name
       * - contain one or more stages
       *
       * Stages
       *
       * - may be repeated N times in a row
       * - can specify p-gains and a speed value
       * - contain one or more key frames
       *
       * Key Frames
       *
       * - specify a set of target positions for motors
       * - specify how many cycles it takes to reach this target
       * - specify an optional number of cycles to pause for after reaching the target
       *
       * During the playback of the script, the time between key frames is divided
       * into sections: PRE, MAIN, POST, PAUSE
       *
       * Sections
       *
       *   PRE
       *     - The initial period of movement between key frames
       *     - Speed will slowly accelerate if coming from a zero finish speed
       *     - Speed will be constant if coming from a non-zero finish speed
       *
       *   MAIN
       *     - A, sometimes short, period of time between PRE and POST
       *     - May involve a very sudden change in position, depending upon the
       *       distance to travel and moveCycles
       *
       *   POST
       *     - The final period of movement between key frames
       *     - Speed will slowly decelerate if approaching a zero finish speed
       *     - Speed will be constant if approaching a non-zero finish speed
       *
       * Steps
       *
       * - another name for 'cycle'
       * - the discrete calculation unit for motor positioning
       * - 8ms apart (125 Hz)
       */
      class MotionScript
      {
      public:
        static const uint16_t INVALID_BIT_MASK = 0x4000;

        /** Describes a body posture via a set of target motor positions.
         *
         * Also defines the number of cycles over which to approach this
         * target position. An optional number of cycles to pause for
         * after motion completes may be specified. Therefore, total
         * duration of this keyframe is moveCycles + pauseCycles.
         */
        struct KeyFrame
        {
          KeyFrame()
            : pauseCycles{0},
              moveCycles{0},
              values{}
          {}

          KeyFrame(KeyFrame const& other) = default;

          /// Gets the position value to apply to the specified joint during this stage of motion
          uint16_t getValue(uint8_t jointId) const { return values.at(jointId - 1u); }

          /// The number of cycles to pause for after the target position has been reached.
          uint8_t pauseCycles;

          /// The number of cycles over which motion towards the target position is planned.
          uint8_t moveCycles;

          /// The target motor position values, indexed by joint ID - 1.
          std::array<uint16_t,(int)JointId::MAX> values;
        };

        /** Describes a group of key frames that share some configuration and may be repeated several times.
         *
         * Configuration options are speed and p-gains per joint.
         */
        struct Stage
        {
          static const uint8_t DEFAULT_SPEED;
          static const uint8_t DEFAULT_P_GAIN;

          Stage()
            : speed(DEFAULT_SPEED),
              repeatCount(1),
              keyFrames()
          {
            pGains.fill(DEFAULT_P_GAIN);
          }

          Stage(Stage const& other) = default;

          /// Gets the proportional gain to apply to the specified joint during this stage of motion
          inline uint8_t getPGain(uint8_t jointId) const { return pGains.at(jointId - 1u); }

          /// Gets the proportional gain to apply to the specified joint during this stage of motion
          inline uint8_t getPGain(JointId jointId) const { return pGains.at((uint8_t)jointId - 1u); }

          /// The speed for motion during this stage. Defaults to DEFAULT_SPEED.
          unsigned speed;

          /// The number of times this stage should be played. Defaults to 1.
          unsigned repeatCount;
      
          /// The p-gain value to be used throughout this stage, indexed by joint ID - 1.
          std::array<uint8_t,(int)JointId::MAX> pGains;
      
          /// The KeyFrames contained within this stage.
          std::vector<KeyFrame> keyFrames;
        };

        /** Loads a MotionScript from a JSON stream
         */
        template<typename JSONStream>
        static std::shared_ptr<MotionScript> fromStream(JSONStream&& stream);
        
        /** Loads a MotionScript from the specified JSON file.
         */
        static std::shared_ptr<MotionScript> fromFile(std::string const& fileName);

        /** Loads a MotionScript from the specified JSON content.
         */
        static std::shared_ptr<MotionScript> fromJsonValue(rapidjson::Value& value);

        /** Initialises the MotionScript with specified name and stages.
         */
        MotionScript(std::string name, std::vector<std::shared_ptr<Stage>> stages,
                     bool controlsHead, bool controlsArms, bool controlsLegs)
          : d_name(name),
            d_stages(stages),
            d_controlsHead(controlsHead),
            d_controlsArms(controlsArms),
            d_controlsLegs(controlsLegs)
        {}

        std::string getName() const { return d_name; }
        void setName(std::string name) { d_name = name; }

        /** Get version of script that is left-right mirrored
         */
        std::shared_ptr<MotionScript> getMirroredScript(std::string name) const;

        bool writeJsonFile(std::string fileName) const;

        template<typename TWriter>
        void writeJson(TWriter& writer) const;

        std::vector<std::shared_ptr<Stage>> const& getStages() const { return d_stages; }
        KeyFrame const& getFinalKeyFrame() const;

        bool getControlsHead() const { return d_controlsHead; }
        bool getControlsArms() const { return d_controlsArms; }
        bool getControlsLegs() const { return d_controlsLegs; }

      private:
        static void makeAutoStandKeyFrame(KeyFrame& keyFrame);

        std::string d_name;
        std::vector<std::shared_ptr<Stage>> d_stages;
        bool d_controlsHead;
        bool d_controlsArms;
        bool d_controlsLegs;
      };


      template<typename JSONStream>
      inline std::shared_ptr<MotionScript> MotionScript::fromStream(JSONStream&& stream)
      {
        rapidjson::Document document;
        document.ParseStream<0, rapidjson::UTF8<>, JSONStream>(stream);

        if (document.HasParseError())
        {
          bold::util::Log::error("MotionScript::fromStream") << "Parse error at offset " << document.GetErrorOffset() << "': "
                                                             << rapidjson::GetParseError_En(document.GetParseError());
          return nullptr;
        }

        auto script = fromJsonValue(document);
        
        return script;
      }
      
      template<typename TWriter>
      inline void MotionScript::writeJson(TWriter& writer) const
      {
        writer.StartObject();
        {
          writer.String("name");
          writer.String(d_name.c_str());

          if (!d_controlsHead)
            {
              writer.String("controlsHead");
              writer.Bool(d_controlsHead);
            }

          if (!d_controlsArms)
            {
              writer.String("controlsArms");
              writer.Bool(d_controlsArms);
            }

          if (!d_controlsLegs)
            {
              writer.String("controlsLegs");
              writer.Bool(d_controlsLegs);
            }

          writer.String("stages");
          writer.StartArray();
          {
            for (std::shared_ptr<Stage> stage : d_stages)
              {
                writer.StartObject();
                {
                  if (stage->repeatCount != 1)
                    {
                      writer.String("repeat");
                      writer.Int(stage->repeatCount);
                    }

                  if (stage->speed != Stage::DEFAULT_SPEED)
                    {
                      writer.String("speed");
                      writer.Int(stage->speed);
                    }

                  if (std::find_if(stage->pGains.begin(), stage->pGains.end(), [](uint8_t p) { return p != Stage::DEFAULT_P_GAIN; }) != stage->pGains.end())
                    {
                      writer.String("pGains");
                      writer.StartObject();
                      for (uint8_t j = (uint8_t)JointId::MIN; j <= (uint8_t)JointId::MAX; j++)
                        {
                          uint8_t pGain = stage->pGains[j-1];
                          if (pGain == Stage::DEFAULT_P_GAIN)
                            continue;

                          // Abbreviate paired gains
                          if (JointPairs::isBase(j))
                            {
                              uint8_t r = stage->pGains[j-1];
                              uint8_t l = stage->pGains[j];
                              if (r == l)
                                {
                                  writer.String(JointName::getJsonPairName(j).c_str());
                                  writer.Uint(r);
                                  j++;
                                  continue;
                                }
                            }

                          writer.String(JointName::getJsonName(j).c_str());
                          writer.Uint(pGain);
                        }
                      writer.EndObject();
                    }

                  writer.String("keyFrames");
                  writer.StartArray();
                  {
                    for (KeyFrame const& step : stage->keyFrames)
                      {
                        writer.StartObject();
                        {
                          if (step.pauseCycles != 0)
                            {
                              writer.String("pauseCycles");
                              writer.Int(step.pauseCycles);
                            }

                          writer.String("moveCycles");
                          writer.Int(step.moveCycles);

                          writer.String("values");
                          writer.StartObject();
                          for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
                            {
                              // Abbreviate mirrored values
                              if (JointPairs::isBase(jointId))
                                {
                                  uint16_t rVal = step.values[jointId-1];
                                  uint16_t lVal = step.values[jointId];
                                  if (rVal == MX28::getMirrorValue(lVal))
                                    {
                                      writer.String(JointName::getJsonPairName(jointId).c_str());
                                      writer.Uint(rVal);
                                      jointId++;
                                      continue;
                                    }
                                }

                              writer.String(JointName::getJsonName(jointId).c_str());
                              writer.Uint(step.values[jointId-1]);
                            }
                          writer.EndObject();
                        }
                        writer.EndObject();
                      }
                  }
                  writer.EndArray();
                }
                writer.EndObject();
              }
          }
          writer.EndArray();
        }
        writer.EndObject();
      }
    }
  }
}
