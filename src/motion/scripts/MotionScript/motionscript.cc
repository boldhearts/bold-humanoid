// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionscript.hh"

#include "config/Config/config.hh"
#include "Kinematics/kinematics.hh"
#include "Math/math.hh"

#include "util/Log/log.hh"
#include "util/json.hh"
#include "util/string.hh"

#include <dirent.h>

#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/error/en.h>

using namespace bold;
using namespace bold::config;
using namespace bold::motion::scripts;
using namespace bold::util::json;
using namespace rapidjson;
using namespace std;

using bold::util::Log;

const uint8_t MotionScript::Stage::DEFAULT_SPEED = 32;
const uint8_t MotionScript::Stage::DEFAULT_P_GAIN = 32;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

shared_ptr<MotionScript> MotionScript::fromFile(string const& fileName)
{
  FILE* file = fopen(fileName.c_str(), "rb");
  if (!file)
  {
    Log::error("MotionScript::fromFile") << "Unable to open file \"" << fileName << "\": "
                                         << strerror(errno) << " (" << errno << ")";
    return nullptr;
  }

  char buffer[65536];
  FileReadStream is(file, buffer, sizeof(buffer));
  auto script = fromStream(is);

  if (script == nullptr)
    Log::error("MotionScript::fromFile") << "Failed reading script from file: " << fileName;
  
  if (fclose(file) != 0)
    Log::warning("MotionScript::fromFile") << "Error closing file \"" << fileName << "\": "
                                           << strerror(errno) << " (" << errno << ")";

  return script;
}

shared_ptr<MotionScript> MotionScript::fromJsonValue(Value& document)
{
  auto name = document["name"].GetString();

  bool controlsHead = getBoolWithDefault(document, "controlsHead", true);
  bool controlsArms = getBoolWithDefault(document, "controlsArms", true);
  bool controlsLegs = getBoolWithDefault(document, "controlsLegs", true);

  vector<shared_ptr<Stage>> stages;

  auto const& stagesArray = document["stages"];
  for (unsigned i = 0; i < stagesArray.Size(); i++)
  {
    auto const& stageMember = stagesArray[i];
    auto stage = make_shared<Stage>();

    stage->repeatCount = (uint8_t)getUintWithDefault(stageMember, "repeat", 1);
    stage->speed       = (uint8_t)getUintWithDefault(stageMember, "speed", Stage::DEFAULT_SPEED);

    ASSERT(stage->speed != 0);

    auto gainsMember = stageMember.FindMember("pGains");
    if (gainsMember != stageMember.MemberEnd())
    {
      for (uint8_t g = 0; g < (uint8_t)JointId::MAX; g++)
      {
        // Try to find a paired gain entry
        if (JointPairs::isBase(g + (uint8_t)1))
        {
          string pairName = JointName::getJsonPairName(g + (uint8_t)1);
          auto pairMember = gainsMember->value.FindMember(pairName.c_str());
          if (pairMember != gainsMember->value.MemberEnd())
          {
            // If a pair exists, the individual gains shouldn't
            if (gainsMember->value.FindMember(JointName::getJsonName(g + (uint8_t)1).c_str()) != gainsMember->value.MemberEnd() ||
                gainsMember->value.FindMember(JointName::getJsonName(g + (uint8_t)2).c_str()) != gainsMember->value.MemberEnd())
            {
              Log::error("MotionScript::fromFile") << "JSON specifies pGain pair name '" << pairName << "' but also specifies L or R property in same stage";
              return nullptr;
            }

            uint8_t value = (uint8_t)pairMember->value.GetUint();

            stage->pGains[g] = value;
            stage->pGains[g+1] = value;

            g++;
            continue;
          }
        }

        stage->pGains[g] = (uint8_t)getUintWithDefault(gainsMember->value, JointName::getJsonName(g + (uint8_t)1).c_str(), Stage::DEFAULT_P_GAIN);
      }
    }

    auto const& keyFrames = stageMember["keyFrames"];
    for (unsigned j = 0; j < keyFrames.Size(); j++)
    {
      auto const& keyFrameMember = keyFrames[j];
      auto keyFrame = KeyFrame();

      keyFrame.pauseCycles = (uint8_t)getUintWithDefault(keyFrameMember, "pauseCycles", 0u);
      keyFrame.moveCycles = (uint8_t)keyFrameMember["moveCycles"].GetUint();

      if (keyFrame.moveCycles < 3)
      {
        // NOTE the MotionScriptRunner hits an arithmetic error (div by zero) if
        //      moveCycles is less than 3. Should really fix the bug there, but
        //      this is easier for now and provides quick feedback.
        Log::error("MotionScript::fromFile") << "moveCycles value must be greater than 2";
        return nullptr;
      }

      auto const& autoMember = keyFrameMember.FindMember("auto");
      if (autoMember != keyFrameMember.MemberEnd())
      {
        auto autoFrameType = autoMember->value.GetString();
        if (string(autoFrameType) == "stand")
        {
          Log::info("MotionScript::fromJsonValue") << "Making automatic stand keyframe";
          makeAutoStandKeyFrame(keyFrame);
          stage->keyFrames.push_back(keyFrame);
          continue;
        }
        Log::warning("MotionScript::fromJsonValue") << "Unknown auto keyframe type: " << autoFrameType;
      }

      auto const& valuesMember = keyFrameMember["values"];
      for (uint8_t v = 0; v < (uint8_t)JointId::MAX; v++)
      {
        // Try to find a paired value
        if (JointPairs::isBase(v + (uint8_t)1))
        {
          string pairName = JointName::getJsonPairName(v + (uint8_t)1);
          auto pairMember = valuesMember.FindMember(pairName.c_str());
          if (pairMember != valuesMember.MemberEnd())
          {
            // If a pair exists, the individual values shouldn't
            if (valuesMember.FindMember(JointName::getJsonName(v + (uint8_t)1).c_str()) != valuesMember.MemberEnd() ||
                valuesMember.FindMember(JointName::getJsonName(v + (uint8_t)2).c_str()) != valuesMember.MemberEnd())
            {
              Log::error("MotionScript::fromFile") << "JSON specifies value pair name '" << pairName << "' but also specifies L or R property in same stage";
              return nullptr;
            }

            uint16_t value = (uint16_t)pairMember->value.GetUint();

            keyFrame.values[v] = value;
            keyFrame.values[v+1] = MX28::getMirrorValue(value);

            v++;
            continue;
          }
        }

        string propName = JointName::getJsonName(v + (uint8_t)1);
        auto prop = valuesMember.FindMember(propName.c_str());
        if (prop == valuesMember.MemberEnd())
        {
          Log::error("MotionScript::fromFile") << "Missing property " << propName;
          return nullptr;
        }
        keyFrame.values[v] = (uint16_t)prop->value.GetUint();
      }

      stage->keyFrames.push_back(keyFrame);
    }

    stages.push_back(stage);
  }

  return make_shared<MotionScript>(name, stages, controlsHead, controlsArms, controlsLegs);
}

bool MotionScript::writeJsonFile(string fileName) const
{
  FILE *file = fopen(fileName.c_str(), "wb");

  if (file == 0)
  {
    Log::error("MotionScript::writeJsonFile") << "Can not open output file for writing: " << fileName;
    return false;
  }

  Log::info("MotionScript::writeJsonFile") << "Writing: " << fileName;

  char buffer[4096];
  FileWriteStream f(file, buffer, sizeof(buffer));
  PrettyWriter<FileWriteStream> writer(f);
  writer.SetIndent(' ', 2);

  writeJson(writer);

  f.Put('\n');

  f.Flush();

  if (fclose(file) != 0)
    Log::warning("MotionScript::writeJsonFile") << "Error closing file \"" << fileName << "\": " << strerror(errno) << " (" << errno << ")";

  return true;
}

MotionScript::KeyFrame const& MotionScript::getFinalKeyFrame() const
{
  ASSERT(d_stages.size() > 0);
  auto const& finalStage = d_stages.back();
  ASSERT(finalStage->keyFrames.size());
  return finalStage->keyFrames[finalStage->keyFrames.size() - 1];
}

shared_ptr<MotionScript> MotionScript::getMirroredScript(std::string name) const
{
  // Copy the script
  auto mirror = make_shared<MotionScript>(*this);

  // Set the new name
  mirror->setName(name);

  for (auto& stage : mirror->d_stages)
  {
    // Transpose p-gains across body
    std::swap(stage->pGains.at((uint8_t)JointId::L_ELBOW - 1),          stage->pGains.at((uint8_t)JointId::R_ELBOW - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_SHOULDER_PITCH - 1), stage->pGains.at((uint8_t)JointId::R_SHOULDER_PITCH - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_SHOULDER_ROLL - 1),  stage->pGains.at((uint8_t)JointId::R_SHOULDER_ROLL - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_ANKLE_PITCH - 1),    stage->pGains.at((uint8_t)JointId::R_ANKLE_PITCH - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_ANKLE_ROLL - 1),     stage->pGains.at((uint8_t)JointId::L_ANKLE_ROLL - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_KNEE - 1),           stage->pGains.at((uint8_t)JointId::R_KNEE - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_HIP_YAW - 1),        stage->pGains.at((uint8_t)JointId::R_HIP_YAW - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_HIP_PITCH - 1),      stage->pGains.at((uint8_t)JointId::R_HIP_PITCH - 1));
    std::swap(stage->pGains.at((uint8_t)JointId::L_HIP_ROLL - 1),       stage->pGains.at((uint8_t)JointId::R_HIP_ROLL - 1));

    for (auto& keyFrame : stage->keyFrames)
    {
      // Transpose values across body
      std::swap(keyFrame.values.at((uint8_t)JointId::L_ELBOW - 1),          keyFrame.values.at((uint8_t)JointId::R_ELBOW - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_SHOULDER_PITCH - 1), keyFrame.values.at((uint8_t)JointId::R_SHOULDER_PITCH - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_SHOULDER_ROLL - 1),  keyFrame.values.at((uint8_t)JointId::R_SHOULDER_ROLL - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_ANKLE_PITCH - 1),    keyFrame.values.at((uint8_t)JointId::R_ANKLE_PITCH - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_ANKLE_ROLL - 1),     keyFrame.values.at((uint8_t)JointId::L_ANKLE_ROLL - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_KNEE - 1),           keyFrame.values.at((uint8_t)JointId::R_KNEE - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_HIP_YAW - 1),        keyFrame.values.at((uint8_t)JointId::R_HIP_YAW - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_HIP_PITCH - 1),      keyFrame.values.at((uint8_t)JointId::R_HIP_PITCH - 1));
      std::swap(keyFrame.values.at((uint8_t)JointId::L_HIP_ROLL - 1),       keyFrame.values.at((uint8_t)JointId::R_HIP_ROLL - 1));

      // Mirror values
      for (uint8_t jointId = (uint8_t)JointId::R_SHOULDER_PITCH; jointId <= (uint8_t)JointId::HEAD_PAN; jointId++)
        keyFrame.values.at(jointId - (uint8_t)1) = MX28::getMirrorValue(keyFrame.values.at(jointId - (uint8_t)1));

      // TODO: Further investigate the reason for why this must be done for ankle joints only for a good mirror

      // Swapped to create correct mirror from script generated on one side
      std::swap(keyFrame.values.at((uint8_t)JointId::L_ANKLE_ROLL - 1), keyFrame.values.at((uint8_t)JointId::R_ANKLE_ROLL - 1));
    }
  }

  return mirror;
}

void MotionScript::makeAutoStandKeyFrame(KeyFrame& keyFrame)
{
  auto _xOffset = Config::getSetting<double>("walk-engine.params.x-offset");
  auto _yOffset = Config::getSetting<double>("walk-engine.params.y-offset");
  auto _zOffset = Config::getSetting<double>("walk-engine.params.z-offset");
  auto _hipPitchStand = Config::getSetting<double>("walk-module.hip-pitch.stable-angle-stand");

  auto offsets = Eigen::Vector3d(_xOffset->getValue(), _yOffset->getValue(), _zOffset->getValue());
  auto hipPitchStand = Math::degToRad(_hipPitchStand->getValue());

  Log::verbose("MotionScript::makeAutoStandKeyFrame") << "Offets: " << offsets.transpose() << ", hip pitch: " << hipPitchStand;

  auto maybeTargetAngles =
    Kinematics::targetAnglesForStandingWithOffsets(offsets,
                                                   -hipPitchStand);

  keyFrame.values.fill(MX28::CENTER_VALUE);

  for (auto targetAngles : maybeTargetAngles)
    for (auto jointAngle : targetAngles)
      keyFrame.values[static_cast<uint8_t>(jointAngle.first) - 1] = MX28::rads2Value(jointAngle.second);
}
