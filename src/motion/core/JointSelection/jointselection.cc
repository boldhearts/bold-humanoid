// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "jointselection.hh"

#include "JointId/jointid.hh"

using namespace bold;
using namespace bold::motion::core;

JointSelection::JointSelection(bool head, bool arms, bool legs)
: d_head(head),
  d_arms(arms),
  d_legs(legs)
{
  bool set = arms;
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    if (jointId == (int)JointId::LEGS_START)
      set = legs;
    else if (jointId == (int)JointId::HEAD_START)
      set = head;

    d_set[jointId] = set;
  }
}

