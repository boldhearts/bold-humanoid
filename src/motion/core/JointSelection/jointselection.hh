// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <memory>

namespace bold
{
  namespace motion
  {
    namespace core
    {
      
      /** Models a set of joints.
       */
      class JointSelection
      {
      public:
        JointSelection(bool head, bool arms, bool legs);

        bool const& operator[] (uint8_t jointId) const { return d_set[jointId]; }

        bool hasHead() const { return d_head; }
        bool hasArms() const { return d_arms; }
        bool hasLegs() const { return d_legs; }

        static std::shared_ptr<JointSelection> const& all();
        static std::shared_ptr<JointSelection> const& head();
        static std::shared_ptr<JointSelection> const& arms();
        static std::shared_ptr<JointSelection> const& legs();
        static std::shared_ptr<JointSelection> const& headAndArms();
        static std::shared_ptr<JointSelection> const& headAndLegs();
        static std::shared_ptr<JointSelection> const& armsAndLegs();

      private:
        bool d_set[21];
        bool d_head;
        bool d_arms;
        bool d_legs;
      };

      inline std::shared_ptr<JointSelection> const& JointSelection::all()
      {
        static auto instance = std::make_shared<JointSelection>(true, true, true);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::head()
      {
        static auto instance = std::make_shared<JointSelection>(true, false, false);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::arms()
      {
        static auto instance = std::make_shared<JointSelection>(false, true, false);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::legs()
      {
        static auto instance = std::make_shared<JointSelection>(false, false, true);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::headAndArms()
      {
        static auto instance = std::make_shared<JointSelection>(true, true, false);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::headAndLegs()
      {
        static auto instance = std::make_shared<JointSelection>(true, false, true);
        return instance;
      }
      
      inline std::shared_ptr<JointSelection> const& JointSelection::armsAndLegs()
      {
        static auto instance = std::make_shared<JointSelection>(false, true,  true);
        return instance;
      }
      
    }
  }
}
