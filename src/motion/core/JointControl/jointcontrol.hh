// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include "src/util/Range/range.hh"

namespace bold
{
  enum class MX28Table : uint8_t;

  namespace motion
  {
    namespace core
    {
      
      class JointControl
      {
      public:
        static constexpr uint8_t DefaultPGain = 32;
        static constexpr uint8_t DefaultIGain = 0;
        static constexpr uint8_t DefaultDGain = 0;

        JointControl(uint8_t jointId);

        uint8_t getId() const { return d_jointId; }

        void setValue(uint16_t value);
        uint16_t getValue() const { return d_value; }

        void setModulationOffset(short delta);
        short getModulationOffset() const { return d_modulationOffset; }

        /// Sets the target angle, in degrees
        void setDegrees(double degrees);
        /// Gets the target angle, in degrees
        double getDegrees() const { return d_degrees; }

        /// Sets the target angle, in radians
        void setRadians(double radian);
        /// Gets the target angle, in radians
        double getRadians() const { return Math::degToRad(getDegrees()); }

        util::Range<MX28Table> getModifiedAddressRange() const { return d_changedAddressRange; }
        bool isDirty() const { return !d_changedAddressRange.isEmpty(); }
        void clearDirty() { d_changedAddressRange.reset(); }

        void notifyOffsetChanged();

        void setPGain(uint8_t p);
        void setIGain(uint8_t i);
        void setDGain(uint8_t d);

        void setPidGains(uint8_t p, uint8_t i, uint8_t d);

        uint8_t getPGain() const { return d_pGain; }
        uint8_t getIGain() const { return d_iGain; }
        uint8_t getDGain() const { return d_dGain; }

      private:
        uint8_t d_jointId;
        uint16_t d_value;
        short d_modulationOffset;
        double d_degrees;
        uint8_t d_pGain;
        uint8_t d_iGain;
        uint8_t d_dGain;
        util::Range<MX28Table> d_changedAddressRange;
      };

    }
  }
}
