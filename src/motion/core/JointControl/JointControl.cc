// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "jointcontrol.hh"

#include "MX28/mx28.hh"

using namespace bold::motion::core;

JointControl::JointControl(uint8_t jointId)
: d_jointId(jointId),
  d_value(MX28::CENTER_VALUE),
  d_modulationOffset(0),
  d_degrees(0.0),
  d_pGain(DefaultPGain),
  d_iGain(DefaultIGain),
  d_dGain(DefaultDGain),
  d_changedAddressRange()
{}

void JointControl::setValue(uint16_t value)
{
  value = MX28::clampValue(value);
  if (d_value == value)
    return;
  d_value = value;
  d_degrees = MX28::value2Degs(value);
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_L);
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_H);
}

void JointControl::setDegrees(double degrees)
{
  degrees = Math::clamp(degrees, MX28::MIN_DEGS, MX28::MAX_DEGS);
  d_degrees = degrees;
  uint16_t value = MX28::degs2Value(degrees);
  if (d_value == value)
    return;
  d_value = value;
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_L);
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_H);
}

void JointControl::setRadians(double radians) { setDegrees(Math::radToDeg(radians)); }

void JointControl::setPGain(uint8_t p) { if (d_pGain == p) return; d_pGain = p; d_changedAddressRange.expand(MX28Table::P_GAIN); }
void JointControl::setIGain(uint8_t i) { if (d_iGain == i) return; d_iGain = i; d_changedAddressRange.expand(MX28Table::I_GAIN); }
void JointControl::setDGain(uint8_t d) { if (d_dGain == d) return; d_dGain = d; d_changedAddressRange.expand(MX28Table::D_GAIN); }

void JointControl::setPidGains(uint8_t p, uint8_t i, uint8_t d) { setPGain(p); setIGain(i); setDGain(d); }

void JointControl::notifyOffsetChanged()
{
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_L);
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_H);
}

void JointControl::setModulationOffset(short delta)
{
  if (d_modulationOffset == delta)
    return;

  d_modulationOffset = delta;
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_L);
  d_changedAddressRange.expand(MX28Table::GOAL_POSITION_H);
}
