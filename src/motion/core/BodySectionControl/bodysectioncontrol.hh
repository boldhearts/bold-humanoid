// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/JointControl/jointcontrol.hh"

#include "JointId/jointid.hh"

namespace bold
{
  namespace motion
  {
    namespace core
    {

      /** Base class to hold all control actions for joints belonging to a body section
       */
      class BodySectionControl
      {
      public:
        BodySectionControl(JointId minJointId, JointId maxJointId)
          : d_minJointId{minJointId},
            d_maxJointId{maxJointId}
        {
          if ((int)minJointId > (int)maxJointId)
            throw std::runtime_error("Invalid min/max joint IDs");

          for (uint8_t jointId = (uint8_t)minJointId; jointId <= (uint8_t)maxJointId; jointId++)
            d_joints.emplace_back(jointId);
        }

        void visitJoints(std::function<void(JointControl&)> action)
        {
          std::for_each(begin(d_joints), end(d_joints), action);
        }

        bool containsJoint(JointId id) const
        {
          return id >= d_minJointId && id <= d_maxJointId;
        }

        std::vector<JointControl>& getJoints() { return d_joints; }
        std::vector<JointControl> const& getJoints() const { return d_joints; }

        JointControl& getJoint(JointId id)
        {
          ASSERT(containsJoint(id));
          return d_joints[(uint8_t)id - (uint8_t)d_minJointId]; 
        }

        JointControl const& getJoint(JointId id) const
        {
          ASSERT(containsJoint(id));
          return d_joints[(uint8_t)id - (uint8_t)d_minJointId]; 
        }

        void clearModulation()
        {
          for (auto& joint : d_joints)
            joint.setModulationOffset(0);
        }

      protected:
        JointId d_minJointId;
        JointId d_maxJointId;

        std::vector<JointControl> d_joints;
      };

      class HeadSectionControl : public BodySectionControl
      {
      public:
        HeadSectionControl() : BodySectionControl(JointId::HEAD_PAN, JointId::HEAD_TILT) {}

        JointControl& pan() { return getJoint(JointId::HEAD_PAN); }
        JointControl& tilt() { return getJoint(JointId::HEAD_TILT); }
      };

      class ArmSectionControl : public BodySectionControl
      {
      public:
        ArmSectionControl() : BodySectionControl(JointId::R_SHOULDER_PITCH, JointId::L_ELBOW) {}

        JointControl& shoulderPitchLeft() { return getJoint(JointId::L_SHOULDER_PITCH); }
        JointControl& shoulderPitchRight() { return getJoint(JointId::R_SHOULDER_PITCH); }
        JointControl& shoulderRollLeft() { return getJoint(JointId::L_SHOULDER_ROLL); }
        JointControl& shoulderRollRight() { return getJoint(JointId::R_SHOULDER_ROLL); }
        JointControl& elbowLeft() { return getJoint(JointId::L_ELBOW); }
        JointControl& elbowRight() { return getJoint(JointId::R_ELBOW); }
      };

      class LegSectionControl : public BodySectionControl
      {
      public:
        LegSectionControl() : BodySectionControl(JointId::R_HIP_YAW, JointId::L_ANKLE_ROLL) {}

        JointControl& hipYawLeft() { return getJoint(JointId::L_HIP_YAW); }
        JointControl& hipYawRight() { return getJoint(JointId::R_HIP_YAW); }
        JointControl& hipRollLeft() { return getJoint(JointId::L_HIP_ROLL); }
        JointControl& hipRollRight() { return getJoint(JointId::R_HIP_ROLL); }
        JointControl& hipPitchLeft() { return getJoint(JointId::L_HIP_PITCH); }
        JointControl& hipPitchRight() { return getJoint(JointId::R_HIP_PITCH); }
        JointControl& kneeLeft() { return getJoint(JointId::L_KNEE); }
        JointControl& kneeRight() { return getJoint(JointId::R_KNEE); }
        JointControl& anklePitchLeft() { return getJoint(JointId::L_ANKLE_PITCH); }
        JointControl& anklePitchRight() { return getJoint(JointId::R_ANKLE_PITCH); }
        JointControl& ankleRollLeft() { return getJoint(JointId::L_ANKLE_ROLL); }
        JointControl& ankleRollRight() { return getJoint(JointId::R_ANKLE_ROLL); }
      };

    }
  }
}
