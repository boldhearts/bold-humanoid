// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/BodySectionId/bodysectionid.hh"

#include <memory>

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class MotionTask;
  
      /** Models a request to control one or more body sections by a MotionModule.
       *
       * Within this request, different parameters may be selected per body section.
       */
      class MotionRequest
      {
      public:
        enum class Status
          {
           /// Request is queued in the scheduler but has not yet been evaluated for
           /// selection.
           Pending,

           /// Request was selected by the scheduler and is executing.
           Selected,

           /// Request was selected and has since run to completion, either by
           /// completing its single think cycle of control, or by being committed and
           /// signaling completion explicitly.
           Completed,

           /// Request did not succeed in being selected. Another request may have been
           /// committed, or had higher priority.
           Ignored
          };

        static std::string getStatusName(Status status);

        MotionRequest() = default;

        MotionRequest(std::shared_ptr<MotionTask> headTask,
                      std::shared_ptr<MotionTask> armsTask,
                      std::shared_ptr<MotionTask> legsTask);
        
        Status getStatus() const;
        std::shared_ptr<MotionTask const> getHeadTask() const { return d_headTask; }
        std::shared_ptr<MotionTask const> getArmsTask() const { return d_armsTask; }
        std::shared_ptr<MotionTask const> getLegsTask() const { return d_legsTask; }

        bool hasCompleted() const;

      private:
        std::shared_ptr<MotionTask const> d_headTask;
        std::shared_ptr<MotionTask const> d_armsTask;
        std::shared_ptr<MotionTask const> d_legsTask;
      };

      std::ostream& operator<<(std::ostream& stream, MotionRequest::Status status);
    }
  }
}
