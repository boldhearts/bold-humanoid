// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionrequest.hh"

#include "motion/core/MotionTask/motiontask.hh"
#include "util/Log/log.hh"

using namespace std;
using namespace bold::motion::core;
using namespace bold::util;

std::string MotionRequest::getStatusName(MotionRequest::Status status)
{
  switch (status)
  {
  case MotionRequest::Status::Pending:   return "Pending";
  case MotionRequest::Status::Selected:  return "Selected";
  case MotionRequest::Status::Completed: return "Completed";
  case MotionRequest::Status::Ignored:   return "Ignored";
    default: return "Unknown";
  }
}

MotionRequest::MotionRequest(std::shared_ptr<MotionTask> headTask,
                             std::shared_ptr<MotionTask> armsTask,
                             std::shared_ptr<MotionTask> legsTask)
  : d_headTask{move(headTask)},
    d_armsTask{move(armsTask)},
    d_legsTask{move(legsTask)}
{
}

MotionRequest::Status MotionRequest::getStatus() const
{
  // Aggregate the state of constituent MotionTask instances.
  vector<shared_ptr<MotionTask const>> tasks = { d_headTask, d_armsTask, d_legsTask };

  int countNotNull = 0;
  int countIgnored = 0;
  int countCompleted = 0;

  for (auto const& task : tasks)
  {
    if (task == nullptr)
      continue;

    countNotNull++;

    switch (task->getStatus())
    {
    case MotionTask::Status::Pending:
        // If any task is pending, then the request is pending
        return MotionRequest::Status::Pending;

    case MotionTask::Status::Selected:
        // If anything is being run, then we're selected
        return MotionRequest::Status::Selected;

    case MotionTask::Status::Completed: countCompleted++; break;
    case MotionTask::Status::Ignored:   countIgnored++;   break;
    }
  }

  if (countNotNull == 0)
    return MotionRequest::Status::Ignored;

  // If all tasks were ignored, then the entire request was ignored
  if (countIgnored == countNotNull)
    return MotionRequest::Status::Ignored;

  // Otherwise, we must be complete
  return MotionRequest::Status::Completed;
}

bool MotionRequest::hasCompleted() const
{
  auto status = getStatus();
  return status == MotionRequest::Status::Completed || status == MotionRequest::Status::Ignored;
}

std::ostream& bold::motion::core::operator<<(std::ostream& stream, MotionRequest::Status status)
{
  return stream << MotionRequest::getStatusName(status);
}

