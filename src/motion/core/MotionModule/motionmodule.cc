// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionmodule.hh"

#include "motion/core/MotionTaskScheduler/motiontaskscheduler.hh"

using namespace bold;
using namespace bold::motion::core;
using namespace std;

MotionModule::MotionModule(std::string const& type, std::shared_ptr<MotionTaskScheduler> scheduler)
  : d_scheduler(move(scheduler)),
    d_name(type),
    d_isCompleted(false)
{
  d_scheduler->registerModule(this);
}
