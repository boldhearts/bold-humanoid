// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <iosfwd>
#include <mutex>

#include "motion/core/PoseProvider/poseprovider.hh"

#include "ThreadUtil/threadutil.hh"
#include "util/assert.hh"

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class JointSelection;
      class MotionTaskScheduler;

  
      /** Abstract base for types of motion such as walking, running scripts or controlling the head.
       */
      class MotionModule : public PoseProvider
      {
        friend class bold::motion::core::MotionTaskScheduler;

      public:
        MotionModule(std::string const& type, std::shared_ptr<motion::core::MotionTaskScheduler> scheduler);

        virtual ~MotionModule() = default;

        std::string getName() const { return d_name; }

        /** Calculate the position for the next timestep.
         *
         * @param selectedJoints indicates which body sections and joints may be
         *                       controlled.
         */
        virtual void step(motion::core::JointSelection const& selectedJoints) = 0;

      protected:
        /// Called by the motion module (on the motion loop thread) when the module
        /// has completed. For example, when a motion script has reached its final
        /// pose, or when walking has come to a stop.
        void setCompletedFlag()
        {
          ASSERT(ThreadUtil::isMotionLoopThread());

          std::lock_guard<std::mutex> guard(d_isCompletedMutex);
          d_isCompleted = true;
        }

        /// Called by the MotionTaskScheduler (on the think thread) to test whether
        /// the module completed since the time this method was called. For each call
        /// to setCompletedFlag, this method will return true once.
        /// When true is returned, any motion tasks associated with this module that
        /// were committed will be removed from the schedule.
        bool clearCompletedFlag()
        {
          ASSERT(ThreadUtil::isThinkLoopThread());

          std::lock_guard<std::mutex> guard(d_isCompletedMutex);
          bool isSet = d_isCompleted;
          d_isCompleted = false;
          return isSet;
        }

        std::shared_ptr<motion::core::MotionTaskScheduler> getScheduler() const { return d_scheduler; }

      private:
        MotionModule(const MotionModule&) = delete;
        MotionModule& operator=(const MotionModule&) = delete;

        std::mutex d_isCompletedMutex;
        std::shared_ptr<motion::core::MotionTaskScheduler> d_scheduler;
        std::string d_name;
        bool d_isCompleted;
      };
    }
  }
}
