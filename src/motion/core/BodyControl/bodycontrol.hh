// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/BodySectionControl/bodysectioncontrol.hh"
#include "JointId/jointid.hh"

#include <memory>
#include <array>

namespace bold
{
  namespace state {
    class HardwareState;
  }

  namespace motion
  {
    namespace core
    {

      class BodyControl
      {
      public:
        BodyControl();

        /** Sets all JointControl positions to match current hardware values. */
        void updateFromHardwareState(std::shared_ptr<state::HardwareState const> const& hw);

        JointControl& getJoint(JointId const id)
        {
          if (d_headSection.containsJoint(id))
            return d_headSection.getJoint(id);
          else if (d_armSection.containsJoint(id))
            return d_armSection.getJoint(id);
          else if (d_legSection.containsJoint(id))
            return d_legSection.getJoint(id);
          else
            throw std::runtime_error("Invalid joint ID");
        }

        JointControl const& getJoint(JointId const id) const
        {
          if (d_headSection.containsJoint(id))
            return d_headSection.getJoint(id);
          else if (d_armSection.containsJoint(id))
            return d_armSection.getJoint(id);
          else if (d_legSection.containsJoint(id))
            return d_legSection.getJoint(id);
          else
            throw std::runtime_error("Invalid joint ID");
        }

        motion::core::HeadSectionControl& getHeadSection() { return d_headSection; }
        motion::core::ArmSectionControl& getArmSection() { return d_armSection; }
        motion::core::LegSectionControl& getLegSection() { return d_legSection; }

        motion::core::HeadSectionControl const& getHeadSection() const { return d_headSection; }
        motion::core::ArmSectionControl const& getArmSection() const { return d_armSection; }
        motion::core::LegSectionControl const& getLegSection() const { return d_legSection; }

        std::array<std::reference_wrapper<motion::core::BodySectionControl const>, 3> getBodySections() const
        {
          return {{
                   std::ref<motion::core::BodySectionControl const>(d_headSection), 
                   std::ref<motion::core::BodySectionControl const>(d_armSection), 
                   std::ref<motion::core::BodySectionControl const>(d_legSection) 
                   }};
        }

        std::array<std::reference_wrapper<motion::core::BodySectionControl>, 3> getBodySections()
        {
          return {{
                   std::ref<motion::core::BodySectionControl>(d_headSection), 
                   std::ref<motion::core::BodySectionControl>(d_armSection), 
                   std::ref<motion::core::BodySectionControl>(d_legSection) 
                   }};
        }

        void clearModulation();
        void modulateJoint(JointId id, short delta);

      private:
        motion::core::HeadSectionControl d_headSection;
        motion::core::ArmSectionControl d_armSection;
        motion::core::LegSectionControl d_legSection;
      };

    }
  }
}
