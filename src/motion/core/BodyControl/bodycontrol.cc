// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bodycontrol.hh"

#include "MX28Snapshot/mx28snapshot.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"

using namespace bold::motion::core;
using namespace bold::state;
using namespace std;

BodyControl::BodyControl()
{}

void BodyControl::updateFromHardwareState(shared_ptr<HardwareState const> const& hw)
{
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    JointControl& joint = getJoint((JointId)jointId);

    // Set measured hardware position
    joint.setValue(hw->getMX28State(jointId).presentPositionValue);

    // Clear dirty flag. Value came from hardware, so no need to write it back again.
    joint.clearDirty();
  }
}

void BodyControl::clearModulation()
{
  d_headSection.clearModulation();
  d_armSection.clearModulation();
  d_legSection.clearModulation();
}
