// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/BodySectionId/bodysectionid.hh"
#include "util/assert.hh"

#include <string>

namespace bold
{
  namespace motion
  {
    namespace core
    {
      
      class MotionModule;

      /** Represents a desire to control a body section using a particular motion module.
       *
       * May also request to maintain control of the body section until some later
       * point, known as commital.
       *
       * This internal class is instantiated by the MotionTaskScheduler and is not
       * intended for direct use.
       */
      class MotionTask
      {
      public:
        /** The priority of a motion task.
         *
         * Used in arbitrating which motion module gains control of a body section.
         */
        enum class Priority
          {
           High   = 3,
           Normal = 2,
           Low    = 1,
           None   = 0
          };

        enum class Status
          {
           Pending,
           Selected,
           Completed,
           Ignored
          };

        static std::string getPriorityName(Priority priority);
        static std::string getStatusName(Status status);

        MotionTask(MotionModule* module, BodySectionId section, Priority priority, bool isCommitRequested)
          : d_module(module),
            d_section(section),
            d_priority(priority),
            d_isCommitRequested(isCommitRequested),
            d_isCommitted(false),
            d_status(Status::Pending)
        {}

        MotionModule* getModule() const { return d_module; }
        BodySectionId getSection() const { return d_section; }
        Priority getPriority() const { return d_priority; }
        Status getStatus() const { return d_status; }
        bool isCommitRequested() const { return d_isCommitRequested; }
        bool isCommitted() const { return d_isCommitted; }

        /// Called by the framework when a task that requests committal is started.
        void setCommitted()
        {
          ASSERT(d_isCommitRequested);
          d_isCommitted = true;
        }

        /// For testing purposes
        void clearCommitted() { d_isCommitted = false; }
        void setPriority(Priority priority) { d_priority = priority; }

        void setSelected()
        {
          ASSERT(d_status == Status::Pending  || d_status == Status::Selected);
          d_status = Status::Selected;
        }
      
        void setIgnored()
        {
          ASSERT(d_status == Status::Pending  || d_status == Status::Ignored);
          d_status = Status::Ignored;
        }
      
        void setCompleted()
        {
          ASSERT(d_status == Status::Selected || d_status == Status::Completed);
          d_status = Status::Completed;
        }

      private:
        MotionModule* d_module;
        BodySectionId d_section;
        Priority d_priority;
        bool d_isCommitRequested;
        bool d_isCommitted;
        Status d_status;
      };

      std::ostream& operator<<(std::ostream& stream, MotionTask::Priority priority);
      std::ostream& operator<<(std::ostream& stream, MotionTask::Status status);
    }
  }
}
