// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motiontask.hh"

using namespace std;
using namespace bold::motion::core;

string MotionTask::getPriorityName(Priority priority)
{
  switch (priority)
  {
    case Priority::High:   return "High";
    case Priority::Normal: return "Normal";
    case Priority::Low:    return "Low";
    case Priority::None:   return "None";
    default: return "Unknown";
  }
}

string MotionTask::getStatusName(Status status)
{
  switch (status)
  {
  case MotionTask::Status::Pending:   return "Pending";
  case MotionTask::Status::Selected:  return "Selected";
  case MotionTask::Status::Completed: return "Completed";
  case MotionTask::Status::Ignored:   return "Ignored";
  default: return "Unknown";
  }
}

std::ostream& bold::motion::core::operator<<(std::ostream& stream, MotionTask::Priority priority)
{
  return stream << MotionTask::getPriorityName(priority);
}

std::ostream& bold::motion::core::operator<<(std::ostream& stream, MotionTask::Status status)
{
  return stream << MotionTask::getStatusName(status);
}
