// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>
#include <map>
#include <mutex>

#include "motion/core/MotionRequest/motionrequest.hh"
#include "motion/core/MotionTask/motiontask.hh"

namespace bold
{
  namespace motion
  {
    namespace core
    {

      class MotionModule;

      /** Flag to indicate control over a certain body section is required
       *
       * 'No' means the module's requested task can still continue if
       * another module already is given control to the section.
       */
      enum class BodySectionRequired
        {
         No,
         Yes
        };

      /** Flag to indicate whether a task needs to be run until completion
       *
       * 'No' means a modules' task will only be run for a single
       * think loop tick
       */
      enum class RequestCommit
        {
         No,
         Yes
        };

      /** Schedules tasks that control the motion of the robot's body sections.
       *
       * Thread-safe.
       */
      class MotionTaskScheduler
      {
      public:
        static void sortTasks(std::vector<std::shared_ptr<MotionTask>>& tasks);

        MotionTaskScheduler();

        void registerModule(MotionModule* module) { d_modules.push_back(module); }

        /** Enqueues a request to control body sections.
         *
         * Requests are divided by body section and processed separately.
         *
         * Priorities are used when more than one request is made during a cycle.
         *
         * If the task has a commit request and is selected, it will be set
         * as committed until the corresponding MotionModule clears the committed flag.
         *
         * If a body section is marked as required, then it must be selected in
         * order for non-required sections to also be selected. For example, if
         * a 'stand up' motion is added, then the arms and legs would be required
         * as it makes no sense for only the head to be selected.
         */
        std::shared_ptr<MotionRequest const> request(MotionModule* module,
                                                     MotionTask::Priority headPriority, BodySectionRequired headRequired, RequestCommit headRequestCommit,
                                                     MotionTask::Priority armsPriority, BodySectionRequired armsRequired, RequestCommit armsRequestCommit,
                                                     MotionTask::Priority legsPriority, BodySectionRequired legsRequired, RequestCommit legsRequestCommit);

        /** Integrate any requests made since the last update, and check for completion of committed tasks.
         *
         * Called at the end of each think loop.
         *
         * Updates MotionTaskState.
         */
        void update();

        /** Gets the selected MotionTask for the head BodySection. May be nullptr. */
        std::shared_ptr<MotionTask> getHeadTask() const { return d_headTask; }
        /** Gets the selected MotionTask for the arm BodySection. May be nullptr. */
        std::shared_ptr<MotionTask> getArmTask()  const { return d_armTask; }
        /** Gets the selected MotionTask for the leg BodySection. May be nullptr. */
        std::shared_ptr<MotionTask> getLegTask()  const { return d_legTask; }

      private:
        std::vector<std::shared_ptr<MotionTask>> d_tasks;
        std::multimap<std::shared_ptr<MotionTask>, std::shared_ptr<MotionTask>> d_dependencies;
        std::shared_ptr<MotionTask> d_headTask;
        std::shared_ptr<MotionTask> d_armTask;
        std::shared_ptr<MotionTask> d_legTask;
        std::vector<MotionModule*> d_modules;
        std::mutex d_mutex;
        bool d_hasChange;
      };

    }
  }
}
