# Motion

## Framework

The motion system is set up to be able to handle multiple 'motion
modules' trying to perform some motion at the same time, and for
different body sections. For instance, to look around independently of
a walking or standing motion. A module can place a request to perform
a motion task, which will then be scheduled (or not). The following
combination of classes handles this.

### MotionModule

Anything that wants to control low level motion is a 'motion
module'. This control happens in 3 steps:

1. The module requests control from the task scheduler, flagging which
   body section(s) it wants to control
2. Every motion time step (every 8ms) that it is selected, its `step`
   function is called to do any full-body logic. This is also where to
   set the 'completed' flag if appropriate.
3. The `apply` functions for the body sections it gets to control are
   called to actually set joint angles

### MotionTaskScheduler

The scheduler receives requests, turns them into tasks and decides
which requests are chosen. When a request is made:

1. It creates a motion task for each body section and stores it. Each
   task gets a pointer to the module it belongs to.
2. If any section is flagged as being required, the tasks for other
   sections are linked as a dependency.
3. Returns a `MotionRequest` object to the module so it can track its
   state.

After any behaviour is run that could create motion requests, the
scheduler's `update` method is called. This:

1. Checks each module to see if it is finished and removes any
   previously committed tasks of that module, notifying the task it is
   completed.
2. Sorts all remaining taks by whether they are committed, then by
   priority
3. Tries to to select a task each for all body sections, by selecting
   the first committed task, the first task without dependencies, or
   the first with dependencies that aren't for body sections that
   already have a selected task.
4. Goes through the selected tasks (1 or none for each body section),
   and builds a map of modules to the joints they get to control.
5. Creates a motion task state object for the motion loop to pick up
   after which it will call `step` on the selected modules.

### MotionTask

A task is just a data class without real logic, that is used by the
task scheduler to maintain info about which module wants to move,
which body section it wants to move, and the state of that movement.

### MotionRequest

A request is just a data class wrapping possible motion tasks for each
body section.

## Motion Modules

To create a new type of motion, a new motion module is
implemented. This follows these steps:

1. Create a subclass of `MotionModule` 
2. Have the new class implement the `step` function that gets called
   at every update of the motion loop, and the `applyHead`,
   `applyArms` and `applyLegs` that give the final motor settings to
   apply.
3. Create some public function(s) that higher level behaviour can call
   to place a motion request.
4. Construct the motion module in the main `Agent` constructor.

The following motion modules currently exist.

### Motion Script Module

Motion scripts describe motions consisting of a deterministic series
of waypoints. The motion script module only takes care of the
scheduling of running them, these classes actually do the bulk of the
logic:

- **MotionScript** - Holds the waypoints and other information
  describing the motion, which is read from JSON.
- **MotionScriptRunner** - Wraps a motion script and does the actual
  calculations to determine target angles for every motion time step.

Behaviours (in `Option` classes) create instances of `MotionScript`,
which when run get passed to the motion script module, which wraps
them in a runner and schedules them.

The motion script module also creates configuration actions to run any
motion script in the `motionscripts` directory from the Round Table.

### Head Module

Can schedule actions to move the head to specific angles or by given
amounts.

### Stand Module

Performs a standing motion by placing the feet at specified offsets
and the hip at a given angle.

### Walk Module

Controls walking motion. It uses a walking engine to determine the
basic walking patterns, and a 'walk attitude' that determines
additinal leaning of the torso on top of these motions based on
walking velocity and acceleration..
