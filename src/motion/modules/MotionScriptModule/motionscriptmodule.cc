// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionscriptmodule.hh"

#include "motion/core/MotionRequest/motionrequest.hh"
#include "motion/core/JointSelection/jointselection.hh"
#include "motion/core/MotionTaskScheduler/motiontaskscheduler.hh"
#include "motion/scripts/MotionScriptLibrary/motionscriptlibrary.hh"
#include "roundtable/Action/action.hh"
#include "util/assert.hh"

#include "config/Config/config.hh"
#include "MotionScriptRunner/motionscriptrunner.hh"
#include "ThreadUtil/threadutil.hh"

#include <rapidjson/document.h>

using namespace bold;
using namespace bold::motion::core;
using namespace bold::motion::modules;
using namespace bold::motion::scripts;
using namespace bold::roundtable;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

void MotionScriptModule::createActions(MotionScriptLibrary& library, MotionScriptModule& module)
{
  auto libraryPtr = &library;
  auto modulePtr = &module;
  
  auto scripts = library.getAllScripts();

  // Sort scripts alphabetically by name
  sort(scripts.begin(), scripts.end(),
      [](shared_ptr<MotionScript> const& a, shared_ptr<MotionScript> const& b) -> bool
  {
    return a->getName() < b->getName();
  });

  auto actionId = [](MotionScript const& script) {
    stringstream id;
    id << "motion-script." << script.getName();
    return id.str();
  };

  // Add an action for each known motion script
  for (auto const& script : scripts)
    Action::addAction(actionId(*script), script->getName(), [modulePtr, script] { modulePtr->run(script); });

  // Add an action to play a script from JSON
  Action::addAction("motion-module.play-script-content", "Play Script", [modulePtr](Value* value)
  {
    auto script = MotionScript::fromJsonValue(*value);
    modulePtr->run(script);
  });

  // Add an action to reload all scripts
  Action::addAction("motion-module.reload-scripts", "Reload Scripts from disk", [libraryPtr, modulePtr, actionId] {
    libraryPtr->reloadAll();
    auto scripts = libraryPtr->getAllScripts();
    for (auto const& script : scripts)
    {
      auto id = actionId(*script);
      if (!Action::getAction(id))
        Action::addAction(id, script->getName(), [modulePtr, script] { modulePtr->run(script); });
    }
  });
  
  Log::info("MotionScriptModule::MotionScriptModule") << "Loaded " << scripts.size() << " motion scripts";
}

//////////////////////////////////////////////////////////////////////////////

MotionScriptModule::MotionScriptModule(shared_ptr<MotionTaskScheduler> scheduler)
  : MotionModule("motion-script", move(scheduler))
{}

bool MotionScriptModule::isRunning() const
{
  return d_runner && d_runner->getStatus() != MotionScriptRunner::Status::Finished
      && d_motionRequest && !d_motionRequest->hasCompleted();
}

void MotionScriptModule::step(JointSelection const& selectedJoints)
{
  ASSERT(ThreadUtil::isMotionLoopThread());

  if (!d_runner)
    return;

  if (d_runner->getStatus() == MotionScriptRunner::Status::Finished)
  {
    d_runner = nullptr;
    return;
  }

  // TODO clarify what runner completion means: should we apply the section or not?

  if (!d_runner->step(selectedJoints))
    setCompletedFlag();
}

void MotionScriptModule::applyHead(HeadSectionControl& head) { if (d_runner) d_runner->applyHead(head); }
void MotionScriptModule::applyArms(ArmSectionControl&  arms) { if (d_runner) d_runner->applyArms(arms); }
void MotionScriptModule::applyLegs(LegSectionControl&  legs) { if (d_runner) d_runner->applyLegs(legs); }

shared_ptr<MotionRequest const> MotionScriptModule::run(shared_ptr<MotionScript const> const& script)
{
  return run(make_shared<MotionScriptRunner>(script));
}

shared_ptr<MotionRequest const> MotionScriptModule::run(shared_ptr<MotionScriptRunner> const& scriptRunner)
{
  // TODO thread safety!

  ASSERT(ThreadUtil::isThinkLoopThread(false) || ThreadUtil::isDataStreamerThread());

  auto const& script = scriptRunner->getScript();

  Log::verbose("MotionScriptModule::run") << "Request to run script: " << script->getName();

  if (isRunning())
  {
    Log::warning("MotionScriptModule::run")
      << "Ignoring request to play script " << script->getName()
      << " -- already playing " << d_runner->getScript()->getName()
      << " with status " << MotionScriptRunner::getStatusName(d_runner->getStatus());

    // An empty MotionRequest has Ignored status
    return make_shared<MotionRequest const>();
  }

  d_runner = scriptRunner;

  // NOTE currently we assume that motion scripts control all body parts

  d_motionRequest = getScheduler()->request(
    this,
    MotionTask::Priority::Low,  BodySectionRequired::No,  scriptRunner->getScript()->getControlsHead() ? RequestCommit::Yes : RequestCommit::No,  // HEAD
    MotionTask::Priority::High, BodySectionRequired::Yes, scriptRunner->getScript()->getControlsArms() ? RequestCommit::Yes : RequestCommit::No,  // ARMS
    MotionTask::Priority::High, BodySectionRequired::Yes, scriptRunner->getScript()->getControlsLegs() ? RequestCommit::Yes : RequestCommit::No); // LEGS

  return d_motionRequest;
}
