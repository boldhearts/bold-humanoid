// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/MotionModule/motionmodule.hh"

#include <iosfwd>

namespace bold
{
  class MotionScriptRunner;

  namespace motion
  {
    namespace core
    {
      class MotionRequest;
    }

    namespace scripts
    {
      class MotionScript;
      class MotionScriptLibrary;
    }
    
    namespace modules
    {
      /** Motion module that runs motion scripts
       *
       * Uses MotionScriptRunner to perform actual script running logic
       */
      class MotionScriptModule : public core::MotionModule
      {
      public:
        /** Create config actions to run the scripts in the library
         *
         * @param path File path to load scripts from
         * @param module Module to use to run scripts when actions are fired
         */
        static void createActions(motion::scripts::MotionScriptLibrary& library, MotionScriptModule& module);

        MotionScriptModule(std::shared_ptr<motion::core::MotionTaskScheduler> scheduler);

        ~MotionScriptModule() override = default;

        void step(motion::core::JointSelection const& selectedJoints) override;

        void applyHead(core::HeadSectionControl& head) override;
        void applyArms(core::ArmSectionControl& arms) override;
        void applyLegs(core::LegSectionControl& legs) override;

        /** Attempts to execute the provided script runner.
         */
        std::shared_ptr<motion::core::MotionRequest const> run(std::shared_ptr<MotionScriptRunner> const& scriptRunner);

        /** Attempts to run the provided script.
         */
        std::shared_ptr<motion::core::MotionRequest const> run(std::shared_ptr<motion::scripts::MotionScript const> const& script);

        bool isRunning() const;

      private:
        MotionScriptModule(const MotionScriptModule&) = delete;
        MotionScriptModule& operator=(const MotionScriptModule&) = delete;

        std::shared_ptr<MotionScriptRunner> d_runner;
        std::shared_ptr<motion::core::MotionRequest const> d_motionRequest;
      };
    }
  }
}
