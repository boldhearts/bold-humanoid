// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "standmodule.hh"

#include "util/Log/log.hh"

#include "Kinematics/kinematics.hh"

#include "motion/core/BodySectionControl/bodysectioncontrol.hh"
#include "motion/core/MotionTaskScheduler/motiontaskscheduler.hh"



using namespace bold;
using namespace bold::motion::core;
using namespace bold::motion::modules;
using namespace bold::util;
using namespace std;
using namespace Eigen;

StandModule::StandModule(shared_ptr<MotionTaskScheduler> scheduler)
  : MotionModule("stand", move(scheduler)),
    d_offsets{0.0, 0.0, 0.0},
    d_hipPitch{0.0}
{
}

void StandModule::stand(Vector3d const& offsets, double hipPitch)
{
  d_offsets = offsets;
  d_hipPitch = hipPitch;

  getScheduler()->request(
    this,
    MotionTask::Priority::None, BodySectionRequired::No, RequestCommit::No,  // HEAD
    MotionTask::Priority::Normal, BodySectionRequired::No, RequestCommit::No,  // ARMS
    MotionTask::Priority::High, BodySectionRequired::Yes, RequestCommit::No); // LEGS
}

void StandModule::step(const JointSelection &selectedJoints)
{
}

void StandModule::applyArms(ArmSectionControl &arms)
{
  auto maybeTargetAngles = Kinematics::targetAnglesForStandingWithOffsets(d_offsets, d_hipPitch);
  for (auto targetAngles : maybeTargetAngles)
  {
    arms.shoulderPitchRight().setRadians(targetAngles[JointId::R_SHOULDER_PITCH]);
    arms.shoulderPitchLeft().setRadians(targetAngles[JointId::L_SHOULDER_PITCH]);
    
    arms.shoulderRollRight().setRadians(targetAngles[JointId::R_SHOULDER_ROLL]);
    arms.shoulderRollLeft().setRadians(targetAngles[JointId::L_SHOULDER_ROLL]);
    
    arms.elbowRight().setRadians(targetAngles[JointId::R_ELBOW]);
    arms.elbowLeft().setRadians(targetAngles[JointId::L_ELBOW]);
  }
}

void StandModule::applyLegs(LegSectionControl &legs)
{
  auto maybeTargetAngles = Kinematics::targetAnglesForStandingWithOffsets(d_offsets, d_hipPitch);
  for (auto targetAngles : maybeTargetAngles)
  {
    legs.hipYawLeft().setRadians(targetAngles[JointId::L_HIP_YAW]);
    legs.hipRollLeft().setRadians(targetAngles[JointId::L_HIP_ROLL]);
    legs.hipPitchLeft().setRadians(targetAngles[JointId::L_HIP_PITCH]);
    legs.kneeLeft().setRadians(targetAngles[JointId::L_KNEE]);
    legs.anklePitchLeft().setRadians(targetAngles[JointId::L_ANKLE_PITCH]);
    legs.ankleRollLeft().setRadians(targetAngles[JointId::L_ANKLE_ROLL]);

    legs.hipYawRight().setRadians(targetAngles[JointId::R_HIP_YAW]);
    legs.hipRollRight().setRadians(targetAngles[JointId::R_HIP_ROLL]);
    legs.hipPitchRight().setRadians(targetAngles[JointId::R_HIP_PITCH]);
    legs.kneeRight().setRadians(targetAngles[JointId::R_KNEE]);
    legs.anklePitchRight().setRadians(targetAngles[JointId::R_ANKLE_PITCH]);
    legs.ankleRollRight().setRadians(targetAngles[JointId::R_ANKLE_ROLL]);
  }
}

void StandModule::applyHead(HeadSectionControl &head)
{
  Log::error("StandModule::applyHead") << "SHOULD NOT BE CALLED";
}

