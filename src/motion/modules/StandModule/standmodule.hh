// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/MotionModule/motionmodule.hh"
#include <Eigen/Core>

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class StandModule : public core::MotionModule
      {
      public:
        StandModule(std::shared_ptr<motion::core::MotionTaskScheduler> scheduler);
        
        void stand(Eigen::Vector3d const& offsets, double hipPitch);
        
        void step(motion::core::JointSelection const& selectedJoints) override;
        
        void applyHead(core::HeadSectionControl& head) override;
        void applyArms(core::ArmSectionControl& arms) override;
        void applyLegs(core::LegSectionControl& legs) override;    
        
      private:
        Eigen::Vector3d d_offsets;
        double d_hipPitch;
      };
    }
  }
}
