// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/MotionModule/motionmodule.hh"
#include "config/SettingBase/Setting/setting.hh"
#include "Math/math.hh"
#include "util/Range/range.hh"

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class ArmSectionControl;
      class HeadSectionControl;
      class LegSectionntrol;
    }

    namespace modules
    {
      class HeadModule : public core::MotionModule
      {
      public:
        HeadModule(std::shared_ptr<motion::core::MotionTaskScheduler> scheduler);
        ~HeadModule() override = default;

        HeadModule(const HeadModule&) = delete;
        HeadModule& operator=(const HeadModule&) = delete;

        void step(motion::core::JointSelection const& selectedJoints) override;

        void applyHead(core::HeadSectionControl& head) override;
        void applyArms(core::ArmSectionControl& arms) override;
        void applyLegs(core::LegSectionControl& legs) override;

        // TODO don't store this here, but rather some static model of the body's limits

        double getTopLimitDegs() const    { return d_limitTiltDegs->getValue().max(); }
        double getBottomLimitDegs() const { return d_limitTiltDegs->getValue().min(); }
        double getRightLimitDegs() const  { return d_limitPanDegs->getValue().min(); }
        double getLeftLimitDegs() const   { return d_limitPanDegs->getValue().max(); }

        double getTopLimitRads() const    { return Math::degToRad(getTopLimitDegs()); }
        double getBottomLimitRads() const { return Math::degToRad(getBottomLimitDegs()); }
        double getRightLimitRads() const  { return Math::degToRad(getRightLimitDegs()); }
        double getLeftLimitRads() const   { return Math::degToRad(getLeftLimitDegs()); }

        /// Move the head to the position set as the 'home' position
        void moveToHome();

        /** Move to the absolute angular position specified.
         *
         * @param panDegs zero center, positive left, negative right
         * @param tiltDegs zero center, positive up, negative down
         */
        void moveToDegs(double panDegs, double tiltDegs);

        /** Move the head by the delta specified.
         *
         * @param panDegsDelta positive left, negative right
         * @param tiltDegsDelta positive up, negative down
         */
        void moveByDeltaDegs(double panDegsDelta, double tiltDegsDelta);

        /// Reset motion tracking state to zero
        void initTracking();

        /// Use the provided error as the input into a PD controller
        void moveTracking(double panError, double tiltError);

      private:
        void scheduleMotion();

        config::Setting<util::Range<double>>* d_limitPanDegs;
        config::Setting<util::Range<double>>* d_limitTiltDegs;

        config::Setting<double>* d_panHomeDegs;
        config::Setting<double>* d_tiltHomeDegs;

        /// P gain value set on the MX28
        config::Setting<int>* d_gainP;

        config::Setting<double>* d_panGainP;  ///< P gain value for pan joint used in tracking calculations
        config::Setting<double>* d_panGainI;  ///< I gain value for pan joint used in tracking calculations
        config::Setting<double>* d_panILeak;  ///< I leak value for pan joint used in tracking calculations
        config::Setting<double>* d_panGainD;  ///< D gain value for pan joint used in tracking calculations
        config::Setting<double>* d_tiltGainP; ///< P gain value for tilt joint used in tracking calculations
        config::Setting<double>* d_tiltGainI; ///< I gain value for tilt joint used in tracking calculations
        config::Setting<double>* d_tiltILeak; ///< I leak value for pan joint used in tracking calculations
        config::Setting<double>* d_tiltGainD; ///< D gain value for tilt joint used in tracking calculations

        double d_lastPanError;
        double d_lastTiltError;

        double d_integratedPanError;
        double d_integratedTiltError;

        double d_targetPanAngleDegs;
        double d_targetTiltAngleDegs;
      };
    }
  }
}
