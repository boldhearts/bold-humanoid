// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <memory>

#include "Smoother/LinearSmoother/linearsmoother.hh"
#include "WalkAttitude/walkattitude.hh"

#include "motion/core/MotionModule/motionmodule.hh"

namespace bold
{
  class Balance;
  class RobotisWalkEngine;

  namespace config
  {
    template<typename> class Setting;
  }

  namespace motion
  {
    namespace modules
    {

      enum class WalkStatus : uint8_t
        {
         Stopped = 0,
         Starting = 1,
         Walking = 2,
         Stabilising = 3
        };

      std::string getWalkStatusName(WalkStatus status);

      class WalkModule : public core::MotionModule
      {
      public:
        WalkModule(std::shared_ptr<motion::core::MotionTaskScheduler> scheduler, uint8_t timeUnitMs);
        ~WalkModule() override = default;

        void step(motion::core::JointSelection const& selectedJoints) override;

        void applyHead(core::HeadSectionControl& head) override;
        void applyArms(core::ArmSectionControl& arms) override;
        void applyLegs(core::LegSectionControl& legs) override;

        /** Slow walking to a stop and stabilise.
         */
        void stop();

        /** Stop walking abruptly. Likely unstable.
         *
         * Call stop instead to have walking stop smoothly.
         */
        void stopImmediately();

        bool isRunning() const { return !d_immediateStopRequested && d_status != WalkStatus::Stopped; }

        WalkStatus getStatus() const { return d_status; }

        std::shared_ptr<RobotisWalkEngine> getWalkEngine(){ return d_walkEngine; }

        /**
         * Set the direction of motion, where positive X is in the forwards
         * direction, and positive Y is to the left. The length of the vector
         * determines the velocity of motion (unspecified units).
         *
         * Note that the same value will result in different speeds in the X and Y
         * axes.
         */
        void setMoveDir(double x, double y);

        void setMoveDir(Eigen::Vector2d const& dir)
        {
          setMoveDir(dir.x(), dir.y());
        }

        /**
         * Set the rate of turning, where positive values turn right (clockwise)
         * and negative values turn left (counter-clockwise) (unspecified units).
         */
        void setTurnAngle(double turnSpeed);

      private:
        WalkModule(const WalkModule&) = delete;
        WalkModule& operator=(const WalkModule&) = delete;

        void start();

        uint8_t d_timeUnitMs;
        
        std::shared_ptr<RobotisWalkEngine> d_walkEngine;
        std::shared_ptr<Balance> d_balance;

        config::Setting<int>* d_stabilisationTimeMillis;
        int d_stabilisationCycleCount;
        int d_stabilisationCyclesRemaining;

        LinearSmoother d_xAmpSmoother;
        LinearSmoother d_yAmpSmoother;
        LinearSmoother d_turnAmpSmoother;
        LinearSmoother d_hipPitchSmoother;
        WalkAttitude d_attitude;
        config::Setting<bool>* d_isParalysed;

        bool d_turnAngleSet;
        bool d_moveDirSet;
        bool d_immediateStopRequested;
        WalkStatus d_status;
      };
    }
  }
}
