// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "PixelFilterChain/pixelfilterchain.hh"
#include "config/SettingBase/Setting/setting.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"
#include "config/Config/config.hh"
#include "util/json.hh"
#include "vision/PixelLabel/pixellabel.hh"
#include "vision/PixelLabel/RangePixelLabel/rangepixellabel.hh"

#include <vector>
#include <string>
#include <memory>

namespace bold
{
  namespace vision
  {
    /** Trains labels based on sample pixels
     *
     * Trains labels by taking an image, floodfilling an area from a
     * given seedpoint, and treating all pixels in that area as
     * examples of the class to train.
     */
    class LabelTeacher
    {
    public:
      /** Method to use to detect and filter out outliers
       */
      enum class OutlierDetection
      {
        None = 0,        ///< Keep all samples
        XSigmas = 1      ///< All samples further than X sigmas from mean are outliers
      };

      /** Method to use to train labels given a new set of examples
       */
      enum class TrainMode
      {
        Replace = 0,     ///< Clear label and train only on new examples
        Extend = 1       ///< Extend labels given new examples
      };

      LabelTeacher(std::vector<std::shared_ptr<PixelLabel>> labels);

      /** Get trained labels
       */
      std::vector<std::shared_ptr<PixelLabel>> getLabels() const;

      /** Train label from selected examples
       *
       * Uses the current snapshot image.
       *
       * @param labelIdx Index of label to train, in vector of labels supplied to constructor
       * @param mask Mask selecting which pixels to use as examples
       */
      void train(unsigned labelIdx, cv::Mat const& mask);

      /** Label current snapshot according to given label
       */
      cv::Mat label(unsigned labelIdx) const;

      /** Whether a new snapshot is requested
       */
      bool requestedSnapShot() const { return d_snapshotRequested; }

      /** Whether a new labelled image is requested
       */
      bool requestedLabel() const {return d_labelRequested; }

      /** Set new snapshot image to train on
       */
      void setYUVTrainImage(cv::Mat yuvTrainImg);

      /** Get snapshot image, YUV format
       */
      cv::Mat getYUVTrainImage() const { return d_yuvTrainImage; }

      /** Get snapshot image, BGR format
       */
      cv::Mat getBGRTrainImage() const;

      /** Get current mask that would be used for training
       */
      cv::Mat getMask() const { return d_mask; }

      /** Set new seed point for next flood fill
       */
      void setSeedPoint(Eigen::Vector2i point);

      /** Perform floodfill from current seed point
       */
      cv::Mat floodFill() const;

      /** Get which label would be trained next
       */
      unsigned getLabelToTrain() const { return d_labelToTrain; }

    private:
      void updateState(cv::Mat const& mask) const;
      std::vector<colour::HSV> getSamples(cv::Mat const& mask) const;

      static colour::HSVRange determineRange(std::vector<colour::HSV> const& samples);
      static std::pair<colour::HSV, colour::HSV> determineDistribution(std::vector<colour::HSV> const& samples);

      config::Setting<OutlierDetection>* d_outlierDetection;
      config::Setting<TrainMode>* d_trainMode;

      std::vector<std::shared_ptr<PixelLabel>> d_labels;
      cv::Mat d_yuvTrainImage;
      cv::Mat d_mask;

      Eigen::Vector2i d_seedPoint;
      float d_maxFloodDiff;
      unsigned d_labelToTrain;
      float d_sigmaRange;

      bool d_snapshotRequested;
      bool d_labelRequested;
      bool d_fixedRange;

    };
  }
}
