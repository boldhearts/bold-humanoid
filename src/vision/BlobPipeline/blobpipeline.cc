// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "blobpipeline.hh"

#include <numeric>

using namespace bold;
using namespace bold::vision;
using namespace std;

Blob::Vector BlobPipeline::run(Blob::Vector const& blobs, cv::Mat const& img)
{
  return accumulate(d_steps.begin(), d_steps.end(), blobs,
                    [](Blob::Vector const& blobs, function<Blob::Vector(Blob::Vector const&)> const& fun)
                    {
                      return fun(blobs);
                    });
}

BlobPipeline BlobPipeline::filter(function<bool(Blob const&)> filterFun)
{
  auto newPipeline = *this;
  
  newPipeline.d_steps.push_back([filterFun](Blob::Vector const& blobs) {
      auto filteredBlobs = Blob::Vector{blobs.size()};
      auto newEnd = copy_if(blobs.begin(), blobs.end(), filteredBlobs.begin(), filterFun);
      filteredBlobs.erase(newEnd, filteredBlobs.end());
      return filteredBlobs;
    });

  return newPipeline;
}

BlobPipeline BlobPipeline::map(function<Blob(Blob const&)> mapFun)
{
  auto newPipeline = *this;
  
  newPipeline.d_steps.push_back([mapFun](Blob::Vector const& blobs) {
      auto mappedBlobs = Blob::Vector{blobs.size()};
      transform(blobs.begin(), blobs.end(), mappedBlobs.begin(), mapFun);
      return mappedBlobs;
    });

  return newPipeline;
  
}

BlobPipeline BlobPipeline::flatMap(function<Blob::Vector(Blob const&)> mapFun)
{
  auto newPipeline = *this;
  
  newPipeline.d_steps.push_back([mapFun](Blob::Vector const& blobs) {
      auto mappedBlobs = vector<Blob::Vector>{blobs.size()};
      transform(blobs.begin(), blobs.end(), mappedBlobs.begin(), mapFun);
      auto flatMappedBlobs = Blob::Vector{};
      for (auto bvec : mappedBlobs)
        flatMappedBlobs.insert(flatMappedBlobs.end(), bvec.begin(), bvec.end());
      return flatMappedBlobs;
    });

  return newPipeline;
}

BlobPipeline BlobPipeline::foldLeft(function<Blob::Vector(Blob::Vector const&, Blob const&)> foldFun)
{
  auto newPipeline = *this;

  newPipeline.d_steps.push_back([foldFun](Blob::Vector const& blobs) {
      return accumulate(blobs.begin(), blobs.end(), Blob::Vector{},
                        foldFun);
    });

  return newPipeline;
}
