// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/Blob/blob.hh"

#include <opencv/cv.h>

namespace bold
{
  namespace vision
  {
    class BlobPipeline
    {
    public:

      /** Process a set of Blobs
       *
       * @param blobs Blobs to run pipeline on
       * @param img Image from which blobs were extracted
       * @return Processed blobs
       */
      Blob::Vector run(Blob::Vector const& blobs, cv::Mat const& img);

      /** Append a filter
       */
      BlobPipeline filter(std::function<bool(Blob const&)> filterFun);

      /** Append a mapping function
       */
      BlobPipeline map(std::function<Blob(Blob const&)> mapFun);

      /** Append a flat-map function
       */
      BlobPipeline flatMap(std::function<Blob::Vector(Blob const&)> mapFun);

      /** Append a fold function
       */
      BlobPipeline foldLeft(std::function<Blob::Vector(Blob::Vector const&, Blob const&)> foldFun);
          
    private:
      std::vector<std::function<Blob::Vector(Blob::Vector const&)>> d_steps;
    };
  }
}
