// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "config/Config/config.hh"
#include "Math/math.hh"

namespace bold {
  namespace vision {

    class CameraModel {
    public:
      /// Initialises a CameraModel using parameters defined in configuration.
      CameraModel()
          : CameraModel{uint16_t(config::Config::getStaticValue<int>("camera.image-width")),
                        uint16_t(config::Config::getStaticValue<int>("camera.image-height")),
                        config::Config::getStaticValue<double>("camera.field-of-view.vertical-degrees"),
                        config::Config::getStaticValue<double>("camera.field-of-view.horizontal-degrees")} {}

      /// Initialises a CameraModel using the specified parameters.
      CameraModel(uint16_t imageWidth, uint16_t imageHeight, double rangeVerticalDegs, double rangeHorizontalDegs);

      uint16_t imageWidth() const { return d_imageWidth; }

      uint16_t imageHeight() const { return d_imageHeight; }

      double focalLength() const { return d_focalLength; }

      double rangeVerticalDegs() const { return d_rangeVerticalDegs; }

      double rangeVerticalRads() const { return Math::degToRad(d_rangeVerticalDegs); }

      double rangeHorizontalDegs() const { return d_rangeHorizontalDegs; }

      double rangeHorizontalRads() const { return Math::degToRad(d_rangeHorizontalDegs); }

      /** Gets the direction, in camera coordinates, of the specified pixel.
       * Returns a unit vector.
       */
      Eigen::Vector3d directionForPixel(Eigen::Vector2d const &pixel) const;

      util::Maybe<Eigen::Vector2d> pixelForDirection(Eigen::Vector3d const &direction) const;

      /** Gets a projection matrix
       *
       * This matrix projects from camera frame onto image frame, up to
       * a scaling factor, which is given in the z element of the
       * transformed vector. i.e to get the pixel coordinate p of a
       * point v with projection matrix T: p' = Tv, p = p'/p'_z.
       */
      Eigen::Affine3d getImageCameraTransform() const { return d_projectionTransform; }

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    private:
      uint16_t d_imageWidth;
      uint16_t d_imageHeight;
      double d_focalLength;
      double d_rangeVerticalDegs;
      double d_rangeHorizontalDegs;
      Eigen::Affine3d d_projectionTransform;
    };

  }
}
