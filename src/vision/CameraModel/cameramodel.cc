// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cameramodel.hh"

using namespace bold;
using namespace bold::util;
using namespace bold::vision;
using namespace std;
using namespace Eigen;

/// Initialises a CameraModel using the specified parameters.
CameraModel::CameraModel(uint16_t imageWidth, uint16_t imageHeight, double rangeVerticalDegs, double rangeHorizontalDegs)
  : d_imageWidth(imageWidth),
    d_imageHeight(imageHeight),
    d_rangeVerticalDegs(rangeVerticalDegs),
    d_rangeHorizontalDegs(rangeHorizontalDegs)
{
  //
  // Compute the focal length
  //
  d_focalLength = 1.0 / tan(.5 * rangeHorizontalRads());

  //
  // Compute the projection transform
  //

  // Perspective transform
  Affine3d c;
  c.matrix() <<
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 1, 0;

  double ws = d_imageWidth / 2.0;
  double hs = d_imageHeight / 2.0;
  // c maps vof to x/y in [-1,1]; scale to pixel measurements and transform to have origin in corner pixel
  auto s =
    //Translation3d(d_imageWidth / 2.0, d_imageHeight / 2.0, 0) *
    Scaling(ws / tan(.5 * rangeHorizontalRads()),
            hs / tan(.5 * rangeVerticalRads()),
            1.0);

  // agent frame: x right, y forward, z up
  // camera frame: x left, y up, z forward (remember: camera is upside down)
  Affine3d t;
  t.matrix() <<
    -1, 0, 0, 0,
    0, 0, 1, 0,
    0, 1, 0, 0,
    0, 0, 0, 1;

  // Full transform from agent frame to image
  d_projectionTransform = s * c * t;
}

Vector3d CameraModel::directionForPixel(Vector2d const& pixel) const
{
  double r = focalLength() * tan(rangeVerticalRads() / 2);

  Vector3d p(
      (-2.0 * pixel.x() / d_imageWidth) + 1.0,
      focalLength(),
      ((2.0 * pixel.y() / d_imageHeight) - 1.0) * r
  );

  Vector3d dir = p / focalLength();

  return dir.normalized();
}

Maybe<Vector2d> CameraModel::pixelForDirection(Vector3d const& direction) const
{
  if (direction.y() < std::numeric_limits<double>::epsilon())
    return util::Maybe<Vector2d>::empty();

  // TODO: why doesn't it work when using p = T * d and then divide by p(2) instead?
  Vector4d hDirection{0,0,0,1};
  hDirection.head<3>() = direction;

  auto pixel = Vector4d{d_projectionTransform * hDirection};
  return util::Maybe<Vector2d>(pixel.head<2>() / pixel(2) + Vector2d{d_imageWidth / 2.0, d_imageHeight / 2.0});
}
