// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <opencv2/opencv.hpp>
#include "goalpostdetector.hh"

#include "vision/FieldEdgeTracer/fieldedgetracer.hh"
#include "vision/ImageLabeller/PixelLabeller/pixellabeller.hh"
#include "Spatialiser/spatialiser.hh"
#include "config/Config/config.hh"
#include "util/Log/log.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::util;
using namespace bold::vision;

GoalpostDetector::GoalpostDetector() {
  d_maxPostFieldGapPixels = Config::getSetting<int>("vision.goal-post-detector.max-post-field-gap-pixels");

}

GoalpostDetector::GoalpostBaseVector GoalpostDetector::detect(cv::Mat const &image,
                                                              PixelLabeller const &labeller,
                                                              FieldEdgeTracer const &tracer,
                                                              Spatialiser const &spatialiser) const {
  auto goalPositions = GoalpostBaseVector{};

  auto const& fieldEdgeRuns = tracer.getRuns();

  auto down = spatialiser.getCameraDown();

  for (auto const &run : fieldEdgeRuns) {
    if (!run.label || run.label->getClass() != LabelClass::GOAL)
      continue;

    Point2i middle = run.start + (run.end - run.start) / 2;

    // Search down for field
    Point2i downEnd = middle + (1000 * down).cast<int>();

    auto downIterator = cv::LineIterator(
      image,
      cv::Point(middle.x(), middle.y()), cv::Point(downEnd.x(), downEnd.y()));

    auto nNonGoal = 0;
    auto maxNonGoal = d_maxPostFieldGapPixels->getValue();
    for (auto i = 0; i < downIterator.count; ++i, ++downIterator) {
      auto *pixel = *downIterator;
      auto label = labeller.labelPixel(colour::YCbCr(pixel[0], pixel[1], pixel[2]));
      if (!label || label->getClass() != LabelClass::GOAL)
        ++nNonGoal;

      if (label && label->getClass() == LabelClass::FIELD)
        goalPositions.emplace_back(downIterator.pos().x, downIterator.pos().y);

      if (nNonGoal > maxNonGoal)
        break;
    }
  }

  return goalPositions;

}
