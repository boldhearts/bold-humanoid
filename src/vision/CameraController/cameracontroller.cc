// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cameracontroller.hh"

#include "vision/Camera/camera.hh"
#include "config/Config/config.hh"
#include "config/SettingReader/settingreader.hh"

#include "roundtable/Action/action.hh"
#include "util/Log/log.hh"

using namespace bold::config;
using namespace bold::roundtable;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

CameraController::CameraController(Camera& camera)
  : d_camera{&camera}
{}

void CameraController::createSettings() const
{
  //
  // Create Setting<T> objects for the camera controls
  //

  // NOTE unlike settings defined in configuration-metadata.json, these are not
  // necessarily known before the process starts, and will vary based upon the
  // available hardware.

  set<string> ignoreControlNames = { "Pan (Absolute)", "Tilt (Absolute)" };

  vector<SettingBase*> settings;
  vector<function<void()>> refreshAllActions;

  for (shared_ptr<Camera::Control const> const& control : d_camera->getControls())
  {
    string name = control->name;
    Camera::ControlType type = control->type;
    const bool isReadOnly = false;

    // Remove some we cannot use
    if (ignoreControlNames.find(name) != ignoreControlNames.end())
      continue;

    // Shorten some verbose names
    if (name == "White Balance Temperature, Auto")
      name = "Auto WB";
    else if (name == "White Balance Temperature")
      name = "WB Temp (K)";

    // Reinterpret some two-valued INT controls as BOOL
    if (type == Camera::ControlType::INT && control->minimum == 0 && control->maximum == 1)
      type = Camera::ControlType::BOOL;

    // Create a path from this control's name. This will be used in the config file.
    stringstream path;
    path << "camera.settings.";
    for (char const& c : name)
    {
      if (c == '(' || c == ')' || c ==',')
        continue;
      else if (c == ' ')
        path << '-';
      else
        path << (char)tolower(c);
    }

    Log::info("CameraController::createSettings") << "New setting path: " << path.str();
    
    int currentValue = control->getValue();

    rapidjson::Value const* initialJsonValue = Config::getConfigJsonValue(path.str());

    auto reader = SettingReader{};

    // Create the appropriate type of Setting<T>
    switch (type)
    {
    case Camera::ControlType::BOOL:
      {
        auto setting = new BoolSetting(path.str(), isReadOnly, name);

        if (initialJsonValue == nullptr || !reader.setValueFromJson(*setting, *initialJsonValue))
          setting->setValue(currentValue != 0);

        setting->changed.connect([control, setting](bool value) {
          control->setValue(value ? 1 : 0,
                            [setting](int correction) {
                              setting->setValue(correction != 0);
                            });
        });
        
        settings.push_back(setting);

        refreshAllActions.emplace_back([setting, control] {
          setting->setValue(control->getValue() != 0);
        });
        break;
      }
      
    case Camera::ControlType::INT:
      {
        auto setting = new IntSetting(path.str(), control->minimum, control->maximum, isReadOnly, name);

        if (initialJsonValue == nullptr || !reader.setValueFromJson(*setting, *initialJsonValue))
          setting->setValue(currentValue);

        setting->changed.connect([control, setting](int value) {
          control->setValue(value,
                            [setting](int correction) {
                              setting->setValue(correction);
                            });
        });
        
        settings.push_back(setting);
        
        refreshAllActions.emplace_back([setting, control] {
          setting->setValue(control->getValue());
        });
        break;
      }
      
    case Camera::ControlType::MENU:
      {
        auto setting = new EnumSetting(path.str(), control->menuItems, isReadOnly, name);

        if (initialJsonValue == nullptr || !reader.setValueFromJson(*setting, *initialJsonValue))
          setting->setValue(currentValue);

        setting->changed.connect([control, setting](int value) {
          control->setValue(value,
                            [setting](int correction) {
                              setting->setValue(correction);
                            });
        });
        
        settings.push_back(setting);
        
        refreshAllActions.emplace_back([setting,control] {
          setting->setValue(control->getValue());
        });
        break;
      }
      default:
      {
        Log::error("CameraController::createSettings") << "Unsupported camera control type: " << (int)type;
      }
    }
  }

  Action::addAction("camera.refresh-all-control-values", "Refresh all", [refreshAllActions]
  {
    for (auto& fun : refreshAllActions)
      fun();
  });

  // Camera settings must be set in a particular order. For example, you cannot
  // explicitly set the white balance when 'auto wb' mode is on. The value will
  // not stick.
  //
  // Finally, any setting which were not referenced in the config file will be
  // logged out, and appended in whatever order they were reported by the camera
  // in.

  Setting<vector<string>>* controlOrder = Config::getSetting<vector<string>>("camera.settings.initialise-order");

  for (string const& name : controlOrder->getValue())
  {
    stringstream pathstream;
    pathstream << "camera.settings" << '.' << name;
    string path = pathstream.str();

    auto match = find_if(settings.begin(), settings.end(), [path](SettingBase* setting) { return setting->getPath() == path; });

    if (match == settings.end())
    {
      Log::warning("CameraController::createSettings") << "Configuration document specifies '" << path << "' but no setting exists with that name";
      continue;
    }

    SettingBase* setting = *match;

    // This is not very efficient (O(N)) but this only occurs at startup, so...
    settings.erase(match);

    Config::addSetting(setting);

    setting->triggerChanged();
  }

  for (SettingBase* leftOver : settings)
  {
    Log::warning("CameraController::createSettings") << "Configuration document doesn't specify a value for camera setting: " << leftOver->getPath();
    Config::addSetting(leftOver);
    leftOver->triggerChanged();
  }
}
