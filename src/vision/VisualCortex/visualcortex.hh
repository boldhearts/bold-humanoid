// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <functional>
#include <map>
#include <memory>
#include <opencv2/core/core.hpp>

#include "config/SettingBase/Setting/setting.hh"
#include "geometry2/Geometry/LineSegment/linesegment.hh"
#include "util/meta.hh"
#include "vision/Blob/blob.hh"
#include "vision/FieldEdgeTracer/fieldedgetracer.hh"
#include "vision/GoalpostDetector/goalpostdetector.hh"
#include "vision/LabelTeacher/labelteacher.hh"
#include "vision/PixelLabel/HistogramPixelLabel/histogrampixellabel.hh"
#include "vision/PixelLabel/RangePixelLabel/rangepixellabel.hh"

namespace bold
{
  class Clock;
  class FieldEdge;
  class FieldMap;
  class LineFinder;
  class Spatialiser;

  namespace roundtable
  {
    class CameraProtocol;
    class RoundTableServer;
  }

  namespace util {
      class SequentialTimer;
  }
  
  namespace vision {

    class ImageLabeller;
    class PixelLabeller;

    class BlobDetector;
    class CameraModel;
    class CartoonPainter;
    class CompleteFieldEdgeFinder;
    class FieldEdgeFinder;
    class FieldHistogramBuilder;
    class LabelledImageProcessor;
    class LabelledImageProcessRunner;
    class LabelCounter;
    class LineDotter;
    class PeriodicFieldEdgeFinder;

    enum class ImageType
    {
      None = 0,
      YCbCr = 1,
      RGB = 2,
      Cartoon = 3,
      Teacher = 4
    };

    enum class ImageGranularity
    {
      All = 0,
      Half = 1,
      Third = 2,
      Gradient = 3,
      Projected = 4
    };

    enum class FieldEdgeType
    {
      Complete = 0,
      Periodic = 1
    };

    /** Bold-humanoid's vision processing subsystem.
     *
     **/
    class VisualCortex {
    public:
      static bool shouldMergeBallBlobs(vision::Blob const &larger,
                                       vision::Blob const &smaller);

      VisualCortex(std::shared_ptr<vision::CameraModel> cameraModel,
                   std::shared_ptr<Spatialiser> spatialiser,
                   std::shared_ptr<Clock> clock,
                   roundtable::RoundTableServer &roundTableServer);

      /** Process the provided image, extracting features. */
      void integrateImage(cv::Mat &cameraImage, util::SequentialTimer &timer, ulong thinkCycleNumber);

      /** Saves the provided image to a file, along with information about the current agent's state in a JSON file. */
      void saveImage(cv::Mat const &image, std::map<uint8_t, colour::BGR> *palette);

      /** Composes and enqueues a debugging image. */
      void streamDebugImage(cv::Mat &cameraImage, util::SequentialTimer &timer);

    private:

      util::Maybe<Eigen::Vector2d> detectBall(vision::Blob::Vector &ballBlobs, cv::Mat const &image,
                                              vision::PixelLabeller const &labeller, util::SequentialTimer &t);

      std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d>>
        detectPlayers(vision::Blob::Vector &playerBlobs, cv::Mat const &image, util::SequentialTimer &t);

      std::shared_ptr<vision::CameraModel> d_cameraModel;
      std::shared_ptr<Spatialiser> d_spatialiser;
      std::shared_ptr<Clock> d_clock;

      std::shared_ptr<roundtable::CameraProtocol> d_cameraProtocol;

      std::vector<std::shared_ptr<vision::PixelLabel>> d_pixelLabels;

      std::shared_ptr<vision::ImageLabeller> d_imageLabeller;
      std::shared_ptr<vision::PixelLabeller> d_pixelLabeller;

      std::unique_ptr<vision::LabelTeacher> d_labelTeacher;

      std::function<Eigen::Matrix<uint8_t, 2, 1>(int)> d_granularityFunction;

      std::shared_ptr<LineFinder> d_lineFinder;

      std::shared_ptr<vision::LabelledImageProcessRunner> d_imagePassRunner;

      std::shared_ptr<vision::FieldEdgeFinder> d_fieldEdgeFinder;
      std::shared_ptr<vision::FieldHistogramBuilder> d_fieldHistogramBuilder;
      std::shared_ptr<vision::CartoonPainter> d_cartoonPainter;
      std::shared_ptr<vision::BlobDetector> d_blobDetector;
      std::shared_ptr<vision::LineDotter> d_lineDotter;

      std::unique_ptr<vision::FieldEdgeTracer> d_fieldEdgeTracer;

      vision::GoalpostDetector d_goalpostDetector;

      config::Setting<bool> *d_shouldFlipImage;
      config::Setting<bool> *d_shouldDetectLines;
      config::Setting<bool> *d_shouldCountLabels;
      config::Setting<bool> *d_shouldDetectBlobs;
      config::Setting<bool> *d_shouldIgnoreAboveHorizon;
      config::Setting<int> *d_maxBallFieldEdgeDistPixels;

      config::Setting<int> *d_minBallAreaPixels;
      config::Setting<util::Range<double>> *d_acceptedBallMeasuredSizeRatio;

      config::Setting<int> *d_minGoalDimensionPixels;
      config::Setting<int> *d_maxGoalFieldEdgeDistPixels;
      config::Setting<util::Range<double>> *d_acceptedGoalMeasuredWidthRatio;

      config::Setting<int> *d_minPlayerAreaPixels;
      config::Setting<int> *d_minPlayerLengthPixels;
      config::Setting<double> *d_goalieMarkerHeight;

      config::Setting<double> *d_fieldHistogramThreshold;

      bool d_saveNextYUVFrame;
      config::Setting<bool> *d_isRecordingYUVFrames;
      bool d_saveNextDebugFrame;

      config::Setting<ImageType> *d_imageType;
      config::Setting<int> *d_streamFramePeriod;

      config::Setting<colour::BGR> *d_lineDotColour;
      config::Setting<colour::BGR> *d_observedLineColour;
      config::Setting<colour::BGR> *d_expectedLineColour;
      config::Setting<colour::BGR> *d_horizonColour;
      config::Setting<colour::BGR> *d_fieldEdgeColour;
      config::Setting<colour::BGR> *d_fieldHistogramColour;
      config::Setting<colour::BGR> *d_fieldHistogramIgnoredColour;
      config::Setting<colour::BGR> *d_occlusionEdgeColour;
      config::Setting<colour::BGR> *d_calibrationColour;

      config::Setting<bool> *d_shouldDrawBlobs;
      config::Setting<bool> *d_shouldDrawLineDots;
      config::Setting<bool> *d_shouldDrawObservedLines;
      config::Setting<bool> *d_shouldDrawExpectedLines;
      config::Setting<bool> *d_shouldDrawExpectedLineEdges;
      config::Setting<bool> *d_shouldDrawHorizon;
      config::Setting<bool> *d_shouldDrawFieldEdge;
      config::Setting<bool> *d_shouldDrawFieldHistogram;
      config::Setting<bool> *d_shouldDrawOcclusionEdge;
      config::Setting<bool> *d_shouldDrawCalibration;
      config::Setting<bool> *d_shouldDrawObservedObjects;

      config::Setting<bool> *d_ballBlobMergingEnabled;

      config::Setting<bool> *d_playerDetectionEnabled;
    };
  }
}
