// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "visualcortex.hh"

#include "FieldMap/fieldmap.hh"
#include "ImageCodec/PngCodec/pngcodec.hh"
#include "LineFinder/ScanningLineFinder/scanninglinefinder.hh"
#include "Painter/painter.hh"
#include "Spatialiser/spatialiser.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/WorldFrameState/worldframestate.hh"

#include "config/SettingWriter/settingwriter.hh"

#include "geometry2/Operator/Intersection/intersection.hh"

#include "roundtable/Action/action.hh"
#include "roundtable/Protocol/CameraProtocol/cameraprotocol.hh"
#include "roundtable/RoundTableServer/roundtableserver.hh"

#include "util/SequentialTimer/sequentialtimer.hh"
#include "util/TeamColour/teamcolour.hh"

#include "vision/BallFinder/ballfinder.hh"
#include "vision/BlobMerger/blobmerger.hh"
#include "vision/BlobPipeline/blobpipeline.hh"
#include "vision/CameraModel/cameramodel.hh"
#include "vision/FieldEdgeBlobFilter/fieldedgeblobfilter.hh"
#include "vision/ImageLabeller/PixelLabeller/LUTImageLabeller/lutimagelabeller.hh"
#include "vision/ImageLabeller/TFImageLabeller/tfimagelabeller.hh"
#include "vision/IntegralImage/integralimage.hh"
#include "vision/LabelledImageProcessRunner/labelledimageprocessrunner.hh"
#include "vision/LabelledImageProcessor/BlobDetector/blobdetector.hh"
#include "vision/LabelledImageProcessor/CartoonPainter/cartoonpainter.hh"
#include "vision/LabelledImageProcessor/FieldEdgeFinder/CompleteFieldEdgeFinder/completefieldedgefinder.hh"
#include "vision/LabelledImageProcessor/FieldEdgeFinder/PeriodicFieldEdgeFinder/periodicfieldedgefinder.hh"
#include "vision/LabelledImageProcessor/FieldHistogramBuilder/fieldhistogrambuilder.hh"
#include "vision/LabelledImageProcessor/LabelCounter/labelcounter.hh"
#include "vision/LabelledImageProcessor/LineDotter/linedotter.hh"

#include <sys/stat.h>

using namespace bold;
using namespace bold::colour;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace rapidjson;
using namespace std;
using namespace Eigen;


VisualCortex::VisualCortex(shared_ptr <CameraModel> cameraModel,
                           shared_ptr <Spatialiser> spatialiser,
                           shared_ptr <Clock> clock,
                           roundtable::RoundTableServer &roundTableServer)
  : d_cameraModel(move(cameraModel)),
    d_spatialiser(move(spatialiser)),
    d_clock(move(clock)),
    d_saveNextYUVFrame(false),
    d_saveNextDebugFrame(false) {
  ASSERT(d_cameraModel);
  ASSERT(d_spatialiser);
  ASSERT(d_clock);

  d_shouldFlipImage = Config::getSetting<bool>("vision.flip-image");

  d_shouldDetectLines = Config::getSetting<bool>("vision.line-detection.enable");
  d_shouldCountLabels = Config::getSetting<bool>("vision.label-counter.enable");
  d_shouldDetectBlobs = Config::getSetting<bool>("vision.blob-detection.enable");

  d_shouldIgnoreAboveHorizon = Config::getSetting<bool>("vision.ignore-above-horizon");
  d_isRecordingYUVFrames = Config::getSetting<bool>("camera.recording-frames");

  d_streamFramePeriod = Config::getSetting<int>("round-table.camera-frame-frequency");
  d_imageType = Config::getSetting<ImageType>("round-table.image-type");

  d_shouldDrawBlobs = Config::getSetting<bool>("round-table.image-features.blobs");
  d_shouldDrawLineDots = Config::getSetting<bool>("round-table.image-features.line-dots");
  d_shouldDrawObservedLines = Config::getSetting<bool>("round-table.image-features.observed-lines");
  d_shouldDrawExpectedLines = Config::getSetting<bool>("round-table.image-features.expected-lines");
  d_shouldDrawExpectedLineEdges = Config::getSetting<bool>("round-table.image-features.expected-line-edges");
  d_shouldDrawHorizon = Config::getSetting<bool>("round-table.image-features.horizon");
  d_shouldDrawFieldEdge = Config::getSetting<bool>("round-table.image-features.field-edge");
  d_shouldDrawFieldHistogram = Config::getSetting<bool>("round-table.image-features.field-histogram");
  d_shouldDrawOcclusionEdge = Config::getSetting<bool>("round-table.image-features.occlusion-edge");
  d_shouldDrawCalibration = Config::getSetting<bool>("round-table.image-features.calibration");
  d_shouldDrawObservedObjects = Config::getSetting<bool>("round-table.image-features.objects");

  d_ballBlobMergingEnabled = Config::getSetting<bool>("vision.ball-detection.enable-blob-merging");
  d_playerDetectionEnabled = Config::getSetting<bool>("vision.player-detection.enable");

  d_lineDotColour = Config::getSetting<colour::BGR>("round-table.image-colours.line-dot");
  d_observedLineColour = Config::getSetting<colour::BGR>("round-table.image-colours.observed-line");
  d_expectedLineColour = Config::getSetting<colour::BGR>("round-table.image-colours.expected-line");
  d_horizonColour = Config::getSetting<colour::BGR>("round-table.image-colours.horizon");
  d_fieldEdgeColour = Config::getSetting<colour::BGR>("round-table.image-colours.field-edge");
  d_fieldHistogramColour = Config::getSetting<colour::BGR>("round-table.image-colours.field-histogram");
  d_fieldHistogramIgnoredColour = Config::getSetting<colour::BGR>("round-table.image-colours.field-histogram-ignored");
  d_occlusionEdgeColour = Config::getSetting<colour::BGR>("round-table.image-colours.occlusion-edge");
  d_calibrationColour = Config::getSetting<colour::BGR>("round-table.image-colours.calibration");

  // TODO: this is put in correct LabelClass order, use map instead?

  auto goalLabel = make_shared<RangePixelLabel>("goal",
                                                LabelClass::GOAL,
                                                Config::getValue<colour::HSVRange>("vision.pixel-labels.goal"));

  auto ballLabel = make_shared<RangePixelLabel>("ball",
                                                LabelClass::BALL,
                                                Config::getValue<colour::HSVRange>("vision.pixel-labels.ball"));

  auto ball2Label = make_shared<RangePixelLabel>("ball",
                                                 LabelClass::BALL,
                                                 Config::getValue<colour::HSVRange>("vision.pixel-labels.ball2"));

  auto fieldLabel = make_shared<RangePixelLabel>("field",
                                                 LabelClass::FIELD,
                                                 Config::getValue<colour::HSVRange>("vision.pixel-labels.field"));

  auto lineLabel = make_shared<RangePixelLabel>("line",
                                                LabelClass::LINE,
                                                Config::getValue<colour::HSVRange>("vision.pixel-labels.line"));

  auto cyanLabel = make_shared<RangePixelLabel>("cyan",
                                                LabelClass::CYAN,
                                                Config::getValue<colour::HSVRange>("vision.pixel-labels.cyan"));

  auto magentaLabel = make_shared<RangePixelLabel>("magenta",
                                                   LabelClass::MAGENTA,
                                                   Config::getValue<colour::HSVRange>("vision.pixel-labels.magenta"));

  auto borderLabel = make_shared<RangePixelLabel>("border",
                                                  LabelClass::BORDER,
                                                  Config::getValue<colour::HSVRange>("vision.pixel-labels.border"));

  // All labels we are using
  d_pixelLabels = {goalLabel, ballLabel, fieldLabel, lineLabel, cyanLabel, magentaLabel, borderLabel};

  // The labels we try to find blobs for
  auto blobPixelLabels =
    d_playerDetectionEnabled->getValue() ?
    vector<shared_ptr<PixelLabel>> ({ ballLabel, cyanLabel, magentaLabel }) :
    vector<shared_ptr<PixelLabel>> ({ ballLabel });

  // Use simple fast LUT labeller for per-pixel labelling
  // TODO: This is only needed if we do subsampling; if we work with fully labelled images, we can just use those instead of this
  d_pixelLabeller = make_shared<LUTImageLabeller<18>> ();
  if (Config::getSetting<bool>("vision.tf-image-labeller.enable")->getValue())
    d_imageLabeller = make_shared<TFImageLabeller>(
      Config::getSetting<std::string>("vision.tf-image-labeller.graph-file-name")->getValue(),
      Config::getSetting<std::string>("vision.tf-image-labeller.graph-input-layer")->getValue(),
      Config::getSetting<std::string>("vision.tf-image-labeller.graph-output-layer")->getValue()
    );
  else
    d_imageLabeller = d_pixelLabeller;

  d_labelTeacher = unique_ptr<LabelTeacher > {new LabelTeacher{d_pixelLabels}};

  auto createLookupTable = [this]() {
    Log::info("VisualCortex::VisualCortex") << "Creating pixel label LUT";
    for (shared_ptr <PixelLabel> label : d_pixelLabels)
      Log::verbose("VisualCortex::VisualCortex") << "  " << *label;

    d_pixelLabeller->update(d_pixelLabels);
  };

  createLookupTable();

  // Recreate lookup table on dynamic configuration changes
  auto bindLabel = [createLookupTable](string name, shared_ptr <RangePixelLabel> label) {
    auto setting = Config::getSetting<colour::HSVRange>(string("vision.pixel-labels.") + name);
    setting->changed.connect([label, createLookupTable](colour::HSVRange value) {
      label->setHSVRange(value);
      createLookupTable();
    });
  };
  bindLabel("goal", goalLabel);
  bindLabel("ball", ballLabel);
  bindLabel("field", fieldLabel);
  bindLabel("line", lineLabel);
  bindLabel("cyan", cyanLabel);
  bindLabel("magenta", magentaLabel);

  // ball detection settings
  d_minBallAreaPixels = Config::getSetting<int>("vision.ball-detection.min-area-px");
  d_maxBallFieldEdgeDistPixels = Config::getSetting<int>("vision.ball-detection.max-field-edge-distance-px");
  d_acceptedBallMeasuredSizeRatio =
    Config::getSetting<bold::util::Range<double >> ("vision.ball-detection.accepted-size-ratio");

  // goal detection settings
  d_minGoalDimensionPixels = Config::getSetting<int>("vision.goal-detection.min-dimension-px");
  d_maxGoalFieldEdgeDistPixels = Config::getSetting<int>("vision.goal-detection.max-field-edge-distance-px");
  d_acceptedGoalMeasuredWidthRatio =
    Config::getSetting<Range<double >> ("vision.goal-detection.accepted-width-ratio");

  // player detection settings
  d_minPlayerAreaPixels = Config::getSetting<int>("vision.player-detection.min-area-px");
  d_minPlayerLengthPixels = Config::getSetting<int>("vision.player-detection.min-length-px");
  d_goalieMarkerHeight = Config::getSetting<double>("vision.player-detection.goalie-marker-height");

  // Field thresholding
  d_fieldHistogramThreshold = Config::getSetting<double>("vision.field-edge-pass.field-histogram.threshold");

  // TODO don't pass this around -- look it up from config (?)
  static uint16_t imageWidth = d_cameraModel->imageWidth();
  static uint16_t imageHeight = d_cameraModel->imageHeight();

  d_lineDotter = make_shared<LineDotter>(imageWidth, fieldLabel, lineLabel);
  d_blobDetector = make_shared<BlobDetector>(imageWidth, imageHeight, blobPixelLabels);
  d_cartoonPainter = make_shared<CartoonPainter>(imageWidth, imageHeight);
  auto labelCountPass = make_shared<LabelCounter>(d_pixelLabels);
  auto completeFieldEdgePass = make_shared<CompleteFieldEdgeFinder>(*fieldLabel, imageWidth, imageHeight);
  auto periodicFieldEdgePass = make_shared<PeriodicFieldEdgeFinder>(*fieldLabel, *lineLabel, imageWidth, imageHeight,
                                                                    1 * 2 * 3 * 4);
  d_fieldHistogramBuilder = make_shared<FieldHistogramBuilder>(fieldLabel, imageHeight);

  d_imagePassRunner = make_shared<LabelledImageProcessRunner>();
  d_imagePassRunner->addHandler(d_fieldHistogramBuilder);

  d_fieldEdgeTracer = make_unique<FieldEdgeTracer>();

  Config::getSetting<FieldEdgeType>("vision.field-edge-pass.field-edge-type")->track(
    [this, periodicFieldEdgePass, completeFieldEdgePass]
      (FieldEdgeType fieldEdgeType) {
      switch (fieldEdgeType) {
        case FieldEdgeType::Complete:
          d_imagePassRunner->removeHandler(periodicFieldEdgePass);
          d_imagePassRunner->addHandler(completeFieldEdgePass);
          d_fieldEdgeFinder = completeFieldEdgePass;
          break;
        case FieldEdgeType::Periodic:
          d_imagePassRunner->removeHandler(completeFieldEdgePass);
          d_imagePassRunner->addHandler(periodicFieldEdgePass);
          d_fieldEdgeFinder = periodicFieldEdgePass;
          break;
      }
    }
  );

  Config::getSetting<ImageGranularity>("vision.image-granularity")->track(
    [this](ImageGranularity granularity) {
      switch (granularity) {
        case ImageGranularity::All:
          d_granularityFunction = [](int y) { return Eigen::Matrix<uint8_t, 2, 1>(1, 1); };
          break;
        case ImageGranularity::Half:
          d_granularityFunction = [](int y) { return Eigen::Matrix<uint8_t, 2, 1>(2, 2); };
          break;
        case ImageGranularity::Third:
          d_granularityFunction = [](int y) { return Eigen::Matrix<uint8_t, 2, 1>(3, 3); };
          break;
        case ImageGranularity::Gradient: {
          auto maxGranularity = Config::getSetting<int>("vision.max-granularity");
          d_granularityFunction = [this, maxGranularity](int y) mutable {
            int pixelDelta = (d_cameraModel->imageHeight() - y) / 40;
            if (pixelDelta == 0)
              pixelDelta = 1;
            auto max = maxGranularity->getValue();
            if (pixelDelta > max)
              pixelDelta = max;
            return Eigen::Matrix<uint8_t, 2, 1>(pixelDelta, pixelDelta);
          };
          break;
        }
        case ImageGranularity::Projected: {
          auto maxGranularity = Config::getSetting<int>("vision.max-granularity");
          d_granularityFunction = [this, maxGranularity](int y) mutable {
            Vector2d referencePixel(d_cameraModel->imageWidth() / 2, y);

            auto midLineAgentSpace = d_spatialiser->findGroundPointForPixel(referencePixel);

            int pixelDelta = numeric_limits<int>::max();

            if (midLineAgentSpace) {
              double desiredDistanceMetres = 0.01; // TODO as setting
              auto offsetAgentSpace = *midLineAgentSpace + Vector3d(desiredDistanceMetres, 0, 0);
              util::Maybe <Vector2d> offsetPixel = d_spatialiser->findPixelForAgentPoint(offsetAgentSpace);

              if (offsetPixel)
                pixelDelta = (int) round(abs((*offsetPixel - referencePixel).norm()));
            }

            auto max = maxGranularity->getValue();

            if (pixelDelta > max)
              pixelDelta = max;
            else if (pixelDelta < 1)
              pixelDelta = 1;

            return Eigen::Matrix<uint8_t, 2, 1>(pixelDelta, pixelDelta);
          };
          break;
        }
      }
    }
  );

  d_shouldDetectLines->track([this](bool value) { d_imagePassRunner->setHandler(d_lineDotter, value); });

  d_shouldCountLabels->track(
    [this, labelCountPass](bool value) { d_imagePassRunner->setHandler(labelCountPass, value); });

  d_shouldDetectBlobs->track([this](bool value) {
    d_imagePassRunner->setHandler(d_blobDetector, value);
    if (!value)
      d_blobDetector->clear();
  });

  d_cameraProtocol = make_shared<roundtable::CameraProtocol>(d_clock);

  // Only include the cartoon pass when needed
  auto setCartoonHandler = [this] {
    bool enable = d_imageType->getValue() == ImageType::Cartoon && d_cameraProtocol->hasClients();
    d_imagePassRunner->setHandler(d_cartoonPainter, enable);
  };

  d_imageType->track([setCartoonHandler](ImageType value) { setCartoonHandler(); });

  d_cameraProtocol->hasClientChanged.connect([setCartoonHandler](bool enabled) {
    setCartoonHandler();
  });

  roundTableServer.registerProtocol(d_cameraProtocol);
  d_lineFinder = make_shared<ScanningLineFinder>(d_cameraModel);

  // Image capture
  roundtable::Action::addAction("camera.save-yuv-frame", "Save YUV Frame", [this] { d_saveNextYUVFrame = true; });
  roundtable::Action::addAction("camera.save-debug-frame", "Save Debug Frame", [this] { d_saveNextDebugFrame = true; });
}

void VisualCortex::integrateImage(cv::Mat &image, SequentialTimer &t, ulong thinkCycleNumber) {
  //
  // Flip, if required
  //
  if (d_shouldFlipImage->getValue())
    cv::flip(image, image, -1);

  //
  // Record frame, if required
  //
  static Clock::Timestamp lastRecordTime;
  if (d_saveNextYUVFrame || (d_isRecordingYUVFrames->getValue() && d_clock->getSecondsSince(lastRecordTime) > 1.0)) {
    saveImage(image, nullptr);
    lastRecordTime = d_clock->getTimestamp();
    d_saveNextYUVFrame = false;
    t.timeEvent("Save YUV Frame");
  }

  if (d_labelTeacher->requestedSnapShot()) {
    Log::info("Label Teacher") << "Setting train image";
    d_labelTeacher->setYUVTrainImage(image);
    t.timeEvent("Save YUV Training Image");
  }

  //
  // PROCESS THE IMAGE
  //

  // Build a map to control sub-sampling from the image
  ImageSampleMap sampleMap(d_granularityFunction, (uint16_t) image.cols, (uint16_t) image.rows);
  t.timeEvent("Build Sample Map");

  // Label pixels
  t.enter("Pixel Label");
  auto horizon = LineSegment2i{Point2i{0, image.rows}, Point2i{image.cols - 1, image.rows}};
  if (d_shouldIgnoreAboveHorizon->getValue()) {
    horizon.p1().y() = d_spatialiser->findHorizonForColumn(0);
    horizon.p2().y() = d_spatialiser->findHorizonForColumn(image.cols - 1);
    horizon.initSlope();
  }

  auto labelData = d_imageLabeller->labelImage(image, sampleMap, horizon, &t);
  t.exit();

  // Perform the image pass
  d_imagePassRunner->run(labelData, &t);

  // Set field edge state
  State::set(d_fieldEdgeFinder->getFieldEdgeState());

  // Find lines
  geometry2::LineSegment2i::Vector observedLineSegments;
  if (d_shouldDetectLines->getValue()) {
    t.enter("Line Search");
    observedLineSegments = d_lineFinder->findLineSegments(d_lineDotter->lineDots);
    t.exit();
  }

  auto goalLabel = std::find_if(begin(d_pixelLabels), end(d_pixelLabels),
                                [](shared_ptr <PixelLabel> label) {
                                  return label->getClass() == LabelClass::GOAL;
                                });

  // auto ballLabel = std::find_if(begin(d_pixelLabels), end(d_pixelLabels),
  //                               [](shared_ptr<PixelLabel> label)
  //                               {
  //                                 return label->getClass() == LabelClass::BALL;
  //                               });

  // auto fieldLabel = std::find_if(begin(d_pixelLabels), end(d_pixelLabels),
  //                                [](shared_ptr<PixelLabel> label)
  //                                {
  //                                  return label->getClass() == LabelClass::FIELD;
  //                                });

  // Trace field edge
  ASSERT(goalLabel != end(d_pixelLabels));
  d_fieldEdgeTracer->trace(image, {*goalLabel}, *d_pixelLabeller);

  // Build integral image
  /*
  static unique_ptr<IntegralImage> integralImage = nullptr;
  t.enter("Integral Image");
  integralImage = IntegralImage::create(labelData, LabelClass::BALL, move(integralImage));
  t.exit();
  */

  util::Maybe <Vector2d> ballPosition = Maybe<Vector2d>::empty();
  vector <Vector2d, aligned_allocator<Vector2d>> goalPositions;
  vector <Vector2d, aligned_allocator<Vector2d>> teamMatePositions;

  if (d_shouldDetectBlobs->getValue()) {
    // Find blobs
    t.enter("Blob Detect");
    auto blobsPerLabel = d_blobDetector->detectBlobs(t);
    t.exit();

    //
    // UPDATE STATE
    //
    // Might we have a ball?
    if (blobsPerLabel[d_pixelLabels[(uint8_t) LabelClass::BALL - 1]].size() > 0) {
      auto &ballBlobs = blobsPerLabel[d_pixelLabels[(uint8_t) LabelClass::BALL - 1]];
      ballPosition = detectBall(ballBlobs, image, *d_pixelLabeller, t);
    }

    auto teamColour = Config::getStaticValue<TeamColour>("team-colour");

    auto ourColourLabel = d_pixelLabels[
      (uint8_t)(teamColour == TeamColour::Cyan ? LabelClass::CYAN : LabelClass::MAGENTA) - 1];

    // Do we have own teammate blobs?
    if (d_playerDetectionEnabled->getValue() && blobsPerLabel[ourColourLabel].size() > 0) {
      auto &playerBlobs = blobsPerLabel[ourColourLabel];
      teamMatePositions = detectPlayers(playerBlobs, image, t);
    }
  }

  goalPositions = d_goalpostDetector.detect(image, *d_pixelLabeller, *d_fieldEdgeTracer, *d_spatialiser);

  vector <OcclusionRay<uint16_t>> occlusionRays = d_fieldEdgeFinder->getOcclusionRays();

  // TODO do this widening in the image frame, not during projection
  static auto widenOcclusions = Config::getSetting<bool>("vision.occlusion.widen");
  static auto midHeightToWiden = Config::getSetting<int>("vision.occlusion.widen-min-height-px");

  if (widenOcclusions->getValue()) {
    vector <OcclusionRay<uint16_t>> widenedRays;
    for (uint i = 0; i < occlusionRays.size(); i++) {
      auto const &ray = occlusionRays[i];
      auto near = ray.near();
      if ((ray.far().y() - ray.near().y()) > midHeightToWiden->getValue()) {
        // Widen occlusions by taking the near as the lowest of itself and its neighbours in the image
        if (i != 0)
          near.y() = min(near.y(), occlusionRays[i - 1].near().y());
        if (i != occlusionRays.size() - 1)
          near.y() = min(near.y(), occlusionRays[i + 1].near().y());
      }
      widenedRays.emplace_back(near, ray.far());
    }

    occlusionRays = widenedRays;
  }

  long processedPixelCount = sampleMap.getPixelCount();
  long totalPixelCount = image.rows * image.cols;

  State::make<CameraFrameState>(ballPosition, goalPositions, teamMatePositions,
                                observedLineSegments, occlusionRays,
                                totalPixelCount, processedPixelCount, thinkCycleNumber);

  t.timeEvent("Updating State");
}

Maybe <Vector2d> VisualCortex::detectBall(Blob::Vector &ballBlobs, cv::Mat const &image, PixelLabeller const &labeller,
                                          SequentialTimer &t) {
  //
  // Build up pipeline
  //

  auto pipeline = BlobPipeline{};

  if (d_ballBlobMergingEnabled->getValue()) {
    auto merger = BlobMerger{};
    merger.addPredicate([](Blob const &a, Blob const &b) {
      return shouldMergeBallBlobs(a, b);
    });
    pipeline = pipeline.foldLeft(merger);
  }

  // Ignore blobs that are too small (avoid noise)
  pipeline = pipeline.filter([this](Blob const &blob) {
    auto min = d_minBallAreaPixels->getValue();
    ASSERT(min >= 0)
    return blob.area() >= unsigned(d_minBallAreaPixels->getValue());
  });

  // Ignore blobs if it is too far from the field edge
  auto fieldEdgeFilter = FieldEdgeBlobFilter{d_maxBallFieldEdgeDistPixels};
  pipeline = pipeline.filter(fieldEdgeFilter);

  // Verify blob is about the expected pixel size at that position of the frame
  /*
  pipeline = pipeline.filter([this](Blob const& blob) {
      auto bounds = blob.bounds();
      int maxDimension = std::max(bounds.sizes().x(), bounds.sizes().y());

      Vector2d sidePos = blob.mean().cast<double>() + Vector2d(maxDimension/2.0, 0);

      static double ballRadius = FieldMap::getBallRadius();

      auto midPointAgentSpace = d_spatialiser->findGroundPointForPixel(blob.mean().cast<double>(), ballRadius);
      auto sidePointAgentSpace = d_spatialiser->findGroundPointForPixel(sidePos, ballRadius);

      if (!midPointAgentSpace || !sidePointAgentSpace)
        return false;

      double radiusAgentSpace = (*midPointAgentSpace - *sidePointAgentSpace).norm();

      double ballMeasuredSizeRatio = radiusAgentSpace / ballRadius;
      if (ballMeasuredSizeRatio < d_acceptedBallMeasuredSizeRatio->getValue().min() ||
          ballMeasuredSizeRatio > d_acceptedBallMeasuredSizeRatio->getValue().max())
        return false;

      // If the ball would be further than the max diagonal distance
      // of the field, then we assume it is not the ball.
      if ((*midPointAgentSpace - geometry2::Point3d::ORIGIN).norm()
          > FieldMap::getMaxDiagonalFieldDistance())
        return false;

      return true;
    });
  */

  //
  // Run pipeline
  //

  auto ballBlobCandidates = pipeline.run(ballBlobs, image);


  //
  // Find full extension of ball coordinates
  //
  auto fieldLabel = std::find_if(begin(d_pixelLabels), end(d_pixelLabels),
                                 [](shared_ptr <PixelLabel> label) {
                                   return label->getClass() == LabelClass::FIELD;
                                 });

  auto ballFinder = BallFinder{};
  auto candidateBoxes = ballFinder.find(image, ballBlobCandidates, *fieldLabel, *d_pixelLabeller);

  //
  // Verify box is about the expected pixel size at its position of the frame
  //
  auto candidatesEnd = std::remove_if(candidateBoxes.begin(), candidateBoxes.end(),
                                      [this](AlignedBox2i const &bounds) {
                                        int maxDimension = std::max(bounds.sizes().x(), bounds.sizes().y());

                                        Vector2d center = bounds.center().cast<double>();
                                        Vector2d sidePos = center + Vector2d(maxDimension / 2.0, 0);

                                        static double ballRadius = FieldMap::getBallRadius();

                                        auto midPointAgentSpace = d_spatialiser->findGroundPointForPixel(center,
                                                                                                         ballRadius);
                                        auto sidePointAgentSpace = d_spatialiser->findGroundPointForPixel(sidePos,
                                                                                                          ballRadius);

                                        if (!midPointAgentSpace || !sidePointAgentSpace)
                                          return true;
                                        double radiusAgentSpace = (*midPointAgentSpace - *sidePointAgentSpace).norm();

                                        double ballMeasuredSizeRatio = radiusAgentSpace / ballRadius;
                                        if (ballMeasuredSizeRatio < d_acceptedBallMeasuredSizeRatio->getValue().min() ||
                                            ballMeasuredSizeRatio > d_acceptedBallMeasuredSizeRatio->getValue().max())
                                          return true;
                                        // If the ball would be further than the max diagonal distance
                                        // of the field, then we assume it is not the ball.
                                        if ((*midPointAgentSpace - geometry2::Point3d::ORIGIN).norm() > FieldMap::getMaxDiagonalFieldDistance())
                                          return true;

                                        return false;
                                      });

  auto ballPosition = Maybe<Vector2d>::empty();

  //
  // Take the ball that is closest
  //
  if (candidatesEnd != candidateBoxes.begin()) {
    double ballRadius = FieldMap::getBallRadius();
    auto boxDistCompare = [this, ballRadius](AlignedBox2i const &box1, AlignedBox2i const &box2) {
      auto pos1 = d_spatialiser->findGroundPointForPixel(box1.center().cast<double>(), ballRadius);
      auto pos2 = d_spatialiser->findGroundPointForPixel(box2.center().cast<double>(), ballRadius);
      if (!pos1 || !pos2) {
        // These boxes should have been filtered out
        Log::error("VisualCortex::detectBall") << "Invalid blob(s) while finding nearest";
        // Say 1 is nearer if 2 doesn't exist
        return !pos2;
      }
      return ((pos1->toDim<2>() - geometry2::Point2d::ORIGIN).norm() <
              (pos2->toDim<2>() - geometry2::Point2d::ORIGIN).norm());
    };

    auto nearest = min_element(candidateBoxes.begin(), candidatesEnd,
                               boxDistCompare);
    ballPosition = util::Maybe<Vector2d>(nearest->center().cast<double>());
  }
  t.timeEvent("Ball Blob Selection");

  return ballPosition;
}

vector <Vector2d, aligned_allocator<Vector2d>>
VisualCortex::detectPlayers(Blob::Vector &playerBlobs, cv::Mat const &image, SequentialTimer &t) {
//
// Build up pipeline
//

  auto pipeline = BlobPipeline{};
  auto minArea = d_minPlayerAreaPixels->getValue();
  pipeline = pipeline.filter([minArea](Blob const &blob) {
    return blob.area() >= minArea;
  });

  auto minLength = d_minPlayerLengthPixels->getValue();
  pipeline = pipeline.filter([minLength](Blob const &blob) {
    auto blobSize = blob.bounds().sizes();
    return blobSize.x() >= minLength && blobSize.y() >= minLength;
  });

  auto markerHeight = d_goalieMarkerHeight->getValue();
  pipeline = pipeline.filter([this, markerHeight](Blob const &blob) {
    return d_spatialiser->findGroundPointForPixel(blob.mean().cast<double>(), markerHeight).hasValue();
  });

  auto playerCandidates = pipeline.run(playerBlobs, image);

  auto playerPositions = vector<Vector2d, aligned_allocator<Vector2d>>
  { playerCandidates.size() };

  transform(begin(playerCandidates), end(playerCandidates), begin(playerPositions),
            [](Blob const &blob) {
              return blob.mean().cast<double>();;
            });

  return playerPositions;
}

void VisualCortex::saveImage(cv::Mat const &image, std::map <uint8_t, colour::BGR> *palette) {
  time_t rawtime;
  time(&rawtime);

  tm *timeinfo = localtime(&rawtime);

  char dateTimeString[80];
  // %G yyyy
  // %m MM
  // %d dd
  // %H HH
  // %M mm
  // %S ss
  //
  strftime(dateTimeString, 80, "%G%m%d-%H:%M:%S", timeinfo);

  string folderName = "captures";
  mkdir(folderName.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);

  stringstream fileName;
  fileName << folderName << "/" << dateTimeString << ".png";
  Log::info("VisualCortex::saveImage") << "Saving " << fileName.str();

  // Encode the image
  PngCodec codec;
  vector <uint8_t> imageBuffer;
  codec.encode(image, imageBuffer, palette);

  // Write it to a file
  ofstream imageFile;
  imageFile.open(fileName.str());
  imageFile.write(reinterpret_cast<char *>(imageBuffer.data()), imageBuffer.size());
  imageFile.close();

  // Clear the filename
  fileName.str(std::string());

  fileName << folderName << "/" << dateTimeString << ".json";
  Log::info("VisualCortex::saveImage") << "Saving " << fileName.str();

  // Build up the output JSON
  StringBuffer buffer;
  PrettyWriter <StringBuffer> writer(buffer);
  writer.SetMaxDecimalPlaces(3);
  writer.StartObject();
  {
    // Host name
    char hostName[80];
    if (gethostname(hostName, 80) == -1) {
      Log::warning("VisualCortex::saveImage") << "gethostname failed: " << strerror(errno);
    } else {
      writer.String("hostname");
      writer.String(hostName);
    }

    // Current date and time
    writer.String("date");
    writer.String(dateTimeString);

    // Camera settings
    writer.String("camera-settings");
    auto settingWriter = SettingWriter{};

    writer.StartObject();
    {
      for (SettingBase *setting : Config::getSettings("camera.settings")) {
        writer.String(setting->getName().c_str());
        settingWriter.writeJsonValue(writer, *setting);
      }
    }
    writer.EndObject();

    // Body pose
    writer.String("body");
    StateObjectBase::writeJsonOrNull(writer, State::get<BodyState>(StateTime::CameraImage));

    // Camera Frame
    writer.String("cameraFrame");
    StateObjectBase::writeJsonOrNull(writer, State::get<CameraFrameState>());

    // Agent Frame
    writer.String("agentFrame");
    StateObjectBase::writeJsonOrNull(writer, State::get<AgentFrameState>());
  }
  writer.EndObject();

  // Write the JSON file
  ofstream jsonFile;
  jsonFile.open(fileName.str());
  jsonFile << buffer.GetString();
  jsonFile.close();
}

bool VisualCortex::shouldMergeBallBlobs(Blob const &larger, Blob const &smaller) {
  // Merge if the union would be more square, and the new maxDimension is not far off the original one

  // TODO VISION do not merge blobs if the union would be too large to be considered as the ball

  auto combined = larger.bounds().merged(smaller.bounds());

  double largerAspect = (double) larger.bounds().sizes().minCoeff() / larger.bounds().sizes().maxCoeff();
  double combinedAspect = (double) combined.sizes().minCoeff() / combined.sizes().maxCoeff();

  // If combining these blobs would result in a shape that's less square-like, don't combine
  if (combinedAspect < largerAspect)
    return false;

  int maxDimension = std::max(larger.bounds().sizes().maxCoeff(), smaller.bounds().sizes().maxCoeff());
  int unionMaxDimension = combined.sizes().maxCoeff();

  return unionMaxDimension < maxDimension * 1.3;
}

void VisualCortex::streamDebugImage(cv::Mat &cameraImage, SequentialTimer &t) {
  // Only compose the image if at least one client is connected
  if (!d_cameraProtocol->hasClients())
    return;

  // Only provide an image every N cycles
  if (State::getTracker<CameraFrameState>()->updateCount() % d_streamFramePeriod->getValue() != 0)
    return;

  auto const &cameraFrame = State::get<CameraFrameState>();

  static auto backgroundColour = Config::getSetting<colour::BGR>("round-table.cartoon.background-colour");

  cv::Mat debugImage;

  ImageType imageType = d_imageType->getValue();
  map <uint8_t, colour::BGR> palette;
  bool usePalette = false;

  bool drawDebugData = true;

  // Select the base imagery
  switch (imageType) {
    case ImageType::YCbCr: {
      debugImage = cameraImage;
      break;
    }
    case ImageType::RGB: {
      debugImage = cameraImage;
      PixelFilterChain chain;
      chain.pushFilter(&yCbCrToBgrInPlace);
      chain.applyFilters(debugImage);
      break;
    }
    case ImageType::Cartoon: {
      debugImage = d_cartoonPainter->mat().clone();
      double min, max;
      cv::minMaxIdx(debugImage, &min, &max);
      Log::info("VisualCortex::streamDebugImage") << "Streaming cartoon. min : " << min << ", max: " << max;
      // Do this each frame, as colours can change at runtime
      palette[0] = backgroundColour->getValue();
      for (std::shared_ptr <PixelLabel> const &label : d_pixelLabels) {
        auto c = uint8_t(label->getClass());
        Log::info("VisualCortex::streamDebugImage") << "Adding class to palette: " << unsigned{c};
        palette[c] = label->modalColour().toBGR();
      }
      usePalette = true;
      break;
    }
    case ImageType::None: {
      debugImage = cv::Mat(cameraImage.size(), CV_8UC3, cv::Scalar(0));
      palette[0] = backgroundColour->getValue();
      usePalette = true;
      break;
    }
    case ImageType::Teacher: {
      drawDebugData = false;

      if (d_labelTeacher->requestedLabel()) {
        debugImage = d_labelTeacher->label(d_labelTeacher->getLabelToTrain());
      } else {
        auto trainImage = d_labelTeacher->getBGRTrainImage();
        if (!(trainImage.rows == cameraImage.rows && trainImage.cols == cameraImage.cols)) {
          debugImage = cameraImage;
          PixelFilterChain chain;
          chain.pushFilter(&colour::yCbCrToBgrInPlace);
          chain.applyFilters(debugImage);
        } else {
          auto maskImage = d_labelTeacher->getMask();
          if (maskImage.rows == trainImage.rows && maskImage.cols == trainImage.cols) {
            cv::Mat multiChannelMask;
            cv::merge(vector<cv::Mat>{maskImage, maskImage, maskImage}, multiChannelMask);
            cv::bitwise_xor(trainImage, multiChannelMask, debugImage);
          } else
            debugImage = trainImage;

          cv::rectangle(debugImage, cv::Rect(0, 0, debugImage.cols, debugImage.rows), cv::Scalar{0, 0, 255}, 3);
        }
      }
      break;
    }
    default: {
      Log::error("VisualCortex::streamDebugging") << "Unknown image type requested!";
      debugImage = cv::Mat(cameraImage.size(), CV_8UC3, cv::Scalar(0));
      break;
    }
  }

  auto getColour = [&palette, usePalette](colour::BGR color) -> cv::Scalar {
    if (usePalette) {
      auto existingEntry = find_if(palette.begin(), palette.end(),
                                   [color](decltype(palette)::value_type const &entry) {
                                     return entry.second == color;
                                   });
      if (existingEntry != palette.end())
        return cv::Scalar(existingEntry->first);
      else {
        ASSERT(palette.size() < std::numeric_limits<uint8_t>::max());

        auto nextIndex = uint8_t(palette.size());
        palette[nextIndex] = color;
        return cv::Scalar(nextIndex);
      }
    }

    return color.toScalar();
  };

  // Draw observed lines
  auto const &observedLineSegments = State::get<CameraFrameState>()->getObservedLineSegments();
  if (drawDebugData && d_shouldDrawObservedLines->getValue() && !observedLineSegments.empty()) {
    cv::Scalar observedLineColour = getColour(d_observedLineColour->getValue());
    for (auto const &line : observedLineSegments)
      Painter::draw(line, debugImage, observedLineColour, 2);
  }

  // Draw line dots
  if (drawDebugData && d_shouldDetectLines->getValue() && d_shouldDrawLineDots->getValue() &&
      !d_lineDotter->lineDots.empty()) {
    if (usePalette) {
      uint8_t lineDotColour = (uint8_t)(getColour(d_lineDotColour->getValue()).val[0]);
      for (auto const &lineDot : d_lineDotter->lineDots)
        debugImage.at<uint8_t>(lineDot.y(), lineDot.x()) = lineDotColour;
    } else {
      auto const &lineDotColour = d_lineDotColour->getValue();
      for (auto const &lineDot : d_lineDotter->lineDots)
        debugImage.at<BGR>(lineDot.y(), lineDot.x()) = lineDotColour;
    }
  }

  // Draw blobs
  if (drawDebugData && d_shouldDrawBlobs->getValue()) {
    auto const &blobsByLabel = d_blobDetector->getDetectedBlobs();

    for (auto const &pixelLabel : d_blobDetector->pixelLabels()) {
      // Determine outline colour for the blob
      cv::Scalar blobColour = getColour(pixelLabel->getThemeColour());

      for (auto const &blob : blobsByLabel.at(pixelLabel)) {
        // Blobs with zero area were merged into other blobs
        if (blob.area() != 0) {
          auto rect = cv::Rect(blob.bounds().min().x(), blob.bounds().min().y(),
                               blob.bounds().sizes().x(), blob.bounds().sizes().y());
          cv::rectangle(debugImage, rect, blobColour);
        }
      }
    }
  }

  // Draw observed objects (ie. actual ball/goal posts chosen from blobs)
  if (drawDebugData && d_shouldDrawObservedObjects->getValue()) {
    auto ball = cameraFrame->getBallObservation();
    if (ball) {
      auto ballColor = getColour(BGR(0, 0, 255));
      cv::Rect rect((int) round(ball->x()), (int) round(ball->y()), 5, 5);
      cv::rectangle(debugImage, rect, ballColor, CV_FILLED);
    }

    auto goals = cameraFrame->getGoalObservations();
    auto goalColor = getColour(BGR(0, 255, 255));
    for (auto goal : goals) {
      cv::Rect rect((int) round(goal.x()), (int) round(goal.y()), 5, 5);
      cv::rectangle(debugImage, rect, goalColor, CV_FILLED);
    }
  }

  auto bodyState = State::get<BodyState>(StateTime::CameraImage);

  // Draw expected lines
  bool drawExpectedLines = d_shouldDrawExpectedLines->getValue();
  bool drawExpectedLineEdges = d_shouldDrawExpectedLineEdges->getValue();
  if (drawDebugData && (drawExpectedLines || drawExpectedLineEdges)) {
    auto const &worldFrameState = State::get<WorldFrameState>();
    Affine3d const &agentWorld = worldFrameState->getPosition().agentWorldTransform();
    Affine3d const &cameraAgent = bodyState->getCameraAgentTransform();

    Affine3d const &cameraWorld = cameraAgent * agentWorld;

    auto max = Vector2i(Config::getStaticValue<int>("camera.image-width"),
                        Config::getStaticValue<int>("camera.image-height"));

    auto expectedLineColour = getColour(d_expectedLineColour->getValue());

    auto visibleFieldPoly = worldFrameState->getVisibleFieldPoly();

    if (visibleFieldPoly.hasValue()) {
      for (auto const &expectedLine : drawExpectedLineEdges ? FieldMap::getFieldLineEdges()
                                                            : FieldMap::getFieldLines()) {
        // Clip world lines based upon visible field poly before transforming to camera frame
        auto clippedLine2 = intersection(*visibleFieldPoly, expectedLine.toDim<2>());
        if (clippedLine2.hasValue()) {
          auto clippedLine = clippedLine2.value().toDim<3>();

          auto p1 = d_cameraModel->pixelForDirection(cameraWorld * (clippedLine.p1() - geometry2::Point3d::ORIGIN));
          auto p2 = d_cameraModel->pixelForDirection(cameraWorld * (clippedLine.p2() - geometry2::Point3d::ORIGIN));

          if (p1.hasValue() && p2.hasValue()) {
            auto p1v = p1->cast<int>();
            auto p2v = p2->cast<int>();
            if (p1v != p2v) {
              auto line2i = geometry2::LineSegment2i{max - p1v, max - p2v};

              Painter::draw(line2i, debugImage, expectedLineColour, 1);
            }
          }
        }
      }
    }
  }

  // Draw horizon
  if (drawDebugData && d_shouldDrawHorizon->getValue()) {
    auto const &cameraAgentTr = bodyState->getCameraAgentTransform();

    auto p1 = geometry2::Point2i{0, 0};
    p1.y() = d_spatialiser->findHorizonForColumn(p1.x(), cameraAgentTr);

    auto p2 = geometry2::Point2i{d_cameraModel->imageWidth() - 1, 0};
    p2.y() = d_spatialiser->findHorizonForColumn(p2.x(), cameraAgentTr);

    auto line2i = geometry2::LineSegment2i{p1, p2};

    Painter::draw(line2i, debugImage, getColour(d_horizonColour->getValue()), 1);
  }

  // Draw down
  if (drawDebugData && d_shouldDrawHorizon->getValue()) {
    auto const &cameraAgentTr = bodyState->getCameraAgentTransform();

    auto p1 = geometry2::Point2i{d_cameraModel->imageWidth() / 2,
                                 d_cameraModel->imageHeight() / 2};
    auto p2 = p1 + (20 * d_spatialiser->getCameraDown(cameraAgentTr)).cast<int>();
    auto line2i = geometry2::LineSegment2i{p1, p2};

    Painter::draw(line2i, debugImage, getColour(d_horizonColour->getValue()), 1);
  }

  // Draw occlusion edge
  if (drawDebugData && d_shouldDrawOcclusionEdge->getValue()) {
    auto occlusionColour = getColour(d_occlusionEdgeColour->getValue());
    auto const &rays = cameraFrame->getOcclusionRays();
    auto lastPoint = rays[0].near();
    for (auto const &ray : rays) {
      // Connected left-to-right lines
      cv::line(debugImage,
               cv::Point(lastPoint.x(), lastPoint.y()),
               cv::Point(ray.near().x(), ray.near().y()),
               occlusionColour);

      // Lines from front to back
      cv::line(debugImage,
               cv::Point(ray.near().x(), ray.near().y()),
               cv::Point(ray.far().x(), ray.far().y()),
               occlusionColour);

      lastPoint = ray.near();
    }
  }

  // Draw field histogram
  if (drawDebugData && d_shouldDrawFieldHistogram->getValue()) {
    auto colour = getColour(d_fieldHistogramColour->getValue());
    auto outColour = getColour(d_fieldHistogramIgnoredColour->getValue());
    const double threshold = d_fieldHistogramThreshold->getValue();
    for (uint16_t y = 0; y < debugImage.size().height; ++y) {
      uint16_t count = d_fieldHistogramBuilder->getRowWidth(y);
      if (count) {
        cv::line(
          debugImage,
          cv::Point(0, y),
          cv::Point(count, y),
          d_fieldHistogramBuilder->getRatioBeneath(y) > threshold
          ? outColour
          : colour);
      }
    }
  }

  // Draw field edge
  if (drawDebugData && d_shouldDrawFieldEdge->getValue()) {
    if (usePalette) {
      uint8_t fieldEdgeColour = (uint8_t) getColour(d_fieldEdgeColour->getValue()).val[0];
      for (uint16_t x = 0; x < debugImage.size().width; ++x) {
        // TODO: get state object
        uint16_t y = d_fieldEdgeFinder->getFieldEdgeState()->getEdge().getYAt(x);
        debugImage.at<uint8_t>(y, x) = fieldEdgeColour;
      }
    } else {
      auto fieldEdgeColour = d_fieldEdgeColour->getValue();
      for (uint16_t x = 0; x < debugImage.size().width; ++x) {
        // TODO: get state object
        uint16_t y = d_fieldEdgeFinder->getFieldEdgeState()->getEdge().getYAt(x);
        debugImage.at<BGR>(y, x) = fieldEdgeColour;
      }
    }
  }

  // Draw field edge traces
  if (drawDebugData && d_shouldDrawFieldEdge->getValue()) {
    for (auto const &run : d_fieldEdgeTracer->getRuns()) {
      if (run.label == nullptr)
        continue;

      auto colour = getColour(run.label->getThemeColour());
      cv::line(debugImage, cv::Point(run.start.x(), run.start.y()), cv::Point(run.end.x(), run.end.y()),
               colour, 3);
    }
  }

  // Draw calibration lines
  if (drawDebugData && d_shouldDrawCalibration->getValue()) {
    auto calibrationColour = getColour(d_calibrationColour->getValue());

    auto w = d_cameraModel->imageWidth();
    auto h = d_cameraModel->imageHeight();
    auto midX = static_cast<int>(round(w / 2.0));
    auto midY = static_cast<int>(round(h / 2.0));
    cv::line(debugImage, cv::Point(0, midY), cv::Point(w - 1, midY), calibrationColour);
    cv::line(debugImage, cv::Point(midX, 0), cv::Point(midX, h - 1), calibrationColour);
  }

  auto imageEncoding = d_imageType->getValue() == ImageType::None || d_imageType->getValue() == ImageType::Cartoon
                       ? ImageCodec::ImageEncoding::PNG
                       : ImageCodec::ImageEncoding::JPEG;

  d_cameraProtocol->streamImage(debugImage, imageEncoding, palette);
  t.timeEvent("Compose Debug Image");

  if (d_saveNextDebugFrame) {
    d_saveNextDebugFrame = false;
    saveImage(debugImage, &palette);
    t.timeEvent("Save Debug Image");
  }
}
