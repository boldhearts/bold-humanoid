// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "blob.hh"

using namespace bold::vision;
using namespace std;
using namespace Eigen;

Blob::Blob(set<Run> runSet)
  : d_area{0},
    d_mean{0, 0},
    d_runs{move(runSet)}
{
  for (auto const& run : d_runs)
  {
    auto runBounds = AlignedBox2i{Vector2i{run.startX(), run.y()},
                                  Vector2i{run.endX(), run.y()}};
    d_bounds = d_bounds.merged(runBounds);

    auto y = run.y();
    auto length = run.length();

    d_area += length;
    d_mean.x() += length * ((run.endX() + run.startX()) / 2.0f);
    d_mean.y() += length * y;
  }
  
  d_mean /= d_area;
}

Blob::Blob()
  : d_bounds{Eigen::Vector2i::Zero(), Eigen::Vector2i::Zero()},
    d_area{0},
    d_mean(Eigen::Vector2f::Zero())
    {}

void Blob::merge(Blob const& other)
{
  ASSERT(other.area() != 0);
  d_mean = ((d_mean * d_area) + (other.mean() * other.area())) / (d_area + other.area());
  d_area += other.area();
  d_bounds = d_bounds.merged(other.bounds());
  d_runs.insert(other.runs().begin(), other.runs().end());
}

bool Blob::operator==(Blob const& other) const
{
  return
    d_area == other.area() &&
    d_mean == other.mean() &&
    d_runs == other.runs();
}

bool Blob::operator<(Blob const& other) const
{
  return
    d_area < other.area() ||
             (d_area == other.area() && d_mean.y() < other.mean().y());
}



Run::Run(uint16_t startX, uint16_t y)
  : d_y(y),
    d_startX(startX),
    d_endX(startX)
{}

Run::Run(uint16_t startX, uint16_t endX, uint16_t y)
  : d_y(y),
    d_startX(startX),
    d_endX(endX)
{}

uint16_t Run::length() const
{
  return d_endX - d_startX + (uint16_t)1;
}

bool Run::overlaps(Run const& b) const
{
  return std::max(d_endX, b.endX()) - std::min(d_startX, b.startX()) < length() + b.length();
}

bool Run::operator==(Run const& other) const
{
  return
    d_y == other.y() &&
    d_startX == other.startX() &&
    d_endX == other.endX();    
}

bool Run::operator<(Run const& other) const
{
  return
    d_y < other.y() ||
          (d_y == other.y() && d_startX < other.startX());
}
