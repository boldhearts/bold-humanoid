// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "geometry2/Point/point.hh"
#include "util/assert.hh"
#include <set>

namespace bold
{
  namespace vision
  {
    class Run;

    /** A conjoined set of Runs
     *
     * A Blob is a datastructure for connected components in images,
     * i.e. a group of pixels that are connected to each other. It is
     * built up of a set if runs, where each run is defined by the
     * first and last pixel in a horizontel set of connected pixels.
     **/
    class Blob
    {
    public:
      Blob();
      Blob(std::set<Run> runs);

      Eigen::AlignedBox2i bounds() const { return d_bounds; }
      unsigned area() const { return d_area; }
      Eigen::Vector2f mean() const { return d_mean; };
      std::set<Run> const& runs() const { return d_runs; }
      void merge(Blob const& other);

      void setArea(unsigned area) __attribute__((deprecated)) { d_area = area; }
      
      bool operator==(Blob const& other) const;
      bool operator!=(Blob const& other) const { return !(*this == other); }

      bool operator<(Blob const& other) const;
      bool operator>(Blob const& other) const { return other < *this; }

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      using Vector = std::vector<Blob, Eigen::aligned_allocator<Blob>>;

      inline friend std::ostream& operator<<(std::ostream& stream, Blob const& blob)
      {
        return stream <<
          "Blob (ul=[" << blob.d_bounds.corner(blob.d_bounds.TopLeft).transpose() << "]" <<
          " br=[" << blob.d_bounds.corner(blob.d_bounds.BottomRight).transpose() << "])";
      }
      
    private:
      Eigen::AlignedBox2i d_bounds;
      unsigned d_area;             ///< Number of pixels in blob
      Eigen::Vector2f d_mean;   ///< Mean

      std::set<Run> d_runs;        ///< Runs in this blob

    };   

    /** Horizontal run of pixels
     *
     **/
    class Run
    {
    public:
      /// Create run that starts and ends at @a startX
      Run(uint16_t startX, uint16_t y);

      /// Create run with different start and end points
      Run(uint16_t startX, uint16_t endX, uint16_t y);

      uint16_t y() const { return d_y; }
      uint16_t startX() const { return d_startX; }
      uint16_t endX() const { return d_endX; }
      
      /** Returns the number of pixels in the run.
       *
       * @returns Number of pixels from start up to and including end
       * pixel. So, if a run starts and stops at the same x position,
       * it has length one.
       **/
      uint16_t length() const;

      /** Returns whether two runs overlap, ignoring y positions. */
      bool overlaps(Run const& b) const;

      /** Test equality */
      bool operator==(Run const& other) const;

      /** Compare operator
       *
       * Orders runs by y, then by startX.
       */
      bool operator<(Run const& other) const;

      /** Middle of run
       *
       * If length is even, the middle will be halfway between 2 pixels
       */
      float midX() const { return (d_endX + d_startX) / 2.0f; }

      inline friend std::ostream& operator<<(std::ostream& stream, Run const& run)
      {
        return stream <<
          "Run (y=" << run.d_y <<
          " x=[" << run.d_startX << "," << run.d_endX << "]" <<
          " len=" << run.length() << ")";
      }
      
    private:
      uint16_t d_y;        ///< The row index of the horizontal run
      uint16_t d_startX;   ///< The column index of the left-most pixel
      uint16_t d_endX;     ///< The column index of the right-most pixel
    };
  }
}
