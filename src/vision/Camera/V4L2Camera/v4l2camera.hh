// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "../camera.hh"

#include <linux/videodev2.h>

namespace bold
{
  namespace vision
  {
    class V4L2Camera : public Camera
    {
    public:
      V4L2Camera(std::string device);

      void open() override;
      bool requestSize(size_t width, size_t height) override;
      void start() override;
      void stop() override;

      cv::Mat capture() override;

    private:
      /** Image format description
       *
       * Describes what image data means
       */
      struct Format
      {
        Format(v4l2_fmtdesc const& fd)
          : index(fd.index),
            type(fd.type),
            flags(fd.flags),
            description((const char*)fd.description),
            pixelFormat(fd.pixelformat)
        {}

        /// Number of the format in the enumeration, set by the application
        unsigned index;

        /// Type of the data stream, set by the application, probably to V4L2_BUF_TYPE_VIDEO_CAPTURE
        unsigned type;

        /// Image format description flags. Options: V4L2_FMT_FLAG_COMPRESSED and/or V4L2_FMT_FLAG_EMULATED
        unsigned flags;

        /// Human readable description of the format
        std::string description;

        /// The image format identifier as computed by the v4l2_fourcc() macro
        unsigned pixelFormat;
      };

      /** Format and layout of an image in memory
       * 
       * Describes how data is structured
       */
      struct PixelFormat
      {
        PixelFormat()
        {}

        PixelFormat(v4l2_pix_format const& pf)
          : width (pf.width),
            height(pf.height),
            pixelFormat(pf.pixelformat),
            bytesPerLine(pf.bytesperline),
            imageByteSize(pf.sizeimage)
        {}

        /// Image width in pixels
        unsigned width;

        /// Image height in pixels
        unsigned height;

        /// The pixel format or type of compression, set by the application
        unsigned pixelFormat;

        /// Distance in bytes between the leftmost pixels in two adjacent lines
        unsigned bytesPerLine;

        /// Size in bytes of the buffer to hold a complete image, set by the driver
        unsigned imageByteSize;

        /// Human readable description of the format
        std::string pixelFormatString() const
        {
          char chars[5];
          for (unsigned i = 0; i < 4; ++i)
            chars[i] = ((pixelFormat >> (i * 8)) & 0xFF);
          chars[4] = 0;
          return std::string(chars);
        }
      };

      struct Buffer
      {
        unsigned index;
        unsigned char* start;
        size_t length;
      };

      bool canRead() {  return d_capabilities.capabilities & V4L2_CAP_READWRITE; }
      
      bool canStream() { return d_capabilities.capabilities & V4L2_CAP_STREAMING; }

      void initMemoryMapping();
      void createControls();
      void createFormats();
      
      std::string d_device;
      int d_fd;

      v4l2_capability d_capabilities;
      
      std::vector<Format> d_formats;
      PixelFormat d_pixelFormat;

      std::vector<Buffer> d_buffers;
    };
  }
}
     
