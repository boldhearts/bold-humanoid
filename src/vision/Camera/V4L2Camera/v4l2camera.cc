// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "v4l2camera.hh"

#include "util/Log/log.hh"
#include "vision/YUYVProcessor/yuyvprocessor.hh"
#include "state/State/state.hh"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

using namespace bold::state;
using namespace bold::vision;
using namespace bold::util;
using namespace std;

V4L2Camera::V4L2Camera(string device)
  : d_device{device},
    d_fd{0}
{}

void V4L2Camera::open()
{
  d_fd = ::open(d_device.c_str(), O_RDWR);

  if (d_fd < 0)
  {
    Log::error("Camera::open") << "Failed opening device " << d_device << ": " << strerror(errno) << " (" << errno << ")";
    exit(EXIT_FAILURE);
  }

  // List capabilities
  ioctl(d_fd, VIDIOC_QUERYCAP, &d_capabilities);

  // List current format
  v4l2_format formatReq;
  memset(&formatReq, 0, sizeof(formatReq));
  formatReq.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(d_fd, VIDIOC_G_FMT, &formatReq);
  d_pixelFormat = PixelFormat(formatReq.fmt.pix);

  Log::verbose("Camera::open") << "Creating controls";
  createControls();
  Log::verbose("Camera::open") << "Creating image formats";
  createFormats();

  Log::verbose("Camera::open") << "Capabilities:";
  Log::verbose("Camera::open") << "  Read/write: " << (canRead() ? "YES" : "NO");
  Log::verbose("Camera::open") << "  Streaming:  " << (canStream() ? "YES" : "NO");

  Log::verbose("Camera::open") << "Controls (" << getControls().size() << "):";
  for (auto const& control : getControls())
    Log::verbose("Camera::open") << "  " << control->name;

  Log::verbose("Camera::open") << "Formats (" << d_formats.size() << "):";
  for (auto const& format : d_formats)
    Log::verbose("Camera::open") << "  "  << format.description;
}

bool V4L2Camera::requestSize(size_t width, size_t height)
{
  // Build size request
  v4l2_format formatReq;
  memset(&formatReq, 0, sizeof(formatReq));
  formatReq.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  formatReq.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
  formatReq.fmt.pix.width = width;
  formatReq.fmt.pix.height = height;
  formatReq.fmt.pix.pixelformat = d_pixelFormat.pixelFormat;

  // Perform request
  int res = ::ioctl(d_fd, VIDIOC_S_FMT, &formatReq);

  if (res < 0)
  {
    Log::error("Camera::PixelFormat::requestSize") << "Error setting camera size: " << strerror(errno) << " (" << errno << ")";
  }

  // Get new format from camera
  ::ioctl(d_fd, VIDIOC_G_FMT, &formatReq);

  d_pixelFormat.width = formatReq.fmt.pix.width;
  d_pixelFormat.height = formatReq.fmt.pix.height;
  d_pixelFormat.bytesPerLine = formatReq.fmt.pix.bytesperline;
  d_pixelFormat.imageByteSize = formatReq.fmt.pix.sizeimage;

  // Check if update was successful
  return d_pixelFormat.width == width
      && d_pixelFormat.height == height;
}

void V4L2Camera::start()
{
  // Start memory mapping
  initMemoryMapping();

  // Queue the buffers
  for (Buffer buffer : d_buffers)
  {
    v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = buffer.index;

    if (-1 == ioctl(d_fd, VIDIOC_QBUF, &buf))
    {
      Log::error("Camera::startCapture") << "Buffer failure on capture start: " << strerror(errno) << " (" << errno << ")";
      exit(EXIT_FAILURE);
    }
  }

  // Start stream
  unsigned type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == ioctl(d_fd, VIDIOC_STREAMON, &type))
  {
    Log::error("Camera::startCapture") << "Failed stream start: " << strerror(errno) << " (" << errno << ")";
    exit(EXIT_FAILURE);
  }
}

void V4L2Camera::stop()
{
  // Stop stream
  unsigned type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == ioctl(d_fd, VIDIOC_STREAMOFF, &type))
  {
    Log::error("Camera::stopCapture") << "Failed stream stop";
    exit(EXIT_FAILURE);
  }
}

cv::Mat V4L2Camera::capture()
{
  v4l2_buffer buf;
  memset(&buf, 0, sizeof(buf));

  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;

  if (-1 == ioctl(d_fd, VIDIOC_DQBUF, &buf))
  {
    Log::error("Camera::capture") << "Error dequeueing buffer: " << strerror(errno) << " (" << errno << ")";
    exit(EXIT_FAILURE);
  }

  // Snapshot some state objects at this moment
  State::snapshot(StateTime::CameraImage);

  if (-1 == ioctl(d_fd, VIDIOC_QBUF, &buf))
  {
    Log::error("Camera::capture") << "Error re-queueing buffer: " << strerror(errno) << " (" << errno << ")";
    exit(EXIT_FAILURE);
  }

  Log::trace("Camera::capture") << "Image captured";

  auto img = cv::Mat(d_pixelFormat.height, d_pixelFormat.width, CV_8UC3);

  auto processor = YUYVProcessor{false};

  auto const& buffer = d_buffers[buf.index];
  auto datCursor = buffer.start;
  auto imgCursor = img.data;

  auto datView = Span<uint8_t>{datCursor, buffer.length};
  auto imgView = Span<uint8_t>{imgCursor, img.total() * img.elemSize()};
  processor.process(imgView, datView);

  return img;
}

void V4L2Camera::initMemoryMapping()
{
  struct v4l2_requestbuffers req;
  memset(&req, 0, sizeof(req));

  // Request 4 buffers
  req.count = 4;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;
  ioctl(d_fd, VIDIOC_REQBUFS, &req);

  // Didn't get more than 1 buffer
  if (req.count < 2)
  {
    Log::error("Camera") << "Insufficient buffer memory";
    exit(EXIT_FAILURE);
  }

  d_buffers = vector<Buffer>(req.count);

  for (unsigned i = 0; i < req.count; ++i)
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));

    buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory      = V4L2_MEMORY_MMAP;
    buf.index       = i;

    ioctl(d_fd, VIDIOC_QUERYBUF, &buf);

    d_buffers[i].index = buf.index;
    d_buffers[i].length = buf.length;
    d_buffers[i].start =
      (unsigned char*)mmap(NULL /* start anywhere */,
                           buf.length,
                           PROT_READ | PROT_WRITE /* required */,
                           MAP_SHARED /* recommended */,
                           d_fd, buf.m.offset);

    if (MAP_FAILED == d_buffers[i].start)
    {
      Log::error("Camera") << "Failed mapping device memory";
      exit(EXIT_FAILURE);
    }
  }
}

void V4L2Camera::createControls()
{
  // Because of the way that settings are built up, we cannot run this function more than once
  ASSERT(d_controls.size() == 0);

  //
  // Query the device for all camera controls
  //

  v4l2_queryctrl queryctrl = {0,};
  queryctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;

  while (ioctl(d_fd, VIDIOC_QUERYCTRL, &queryctrl) == 0)
  {
    // Ignore disabled controls
    if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
      continue;

    map<int,string> menuItems;
    if (queryctrl.type == (unsigned)ControlType::MENU)
    {
      struct v4l2_querymenu querymenu;
      memset (&querymenu, 0, sizeof(querymenu));
      querymenu.id = queryctrl.id;

      // Query all enum values
      for (auto i = queryctrl.minimum; i <= queryctrl.maximum; i++)
      {
        querymenu.index = i;
        if (ioctl(d_fd, VIDIOC_QUERYMENU, &querymenu) == 0)
          menuItems[i] = (const char*)querymenu.name;
      }
    }

    auto control = make_shared<Control>();
    auto id = control->id = queryctrl.id;
    auto name = control->name = string{reinterpret_cast<char*>(queryctrl.name)};
    control->type = static_cast<Camera::ControlType>(queryctrl.type);
    control->minimum = queryctrl.minimum;
    control->maximum = queryctrl.maximum;
    control->defaultValue = queryctrl.default_value;
    control->menuItems = move(menuItems);

    auto getValue = control->getValue =
      [id, name, this]() {
        v4l2_control ctrl = {0,};
        ctrl.id = id;
        ioctl(d_fd, VIDIOC_G_CTRL, &ctrl);
        Log::verbose("V4L2Camera::createControls::getCValue") << "Read camera control '" << name << "': " << ctrl.value;
        return ctrl.value;
      };

    control->setValue =
      [id, name, getValue, this](int value, function<void(int)> setBack) {
        Log::verbose("V4L2Camera::createControls::setCValue") << "Setting camera control '" << name << "' to value " << value;
        v4l2_control ctrl = {0,};
        ctrl.id = id;
        ctrl.value = value;
        ioctl(d_fd, VIDIOC_S_CTRL, &ctrl);

        // Test whether the value we set was taken or not
        int retrieved = getValue();

        Log::verbose("V4L2Camera::createControls::setCValue") << "Read back after setting '" << name << "': " << retrieved;

        if (retrieved != value)
        {
          Log::error("Camera::createControls::setValue") << "Setting camera control '" << name << "' failed -- set " << value << " but read back " << retrieved;
          setBack(retrieved);
        }
      };
    
    d_controls.push_back(move(control));

    // Get ready to query next item
    queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
  }
}

void V4L2Camera::createFormats()
{
  d_formats.clear();

  struct v4l2_fmtdesc fmtDesc;
  fmtDesc.index = 0;
  fmtDesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  while (ioctl(d_fd, VIDIOC_ENUM_FMT, &fmtDesc) == 0)
  {
    d_formats.emplace_back(fmtDesc);
    fmtDesc.index++;
  }
}
