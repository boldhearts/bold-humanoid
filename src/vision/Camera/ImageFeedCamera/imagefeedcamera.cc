// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "imagefeedcamera.hh"

#include "ImageCodec/PngCodec/pngcodec.hh"
#include "state/State/state.hh"
#include "util/Log/log.hh"

#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace bold::state;
using namespace bold::vision;
using namespace bold::util;

ImageFeedCamera::ImageFeedCamera(string const& imagePath)
  : d_imagePath{imagePath},
    d_requestedWidth{0},
    d_requestedHeight{0},
    d_regulator{}
{
  setFPS(1e6f);
}

void ImageFeedCamera::open()
{
  auto pngCodec = PngCodec{false, true};
  if (!pngCodec.read(d_imagePath, d_rawImage))
    throw std::runtime_error(string{"Unable to read image feed file: "} + d_imagePath);

  if (d_requestedWidth == 0)
  {
    d_requestedWidth = d_rawImage.cols;
    d_requestedHeight = d_rawImage.rows;

    d_image = d_rawImage;
  }
  else
    cv::resize(d_rawImage, d_image, cv::Size(d_requestedWidth, d_requestedHeight));
}

void ImageFeedCamera::start()
{
  d_regulator.start();
}

bool ImageFeedCamera::requestSize(size_t width, size_t height)
{
  d_requestedWidth = width;
  d_requestedHeight = height;

  if (d_rawImage.cols > 0)
    cv::resize(d_rawImage, d_image, cv::Size(width, height));
  return true;
}

void ImageFeedCamera::setFPS(float fps)
{
  d_fps = fps;
  d_regulator.setIntervalMicroseconds(1000000 / d_fps);
}

cv::Mat ImageFeedCamera::capture()
{
  d_regulator.wait();
  // TODO: use signal to remove dependency on State
  State::snapshot(StateTime::CameraImage);
  return d_image.clone();
}
