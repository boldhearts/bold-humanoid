// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "../camera.hh"
#include "util/LoopRegulator/loopregulator.hh"

namespace bold
{
  namespace vision
  {
    class ImageFeedCamera : public Camera
    {
    public:
      ImageFeedCamera(std::string const& imagePath);
      
      void open() override;
      void start() override;
      bool requestSize(size_t width, size_t height) override;
      cv::Mat capture() override;

      void setFPS(float fps);
      
    private:
      std::string d_imagePath;
      cv::Mat d_rawImage;

      size_t d_requestedWidth;
      size_t d_requestedHeight;

      util::LoopRegulator d_regulator;
      float d_fps;
      
      cv::Mat d_image;
    };
    
  }
}
