// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>
#include <functional>
#include <memory>
#include <algorithm>
#include <map>

namespace bold
{
  namespace vision
  {
    class Camera
    {
    public:
      struct Control;

      /// Type of camera control
      enum class ControlType : unsigned
      {
        INT        = 1,
        BOOL       = 2,
        MENU       = 3,
        BUTTON     = 4,
        INT64      = 5,
        CTRL_CLASS = 6,
        STRING     = 7,
        BITMASK    = 8
      };
      
      /** Open the camera resources
       */
      virtual void open() {};

      /** Request an image size, reports if request succeeded
       */
      virtual bool requestSize(size_t width, size_t height) = 0;

      /** Start processes for capturing
       */
      virtual void start() {};

      /** Stop processes for capturing
       */
      virtual void stop() {};

      /** Capture new frame
       *
       * Can block until the next frame is ready
       */
      virtual cv::Mat capture() = 0;

      struct Control
      {
        /// Identifies the control, set by the application
        unsigned id;
        
        /// Human readable name
        std::string name;
        
        /// Type of control
        ControlType type;
        
        /// Minimum value, inclusive
        int minimum;
        
        /// Maximum value, inclusive
        int maximum;
        
        /// The default value of of an integer, boolean, bitmask, menu or integer menu control
        int defaultValue;
        
        /// Menu item names by index. Empty if this is not a menu control
        std::map<int,std::string> menuItems;

        /// Get control value
        std::function<int()> getValue;

        /// Set control value
        std::function<void(int,std::function<void(int)>)> setValue;
      };

      std::vector<std::shared_ptr<Control>> const& getControls() const
      {
        return d_controls;
      }
      
      std::shared_ptr<Control> getControl(std::string const& name) const
      {
        auto control = std::find_if(d_controls.begin(), d_controls.end(),
                     [&name](std::shared_ptr<Control> const& control) {
                       return control->name == name;
                     });

        if (control == d_controls.end())
          return nullptr;
        return *control;
      }
        
    protected:
      std::vector<std::shared_ptr<Control>> d_controls;
    };
  }
}
