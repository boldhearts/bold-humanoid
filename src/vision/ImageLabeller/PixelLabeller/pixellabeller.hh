// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/ImageLabeller/imagelabeller.hh"

namespace bold {
  namespace vision {

    class PixelLabeller : public ImageLabeller {
    public:

      /** Update labeller to label according to given labels
       */
      virtual void update(std::vector<std::shared_ptr<PixelLabel>> const &pixels) = 0;

      /** Label colour of a single pixel
       */
      virtual std::shared_ptr<PixelLabel> labelPixel(colour::YCbCr yuvColour) const = 0;

      /** Get label class for a single pixel
       */
      virtual LabelClass labelClassPixel(colour::YCbCr yuvColour) const {
        return labelPixel(yuvColour)->getClass();
      }

    };

  }
}
