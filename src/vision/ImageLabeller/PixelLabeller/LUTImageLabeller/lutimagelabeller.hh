// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/ImageLabeller/PixelLabeller/pixellabeller.hh"

#include "colour/HSV/hsv.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/ImageSampleMap/imagesamplemap.hh"

namespace bold {
  namespace vision {
    
    template<int BITS>
    class LUTImageLabeller : public PixelLabeller {
    public:
      LUTImageLabeller();

      explicit LUTImageLabeller(std::vector<std::shared_ptr<PixelLabel>> labels);

      void update(std::vector<std::shared_ptr<PixelLabel>> const &labels) override;

      LabelledImageData labelImage(cv::Mat const &image, ImageSampleMap const &sampleMap,
                                   geometry2::LineSegment2i const &horizon,
                                   util::SequentialTimer *timer = nullptr) const override;

      LabelClass labelClassPixel(colour::YCbCr yuvColour) const override {
        int constexpr CBITS = BITS / 3;
        unsigned idx = ((yuvColour.y >> (8 - CBITS)) << (2 * CBITS)) |
                       ((yuvColour.cb >> (8 - CBITS)) << CBITS) |
                       ((yuvColour.cr >> (8 - CBITS)));
        return (LabelClass) d_LUT[idx];
      }

      std::shared_ptr<PixelLabel> labelPixel(colour::YCbCr yuvColour) const override {
        LabelClass lClass = labelClassPixel(yuvColour);
        if (lClass == LabelClass::UNKNOWN)
          return nullptr;

        return *std::find_if(d_labels.begin(), d_labels.end(),
                             [lClass](std::shared_ptr<PixelLabel> const &label) {
                               return label->getClass() == lClass;
                             });
      }

      std::array<uint8_t, 1 << BITS> const &getLUT() const {
        return d_LUT;
      }

    private:
      std::array<uint8_t, 1 << BITS> buildLUT(std::vector<std::shared_ptr<PixelLabel>> const &labels);

      uint8_t findBestLabelClass(std::vector<std::shared_ptr<PixelLabel>> const &labels,
                                 colour::YCbCr ycbcr);

      std::vector<std::shared_ptr<PixelLabel>> d_labels;
      std::array<uint8_t, 1 << BITS> d_LUT;
      mutable std::mutex d_lutMutex;
    };


    template<int BITS>
    LUTImageLabeller<BITS>::LUTImageLabeller()
        : d_LUT{},
          d_lutMutex{} {}

    template<int BITS>
    LUTImageLabeller<BITS>::LUTImageLabeller(std::vector<std::shared_ptr<PixelLabel>> labels)
        : d_labels{std::move(labels)},
          d_LUT(buildLUT(d_labels)),
          d_lutMutex{} {}

    template<int BITS>
    void LUTImageLabeller<BITS>::update(std::vector<std::shared_ptr<PixelLabel>> const &labels) {
      d_labels = labels;
      std::lock_guard<std::mutex> guard(d_lutMutex);
      d_LUT = buildLUT(labels);
    }

    template<int BITS>
    LabelledImageData LUTImageLabeller<BITS>::labelImage(cv::Mat const &image,
                                                         ImageSampleMap const &sampleMap,
                                                         geometry2::LineSegment2i const &horizon,
                                                         util::SequentialTimer *timer) const {
      ASSERT(horizon.p1().x() == 0 && horizon.p2().x() == image.cols - 1);

      // Lock LUT, in case another thread reassigns the LUT (avoids segfault)
      std::unique_lock<std::mutex> guard(d_lutMutex);

      // Everything above (and including) this row is guaranteed to be above horizon
      int maxHorizonY = 0;
      // Everything below this row is guaranteed to be under horizon
      int minHorizonY;

      // If we are ignoring everything above the horizon, find out where
      // the horizon is for each column.
      // Remember, y = 0 is bottom of field of view (the image is upside down)
      auto horizonBottom = horizon.p1().y() < horizon.p2().y() ? horizon.p1() : horizon.p2();
      auto horizonTop = horizon.p1().y() > horizon.p2().y() ? horizon.p1() : horizon.p2();

      minHorizonY = horizonBottom.y();
      maxHorizonY = horizonTop.y();

      if (timer != nullptr)
        timer->timeEvent("Find Horizon");

      auto granularity = sampleMap.begin();
      uint16_t y = 0;

      minHorizonY = std::min(minHorizonY, image.rows);

      auto imageLabelData = LabelledImageData{sampleMap};
      auto labelData = imageLabelData.labelsBegin();

      //
      // First batch: everything guaranteed under the horizon
      //

      while (y < minHorizonY) {
        auto const *px = image.ptr<uint8_t>(y);

        auto from = labelData;

        const uint8_t dx = granularity->x();

        for (int x = 0; x < image.cols; x += dx) {
          *labelData = d_LUT[((px[0] >> 2) << 12) | ((px[1] >> 2) << 6) | (px[2] >> 2)];
          labelData++;
          px += dx * 3;
        }

        auto to = labelData;

        imageLabelData.emplace_row(from, to, y, *granularity);

        y += granularity->y();
        granularity++;
      }

      if (timer != nullptr)
        timer->timeEvent("Rows Under Horizon");

      //
      // Second batch: horizon goes through these rows
      //

      if (minHorizonY < image.rows) {
        ++maxHorizonY;

        // Upwards in x, where x=0 is right
        auto horizonUpwards = horizonTop.x() > horizonBottom.x();
        double horizonYRange = maxHorizonY - minHorizonY;

        while (y < maxHorizonY && y < image.rows) {
          auto const *px = image.ptr<uint8_t>(y);

          auto from = labelData;

          // at which point the horizon crosses this row, between 0 and 1
          double ratio = double(y - minHorizonY) / horizonYRange;
          int horizonX = Math::lerp(ratio, horizonBottom.x(), horizonTop.x());

          // TODO: wrap two loops in for loop? Our Atom can't do branch
          // prediction, so doing same check inside loop may be costly
          // We can also directly fill the line we know to be 0
          for (int x = 0; x < image.cols; x += granularity->x()) {
            bool aboveHorizon = (x < horizonX && horizonUpwards) || (x > horizonX && !horizonUpwards);

            uint8_t l = aboveHorizon
                        ? (uint8_t) 0
                        : d_LUT[((px[0] >> 2) << 12) | ((px[1] >> 2) << 6) | (px[2] >> 2)];

            *labelData = l;
            ++labelData;

            px += granularity->x() * 3;
          }

          auto to = labelData;

          imageLabelData.emplace_row(from, to, y, *granularity);

          y += granularity->y();
          granularity++;
        }

        if (timer != nullptr)
          timer->timeEvent("Rows Intersecting Horizon");
      }

      guard.unlock();

      return imageLabelData;
    }

    template<int BITS>
    std::array<uint8_t, 1 << BITS>
    LUTImageLabeller<BITS>::buildLUT(std::vector<std::shared_ptr<PixelLabel>> const &labels) {
      auto lut = std::array<uint8_t, 1 << BITS>{};
      auto p = lut.begin();

      int constexpr CBITS = BITS / 3;
      for (int y = 0; y < (1 << CBITS); ++y)
        for (int cb = 0; cb < (1 << CBITS); ++cb)
          for (int cr = 0; cr < (1 << CBITS); ++cr)
            *(p++) = findBestLabelClass(labels, colour::YCbCr(static_cast<uint8_t>(y << (8 - CBITS)),
                                                              static_cast<uint8_t>(cb << (8 - CBITS)),
                                                              static_cast<uint8_t>(cr << (8 - CBITS))));
      return lut;
    }

    template<int BITS>
    uint8_t LUTImageLabeller<BITS>::findBestLabelClass(std::vector<std::shared_ptr<PixelLabel>> const &labels,
                                                       colour::YCbCr ycbcr) {
      // TODO should we use the floating point conversion from YCbCr to BGR here for accuracy?
      auto hsv = ycbcr.toBGRInt().toHSV();

      // Find best matching label
      float bestProb = 0.0f;
      std::shared_ptr<PixelLabel> bestLabel = nullptr;

      for (auto const &label : labels) {
        auto prob = label->labelProb(hsv);
        if (prob > bestProb) {
          bestProb = prob;
          bestLabel = label;
        }
      }

      if (bestLabel)
        return uint8_t(bestLabel->getClass());
      else
        return 0;
    }
  }
}
