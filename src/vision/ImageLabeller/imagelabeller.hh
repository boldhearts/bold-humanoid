// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>

#include <vector>
#include <memory>
#include <mutex>

#include "colour/YCbCr/ycbcr.hh"
#include "geometry2/Geometry/LineSegment/linesegment.hh"
#include "vision/PixelLabel/pixellabel.hh"

namespace bold {
  namespace util {
    class SequentialTimer;
  }

  namespace vision {
    class LabelledImageData;

    class ImageSampleMap;

    class ImageLabeller {
    public:
      virtual ~ImageLabeller() = default;;

      /** Labels image pixels according to the specified sample map.
       */
      virtual LabelledImageData labelImage(cv::Mat const &image, ImageSampleMap const &sampleMap,
                                           geometry2::LineSegment2i const &horizon,
                                           util::SequentialTimer *timer) const = 0;
    };
  }
}
