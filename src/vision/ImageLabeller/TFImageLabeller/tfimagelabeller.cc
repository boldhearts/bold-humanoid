// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "tfimagelabeller.hh"

#include "util/Log/log.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"

#include "tensorflow/core/public/session.h"

using namespace bold::util;
using namespace bold::vision;
using namespace tensorflow;

TFImageLabeller::TFImageLabeller(std::string const& graphFile, std::string inputLayer, std::string outputLayer)
  : d_session{nullptr},
    d_inputLayer{std::move(inputLayer)},
    d_outputLayer{std::move(outputLayer)}
{
  // Load graph
  tensorflow::GraphDef graph_def;
  auto load_graph_status =
      ReadBinaryProto(tensorflow::Env::Default(), graphFile, &graph_def);
  if (!load_graph_status.ok())
    throw std::runtime_error{"Failed to load compute graph at '" + graphFile + "'"};

  d_session.reset(tensorflow::NewSession(tensorflow::SessionOptions()));

  Status session_create_status = d_session->Create(graph_def);
  if (!session_create_status.ok())
    throw std::runtime_error{"Failed creating TF session"};
}

TFImageLabeller::~TFImageLabeller() = default;
TFImageLabeller::TFImageLabeller(TFImageLabeller&& other) = default;

LabelledImageData TFImageLabeller::labelImage(cv::Mat const& image, ImageSampleMap const &sampleMap,
                                              geometry2::LineSegment2i const& horizon,
                                              SequentialTimer *timer) const
{
  if (timer != nullptr)
    timer->timeEvent("start labelling");
  
  tensorflow::Tensor imageTensor(
    tensorflow::DT_FLOAT,
    tensorflow::TensorShape({1, image.rows, image.cols, 3})
    );

  Log::verbose("TFImageLabeller::labelImage") << "Creating tensor...";
  
  float *imageTensorData = imageTensor.flat<float>().data();

  Log::verbose("TFImageLabeller::labelImage") << "Converting to float...";
  auto floatImage = cv::Mat{image.rows, image.cols, CV_32FC3, imageTensorData};
  image.convertTo(floatImage, CV_32FC3, 1 / 255.0);

  if (timer != nullptr)
    timer->timeEvent("convert to float");
  
  std::vector<Tensor> outputs;

  // Run image through model
  Log::verbose("TFImageLabeller::labelImage") << "Running model... " << d_inputLayer << " -> " << d_outputLayer << " | "
                                           << imageTensor.NumElements() << " : " << imageTensor.SummarizeValue(10);
  
  auto runStatus = d_session->Run({{d_inputLayer, imageTensor}},
                                  {d_outputLayer}, {}, &outputs);
  if (timer != nullptr)
    timer->timeEvent("model run");

  Log::verbose("TFImageLabeller::labelImage") << "Done! status: " << runStatus;
  
  if (!runStatus.ok())
  {
    auto msg = std::ostringstream{};
    msg << runStatus;
    throw std::runtime_error{msg.str()};
  }

  Log::verbose("TFImageLabeller::labelImage") << "Number of output tensors: " << outputs.size();
  Log::verbose("TFImageLabeller::labelImage") << "Number of dims: " << outputs[0].dims();
  Log::verbose("TFImageLabeller::labelImage") << outputs[0].NumElements() << " : " << outputs[0].SummarizeValue(10);

  auto outputHeight = outputs[0].dim_size(1);
  auto outputWidth = outputs[0].dim_size(2);
  auto nLabels = outputs[0].dim_size(3);

  auto outputMapped = outputs[0].tensor<float, 4>();
  auto* outputData = outputMapped.data();
  
  auto labelledData = LabelledImageData{sampleMap};
  auto labelledDataIter = labelledData.labelsBegin();
  auto granularity = sampleMap.begin();
  auto y = 0;

  while (y < outputHeight)
  {
    auto* outputLabelScores = outputData + outputWidth * y * nLabels;

    auto from = labelledDataIter;
    auto dx = granularity->x();

    for (int x = 0; x < outputWidth; x += dx) {
      auto outputLabelScoresEnd = outputLabelScores;
      std::advance(outputLabelScoresEnd, nLabels);
      auto maxLabel = std::distance(
        outputLabelScores,
        std::max_element(outputLabelScores, outputLabelScoresEnd)
        );

      *labelledDataIter = maxLabel;
      labelledDataIter++;
      outputLabelScores += dx * nLabels;
    }

    auto to = labelledDataIter;

    labelledData.emplace_row(from, to, y, *granularity);

    y += granularity->y();
    granularity++;
  }
  
  if (timer != nullptr)
    timer->timeEvent("build labelled data");

  return labelledData;
}
