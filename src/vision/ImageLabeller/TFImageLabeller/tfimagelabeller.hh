// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/ImageLabeller/imagelabeller.hh"
#include <memory>

namespace tensorflow
{
  class Session;
}

namespace bold {
  namespace vision {

    class TFImageLabeller : public ImageLabeller {
    public:
      explicit TFImageLabeller(std::string const& graphFile,
                               std::string inputLayer, std::string outputLayer);
      TFImageLabeller(TFImageLabeller&& other);
      ~TFImageLabeller();
      
      /** Labels image pixels according to the specified sample map.
       */
      LabelledImageData labelImage(cv::Mat const &image, ImageSampleMap const &sampleMap,
                                   geometry2::LineSegment2i const &horizon,
                                   util::SequentialTimer *timer) const override;
    private:
      std::unique_ptr<tensorflow::Session> d_session;

      std::string d_inputLayer;
      std::string d_outputLayer;
    };

  }
}
