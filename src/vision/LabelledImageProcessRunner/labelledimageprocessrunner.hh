// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <mutex>
#include <memory>
#include <set>

namespace bold
{
  namespace util
  {
    class SequentialTimer;
  }
  
  namespace vision
  {
    class LabelledImageData;
    class LabelledImageProcessor;

    class LabelledImageProcessRunner
    {
    public:
      LabelledImageProcessRunner() = default;

      /** Adds the specified handler, if it does not already exist in the runner.
       */
      void addHandler(std::shared_ptr<LabelledImageProcessor> processor);
      
      /** Removes the specified handler, if it already exists in the runner.
       */
      void removeHandler(std::shared_ptr<LabelledImageProcessor> processor);
      
      /** Adds or removes the specified handler, depending upon the bool 'enabled' parameter.
       */
      void setHandler(std::shared_ptr<LabelledImageProcessor> processor, bool enabled);

      /** Passes over the image, calling out to all processors with data from the image.
       */
      void run(LabelledImageData const& labelledImageData, util::SequentialTimer* timer) const;

    private:
      mutable std::mutex d_handlerMutex;

      std::set<std::shared_ptr<LabelledImageProcessor>> d_handlers;
    };
  }
}
