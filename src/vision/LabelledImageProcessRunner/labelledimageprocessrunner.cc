// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "labelledimageprocessrunner.hh"

#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"

using namespace bold;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

void LabelledImageProcessRunner::addHandler(std::shared_ptr<LabelledImageProcessor> handler)
{
  ASSERT(handler);

  lock_guard<mutex> guard(d_handlerMutex);
  if (std::find(d_handlers.begin(), d_handlers.end(), handler) == d_handlers.end())
    d_handlers.insert(handler);
}

/** Removes the specified handler, if it already exists in the runner.
*/
void LabelledImageProcessRunner::removeHandler(std::shared_ptr<LabelledImageProcessor> handler)
{
  ASSERT(handler);

  lock_guard<mutex> guard(d_handlerMutex);
  auto it = d_handlers.find(handler);

  if (it != d_handlers.end())
    d_handlers.erase(it);
}

/** Adds or removes the specified handler, depending upon the bool 'enabled' parameter.
*
* Idempotent.
*/
void LabelledImageProcessRunner::setHandler(std::shared_ptr<LabelledImageProcessor> handler, bool enabled)
{
  if (enabled)
    addHandler(handler);
  else
    removeHandler(handler);
}

/** Passes over the image, calling out to all ImagePassHandlers with data from the image.
*/
void LabelledImageProcessRunner::run(LabelledImageData const& labelData, SequentialTimer* timer) const
{
  lock_guard<mutex> guard(d_handlerMutex);
  for (auto const& handler : d_handlers)
  {
    if (timer != nullptr)
      timer->enter(handler->id());
    handler->process(labelData, timer);
    if (timer != nullptr)
      timer->exit();
  }
}
