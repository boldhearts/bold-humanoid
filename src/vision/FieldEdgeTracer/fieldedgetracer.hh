// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "config/SettingBase/Setting/setting.hh"
#include "geometry2/Point/point.hh"

#include <Eigen/Core>
#include <memory>
#include <opencv2/core/core.hpp>

namespace bold {
  namespace vision {
    class PixelLabel;

    class PixelLabeller;

    struct FieldEdgeRun {
      geometry2::Point2i start;
      geometry2::Point2i end;
      std::shared_ptr<vision::PixelLabel> label;
    };

    class FieldEdgeTracer {
    public:
      FieldEdgeTracer();

      void trace(cv::Mat const &image,
                 std::vector<std::shared_ptr<vision::PixelLabel>> const &labels,
                 vision::PixelLabeller const &labeller);

      std::vector<FieldEdgeRun> const &getRuns() const { return d_runs; }

    private:
      config::Setting<int> *d_pixelsBelowEdge;
      std::vector<FieldEdgeRun> d_runs;
    };
  }
}

