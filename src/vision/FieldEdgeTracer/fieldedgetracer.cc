// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fieldedgetracer.hh"

#include "vision/PixelLabel/pixellabel.hh"
#include "vision/ImageLabeller/PixelLabeller/pixellabeller.hh"
#include "state/State/state.hh"
#include "state/StateObject/FieldEdgeState/fieldedgestate.hh"
#include "config/Config/config.hh"
#include "util/Log/log.hh"

using namespace std;
using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::vision;
using namespace bold::util;
using namespace Eigen;

FieldEdgeTracer::FieldEdgeTracer() {
  d_pixelsBelowEdge = Config::getSetting<int>("field-edge-tracer.pixels-below-edge");
}

void FieldEdgeTracer::trace(cv::Mat const &image,
                            vector<shared_ptr<PixelLabel>> const &labels,
                            PixelLabeller const &labeller) {
  auto fieldEdgeState = State::get<FieldEdgeState>();
  if (!fieldEdgeState)
    return;

  auto const &fieldEdge = fieldEdgeState->getEdge();

  d_runs.clear();

  auto labelAt = [&](unsigned x, unsigned y) -> shared_ptr<PixelLabel> {
    Vector2i p(x, y);

    auto pp = image.ptr<uint8_t>(p.y()) + 3 * p.x();
    auto yuvColour = colour::YCbCr{pp[0], pp[1], pp[2]};
    return labeller.labelPixel(yuvColour);
  };

  FieldEdgeRun curRun;
  curRun.start = curRun.end = Vector2i(0, fieldEdge.getYAt(0));
  curRun.label = labelAt(0, curRun.start.y());

  auto pixelsBelow = d_pixelsBelowEdge->getValue();

  for (uint16_t i = 1; i < static_cast<uint16_t>(image.cols); ++i) {
    auto y = std::max(0, fieldEdge.getYAt(i) - pixelsBelow);
    Vector2i p(i, y);
    auto label = labelAt(p.x(), p.y());

    if (label != curRun.label) {
      d_runs.push_back(curRun);
      curRun = FieldEdgeRun{};
      curRun.start = curRun.end = p;
      curRun.label = label;
    } else
      curRun.end = p;
  }
  d_runs.push_back(curRun);
}
