// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <numeric>
#include "ballfinder.hh"

#include "vision/PixelLabel/pixellabel.hh"
#include "vision/ImageLabeller/PixelLabeller/pixellabeller.hh"

using namespace bold::vision;
using namespace Eigen;
using namespace std;

vector<AlignedBox2i> BallFinder::find(cv::Mat image, Blob::Vector const& seeds, shared_ptr<PixelLabel> fieldLabel, PixelLabeller const& labeller)
{
  auto labelAt = [&](Vector2i const& p) -> shared_ptr<PixelLabel const>
    {
      auto pp = image.ptr<uint8_t>(p.y()) + 3 * p.x();
      auto yuvColour = colour::YCbCr{pp[0], pp[1], pp[2]};
      return labeller.labelPixel(yuvColour);
    };

  auto boxes = vector<AlignedBox2i>();
  
  // For each blob find nearest field pixels left, right, up and down
  for (auto const& seedBlob : seeds)
  {
    Vector2i startPoint = seedBlob.bounds().center();
    
    Vector2i left = startPoint;
    Vector2i right = startPoint;
    Vector2i up = startPoint;
    Vector2i down = startPoint;
    
    while (left.x() > 0)
    {
      --left.x();
      auto label = labelAt(left);
      if (label == fieldLabel)
        break;
    }

    while (right.x() < image.cols - 1)
    {
      ++right.x();
      auto label = labelAt(right);
      if (label == fieldLabel)
        break;
    }
    
    while (down.y() > 0)
    {
      --down.y();
      auto label = labelAt(down);
      if (label == fieldLabel)
        break;
    }
    
    while (up.y() < image.rows - 1)
    {
      ++up.y();
      auto label = labelAt(up);
      if (label == fieldLabel)
        break;
    }

    boxes.push_back(AlignedBox2i(Vector2i(left.x(), down.y()), Vector2i(right.x(), up.y())));
  }

  auto unionBoxes = [](vector<AlignedBox2i> const& mergeTargets, AlignedBox2i const& box) {
                      auto merged = vector<AlignedBox2i>{};
                      auto newBox = box;

                      for (auto const& targetBox : mergeTargets)
                      {
                        if (newBox.intersects(targetBox))
                          newBox = newBox.merged(targetBox);
                        else
                          merged.push_back(targetBox);
                      }
                      merged.push_back(newBox);
                      return merged;
                    };

  return std::accumulate(boxes.begin(), boxes.end(), vector<AlignedBox2i>{}, unionBoxes);
}
