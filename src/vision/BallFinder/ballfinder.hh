// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Geometry>
#include <opencv2/core/core.hpp>
#include <memory>

#include "vision/Blob/blob.hh"

namespace bold
{
  namespace vision
  {
    class PixelLabel;
    class PixelLabeller;
    
    class BallFinder
    {
    public:
      std::vector<Eigen::AlignedBox2i> find(cv::Mat image, Blob::Vector const& seeds, std::shared_ptr<PixelLabel> fieldLabel,
                                            PixelLabeller const& labeller);
    };
  }
}
