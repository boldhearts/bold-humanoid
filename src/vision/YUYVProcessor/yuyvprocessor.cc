// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "yuyvprocessor.hh"

#include "util/assert.hh"
#include "util/Log/log.hh"

using namespace bold::vision;
using namespace bold::util;

YUYVProcessor::YUYVProcessor(bool squash)
  : d_squash{squash}
{}

void YUYVProcessor::process(Span<uint8_t> &out, Span<uint8_t> const& in)
{
  Log::verbose("YUYVProcessor") << "In length: " << in.length() << ", out length: " << out.length();
  
  // Convert each set of four input values into either one or two pixels
  if (d_squash)
  {
    ASSERT( in.length() / 4 * 3 >= out.length() );
    auto inCursor = in.begin();
    auto outCursor = out.begin();
    
    for (; outCursor != out.end();
         std::advance(inCursor, 4), std::advance(outCursor, 3))
    {
      int y1 = inCursor[0];
      int y2 = inCursor[2];
      uint8_t cb = inCursor[1];
      uint8_t cr = inCursor[3];

      outCursor[0] = (y1 + y2) / 2;
      outCursor[1] = cb;
      outCursor[2] = cr;
    }
  }
  else
  {
    ASSERT( in.length() / 4 * 6 >= out.length() );

    auto inCursor = in.begin();
    auto outCursor = out.begin();
    
    for (; outCursor != out.end();
         std::advance(inCursor, 4), std::advance(outCursor, 6))
    {
      uint8_t y1 = inCursor[0];
      uint8_t y2 = inCursor[2];
      uint8_t cb = inCursor[1];
      uint8_t cr = inCursor[3];

      outCursor[0] = y1;
      outCursor[1] = cb;
      outCursor[2] = cr;

      outCursor[3] = y2;
      outCursor[4] = cb;
      outCursor[5] = cr;
    }
  }
}
