// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/Span/span.hh"

namespace bold
{
  namespace vision
  {
    /** Processor to take 4 byte YUYV data and turn it into 3 channel images
     */
    class YUYVProcessor
    {
    public:
      YUYVProcessor(bool squash = false);
      
      void process(util::Span<uint8_t>& out, util::Span<uint8_t> const& in);

    private:
      bool d_squash;
    };
  }
}
