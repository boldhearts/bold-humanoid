// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "blobmerger.hh"

using namespace bold::vision;

BlobMerger::BlobMerger()
{
}

Blob::Vector BlobMerger::operator()(const Blob::Vector &mergeTargets, const bold::vision::Blob &blob) const
{
  auto merged = Blob::Vector{};
  auto newBlob = blob;
  for (auto const& target : mergeTargets)
  {
    if (std::any_of(d_predicates.begin(), d_predicates.end(),
                    [&](Predicate const& pred) {
                      return pred(newBlob, target);
                    }))
      newBlob.merge(target);
    else
      merged.push_back(target);
  }
  merged.push_back(newBlob);
  return merged;
}
