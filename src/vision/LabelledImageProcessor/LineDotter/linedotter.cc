// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "linedotter.hh"

#include "config/Config/config.hh"
#include "util/SequentialTimer/sequentialtimer.hh"

#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/PixelLabel/pixellabel.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

LineDotter::LineDotter(uint16_t imageWidth, shared_ptr<PixelLabel> inLabel, shared_ptr<PixelLabel> onLabel)
  : LabelledImageProcessor{"LineDotPass"},
    lineDots{},
    d_imageWidth{imageWidth},
    d_inLabel{move(inLabel)},
    d_onLabel{move(onLabel)},
    d_lastXGranularity{-1} {
  auto hysteresisLimit = Config::getSetting<int>("vision.line-detection.line-dots.hysteresis");

  // Create trackers

  d_colTrackers = vector<bold::LineRunTracker>();

  for (uint16_t x = 0; x <= imageWidth; ++x) {
    d_colTrackers.emplace_back(
      uint8_t(d_inLabel->getClass()), uint8_t(d_onLabel->getClass()), /*otherCoordinate*/x, hysteresisLimit->getValue(),
      [this](uint16_t const from, uint16_t const to, uint16_t const other) mutable {
        int mid = (from + to) / 2;
        lineDots.emplace_back((int) other, mid);
      }
    );
  }

  d_rowTracker = make_unique<LineRunTracker>(
    uint8_t(d_inLabel->getClass()), uint8_t(d_onLabel->getClass()), /*otherCoordinate*/0, hysteresisLimit->getValue(),
    [this](uint16_t const from, uint16_t const to, uint16_t const other) mutable {
      int mid = (from + to) / 2;
      lineDots.emplace_back(mid, (int) other);
    }
  );

  // Create controls

  hysteresisLimit->changed.connect([this](int const &value) mutable {
    d_rowTracker->setHysteresisLimit(value);

    for (LineRunTracker &colTracker : d_colTrackers)
      colTracker.setHysteresisLimit(value);
  });
}

void LineDotter::process(LabelledImageData const &labelData, SequentialTimer *timer) {
  // reset all run trackers
  d_rowTracker->reset();
  for (uint16_t x = 0; x < d_imageWidth; ++x)
    d_colTrackers[x].reset();
  lineDots.clear();
  if (timer != nullptr)
    timer->timeEvent("Clear");

  for (auto const &row : labelData) {
    d_rowTracker->reset();
    d_rowTracker->otherCoordinate = row.imageY;

    // *   *   *   *   *   *   *   *   *   *   *   *   * 4
    // *   *   *   *   *   *   *   *   *   *   *   *   * 4
    // ************************************************* 1
    // ************************************************* 1
    // * * * * * * * * * * * * * * * * * * * * * * * * * 2
    // * * * * * * * * * * * * * * * * * * * * * * * * * 2
    // *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  * 3
    // *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  * 3
    // *   *   *   *   *   *   *   *   *   *   *   *   * 4
    // *   *   *   *   *   *   *   *   *   *   *   *   * 4

    if (d_lastXGranularity != row.granularity.x()) {
      // We've transitioned between x-granularities and need to reset columns
      // for which we have just stopped observing pixels, as otherwise they
      // will carry incorrect state across vertical regions of the image.

      // If this isn't the first row of the image...
      if (d_lastXGranularity != -1) {
        // Iterate through at the previous granularity. Any column which is
        // not a multiple of the new granularity must be reset.
        for (uint16_t x = 0; x < d_imageWidth; x += d_lastXGranularity) {
          if (x % row.granularity.x() != 0)
            d_colTrackers[x].reset();
        }
      }

      d_lastXGranularity = row.granularity.x();
    }

    uint16_t x = 0;

    for (auto const &label : row) {
      d_rowTracker->update(label, x);
      d_colTrackers[x].update(label, row.imageY);
      x += row.granularity.x();
    }
  }
}
