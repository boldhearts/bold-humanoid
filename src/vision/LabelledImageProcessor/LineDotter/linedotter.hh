// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"
#include "LineRunTracker/lineruntracker.hh"

#include <Eigen/Core>

#include <memory>
#include <vector>

namespace bold {
  namespace vision {

    class PixelLabel;
    class LabelledImageData;

    /** Finds dots along lines
     *
     * Searches for transitions between field and line pixels, and puts a dot in the middle of such transitions
     */
    class LineDotter : public LabelledImageProcessor {
    public:
      LineDotter(uint16_t imageWidth, std::shared_ptr<vision::PixelLabel> inLabel,
                 std::shared_ptr<vision::PixelLabel> onLabel);

      void process(LabelledImageData const &labelData, util::SequentialTimer* timer) override;

      std::vector<Eigen::Vector2i> lineDots;

    private:
      const uint16_t d_imageWidth;
      std::shared_ptr<vision::PixelLabel> d_inLabel;
      std::shared_ptr<vision::PixelLabel> d_onLabel;
      std::unique_ptr<LineRunTracker> d_rowTracker;
      std::vector<bold::LineRunTracker> d_colTrackers;
      int d_lastXGranularity;
    };
  }
}
