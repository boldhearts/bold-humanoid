// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/SequentialTimer/sequentialtimer.hh"

#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/PixelLabel/pixellabel.hh"

#include <algorithm>
#include <vector>


namespace bold {
  namespace vision {
    /**
     * Builds a histogram of field pixels per image row.
     */
    class FieldHistogramBuilder : public LabelledImageProcessor {
    public:
      FieldHistogramBuilder(std::shared_ptr<vision::PixelLabel> const &fieldLabel, uint16_t height)
        : LabelledImageProcessor{"FieldHistogramPass"},
          d_fieldLabelId{uint8_t(fieldLabel->getClass())},
          d_rowWidths{height},
          d_cumulativePixelCounts(height),
          d_cumulativePixelCount{0} {}

      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override {
        std::fill(d_rowWidths.begin(), d_rowWidths.end(), (uint16_t) 0);
        std::fill(d_cumulativePixelCounts.begin(), d_cumulativePixelCounts.end(), (uint) 0);
        if (timer != nullptr)
          timer->timeEvent("Clear");

        unsigned cumulativePixelCount = 0;

        for (auto const &row : labelData) {
          const uint16_t y = row.imageY;
          uint16_t &countForRow = d_rowWidths[y];

          for (auto const &label : row) {
            if (label == d_fieldLabelId)
              countForRow++;
          }

          d_rowWidths[y] *= row.granularity.x();
          cumulativePixelCount += d_rowWidths[y] * row.granularity.y();
          d_cumulativePixelCounts[y] = cumulativePixelCount;
        }
        if (timer != nullptr)
          timer->timeEvent("Process Rows");

        d_cumulativePixelCount = cumulativePixelCount;
      }

      uint16_t getRowWidth(uint16_t y) const {
        return d_rowWidths[y];
      }

      double getRatioBeneath(uint16_t y) const {
        return d_cumulativePixelCount == 0
               ? 0.0
               : (double) d_cumulativePixelCounts[y] / d_cumulativePixelCount;
      }

    private:
      const uint8_t d_fieldLabelId;
      std::vector<uint16_t> d_rowWidths;
      std::vector<uint> d_cumulativePixelCounts;
      unsigned d_cumulativePixelCount;
    };
  }
}
