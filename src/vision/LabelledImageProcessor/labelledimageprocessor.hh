// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>

namespace bold
{
  namespace util {
    class SequentialTimer;
  }

  namespace vision
  {
    class LabelledImageData;

    /**
     * Abstract base class for classes that process labelled pixel data.
     */
    class LabelledImageProcessor
    {
    protected:
      explicit LabelledImageProcessor(std::string id)
        : d_id{std::move(id)}
      {}
    
    public:
      virtual ~LabelledImageProcessor() = default;

      virtual void process(LabelledImageData const& labelledImageData, util::SequentialTimer* timer) = 0;

      std::string const& id() const { return d_id; };

    private:
      std::string d_id;
    };
  }
}
