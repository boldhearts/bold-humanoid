// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "blobdetector.hh"

#include "vision/LabelledImageData/labelledimagedata.hh"

using namespace bold;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

BlobDetector::BlobDetector(uint16_t imageWidth, uint16_t imageHeight,
                           vector<shared_ptr<PixelLabel>> pixelLabels)
  : LabelledImageProcessor{"BlobDetectPass"},
    d_imageHeight{imageHeight},
    d_imageWidth{imageWidth},
    d_pixelLabels{move(pixelLabels)},
    d_runsPerRowPerLabel{} {
  // Create a run length code for each label
  for (auto const &pixelLabel : d_pixelLabels) {
    auto pixelLabelId = static_cast<uint8_t>(pixelLabel->getClass());

    // A RunLengthCode is a vector of vectors of runs
    d_runsPerRowPerLabel[pixelLabelId] = RunLengthCode();

    // Initialise a vector of Runs for each row in the image
    for (unsigned y = 0; y < d_imageHeight; ++y)
      d_runsPerRowPerLabel[pixelLabelId].emplace_back();

    // Initialize blob container
    d_blobsDetectedPerLabel[pixelLabel] = Blob::Vector{};
  }
}

void BlobDetector::process(LabelledImageData const &labelData, SequentialTimer *timer) {
  // Clear all persistent data
  for (auto &pair : d_runsPerRowPerLabel)
    for (std::vector<vision::Run> &runs : pair.second)
      runs.clear();

  // TODO size of this vector is known at this point -- avoid reallocation
  d_rowIndices.clear();

  if (timer != nullptr)
    timer->timeEvent("Clear");

  if (labelData.getLabelledRowCount() == 0)
    return;

  for (auto const &row : labelData) {
    auto currentStartX = uint16_t{0};
    auto currentY = row.imageY;
    auto currentLabel = uint8_t{0};

    d_rowIndices.push_back(row.imageY);

    auto x = uint16_t{0};
    for (auto const &label : row) {
      // Check if we have a run boundary
      if (label != currentLabel) {
        // Check whether this is the end of the current run
        if (currentLabel != 0) {
          // Finished run
          auto endX = uint16_t(x - 1);
          addRun(currentStartX, endX, currentY, currentLabel);
        }

        // Check whether this is the start of a new run, and start it
        if (label != 0)
          currentStartX = x;

        currentLabel = label;
      }
      x += row.granularity.x();
    }

    if (currentLabel != 0) {
      // finish whatever run we were on
      auto endX = uint16_t(d_imageWidth - 1);
      addRun(currentStartX, endX, currentY, currentLabel);
    }
  }
  if (timer != nullptr)
    timer->timeEvent("Process Rows");
}

map<shared_ptr<PixelLabel>, Blob::Vector> const &BlobDetector::detectBlobs(SequentialTimer &timer) {
  // Return early if we haven't processed a whole image yet
  if (d_rowIndices.size() < 2)
    return d_blobsDetectedPerLabel;

  // For each label that we're configured to look at
  for (const auto &pixelLabel : d_pixelLabels) {
    timer.enter(pixelLabel->getName());
    auto pixelLabelId = static_cast<uint8_t>(pixelLabel->getClass());

    // Go through all runs and add them to the disjoint set

    // RunSets; one set of runSets for each label, each blob is a set of runs

    RunLengthCode &runsPerRow = d_runsPerRowPerLabel[pixelLabelId];

    DisjointSet<Run> rSet;

    // Just insert all runs of first (bottom) row
    for (Run const &run : runsPerRow[d_rowIndices[0]])
      rSet.insert(run);

    // From the second row on...
    for (unsigned i = 1; i < d_rowIndices.size(); ++i) {
      unsigned y = d_rowIndices[i];

      for (Run &run : runsPerRow[y]) {
        rSet.insert(run);

        // Attempt to merge this run with runs in the row above
        for (Run &run2 : runsPerRow[d_rowIndices[i - 1]])
          if (run.overlaps(run2))
            rSet.merge(run, run2);
      }
    }

    timer.timeEvent("Build Run Set");

    auto runSets = rSet.getSubSets();

    timer.timeEvent("Get SubSets");

    auto &blobSet = d_blobsDetectedPerLabel[pixelLabel];
    blobSet.clear();

    // Convert sets of run-sets to sets of blob
    transform(runSets.begin(), runSets.end(),
              inserter(blobSet, blobSet.end()),
              [](set<Run> const &runSet) { return Blob{runSet}; });

    timer.timeEvent("Convert");

    timer.exit();
  }

  return d_blobsDetectedPerLabel;
}

void BlobDetector::clear() {
  // Clear all persistent data
  for (auto &pair : d_runsPerRowPerLabel)
    for (std::vector<vision::Run> &runs : pair.second)
      runs.clear();
  d_rowIndices.clear();
  for (auto &pair : d_blobsDetectedPerLabel) {
    auto &blobs = pair.second;
    blobs.clear();
  }
}

void BlobDetector::addRun(uint16_t startX, uint16_t endX, uint16_t y, uint8_t label) {
  auto it = d_runsPerRowPerLabel.find(label);
  if (it != d_runsPerRowPerLabel.end())
    it->second[y].emplace_back(startX, endX, y);
}
