// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>
#include <Eigen/Core>

#include <algorithm>
#include <memory>
#include <set>
#include <vector>

#include "DisjointSet/disjointset.hh"

#include "util/assert.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/Blob/blob.hh"
#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"
#include "vision/PixelLabel/pixellabel.hh"

namespace bold {
  namespace util {
    class SequentialTimer;
  }

  namespace vision {
    /** Blob detection labelled image processor
     *
     * Builds blobs while passing through a labelled image
     **/
    class BlobDetector : public LabelledImageProcessor {
    public:
      BlobDetector(uint16_t imageWidth, uint16_t imageHeight,
                   std::vector<std::shared_ptr<vision::PixelLabel>> blobTypes);

      /// Reset any persistent data from previous runs
      void clear();

      /// Get labels that this will detect blobs for
      std::vector<std::shared_ptr<vision::PixelLabel>> const &pixelLabels() const { return d_pixelLabels; }

      /** Perform initial run length encoding
       *
       * Call @a detectBlobs to turn encoding into actual blobs.
       */
      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override;

      /** Processes Runs into Blobs
       *
       * Detected blobs are stored and can be retrieved again using @a getDetectedBlobs
       *
       * @returns A set of blobs per label
       */
      std::map<std::shared_ptr<vision::PixelLabel>, vision::Blob::Vector> const &
      detectBlobs(util::SequentialTimer &timer);

      /** Get blobs detected in last pass
       */
      std::map<std::shared_ptr<vision::PixelLabel>, vision::Blob::Vector> const &
      getDetectedBlobs() const { return d_blobsDetectedPerLabel; }

    private:
      typedef std::vector<std::vector<vision::Run>> RunLengthCode;

      void addRun(uint16_t startX, uint16_t endX, uint16_t y, uint8_t label);

      uint16_t d_imageHeight;
      uint16_t d_imageWidth;

      std::vector<std::shared_ptr<vision::PixelLabel>> d_pixelLabels;
      std::vector<uint16_t> d_rowIndices;

      // Image pass state Accumulated data for the most recently passed image.
      std::map<uint8_t, RunLengthCode> d_runsPerRowPerLabel;

      // Blobs detected
      std::map<std::shared_ptr<vision::PixelLabel>, vision::Blob::Vector> d_blobsDetectedPerLabel;
    };
  }
}
