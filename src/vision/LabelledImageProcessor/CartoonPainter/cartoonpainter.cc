// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cartoonpainter.hh"

#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"

using namespace bold;
using namespace bold::util;
using namespace bold::vision;

void CartoonPainter::process(LabelledImageData const &labelData, SequentialTimer *timer) {
  d_mat = cv::Scalar(0);
  if (timer != nullptr)
    timer->timeEvent("Clear");

  for (auto const &row : labelData) {
    auto *ptr = d_mat.ptr<uint8_t>(row.imageY);
    auto dx = row.granularity.x();
    for (auto const &label : row) {
      // NOTE Have tested whether it was better to check if value is zero here, but it runs the
      //      pass 0.3ms slower on average with the if-check here, so skip it. The write pattern
      //      here is sequential anyway. If this becomes random access, it may help.
      // TODO: Test whether this is stil the case for Odroid, which does have branch prediction
      *ptr = label;
      ptr += dx;
    }
  }

  if (timer != nullptr)
    timer->timeEvent("Process Rows");
}
