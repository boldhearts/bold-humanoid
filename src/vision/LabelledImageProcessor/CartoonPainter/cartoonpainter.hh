// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>

#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"

namespace bold {
  namespace vision {

    /** Builds a cv::Mat of labelled pixels, resembling a cartoon
     */
    class CartoonPainter : public LabelledImageProcessor {
    public:
      CartoonPainter(uint16_t width, uint16_t height)
        : LabelledImageProcessor{"CartoonPass"},
          d_mat{height, width, CV_8UC1} {}

      /** Gets the resulting cartoon image
       */
      cv::Mat const &mat() { return d_mat; }

      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override;

    private:
      cv::Mat d_mat;
    };

  }
}
