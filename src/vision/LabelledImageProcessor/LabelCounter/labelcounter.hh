// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"

#include <memory>
#include <vector>

namespace bold {
  namespace vision {
    class PixelLabel;

    /** Counts the number of each label value seen in an image.
     */
    class LabelCounter : public LabelledImageProcessor {
    public:
      // assuming we'll never have more than 7 labels (1-8)
      static const uint8_t MAX_LABEL_COUNT = 8;

      explicit LabelCounter(std::vector<std::shared_ptr<vision::PixelLabel>> labels);

      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override;

    private:
      unsigned d_countByLabelId[MAX_LABEL_COUNT];
      std::vector<std::shared_ptr<vision::PixelLabel>> d_labels;
    };
  }
}
