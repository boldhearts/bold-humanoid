// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "labelcounter.hh"

#include "util/SequentialTimer/sequentialtimer.hh"
#include "state/State/state.hh"
#include "state/StateObject/LabelCountState/labelcountstate.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

LabelCounter::LabelCounter(std::vector<std::shared_ptr<PixelLabel>> labels)
  : LabelledImageProcessor{"LabelCountPass"},
    d_countByLabelId{},
    d_labels{move(labels)} {}

void LabelCounter::process(LabelledImageData const &labelData, SequentialTimer *timer) {
  for (const auto &label : d_labels) {
    ASSERT(uint8_t(label->getClass()) < MAX_LABEL_COUNT);
    d_countByLabelId[uint8_t(label->getClass())] = 0;
  }
  if (timer != nullptr)
    timer->timeEvent("Clear");

  for (auto const &row : labelData) {
    for (auto const &label : row) {
      if (label != 0)
        d_countByLabelId[label]++;
    }
  }

  if (timer != nullptr)
    timer->timeEvent("Process Rows");

  std::map<std::shared_ptr<PixelLabel>, uint> counts;

  for (const auto &label : d_labels)
    counts[label] = d_countByLabelId[uint8_t(label->getClass())];

  State::make<LabelCountState>(counts);
}
