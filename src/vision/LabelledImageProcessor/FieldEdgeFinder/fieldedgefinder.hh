// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>

#include "OcclusionRay/occlusionray.hh"
#include "state/StateObject/FieldEdgeState/fieldedgestate.hh"

#include "vision/LabelledImageProcessor/labelledimageprocessor.hh"

namespace bold {
  namespace config {
    template<typename T>
    class Setting;
  }

  namespace vision {
    /** Abstract processor for finding he field edge in a labelled image
     */
    class FieldEdgeFinder : public LabelledImageProcessor {
    public:
      FieldEdgeFinder(std::string id, uint16_t imageWidth, uint16_t imageHeight);

      ~FieldEdgeFinder() override = default;

      /** Get state object containing field edge data
       */
      virtual std::shared_ptr<state::FieldEdgeState const> getFieldEdgeState() const = 0;

      /** Get information on where field edge is occluded
       *
       * This represents where the field edge is inside of its convex
       * hull, i.e. where something in the field is occluding it.
       */
      virtual std::vector<OcclusionRay<uint16_t>> getOcclusionRays() const = 0;

    protected:
      /** Function that subclasses can use to find the convex hull of a set of points
       *
       * @param points Set of points including the ones to determine convex hull of. Will be overwritten with points from convex hull
       * @param fromIndex Index of first point in @a points to use
       * @param toIndex Index of last point in @a points to use (inclusive)
       */
      static void applyConvexHull(std::vector<uint16_t> &points, unsigned fromIndex, unsigned toIndex);

      config::Setting<bool> *d_useConvexHull;
      uint16_t d_imageWidth;
      uint16_t d_imageHeight;
      uint16_t d_minVerticalRunLength;
    };
  }
}
