// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "completefieldedgefinder.hh"

#include "config/Config/config.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "stats/movingaverage.hh"
#include "vision/PixelLabel/pixellabel.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace Eigen;
using namespace std;

CompleteFieldEdgeFinder::CompleteFieldEdgeFinder(PixelLabel const &fieldLabel,
                                                 uint16_t pixelWidth, uint16_t pixelHeight)
  : FieldEdgeFinder{"CompleteFieldEdgePass", pixelWidth, pixelHeight},
    d_fieldLabelId{uint8_t(fieldLabel.getClass())},
    d_maxYByX(pixelWidth),
    d_runByX(pixelWidth) {
  Config::getSetting<int>("vision.field-edge-pass.complete.smoothing-window-length")->track(
    [this](int value) { d_smoothingWindowSize = (uint16_t) value; });
}

void CompleteFieldEdgeFinder::process(LabelledImageData const &labelData, SequentialTimer *timer) {
  std::fill(d_maxYByX.begin(), d_maxYByX.end(), d_imageHeight - 1);
  std::fill(d_runByX.begin(), d_runByX.end(), 0);
  if (timer != nullptr)
    timer->timeEvent("Clear");

  for (auto const &row : labelData) {
    uint16_t x = 0;
    for (auto const &label : row) {
//   ASSERT(x >= 0 && x < d_imageWidth);

      if (label == d_fieldLabelId) {
//     ASSERT(y >= d_maxYByX[x]);

        uint16_t run = d_runByX[x];
        run++;

        if (run >= d_minVerticalRunLength)
          d_maxYByX[x] = row.imageY;

        d_runByX[x] = run;
      } else {
        d_runByX[x] = 0;
      }

      x += row.granularity.x();
    }
  }
  if (timer != nullptr)
    timer->timeEvent("Process Rows");

  if (d_smoothingWindowSize > 1) {
    MovingAverage<int> avg(d_smoothingWindowSize);

    int offset = int(d_smoothingWindowSize) / 2;
    for (int x = 0, t = -offset; x < d_imageWidth; x++, t++) {
      auto smoothedY = avg.next(d_maxYByX[x]);
      if (t >= 0 && smoothedY > d_maxYByX[t]) {
        d_maxYByX[x] = smoothedY;
      }
    }

    if (timer != nullptr)
      timer->timeEvent("Smooth");
  }

  // Create convex hull values
  std::copy(d_maxYByX.begin(), d_maxYByX.end(), d_maxYByXConvex.begin());
  applyConvexHull(d_maxYByXConvex, 0, d_imageWidth - 1);
  if (timer != nullptr)
    timer->timeEvent("Convex Hull");
}

shared_ptr<FieldEdgeState const> CompleteFieldEdgeFinder::getFieldEdgeState() const {
  return make_shared<FieldEdgeState const>(make_unique<CompleteFieldEdge>(d_maxYByX));
}

vector<OcclusionRay<uint16_t>> CompleteFieldEdgeFinder::getOcclusionRays() const {
  vector<OcclusionRay<uint16_t>> rays;

  for (uint16_t x = 0; x < d_imageWidth; x++)
    rays.emplace_back(
      Matrix<uint16_t, 2, 1>(x, d_maxYByX[x]),
      Matrix<uint16_t, 2, 1>(x, d_maxYByXConvex[x]));

  return rays;
}

///
/// CompleteFieldEdge
///

CompleteFieldEdgeFinder::CompleteFieldEdge::CompleteFieldEdge(vector<uint16_t> maxYByX)
  : d_maxYByX{move(maxYByX)} {}

uint16_t CompleteFieldEdgeFinder::CompleteFieldEdge::getYAt(uint16_t x) const {
  return d_maxYByX[x];
}
