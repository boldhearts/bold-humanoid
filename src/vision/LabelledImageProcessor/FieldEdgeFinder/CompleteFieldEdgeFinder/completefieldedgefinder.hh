// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>

#include "vision/LabelledImageProcessor/FieldEdgeFinder/fieldedgefinder.hh"

namespace bold {
  namespace vision {
    class PixelLabel;

    class CompleteFieldEdgeFinder : public FieldEdgeFinder {
    public:
      CompleteFieldEdgeFinder(vision::PixelLabel const &fieldLabel,
                              uint16_t pixelWidth, uint16_t pixelHeight);

      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override;

      std::shared_ptr<state::FieldEdgeState const> getFieldEdgeState() const override;

      std::vector<OcclusionRay<uint16_t>> getOcclusionRays() const override;

    private:
      class CompleteFieldEdge : public state::FieldEdge {
      public:
        explicit CompleteFieldEdge(std::vector<uint16_t> maxYByX);

        uint16_t getYAt(uint16_t x) const override;

      private:
        std::vector<uint16_t> d_maxYByX;
      };

      uint8_t d_fieldLabelId;
      std::vector<uint16_t> d_maxYByX;
      std::vector<uint16_t> d_maxYByXConvex;
      std::vector<uint16_t> d_runByX;
      uint16_t d_smoothingWindowSize;
    };
  }
}
