// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <utility>

#include "fieldedgefinder.hh"

#include "config/Config/config.hh"
#include "geometry2/Operator/ConvexHull/andrewconvexhull.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::vision;
using namespace Eigen;
using namespace std;

FieldEdgeFinder::FieldEdgeFinder(string id, uint16_t imageWidth, uint16_t imageHeight)
  : LabelledImageProcessor{std::move(id)},
    d_imageWidth{imageWidth},
    d_imageHeight{imageHeight} {
  Config::getSetting<int>("vision.field-edge-pass.min-vertical-run-length")->track(
    [this](int value) { d_minVerticalRunLength = (uint16_t) value; });
  d_useConvexHull = Config::getSetting<bool>("vision.field-edge-pass.use-convex-hull");
}

void FieldEdgeFinder::applyConvexHull(vector<uint16_t> &points, unsigned fromIndex, unsigned toIndex) {
  ASSERT(toIndex < points.size());

  auto coords = Point2i::Vector{};
  for (unsigned c = fromIndex; c <= toIndex; ++c)
    coords.emplace_back(c, points[c]);

  // Build the convex hull
  auto convexHull = AndrewConvexHull<int>{};
  auto hullBoundary = convexHull(MultiPoint2i{coords});

  // Find the topmost points of the hull
  auto hullTopCoords = Point2i::Vector{hullBoundary.coords().size() - convexHull.getLowerUpperPivot()};
  copy(std::next(hullBoundary.coords().begin(), convexHull.getLowerUpperPivot()),
       hullBoundary.coords().end(),
       hullTopCoords.begin());

  // Hull is CCW, so top is from right to left; reverse to have left to right
  // TODO we can prevent this by inserting them in reverse above
  reverse(begin(hullTopCoords), end(hullTopCoords));

  // The convex hull output has fewer columns than the input.
  // Walk through both the columnar data and the hull output,
  // filling any missing column values with interpolations.

  unsigned outputIndex = 0;
  unsigned lastMatchedColumn = fromIndex;

  for (unsigned c = fromIndex; c <= toIndex; c++) {
    if (unsigned(hullTopCoords[outputIndex].x()) == c)
    {
      // This column's value is unchanged by the hull operation.
      // Its value is part of the hull

      for (unsigned fillC = lastMatchedColumn + 1; fillC < c; fillC++) {
        double ratio = 1.0 - (double) (c - fillC) / (c - lastMatchedColumn);
        points[fillC] = Math::lerp(ratio, points[lastMatchedColumn], points[c]);
      }

      lastMatchedColumn = c;
      outputIndex++;
    }
  }
}
