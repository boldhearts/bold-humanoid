// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "periodicfieldedgefinder.hh"

#include "config/Config/config.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/PixelLabel/pixellabel.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace Eigen;
using namespace std;

PeriodicFieldEdgeFinder::PeriodicFieldEdgeFinder(PixelLabel const &fieldLabel,
                                                 PixelLabel const &lineLabel,
                                                 uint16_t imageWidth, uint16_t imageHeight,
                                                 uint16_t period)
  : FieldEdgeFinder{"PeriodicFieldEdgeFinder", imageWidth, imageHeight},
    d_fieldLabelId{uint8_t(fieldLabel.getClass())},
    d_lineLabelId{uint8_t(lineLabel.getClass())},
    d_maxYByC(static_cast<size_t>(imageWidth / period + 1)),
    d_maxYByCConvex(static_cast<size_t>(imageWidth / period + 1)),
    d_runByC(static_cast<size_t>(imageWidth / period + 1)),
    d_period{period} {}

void PeriodicFieldEdgeFinder::process(vision::LabelledImageData const &labelData, SequentialTimer *timer) {
  auto unset = numeric_limits<uint16_t>::max();
  std::fill(d_maxYByC.begin(), d_maxYByC.end(), unset);
  std::fill(d_runByC.begin(), d_runByC.end(), 0);
  if (timer != nullptr)
    timer->timeEvent("Clear");

  const uint8_t lineLabelId = d_lineLabelId;
  const uint8_t fieldLabelId = d_fieldLabelId;

  for (auto const &row : labelData) {
    uint16_t c = 0;
    uint16_t step = d_period / row.granularity.x();
    for (auto label = row.begin(); label < row.end(); label += step) {
      if (*label == lineLabelId) {
        // Do nothing! Line may still be within field.
        // We don't increase the score however, and still reset at the first non-field/line pixel.
      } else if (*label == fieldLabelId) {
//      ASSERT(y >= d_maxYByX[c]);

        uint16_t run = d_runByC[c];
        run++;

        if (run >= d_minVerticalRunLength)
          d_maxYByC[c] = row.imageY;

        d_runByC[c] = run;
      } else {
        d_runByC[c] = 0;
      }

      c++;
    }
  }
  if (timer != nullptr)
    timer->timeEvent("Process Rows");

  // d_maxYByC is initialised with `unset` in all positions.
  //
  // If we didn't observe a single column with enough green, then we will set
  // the entire line at of top the screen (when image is the right way up.)
  //
  // If we did see some good columns, take any that remain at `unset` and set them
  // to the bottom of the screen.
  //
  // This stops the line jumping from the bottom to the top, which can happen
  // when the bot is near the side of the field and looking down its length.
  // The convex hull makes this issue much worse.

  bool allUnset = true;
  bool anyUnset = false;

  // If the edges are untouched (`unset`) we exclude them from the convex hull.
  // These variables track where the convex hull should start and end.
  auto fromIndex = 0U;
  auto toIndex = unsigned(d_maxYByC.size() - 1);

  for (uint16_t c = 0; c < d_maxYByC.size(); c++) {
    auto y = d_maxYByC[c];

    if (y == unset) {
      anyUnset = true;

      if (allUnset)
        fromIndex = c + 1;
    } else {
      allUnset = false;
      toIndex = c;
    }
  }

  if (allUnset) {
    // TODO control this via a setting, and disable by default -- if looking off the field, don't start chasing rubbish

    // set line at top of image
    for (uint16_t c = 0; c < d_runByC.size(); c++)
      d_maxYByC[c] = static_cast<uint16_t>(d_imageHeight - 1);

    // skip convex hull as line is straight
    return;
  } else if (anyUnset) {
    // set untouched columns to the bottom of the image
    for (uint16_t c = 0; c < d_runByC.size(); c++) {
      if (d_maxYByC[c] == unset)
        d_maxYByC[c] = 0;
    }
  }

  // Create convex hull values
  std::copy(d_maxYByC.begin(), d_maxYByC.end(), d_maxYByCConvex.begin());
  if (fromIndex < toIndex)
    applyConvexHull(d_maxYByCConvex, fromIndex, toIndex);

  if (timer != nullptr)
    timer->timeEvent("Convex Hull");
}

shared_ptr<FieldEdgeState const> PeriodicFieldEdgeFinder::getFieldEdgeState() const {
  return make_shared<FieldEdgeState const>(
    make_unique<PeriodicFieldEdge>(d_period, d_useConvexHull->getValue() ? d_maxYByCConvex : d_maxYByC, d_runByC)
  );
}

vector<OcclusionRay<uint16_t>> PeriodicFieldEdgeFinder::getOcclusionRays() const {
  vector<OcclusionRay<uint16_t>> rays;

  uint16_t x = 0;
  for (uint16_t c = 0; c < d_maxYByC.size(); c++) {
    rays.emplace_back(
      Matrix<uint16_t, 2, 1>(x, d_maxYByC[c]),
      Matrix<uint16_t, 2, 1>(x, d_maxYByCConvex[c]));

    x += d_period;
  }

  return rays;
}

///
/// PeriodicFieldEdge
///

PeriodicFieldEdgeFinder::PeriodicFieldEdge::PeriodicFieldEdge(uint16_t period, vector<uint16_t> maxYByC,
                                                              vector<uint16_t> runByC)
  : d_period{period},
    d_maxYByC{move(maxYByC)},
    d_runByC{move(runByC)} {}

uint16_t PeriodicFieldEdgeFinder::PeriodicFieldEdge::getYAt(uint16_t x) const {
  // Map from the x-position to the periodic samples.

  uint16_t rem = x % d_period;
  uint16_t c = x / d_period;

  if (rem == 0 || c == d_runByC.size() - 1)
    return d_maxYByC[c];

  // Pixels at the far edge of the image may be beyond the last sampled column.
  if (c == d_maxYByC.size())
    return d_maxYByC[c - 1];

  // Interpolate between the two closest samples.
  return Math::lerp((double) rem / d_period, d_maxYByC[c], d_maxYByC[c + 1]);
}

