// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>

#include "vision/LabelledImageProcessor/FieldEdgeFinder/fieldedgefinder.hh"

namespace bold {
  namespace vision {
    class PixelLabel;

    class PeriodicFieldEdgeFinder : public FieldEdgeFinder {
    public:
      PeriodicFieldEdgeFinder(vision::PixelLabel const &fieldLabel,
                              vision::PixelLabel const &lineLabel,
                              uint16_t imageWidth, uint16_t imageHeight,
                              uint16_t period);

      void process(LabelledImageData const &labelData, util::SequentialTimer *timer) override;

      std::shared_ptr<state::FieldEdgeState const> getFieldEdgeState() const override;

      std::vector<OcclusionRay<uint16_t>> getOcclusionRays() const override;

    private:
      class PeriodicFieldEdge : public state::FieldEdge {
      public:
        PeriodicFieldEdge(uint16_t period, std::vector<uint16_t> maxYByC, std::vector<uint16_t> runByC);

        uint16_t getYAt(uint16_t x) const override;

      private:
        uint16_t d_period;
        std::vector<uint16_t> d_maxYByC;
        std::vector<uint16_t> d_runByC;
      };

      uint8_t d_fieldLabelId;
      uint8_t d_lineLabelId;
      std::vector<uint16_t> d_maxYByC;
      std::vector<uint16_t> d_maxYByCConvex;
      std::vector<uint16_t> d_runByC;
      uint16_t d_period;
    };
  }
}
