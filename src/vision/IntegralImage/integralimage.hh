// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <type_traits>
#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <memory>

#include "geometry2/Point/point.hh"
#include "util/assert.hh"

namespace bold
{
  namespace vision
  {
    class LabelledImageData;
    enum class LabelClass : uint8_t;
    
    /** Models a integral image, also known as a summed area table.
     *
     * Used in some kinds of object recognition.
     *
     * See https://en.wikipedia.org/wiki/Integral_image
     */
    class IntegralImage
    {
    public:
      /** Creates an IntegralImage instance from the specified image.
       *
       * Linear time.
       *
       * @param inputImage Must be a CV_8UC1 image (8bpp)
       */
      static std::unique_ptr<IntegralImage> create(cv::Mat const& inputImage,
                                                   std::unique_ptr<IntegralImage> recycle = nullptr);

      static std::unique_ptr<IntegralImage> create(LabelledImageData const& labelData, LabelClass labelClass,
                                                   std::unique_ptr<IntegralImage> recycle = nullptr);
      
      IntegralImage() = default;

      explicit IntegralImage(cv::Mat data)
        : d_mat(data)
      {
        ASSERT(data.type() == CV_32SC1 && "Image must be of type CV_32SC1");
      }

      /** Lookup the sum of intensities between the min/max coordinates, inclusive.
       *
       * Constant time.
       */
      int getSummedArea(geometry2::Point2i min, geometry2::Point2i max) const;

      /** Return the sum of intensities from (0,0) to the specified coordinates, inclusive.
       */
      int at(unsigned y, unsigned x) const { return d_mat.at<int>(y, x); }

    private:
      cv::Mat d_mat;
    };
  }
}
