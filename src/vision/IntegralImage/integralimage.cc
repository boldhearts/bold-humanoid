// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "integralimage.hh"

#include "util/memory.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"

using namespace bold;
using namespace bold::geometry2;
using namespace bold::vision;
using namespace Eigen;
using namespace std;

unique_ptr<IntegralImage> IntegralImage::create(cv::Mat const &image, unique_ptr<IntegralImage> output) {
  ASSERT(image.rows > 0 && image.cols > 0 && "Must have at least one row and one column");
  ASSERT(image.type() == CV_8UC1 && "Image must be of type CV_8UC1");

  if (output == nullptr)
    output = make_unique<IntegralImage>();

  if (output->d_mat.size() != image.size())
    output->d_mat = cv::Mat{image.size(), CV_32SC1};

  // Process the first row
  auto const *source = image.template ptr<uint8_t const>(0);
  auto *target = output->d_mat.ptr<int>(0);
  auto *targetEnd = target + image.cols;
  auto sum = 0;
  while (target != targetEnd) {
    sum += *(source++);
    *(target++) = sum;
  }

  // Subsequent rows consider the row above as well
  for (int y = 1; y < image.rows; y++) {
    source = image.template ptr<uint8_t const>(y);
    target = output->d_mat.ptr<int>(y);
    auto const *above = output->d_mat.template ptr<int const>(y - 1);

    *target = *source + *above;
    targetEnd = target + image.cols - 1;
    while (target != targetEnd) {
      int leftValue = *(target++);
      int aboveLeftValue = *(above++);

      *target = *(source++) + leftValue + *above - aboveLeftValue;
    }
  }

  return output;
}

unique_ptr<IntegralImage> IntegralImage::create(LabelledImageData const &labelData, bold::vision::LabelClass labelClass,
                                                unique_ptr<IntegralImage> output) {
  auto imageWidth = labelData.getSampleMap().getImageWidth();
  auto imageHeight = labelData.getSampleMap().getImageHeight();

  if (output == nullptr)
    output = make_unique<IntegralImage>();

  auto labelledImageSize = cv::Size(imageWidth, imageHeight);

  if (output->d_mat.size() != labelledImageSize)
    output->d_mat = cv::Mat{labelledImageSize, CV_32SC1};

  // Process the first row
  auto firstLabelledRow = labelData.begin();
  int *target = output->d_mat.ptr<int>(0);
  int *targetEnd = target + imageWidth;
  int sum = 0;
  auto lastLabelledPixel = firstLabelledRow->begin();
  unsigned dxSinceLast = 0;
  auto stepX = firstLabelledRow->granularity.x();
  auto stepY = firstLabelledRow->granularity.y();
  while (target != targetEnd) {
    sum += (*lastLabelledPixel == uint8_t(labelClass) ? 1 : 0);
    *target = sum;
    ++target;
    ++dxSinceLast;
    if (dxSinceLast == stepX) {
      ++lastLabelledPixel;
      dxSinceLast = 0;
    }
  }

  auto lastLabelledRow = firstLabelledRow;
  // Subsequent rows consider the row above as well
  int y = 1;
  for (; y < output->d_mat.rows; y++) {
    if (y - lastLabelledRow->imageY == stepY) {
      ++lastLabelledRow;
      if (lastLabelledRow == labelData.end())
        break;

      stepX = lastLabelledRow->granularity.x();
      stepY = lastLabelledRow->granularity.y();
      ASSERT(lastLabelledRow != labelData.end());
      ASSERT(lastLabelledRow->imageY == y);
    }

    ASSERT(lastLabelledRow->imageY <= y);

    lastLabelledPixel = lastLabelledRow->begin();
    dxSinceLast = 0;

    target = output->d_mat.ptr<int>(y);
    targetEnd = target + imageWidth - 1;

    auto above = output->d_mat.template ptr<int const>(y - 1);
    auto lastLabel = uint8_t(*lastLabelledPixel == uint8_t(labelClass) ? 1u : 0u);
    *target = lastLabel + *above;
    while (target != targetEnd) {
      ++dxSinceLast;
      if (dxSinceLast == stepX) {
        ++lastLabelledPixel;
        dxSinceLast = 0;
        lastLabel = uint8_t(*lastLabelledPixel == uint8_t(labelClass) ? 1u : 0u);
      }

      int leftValue = *(target++);
      int aboveLeftValue = *(above++);

      *target = lastLabel + leftValue + *above - aboveLeftValue;
    }
  }

  // Do the rest of the rows that aren't labelled
  // TODO: we could just copy the last row
  for (; y < output->d_mat.rows; y++) {
    target = output->d_mat.ptr<int>(y);
    targetEnd = target + imageWidth - 1;
    auto above = output->d_mat.template ptr<int const>(y - 1);

    *target = *above;
    while (target != targetEnd) {
      int leftValue = *(target++);
      int aboveLeftValue = *(above++);

      *target = leftValue + *above - aboveLeftValue;
    }
  }

  return output;
}

int IntegralImage::getSummedArea(Point2i min, Point2i max) const {
  int sum = at(max.y(), max.x());

  if (min.x() != 0 && min.y() != 0) {
    sum += at(min.y() - 1, min.x() - 1);
    sum -= at(max.y(), min.x() - 1);
    sum -= at(min.y() - 1, max.x());
  } else if (min.y() != 0) {
    sum -= at(min.y() - 1, max.x());
  } else if (min.x() != 0) {
    sum -= at(max.y(), min.x() - 1);
  }

  return sum;
}
