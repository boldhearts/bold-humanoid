// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <ostream>

#include "colour/colour.hh"
#include "colour/BGR/bgr.hh"
#include "colour/HSVRange/hsvrange.hh"

namespace bold
{
  namespace vision
  {
    enum class LabelClass : uint8_t
    {
      UNKNOWN = 0,
      GOAL = 1,
      BALL,
      FIELD,
      LINE,
      CYAN,
      MAGENTA,
      BORDER
    };

    class PixelLabel
    {
    public:
      PixelLabel(std::string name, LabelClass cls, colour::BGR themeColour);
      virtual ~PixelLabel() {}

      LabelClass getClass() const { return d_class; }
      std::string getName() const { return d_name; }
      colour::BGR getThemeColour() const { return d_themeColour; }

      virtual void setHSVRange(colour::HSVRange range) {}
      virtual void addSample(colour::HSV const& pixelColour) = 0;
      virtual float labelProb(colour::HSV const& pixelColour) const = 0;
      virtual colour::HSV modalColour() const = 0;

      virtual void print(std::ostream& out) const;

    private:
      LabelClass d_class;
      std::string d_name;
      colour::BGR d_themeColour;
    };

    inline PixelLabel::PixelLabel(std::string name, LabelClass cls, colour::BGR themeColour)
      : d_class(cls),
        d_name(name),
        d_themeColour(themeColour)
    {}

    inline void PixelLabel::print(std::ostream& out) const
    {
      out << d_name << " (" << unsigned(d_class) << ")";
    }
  }
}

namespace std
{
  ostream& operator<<(ostream& out, bold::vision::PixelLabel const& PixelLabel);
}
