// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "colour/HSV/hsv.hh"
#include "vision/PixelLabel/pixellabel.hh"

namespace bold
{
  namespace vision
  {
    class RangePixelLabel : public PixelLabel
    {
    public:
      RangePixelLabel(std::string name, LabelClass id, colour::HSVRange hsvRange);

      void addSample(colour::HSV const& pixelColour) override;

      colour::HSVRange getHSVRange() const;
      void setHSVRange(colour::HSVRange range) override;

      float labelProb(colour::HSV const& pixelColour) const override;
      colour::HSV modalColour() const override;

      void print(std::ostream& out) const override;

    private:
      colour::HSVRange d_hsvRange;
    };



    inline RangePixelLabel::RangePixelLabel(std::string name, LabelClass id, colour::HSVRange hsvRange)
      : PixelLabel{name, id, hsvRange.toBGR()},
        d_hsvRange{std::move(hsvRange)}
  {}

    inline void RangePixelLabel::addSample(colour::HSV const& pixelColour)
    {
      d_hsvRange = d_hsvRange.containing(pixelColour);
    }
  
    inline colour::HSVRange RangePixelLabel::getHSVRange() const
    {
      return d_hsvRange;
    }

    inline void RangePixelLabel::setHSVRange(colour::HSVRange range)
    {
      d_hsvRange = std::move(range);
    }

    inline float RangePixelLabel::labelProb(colour::HSV const& pixelColour) const
    {
      return d_hsvRange.contains(pixelColour) ? 1.0f : 0.0f;
    }

    inline colour::HSV RangePixelLabel::modalColour() const
    {
      return d_hsvRange.toHSV();
    }

    inline void RangePixelLabel::print(std::ostream& out) const
    {
      PixelLabel::print(out);
      out << " " << d_hsvRange;
    }
  }
}
