// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once 

#include "vision/PixelLabel/pixellabel.hh"
#include "colour/HSV/hsv.hh"

#include <array>
#include <opencv2/imgproc/imgproc.hpp>

namespace bold
{
  namespace vision
  {
    template<uint8_t CHANNEL_BITS>
    class HistogramPixelLabel : public PixelLabel
    {
    public:
      HistogramPixelLabel(std::string name, LabelClass id);

      void addSample(colour::HSV const& pixelColour) override;

      unsigned getTotalCount() const;

      float labelProb(colour::HSV const& pixelColour) const override;

      colour::HSV modalColour() const override;

      unsigned getHistogramValue(uint8_t h, uint8_t s, uint8_t v) const;

      static unsigned hsvToIndex(colour::HSV const& hsv);

      static unsigned hsvToIndex(uint8_t h, uint8_t s, uint8_t v);

      static colour::HSV indexToHsv(unsigned idx);

      static uint8_t channelIndexToValue(uint8_t idx);

      static uint8_t valueToChannelIndex(uint8_t val);

      static unsigned getBinSize();

      static unsigned getNBins();

      cv::Mat getHSImage() const;
      cv::Mat getHVImage() const;
      cv::Mat getSVImage() const;

      void write(std::ostream& out) const;
      void read(std::istream& in);

    private:
      constexpr static unsigned BIN_SIZE = (256 >> CHANNEL_BITS);
      constexpr static unsigned NBINS = 1 << CHANNEL_BITS;
      constexpr static unsigned TOTAL_BINS = NBINS * NBINS * NBINS;

      std::array<unsigned, TOTAL_BINS> d_histogram;
      unsigned d_totalCount;
    };

    extern template class HistogramPixelLabel<8>;
    extern template class HistogramPixelLabel<7>;
    extern template class HistogramPixelLabel<6>;
    extern template class HistogramPixelLabel<5>;
    extern template class HistogramPixelLabel<4>;
    extern template class HistogramPixelLabel<3>;
    extern template class HistogramPixelLabel<2>;
    extern template class HistogramPixelLabel<1>;
  }
}
