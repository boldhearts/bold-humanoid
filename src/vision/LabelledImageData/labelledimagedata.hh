// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "vision/ImageSampleMap/imagesamplemap.hh"
#include <vector>
#include <Eigen/Core>

namespace bold
{
  namespace vision
  {
    struct RowLabels;

    using label_iterator = std::vector<uint8_t>::iterator;
    using const_label_iterator = std::vector<uint8_t>::const_iterator;
    using rowlabels_iterator = std::vector<RowLabels>::iterator;
    using const_rowlabels_iterator = std::vector<RowLabels>::const_iterator;

    /** Efficient representation of sparsely labelled image
     *
     * Stores label data in contiguous memory and provides iterators
     * to efficiently iterate over them. Iteration happens per row,
     * providing a @a RowLabels element per row. This object can be
     * used to iterate over the actual labels, and provides
     * information about the pixel coordinates the labels belong to.
     */
    class LabelledImageData
    {
    public:

      /** Constructor
       *
       * @param sampleMap the map that defines which pixels have been sampled, and thus where the pixels belong
       */
      LabelledImageData(ImageSampleMap sampleMap)
        : d_sampleMap{std::move(sampleMap)},
          d_labels(d_sampleMap.getPixelCount())
      {
        d_rows.reserve(d_sampleMap.getSampleRowCount());
      }

      /// Retrieve the mapping definition of the pixels in the image
      ImageSampleMap const& getSampleMap() const { return d_sampleMap; }
      
      /** Number of labelled rows
       *
       * Less than total rows in the original image when subsampling
       */
      size_t getLabelledRowCount() const { return d_rows.size(); }

      /** Begin for iteration over all labels
       *
       * Used to fill labels, or to access them more efficiently when
       * row location and granularity is not required.
       */
      label_iterator labelsBegin() { return d_labels.begin(); }
      label_iterator labelsEnd() { return d_labels.end(); }

      /** Add labelled row data
       *
       * @param begin Starting point in the label storage of this object
       * @param end End point in the label storage of this object
       * @param y Vertical coordinate of pixels in this row
       * @param granularity x and y granularity of this row, i.e. distance between pixels in this row and between this and the next row.
       */
      void emplace_row(label_iterator begin, label_iterator end,
                       uint16_t y, Eigen::Matrix<uint8_t,2,1> granularity)
      {
        d_rows.emplace_back(begin, end, y, granularity);
      }
      
      const_rowlabels_iterator begin() const { return d_rows.begin(); }
      const_rowlabels_iterator end() const { return d_rows.end(); }

      rowlabels_iterator begin() { return d_rows.begin(); }
      rowlabels_iterator end() { return d_rows.end(); }

    private:
      ImageSampleMap d_sampleMap;
      std::vector<uint8_t> d_labels;
      std::vector<RowLabels> d_rows;
    };

    /** Helper structure to hold labelled row information
     */
    struct RowLabels
    {
      RowLabels(label_iterator begin, label_iterator end, uint16_t y, Eigen::Matrix<uint8_t,2,1> granularity)
        : imageY(y),
          granularity(granularity),
          d_begin(begin),
          d_end(end)
      {}
      
      uint16_t imageY;
      Eigen::Matrix<uint8_t,2,1> granularity;
      
      const_label_iterator begin() const { return d_begin; }
      const_label_iterator end() const { return d_end; }

      label_iterator begin() { return d_begin; }
      label_iterator end() { return d_end; }

    private:
      label_iterator d_begin;
      label_iterator d_end;
    };
  }
}
