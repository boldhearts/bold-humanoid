// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <functional>
#include <vector>
#include <Eigen/Core>

namespace bold
{
  namespace vision
  {
    /** Produces a map used in sub-sampling image data.
     *
     * A granularity function allows processing fewer than all pixels
     * in the image.
     *
     * Granularity is calculated based on y-value, and is specified in
     * both x and y dimensions.
     */
    class ImageSampleMap
    {
    public:
      ImageSampleMap(std::function<Eigen::Matrix<uint8_t,2,1>(uint16_t)> granularityFunction,
                     uint16_t imageWidth, uint16_t imageHeight);

      uint16_t getImageWidth() const { return d_imageWidth; }
      uint16_t getImageHeight() const { return d_imageHeight; }
      
      /** Total number of pixels sampled
       *
       * Will be less than width x height for subsampling
       */
      unsigned getPixelCount() const { return d_pixelCount; }

      /** Total number of rows with sampled pixels
       */
      uint16_t getSampleRowCount() const { return static_cast<uint16_t>(d_granularities.size()); }

      /** Iterator over granularities
       *
       * One element for each row with sampled pixels
       */
      std::vector<Eigen::Matrix<uint8_t,2,1>>::const_iterator begin() const;

      /** Map that samples every pixel
       */
      static ImageSampleMap all(uint16_t width, uint16_t height);

      /** Map that samples every second pixel
       */
      static ImageSampleMap half(uint16_t width, uint16_t height);

      /** Map that samples every third
       */
      static ImageSampleMap third(uint16_t width, uint16_t height);

    private:
      std::vector<Eigen::Matrix<uint8_t,2,1>> d_granularities;
      unsigned d_pixelCount;
      uint16_t d_imageWidth;
      uint16_t d_imageHeight;
    };



    inline std::vector<Eigen::Matrix<uint8_t,2,1>>::const_iterator ImageSampleMap::begin() const
    {
      return d_granularities.begin();
    }

    inline ImageSampleMap ImageSampleMap::all(uint16_t width, uint16_t height)
    {
      return ImageSampleMap([](int y) { return Eigen::Matrix<uint8_t,2,1>(1,1); },
                            width, height);
    }

    inline ImageSampleMap ImageSampleMap::half(uint16_t width, uint16_t height)
    {
      return ImageSampleMap([](int y) { return Eigen::Matrix<uint8_t,2,1>(2,2); },
                            width, height);
    }

    inline ImageSampleMap ImageSampleMap::third(uint16_t width, uint16_t height)
    {
      return ImageSampleMap([](int y) { return Eigen::Matrix<uint8_t,2,1>(3,3); },
                            width, height);
    }
  }
}
