// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <numeric>

namespace bold
{
  namespace util
  {
    namespace binary
    {
      constexpr uint16_t makeWord(uint8_t lowByte, uint8_t highByte)
      {
        return (highByte << 8) | lowByte;
      }
      
      constexpr uint8_t getLowByte(uint16_t word)
      {
        return word & 0xFF;
      }
      
      constexpr uint8_t getHighByte(uint16_t word)
      {
        return (word >> 8) & 0xFF;
      }

      template<class InputIt>
      constexpr uint8_t calculateChecksum(InputIt first, InputIt last)
      {
        return ~std::accumulate(first, last, 0u);
      }
    };
  }
}
