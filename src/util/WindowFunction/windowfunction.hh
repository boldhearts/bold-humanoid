// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>
#include <memory>
#include <algorithm>

namespace bold
{
  namespace util
  {
    enum class WindowFunctionType
    {
      Rectangular = 1,
        Triangular = 2,
        Welch = 3,
        Hann = 4,
        Hamming = 5
        };

    class WindowFunction
    {
    public:
      template<typename Iter>
      void apply(Iter begin, Iter end);

      template<typename Derived>
      static std::unique_ptr<Derived> create(size_t size);
      
    protected:
      virtual void fill(std::vector<float>& window) = 0;

      WindowFunction(std::size_t size)
        : d_window(size)
      {}

    private:

      void init()
      {
        fill(d_window);
      }
      
      std::vector<float> d_window;
    };

    class RectangleWindowFunction : public WindowFunction
    {
    protected:
      void fill(std::vector<float>& window) override;
      
    private:
      using WindowFunction::WindowFunction;
      friend class WindowFunction;
    };

    class TriangularWindowFunction : public WindowFunction
    {
    protected:
      void fill(std::vector<float>& window) override;

    private:
      using WindowFunction::WindowFunction;
      friend class WindowFunction;
    };
    
    class WelchWindowFunction : public WindowFunction
    {
    protected:
      void fill(std::vector<float>& window) override;
      
    private:
      using WindowFunction::WindowFunction;
      friend class WindowFunction;
    };

    class HannWindowFunction : public WindowFunction
    {
    protected:
      void fill(std::vector<float>& window) override;
      
    private:
      using WindowFunction::WindowFunction;
      friend class WindowFunction;
    };

    class HammingWindowFunction : public WindowFunction
    {
    protected:
      void fill(std::vector<float>& window) override;
      
    private:
      using WindowFunction::WindowFunction;
      friend class WindowFunction;
    };

    
    template<typename Iter>
    void WindowFunction::apply(Iter begin, Iter end)
    {
      for (auto window_iter = d_window.begin();
           window_iter != d_window.end() && begin != end;
           ++window_iter, ++begin)
        *begin *= *window_iter;
    }

    template<typename Derived>
    std::unique_ptr<Derived> WindowFunction::create(size_t size)
    {
      auto windowFunction = std::unique_ptr<Derived>(new Derived(size));
      windowFunction->init();
      return windowFunction;
    }
  }
}
