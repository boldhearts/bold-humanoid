// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "windowfunction.hh"

#include <cmath>

using namespace bold::util;
using namespace std;

void RectangleWindowFunction::fill(std::vector<float>& window)
{
  std::fill(window.begin(), window.end(), 1.0f);
}

void TriangularWindowFunction::fill(std::vector<float>& window)
{
  double a = (window.size() - 1) / 2.0;
  
  for (unsigned i = 0; i < window.size(); i++)
    window[i] = 1.0 - fabs(((double)i - a) / a);
}

void WelchWindowFunction::fill(std::vector<float>& window)
{
  double a = (window.size() - 1) / 2.0;
  double b = (window.size() + 1) / 2.0;
  
  for (unsigned i = 0; i < window.size(); i++)
  {
    double c = ((double)i - a) / b;
    window[i] = 1.0 - c*c;
  }
}

void HannWindowFunction::fill(std::vector<float>& window)
{
  double a = (2.0 * M_PI) / (window.size() - 1);
  
  for (unsigned i = 0; i < window.size(); i++)
    window[i] = 0.5 * (1 - cos(i * a));
}

void HammingWindowFunction::fill(std::vector<float>& window)
{
  const double alpha = 0.53836;
  const double beta = 1 - alpha;
  double c = (2.0 * M_PI) / (window.size() - 1);
  
  for (unsigned i = 0; i < window.size(); i++)
    window[i] = alpha - beta * cos(i * c);
}
