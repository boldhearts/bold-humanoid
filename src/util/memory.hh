// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <memory>
#include <type_traits>
#include <utility>

namespace bold
{
  namespace util
  {
#ifdef EIGEN_DONT_ALIGN
    template<typename T, typename... Args>
    std::shared_ptr<T> allocate_aligned_shared(Args&&... args)
    {
      return std::make_shared<T>(std::forward<Args>(args)...);
    }
#else
    template<typename T, typename... Args>
    std::shared_ptr<T> allocate_aligned_shared(Args&&... args)
    {
      typedef typename std::remove_const<T>::type NonConst;
      return std::allocate_shared<T>(Eigen::aligned_allocator<NonConst>(), std::forward<Args>(args)...);
    }

#endif
  }
}

#if __cplusplus == 201103L
// Stub for std::make_unique which will be added in c++14
namespace std
{
  template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args)
  {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }
}
#endif
