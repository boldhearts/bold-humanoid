// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/FPS/fps.hh"
#include <string>

namespace bold
{
  class Clock;
  
  namespace util
  {

    class Loop
    {
    public:
      Loop(std::string loopName, std::shared_ptr<Clock> clock,
           int schedulePolicy = SCHED_OTHER, int priority = 0);

      ~Loop();

      bool start();

      void stop();

      uint32_t getCycleNumber() const { return d_cycleNumber; }

      double getFps() const { return d_lastFps; }

      bool isRunning() const { return d_isRunning; }

    protected:
      virtual void onLoopStart() {}
      virtual void onStep(unsigned cycleNumber) = 0;
      virtual void onStopped() {}

    private:
      /** Governs the thread's lifetime and operation. */
      static void *threadMethod(void* param);

      uint32_t d_cycleNumber;
      FPS<100> d_fpsCounter;
      double d_lastFps;
      pthread_t d_thread;
      std::string d_loopName;
      bool d_isRunning;
      bool d_isStopRequested;
      int d_schedulePolicy;
      int d_priority;
    };
  }
}
