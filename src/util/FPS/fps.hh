// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "stats/movingaverage.hh"
#include "Clock/clock.hh"
#include "src/util/assert.hh"

#include <memory>

namespace bold
{
  namespace util
  {
    template<int WindowSize>
    class FPS
    {
    public:
      FPS(std::shared_ptr<Clock> clock)
        : d_clock(std::move(clock)),
          d_avg(WindowSize),
          d_last(0)
      {}

      double next()
      {
        auto now = d_clock->getTimestamp();

        if (!unlikely(d_last == 0 || now < d_last))
        {
          auto delta = now - d_last;
          d_avg.next(Clock::timestampToSeconds(delta));
        }

        d_last = now;

        return likely(d_avg.isMature()) ? 1.0/d_avg.getAverage() : 0.0;
      }

    private:
      std::shared_ptr<Clock> d_clock;
      MovingAverage<double> d_avg;
      Clock::Timestamp d_last;
    };
  }
}
