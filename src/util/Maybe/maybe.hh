// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/memory.hh"
#include "util/assert.hh"

#include <memory>
#include <iosfwd>

namespace bold
{
  namespace util
  {
    template<typename T>
    struct Maybe
    {
      Maybe()
        : d_storage{nullptr}
      {}

      Maybe(T value)
        : d_storage{new T{value}}
      {}

      Maybe(Maybe<T>&& rhs)
        : d_storage{move(rhs.d_storage)}
      {}

      Maybe(Maybe<T> const& other)
      {
        copy(other);
      }

      bool hasValue() const { return d_storage != nullptr; }
      T const& value() const { return *d_storage; }

      T const& getOrElse(T const& alt) { return hasValue() ? value() : alt; }

      static Maybe<T> empty() { return Maybe<T>{}; }

      Maybe<T>& operator=(Maybe<T> const& other)
      {
        copy(other);
        return *this;
      }

      Maybe<T>& operator=(Maybe<T>&& other)
      {
        d_storage = move(other.d_storage);
        return *this;
      }

      bool operator==(Maybe<T> const& other) const
      {
        if (hasValue() != other.hasValue())
          return false;
        return !hasValue() || (*d_storage == *(other.d_storage));
      }

      bool operator!=(Maybe<T> const& other) const
      {
        return !(*this == other);
      }

      T& operator*() const
      {
        ASSERT(d_storage != nullptr);
        return *d_storage;
      }

      T* operator->() const
      {
        return d_storage.get();
      }

      explicit operator bool() const
      {
        return hasValue();
      }

      void reset()
      {
        d_storage.release();
      }

    private:
      template<typename U>
      void copy(Maybe<U> const& other)
      {
        d_storage = other ? std::make_unique<T>(*other.d_storage) : nullptr;
      }

      std::unique_ptr<T> d_storage;
    };

    template<typename T>
    Maybe<T> make_maybe(T value)
    {
      return Maybe<T>{value};
    }

    template<typename T>
    inline std::ostream& operator<<(std::ostream& stream, Maybe<T> const& maybe)
    {
      return maybe
        ? stream << "Maybe (hasValue=true value=" << *maybe << ")"
        : stream << "Maybe (hasValue=false)";
    }

    // Functionality to use maybe in range based for loop

    template<typename T>
    class MaybeIterator
    {
    public:
      MaybeIterator(Maybe<T> const& m, bool end = false)
        : d_m{&m},
          d_end{end || !m}
      {}

      bool operator!=(MaybeIterator<T> const& other)
      {
        return other.d_m != d_m || other.d_end != d_end;
      }

      MaybeIterator<T> const& operator++()
      {
        d_end = true;
        return *this;
      }

      T const& operator*() const
      {
        ASSERT(!d_end);
        return d_m->value();
      }

    private:
      Maybe<T> const* d_m;
      bool d_end;
    };

    template<typename T>
    MaybeIterator<T> begin(Maybe<T> const& m)
    {
      return MaybeIterator<T>(m);
    }

    template<typename T>
    MaybeIterator<T> end(Maybe<T> const& m)
    {
      return util::MaybeIterator<T>(m, true);
    }
  }
}
