// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <type_traits>

// TODO endianness as a type param? eg: BufferWriter<LittleEndian>

// TODO write N bytes
// TODO write string of N bytes (encoding?)
// TODO test length and remaining

namespace bold
{
  namespace util
  {
    class BufferWriter
    {
    public:
      BufferWriter(char* ptr);

      template<typename T>
      void write(T val)
      {
        static_assert(std::is_integral<T>::value, "Integral type required");
        *reinterpret_cast<T*>(d_ptr) = val;
        d_ptr += sizeof(T);
      }

      std::size_t pos() const;
      
    private:
      char const* const d_start;
      char* d_ptr;
    };

    
    inline BufferWriter::BufferWriter(char* ptr)
      : d_start(ptr),
        d_ptr(ptr)
    {}
    
    inline std::size_t BufferWriter::pos() const
    {
      return d_ptr - d_start;
    }
  }
}
