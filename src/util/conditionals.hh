// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <functional>
#include "Clock/clock.hh"

namespace bold
{
  namespace util
  {
    auto negate = [](std::function<bool()> condition)
    {
      return [condition] { return !condition(); };
    };

    auto isRepeated = [](int times, std::function<bool()> condition)
    {
      auto count = std::make_shared<int>(0);
      return [count,times,condition]
      {
        if (condition())
        {
          (*count)++;
          return *count >= times;
        }
        *count = 0;
        return false;
      };
    };

    auto trueNTimes = [](int times)
    {
      auto count = std::make_shared<int>(0);
      return [count,times]
      {
        (*count)++;
        return *count <= times;
      };
    };

    auto trueForMillis = [](int millis, std::shared_ptr<Clock> clock, std::function<bool()> condition)
    {
      auto turnedTrueAt = std::make_shared<double>(0);
      auto lastTrue = std::make_shared<bool>(false);

      return [turnedTrueAt, lastTrue, millis, clock, condition]
      {
        if (condition())
        {
          auto t = clock->getMillis();
          if (*lastTrue)
          {
            if (t - *turnedTrueAt > millis)
            {
              return true;
            }
          }
          else
          {
            *lastTrue = true;
            *turnedTrueAt = t;
          }
        }
        else
        {
          *lastTrue = false;
        }
        return false;
      };
    };

    auto stepUpDownThreshold = [](int threshold, std::function<bool()> condition)
    {
      auto count = std::make_shared<int>(0);

      return [count,condition,threshold]
      {
        if (condition())
        {
          if (*count < threshold)
          {
            (*count)++;
            return *count == threshold;
          }

          return true;
        }
        else if (*count > 0)
        {
          (*count)--;
        }

        return false;
      };
    };

    auto oneShot = [](std::function<std::function<bool()>()> factory)
    {
      auto currentFun = std::make_shared<std::function<bool()>>();
      auto hasFun = std::make_shared<bool>(false);

      return [currentFun,factory,hasFun]
      {
        if (!(*hasFun))
        {
          *currentFun = factory();
          *hasFun = true;
        }

        if ((*currentFun)())
        {
          *currentFun = nullptr;
          *hasFun = false;
          return true;
        }

        return false;
      };
    };

    auto changedTo = [](bool trueWhen, std::function<bool()> condition)
    {
      auto lastValue = std::make_shared<bool>(!trueWhen);

      return [lastValue,trueWhen,condition]
      {
        bool val = condition();

        if (val == *lastValue)
          return false;

        *lastValue = val;
        return val == trueWhen;
      };
    };
  }
}
