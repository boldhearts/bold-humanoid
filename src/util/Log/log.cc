// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "log.hh"
#include "util/LogAppender/logappender.hh"

#include <unistd.h>

using namespace bold;
using namespace bold::util;
using namespace std;

LogLevel Log::minLevel = LogLevel::Verbose;
bool Log::logGameState = false;

vector<shared_ptr<LogAppender>> Log::s_appenders;

Log::~Log()
{
  if (!d_message)
    return;

  string message = d_message->str();

  for (auto& appender : d_appenders)
    appender->append(d_level, d_scope, message);
}
