// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iostream>
#include <sstream>
#include <memory>
#include <set>
#include <vector>

#include "util/memory.hh"

namespace bold
{
  namespace util
  {
    class LogAppender;
    
    enum class LogLevel
    {
      Trace = 0,
        Verbose,
        Info,
        Warning,
        Error
        };

    /** A very minimal logging framework to control console output.
     */
    class Log
    {
    public:
      static LogLevel minLevel;
      static bool logGameState;

      static void addAppender(std::shared_ptr<LogAppender> appender);

      static Log trace()   { return Log(LogLevel::Trace); }
      static Log info()    { return Log(LogLevel::Info); }
      static Log verbose() { return Log(LogLevel::Verbose); }
      static Log warning() { return Log(LogLevel::Warning); }
      static Log error()   { return Log(LogLevel::Error); }

      static Log trace(std::string const& scope)   { return Log(scope, LogLevel::Trace); }
      static Log info(std::string const& scope)    { return Log(scope, LogLevel::Info); }
      static Log verbose(std::string const& scope) { return Log(scope, LogLevel::Verbose); }
      static Log warning(std::string const& scope) { return Log(scope, LogLevel::Warning); }
      static Log error(std::string const& scope)   { return Log(scope, LogLevel::Error); }

      template<typename T, typename... Args>
      static Log warnOnce(std::set<T>& ignored, T value, Args... args);

      Log(Log&& Log);
      ~Log();

      template<typename T>
      Log& operator<<(T const& value);

      operator bool() const { return d_message.get(); }

    private:
      Log(LogLevel level)
        : Log{std::string{}, level}
      {}

      Log(std::string const& scope, LogLevel level)
        : d_appenders{s_appenders},
          d_scope{scope},
          d_level{level},
          d_message{level < minLevel ? nullptr : new std::ostringstream()}
      {}

      Log() = default;

      static std::vector<std::shared_ptr<LogAppender>> s_appenders;

      std::vector<std::shared_ptr<LogAppender>> d_appenders;
      std::string d_scope;
      LogLevel d_level;
      std::unique_ptr<std::ostringstream> d_message;
    };



    inline std::ostream& operator<<(std::ostream &stream, std::vector<std::string> const& strings)
    {
      stream <<  "[";
      bool comma = false;
      for (auto const& s : strings)
      {
        if (comma)
          stream << ",";
        comma = true;
        stream << "\"" << s << "\"";
      }
      stream <<  "]";
      return stream;
    }

    inline void Log::addAppender(std::shared_ptr<LogAppender> appender)
    {
      s_appenders.push_back(move(appender));
    }

    template<typename T, typename... Args>
    Log Log::warnOnce(std::set<T>& ignored, T value, Args... args)
    {
      if (ignored.find(value) == ignored.end())
      {
        ignored.insert(value);
        auto l = Log::warning(std::forward<Args>(args)...);
        l << "(ONCE ONLY) ";
        return std::move(l);
      }
      return Log();
    }

    inline Log::Log(Log&& Log)
      : d_scope(move(Log.d_scope)),
        d_level(Log.d_level),
        d_message(std::move(Log.d_message))
    {}

    template<typename T>
    Log& Log::operator<<(T const& value)
    {
      if (d_message)
        (*d_message) << value;
      return *this;
    }
  }
}
