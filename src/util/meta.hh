// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <tuple>

namespace bold
{
  namespace meta
  {
    ///@{
    /** Compilation-time if */

    template <bool COND, int A, int B>
    struct if_
    {
      typedef if_<COND,A,B> type;
      static const int value = A;
    };
  
    template <int A, int B>
    struct if_<false, A, B>
    {
      typedef if_<false,A,B> type;
      static const int value = B;
    };
  
    ///@}


    ///@{
    /** Compilation-time binary min/max operator */

    template <int A, int B>
    struct min : if_<A < B, A, B>
    {};

    template <int A, int B>
    struct max : min<B, A>
    {};

    ///@}


    ///@{
    /** Get index of type in type pack */

    template<int Index, typename T, typename U, typename... Types>
    struct index_of_internal
    {
      static constexpr int value = index_of_internal<Index + 1, T, Types...>::value;
    };

    template<int Index, typename T, typename... Types>
    struct index_of_internal<Index, T, T, Types...>
    {
      static constexpr int value = Index;
    };

    template<typename T, typename... Types>
    struct index_of
    {
      static constexpr int value = index_of_internal<0, T, Types...>::value;
    };

    ///@}


    ///@{
    /** Get tuple member by type.
        When using C++14 this uses std::get<T>
    */
    
#if __cplusplus <= 201103L
    template<typename T, typename... Types>
    T& get(std::tuple<Types...>& tuple)
    {
      return std::get<meta::index_of<T,Types...>::value>(tuple);
    }

    template<typename T, typename... Types>
    T const& get(std::tuple<Types...> const& tuple)
    {
      return std::get<meta::index_of<T,Types...>::value>(tuple);
    }
#else
    using std::get;
#endif

    ///@}

    
    ///@{
    /** Apply function to all elements of a tuple
        Unrolls to equivilant of:

        F::do_it(get<0>(tuple));
        F::do_it(get<1>(tuple));
        F::do_it(get<2>(tuple));
        ...

    */

    template<typename ...>
    struct for_each_impl
    {};

    template<typename T, typename F, typename... Pre, typename... ArgTypes>
    struct for_each_impl<T, F, std::tuple<Pre...>, std::tuple<>, ArgTypes... >
    {
      static void do_it(T const& tuple, ArgTypes... args) {}
    };

    template<typename T, typename F, typename... Pre, typename Cur, typename... Post, typename... ArgTypes>
    struct for_each_impl<T, F, std::tuple<Pre...>, std::tuple<Cur,Post...>, ArgTypes...>
    {
      static void do_it(T const& tuple, ArgTypes... args)
      {
        F::do_it(meta::get<Cur>(tuple), args...);
        for_each_impl<T, F, std::tuple<Pre...,Cur>,std::tuple<Post...>, ArgTypes... >::do_it(tuple, args...);
      }
    };

    template<typename F, typename... Types, typename... ArgTypes>
    void for_each(std::tuple<Types...> const& tuple, ArgTypes... args)
    {
      for_each_impl<std::tuple<Types...>, F, std::tuple<>, std::tuple<Types...>, ArgTypes... >::do_it(tuple, args...);
    }
  }
}
