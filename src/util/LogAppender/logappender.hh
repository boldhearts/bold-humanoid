// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>

namespace bold
{
  namespace util
  {
    enum class LogLevel;
    
    class LogAppender
    {
    public:
      virtual void append(LogLevel level, std::string const& scope, std::string const& message) = 0;
    };

    class ConsoleLogAppender : public LogAppender
    {
    public:
      void append(LogLevel level, std::string const& scope, std::string const& message) override;

      static bool isStdOutRedirected();
      static bool isStdErrRedirected();
    };
    
  }
}
