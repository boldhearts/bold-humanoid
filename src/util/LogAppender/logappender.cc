// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "logappender.hh"
#include "util/Log/log.hh"
#include "util/ccolor.hh"

#include "state/StateObject/GameState/gamestate.hh"
#include "state/State/state.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

void writeLogTimestamp(ostream& o, bool isConsole)
{
  time_t now = time(0);
  tm tstruct = *localtime(&now);
  char buf[80];
  // http://en.cppreference.com/w/cpp/chrono/c/strftime
  strftime(buf, sizeof(buf), isConsole ? "%T" : "%X", &tstruct);
  o << buf << " ";
}

/*
void writeGameState(ostream& o)
{
  // Ensure the game state has been registered
  if (!Log::logGameState)
    return;

  auto game = State::get<GameState>();

  if (!game)
    return;

  auto result = game->getGameResult();
  if (result != GameResult::Undecided)
  {
    o << getGameResultName(result)
      << " "
      << (int)game->getMyTeam().getTeamNumber() << "v" << (int)game->getOpponentTeam().getTeamNumber()
      << " "
      << (int)game->getMyTeam().getScore() << ":" << (int)game->getOpponentTeam().getScore();
  }
  else
  {
    auto mode = game->getPlayMode();
    o << (int)game->getMyTeam().getTeamNumber() << "v" << (int)game->getOpponentTeam().getTeamNumber()
      << " "
      << (int)game->getMyTeam().getScore() << ":" << (int)game->getOpponentTeam().getScore()
      << " "
      << getPlayModeName(mode)
      << " ";
    if (game->isPenaltyShootout())
    {
      o << "penalties ";
    }
    else
    {
      o << (game->isFirstHalf() ? "first" : "second");
      if (game->isOvertime())
        o << "-extra";
      o << " ";
    }

    auto clock = SystemClock{};
    o << game->getSecondsRemaining(clock);
    auto secondary = game->getSecondaryTime(clock);
    if (secondary != 0)
      o << "/" << secondary;
  }

  o << " ";
}
*/

void writeLevelShortName(ostream& o, LogLevel level)
{
  switch (level)
  {
    case LogLevel::Trace:   o << "trc "; break;
    case LogLevel::Verbose: o << "vrb "; break;
    case LogLevel::Info:    o << "inf "; break;
    case LogLevel::Warning: o << "WRN "; break;
    case LogLevel::Error:   o << "ERR "; break;
    default:                o << "??? "; break;
  }
}

void ConsoleLogAppender::append(LogLevel level, string const& scope, string const& message)
{
  int fgColor = 39;
  int bgColor = 49;

  switch (level)
  {
    case LogLevel::Trace:
      fgColor = 90;
      break;
    case LogLevel::Verbose:
      fgColor = 37;
      break;
    case LogLevel::Info:
      break;
    case LogLevel::Warning:
      fgColor = 95;
      break;
    case LogLevel::Error:
      fgColor = 37;
      bgColor = 41;
      break;
  }

  auto& ostream = level == LogLevel::Error ? cerr : cout;
  bool isRedirected = level == LogLevel::Error ? isStdErrRedirected() : isStdOutRedirected();

  if (isRedirected)
  {
    writeLogTimestamp(ostream, /*isConsole*/ false);
    /* TODO: It could be that we are logging from a place that has
       locked State::s_mutex, so this can give a deadlock */
    // writeGameState(ostream);
    writeLevelShortName(ostream, level);

    if (scope.size())
      ostream << "[" << scope << "] ";

    ostream << message << endl;
  }
  else
  {
    ostream << ccolor::fore::lightblack;
    writeLogTimestamp(ostream, /*isConsole*/ true);

    if (scope.size())
      ostream << ccolor::fore::lightblue << "[" << scope << "] ";

    ostream << "\033[" << fgColor << ";" << bgColor << "m"
      << message
      << "\033[00m" << endl;
  }
}

bool ConsoleLogAppender::isStdOutRedirected()
{
  static bool isStdOutRedirected = isatty(STDOUT_FILENO) == 0;
  return isStdOutRedirected;
}

bool ConsoleLogAppender::isStdErrRedirected()
{
  static bool isStdErrRedirected = isatty(STDERR_FILENO) == 0;
  return isStdErrRedirected;
}

