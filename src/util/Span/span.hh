// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <array>

namespace bold
{
  namespace util
  {
    /** Helper class designating a range of values
     *
     * Use this class to create a view on a range of data. There are 2
     * major uses:
     *
     * - wrap plain C pointer-length ranges, so that the two can be
     *   passed together transparently and with no ambiguity
     * - create light weight non-owning view on data in an owning
     *   container
     */
    template <typename T>
    class Span
    {
    public:
      using iterator = T*;
      using const_iterator = T const*;
      
      Span(T* data, std::size_t length);

      template<std::size_t N>
      Span(std::array<T, N>& data);

      std::size_t length() const;
      
      T operator[](std::size_t idx) const;
      T& operator[](std::size_t idx);

      const_iterator begin() const;
      const_iterator end() const;

      iterator begin();
      iterator end();

    private:
      T* d_data;
      T* d_dataEnd;
      std::size_t d_length;
    };



    template<typename T>
    Span<T>::Span(T* data, std::size_t length)
      : d_data{data},
        d_dataEnd{data + length},
        d_length{length}
        {}

    template<typename T>
    template<std::size_t N>
    Span<T>::Span(std::array<T, N>& data)
      : d_data{data.data()},
        d_dataEnd{d_data + N},
        d_length{N}
        {}

    template<typename T>
    std::size_t Span<T>::length() const
    {
      return d_length;
    }

    template<typename T>
    typename Span<T>::const_iterator Span<T>::begin() const
    {
      return d_data;
    }

    template<typename T>
    typename Span<T>::const_iterator Span<T>::end() const
    {
      return d_dataEnd;
    }

    template<typename T>
    typename Span<T>::iterator Span<T>::begin()
    {
      return d_data;
    }

    template<typename T>
    typename Span<T>::iterator Span<T>::end()
    {
      return d_data + d_length;
    }
  }
}
