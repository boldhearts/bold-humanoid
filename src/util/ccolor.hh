// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

namespace bold
{
  namespace util
  {
    struct ccolor
    {
      struct fore
      {
        static const char *black;
        static const char *blue;
        static const char *red;
        static const char *magenta;
        static const char *green;
        static const char *cyan;
        static const char *yellow;
        static const char *white;
        static const char *console;

        static const char *lightblack;
        static const char *lightblue;
        static const char *lightred;
        static const char *lightmagenta;
        static const char *lightgreen;
        static const char *lightcyan;
        static const char *lightyellow;
        static const char *lightwhite;

      private:
        fore() = delete;
      };

      struct back
      {
        static const char *black;
        static const char *blue;
        static const char *red;
        static const char *magenta;
        static const char *green;
        static const char *cyan;
        static const char *yellow;
        static const char *white;
        static const char *console;

        static const char *lightblack;
        static const char *lightblue;
        static const char *lightred;
        static const char *lightmagenta;
        static const char *lightgreen;
        static const char *lightcyan;
        static const char *lightyellow;
        static const char *lightwhite;

      private:
        back() = delete;
      };

      static const char *error;
      static const char *warning;
      static const char *info;

      static char *color(int attr, int fg, int bg);
      static const char *reset;
      static const char *bold;
      static const char *underline;

    private:
      ccolor() = delete;
    };
  }
}
