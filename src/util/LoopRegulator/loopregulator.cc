// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "loopregulator.hh"

#include "util/Log/log.hh"

using namespace bold::util;

void LoopRegulator::start()
{
  clock_gettime(CLOCK_MONOTONIC, &d_nextTime);
}

void LoopRegulator::setIntervalMicroseconds(unsigned intervalMicroseconds)
{
  clock_gettime(CLOCK_MONOTONIC, &d_nextTime);
  d_intervalMicroseconds = intervalMicroseconds;
}

void LoopRegulator::wait()
{
  // NOTE this will always increment by the interval, even if something stalled
  d_nextTime.tv_sec += (d_nextTime.tv_nsec + d_intervalMicroseconds * 1000) / 1000000000;
  d_nextTime.tv_nsec = (d_nextTime.tv_nsec + d_intervalMicroseconds * 1000) % 1000000000;

  int sleepResult = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &d_nextTime, nullptr);

  if (sleepResult != 0)
  {
    if (sleepResult == EINTR)
      Log::warning("LoopRegulator::wait") << "Sleep interrupted";
    else
      Log::warning("LoopRegulator::wait") << "clock_nanosleep returned error code: " << sleepResult;
  }
}
