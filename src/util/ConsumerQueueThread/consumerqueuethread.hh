// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <utility>

#include "util/Log/log.hh"

namespace bold {
  namespace util {
    template<typename T>
    class ConsumerQueueThread {
    public:
      ConsumerQueueThread(ConsumerQueueThread const &) = delete;

      ConsumerQueueThread(std::string threadName, std::function<void(T)> processor);

      ConsumerQueueThread &operator=(ConsumerQueueThread const &) = delete;

      size_t size() const;

      void push(const T &item);

      void stop();

    private:

      void run();

      std::queue<T> d_queue;
      mutable std::mutex d_mutex;
      std::condition_variable d_condition;
      std::string d_threadName;
      std::function<void(T)> d_processor;
      std::thread d_thread;
      bool d_stop;
    };


    template<typename T>
    ConsumerQueueThread<T>::ConsumerQueueThread(std::string threadName, std::function<void(T)> processor)
      : d_threadName(std::move(threadName)),
        d_processor(processor),
        d_thread(std::thread(&ConsumerQueueThread::run, this)),
        d_stop(false) {}

    template<typename T>
    size_t ConsumerQueueThread<T>::size() const {
      std::unique_lock<std::mutex> lock(d_mutex);
      return d_queue.size();
    }

    template<typename T>
    void ConsumerQueueThread<T>::push(const T &item) {
      if (d_stop) {
        Log::error("ConsumerQueueThread::push") << "Invalid operation once stopped";
        throw std::runtime_error("Invalid operation once stopped");
      }

      std::unique_lock<std::mutex> lock(d_mutex);
      d_queue.push(item);
      lock.unlock();
      d_condition.notify_one();
    }

    template<typename T>
    void ConsumerQueueThread<T>::stop() {
      std::unique_lock<std::mutex> lock(d_mutex);
      d_stop = true;
      lock.unlock();
      d_condition.notify_one();
      d_thread.join();
    }

    template<typename T>
    void ConsumerQueueThread<T>::run() {
      pthread_setname_np(pthread_self(), d_threadName.c_str());

      while (true) {
        std::unique_lock<std::mutex> lock(d_mutex);

        // Wait for an item to appear in the queue.
        // Note that we may loop around due to 'spurious wakes'.
        // Check that stop hasn't been requested as well.
        while (!d_stop && d_queue.empty()) {
          d_condition.wait(lock);
        }

        if (d_stop)
          return;

        T item = d_queue.front();
        d_queue.pop();

        lock.unlock();

        d_processor(item);
      }
    }

  }
}
