// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <stdexcept>
#include "util/Log/log.hh"

namespace bold
{
  namespace util
  {
    enum class SchmittTriggerTransition
    {
      None,
      High,
      Low
    };

    /** A dual-threshold latch with hysteresis.
     *
     * See https://en.wikipedia.org/wiki/Schmitt_trigger
     */
    template<typename T>
    class SchmittTrigger
    {
    public:
      SchmittTrigger(T lowThreshold, T highThreshold, bool isInitiallyHigh);
      SchmittTriggerTransition next(T level);

      bool isHigh() const { return d_isHigh; }
      
      T getLowThreshold() const { return d_lowThreshold; }
      T getHighThreshold() const { return d_highThreshold; }

    private:
      T const d_lowThreshold;
      T const d_highThreshold;
      bool d_isHigh;
    };



    
    template<typename T>
    SchmittTrigger<T>::SchmittTrigger(T lowThreshold, T highThreshold, bool isInitiallyHigh)
      : d_lowThreshold(lowThreshold),
        d_highThreshold(highThreshold),
        d_isHigh(isInitiallyHigh)
    {
      if (lowThreshold >= highThreshold)
      {
        Log::error("SchmittTrigger::SchmittTrigger") << "Low threshold must be lower than high threshold";
        throw std::runtime_error("Low threshold must be lower than high threshold");
      }
    }

    template<typename T>
    SchmittTriggerTransition SchmittTrigger<T>::next(T level)
    {
      if (d_isHigh && level <= d_lowThreshold)
      {
        d_isHigh = false;
        return SchmittTriggerTransition::Low;
      }
      else if (!d_isHigh && level >= d_highThreshold)
      {
        d_isHigh = true;
        return SchmittTriggerTransition::High;
      }
      
      return SchmittTriggerTransition::None;
    }
  }
}
