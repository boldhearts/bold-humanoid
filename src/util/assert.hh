// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <stdlib.h>
#include <iostream>

#ifdef __GNUC__
  #define likely(x)      __builtin_expect(!!(x), 1)
  #define unlikely(x)    __builtin_expect(!!(x), 0)
#else
  #define likely(x)      (x)
  #define unlikely(x)    (x)
#endif

/// ASSERT(expr) checks if expr is true.  If not, error details are logged
/// and the process is exited with a non-zero code.
#ifdef INCLUDE_ASSERTIONS

void print_trace();

#define ASSERT(expr)                                                      \
    if (unlikely(!(expr))) {                                              \
        char buf[4096];                                                   \
        snprintf (buf, 4096, "Assertion failed in \"%s\", line %d\n%s\n", \
                 __FILE__, __LINE__, #expr);                              \
        std::cerr << buf;                                                 \
        print_trace();                                                    \
        ::abort();                                                        \
    }                                                                     \
    else // This 'else' exists to catch the user's following semicolon
#else
#define ASSERT(expr)
#endif


/// DASSERT(expr) is just like ASSERT, except that it only is functional in
/// DEBUG mode, but does nothing when in a non-DEBUG build.
#ifdef DEBUG
# define DASSERT(expr) ASSERT(expr)
#else
# define DASSERT(expr) /* DASSERT does nothing when not debugging */
#endif
