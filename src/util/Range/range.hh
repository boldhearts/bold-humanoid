// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iostream>

#include "Math/math.hh"
#include "util/assert.hh"

namespace bold
{
  namespace util
  {
    template<typename T>
    class Range
    {
    public:
      Range();
      Range(T min, T max);

      bool isEmpty() const;
      T min() const;
      T max() const;

      void expand(T value);
      void expand(Range<T> value);
      
      void reset();

      T size() const;

      bool contains(T value) const;

      T clamp(T value) const;

      bool operator==(Range<T> const& other) const;
      bool operator!=(Range<T> const& other) const;
      
    private:
      bool d_isEmpty;
      T d_min;
      T d_max;
    };

    template<typename T>
    std::ostream& operator<<(std::ostream &stream, Range<T> const& range)
    {
      if (range.isEmpty())
        return stream << "(empty)";
      return stream << "(" << range.min() << " -> " << range.max() << ")";
    }



    template<typename T>
    Range<T>::Range() : d_isEmpty(true), d_min(), d_max() {}

    template<typename T>
    Range<T>::Range(T min, T max) : d_isEmpty(false), d_min(min), d_max(max) { ASSERT(min <= max); }

    template<typename T>
    bool Range<T>::isEmpty() const { return d_isEmpty; }

    template<typename T>
    T Range<T>::min() const { return d_min; }
    template<typename T>
    T Range<T>::max() const { return d_max; }

    template<typename T>
    void Range<T>::expand(T value)
    {
      if (d_isEmpty)
      {
        d_isEmpty = false;
        d_min = value;
        d_max = value;
      }
      else
      {
        if (d_min > value)
          d_min = value;
        if (d_max < value)
          d_max = value;
      }
    }

    template<typename T>
    void Range<T>::expand(Range<T> value)
    {
      if (value.isEmpty())
        return;
      
      expand(value.min());
      expand(value.max());
    }

    template<typename T>
    void Range<T>::reset()
    {
      d_isEmpty = true;
    }

    template<typename T>
    T Range<T>::size() const
    {
      return d_max - d_min;
    }

    template<typename T>
    bool Range<T>::contains(T value) const
    {
      return d_min <= value && d_max >= value;
    }
    
    template<typename T>
    T Range<T>::clamp(T value) const
    {
      return Math::clamp(value, d_min, d_max);
    }
    
    template<typename T>
    bool Range<T>::operator==(Range<T> const& other) const
    {
      if (d_isEmpty && other.d_isEmpty)
        return true;
      
      return
        d_isEmpty == other.d_isEmpty &&
        d_min == other.d_min &&
        d_max == other.d_max;
    }
    
    template<typename T>
    bool Range<T>::operator!=(Range<T> const& other) const
    {
      return !((*this) == other);
    }
  }
}
