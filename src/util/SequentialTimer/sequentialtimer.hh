// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>
#include <memory>
#include <deque>
#include <sstream>

#include "Clock/clock.hh"
#include "util/assert.hh"

namespace bold {
  namespace util {
    using EventTiming = std::pair<std::string, double>;

    /** Nesting timer of sequential events
     */
    class SequentialTimer {
    public:
      SequentialTimer(std::shared_ptr<Clock> clock)
          : d_clock{std::move(clock)},
            d_eventTimings{std::make_unique<std::vector<EventTiming>>()},
            d_prefix{""},
            d_last{d_clock->getTimestamp()}
      {}

      /** Time singular event
       *
       * Records the time since the last timed event and now.
       *
       * @param eventName Name to record this event under, will be prefixed by previously entered contexts
       */
      void timeEvent(std::string const &eventName) {
        ASSERT(d_eventTimings != nullptr);
        auto now = d_clock->getTimestamp();
        auto timeMillis = Clock::timestampToMillis(now - d_last);
        std::string name = d_prefix.size() ? d_prefix + '/' + eventName : eventName;
        d_eventTimings->emplace_back(name, timeMillis);
        d_last = now;
      }

      /** Get recorded event timings
       *
       * No more events can be timed after this.
       *
       * @return All timed events
       */
      std::unique_ptr<std::vector<EventTiming>> flush() {
        ASSERT(d_entered.size() == 0);
        return std::move(d_eventTimings);
      }

      /** Enter a nested timing context
       *
       * Timing of the next event will be since calling this method.
       *
       * @param name Name of context, to be added to prefix
       */
      void enter(std::string name) {
        auto now = d_clock->getTimestamp();
        d_last = now;
        d_entered.emplace_back(name, now);
        rebuildPrefix();
      }

      /** Exit the last entered nested timing context
       *
       * This will be record the time since entering this context.
       * Timing of the next event will be since calling this method.
       */
      void exit() {
        ASSERT(d_entered.size() != 0);
        auto now = d_clock->getTimestamp();
        auto top = d_entered[d_entered.size() - 1];
        auto timeMillis = Clock::timestampToMillis(now - top.second);
        d_eventTimings->emplace_back(d_prefix, timeMillis);
        d_last = now;
        d_entered.pop_back();
        rebuildPrefix();
      }

      /**
       * @return Current timing context prefix
       */
      std::string const& getPrefix() const { return d_prefix; }

    private:
      void rebuildPrefix() {
        std::stringstream prefix;
        bool first = true;
        for (auto const &e : d_entered) {
          if (first)
            first = false;
          else
            prefix << '/';
          prefix << e.first;
        }
        d_prefix = prefix.str();
      }

      std::shared_ptr<Clock> d_clock;
      std::unique_ptr<std::vector<EventTiming>> d_eventTimings;
      std::deque<std::pair<std::string, Clock::Timestamp>> d_entered;
      std::string d_prefix;
      Clock::Timestamp d_last;
      bool d_flushed;
    };
  }
}
