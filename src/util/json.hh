// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <rapidjson/document.h>

namespace bold
{
  namespace util
  {
    namespace json
    {
      inline int getIntWithDefault(const rapidjson::Value& val, const char* name, int defaultValue)
      {
        auto member = val.FindMember(name);
        if (member == val.MemberEnd() || !member->value.IsInt())
          return defaultValue;
        return member->value.GetInt();
      }

      inline unsigned int getUintWithDefault(const rapidjson::Value& val, const char* name, unsigned int defaultValue)
      {
        auto member = val.FindMember(name);
        if (member == val.MemberEnd() || !member->value.IsUint())
          return defaultValue;
        return member->value.GetUint();
      }

      inline double getDoubleWithDefault(const rapidjson::Value& val, const char* name, double defaultValue)
      {
        auto member = val.FindMember(name);
        if (member == val.MemberEnd() || !member->value.IsDouble())
          return defaultValue;
        return member->value.GetDouble();
      }

      inline bool getBoolWithDefault(const rapidjson::Value& val, const char* name, bool defaultValue)
      {
        auto member = val.FindMember(name);
        if (member == val.MemberEnd() || !member->value.IsBool())
          return defaultValue;
        return member->value.GetBool();
      }

      inline const char* getStringWithDefault(const rapidjson::Value& val, const char* name, const char* defaultValue)
      {
        auto member = val.FindMember(name);
        if (member == val.MemberEnd() || !member->value.IsString())
          return defaultValue;
        return member->value.GetString();
      }
    }
  }
}
