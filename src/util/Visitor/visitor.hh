// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <tuple>

namespace bold
{
  namespace util
  {
    template<typename T>
    struct Visitor;

    template<typename T>
    struct Visitor<std::tuple<T>>
    {
      virtual void visit(T& visitable) = 0;
    };
    
    template<typename T, typename... Types>
    struct Visitor<std::tuple<T, Types...>> : public Visitor<std::tuple<Types...>>
    {
      using Visitor<std::tuple<Types...>>::visit;
      virtual void visit(T& visitable) = 0;
    };

    template<typename T>
    class Visitable
    {
    public:
      virtual void accept(Visitor<T>& visitor) = 0;
    };

    template<typename Derived, typename T>
    class VisitableImpl : public Visitable<T>
    {
    public:
      void accept(Visitor<T>& visitor) override
      {
        visitor.visit(static_cast<Derived&>(*this));
      }
    };


    template<typename T>
    struct ConstVisitor;

    template<typename T>
    struct ConstVisitor<std::tuple<T>>
    {
      virtual void visit(T const& visitable) = 0;
    };
    
    template<typename T, typename... Types>
    struct ConstVisitor<std::tuple<T, Types...>> : public ConstVisitor<std::tuple<Types...>>
    {
      using ConstVisitor<std::tuple<Types...>>::visit;
      virtual void visit(T const& visitable) = 0;
    };

    template<typename T>
    class ConstVisitable
    {
    public:
      virtual void accept(ConstVisitor<T>& visitor) const = 0;
    };

    template<typename Derived, typename T>
    class ConstVisitableImpl : public ConstVisitable<T>
    {
    public:
      void accept(ConstVisitor<T>& visitor) const override
      {
        visitor.visit(static_cast<Derived&>(*this));
      }
    };
  }
}
