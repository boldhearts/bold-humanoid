// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "gyrocalibrator.hh"

#include "CM730Snapshot/cm730snapshot.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using namespace Eigen;

GyroCalibrator::GyroCalibrator()
    : TypedStateObserver<HardwareState>("Gyro calibrator", ThreadId::MotionLoop),
      d_windowSize(100),
      d_avgValues(d_windowSize),
      d_standardDeviations(0.2),
      d_calibrationStatus(GyroCalibrationState::CalibrationState::PREPARING) {
  reset();
}

void GyroCalibrator::reset() {
  d_calibrationStatus = GyroCalibrationState::CalibrationState::PREPARING;
  d_zeroRateLevel = Vector3i(-1, -1, -1);
}

void GyroCalibrator::observeTyped(std::shared_ptr<HardwareState const> const &hardwareState, SequentialTimer &timer) {
  if (d_calibrationStatus == GyroCalibrationState::CalibrationState::COMPLETE)
    return;

  auto const &gyroRaw = hardwareState->getCM730State().gyroRaw;
  Log::verbose("GyroCalibrator::observeTyped") << "New raw value: " << gyroRaw.transpose();

  d_avgValues.next(gyroRaw.cast<double>());

  if (!d_avgValues.isMature())
    return;

  auto stdDevs = d_avgValues.calculateStdDev();

  Log::verbose("GyroCalibrator::observeTyped") << "Mature, stdDevs: " << stdDevs.transpose();

  if ((stdDevs.array() < d_standardDeviations).all()) {
    auto avgs = d_avgValues.getAverage();
    d_zeroRateLevel.x() = round(avgs.x());
    d_zeroRateLevel.y() = round(avgs.y());
    d_zeroRateLevel.z() = round(avgs.z());

    d_calibrationStatus = GyroCalibrationState::CalibrationState::COMPLETE;

    Log::info("GyroCalibrator::observeTyped") << "Gyro calibrated with centers at " << d_zeroRateLevel.transpose();

    State::make<GyroCalibrationState>(d_zeroRateLevel);
  } else {
    // Reset. We might have been moving and we want to ensure that no
    // samples in the window are considered when calculating the true
    // zero point.
    reset();

    d_calibrationStatus = GyroCalibrationState::CalibrationState::ERRORED;
  }
}
