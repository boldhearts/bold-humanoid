// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "stats/movingaverage.hh"
#include "state/StateObject/GyroCalibrationState/gyrocalibrationstate.hh"

namespace bold {
  namespace state {
    class HardwareState;
  }

  class GyroCalibrator : public state::TypedStateObserver<state::HardwareState> {
  public:
    GyroCalibrator();

    void observeTyped(std::shared_ptr<state::HardwareState const> const &hardwareState,
                      util::SequentialTimer &timer) override;

    void reset();

  private:
    int d_windowSize;
    MovingAverage<Eigen::Vector3d> d_avgValues;
    double d_standardDeviations;

    state::GyroCalibrationState::CalibrationState d_calibrationStatus;
    Eigen::Vector3i d_zeroRateLevel;
  };
}
