// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "stabilityobserver.hh"

#include "BodyModel/bodymodel.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/StabilityState/stabilitystate.hh"
#include "geometry2/Operator/Predicate/Contains/contains.hh"
#include "geometry2/Operator/ConvexHull/andrewconvexhull.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace bold::geometry2;
using namespace std;
using namespace Eigen;

StabilityObserver::StabilityObserver(shared_ptr<BodyModel> bodyModel)
    : TypedStateObserver<AccelerometerState>{"Stability Observer", ThreadId::MotionLoop},
      d_bodyModel{move(bodyModel)} {}

void StabilityObserver::observeTyped(shared_ptr<AccelerometerState const> const &state, SequentialTimer &timer) {
  auto acc = state->gs();

  auto bodyState = State::get<BodyState>();

  // For now assume double support
  auto rightFootPoly = d_bodyModel->getFootSolePolygon();
  auto leftFootPoly = d_bodyModel->getFootSolePolygon();
  for (auto &coord : leftFootPoly)
    coord.x() = -coord.x();

  // Transform into torso frame
  auto supportPolyVerts = Point3d::Vector{};

  for (auto const &coord : rightFootPoly)
    supportPolyVerts.push_back(
        Point3d::ORIGIN + bodyState->getLimb("right-foot")->getTransform() * (coord.toDim<3>() - Point3d::ORIGIN));
  for (auto const &coord : leftFootPoly)
    supportPolyVerts.push_back(
        Point3d::ORIGIN + bodyState->getLimb("left-foot")->getTransform() * (coord.toDim<3>() - Point3d::ORIGIN));

  // Find convex hull projected on torso XY plane
  auto supportPoly2d = Polygon3d{supportPolyVerts}.toDim<2>();

  auto supportPolyHull2d = AndrewConvexHull<double>{}(supportPoly2d);


  // Now bring back to full 3D again
  auto supportPolyHull3dVerts = Point3d::Vector{supportPolyHull2d.coords().size()};
  std::transform(std::begin(supportPolyHull2d), std::end(supportPolyHull2d), std::begin(supportPolyHull3dVerts),
                 [&supportPolyVerts](Point2d const &point2d) {
                   return *std::find_if(std::begin(supportPolyVerts), std::end(supportPolyVerts),
                                        [&point2d](Point3d const &point3d) {
                                          return point3d.toDim<2>() == point2d;
                                        });
                 });

  auto supportPolyHull3d = Polygon3d{supportPolyHull3dVerts};

  auto supportPolyCenter = supportPolyHull3d.center();

  // Determine plane that foot sole is in. The foot transform has the
  // axes of the foot reference system relative to torso reference
  // system.
  auto planeNormal = Vector3d{bodyState->getLimb("left-foot")->getTransform().matrix().col(2).head<3>()};

  auto pointOnPlane = supportPolyHull3dVerts[0] - Point3d::ORIGIN;

  auto plane = Hyperplane<double, 3>(planeNormal, pointOnPlane);

  // Find where acceleration ray from torso center intersects foot
  // sole plane
  auto line = ParametrizedLine<double, 3>(bodyState->getCentreOfMass() - Point3d::ORIGIN, acc);
  auto intersection = Point3d::ORIGIN + line.intersectionPoint(plane);

  auto stable = contains(supportPolyHull2d, intersection.toDim<2>());

  // Stability based on distance from stance polygon center
  State::make<StabilityState>(stable, intersection - supportPolyCenter, intersection);
}
