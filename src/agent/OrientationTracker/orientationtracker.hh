// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "geometry2/Orientation/orientation.hh"

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace bold {
  namespace config {
    template<typename>
    class Setting;
  }

  enum class OrientationTechnique {
    Madgwick = 0,
    Sum = 1
  };

  /** Tracks the orientation of the torso using data from the IMU.
   */
  class OrientationTracker : public state::TypedStateObserver<state::HardwareState> {
  public:
    OrientationTracker();

    Eigen::Quaterniond const &getQuaternion() const;

    void reset();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  private:
    void observeTyped(std::shared_ptr<state::HardwareState const> const &state, util::SequentialTimer &timer) override;

    void updateMadgwick(std::shared_ptr<state::HardwareState const> const &state);

    void updateSum(std::shared_ptr<state::HardwareState const> const &state);

    // estimated orientation
    geometry2::Orientation3d d_orientation;

    config::Setting<OrientationTechnique> *d_technique;
  };
}
