// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "orientationtracker.hh"

#include "config/Config/config.hh"
#include "state/StateObject/OrientationState/orientationstate.hh"

#include "roundtable/Action/action.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::roundtable;
using namespace bold::util;
using namespace std;
using Eigen::Quaterniond;

// sampling period in seconds
#define deltat 0.008

// gyroscope measurement error in rad/s (shown as 5 deg/s)
#define gyroMeasError 3.14159265358979f * (5.0f / 180.0f)

// compute beta
#define beta sqrt(3.0f / 4.0f) * gyroMeasError

OrientationTracker::OrientationTracker()
    : TypedStateObserver<HardwareState>("Orientation tracker", ThreadId::MotionLoop),
      d_technique(Config::getSetting<OrientationTechnique>("orientation-tracker.technique")) {
  reset();
  Action::addAction("orientation-tracker.zero", "Zero", [this] { reset(); });
}

void OrientationTracker::reset() {
  d_orientation.reset();
}

Quaterniond const &OrientationTracker::getQuaternion() const {
  return d_orientation.getQuaternion();
}

void OrientationTracker::observeTyped(shared_ptr<HardwareState const> const &state, SequentialTimer &timer) {
  switch (d_technique->getValue()) {
    case OrientationTechnique::Madgwick:
      updateMadgwick(state);
      break;

    case OrientationTechnique::Sum:
      updateSum(state);
      break;

    default:
      Log::error("OrientationTracker::observeTyped") << "Unexpected OrientationTechnique value: "
                                                     << (int) d_technique->getValue();
      throw runtime_error("Unexpected OrientationTechnique value");
  }

  State::make<OrientationState>(getQuaternion());
}

void OrientationTracker::updateMadgwick(shared_ptr<HardwareState const> const &state) {
  //
  // Using technique from paper:
  //
  //   "An efficient orientation filter for inertial and inertial/magnetic sensor arrays"
  //
  //   Sebastian O.H. Madgwick, April 30, 2010
  //

  auto const &gyro = state->getCM730State().gyro;
  auto const &acc = state->getCM730State().acc.normalized();

  d_orientation.integrate(gyro, deltat, acc, gyroMeasError);
}

void OrientationTracker::updateSum(shared_ptr<HardwareState const> const &state) {
  auto const &gyro = state->getCM730State().gyro;
  d_orientation.integrate(gyro, deltat);
}
