// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "buttonobserver.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ButtonTracker::observe(bool isPressed, Clock const &clock) {
  if (isPressed == d_lastPressedState)
    return;

  if (isPressed) {
    // New press
    d_downAt = clock.getTimestamp();
    d_isClaimed = false;
  }

  d_lastPressedState = isPressed;
}

bool ButtonTracker::isPressedForMillis(double millis, Clock const &clock) {
  // Check if the button is down
  if (!d_lastPressedState)
    return false;

  // Check whether a client has already claimed this button press
  if (d_isClaimed)
    return false;

  // Check whether the button has been held for long enough
  if (clock.getMillisSince(d_downAt) >= millis) {
    d_isClaimed = true;
    return true;
  }

  return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ButtonObserver::ButtonObserver()
    : TypedStateObserver<HardwareState>("Button observer", ThreadId::MotionLoop) {}

std::shared_ptr<ButtonTracker> ButtonObserver::track(Button button) {
  // TODO there is currently no way to stop tracking a button -- we don't need that yet though

  auto tracker = make_shared<ButtonTracker>(button);
  d_trackers.push_back(tracker);
  return tracker;
}

void ButtonObserver::observeTyped(shared_ptr<HardwareState const> const &hardwareState, SequentialTimer &timer) {
  auto clock = SystemClock{};
  for (auto const &tracker : d_trackers) {
    tracker->observe(
        tracker->getButton() == Button::Left
        ? hardwareState->getCM730State().isModeButtonPressed
        : hardwareState->getCM730State().isStartButtonPressed, clock);
  }
}
