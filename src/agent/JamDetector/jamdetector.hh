// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "stats/movingaverage.hh"
#include "src/util/SchmittTrigger/schmitttrigger.hh"

namespace bold {
  namespace config {
    template<typename>
    class Setting;
  }

  class Voice;

  class JamDetector : public state::TypedStateObserver<state::BodyState> {
  public:
    JamDetector(std::shared_ptr<Voice> voice);

    void observeTyped(std::shared_ptr<state::BodyState const> const &bodyState, util::SequentialTimer &timer) override;

  private:
    struct JointErrorTracker {
      JointErrorTracker(int lowThreshold, int highThreshold, int windowSize)
          : trigger(lowThreshold, highThreshold, false),
            movingAverage(windowSize) {}

      util::SchmittTrigger<int> trigger;
      MovingAverage<int> movingAverage;
    };

    std::vector<JointErrorTracker> d_trackerByJointId;
    std::shared_ptr<Voice> d_voice;
    config::Setting<bool> *d_enabled;
  };
}
