// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "jamdetector.hh"

#include "config/Config/config.hh"
#include "Voice/voice.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;

JamDetector::JamDetector(shared_ptr<Voice> voice)
    : TypedStateObserver<BodyState>("Jam detector", ThreadId::MotionLoop),
      d_trackerByJointId(),
      d_voice(voice),
      d_enabled(Config::getSetting<bool>("hardware.jam-detector.enabled")) {
  int lowThreshold = Config::getStaticValue<int>("hardware.jam-detector.low-threshold");
  int highThreshold = Config::getStaticValue<int>("hardware.jam-detector.high-threshold");
  int windowSize = Config::getStaticValue<int>("hardware.jam-detector.window-size");

  for (uint8_t jointId = (uint8_t) JointId::MIN; jointId <= (int) JointId::MAX; jointId++)
    d_trackerByJointId.emplace_back(lowThreshold, highThreshold, windowSize);
}

void JamDetector::observeTyped(std::shared_ptr<BodyState const> const &bodyState, SequentialTimer &timer) {
  const auto &diffById = bodyState->getPositionValueDiffById();

  ASSERT(diffById.size() == (int) JointId::MAX + 1);

  // TODO Don't just detect large error, but also the fact that the joint is hardly moving
  //      This is because during some motion script playback, there may be very large errors for a short while
  //      Alternative is to increase the window length

  for (uint8_t jointId = (uint8_t) JointId::MIN; jointId <= (int) JointId::MAX; jointId++) {
    auto &tracker = d_trackerByJointId[jointId - 1];

    int diff = diffById[jointId];
    int avg = tracker.movingAverage.next(diff);

    if (!tracker.movingAverage.isMature())
      continue;

    auto transition = tracker.trigger.next(avg);

    if (transition == SchmittTriggerTransition::High) {
      Log::warning("JamDetector::observeTyped") << "Prolonged deviation of joint " << (int) jointId << " ("
                                                << JointName::getEnumName(jointId) << ") at avg level of " << avg
                                                << " over the last "
                                                << Config::getStaticValue<int>("hardware.jam-detector.window-size")
                                                << " cycles";
      d_voice->say("Help, my " + JointName::getNiceName(jointId) + " joint is stuck!");

      // TODO actually trigger a defensive movement
    } else if (transition == SchmittTriggerTransition::Low) {
      Log::info("JamDetector::observeTyped") << "Joint " << (int) jointId << " is now unstuck";
      d_voice->say("That's better");
    }
  }
}
