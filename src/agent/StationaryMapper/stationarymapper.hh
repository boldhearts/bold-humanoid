// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/stateobserver.hh"
#include "state/StateObject/StationaryMapState/stationarymapstate.hh"

#include <Eigen/Core>

namespace bold {
  class BehaviourControl;

  class Voice;

  class StationaryMapper : public state::StateObserver {
  public:
    StationaryMapper(std::shared_ptr<Voice> voice, std::shared_ptr<BehaviourControl> behaviourControl);

    void observe(util::SequentialTimer &timer) override;

  private:
    void updateStateObject() const;

    static void
    integrate(state::StationaryMapState::PositionEstimates &estimates, geometry2::Point2d pos, double mergeDistance);

    state::StationaryMapState::PositionEstimates d_ballEstimates;
    state::StationaryMapState::PositionEstimates d_goalEstimates;
    state::StationaryMapState::PositionEstimates d_teammateEstimates;
    state::RadialOcclusionMap d_occlusionMap;
    bool d_hasData;
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<BehaviourControl> d_behaviourControl;
  };
}
