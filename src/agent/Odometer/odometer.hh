// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "state/StateObject/BodyState/bodystate.hh"

namespace bold
{
  /** Calculates cumulative translation of the torso over time.
   *
   * Done by observing BodyState for knowledge of the stance foot.
   */
  class Odometer : public state::TypedStateObserver<state::BodyState>
  {
  public:
    Odometer();

    Eigen::Affine3d getTransform() const;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  
  private:
    void observeTyped(std::shared_ptr<state::BodyState const> const& state, util::SequentialTimer& timer) override;

    std::shared_ptr<state::BodyState const> d_lastBodyState;
    Eigen::Affine3d d_transform;
    mutable std::mutex d_transformMutex;
  };
}
