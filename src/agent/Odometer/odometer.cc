// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "odometer.hh"

#include "config/Config/config.hh"
#include "state/StateObject/RobotisWalkState/robotiswalkstate.hh"
#include "state/StateObject/OdometryState/odometrystate.hh"

#include "roundtable/Action/action.hh"
#include "util/assert.hh"

using namespace bold;
using namespace bold::roundtable;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using Eigen::Affine3d;

Odometer::Odometer()
: TypedStateObserver<BodyState>("Odometer", ThreadId::MotionLoop),
  d_lastBodyState{},
  d_transform{Affine3d::Identity()}
{
  State::make<OdometryState>(d_transform);

  Action::addAction("odometer.reset", "Reset odometer", [this]
  {
    lock_guard<mutex> lock(d_transformMutex);
    d_transform = Affine3d::Identity();
  });
}

void Odometer::observeTyped(shared_ptr<BodyState const> const& state, SequentialTimer& timer)
{
  ASSERT(state);

  auto walkState = State::get<RobotisWalkState>();

  if (!walkState || !walkState->isRunning())
  {
    d_lastBodyState = nullptr;
    return;
  }

  if (d_lastBodyState)
  {
    // Measure delta of movement

    // AtA0 = AtAt-1 * At-1A0
    //      = AtFt * FtAt-1 * At-1A0
    //      = AtFt * FtFt-1 * Ft-1At-1 * At-1A0
    //      = AtFt * Ft-1At-1 * At-1A0
    auto leftFootAgentTr = state->determineFootAgentTr(true);
    auto rightFootAgentTr = state->determineFootAgentTr(false);

    // Translation is location of agent/torso in foot frame, so stance/lowest foot has highest z
    bool isLeftSupportFoot = leftFootAgentTr.translation().z() > rightFootAgentTr.translation().z();

    auto lastFootAgentTr = d_lastBodyState->determineFootAgentTr(isLeftSupportFoot);
    auto agentFootTr = isLeftSupportFoot ? leftFootAgentTr.inverse() : rightFootAgentTr.inverse();

    lock_guard<mutex> lock(d_transformMutex);
    d_transform = agentFootTr * lastFootAgentTr * d_transform;

    State::make<OdometryState>(d_transform);
  }

  d_lastBodyState = state;
}

Eigen::Affine3d Odometer::getTransform() const {
  lock_guard<mutex> lock(d_transformMutex);
  return d_transform;
}
