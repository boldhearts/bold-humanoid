// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "vocaliser.hh"

#include "config/Config/config.hh"
#include "Voice/voice.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;

Vocaliser::Vocaliser(shared_ptr<Voice> voice)
    : TypedStateObserver<AgentFrameState>("Vocaliser", ThreadId::ThinkLoop),
      d_enableBallPos(Config::getSetting<bool>("vocaliser.announce-ball-position")),
      d_voice(voice) {}

void Vocaliser::observeTyped(std::shared_ptr<AgentFrameState const> const &agentFrameState, SequentialTimer &timer) {
  if (d_enableBallPos->getValue()) {
    if (int(fmod(agentFrameState->getThinkCycleNumber(), 4 * 30)) == 0 && d_voice->queueLength() == 0)
    {
      if (agentFrameState->isBallVisible()) {
        auto const &ballPos = agentFrameState->getBallObservation();

        stringstream s;
        s << int(fabs(ballPos->x() * 100)) << (ballPos->x() > 0 ? "right " : "left ");
        s << int(fabs(ballPos->y() * 100)) << (ballPos->y() > 0 ? "forward" : "backward");

        d_voice->say(SpeechTask({s.str(), 180, true}));
      } else {
        d_voice->say("no ball");
      }
    }
  }
}
