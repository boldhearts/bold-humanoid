// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "orientationfalldetector.hh"

#include "config/Config/config.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;

OrientationFallDetector::OrientationFallDetector(shared_ptr<Voice> voice, shared_ptr<Clock> clock)
    : FallDetector(voice, clock),
      TypedStateObserver<OrientationState>("Fall detector", ThreadId::MotionLoop) {}

void OrientationFallDetector::observeTyped(std::shared_ptr<OrientationState const> const &orientationState,
                                           SequentialTimer &timer) {
  double pitch = orientationState->getPitchAngle();
  double roll = orientationState->getRollAngle();

  static auto pitchThreshold = Config::getSetting<double>("fall-detector.orientation.pitch-threshold");
  static auto rollThreshold = Config::getSetting<double>("fall-detector.orientation.roll-threshold");

  double pitchBreach = pitchThreshold->getValue() - fabs(pitch);
  double rollBreach = rollThreshold->getValue() - fabs(roll);

  if (pitchBreach > 0 && rollBreach > 0) {
    setFallState(FallState::STANDUP);
  } else if (pitchBreach < rollBreach) {
    setFallState(pitch < 0 ? FallState::FORWARD : FallState::BACKWARD);
  } else {
    setFallState(roll > 0 ? FallState::LEFT : FallState::RIGHT);
  }
}

void OrientationFallDetector::logFallData(stringstream &msg) const {
  auto const &orientationState = State::get<OrientationState>();

  msg << round(Math::radToDeg(orientationState->getPitchAngle())) << ","
      << round(Math::radToDeg(orientationState->getRollAngle())) << ","
      << round(Math::radToDeg(orientationState->getYawAngle()));
}
