// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "falldetector.hh"

#include "Voice/voice.hh"
#include "JointId/jointid.hh"
#include "state/State/state.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"

#include <iomanip>

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

typedef unsigned char uint8_t;

FallDetector::FallDetector(shared_ptr<Voice> voice, shared_ptr<Clock> clock)
  : d_voice(move(voice)),
    d_clock(move(clock)),
    d_fallenState(FallState::STANDUP),
    d_startTime(d_clock->getTimestamp())
{}

void FallDetector::setFallState(FallState fallState)
{
  // TODO add some hysteresis here to avoid flickering between states (seen as multiple consecutive fall-data log entries)

  bool standingBefore = d_fallenState == FallState::STANDUP;

  d_fallenState = fallState;

  if (standingBefore && fallState != FallState::STANDUP)
  {
    auto const& hardwareState = State::get<HardwareState>();

    ASSERT(hardwareState);

    // Log a bunch of data when a fall is detected
    stringstream msg;
    msg << setprecision(3) << d_clock->getSecondsSince(d_startTime) << ","
      << getFallStateName(fallState) << ","
      << hardwareState->getCM730State().voltage;

    for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
      msg << "," << (int)hardwareState->getMX28State(jointId).presentTemp;

    msg << ",";
    logFallData(msg);

    Log::info("fall-data") << msg.str();

    // Announce the fall
    if (d_voice->queueLength() == 0)
      d_voice->sayOneOf({"Ouch", "Dammit", "Ooopsy", "Bah", "Why me", "Not again", "That hurt"});
  }
}

string bold::getFallStateName(FallState fallState)
{
  switch (fallState)
  {
    case FallState::STANDUP:
      return "Standup";
    case FallState::BACKWARD:
      return "Backward";
    case FallState::FORWARD:
      return "Forward";
    case FallState::LEFT:
      return "Left";
    case FallState::RIGHT:
      return "Right";
    default:
      return "Unknown";
  }
}
