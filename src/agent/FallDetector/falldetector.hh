// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include "Clock/clock.hh"

namespace bold {
  class Voice;

  enum class FallState {
    STANDUP,
    BACKWARD,
    FORWARD,
    LEFT,
    RIGHT
  };

  enum class FallDetectorTechnique {
    Accelerometer = 1,
    Orientation = 2
  };

  std::string getFallStateName(FallState fallState);

  class FallDetector {
  public:
    FallState getFallenState() const { return d_fallenState; }

  protected:
    FallDetector(std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock);

    ~FallDetector() = default;

    void setFallState(FallState fallState);

    virtual void logFallData(std::stringstream &msg) const = 0;

  private:
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;

    FallState d_fallenState;
    Clock::Timestamp d_startTime;
  };
}
