// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "agent/FallDetector/falldetector.hh"

#include "state/StateObserver/typedstateobserver.hh"
#include "stats/movingaverage.hh"

namespace bold {
  namespace state {
    class HardwareState;
  }

  class AccelerometerFallDetector : public FallDetector, public state::TypedStateObserver<state::HardwareState> {
  public:
    AccelerometerFallDetector(std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock);

    void observeTyped(std::shared_ptr<state::HardwareState const> const &hardwareState,
                      util::SequentialTimer &timer) override;

  protected:
    void logFallData(std::stringstream &msg) const override;

  private:
    MovingAverage<double> d_xAvg;
    MovingAverage<double> d_yAvg;
    MovingAverage<double> d_zAvg;
  };
}
