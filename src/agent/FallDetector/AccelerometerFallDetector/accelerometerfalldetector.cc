// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "accelerometerfalldetector.hh"

#include "CM730/cm730.hh"
#include "config/Config/config.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;

AccelerometerFallDetector::AccelerometerFallDetector(shared_ptr <Voice> voice, shared_ptr <Clock> clock)
    : FallDetector(voice, clock),
      TypedStateObserver<HardwareState>("Fall detector", ThreadId::MotionLoop),
      d_xAvg((uint16_t) Config::getStaticValue<int>("fall-detector.accelerometer.window-size")),
      d_yAvg((uint16_t) Config::getStaticValue<int>("fall-detector.accelerometer.window-size")),
      d_zAvg((uint16_t) Config::getStaticValue<int>("fall-detector.accelerometer.window-size")) {}

void AccelerometerFallDetector::observeTyped(std::shared_ptr<HardwareState const> const &hardwareState,
                                             SequentialTimer &timer) {
  // Track the smoothed acceleration along each axis to test for a consistent
  // indication that we have fallen.

  auto const &accRaw = hardwareState->getCM730State().accRaw;

  auto xAvg = d_xAvg.next(accRaw.x());
  auto yAvg = d_yAvg.next(accRaw.y());
  auto zAvg = d_zAvg.next(accRaw.z());

  if (d_xAvg.isMature()) {
    ASSERT(d_yAvg.isMature() && d_zAvg.isMature());

    if (abs(zAvg) > abs(xAvg) && abs(zAvg) > abs(yAvg)) {
      // NOTE could actually be standing on our head, but this is quite unlikely :)
      setFallState(FallState::STANDUP);
    } else if (abs(xAvg) > abs(yAvg)) {
      setFallState(xAvg < 0 ? FallState::RIGHT : FallState::LEFT);
    } else {
      setFallState(yAvg < 0 ? FallState::FORWARD : FallState::BACKWARD);
    }
  }
}

void AccelerometerFallDetector::logFallData(stringstream &msg) const {
  msg << d_xAvg.getAverage() << ","
      << d_yAvg.getAverage() << ","
      << d_zAvg.getAverage();
}
