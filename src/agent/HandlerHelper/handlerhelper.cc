// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "handlerhelper.hh"

#include "Voice/voice.hh"
#include "BehaviourControl/behaviourcontrol.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

HandlerHelper::HandlerHelper(shared_ptr<Voice> voice, shared_ptr<Clock> clock, shared_ptr<BehaviourControl> behaviourControl)
: TypedStateObserver<GameState>("Handler Helper", ThreadId::ThinkLoop),
  d_voice(move(voice)),
  d_clock(move(clock)),
  d_behaviourControl(behaviourControl),
  d_lastPenaltyLiftAnnounceTime(0)
{
  ASSERT(d_voice);
}

void HandlerHelper::observeTyped(shared_ptr<GameState const> const& state, SequentialTimer& timer)
{
  // State may be null if the game controller has been lost for a while and we decide to forget its prior state
  if (!state)
    return;

  // Announce when our penalty period is over, in case the assistant referee has failed to unpenalise us
  auto const& myPlayerInfo = state->getMyPlayerInfo();
  if (myPlayerInfo.hasPenalty() && myPlayerInfo.getPenaltyType() != PenaltyType::SUBSTITUTE)
  {
    if (myPlayerInfo.getSecondsUntilPenaltyLifted() == 0)
    {
      if (d_clock->getSecondsSince(d_lastPenaltyLiftAnnounceTime) > 10)
      {
        d_voice->say("Penalty complete");
        d_lastPenaltyLiftAnnounceTime = d_clock->getTimestamp();
      }
    }
  }

  // Announce when game play mode is initial/ready/set and robot is paused
  if (d_behaviourControl->getPlayerStatus() == PlayerStatus::Paused)
  {
    switch (state->getPlayMode())
    {
      case PlayMode::INITIAL:
      case PlayMode::READY:
      case PlayMode::SET:
        if (d_clock->getSecondsSince(d_lastPauseAtGameStartTime) > 10)
        {
          d_voice->say("I am paused and the game is starting");
          d_lastPauseAtGameStartTime = d_clock->getTimestamp();
        }
        break;
      default:
        break;
    }
  }
}
