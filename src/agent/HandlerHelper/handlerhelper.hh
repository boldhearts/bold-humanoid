// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"

namespace bold
{
  class BehaviourControl;
  class Voice;

  namespace state {
    class GameState;
  }
  /**
  * Attempts to catch things that the robot handler may have missed, such as a robot being paused
  * as the game is starting, or a penalty period elapsing that the assistant referee has missed.
  */
  class HandlerHelper : public state::TypedStateObserver<state::GameState>
  {
  public:
    HandlerHelper(std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock, std::shared_ptr<BehaviourControl> behaviourControl);

    void observeTyped(std::shared_ptr<state::GameState const> const& state, util::SequentialTimer& timer) override;

  private:
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;

    std::shared_ptr<BehaviourControl> d_behaviourControl;
    Clock::Timestamp d_lastPenaltyLiftAnnounceTime;
    Clock::Timestamp d_lastPauseAtGameStartTime;
  };
}
