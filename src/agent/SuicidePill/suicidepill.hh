// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "Clock/clock.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"

namespace bold {
  class Agent;

  class Debugger;

  /** Kills the agent when requested by the robot handler.
   *
   * Holding both the start and mode button down for N seconds will exit the
   * process. Holding them both for M (M > N) seconds will stop the
   * boldhumanoid service.
   */
  class SuicidePill : public state::TypedStateObserver<state::HardwareState> {
  public:
    SuicidePill(Agent *agent,
                std::shared_ptr<Clock> clock,
                std::shared_ptr<Debugger> debugger);

  private:
    void observeTyped(std::shared_ptr<state::HardwareState const> const &state,
                      util::SequentialTimer &timer) override;

    Agent *const d_agent;
    std::shared_ptr<Clock> d_clock;
    std::shared_ptr<Debugger> d_debugger;
    bool d_isActive;
    Clock::Timestamp d_pressedAt;
    bool d_exited;
  };
}
