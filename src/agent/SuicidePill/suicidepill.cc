// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "suicidepill.hh"

#include "Agent/agent.hh"
#include "Debugger/debugger.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

SuicidePill::SuicidePill(Agent *agent,
                         shared_ptr<Clock> clock,
                         shared_ptr<Debugger> debugger)
    : TypedStateObserver<HardwareState>{"Suicide Pill", ThreadId::MotionLoop},
      d_agent{agent},
      d_clock{move(clock)},
      d_debugger{move(debugger)},
      d_isActive{false},
      d_pressedAt{0},
      d_exited{false} {
  ASSERT(agent);
  ASSERT(d_debugger);
}

void SuicidePill::observeTyped(shared_ptr<HardwareState const> const &state, SequentialTimer &timer) {
  if (d_exited)
    return;

  const double stopProcessAfterSeconds = 3.0;
  const double shutdownAfterSeconds = 6.0;

  auto const &cm730 = state->getCM730State();

  bool showDazzle = false;

  if (cm730.isStartButtonPressed && cm730.isModeButtonPressed) {
    // Both buttons pressed

    if (d_isActive) {
      double seconds = d_clock->getSecondsSince(d_pressedAt);

      if (seconds > shutdownAfterSeconds) {
        Log::warning("SuicidePill::observeTyped") << "Both buttons held for " << shutdownAfterSeconds
                                                  << " seconds. Shutdown.";
        int res = system("shutdown now");
        if (res != 0)
          Log::error("SuicidePill::observeTyped") << "System call to shutdown exited with: " << res;
        d_exited = true;
      } else if (seconds > stopProcessAfterSeconds) {
        showDazzle = true;
      }
    } else {
      Log::info("SuicidePill::observeTyped") << "Both buttons pressed. Waiting...";
      d_isActive = true;
      d_pressedAt = d_clock->getTimestamp();
    }
  } else {
    // Neither button pressed

    if (d_isActive) {
      d_isActive = false;

      double seconds = d_clock->getSecondsSince(d_pressedAt);

      if (seconds > stopProcessAfterSeconds && seconds < shutdownAfterSeconds) {
        Log::warning("SuicidePill::observeTyped") << "Both buttons held for " << stopProcessAfterSeconds
                                                  << " seconds. Stopping process.";
        d_agent->stop();
      }
    }
  }

  d_debugger->showDazzle(showDazzle);
}
