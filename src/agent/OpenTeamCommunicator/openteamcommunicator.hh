// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>

#include "Clock/clock.hh"
#include "state/StateObject/TeamState/teamstate.hh"
#include "state/StateObserver/stateobserver.hh"

namespace bold {
  class BehaviourControl;

  namespace config {
    template<typename>
    class Setting;
  }

  namespace util {
    class SequentialTimer;
  }

  class OpenTeamCommunicator : public state::StateObserver {
  public:
    OpenTeamCommunicator(std::shared_ptr<BehaviourControl> behaviourControl,
                         std::shared_ptr<Clock> clock);

    void observe(util::SequentialTimer &timer) override;

    void receiveData();

    void sendData(state::PlayerState &state);

  private:
    static state::PlayerRole decodePlayerRole(int value);

    static state::PlayerActivity decodePlayerActivity(int value);

    static state::PlayerStatus decodePlayerStatus(int value);

    static int encodePlayerRole(state::PlayerRole role);

    static int encodePlayerActivity(state::PlayerActivity activity);

    static int encodePlayerStatus(state::PlayerStatus status);

    void mergePlayerState(state::PlayerState &state);

    void updateStateObject();

    std::shared_ptr<BehaviourControl> d_behaviourControl;
    std::shared_ptr<Clock> d_clock;

    const uint8_t d_teamNumber;
    const uint8_t d_uniformNumber;
    const int d_localPort;
    const int d_remotePort;
    config::Setting<double> *d_sendPeriodSeconds;
    config::Setting<int> *d_maxPlayerDataAgeMillis;
    Clock::Timestamp d_lastBroadcast;
    int d_sock;
    std::vector<state::PlayerState> d_players;
  };
}
