// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "healthandsafety.hh"

#include "CM730Snapshot/cm730snapshot.hh"
#include "config/Config/config.hh"
#include "MX28Snapshot/mx28snapshot.hh"
#include "state/StateObserver/typedstateobserver.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "Voice/voice.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;

HealthAndSafety::HealthAndSafety(shared_ptr<Voice> voice, shared_ptr<Clock> clock)
: TypedStateObserver<HardwareState>("Health & Safety", ThreadId::MotionLoop),
  d_voice(move(voice)),
  d_clock(move(clock)),
  d_voltageMovingAverage((uint16_t)Config::getStaticValue<int>("health-and-safety.voltage.smoothing-window-size")),
  d_voltageTrigger(
    (float)Config::getStaticValue<double>("health-and-safety.voltage.low-threshold"),
    (float)Config::getStaticValue<double>("health-and-safety.voltage.high-threshold"),
    true),
  d_temperatureThreshold(Config::getSetting<int>("health-and-safety.temperature.high-threshold"))
{
  ASSERT(d_voice);

  d_lastTempByJoint.fill((uint8_t)0);

  int tempSmoothingWindowSize = Config::getStaticValue<int>("health-and-safety.temperature.smoothing-window-size");
  for (int i = 0; i <= (int)JointId::MAX; i++)
    d_averageTempByJoint.emplace_back(tempSmoothingWindowSize);
}

void HealthAndSafety::observeTyped(shared_ptr<HardwareState const> const& state, SequentialTimer& timer)
{
  processVoltage(state);
  processTemperature(state);
}

void HealthAndSafety::processVoltage(shared_ptr<HardwareState const> const& state)
{
  float voltage = d_voltageMovingAverage.next(state->getCM730State().voltage);

  switch (d_voltageTrigger.next(voltage))
  {
    case SchmittTriggerTransition::High:
    {
      // Was probably plugged into a charger
      if (d_voice)
        d_voice->sayOneOf({"Thank you", "Ooo yeah."}); // haha
      Log::info("HealthAndSafety::observeTyped") << "Voltage level restored above " << d_voltageTrigger.getHighThreshold() << " volts";
      break;
    }
    case SchmittTriggerTransition::Low:
    {
      if (d_voice)
        d_voice->say("Low voltage warning.");
      Log::warning("HealthAndSafety::observeTyped") << "Voltage level dropped below " << d_voltageTrigger.getLowThreshold() << " volts";
      d_lastVoltageWarningTime = d_clock->getTimestamp();
      break;
    }
    case SchmittTriggerTransition::None:
    {
      if (!d_voltageTrigger.isHigh() && d_clock->getSecondsSince(d_lastVoltageWarningTime) > 20)
      {
        if (d_voice)
          d_voice->say("My voltage is still low.");
        d_lastVoltageWarningTime = d_clock->getTimestamp();
      }
      break;
    }
  }
}

void HealthAndSafety::processTemperature(shared_ptr<HardwareState const> const& state)
{
  const uint8_t tempThreshold = (uint8_t)d_temperatureThreshold->getValue();

  stringstream msg;

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId < (uint8_t)JointId::MAX; jointId++)
  {
    const uint8_t presentTemp = state->getMX28State(jointId).presentTemp;
    const uint8_t temp = (uint8_t)round(d_averageTempByJoint[jointId].next(presentTemp));
    const uint8_t lastTemp = d_lastTempByJoint[jointId];

    // Require a movement of two degrees before making an announcement, avoiding noise on transitions between degrees
    if (abs(temp - lastTemp) < 2)
      continue;

    d_lastTempByJoint[jointId] = temp;

    if (temp > lastTemp)
    {
      // Temperature is increasing
      if (temp >= tempThreshold)
      {
        // We are now above the threshold
        if (lastTemp < tempThreshold)
        {
          // First time above the threshold
          msg << "My " << JointName::getNiceName(jointId) << " is " << (int)temp << " degrees. ";
          Log::warning("HealthAndSafety::processTemperature") << JointName::getNiceName(jointId) << " (" << (int)jointId << ") is " << (int)temp << "°C";
        }
        else
        {
          // Was previously above threshold, and now increased
          msg << "My " << JointName::getNiceName(jointId) << " has increased to " << (int)temp << " degrees. ";
          Log::warning("HealthAndSafety::processTemperature") << JointName::getNiceName(jointId) << " (" << (int)jointId << ") increased to " << (int)temp << "°C";
        }
      }
    }
    else
    {
      // Temperature is decreasing
      ASSERT(temp < lastTemp);

      if (temp >= tempThreshold)
      {
        // We are still above the threshold, but decreased
        msg << "My " << JointName::getNiceName(jointId) << " has decreased to " << (int)temp << " degrees. ";
        Log::warning("HealthAndSafety::processTemperature") << JointName::getNiceName(jointId) << " (" << (int)jointId << ") decreased to " << (int)temp << "°C";
      }
      else if (lastTemp >= tempThreshold)
      {
        // Was previously above threshold, and now below
        msg << "My " << JointName::getNiceName(jointId) << " has decreased to " << (int)temp << " degrees, below threshold. ";
        Log::info("HealthAndSafety::processTemperature") << JointName::getNiceName(jointId) << " (" << (int)jointId << ") is below threshold again at " << (int)temp << "°C";
      }
    }
  }

  if (msg.tellp() != 0)
    d_voice->say(msg.str());
}
