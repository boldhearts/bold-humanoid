// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObserver/typedstateobserver.hh"
#include "Clock/clock.hh"
#include "stats/movingaverage.hh"
#include "util/SchmittTrigger/schmitttrigger.hh"
#include "JointId/jointid.hh"

#include <array>
#include <vector>

namespace bold {
  class Voice;

  namespace config {
    template<typename T>
    class Setting;
  }

  namespace state {
    class HardwareState;
  }

  class HealthAndSafety : public state::TypedStateObserver<state::HardwareState> {
  public:
    HealthAndSafety(std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock);

    void observeTyped(std::shared_ptr<state::HardwareState const> const &state, util::SequentialTimer &timer) override;

  private:
    void processVoltage(std::shared_ptr<state::HardwareState const> const &state);

    void processTemperature(std::shared_ptr<state::HardwareState const> const &state);

    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;

    MovingAverage<float> d_voltageMovingAverage;
    util::SchmittTrigger<float> d_voltageTrigger;
    Clock::Timestamp d_lastVoltageWarningTime;

    std::vector<MovingAverage<float>> d_averageTempByJoint;
    std::array<uint8_t, (int) JointId::MAX + 1> d_lastTempByJoint;
    config::Setting<int> *d_temperatureThreshold;
  };
}
