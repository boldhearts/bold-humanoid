// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "hsv.hh"
#include "colour/BGR/bgr.hh"
#include "colour/colour.hh"

using namespace bold::colour;

BGR HSV::toBGR() const
{
  int const   nh = 256;         // Maximum hue value
  int const   hstep = nh / 3;   // Hue step size between red -> green -> blue

  int r, g, b;

  if (s == 0)
  {
    // Achromatic case, set level of grey
    r = g = b = v;
  }
  else
  {
    int chroma = s * v / 255;

    int I = 2 * h / hstep; // [0-5]
    if (I > 5)
      I = 5;
    
    int F = h % hstep; // [0-hstep)

    assert (I >= 0 && I <= 5);

    int X = chroma * (255 - abs(2 * 255 * F / hstep - 255)) / 255;

    switch (I)
    {
    case 0:
      r = chroma;
      g = X;
      b = 0;
      break;
    case 1:
      r = X;
      g = chroma;
      b = 0;
      break;
    case 2:
      r = 0;
      g = chroma;
      b = X;
      break;
    case 3:
      r = 0;
      g = X;
      b = chroma;
      break;
    case 4:
      r = X;
      g = 0;
      b = chroma;
      break;
    case 5:
      r = chroma;
      g = 0;
      b = X;
      break;
    default:
      // Should never hit this
      ASSERT(false && "IMPOSSIBLE CASE");
      r = g = b = 0;
    }

    int M = v - chroma;
    r += M;
    g += M;
    b += M;
  }

  return BGR(b, g, r);
}
