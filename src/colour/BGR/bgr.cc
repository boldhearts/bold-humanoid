// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bgr.hh"

#include "colour/HSV/hsv.hh"
#include "util/assert.hh"

using namespace bold::colour;

cv::Scalar BGR::toScalar() const
{
  return cv::Scalar(b, g, r);
}

HSV BGR::toHSV() const
{
  int const hstep = 255 / 3;   // Hue step size between red -> green -> blue

  int min = r < g ? r : g;
  min = min  < b ? min  : b;

  int max = r > g ? r : g;
  max = max  > b ? max  : b;

  int chroma = max - min;

  HSV out;

  if (max > 0)
  {
    out.s = 255 * chroma / max;         // s
  }
  else
  {
    // r = g = b = 0                    // s = 0, v is undefined
    out.s = 0;
    out.h = 0;
    out.v = 0; // it's now undefined
    return out;
  }

  out.v = max;                          // v

  if (chroma == 0)
  {
    out.h = 0;
    return out;
  }

  const int chroma2 = chroma * 2;
  int offset;
  int diff;

  if (r == max)
  {
    offset = 3 * hstep;
    diff = g - b;
  }
  else if (g == max)
  {
    offset = hstep;
    diff = b - r;
  }
  else
  {
    offset = 2 * hstep;
    diff = r - g;
  }

  int h = offset + (diff * (hstep + 1)) / chroma2;

  // Rotate such that red has hue 0
  if (h >= 255) // TODO would this be faster using mod?
    h -= 255;

  ASSERT(h >= 0 && h < 256);

  out.h = h;

  return out;
}

Eigen::Vector3d BGR::toRgbUnitVector() const
{
  return Eigen::Vector3d(r/255.0, g/255.0, b/255.0);
}
