// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <opencv2/core/core.hpp>
#include <Eigen/Core>
#include <iosfwd>

namespace bold
{
  namespace colour
  {
    struct HSV;
    
    struct BGR
    {
      constexpr BGR()
        : b{0}, g{0}, r{0}
      {}
      
      constexpr BGR(uint8_t b, uint8_t g, uint8_t r)
       : b{b}, g{g}, r{r}
      {}

      constexpr BGR invert() const
      {
        return BGR(255-b, 255-g, 255-r);
      }

      cv::Scalar toScalar() const;

      HSV toHSV() const;

      /** Return RGB floating point vector
       *
       * Returns RGB channel order rather than BGR, and each channel is scaled by 1/255
       * TODO: Rename? A unit vector has length 1, this does not normalize to 1.
       */
      Eigen::Vector3d toRgbUnitVector() const;

      constexpr bool operator==(BGR const& other) const
      {
        return
          b == other.b &&
          g == other.g &&
          r == other.r;
      }
      
      uint8_t b;
      uint8_t g;
      uint8_t r;

      static constexpr BGR black() {return BGR(0,0,0); }
      static constexpr BGR grey() { return BGR(128, 128, 128);}
      static constexpr BGR white() { return BGR(255, 255, 255);}
      static constexpr BGR red() { return BGR(0, 0, 128);}
      static constexpr BGR lightRed() { return BGR(0, 0, 255);}
      static constexpr BGR darkRed() { return BGR(0, 0, 64);}
      static constexpr BGR green() { return BGR(0, 128, 0);}
      static constexpr BGR lightGreen() { return BGR(0, 255, 0);}
      static constexpr BGR darkGreen() { return BGR(0, 64, 0);}
      static constexpr BGR blue() { return BGR(128, 0, 0);}
      static constexpr BGR lightBlue() { return BGR(255, 0, 0);}
      static constexpr BGR darkBlue() { return BGR(64, 0, 0);}
      static constexpr BGR orange() { return BGR(255, 128, 0);}
      static constexpr BGR purple() { return BGR(0x85, 0x24, 0x79);} // BH purple
      static constexpr BGR yellow() { return BGR(0, 255, 255);}
      static constexpr BGR cyan() { return BGR(255, 255, 0);}
      static constexpr BGR magenta() { return BGR(255, 0, 255);}
    };

    std::ostream& operator<<(std::ostream &stream, BGR const& bgr);
  }
}
