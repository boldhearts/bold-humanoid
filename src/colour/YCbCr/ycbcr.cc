// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ycbcr.hh"

#include "colour/BGR/bgr.hh"

using namespace std;
using namespace bold::colour;

bool YCbCr::isValid() const
{
  return y >= 16 && y <= 235
      && cb >= 16 && cb <= 240
      && cr >= 16 && cr <= 240;
}

BGR YCbCr::toBGRInt() const
{
  // YCrCb coefficients
  int c0 = 22987;
  int c1 = -11698;
  int c2 = -5636;
  int c3 = 29049;
  
  int y = this->y;
  int cb = this->cb - 128;
  int cr = this->cr - 128;

  int b = y + (((c3 * cb) + (1 << 13)) >> 14);
  int g = y + (((c2 * cb + c1 * cr) + (1 << 13)) >> 14);
  int r = y + (((c0 * cr) + (1 << 13)) >> 14);

  if (r < 0)
    r = 0;
  else if (r > 255)
    r = 255;

  if (g < 0)
    g = 0;
  else if (g > 255)
    g = 255;

  if (b < 0)
    b = 0;
  else if (b > 255)
    b = 255;

  return BGR(b, g, r);
}

BGR YCbCr::toBGRFloat() const
{
  float y = this->y;
  float cb = this->cb;
  float cr = this->cr;

  int r = y + 1.40200 * (cr - 0x80);
  int g = y - 0.34414 * (cb - 0x80) - 0.71414 * (cr - 0x80);
  int b = y + 1.77200 * (cb - 0x80);

  if (r < 0)
    r = 0;
  else if (r > 255)
    r = 255;

  if (g < 0)
    g = 0;
  else if (g > 255)
    g = 255;

  if (b < 0)
    b = 0;
  else if (b > 255)
    b = 255;

  return BGR(b, g, r);
}

ostream& bold::colour::operator<<(ostream& out, YCbCr const& ycbcr)
{
  return out << unsigned{ycbcr.y} << " " << unsigned{ycbcr.cb} << " " << unsigned{ycbcr.cr};
}
