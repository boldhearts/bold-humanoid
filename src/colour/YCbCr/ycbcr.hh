// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <iosfwd>

namespace bold
{
  namespace colour
  {
    struct BGR;
    
    struct YCbCr
    {
      // http://www.equasys.de/colorconversion.html

      YCbCr() = default;
      YCbCr(uint8_t y, uint8_t cb, uint8_t cr)
        : y{y}, cb{cb}, cr{cr}
      {}

      /** Check validity
       *
       * For valid YCbCr colour it must hold that: 16 <= y <= 235,
       * 16 <= Cb <= 240, 16 <= Cr <= 240.
       */
      bool isValid() const;

      /** Convert to BGR using integer arithmetic
       */
      BGR toBGRInt() const;
      
      /** Convert to BGR using floating point arithmetic
       */
      BGR toBGRFloat() const;

      bool operator==(YCbCr const& other) const
      {
        return
          y  == other.y  &&
          cb == other.cb &&
          cr == other.cr;
      }

      uint8_t y;
      uint8_t cb;
      uint8_t cr;
    };

    std::ostream& operator<<(std::ostream& out, YCbCr const& ycbcr);
  }
}
