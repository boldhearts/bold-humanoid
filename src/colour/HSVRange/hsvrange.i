%{
#include <colour/HSVRange/hsvrange.hh>
%}

namespace bold
{
  namespace colour
  {
    struct HSVRange
    {
      uint8_t hMin;
      uint8_t hMax;
      uint8_t sMin;
      uint8_t sMax;
      uint8_t vMin;
      uint8_t vMax;
    };
  }
}
