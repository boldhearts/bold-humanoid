// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <iosfwd>

namespace bold
{
  namespace colour
  {
    struct HSV;
    struct BGR;
    
    struct HSVRange
    {
      uint8_t hMin;
      uint8_t hMax;
      uint8_t sMin;
      uint8_t sMax;
      uint8_t vMin;
      uint8_t vMax;

      HSVRange() = default;

      HSVRange(
        uint8_t hMin, uint8_t hMax,
        uint8_t sMin, uint8_t sMax,
        uint8_t vMin, uint8_t vMax);

      uint8_t getHMid() const;
      uint8_t getSMid() const;
      uint8_t getVMid() const;

      /** Obtain the center colour for the range, in HSV space
       */
      HSV toHSV() const;

      /** Obtain the center colour for the range, in BGR space
       */
      BGR toBGR() const;

      bool contains(HSV const& hsv) const;

      bool isValid() const;

      bool operator==(HSVRange const& other) const
      {
        return
          hMin == other.hMin &&
          hMax == other.hMax &&
          sMin == other.sMin &&
          sMax == other.sMax &&
          vMin == other.vMin &&
          vMax == other.vMax;
      }

      static HSVRange fromBytes(uint8_t hMin, uint8_t hMax, uint8_t sMin, uint8_t sMax, uint8_t vMin, uint8_t vMax);
      static HSVRange fromDoubles(double hMin, double hMax, double sMin, double sMax, double vMin, double vMax);

      HSVRange withHMin(uint8_t value) const { return HSVRange(value, hMax, sMin, sMax, vMin, vMax); }
      HSVRange withHMax(uint8_t value) const { return HSVRange(hMin, value, sMin, sMax, vMin, vMax); }
      HSVRange withSMin(uint8_t value) const { return HSVRange(hMin, hMax, value, sMax, vMin, vMax); }
      HSVRange withSMax(uint8_t value) const { return HSVRange(hMin, hMax, sMin, value, vMin, vMax); }
      HSVRange withVMin(uint8_t value) const { return HSVRange(hMin, hMax, sMin, sMax, value, vMax); }
      HSVRange withVMax(uint8_t value) const { return HSVRange(hMin, hMax, sMin, sMax, vMin, value); }

      HSVRange containing(HSV const& hsv);

    };

    std::ostream& operator<<(std::ostream &stream, HSVRange const& hsv);
  }
}
