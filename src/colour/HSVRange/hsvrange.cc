// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "hsvrange.hh"

#include "colour/BGR/bgr.hh"
#include "colour/HSV/hsv.hh"
#include "util/assert.hh"
#include "Math/math.hh"

using namespace bold;
using namespace bold::colour;

HSVRange::HSVRange(
  uint8_t hMin, uint8_t hMax,
  uint8_t sMin, uint8_t sMax,
  uint8_t vMin, uint8_t vMax)
  : hMin(hMin), hMax(hMax),
    sMin(sMin), sMax(sMax),
    vMin(vMin), vMax(vMax)
{
  // NOTE if hMin > hMax, then the hue value wraps around 360 degrees (255 in 8-bit form)
  ASSERT(sMin <= sMax);
  ASSERT(vMin <= vMax);
}

HSV HSVRange::toHSV() const
{
  return HSV(getHMid(), getSMid(), getVMid());
}

BGR HSVRange::toBGR() const
{
  return toHSV().toBGR();
}

uint8_t HSVRange::getHMid() const
{
  // The hue midpoint is a little trickier than the S or V midpoints, as
  // hue may wrap around
  if (hMin <= hMax)
  {
    return hMin + ((hMax-hMin)/2);
  }

  int hMinInt = hMin;
  int hMaxInt = hMax;

  int mid = hMaxInt + ((hMinInt+255-hMaxInt)/2);
  while (mid > 255)
    mid -= 255;
  ASSERT(mid >= 0);
  return mid;
}

uint8_t HSVRange::getSMid() const
{
  return sMin + ((sMax-sMin)/2);
}

uint8_t HSVRange::getVMid() const
{
  return vMin + ((vMax-vMin)/2);
}

bool HSVRange::contains(HSV const& hsv) const
{
  if (hMin <= hMax)
  {
    // Hue is not wrapped
    if (hsv.h < hMin || hsv.h > hMax)
      return false;
  }
  else
  {
    // Hue is wrapped
    if (hsv.h < hMin && hsv.h > hMax)
      return false;
  }

  return hsv.s >= sMin && hsv.s <= sMax &&
         hsv.v >= vMin && hsv.v <= vMax;
}

bool HSVRange::isValid() const
{
  return sMin <= sMax && vMin <= vMax;
}

HSVRange HSVRange::fromBytes(uint8_t hMin, uint8_t hMax, uint8_t sMin, uint8_t sMax, uint8_t vMin, uint8_t vMax)
{
  return HSVRange(hMin, hMax, sMin, sMax, vMin, vMax);
}

HSVRange HSVRange::fromDoubles(double hMin, double hMax, double sMin, double sMax, double vMin, double vMax)
{
  ASSERT(hMin >= 0.0 && hMin <= 360.0);
  ASSERT(hMax >= 0.0 && hMax <= 360.0);
  ASSERT(sMin >= 0.0 && sMin <= 1.0);
  ASSERT(sMax >= 0.0 && sMax <= 1.0);
  ASSERT(vMin >= 0.0 && vMin <= 1.0);
  ASSERT(vMax >= 0.0 && vMax <= 1.0);

  return HSVRange((hMin/360.0)*255, (hMax/360)*255, sMin*255, sMax*255, vMin*255, vMax*255);
}

HSVRange HSVRange::containing(HSV const& hsv)
{
  HSVRange res = *this;
  if ((hMin <= hMax && (hsv.h < hMin || hsv.h > hMax)) ||
      (hMin > hMax && (hsv.h < hMin && hsv.h > hMax)))
  {
    auto d1 = Math::angleDiffRads(hsv.h * M_PI / 128.0, hMax * M_PI / 128.0);
    auto d2 = Math::angleDiffRads(hMin * M_PI / 128.0, hsv.h * M_PI / 128.0);
    if (d1 < d2)
      res.hMin = hsv.h;
    else
      res.hMax = hsv.h;
  }

  res.sMin = std::min(sMin, hsv.s);
  res.sMax = std::max(sMax, hsv.s);
  res.vMin = std::min(vMin, hsv.v);
  res.vMax = std::max(vMax, hsv.v);
  return res;
}

