// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "BGR/bgr.hh"
#include "HSV/hsv.hh"
#include "HSVRange/hsvrange.hh"

#include <iomanip>

using namespace bold;
using namespace std;

ostream& bold::colour::operator<<(ostream &stream, colour::BGR const& bgr)
{
  return stream <<  "B=" << (unsigned int)bgr.b
                << " G=" << (unsigned int)bgr.g
                << " R=" << (unsigned int)bgr.r;
}

ostream& bold::colour::operator<<(ostream &stream, colour::HSV const& hsv)
{
  return stream <<  "H=" << (unsigned int)hsv.h
                << " S=" << (unsigned int)hsv.s
                << " V=" << (unsigned int)hsv.v;
}

ostream& bold::colour::operator<<(ostream &stream, colour::HSVRange const& hsvRange)
{
  return stream << setprecision(2)
                <<  "H=" << (int)(360*(hsvRange.hMin/255.0)) << "->" << (int)(360*(hsvRange.hMax/255.0))
                << " S=" << (hsvRange.sMin/255.0) << "->" << (hsvRange.sMax/255.0)
                << " V=" << (hsvRange.vMin/255.0) << "->" << (hsvRange.vMax/255.0)
                << setprecision(6);
}
