// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Operator/operator.hh"
#include "util/Maybe/maybe.hh"
#include "geometry2/Geometry/LineSegment/linesegment.hh"
#include "geometry2/Geometry/MultiPoint/Polygon/polygon.hh"
#include "geometry2/Operator/Predicate/Contains/contains.hh"

#include <Eigen/LU>

namespace bold
{
  namespace geometry2
  {
    class Intersection : public Operator<Intersection>
    {
    public:
      template<typename T>
      util::Maybe<Point<T, 2>> apply(LineSegment<T, 2> const& a, LineSegment<T, 2> const& b, double* t = nullptr, double* u = nullptr, double e = 1e-6) const
      {
        Point<double, 2> pos1 = a.p1().template cast<double>();
        Point<double, 2> pos2 = b.p1().template cast<double>();
        Eigen::Matrix<double, 2, 1> dir1 = a.delta().template cast<double>();
        Eigen::Matrix<double, 2, 1> dir2 = b.delta().template cast<double>();

        auto denom = (Eigen::Matrix2d() << dir1, dir2).finished().determinant();

        if (denom == 0)
          return util::Maybe<Point<T, 2>>::empty();

        Eigen::Matrix<double, 2, 1> diff = pos2 - pos1;
        auto t_numer = (Eigen::Matrix2d() << diff, dir2).finished().determinant();
        auto u_numer = (Eigen::Matrix2d() << diff, dir1).finished().determinant();

        auto _t = t_numer / denom;
        auto _u = u_numer / denom;

        if (t != nullptr)
          *t = _t;
        if (u != nullptr)
          *u = _u;

        if (_t < -e || _t > 1 + e || _u < -e || _u > 1 + e)
          return util::Maybe<Point<T, 2>>::empty();

        auto result = pos1 + dir1 * _t;
        if (std::is_integral<T>::value)
        {
          auto rounded = result.unaryExpr([](double v) { return round(v); });
          return util::make_maybe(Point<T, 2>{rounded.template cast<T>()});
        }
        else
          return util::make_maybe(Point<T, 2>{result.template cast<T>()});
      }

      template<typename T>
      util::Maybe<LineSegment<T, 2>> apply(Polygon<T, 2> const& polygon, LineSegment<T, 2> const& lineSegment) const
      {
        auto contains1 = contains(polygon, lineSegment.p1());
        auto contains2 = contains(polygon, lineSegment.p2());
    
        // Line is completely inside the polygon.
        // As we're convex, there cannot be any intersection.
        if (contains1 && contains2)
          return util::make_maybe(lineSegment);
    
        auto intersectionPoints = typename Point<T, 2>::Vector{};
        for (unsigned i = 0, j = polygon.coords().size() - 1; i < polygon.coords().size(); j = i++)
        {
          auto const& a = polygon.coords()[i];
          auto const& b = polygon.coords()[j];
          LineSegment<T, 2> l(a, b);
          auto result = apply(l, lineSegment);
          if (result.hasValue())
          {
            // Ensure unique members of the list
            if (intersectionPoints.size() > 0 && intersectionPoints[0] == result.value())
              continue;
            if (intersectionPoints.size() > 1 && intersectionPoints[1] == result.value())
              continue;

            intersectionPoints.push_back(std::move(result.value()));

            // In a convex polygon, we should never see more than two intersections
            if (intersectionPoints.size() == 2)
              break;
          }
        }

        // Line is completely outside and does not intersect with polygon
        if (intersectionPoints.size() == 0)
          return util::Maybe<LineSegment<T, 2>>::empty();

        // Line is completely outside and intersects with polygon
        if (intersectionPoints.size() == 2)
          return util::make_maybe(LineSegment<T, 2>(intersectionPoints[0], intersectionPoints[1]));

        // One end of the lineSegment is outside, and we have a single intersection
        ASSERT(intersectionPoints.size() == 1);
        if (contains1)
          return util::make_maybe(LineSegment<T, 2>(lineSegment.p1(), intersectionPoints[0]));
        else
          return util::make_maybe(LineSegment<T, 2>(intersectionPoints[0], lineSegment.p2()));
      }
    };

    template<typename T1, typename T2>
    auto intersection(T1 const& a, T2 const& b)
    {
      return Intersection()(a, b);
    }
  }
}
