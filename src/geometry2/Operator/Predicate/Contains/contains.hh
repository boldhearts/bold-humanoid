// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Operator/Predicate/predicate.hh"
#include "geometry2/Geometry/MultiPoint/Polygon/polygon.hh"

namespace bold
{
  namespace geometry2
  {
    class Contains : public Predicate<Contains>
    {
    public:      
      template<typename T>
      bool eval(Polygon<T, 2> const& polygon, Point<T, 2> const& point) const
      {
        bool isInside = false;
        for (unsigned i = 0, j = polygon.coords().size() - 1; i < polygon.coords().size(); j = i++)
        {
          auto const& a = polygon.coords()[i];
          auto const& b = polygon.coords()[j];
      
          if (((a.y() > point.y()) != (b.y() > point.y()))
              && (point.x() < (b.x() - a.x()) * (point.y() - a.y()) / (b.y() - a.y()) + a.x()))
          {
            isInside = !isInside;
          }
        }
        return isInside;
      }
    };
        
    template<typename T1, typename T2>
    bool contains(T1 const& a, T2 const& b)
    {
      return Contains()(a, b);
    }
  }
}
