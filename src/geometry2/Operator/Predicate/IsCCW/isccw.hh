// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Operator/Predicate/predicate.hh"
#include "geometry2/Geometry/MultiPoint/LineString/linestring.hh"

namespace bold
{
  namespace geometry2
  {
    class IsCCW : public Predicate<IsCCW>
    {
    public:
      template<typename T>
      bool eval(LineString<T, 2> const& lineString) const;

      template<typename InputIterator>
      bool eval(InputIterator first, InputIterator last) const;

      template<typename T>
      bool eval(Point<T, 2> const& p1, Point<T, 2> const& p2, Point<T, 2> const& p3) const;

    private:
      template<typename T>
      double ccw(Point<T, 2> const& p1, Point<T, 2> const& p2, Point<T, 2> const& p3) const;
    };


    template<typename T>
    bool IsCCW::eval(LineString<T, 2> const& lineString) const
    {
      return eval(std::begin(lineString), std::end(lineString));
    }
 
    template<typename InputIterator>
    bool IsCCW::eval(InputIterator first, InputIterator last) const
    {
      if (std::distance(first, last) < 3)
        return false;

      auto pp2 = std::next(first);
      auto pp3 = std::next(pp2);
      
      for (;pp3 != last; ++first, ++pp2, ++pp3)
      {
        if (ccw(*first, *pp2, *pp3) <= 0)
          return false;
      }
      
      return true;
    }
    
    template<typename T>
    bool IsCCW::eval(Point<T, 2> const& p1, Point<T, 2> const& p2, Point<T, 2> const& p3) const
    {
      return ccw(p1, p2, p3) > 0;
    }

    template<typename T>
    double IsCCW::ccw(Point<T, 2> const& p1, Point<T, 2> const& p2, Point<T, 2> const& p3) const
    {
      auto d1 = p2 - p1;
      auto d2 = p3 - p1;
      return d1.x() * d2.y() - d1.y() * d2.x();
    }
  }
}
