// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "convexhull.hh"

namespace bold
{
  namespace geometry2
  {
    template<typename T>
    class AndrewConvexHull : public ConvexHull<AndrewConvexHull<T>>
    {
    public:
      Polygon<T, 2> buildHull(MultiPoint<T, 2> const& geom)
      {
        // Sort coords by x coordinate, and y in case of tie
        auto coords = geom.coords();

        std::sort(std::begin(coords), std::end(coords),
                  [](Point<T, 2> const& a, Point<T, 2> const& b) {
                    return a.x() < b.x() || (a.x() == b.x() && a.y() < b.y());
                  });

        auto hullCoords = typename Point<T, 2>::Vector{coords.size() * 2};

        auto isCCW = IsCCW();

        unsigned k = 0;

        // Build lower hull
        for (int i = 0; i < coords.size(); ++i)
        {
          while (k >= 2 && !isCCW(hullCoords[k - 2], hullCoords[k - 1], coords[i]))
            --k;
          hullCoords[k++] = coords[i];
        }

        d_lowerUpperPivot = k - 1;
        
        // Build upper hull
        int t = k + 1;
        for (int i = coords.size() - 2; i >= 0; --i)
        {
          while (k >= t && !isCCW(hullCoords[k - 2], hullCoords[k - 1], coords[i]))
            --k;
          hullCoords[k++] = coords[i];
        }

        hullCoords.resize(k - 1);

        return Polygon<T, 2>{hullCoords};
      }

      unsigned getLowerUpperPivot() const
      {
        return d_lowerUpperPivot;
      }
      
    private:
      unsigned d_lowerUpperPivot;
    };
  }
}
