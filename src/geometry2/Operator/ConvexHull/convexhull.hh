// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Operator/operator.hh"
#include "geometry2/Operator/Predicate/IsCCW/isccw.hh"
#include "geometry2/Geometry/MultiPoint/multipoint.hh"
#include "geometry2/Geometry/MultiPoint/Polygon/polygon.hh"
#include "geometry2/Geometry/LineSegment/linesegment.hh"

#include <algorithm>
#include <utility>
#include <iostream>

namespace bold
{
  namespace geometry2
  {

    template<typename Derived>
    class ConvexHull : public Operator<ConvexHull<Derived>>
    {
    public:
      template<typename Arg>
      auto apply(Arg&& arg)
      {
        return static_cast<Derived*>(this)->buildHull(std::forward<Arg>(arg));
      }
    };
  }
}
