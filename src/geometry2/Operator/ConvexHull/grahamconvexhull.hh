// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "convexhull.hh"

namespace bold
{
  namespace geometry2
  {
    template<typename T>
    class GrahamConvexHull : public ConvexHull<GrahamConvexHull<T>>
    {
    public:
      Polygon<T, 2> buildHull(MultiPoint<T, 2> const& geom) const
      {
        auto coords = geom.coords();
        
        // Graham scan
        auto lowest = std::min_element(coords.begin(), coords.end(), [](Point<T, 2> const& a, Point<T, 2> const& b) {
            return a.y() != b.y() ? a.y() < b.y() : a.x() < b.x();
          });

        std::swap(coords[0], *lowest);

        // Determine polar angle from coords[0]
        auto coordsWithPolarAngles = std::vector<std::pair<Point<T, 2>, double>>(coords.size());
        auto firstPoint = coords[0];
        std::transform(coords.begin(), coords.end(), coordsWithPolarAngles.begin(), [&firstPoint](Point<T, 2> const& c) {
            if (c == firstPoint)
              return std::make_pair(c, 0.0);
            return std::make_pair(c, LineSegment<T, 2>(firstPoint, c).angle());
          });

        // sort coords by polar angle with coords[0]
        std::sort(coordsWithPolarAngles.begin(), coordsWithPolarAngles.end(),
                  [](std::pair<Point<T, 2>, double> const& a, std::pair<Point<T, 2>, double> const& b) {
                    return a.second < b.second;
                  });

        std::transform(coordsWithPolarAngles.begin(), coordsWithPolarAngles.end(), coords.begin(),
                       [](std::pair<Point<T, 2>, double> const& pa) {
                         return pa.first;
                       });
        
        // We want coords[0] to be a sentinel point that will stop the loop.
        // The last point is always on convex hull
        coords.insert(coords.begin(), coords.back());

        auto isCCW = IsCCW();
        unsigned nHullCoords = 1;
        for (size_t i = 2; i < coords.size(); ++i)
        {
          // Find next valid point on convex hull
          while (isCCW(LineString<T, 2>{coords[nHullCoords - 1],
                                        coords[nHullCoords],
                                        coords[i]}) <= 0)
            if (nHullCoords > 1)
              nHullCoords -= 1;
            else if (i == coords.size() - 1) // All points are collinear
              break;
            else
              ++i;
          // Update nr of hull coordinates and move new point in place
          ++nHullCoords;
          std::swap(coords[nHullCoords], coords[i]);
        }

        coords.resize(nHullCoords);
        return Polygon<T, 2>{std::next(coords.begin()), std::next(coords.begin(), nHullCoords + 1)};
      }
    };
  }
}
