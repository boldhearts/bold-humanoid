// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Geometry/geometry.hh"
#include "util/meta.hh"

namespace bold
{
  namespace geometry2
  {
    template<typename T, int DIM>
    class LineSegment : public Geometry<T, DIM>
    {
    public:
      LineSegment() = default;
      LineSegment(LineSegment<T, DIM> const& other) = default;

      LineSegment(Point<T, DIM> p1, Point<T, DIM> p2);

      void initSlope();

      Eigen::AlignedBox<T, DIM> getBounds() const override;

      Point<T, DIM> const& p1() const { return d_p1; }
      Point<T, DIM> const& p2() const { return d_p2; }

      Point<T, DIM>& p1() { return d_p1; }
      Point<T, DIM>& p2() { return d_p2; }

      T length() const { return delta().norm(); }

      /** Difference between end points
       */
      Eigen::Matrix<T, DIM, 1> delta() const { return d_p2 - d_p1; }

      /** Angle line segment makes with horizontal
       */
      double angle() const;

      double slope() const;

      T heightAt(Point<T, DIM - 1> const& p);
      
      LineSegment<T, DIM> operator+(Eigen::Matrix<T, DIM, 1> const& vec) const;
      LineSegment<T, DIM> operator-(Eigen::Matrix<T, DIM, 1> const& vec) const;

      template<typename NEW_T>
      LineSegment<NEW_T, DIM> cast() const;
      
      template<int NEW_DIM>
      LineSegment<T, NEW_DIM> toDim() const;

      double angleBetween(LineSegment<T, DIM> const& other);

      bool isPointAbove(Point<T, DIM> const& p) const;
      
      static bool constexpr needsToAlign = (sizeof(Point<T, DIM>) % 16) == 0;
      
      /// Vector type with automatic alignment if required
      using Vector = typename std::conditional<
        needsToAlign,
        std::vector<LineSegment<T, DIM>, Eigen::aligned_allocator<LineSegment<T, DIM>>>,
        std::vector<LineSegment<T, DIM>>
      >::type;
                  
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF(needsToAlign);

      friend std::ostream& operator<<(std::ostream& out, LineSegment const& lineSegment)
      {
        return out << "LINESTRING (" <<
          (lineSegment.p1() - Point<T, DIM>::ORIGIN).transpose() << ", " <<
          (lineSegment.p2() - Point<T, DIM>::ORIGIN).transpose() << ")";
      }
      
    private:
      Point<T, DIM> d_p1;
      Point<T, DIM> d_p2;

      double d_slope;
    };



    template<typename T, int DIM>
    LineSegment<T, DIM>::LineSegment(Point<T, DIM> p1, Point<T, DIM> p2)
      : d_p1{p1},
        d_p2{p2}
    {
      initSlope();
    }

    template<typename T, int DIM>
    Eigen::AlignedBox<T, DIM> LineSegment<T, DIM>::getBounds() const
    {
      auto o = Point<T, DIM>::ORIGIN;
      auto mat = Eigen::Matrix<T, DIM, 2>{};
      mat << (d_p1 - o), (d_p2 - o);

      auto min = mat.rowwise().minCoeff();
      auto max = mat.rowwise().maxCoeff();
      
      return Eigen::AlignedBox<T, DIM>{min, max};
    }

    template<typename T, int DIM>
    double LineSegment<T, DIM>::angle() const
    {
      if (DIM == 2)
      {
        auto d = delta();
        return std::atan2(d.y(), d.x());
      }
      else
      {
        auto o = Point<T, DIM>::ORIGIN;
        Eigen::Matrix<T, DIM, 1> d = delta();
        auto adjacentVec = d.template head<DIM - 1>();
        auto adjacent = (d_p1 - o).norm() > (d_p2 - o).norm() ? -adjacentVec.norm() : adjacentVec.norm();
        return std::atan2(d[DIM - 1], adjacent);
      }
    }

    template<typename T, int DIM>
    void LineSegment<T, DIM>::initSlope()
    {
      auto d = delta();
      double adjacent = d.template head<DIM - 1>().norm();
      if (adjacent == 0)
        d_slope = std::numeric_limits<double>::infinity();
      else
        d_slope = d(DIM - 1) / adjacent;
    }

    
    template<typename T, int DIM>
    double LineSegment<T, DIM>::slope() const
    {
      return d_slope;
    }


    template<typename T, int DIM>
    T LineSegment<T, DIM>::heightAt(const Point<T, DIM - 1> &p)
    {
      static_assert(DIM == 2, "Not implemented for dimensions > 2");
      
      Point<T, DIM - 1> start1d = d_p1.template head<DIM - 1>();
      Point<T, DIM - 1> end1d = d_p2.template head<DIM - 1>();

      double ratio = static_cast<double>((p - start1d).norm()) / static_cast<double>((end1d - start1d).norm());
      Eigen::Matrix<double, DIM, 1> diff = (d_p2 - d_p1).template cast<double>();
  
      Point<double, DIM> atP = d_p1.template cast<double>() + ratio * diff;
  
      return atP(DIM - 1);
    }
    
    template<typename T, int DIM>
    LineSegment<T, DIM> LineSegment<T, DIM>::operator+(const Eigen::Matrix<T, DIM, 1> &vec) const
    {
      return LineSegment<T, DIM>{d_p1 + vec, d_p2 + vec};
    }
    
    template<typename T, int DIM>
    LineSegment<T, DIM> LineSegment<T, DIM>::operator-(const Eigen::Matrix<T, DIM, 1> &vec) const
    {
      return LineSegment<T, DIM>{d_p1 - vec, d_p2 - vec};
    }

    template<typename T, int DIM>
    template<typename NEW_T>
    LineSegment<NEW_T, DIM> LineSegment<T, DIM>::cast() const
    {
      return LineSegment<NEW_T, DIM>{d_p1.template cast<NEW_T>(), d_p2.template cast<NEW_T>()};
    }
    
    template<typename T, int DIM>
    template<int NEW_DIM>
    LineSegment<T, NEW_DIM> LineSegment<T, DIM>::toDim() const
    {
      return LineSegment<T, NEW_DIM>{d_p1.template toDim<NEW_DIM>(), d_p2.template toDim<NEW_DIM>()};
    }

    template<typename T, int DIM>
    bool LineSegment<T, DIM>::isPointAbove(Point<T, DIM> const &p) const
    {
      static_assert(DIM == 2, "Only 2D line segments allowed");
      
      if (p.y() > d_p1.y() && p.y() > d_p2.y())
        return true;

      if (p.y() <= d_p1.y() && p.y() <= d_p2.y())
        return false;

      auto lineYAtPoint = d_p1.y() + slope() * (p.x() - d_p1.x());
      return p.y() > lineYAtPoint;
    }
    
    template<typename T, int DIM>
    double LineSegment<T, DIM>::angleBetween(LineSegment<T, DIM> const &other)
    {
      
      auto a = fabs(acos(delta().normalized().dot(other.delta().normalized())));
      if (a >= M_PI_2)
        a = M_PI - a;
      return a; 
    }
    
    using LineSegment2i = LineSegment<int, 2>;
    using LineSegment2f = LineSegment<float, 2>;
    using LineSegment2d = LineSegment<double, 2>;
    
    using LineSegment3i = LineSegment<int, 3>;
    using LineSegment3f = LineSegment<float, 3>;
    using LineSegment3d = LineSegment<double, 3>;
  }
}
