// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Geometry/MultiPoint/multipoint.hh"

namespace bold
{
  namespace geometry2
  {
    template<typename T, int DIM>
    class LineString : public MultiPoint<T, DIM>
    {
    public:
      using MultiPoint<T, DIM>::MultiPoint;
    };

    using LineString2i = LineString<int, 2>;
    using LineString2f = LineString<float, 2>;
    using LineString2d = LineString<double, 2>;
  }
}
