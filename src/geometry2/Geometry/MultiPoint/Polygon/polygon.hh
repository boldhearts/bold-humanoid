// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Geometry/MultiPoint/multipoint.hh"
#include "geometry2/Geometry/MultiPoint/LineString/linestring.hh"

#include <limits>

namespace bold
{
  namespace geometry2
  {
    template<typename T, int DIM>
    class Polygon : public MultiPoint<T, DIM>
    {
    public:
      using MultiPoint<T, DIM>::MultiPoint;

      Polygon(Eigen::AlignedBox<T, DIM> const& box);
      
      LineString<T, DIM> boundary() const;

      template<int NEW_DIM>
      Polygon<T, NEW_DIM> toDim() const;

      friend std::ostream& operator<<(std::ostream& out, Polygon const& polygon)
      {
        out << "POLYGON ((";
        out << (polygon.coords()[0] - Point<T, DIM>::ORIGIN).transpose();
        for (size_t i = 1; i < polygon.coords().size(); ++i)
          out << ", " << (polygon.coords()[i] - Point<T, DIM>::ORIGIN).transpose();
        out << "))";
        return out;
      }
    };


    template<typename T, int DIM>
    Polygon<T, DIM>::Polygon(Eigen::AlignedBox<T, DIM> const& box)
    {
      this->d_coords.push_back(box.corner(Eigen::AlignedBox<T, DIM>::BottomLeft));
      this->d_coords.push_back(box.corner(Eigen::AlignedBox<T, DIM>::BottomRight));
      this->d_coords.push_back(box.corner(Eigen::AlignedBox<T, DIM>::TopRight));
      this->d_coords.push_back(box.corner(Eigen::AlignedBox<T, DIM>::TopLeft));
    }
    
    template<typename T, int DIM>
    LineString<T, DIM> Polygon<T, DIM>::boundary() const
    {
      auto coords = typename Point<T, DIM>::Vector{this->coords()};
      coords.push_back(coords.front());
      return LineString<T, DIM>{coords};
    }

    template<typename T, int DIM>
    template<int NEW_DIM>
    Polygon<T, NEW_DIM> Polygon<T, DIM>::toDim() const
    {
      typename Point<T, NEW_DIM>::Vector newCoords = typename Point<T, NEW_DIM>::Vector{this->coords().size()};
      std::transform(this->coords().begin(), this->coords().end(),
                     newCoords.begin(),
                     [](Point<T, DIM> const& p) {
                       return p.template toDim<NEW_DIM>();
                     });
      return Polygon<T, NEW_DIM>{newCoords};
    }

    using Polygon2i = Polygon<int, 2>;
    using Polygon2f = Polygon<float, 2>;
    using Polygon2d = Polygon<double, 2>;
    using Polygon3d = Polygon<double, 3>;
  }
}
