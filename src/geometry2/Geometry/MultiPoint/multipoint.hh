// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "geometry2/Geometry/geometry.hh"

namespace bold {
  namespace geometry2
  {
    template<typename T, int DIM>
    class MultiPoint : public Geometry<T, DIM>
    {
    public:
      MultiPoint() = default;
      MultiPoint(typename Point<T, DIM>::Vector coords);

      template<typename InputIterator>
      MultiPoint(InputIterator first, InputIterator last);

      MultiPoint(std::initializer_list<Point<T, DIM>> coords);

      Eigen::AlignedBox<T, DIM> getBounds() const override;

      typename Point<T, DIM>::Vector const& coords() const;

      using iterator = typename Point<T, DIM>::Vector::iterator;
      using const_iterator = typename Point<T, DIM>::Vector::const_iterator;

      iterator begin();
      iterator end();

      const_iterator begin() const;
      const_iterator end() const;
      
    protected:
      typename Point<T, DIM>::Vector d_coords;    
    };

    
    template<typename T, int DIM>
    MultiPoint<T, DIM>::MultiPoint(typename Point<T, DIM>::Vector coords)
      : d_coords{std::move(coords)}
    {}

    template<typename T, int DIM>
    template<typename InputIterator>
    MultiPoint<T, DIM>::MultiPoint(InputIterator first, InputIterator last)
      : d_coords(std::move(first), std::move(last))
    {}

    template<typename T, int DIM>
    MultiPoint<T, DIM>::MultiPoint(std::initializer_list<Point<T, DIM>> coords)
      : d_coords{std::move(coords)}
    {}

    template<typename T, int DIM>
    typename Point<T, DIM>::Vector const& MultiPoint<T, DIM>::coords() const
    {
      return d_coords;
    }

    template<typename T, int DIM>
    typename MultiPoint<T, DIM>::iterator MultiPoint<T, DIM>::begin()
    {
      return d_coords.begin();
    }
    
    template<typename T, int DIM>
    typename MultiPoint<T, DIM>::iterator MultiPoint<T, DIM>::end()
    {
      return d_coords.end();
    }

    template<typename T, int DIM>
    typename MultiPoint<T, DIM>::const_iterator MultiPoint<T, DIM>::begin() const
    {
      return d_coords.begin();
    }
    
    template<typename T, int DIM>
    typename MultiPoint<T, DIM>::const_iterator MultiPoint<T, DIM>::end() const
    {
      return d_coords.end();
    }
    
    template<typename T, int DIM>
    Eigen::AlignedBox<T, DIM> MultiPoint<T, DIM>::getBounds() const
    {
      auto coords = Eigen::Matrix<T, DIM, Eigen::Dynamic>{};
      coords.resize(DIM, d_coords.size());
      for (size_t c = 0; c < d_coords.size(); ++c)
        coords.col(c) = d_coords[c] - Point<T, DIM>::ORIGIN;

      auto min = coords.rowwise().minCoeff();
      auto max = coords.rowwise().maxCoeff();
      return Eigen::AlignedBox<T, DIM>(min, max);
    }

    using MultiPoint2i = MultiPoint<int, 2>;
    using MultiPoint2f = MultiPoint<float, 2>;
    using MultiPoint2d = MultiPoint<double, 2>;
  }
}
