// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>
#include <Eigen/Core>
#include "util/meta.hh"

namespace bold
{
  namespace geometry2
  {
    template<typename T, int DIM>
    class Point : private Eigen::Matrix<T, DIM, 1>
    {
      using Base = Eigen::Matrix<T, DIM, 1>;

      class OriginPoint {
      public:
        Point operator+(Base vec) const
        {
          return vec;
        }

        Point operator-(Base vec) const
        {
          return vec;
        }

        bool operator==(Point const& p) const
        {
          return p == Point::Zero();
        }
      };

    public:

      static const OriginPoint ORIGIN;

      // TODO: too broad?
      using Base::Base;
#ifndef EIGEN_DONT_ALIGN
      using Base::operator new;
      using Base::operator delete;
#endif
      using Base::cast;
      using Base::array;
      using Base::unaryExpr;
      
      using Base::x;
      using Base::y;
      using Base::z;
      using Base::w;

      using Base::head;
      using Base::tail;

      using Base::fill;
      using Base::Zero;

      using Base::operator();
      
      bool operator==(Point const& other) const
      {
        return
          *static_cast<Base const*>(this) ==
          static_cast<Base const&>(other);
      }
      

      Eigen::CwiseBinaryOp<Eigen::internal::scalar_difference_op<T>,
                           Base const,
                           Base const> const
      operator-(Point const& other) const
      {
        return
          *static_cast<Base const*>(this) -
          static_cast<Base const&>(other);
      }

      Base const& operator-(OriginPoint const& origin) const
      {
        return *this;
      }

      
      Point operator+(Base const& vec) const
      {
        return
          *static_cast<Base const*>(this) +
          static_cast<Base const&>(vec);
      }

      Point operator-(Base const& vec) const
      {
        return *this + (-vec);
      }

      template<int NEW_DIM>
      Point<T, NEW_DIM> toDim() const
      {
        auto constexpr MIN_DIM = meta::min<DIM, NEW_DIM>::value;
        
        Point<T, NEW_DIM> p = Point<T, NEW_DIM>::Zero();
        p.template head<MIN_DIM>() = this->template head<MIN_DIM>();
        return p;
      }
      
      friend std::ostream& operator<<(std::ostream& out, Point const& point)
      {
        return out << "POINT (" << static_cast<Base const&>(point).transpose() << ")";
      }

      using Vector = std::vector<Point<T, DIM>, Eigen::aligned_allocator<Point<T, DIM>>>;
    };

    template<typename T, int DIM>
    typename Point<T, DIM>::OriginPoint const Point<T, DIM>::ORIGIN = Point<T,DIM>::OriginPoint();

    using Point2i = Point<int, 2>;
    using Point2f = Point<float, 2>;
    using Point2d = Point<double, 2>;
    using Point3i = Point<int, 3>;
    using Point3d = Point<double, 3>;
  }
}
