// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/LogAppender/logappender.hh"
#include "roundtable/JsonSession/jsonsession.hh"

#include <libwebsockets.h>
#include <rapidjson/writer.h>

namespace bold
{
  namespace roundtable
  {
    /** Log appender that takes log messages and sends them over websocket
     */
    class WebSocketLogAppender : public util::LogAppender
    {
    public:
      WebSocketLogAppender();

      void append(util::LogLevel level, std::string const& scope, std::string const& message) override;

      void setLwsProtocolContext(lws_protocols* protocol, lws_context* context);
      
      size_t addSession(JsonSession* session);
      size_t removeSession(JsonSession* session);

    private:
      struct LogMessage
      {
        util::LogLevel level;
        std::string scope;
        std::string message;
      };

      lws_protocols* d_protocol;
      lws_context* d_context;
      std::vector<LogMessage> d_criticalMessages;
      std::vector<JsonSession*> d_sessions;
      std::mutex d_sessionsMutex;

      void writeCriticalMessages(rapidjson::Writer<WebSocketBuffer>& writer);

      void writeJson(rapidjson::Writer<WebSocketBuffer>& writer, util::LogLevel level,
                     std::string const& scope, std::string const& message);
    };
  }
}
