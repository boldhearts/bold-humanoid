// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "websocketlogappender.hh"

#include "util/Log/log.hh"

using namespace bold::roundtable;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

WebSocketLogAppender::WebSocketLogAppender()
  : d_protocol(nullptr),
    d_context(nullptr)
{}

void WebSocketLogAppender::setLwsProtocolContext(lws_protocols *protocol, lws_context *context)
{
  d_protocol = protocol;
  d_context = context;
}

void WebSocketLogAppender::append(LogLevel level, string const& scope, string const& message)
{
  if (level == LogLevel::Warning || level == LogLevel::Error)
  {
    d_criticalMessages.push_back({level, scope, message});
  }

  lock_guard<mutex> guard(d_sessionsMutex);

  for (JsonSession* session : d_sessions)
  {
    WebSocketBuffer buffer;
    Writer<WebSocketBuffer> writer(buffer);
    writeJson(writer, level, scope, message);
    session->enqueue(move(buffer), /*suppressLwsNotify*/ true);
  }

  lws_callback_on_writable_all_protocol(d_context, d_protocol);
}

size_t WebSocketLogAppender::addSession(JsonSession* session)
{
  lock_guard<mutex> guard(d_sessionsMutex);

  d_sessions.push_back(session);

  // Write all past critical messages, so none are missed
  WebSocketBuffer buffer;
  Writer<WebSocketBuffer> writer(buffer);
  writeCriticalMessages(writer);
  session->enqueue(move(buffer));

  return d_sessions.size();
}

size_t WebSocketLogAppender::removeSession(JsonSession* session)
{
  lock_guard<mutex> guard(d_sessionsMutex);

  d_sessions.erase(find(d_sessions.begin(), d_sessions.end(), session));

  session->~JsonSession();

  return d_sessions.size();
}

void WebSocketLogAppender::writeCriticalMessages(Writer<WebSocketBuffer>& writer)
{
  writer.StartArray();
  {
    for (auto const& msg : d_criticalMessages)
      writeJson(writer, msg.level, msg.scope, msg.message);
  }
  writer.EndArray();
}

void WebSocketLogAppender::writeJson(Writer<WebSocketBuffer>& writer, LogLevel level, string const& scope, string const& message)
{
  writer.StartObject();
  {
    writer.String("lvl");
    writer.Uint(static_cast<unsigned>(level));

    writer.String("scope");
    writer.String(scope.c_str());

    writer.String("msg");
    writer.String(message.c_str());
  }
  writer.EndObject();
}
