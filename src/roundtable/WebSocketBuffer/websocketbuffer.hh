// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <rapidjson/rapidjson.h>
#include <rapidjson/internal/stack.h>
#include <libwebsockets.h>

namespace bold
{
  namespace roundtable
  {
    /** A string buffer, with additional padding to be used by libwebsockets
     */
    template <typename Allocator = rapidjson::CrtAllocator>
    class GenericWebSocketBuffer
    {
    public:
      using Ch = unsigned char;
      
      GenericWebSocketBuffer(Allocator* allocator = nullptr, size_t capacity = kDefaultCapacity)
        : d_stack(allocator, capacity)
      {
        Push(LWS_SEND_BUFFER_PRE_PADDING);
      }

      void Put(Ch c) { *d_stack.template Push<Ch>() = c; }
      void Flush() {}
      void Clear() { d_stack.Clear(); }
      void ShrinkToFit() { d_stack.ShrinkToFit(); }
      unsigned char* Push(size_t count) { return d_stack.template Push<unsigned char>(count); }
      void Pop(size_t count) { d_stack.template Pop<unsigned char>(count); }
      unsigned char* GetBuffer() { return d_stack.template Bottom<unsigned char>(); }
      size_t GetSize() const { return d_stack.GetSize(); }

      static constexpr size_t kDefaultCapacity = 256;

      friend void PutN(GenericWebSocketBuffer<>& stream, char c, size_t n);
      
    private:
      rapidjson::internal::Stack<Allocator> d_stack;
    };

    using WebSocketBuffer = GenericWebSocketBuffer<>;

    /// Specialized version of PutN() with memset() for better performance.
    inline void PutN(WebSocketBuffer& stream, char c, size_t n)
    {
      std::memset(stream.d_stack.Push<char>(n), c, n * sizeof(c));
    }
  }
}
