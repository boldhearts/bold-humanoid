// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ImageCodec/JpegCodec/jpegcodec.hh"
#include "ImageCodec/PngCodec/pngcodec.hh"

#include <libwebsockets.h>
#include <memory>
#include <mutex>

namespace cv {
  class Mat;
}

namespace bold {
  class Clock;

  namespace roundtable {
    struct CameraSession {
    public:
      CameraSession(lws *wsi, std::shared_ptr<Clock> clock);

      ~CameraSession() = default;

      void notifyImageAvailable(cv::Mat const &image, ImageCodec::ImageEncoding encoding,
                                std::map<uint8_t, colour::BGR> const &palette);

      int write();

    private:
      lws *d_wsi;
      std::shared_ptr<Clock> d_clock;

      /** Whether an image is waiting to be encoded. */
      bool d_imgWaiting;
      /** Whether encoded image data is currently being sent. */
      bool d_imgSending;
      /** If d_imgSending is true, the encoded JPEG bytes will be here. */
      std::unique_ptr<std::vector<uint8_t>> d_imageBytes;
      /** If d_imgSending is true, the number of bytes already sent. */
      unsigned d_bytesSent;

      std::mutex d_imageMutex;
      cv::Mat d_image;
      ImageCodec::ImageEncoding d_imageEncoding;
      std::map<uint8_t, colour::BGR> d_palette;

      PngCodec d_pngCodec;
      JpegCodec d_jpegCodec;

    };

  }
}
