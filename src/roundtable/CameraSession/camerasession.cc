// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "camerasession.hh"

#include "util/Log/log.hh"
#include "ThreadUtil/threadutil.hh"
#include "Clock/clock.hh"

using namespace bold::roundtable;
using namespace bold::util;
using namespace std;

CameraSession::CameraSession(lws *wsi, shared_ptr<Clock> clock)
    : d_wsi(wsi),
      d_clock(move(clock)),
      d_imgWaiting(false),
      d_imgSending(false),
      d_imageBytes(make_unique<vector<uint8_t>>()) {}

void CameraSession::notifyImageAvailable(cv::Mat const &image, ImageCodec::ImageEncoding encoding,
                                         std::map<uint8_t, colour::BGR> const &palette) {
  ASSERT(ThreadUtil::isThinkLoopThread());

  {
    lock_guard<mutex> imageGuard(d_imageMutex);
    d_image = image;
    d_imageEncoding = encoding;
    d_palette = palette;
    d_imgWaiting = true;
  }

  if (!d_imgSending)
    lws_callback_on_writable(d_wsi);
}

int CameraSession::write() {
  ASSERT(ThreadUtil::isDataStreamerThread());

  if (!d_imgSending) {
    if (!d_imgWaiting)
      return 0;

    // Take a thread-safe copy of the image to encode
    cv::Mat image;
    ImageCodec::ImageEncoding encoding;
    std::map<uint8_t, colour::BGR> palette;
    {
      lock_guard<mutex> imageGuard(d_imageMutex);
      image = d_image;
      encoding = d_imageEncoding;
      palette = d_palette;
      d_imgWaiting = false;
      d_imgSending = true;
    }

    auto t = d_clock->getTimestamp();

    // Encode the image
    ASSERT(d_imageBytes->empty());
    d_imageBytes->resize(LWS_SEND_BUFFER_PRE_PADDING);
    switch (encoding) {
      case ImageCodec::ImageEncoding::PNG:
        if (!d_pngCodec.encode(image, *d_imageBytes, &palette)) {
          Log::error("CameraSession::write") << "Error encoding image as PNG";
          return 1;
        }
        break;

      case ImageCodec::ImageEncoding::JPEG:
        if (!d_jpegCodec.encode(image, *d_imageBytes)) {
          Log::error("CameraSession::write") << "Error encoding image as JPEG";
          return 1;
        }
        break;
    }
    d_imageBytes->resize(d_imageBytes->size() + LWS_SEND_BUFFER_POST_PADDING);

    Log::trace("CameraSession::write") << "Encoded image (" << d_imageBytes->size() << " bytes) in "
                                       << d_clock->getMillisSince(t) << " ms";

    d_bytesSent = 0;
  }

  // Fill the outbound pipe with frames of data
  while (!lws_send_pipe_choked(d_wsi)) {
    unsigned totalSize = (uint) d_imageBytes->size() - LWS_SEND_BUFFER_PRE_PADDING - LWS_SEND_BUFFER_POST_PADDING;

    ASSERT(d_bytesSent < totalSize);

    uint8_t *start = d_imageBytes->data() + LWS_SEND_BUFFER_PRE_PADDING + d_bytesSent;

    unsigned remainingSize = totalSize - d_bytesSent;
    unsigned frameSize = min(2048u, remainingSize);

    int writeMode = d_bytesSent == 0
                    ? LWS_WRITE_BINARY
                    : LWS_WRITE_CONTINUATION;

    if (frameSize != remainingSize)
      writeMode |= LWS_WRITE_NO_FIN;

    bool storePostPadding = d_bytesSent + frameSize < totalSize;
    std::array<uint8_t, LWS_SEND_BUFFER_POST_PADDING> postPadding{};
    if (storePostPadding)
      std::copy(start + frameSize, start + frameSize + LWS_SEND_BUFFER_POST_PADDING, postPadding.data());

    int res = lws_write(d_wsi, start, frameSize, (lws_write_protocol) writeMode);

    if (res < 0) {
      Log::error("callback_camera") << "Error " << res << " writing to socket (image)";
      return 1;
    }

    d_bytesSent += frameSize;

    if (d_bytesSent == totalSize) {
      // Done sending
      d_imgSending = false;
      d_bytesSent = 0;
      d_imageBytes->clear();
      return 0;
    } else if (storePostPadding) {
      std::copy(postPadding.data(), postPadding.data() + LWS_SEND_BUFFER_POST_PADDING, start + frameSize);
    }
  }

  // Queue for more writing later on if we still have data remaining
  if (d_imgSending || d_imgWaiting)
    lws_callback_on_writable(d_wsi);

  return 0;
}
