// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "roundtable/Protocol/protocol.hh"

#include "roundtable/CameraSession/camerasession.hh"

#include <sigc++/signal.h>

namespace bold
{
  namespace roundtable
  {
    class CameraProtocol;

    template<>
    struct ProtocolTraits<CameraProtocol>
    {
      using SessionData = CameraSession;
      using InputData = char;
    };
    
    class CameraProtocol : public Protocol<CameraProtocol>
    {
    public:
      explicit CameraProtocol(std::shared_ptr<Clock> clock);

      int callbackEstablished(lws* wsi, CameraSession* sessionData) override;
      int callbackClosed(lws* wsi, CameraSession* sessionData) override;
      int callbackServerWritable(lws* wsi, CameraSession* sessionData) override;

      /** Enqueues an image to be sent to connected clients. */
      void streamImage(cv::Mat const& img, ImageCodec::ImageEncoding imageEncoding, std::map<uint8_t, colour::BGR> const& palette);

      bool hasClients() const { return d_sessions.size() > 0; }

      /// Emits signal whenever the first client connects to a protocol, or the last disconnects.
      sigc::signal<void, bool> hasClientChanged;

    private:
      std::mutex d_sessionsMutex;
      std::vector<CameraSession*> d_sessions;

      std::shared_ptr<Clock> d_clock;
    };
  }
}
