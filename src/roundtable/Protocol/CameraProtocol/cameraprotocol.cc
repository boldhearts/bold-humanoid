// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cameraprotocol.hh"

using namespace bold::roundtable;
using namespace std;

CameraProtocol::CameraProtocol(shared_ptr<Clock> clock)
  : Protocol<CameraProtocol>("camera-protocol"),
  d_clock{move(clock)}
{
}

int CameraProtocol::callbackEstablished(lws *wsi, CameraSession *sessionData)
{
  // New client connected; initialize session
  new (sessionData) CameraSession(wsi, d_clock);

  lock_guard<mutex> guard(d_sessionsMutex);
  d_sessions.push_back(sessionData);

  if (d_sessions.size() == 1)
    hasClientChanged(true);
  
  return 0;
}

int CameraProtocol::callbackClosed(lws *wsi, CameraSession *sessionData)
{
  lock_guard<mutex> guard(d_sessionsMutex);
  d_sessions.erase(find(d_sessions.begin(), d_sessions.end(), sessionData));

  sessionData->~CameraSession();

  if (d_sessions.size() == 0)
    hasClientChanged(false);

  return 0;
}

int CameraProtocol::callbackServerWritable(lws *wsi, CameraSession* sessionData)
{
  return sessionData->write();
}

void CameraProtocol::streamImage(cv::Mat const& img, ImageCodec::ImageEncoding imageEncoding,
                                 std::map<uint8_t, colour::BGR> const& palette)
{
  lock_guard<mutex> guard(d_sessionsMutex);

  for (auto* session : d_sessions)
    session->notifyImageAvailable(img, imageEncoding, palette);
}
