// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "stateprotocol.hh"

#include "util/Log/log.hh"
#include "state/State/state.hh"
#include "state/StateObject/stateobject.hh"

#include <rapidjson/writer.h>

using namespace bold::roundtable;
using namespace bold::state;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

StateProtocol::StateProtocol(string const& name)
  : Protocol<StateProtocol>{name}
{
  // Listen for StateObject changes and publish them via websockets
  State::updated.connect(
    [this](shared_ptr<StateTracker const> tracker)
    {
      // There may be state updates before websocket server was started
      if (getLwsContext() == nullptr || getLwsProtocol() == nullptr)
        return;
      
      if (tracker->name() != getName())
        return;
        
      // NOTE we may be writing from one thread, while another is dealing with
      // a client connection (which modifies d_stateSessions), or sending data
      // (which modifies the JsonSession queue.)
      lock_guard<mutex> guard(d_sessionsMutex);
      
      shared_ptr<StateObjectBase const> obj = tracker->stateBase();

      for (auto *session : d_sessions)
      {
        WebSocketBuffer buffer;
        Writer<WebSocketBuffer> writer(buffer);
        writer.SetMaxDecimalPlaces(3);
        obj->writeJson(writer);
        session->enqueue(move(buffer), /*suppressLwsNotify*/ true);
      }

      lws_callback_on_writable_all_protocol(getLwsContext(), getLwsProtocol());
    }
    );
}

int StateProtocol::callbackEstablished(lws *wsi, bold::roundtable::JsonSession *sessionData)
{
  const lws_protocols* protocol = lws_get_protocol(wsi);

  Log::verbose("StateProtocol::callbackEstablished") << protocol->name << " - Client connected";

  // New client connected; initialize session
  new (sessionData) JsonSession(protocol->name, wsi);

  std::lock_guard<std::mutex> guard(d_sessionsMutex);
  d_sessions.push_back(sessionData);

  // Some state objects change very infrequently (such as StaticHardwareState)
  // and so we send the latest object to a client when they connect.

  // Find any existing state object
  shared_ptr<StateObjectBase const> stateObject = State::getByName(protocol->name);

  if (stateObject)
  {
    // Encode and enqueue for this client
    WebSocketBuffer buffer;
    Writer<WebSocketBuffer> writer(buffer);
    writer.SetMaxDecimalPlaces(3);
    stateObject->writeJson(writer);
    sessionData->enqueue(move(buffer));
  }

  return 0;
}

int StateProtocol::callbackClosed(lws *wsi, bold::roundtable::JsonSession *sessionData)
{
  const lws_protocols* protocol = lws_get_protocol(wsi);

  Log::verbose(protocol->name) << "Client disconnected";

  std::lock_guard<std::mutex> guard(d_sessionsMutex);
  d_sessions.erase(find(d_sessions.begin(), d_sessions.end(), sessionData));

  // We have to call the destructor by hand
  sessionData->~JsonSession();

  return 0;
}

int StateProtocol::callbackServerWritable(lws *wsi, JsonSession *sessionData)
{
  return sessionData->write();
}
