// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "roundtable/Protocol/protocol.hh"
#include "roundtable/JsonSession/jsonsession.hh"

namespace bold
{
  namespace roundtable
  {
    class StateProtocol;

    template<>
    struct ProtocolTraits<StateProtocol>
    {
      using SessionData = JsonSession;
      using InputData = char;
    };

    /** Protocol for streaming state objects
     *
     * Listens to any changes of a specific stae, and streams it directly to any listeners.
     * Set up a separate protocol for each
     */
    class StateProtocol : public Protocol<StateProtocol>
    {
    public:
      StateProtocol(std::string const& name);

      int callbackEstablished(lws* wsi, JsonSession* sessionData) override;
      int callbackClosed(lws* wsi, JsonSession* sessionData) override;
      int callbackServerWritable(lws* wsi, JsonSession* sessionData) override;
    private:
      std::vector<JsonSession*> d_sessions;
      std::mutex d_sessionsMutex;

    };
  }
}
