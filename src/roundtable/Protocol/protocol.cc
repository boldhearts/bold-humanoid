// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "protocol.hh"

#include "util/Log/log.hh"

using namespace bold::roundtable;
using namespace bold::util;

int ProtocolBase::callback(lws* wsi, lws_callback_reasons reason, void* sessionData, void* inputData, size_t len)
{
  auto lwsProtocol = lws_get_protocol(wsi);
  if (lwsProtocol == nullptr)
  {
    switch (reason)
    {
    case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_SERVER_VERIFY_CERTS:
    case LWS_CALLBACK_PROTOCOL_INIT:
    case LWS_CALLBACK_GET_THREAD_ID:
      // This is fine if protocol hasn't been initialised yet
      return 0;

    default:
      // Else this is an error
      Log::error("ProtocolBase::callback") << "No protocol; callback reason: " << reason;
      return 1;
    }
  }

  if (lwsProtocol->user == nullptr)
  {
    Log::trace("ProtocolBase::callback") <<
      "No userdata; protocol: " << lwsProtocol->name <<
      ", callback reason: " << reason;
    return 0;
  }
  
  auto protocol = static_cast<ProtocolBase*>(lwsProtocol->user);
  return protocol->callbackImpl(wsi, reason, sessionData, inputData, len);
}
