// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "controlprotocol.hh"

#include "util/Log/log.hh"
#include "config/Config/config.hh"
#include "config/SettingReader/settingreader.hh"
#include "config/SettingWriter/settingwriter.hh"
#include "roundtable/Action/action.hh"

#include <rapidjson/writer.h>
#include <rapidjson/error/en.h>

using namespace bold::config;
using namespace bold::roundtable;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

ControlProtocol::ControlProtocol()
  : Protocol<ControlProtocol>("control-protocol")
{
  // Listen for Setting<T> changes and publish them via websockets
  Config::updated.connect(
    [this](SettingBase const& setting)
    {
      lock_guard<mutex> guard(d_sessionsMutex);
      for (auto* session : d_sessions)
      {
        WebSocketBuffer buffer;
        Writer<WebSocketBuffer> writer(buffer);
        writer.SetMaxDecimalPlaces(3);
        writeSettingUpdate(setting, writer);
        session->enqueue(move(buffer), /*suppressLwsNotify*/ true);
      }

      lws_callback_on_writable_all_protocol(getLwsContext(), getLwsProtocol());
    }
    );
}

void ControlProtocol::addSyncDataWriter(string key, SyncDataWriter writer)
{
  d_extraSyncDataWriters.emplace(move(key), move(writer));
}

int ControlProtocol::callbackEstablished(lws *wsi, JsonSession *sessionData)
{
    Log::verbose("ControlProtocol::callbackEstablished") << "Client connected";

    new (sessionData) JsonSession("control-protocol", wsi);

    // Write control sync message, with current snapshot of state
    WebSocketBuffer buffer;
    Writer<WebSocketBuffer> writer(buffer);
    writer.SetMaxDecimalPlaces(3);
    writeSyncData(writer);
    sessionData->enqueue(move(buffer));

    lock_guard<mutex> guard(d_sessionsMutex);
    d_sessions.push_back(sessionData);

    return 0;
}

int ControlProtocol::callbackClosed(lws *wsi, JsonSession *sessionData)
{
    Log::verbose("control-protocol") << "Client disconnecting";

    lock_guard<mutex> guard(d_sessionsMutex);
    d_sessions.erase(find(d_sessions.begin(), d_sessions.end(), sessionData));

    // We have to call the destructor by hand
    sessionData->~JsonSession();

    return 0;
}

int ControlProtocol::callbackServerWritable(lws *wsi, JsonSession *sessionData)
{
  return sessionData->write();
}

int ControlProtocol::callbackReceive(lws *wsi, JsonSession *sessionData, char* in, size_t len)
{
  if (len != 0)
  {
    Log::verbose("control-protocol") << "Receiving data";
    static string message;
    message.append((char const*)in, len);

    if (lws_remaining_packet_payload(wsi) == 0)
    {
      // We now have all the data
      processCommand(message, *sessionData);

      message.clear();
    }
  }
    
  return 0;
}

void ControlProtocol::writeSyncData(Writer<WebSocketBuffer>& writer)
{
  writer.StartObject();
  {
    // Signify this is a sync message
    writer.String("type");
    writer.String("sync");

    // Write all config file names
    writer.String("files");
    writer.StartArray();
    {
      for (auto const& fileName : Config::getConfigDocumentNames())
        writer.String(fileName.c_str());
    }
    writer.EndArray();

    // Write all actions
    writer.String("actions");
    writer.StartArray();
    {
      for (auto const& action : Action::getAllActions())
      {
        writer.StartObject();
        {
          writer.String("id");
          writer.String(action.getId().c_str());

          writer.String("label");
          writer.String(action.getLabel().c_str());

          writer.String("hasArguments");
          writer.Bool(action.hasArguments());
        }
        writer.EndObject();
      }
    }
    writer.EndArray();

    // Write full info on all settings
    auto settingWriter = SettingWriter{};
    writer.String("settings");
    writer.StartArray();
    {
      for (SettingBase const* setting : Config::getAllSettings())
        settingWriter.writeFullJson(writer, *setting);
    }
    writer.EndArray();

    // Write any other sync data provided
    for (auto const& entry : d_extraSyncDataWriters)
    {
      writer.String(entry.first.c_str());
      entry.second(writer);
    }
  }
  writer.EndObject();
}

void ControlProtocol::writeSettingUpdate(SettingBase const& setting, Writer<WebSocketBuffer>& writer)
{
  writer.StartObject();
  {
    writer.String("type");
    writer.String("update");

    writer.String("path");
    writer.String(setting.getPath().c_str());

    writer.String("value");
    
    auto settingWriter = SettingWriter{};
    settingWriter.writeJsonValue(writer, setting);
  }
  writer.EndObject();
}

void ControlProtocol::processCommand(string const& json, JsonSession& jsonSession)
{
  Log::info("DataStreamer::processCommand") << "Processing: " << json;

  Document doc;
  doc.Parse<0>(json.c_str());

  if (doc.HasParseError())
  {
    Log::error("DataStreamer::processCommand") <<
      "Error parsing command JSON at offset " << doc.GetErrorOffset() << ": " << GetParseError_En(doc.GetParseError());
    return;
  }

  auto typeMember = doc.FindMember("type");
  if (typeMember == doc.MemberEnd() || !typeMember->value.IsString())
  {
    Log::error("DataStreamer::processCommand") << "No 'type' specified in received command JSON";
    return;
  }
  auto type = string{typeMember->value.GetString()};

  if (type == "action")
  {
    //
    // Handle ACTION
    //

    // { "type": "action", "id": "some.action" }
    // { "type": "action", "id": "some.action", "args": ... }

    auto idMember = doc.FindMember("id");
    if (idMember == doc.MemberEnd() || !idMember->value.IsString())
    {
      Log::error("DataStreamer::processCommand") << "No 'id' specified in received action JSON";
      return;
    }

    char const* id = idMember->value.GetString();

    auto action = Action::getAction(string(id));

    if (!action)
    {
      Log::error("DataStreamer::processCommand") << "No action exists with id: " << id;
      return;
    }

    auto argsMember = doc.FindMember("args");
    action->handleRequest(argsMember != doc.MemberEnd() ? &argsMember->value : nullptr);
  }
  else if (type == "setting")
  {
    //
    // Handle SETTING
    //

    // { "type": "setting", "path": "some.setting", "value": 1234 }

    auto pathMember = doc.FindMember("path");
    if (pathMember == doc.MemberEnd() || !pathMember->value.IsString())
    {
      Log::error("DataStreamer::processCommand") << "No 'path' specified in received setting JSON";
      return;
    }
    char const* path = pathMember->value.GetString();

    auto valueMember = doc.FindMember("value");
    if (valueMember == doc.MemberEnd())
    {
      Log::error("DataStreamer::processCommand") << "No 'value' specified in received setting JSON";
      return;
    }

    auto setting = Config::getSettingBase(path);

    if (!setting)
    {
      Log::error("DataStreamer::processCommand") << "No setting exists with path: " << path;
      return;
    }

    auto reader = SettingReader{};
    if (!reader.setValueFromJson(*setting, valueMember->value))
    {
      // Setting the value failed. Send the current value back to the client
      // that requested this invalid value.
      WebSocketBuffer buffer;
      Writer<WebSocketBuffer> writer(buffer);
      writeSettingUpdate(*setting, writer);
      jsonSession.enqueue(move(buffer));
    }
  }
}
