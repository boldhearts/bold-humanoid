// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "roundtable/Protocol/protocol.hh"
#include "roundtable/JsonSession/jsonsession.hh"
#include "roundtable/WebSocketBuffer/websocketbuffer.hh"

#include <mutex>
#include <rapidjson/writer.h>
#include <memory>
#include <map>
#include <functional>

namespace bold
{
  class OptionTree;
  namespace motion
  {
    namespace scripts
    {
      class MotionScriptLibrary;
    }
  }
  
  namespace config
  {
    class SettingBase;
  }
  
  namespace roundtable
  {
    class ControlProtocol;

    template<>
    struct ProtocolTraits<ControlProtocol>
    {
      using SessionData = JsonSession;
      using InputData = char;
    };

    /** Protocol for supplying control information and handle control commands from client, such as setting changes and actions
     *
     * When a client connects, this protocol first sends 'sync data' to bring the client up to speed:
     * - a list of config files used
     * - a list of all the available actions
     * - all settings
     * - other data provided by additional SyncDataWriters, such as ones that provide motion script information, or FSMs
     *
     * After that, it sends any updated setting, and processes any setting changes and actions sent by clients
     */
    class ControlProtocol : public Protocol<ControlProtocol>
    {
    public:
      /// A function to add sync data when clients connect
      using SyncDataWriter = std::function<void(rapidjson::Writer<WebSocketBuffer>&)>;
      
      ControlProtocol();

      void addSyncDataWriter(std::string key, SyncDataWriter writer);
      
      int callbackEstablished(lws* wsi, JsonSession* sessionData) override;

      int callbackClosed(lws* wsi, JsonSession* sessionData) override;

      int callbackServerWritable(lws* wsi, JsonSession* sessionData) override;

      int callbackReceive(lws* wsi, JsonSession* sessionData,
                          typename ProtocolTraits<ControlProtocol>::InputData* in, size_t len) override;
    private:
      std::vector<JsonSession*> d_sessions;
      std::mutex d_sessionsMutex;

      std::map<std::string, SyncDataWriter> d_extraSyncDataWriters;
      
      void writeSyncData(rapidjson::Writer<WebSocketBuffer>& writer);
      void writeSettingUpdate(config::SettingBase const& setting, rapidjson::Writer<WebSocketBuffer>& writer);
      void processCommand(std::string const& message, JsonSession& sessionData);
    };
  }
}  
