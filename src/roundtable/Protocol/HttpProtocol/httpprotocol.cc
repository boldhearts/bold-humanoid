// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "httpprotocol.hh"

#include "config/Config/config.hh"
#include "util/Log/log.hh"

using namespace bold::config;
using namespace bold::roundtable;
using namespace bold::util;
using namespace std;

HttpProtocol::HttpProtocol()
  : Protocol<HttpProtocol>{"http-only"}
{
  d_webRoot = Config::getStaticValue<string>("round-table.web-root");
  Log::info("HttpProtocol::HttpProtocol") << "Serving Http from root: " << d_webRoot;
}

int HttpProtocol::callbackHttp(lws *wsi, char *in, size_t len)
{
  string path;
  if (in)
  {
    path = (char*)in;
    if (path == "/")
      path = "/index.html";
    else if (path.find("..") != string::npos)
    {
      // protect against ../ attacks
      Log::info("DataStreamer::callback_http") << "invalid request path: " << path;
      return 1;
    }

    // ignore query string (for now)
    size_t queryStart = path.find("?");
    if (queryStart != string::npos)
      path = path.substr(0, queryStart);

    Log::info("DataStreamer::callback_http") << "requested path: " << path;
  }
  else
  {
    path = "/index.html";
  }

  //
  // Determine the MIME type based upon the path extension
  //
  auto extension = path.substr(path.find_last_of(".") + 1);
  string mimeType = "application/binary";
  if (extension == "html") {
    mimeType = "text/html";
  }
  else if (extension == "js") {
    mimeType = "text/javascript";
  }
  else if (extension == "json") {
    mimeType = "application/json";
  }
  else if (extension == "css") {
    mimeType = "text/css";
  }
  else if (extension == "png") {
    mimeType = "image/png";
  }
  else if (extension == "jpg") {
    mimeType = "image/jpeg";
  }
  else if (extension == "ico") {
    mimeType = "image/x-icon";
  }
  else if (extension == "map") {
    mimeType = "application/json";
  }
  else if (extension == "svg") {
    mimeType = "image/svg+xml";
  }

  path = d_webRoot + path;
  char buf[256];
  sprintf(buf, "%s", path.c_str());

  if (lws_serve_http_file(wsi, buf, mimeType.c_str(), nullptr, 0) < 0)
    Log::error("callback_http") << "Failed to send HTTP file";

  return 0;
}

int HttpProtocol::callbackHttpFileCompletion()
{
  return 1;
}
