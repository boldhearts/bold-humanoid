// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/assert.hh"

#include <string>
#include <libwebsockets.h>

namespace bold
{
  namespace roundtable
  {
    /** Base class for Round Table protocols
     *
     * Performs basic bridging between LWS C API. New protocols should
     * not extend this class, but rather the @a Protocol
     * template.
     */
    class ProtocolBase
    {
    public:
      explicit ProtocolBase(std::string const& name);

      void setAndFillLwsProtocol(lws_protocols* lwsProtocol);
      lws_protocols* getLwsProtocol() const;

      void setLwsContext(lws_context* lwsContext);
      lws_context* getLwsContext() const;
      
      std::string const& getName() const;

      virtual size_t perSessionDataSize() = 0;
      
      virtual int callbackImpl(lws* wsi, lws_callback_reasons reason, void* sessionData, void* inputData, size_t len) = 0;
      
      static int callback(lws* wsi, lws_callback_reasons reason, void* sessionData, void* inputData, size_t len);
      
    private:
      std::string d_name;
      lws_protocols* d_lwsProtocol;
      lws_context* d_lwsContext;

      /// Can be overridden by subclass to perform initialisation depending on the context
      virtual void onLwsContextSet() {}
    };

    /** Traits class specifying types used by protocols
     *
     * A specialisation for each protocol of this struct must be
     * given, specifying these types:
     *
     * - SessionData: type of per-connection data that is
     *   allocated. If do session data is used, use void.
     *    
     * - InputData: type of input data the protocol expects. Most
     *   likely char
     */
    template<typename Derived>
    struct ProtocolTraits;

    /// Helper trait class to be able to handle void session data
    template<typename SessionData>
    struct SessionDataTraits
    {
      static constexpr size_t size = sizeof(SessionData);
    };
    
    template<>
    struct SessionDataTraits<void>
    {
      static constexpr size_t size = 0;
    };

    /** Websocket protocol baseclass
     *
     * Specific implementations of protocols should extend this
     * class. Besides implementing the virtual methods, the derived
     * class must expose a @a SessionData type, that at least has the following structure:
     * 
     * struct SessionData
     * {
     *   ProtocolBase* protocol
     * };
     */
    template<typename Derived>
    class Protocol : public ProtocolBase
    {
    public:
      Protocol(std::string const& name);

      int callbackImpl(lws* wsi, lws_callback_reasons reason, void* _sessionData, void* _inputData, size_t len) override;

      /// Size of data holding session data
      size_t perSessionDataSize() override
      {
        return SessionDataTraits<typename ProtocolTraits<Derived>::SessionData>::size;
      }

      /// Called when a new session is established
      virtual int callbackEstablished(lws* wsi, typename ProtocolTraits<Derived>::SessionData* sessionData) { return 0; }

      /// Called when a session is closed
      virtual int callbackClosed(lws* wsi, typename ProtocolTraits<Derived>::SessionData* sessionData)  { return 0; }

      /// Called when the server is ready to be written to
      virtual int callbackServerWritable(lws* wsi, typename ProtocolTraits<Derived>::SessionData* sessionData)  { return 0; }

      /// Called when data is received
      virtual int callbackReceive(lws* wsi, typename ProtocolTraits<Derived>::SessionData* sessionData, typename ProtocolTraits<Derived>::InputData* in, size_t len)  { return 0; }

      /// Called when an HTTP request is received
      virtual int callbackHttp(lws* wsi, typename ProtocolTraits<Derived>::InputData* in, size_t len)  { return 0; }

      /// Called when serving an HTTP file has finished
      virtual int callbackHttpFileCompletion()  { return 0; }
    };



    inline ProtocolBase::ProtocolBase(std::string const& name)
      : d_name{name},
        d_lwsProtocol{nullptr},
        d_lwsContext{nullptr}
    {}


    inline void ProtocolBase::setAndFillLwsProtocol(lws_protocols *lwsProtocol)
    {
      d_lwsProtocol = lwsProtocol;
      *d_lwsProtocol = { getName().c_str(), ProtocolBase::callback, perSessionDataSize(), 0, 0, this };
    }

    inline lws_protocols* ProtocolBase::getLwsProtocol() const
    {
      return d_lwsProtocol;
    }


    inline void ProtocolBase::setLwsContext(lws_context *lwsContext)
    {
      d_lwsContext = lwsContext;
      onLwsContextSet();
    }

    inline lws_context* ProtocolBase::getLwsContext() const
    {
      return d_lwsContext;
    }
    

    inline std::string const& ProtocolBase::getName() const
    {
      return d_name;
    }
    

    template<typename Derived>
    inline Protocol<Derived>::Protocol(std::string const& name)
      : ProtocolBase{name}
    {}

    template<typename Derived>
    inline int Protocol<Derived>::callbackImpl(lws* wsi, lws_callback_reasons reason, void* _sessionData, void* _inputData, size_t len)
    {
      auto sessionData = static_cast<typename ProtocolTraits<Derived>::SessionData*>(_sessionData);
      auto inputData = static_cast<typename ProtocolTraits<Derived>::InputData*>(_inputData);
        
      switch(reason)
      {
      case LWS_CALLBACK_ESTABLISHED:
        return callbackEstablished(wsi, sessionData);
          
      case LWS_CALLBACK_CLOSED:
        return callbackClosed(wsi, sessionData);
          
      case LWS_CALLBACK_SERVER_WRITEABLE:
        return callbackServerWritable(wsi, sessionData);
          
      case LWS_CALLBACK_RECEIVE:
        return callbackReceive(wsi, sessionData, inputData, len);
          
      case LWS_CALLBACK_HTTP:
        return callbackHttp(wsi, inputData, len);
          
      case LWS_CALLBACK_HTTP_FILE_COMPLETION:
        return callbackHttpFileCompletion();

      default:
        return 0;
      }
    }

  }
}
