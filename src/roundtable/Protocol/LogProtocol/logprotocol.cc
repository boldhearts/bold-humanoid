// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "logprotocol.hh"

#include "util/Log/log.hh"

using namespace bold::roundtable;
using namespace bold::util;
using namespace std;

LogProtocol::LogProtocol(shared_ptr<WebSocketLogAppender> logAppender)
  : Protocol<LogProtocol>{"log-protocol"},
  d_logAppender{move(logAppender)}
{}

void LogProtocol::onLwsContextSet()
{
  if (getLwsContext() != nullptr)
  {
    d_logAppender->setLwsProtocolContext(getLwsProtocol(), getLwsContext());
    Log::addAppender(d_logAppender);
  }
}

int LogProtocol::callbackEstablished(lws *wsi, bold::roundtable::JsonSession *sessionData)
{
  Log::verbose("LogProtocol::callbackEstablished") << "Client connected";

  // Create new session data in pre-allocated space
  new (sessionData) JsonSession(getName(), wsi);

  // Add session to appender so it will send log lines to it
  d_logAppender->addSession(sessionData);
    
  return 0;
}

int LogProtocol::callbackClosed(lws *wsi, bold::roundtable::JsonSession *sessionData)
{
  Log::verbose("log-callback") << "Client disconnecting";

  // Remove session from log appender
  d_logAppender->removeSession(sessionData);
  return 0;
}

int LogProtocol::callbackServerWritable(lws *wsi, bold::roundtable::JsonSession *sessionData)
{
  sessionData->write();
  return 0;
}
