// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "roundtable/Protocol/protocol.hh"
#include "roundtable/JsonSession/jsonsession.hh"
#include "roundtable/WebSocketLogAppender/websocketlogappender.hh"

#include <memory>

namespace bold
{
  namespace roundtable
  {
    // Forward decalre, because traits must be available when defining protocol
    class LogProtocol;
    
    template<>
    struct ProtocolTraits<LogProtocol>
    {
      using SessionData = JsonSession;
      using InputData = char;
    };

    class LogProtocol : public Protocol<LogProtocol>
    {
    public:
      LogProtocol(std::shared_ptr<WebSocketLogAppender> logAppender);
      
      int callbackEstablished(lws* wsi, JsonSession* sessionData) override;

      int callbackClosed(lws* wsi, JsonSession* sessionData) override;

      int callbackServerWritable(lws* wsi, JsonSession* sessionData) override;

    private:
      std::shared_ptr<WebSocketLogAppender> d_logAppender;

      void onLwsContextSet() override;
    };

  }
}
