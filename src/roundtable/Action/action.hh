// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util/Maybe/maybe.hh"
#include <string>
#include <rapidjson/document.h>
#include <functional>
#include <vector>
#include <map>
#include <memory>

namespace bold
{
  namespace roundtable
  {
    /** Represents an action to be sent from RT to trigger some function
     */
    class Action
    {
    public:
      /** Create an action that takes no parameters
       */
      Action(std::string id, std::string label, std::function<void()> callback)
        : d_id(id),
          d_label(label),
          d_callback([callback](rapidjson::Value* doc) { callback(); }),
          d_requiresArguments(false)
      {}

      /** Create an action that can take parameters in the form of JSON data
       */
      Action(std::string id, std::string label, std::function<void(rapidjson::Value*)> callback)
        : d_id(id),
          d_label(label),
          d_callback(callback),
          d_requiresArguments(true)
      {}

      /** Handles a request to perform this action
       */
      void handleRequest() const { d_callback(nullptr); }
      
      /** Handles a request to perform this action that includes JSON arguments.
       */
      void handleRequest(rapidjson::Value* args) const { d_callback(args); }

      std::string getId() const { return d_id; }
      std::string getLabel() const { return d_label; }
      bool hasArguments() const { return d_requiresArguments; }

      static void addAction(std::string const& id, std::string const& label, std::function<void()> callback);

      static void addAction(std::string const& id, std::string const& label, std::function<void(rapidjson::Value*)> callback);

      static util::Maybe<Action> getAction(std::string const& id);

      static std::vector<Action> getAllActions();

      static void clearActions() { d_actionById.clear(); }
      
    private:
      std::string d_id;
      std::string d_label;
      std::function<void(rapidjson::Value*)> d_callback;
      bool d_requiresArguments;

      static std::map<std::string, roundtable::Action> d_actionById;
    };
  }
}
