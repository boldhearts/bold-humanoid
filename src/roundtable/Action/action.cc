// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "action.hh"

#include "util/assert.hh"
#include "util/Log/log.hh"

using namespace bold::roundtable;
using namespace bold::util;

std::map<std::string, Action> Action::d_actionById;

void Action::addAction(std::string const& id, std::string const& label, std::function<void()> callback)
{
  auto action = Action(id, label, callback);
  ASSERT(!action.hasArguments());
  auto it = d_actionById.insert(make_pair(id, action));

  if (it.second == false)
  {
    Log::error("Config::addAction") << "Action with id '" << id << "' already registered";
    throw std::runtime_error("Action already registered with provided id");
  }
}

void Action::addAction(std::string const& id, std::string const& label, std::function<void(rapidjson::Value*)> callback)
{
  auto action = Action(id, label, callback);
  ASSERT(action.hasArguments());
  auto it = d_actionById.insert(make_pair(id, action));

  if (it.second == false)
  {
    Log::error("Config::addAction") << "Action with id '" << id << "' already registered";
    throw std::runtime_error("Action already registered with provided id");
  }
}

std::vector<Action> Action::getAllActions()
{
  auto actions = std::vector<Action>{};
  for (auto const& pair : d_actionById)
    actions.push_back(pair.second);
  return actions;
}

Maybe<Action> Action::getAction(std::string const& id)
{
  auto it = d_actionById.find(id);
  if (it == d_actionById.end())
    return Maybe<Action>::empty();
  return it->second;
}

