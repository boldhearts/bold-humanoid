// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "roundtableserver.hh"

#include "Option/FSMOption/fsmoption.hh"
#include "OptionTree/optiontree.hh"
#include "state/State/state.hh"

#include "config/Config/config.hh"
#include "motion/scripts/MotionScript/motionscript.hh"
#include "motion/scripts/MotionScriptLibrary/motionscriptlibrary.hh"
#include "roundtable/Protocol/ControlProtocol/controlprotocol.hh"
#include "roundtable/Protocol/HttpProtocol/httpprotocol.hh"
#include "roundtable/Protocol/LogProtocol/logprotocol.hh"
#include "roundtable/Protocol/StateProtocol/stateprotocol.hh"

using namespace bold::config;
using namespace bold::motion::scripts;
using namespace bold::roundtable;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

RoundTableServer::RoundTableServer(shared_ptr<Clock> clock, shared_ptr<MotionScriptLibrary> motionScriptLibrary)
    : d_clock(move(clock)),
      d_port(Config::getStaticValue<int>("round-table.tcp-port")),
      d_context(0),
      d_isStopRequested(false),
      d_thread(),
      d_motionScriptLibrary(move(motionScriptLibrary)) {
  // Set libwebsockets logging level, and redirect logging through our log framework
  lws_set_log_level(LLL_ERR | LLL_WARN, [](int level, char const *line) {
    // Trim the newline character
    string l(line);
    l = l.substr(0, l.length() - 1);

    if (level == LLL_ERR)
      Log::error("libwebsockets") << l;
    else if (level == LLL_WARN)
      Log::warning("libwebsockets") << l;
    else
      Log::info("libwebsockets") << l;
  });

  // All HTPP requests go to the first protocol, so this always has to
  // be the first one
  registerProtocol(make_unique<HttpProtocol>());

  // TODO: move this somewhere else, and check if DataStreamer needs to store appender at all
  registerProtocol(make_unique<LogProtocol>(make_shared<roundtable::WebSocketLogAppender>()));

  auto controlProtocol = make_unique<ControlProtocol>();

  controlProtocol->addSyncDataWriter("motionScripts", [this](Writer<WebSocketBuffer> &writer) {
    writer.StartArray();
    {
      for (auto const &motionScript : d_motionScriptLibrary->getAllScripts()) {
        Log::verbose("RoundTableServer::WriteMotionScriptSyncData") << "Writing script: " << motionScript->getName();
        motionScript->writeJson(writer);
      }
    }
    writer.EndArray();
  });

  controlProtocol->addSyncDataWriter("fsms", [this](Writer<WebSocketBuffer> &writer) {
    writer.StartArray();
    {
      for (auto const &fsm : d_optionTree->getFSMs())
        fsm->toJson(writer);
    }
    writer.EndArray();
  });

  registerProtocol(move(controlProtocol));

  for (shared_ptr<StateTracker> stateTracker : State::getTrackers())
    registerProtocol(make_unique<StateProtocol>(stateTracker->name()));
}

void RoundTableServer::start() {
  // We have three special protocols: HTTP-only, Camera, Control, and Log.
  // These are followed by N other protocols, one per type of state in the system
  // 1 is added for null end of list
  unsigned protocolCount = d_registeredProtocols.size() + 1 + 1;

  d_protocols = new lws_protocols[protocolCount];

  unsigned protocolIndex = 0;

  // All main registered protocols
  for (auto &protocol : d_registeredProtocols)
    protocol->setAndFillLwsProtocol(&d_protocols[protocolIndex++]);

  // Mark the end of the protocols
  d_protocols[protocolIndex] = {nullptr, nullptr, 0, 0, 0, nullptr};

  //
  // Create the libwebsockets context object
  //
  lws_context_creation_info contextInfo;
  memset(&contextInfo, 0, sizeof(contextInfo));
  contextInfo.port = d_port;
  contextInfo.protocols = d_protocols;
  contextInfo.gid = contextInfo.uid = -1;
  contextInfo.user = this;
  d_context = lws_create_context(&contextInfo);

  for (auto &protocol : d_registeredProtocols)
    protocol->setLwsContext(d_context);

  bool hasWebSockets = d_context != nullptr;

  if (hasWebSockets)
    Log::info("DataStreamer::DataStreamer") << "Listening on TCP port " << d_port;
  else
    Log::error("DataStreamer::DataStreamer") << "libwebsocket context creation failed";

  Log::info("DataStreamer::DataStreamer") << "Starting DataStreamer thread";
  d_thread = std::thread(&RoundTableServer::run, this);
}

void RoundTableServer::run() {
  if (!d_context) {
    Log::error("DataStreamer::run") << "LWS context has not been established";
    throw runtime_error("LWS context has not been established");
  }

  ThreadUtil::setThreadId(ThreadId::DataStreamer);

  while (!d_isStopRequested) {
    //
    // Process whatever needs doing on the web socket (new clients, writing, receiving, etc)
    //
    lws_service(d_context, 10);
  }

  if (d_context)
    lws_context_destroy(d_context);
}

void RoundTableServer::stop() {
  if (d_isStopRequested) {
    Log::error("DataStreamer::stop") << "Stop has already been requested";
    return;
  }

  d_isStopRequested = true;

  d_thread.join();
}
