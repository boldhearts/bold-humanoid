// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "roundtable/Protocol/protocol.hh"

#include "util/assert.hh"

#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>

namespace bold
{
  class Clock;
  class OptionTree;

  namespace motion
  {
    namespace scripts
    {
      class MotionScriptLibrary;
    }
  }

  namespace roundtable
  {
    class RoundTableServer
    {
    public:
      RoundTableServer(std::shared_ptr<Clock> clock,
                       std::shared_ptr<motion::scripts::MotionScriptLibrary> motionScriptLibrary);

      /** Register a new protocol
       *
       * This must be called before the server is started
       */
      void registerProtocol(std::shared_ptr<roundtable::ProtocolBase> protocol) { d_registeredProtocols.emplace_back(move(protocol)); }

      /** Start LWS context and run serving thread */
      void start();

      /** Stop serving thread */
      void stop();

      void setOptionTree(std::shared_ptr<OptionTree> optionTree) { d_optionTree = optionTree; }

    private:
      void run();

      std::shared_ptr<Clock> d_clock;

      const int d_port;

      lws_context* d_context;
      lws_protocols* d_protocols;

      std::vector<std::shared_ptr<roundtable::ProtocolBase>> d_registeredProtocols;
    
      bool d_isStopRequested;
      std::thread d_thread;
      std::shared_ptr<OptionTree> d_optionTree;

      std::shared_ptr<motion::scripts::MotionScriptLibrary> d_motionScriptLibrary;
    };
  }
}
