// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "jsonsession.hh"

#include "util/assert.hh"
#include "util/Log/log.hh"

using namespace bold::util;
using namespace bold::roundtable;
using namespace rapidjson;
using namespace std;

JsonSession::JsonSession(std::string protocolName, lws* wsi)
  : d_protocolName(protocolName),
    d_wsi(wsi),
    d_queue(),
    d_bytesSent(0),
    d_maxQueueSeen(0)
{
  // add client host name and IP address
  int fd = lws_get_socket_fd(wsi);
  char hostName[256];
  char ipAddress[32];
  lws_get_peer_addresses(wsi, fd, hostName, sizeof(hostName), ipAddress, sizeof(ipAddress));
  d_hostName = string(hostName);
  d_ipAddress = string(ipAddress);
}

int JsonSession::write()
{
  lock_guard<mutex> guard(d_queueMutex);

  // Fill the outbound pipe with frames of data
  while (!lws_send_pipe_choked(d_wsi) && !d_queue.empty())
  {
    WebSocketBuffer& buffer = d_queue.front();

    const int totalSize = static_cast<int>(buffer.GetSize()) - LWS_SEND_BUFFER_PRE_PADDING - LWS_SEND_BUFFER_POST_PADDING;

    ASSERT(d_bytesSent < totalSize);

    uint8_t* start = buffer.GetBuffer() + LWS_SEND_BUFFER_PRE_PADDING + d_bytesSent;

    const int remainingSize = totalSize - d_bytesSent;
    const int frameSize = min(2048, remainingSize);

    int writeMode = d_bytesSent == 0
      ? LWS_WRITE_TEXT
      : LWS_WRITE_CONTINUATION;

    if (frameSize != remainingSize)
      writeMode |= LWS_WRITE_NO_FIN;

    bool storePostPadding = d_bytesSent + frameSize < totalSize;
    std::array<uint8_t,LWS_SEND_BUFFER_POST_PADDING> postPadding{};
    if (storePostPadding)
      std::copy(start + frameSize, start + frameSize + LWS_SEND_BUFFER_POST_PADDING, postPadding.data());

    int res = lws_write(d_wsi, start, frameSize, (lws_write_protocol)writeMode);

    if (res < 0)
    {
      Log::error("JsonSession::write") << "Error writing JSON to socket (" << res << ")";
      return 1;
    }

    d_bytesSent += frameSize;

    if (d_bytesSent == totalSize)
    {
      // Done sending this queue item, so ditch it, reset and loop around again
      d_queue.pop();
      d_bytesSent = 0;
    }
    else if (storePostPadding)
    {
      std::copy(postPadding.data(), postPadding.data() + LWS_SEND_BUFFER_POST_PADDING, start + frameSize);
    }
  }

  // Queue for more writing later on if we still have data remaining
  if (!d_queue.empty())
    lws_callback_on_writable(d_wsi);

  return 0;
}

void JsonSession::enqueue(WebSocketBuffer buffer, bool suppressLwsNotify)
{
  lock_guard<mutex> guard(d_queueMutex);

  ASSERT(buffer.GetSize() != 0);

  auto queueSize = static_cast<unsigned>(d_queue.size());

  if (queueSize / 10 > d_maxQueueSeen / 10)
  {
    d_maxQueueSeen = queueSize;
    Log::warning("JsonSession::enqueue") << d_protocolName << " max queue seen " << queueSize;
  }

  // If queue is too long, deal with it
  const int MaxQueueSize = 200;
  if (queueSize > MaxQueueSize)
  {
    Log::error("StateUpdated") << "JsonSession queue to " << d_hostName << '@' << d_ipAddress << " for protocol '" << d_protocolName << "' too long (" << queueSize << " > " << MaxQueueSize << ") — purging";
    queue<WebSocketBuffer> empty;
    swap(d_queue, empty);
  }
  else
  {
    buffer.Push(LWS_SEND_BUFFER_POST_PADDING);
    d_queue.emplace(move(buffer));

    if (!suppressLwsNotify)
      lws_callback_on_writable(d_wsi);
  }
}
