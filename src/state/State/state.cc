// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "state.hh"

#include "state/StateObject/stateobject.hh"

#include "util/SequentialTimer/sequentialtimer.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

sigc::signal<void, shared_ptr<StateTracker const>> State::updated;

mutex State::s_mutex;

unordered_map<type_index, vector<shared_ptr<StateObserver>>> State::s_observersByTypeIndex;
unordered_map<int, vector<shared_ptr<StateObserver>>> State::s_observersByThreadId;
unordered_map<type_index, shared_ptr<StateTracker>> State::s_trackerByTypeId;
unordered_map<string, shared_ptr<StateTracker>> State::s_trackerByName;

void State::initialise()
{
  // Only allow observers to be called back on specified threads
  s_observersByThreadId[(int)ThreadId::MotionLoop] = vector<shared_ptr<StateObserver>>();
  s_observersByThreadId[(int)ThreadId::ThinkLoop] = vector<shared_ptr<StateObserver>>();
}

shared_ptr<StateObjectBase const> State::getByName(string name)
{
  auto it = s_trackerByName.find(name);
  if (it == s_trackerByName.end())
  {
    Log::warning("State::getByName") << "No tracker exists with name " << name;
    return nullptr;
  }

  shared_ptr<StateTracker> const& tracker = it->second;
  return tracker->stateBase();
}

vector<shared_ptr<StateTracker>> State::getTrackers()
{
  vector<shared_ptr<StateTracker>> stateObjects;
  lock_guard<mutex> guard(s_mutex);
  transform(s_trackerByTypeId.begin(), s_trackerByTypeId.end(),
                  back_inserter(stateObjects),
                  [](decltype(s_trackerByTypeId)::value_type const& pair) { return pair.second; });
  return stateObjects;
}

void State::registerObserver(shared_ptr<StateObserver> observer)
{
  ASSERT(observer);

  // TODO STATE assert that we are in the configuration phase

  // Store the observer by each type is supports
  for (type_index const& type : observer->getTypes())
  {
    auto it = s_observersByTypeIndex.find(type);
    ASSERT(it != s_observersByTypeIndex.end() && "Tracker type must be registered");
    it->second.push_back(observer);
  }

  // Store the observer by ThreadUtil
  auto it2 = s_observersByThreadId.find((int)observer->getCallbackThreadId());
  ASSERT(it2 != s_observersByThreadId.end() && "Observers not supported for this ThreadI");
  it2->second.push_back(observer);
}

void State::callbackObservers(ThreadId threadId, SequentialTimer& timer)
{
  // TODO STATE assert that we are NOT in the configuration phase

  // Find all observers for the specified thread and call them, if they're dirty
  auto it = s_observersByThreadId.find((int)threadId);
  ASSERT(it != s_observersByThreadId.end() && "No observers for specified thread");

  for (shared_ptr<StateObserver> const& observer : it->second)
  {
    if (observer->testAndClearDirty())
    {
      timer.enter(observer->getName());
      observer->observe(timer);
      timer.exit();
    }
  }
}

shared_ptr<StateObjectBase const> State::getByTypeIndex(type_index const& typeIndex)
{
  // TODO STATE if this is readonly at this point, do we need to lock? can trackers be added/removed dynamically? just assert we're not in configuration mode
  lock_guard<mutex> guard(s_mutex);
  auto pair = s_trackerByTypeId.find(typeIndex);
  ASSERT(pair != s_trackerByTypeId.end() && "Tracker type must be registered");
  auto tracker = pair->second;
  return tracker->stateBase();
}
