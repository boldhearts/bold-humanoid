// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <unordered_map>
#include <memory>
#include <mutex>
#include <sigc++/signal.h>
#include <type_traits>
#include <typeindex>
#include <vector>

#include "state/StateObserver/stateobserver.hh"
#include "util/assert.hh"
#include "util/Log/log.hh"
#include "util/memory.hh"

namespace bold {
  namespace util {
    class SequentialTimer;
  }

  namespace state {
    class StateObjectBase;

    enum class StateTime {
      MostRecent,
      CameraImage
    };

    class StateTracker {
    public:
      template<typename T>
      static std::shared_ptr<StateTracker> create(std::string name) {
        static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");
        return std::make_shared<StateTracker>(name);
      }

      StateTracker(std::string name)
          : d_name(name) {}

      void set(std::shared_ptr<StateObjectBase const> state) {
        std::lock_guard<std::mutex> guard(s_mutex);
        d_stateMostRecent = state;
        d_updateCount++;
      }

      void snapshot(StateTime time) {
        if (time == StateTime::CameraImage) {
          std::lock_guard<std::mutex> guard(s_mutex);
          d_stateCameraImage = d_stateMostRecent;
        } else {
          util::Log::error("StateTracker::snapshot") << "Unexpected StateTime value: " << (int) time;
          throw std::runtime_error("Unexpected StateTime value");
        }
      }

      template<typename T>
      std::shared_ptr<T const> state(StateTime time = StateTime::MostRecent) const {
        static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");
        std::lock_guard<std::mutex> guard(s_mutex);
        return std::dynamic_pointer_cast<T const>(
            time == StateTime::MostRecent ? d_stateMostRecent : d_stateCameraImage);
      }

      std::shared_ptr<StateObjectBase const> stateBase(StateTime time = StateTime::MostRecent) const {
        std::lock_guard<std::mutex> guard(s_mutex);
        return time == StateTime::MostRecent ? d_stateMostRecent : d_stateCameraImage;
      }

      std::string const &name() const { return d_name; }

      long long unsigned updateCount() const { return d_updateCount; }

    private:
      mutable std::mutex s_mutex;
      const std::string d_name;
      std::shared_ptr<StateObjectBase const> d_stateMostRecent;
      std::shared_ptr<StateObjectBase const> d_stateCameraImage;
      long long unsigned d_updateCount;
    };

    class State {
    public:
      static void initialise();

      template<typename T>
      static void registerStateType(std::string name);

      static std::vector<std::shared_ptr<StateTracker>> getTrackers();

      static unsigned stateTypeCount() { return s_trackerByTypeId.size(); }

      /** Fires when a state object is updated.
       *
       * Currently only used by DataStreamer, which may serialise the
       * StateObject for attached clients.
       *
       * A lock is held over s_mutex while this event is raised :(
       *
       * TODO avoid unbounded holding of s_mutex by getting rid of this
       * signal and using a queue for the DataStreamer thread to process
       * (?)
       */
      static sigc::signal<void, std::shared_ptr<StateTracker const>> updated;

      static void registerObserver(std::shared_ptr<StateObserver> observer);

      static void callbackObservers(ThreadId threadId, util::SequentialTimer &timer);

      template<typename T>
      static void set(std::shared_ptr<T const> state);

      template<typename T, typename... TArgs>
      static std::shared_ptr<T const> make(TArgs &&... args);

      /** Get the StateObject of specified type T. May be nullptr.
       */
      template<typename T>
      static std::shared_ptr<T const> get(StateTime time = StateTime::MostRecent);

      static std::shared_ptr<StateObjectBase const> getByName(std::string name);

      template<typename T>
      static std::shared_ptr<T const> getTrackerState(StateTime time = StateTime::MostRecent);

      static std::shared_ptr<StateObjectBase const> getByTypeIndex(std::type_index const &typeIndex);

      template<typename T>
      static std::shared_ptr<StateTracker> getTracker();

      static void snapshot(StateTime time) {
        std::lock_guard<std::mutex> guard(s_mutex);
        for (auto const &pair : s_trackerByTypeId)
          pair.second->snapshot(time);
      }

    private:
      State() = delete;

      static std::mutex s_mutex;

      static std::unordered_map<std::type_index, std::vector<std::shared_ptr<StateObserver>>> s_observersByTypeIndex;
      static std::unordered_map<int, std::vector<std::shared_ptr<StateObserver>>> s_observersByThreadId;
      static std::unordered_map<std::type_index, std::shared_ptr<StateTracker>> s_trackerByTypeId;
      static std::unordered_map<std::string, std::shared_ptr<StateTracker>> s_trackerByName;
    };

    template<typename T>
    void State::registerStateType(std::string name) {
      static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObject");
      util::Log::verbose("State::registerStateType") << "Registering state type: " << name;

      std::lock_guard<std::mutex> guard(s_mutex);
      if (s_trackerByTypeId.find(typeid(T)) != s_trackerByTypeId.end()) {
        util::Log::warning("State::registerStateType") << "State type already registered: " << name;
        return;
      }

      auto const &tracker = StateTracker::create<T>(name);
      s_trackerByTypeId[typeid(T)] = tracker;
      s_trackerByName[name] = tracker;

      // Create an empty vector for the observers so that we don't have to lock later on the observer map
      s_observersByTypeIndex[typeid(T)] = {};
    }

    template<typename T>
    void State::set(std::shared_ptr<T const> state) {
      static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");

      auto const &tracker = getTracker<T const>();
      tracker->set(state);

      std::vector<std::shared_ptr<StateObserver>> *observers;

      // Raise event and look up observers while holding lock
      {
        // TODO this blocks for too long. eventing won't work well. need to do all updates async, off the motion thread
        std::lock_guard<std::mutex> guard(s_mutex);
        updated(tracker);

        auto it = s_observersByTypeIndex.find(typeid(T));
        ASSERT(it != s_observersByTypeIndex.end());
        observers = &it->second;
      }

      // Release lock before notifying observers
      for (auto &observer : *observers) {
        ASSERT(observer);
        observer->setDirty();
      }
    }

    template<typename T, typename... Args>
    std::shared_ptr<T const> State::make(Args &&... args) {
      // TODO: we don't always need aligned allocation
      std::shared_ptr<T const> state = util::allocate_aligned_shared<T const>(std::forward<Args>(args)...);
      set(state);
      return state;
    }

    template<typename T>
    std::shared_ptr<T const> State::get(StateTime time) {
      static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");
      return State::getTrackerState<T>(time);
    }

    template<typename T>
    std::shared_ptr<T const> State::getTrackerState(StateTime time) {
      static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");
      std::lock_guard<std::mutex> guard(s_mutex);
      auto pair = s_trackerByTypeId.find(typeid(T));
      ASSERT(pair != s_trackerByTypeId.end() && "Tracker type must be registered");
      return pair->second->state<T>(time);
    }

    template<typename T>
    std::shared_ptr<StateTracker> State::getTracker() {
      static_assert(std::is_base_of<StateObjectBase, T>::value, "T must be a descendant of StateObjectBase");
      std::lock_guard<std::mutex> guard(s_mutex);
      auto pair = s_trackerByTypeId.find(typeid(T));
      ASSERT(pair != s_trackerByTypeId.end() && "Tracker type must be registered");
      auto tracker = pair->second;
      return tracker;
    }
  }
}
