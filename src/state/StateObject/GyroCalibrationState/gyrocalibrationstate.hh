// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include <Eigen/Core>

namespace bold
{
  namespace state {

    class GyroCalibrationState : public StateObject<GyroCalibrationState> {
    public:
      enum class CalibrationState {
        /// Calibration is occurring
            PREPARING,
        /// Calibration failed to determine a reliable baseline
            ERRORED,
        /// Calibration completed successfully
            COMPLETE
      };


      GyroCalibrationState(Eigen::Vector3i gyroZeroRateLevel)
          : d_gyroZeroRateLevel(gyroZeroRateLevel) {}

      Eigen::Vector3i getZeroRateLevel() const {
        return d_gyroZeroRateLevel;
      }

    private:
      friend class StateObject<GyroCalibrationState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      CalibrationState d_state;
      Eigen::Vector3i d_gyroZeroRateLevel;
    };

    template<typename TBuffer>
    inline void GyroCalibrationState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("state");
        switch (d_state) {
          case CalibrationState::PREPARING:
            writer.String("PREPARING");
            break;
          case CalibrationState::ERRORED:
            writer.String("ERRORED");
            break;
          case CalibrationState::COMPLETE:
            writer.String("COMPLETE");
            break;
        };

        writer.String("gyroZeroRateLevel");
        writer.StartArray();
        {
          writer.Int(d_gyroZeroRateLevel.x());
          writer.Int(d_gyroZeroRateLevel.y());
          writer.Int(d_gyroZeroRateLevel.z());
        }
      }
    }
  }
}
