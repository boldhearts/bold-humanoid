// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "../stateobject.hh"

namespace bold {
  namespace state {
    class FieldEdge {
    public:
      virtual ~FieldEdge() = default;

      virtual uint16_t getYAt(uint16_t x) const = 0;
    };

    class FieldEdgeState : public StateObject<FieldEdgeState> {
    public:
      FieldEdgeState(std::unique_ptr<FieldEdge> edge)
          : d_edge{std::move(edge)} {}

      FieldEdge const &getEdge() const { return *d_edge; }

    private:
      friend class StateObject<FieldEdgeState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {}

      std::unique_ptr<FieldEdge> d_edge;
    };
  }
}
