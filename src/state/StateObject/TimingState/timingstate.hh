// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "src/colour/colour.hh"
#include "src/util/json.hh"

#include <memory>
#include <vector>

namespace bold
{
  namespace state {
    typedef std::pair<std::string, double> EventTiming;

    class TimingState : public StateObject<TimingState> {
    protected:
      TimingState(std::shared_ptr<std::vector<EventTiming>> eventTimings, ulong cycleNumber, double averageFps)
          : d_eventTimings(eventTimings),
            d_cycleNumber(cycleNumber),
            d_averageFps(averageFps) {}

      virtual ~TimingState() = default;

    public:
      std::shared_ptr<std::vector<EventTiming> const> getTimings() const { return d_eventTimings; }

      ulong getCycleNumber() const { return d_cycleNumber; }

      double getAverageFps() const { return d_averageFps; }

    private:
      friend class StateObject<TimingState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      std::shared_ptr<std::vector<EventTiming>> d_eventTimings;
      ulong d_cycleNumber;
      double d_averageFps;
    };

    template<typename TBuffer>
    inline void TimingState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("cycle");
        writer.Uint64(d_cycleNumber);
        writer.String("fps");
        writer.Double(d_averageFps);
        writer.String("timings");
        writer.StartObject();
        {
          std::vector<EventTiming> const &timings = *d_eventTimings;
          for (EventTiming const &timing : timings) {
            writer.String(timing.first.c_str()); // event name
            writer.Double(timing.second);  // duration in milliseconds
          }
        }
        writer.EndObject();
      }
      writer.EndObject();
    }

    class MotionTimingState : public TimingState {
    public:
      MotionTimingState(std::shared_ptr<std::vector<EventTiming>> eventTimings, ulong cycleNumber, double averageFps)
          : TimingState(eventTimings, cycleNumber, averageFps) {}
    };

    class ThinkTimingState : public TimingState {
    public:
      ThinkTimingState(std::shared_ptr<std::vector<EventTiming>> eventTimings, ulong cycleNumber, double averageFps)
          : TimingState(eventTimings, cycleNumber, averageFps) {}
    };
  }
}
