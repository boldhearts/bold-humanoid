// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "src/util/Maybe/maybe.hh"

#include <array>

namespace bold
{
  class BulkReadTable;
  class FSRModel;

  namespace state {

    class FSRState : public StateObject<FSRState> {
    public:
      FSRState(BulkReadTable const &dataL, BulkReadTable const &dataR, FSRModel const &model);

      std::array<uint8_t, 2> getRawCenterL() const {
        return d_rawCenterL;
      }

      std::array<uint8_t, 2> getRawCenterR() const {
        return d_rawCenterR;
      }

      util::Maybe<Eigen::Vector2d> getCenterL() const {
        return d_centerL;
      }

      util::Maybe<Eigen::Vector2d> getCenterR() const {
        return d_centerR;
      }

      std::array<uint16_t, 4> getRawReadingsL() const {
        return d_rawReadingsL;
      }

      std::array<uint16_t, 4> getRawReadingsR() const {
        return d_rawReadingsR;
      }

      Eigen::Vector4d getReadingsL() const {
        return d_readingsL;
      }

      Eigen::Vector4d getReadingsR() const {
        return d_readingsR;
      }

    private:
      friend class StateObject<FSRState>;

      template<class TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
        writer.StartObject();
        {
          writer.String("left");
          writer.StartObject();
          {
            writer.String("readings");
            writer.StartArray();
            {
              writer.Double(d_readingsL[0]);
              writer.Double(d_readingsL[1]);
              writer.Double(d_readingsL[2]);
              writer.Double(d_readingsL[3]);
            }
            writer.EndArray();
            if (d_centerL.hasValue()) {
              writer.String("center");
              writer.StartArray();
              {
                writer.Double(d_centerL.value()[0]);
                writer.Double(d_centerL.value()[1]);
              }
              writer.EndArray();
            }
          }
          writer.EndObject();

          writer.String("right");
          writer.StartObject();
          {
            writer.String("readings");
            writer.StartArray();
            {
              writer.Double(d_readingsR[0]);
              writer.Double(d_readingsR[1]);
              writer.Double(d_readingsR[2]);
              writer.Double(d_readingsR[3]);
            }
            writer.EndArray();
            if (d_centerR.hasValue()) {
              writer.String("center");
              writer.StartArray();
              {
                writer.Double(d_centerR.value()[0]);
                writer.Double(d_centerR.value()[1]);
              }
              writer.EndArray();
            }
          }
          writer.EndObject();
        }
        writer.EndObject();
      }

      std::array<uint16_t, 4> d_rawReadingsL;
      std::array<uint16_t, 4> d_rawReadingsR;

      Eigen::Vector4d d_readingsL;
      Eigen::Vector4d d_readingsR;

      std::array<uint8_t, 2> d_rawCenterL;
      std::array<uint8_t, 2> d_rawCenterR;

      util::Maybe<Eigen::Vector2d> d_centerL;
      util::Maybe<Eigen::Vector2d> d_centerR;

    };
  }
}
