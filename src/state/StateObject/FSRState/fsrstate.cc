// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fsrstate.hh"

#include "BulkReadTable/bulkreadtable.hh"
#include "FSRModel/fsrmodel.hh"
#include "FSR/fsr.hh"

using namespace bold;
using namespace bold::state;

FSRState::FSRState(BulkReadTable const& dataL, BulkReadTable const& dataR, FSRModel const& model)
{
  d_rawReadingsL = {{ dataL.readWord(FSRTable::FSR1_L),
                      dataL.readWord(FSRTable::FSR2_L),
                      dataL.readWord(FSRTable::FSR3_L),
                      dataL.readWord(FSRTable::FSR4_L) }};
  d_rawReadingsR = {{ dataR.readWord(FSRTable::FSR1_L),
                      dataR.readWord(FSRTable::FSR2_L),
                      dataR.readWord(FSRTable::FSR3_L),
                      dataR.readWord(FSRTable::FSR4_L) }};

  d_rawCenterL = {{ dataL.readByte(FSRTable::FSR_X), dataL.readByte(FSRTable::FSR_Y) }};
  d_rawCenterR = {{ dataR.readByte(FSRTable::FSR_X), dataR.readByte(FSRTable::FSR_Y) }};

  for (size_t i = 0; i < 4; ++i)
  {
    d_readingsL[i] = model.fsrValueToNs(d_rawReadingsL[i]);
    d_readingsR[i] = model.fsrValueToNs(d_rawReadingsR[i]);
  }
  d_centerL = model.centerValueTo2D(d_rawCenterL[0], d_rawCenterL[1], FSRModel::LEFT);
  d_centerR = model.centerValueTo2D(d_rawCenterR[0], d_rawCenterR[1], FSRModel::RIGHT);
}
