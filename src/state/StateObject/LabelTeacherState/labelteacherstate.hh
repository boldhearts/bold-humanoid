// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "src/colour/colour.hh"
#include "src/colour/HSV/hsv.hh"
#include "src/colour/HSVRange/hsvrange.hh"

namespace bold {
  namespace state {

    class LabelTeacherState : public StateObject<LabelTeacherState> {
    public:
      LabelTeacherState(colour::HSVRange selectedRange, std::pair<colour::HSV, colour::HSV> selectedDistribution)
          : d_selectedRange(selectedRange),
            d_selectedDistribution(selectedDistribution) {}

    private:
      friend class StateObject<LabelTeacherState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      colour::HSVRange d_selectedRange;
      std::pair<colour::HSV, colour::HSV> d_selectedDistribution;
    };


    template<typename TBuffer>
    void LabelTeacherState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("selectedRange");
        writer.StartObject();
        {
          writer.String("hue");
          writer.StartArray();
          {
            writer.Uint(d_selectedRange.hMin);
            writer.Uint(d_selectedRange.hMax);
          }
          writer.EndArray();
          writer.String("sat");
          writer.StartArray();
          {
            writer.Uint(d_selectedRange.sMin);
            writer.Uint(d_selectedRange.sMax);
          }
          writer.EndArray();
          writer.String("val");
          writer.StartArray();
          {
            writer.Uint(d_selectedRange.vMin);
            writer.Uint(d_selectedRange.vMax);
          }
          writer.EndArray();
        }
        writer.EndObject();

        writer.String("selectedDist");
        writer.StartObject();
        {
          writer.String("hue");
          writer.StartArray();
          {
            writer.Uint(d_selectedDistribution.first.h);
            writer.Uint(d_selectedDistribution.second.h);
          }
          writer.EndArray();
          writer.String("sat");
          writer.StartArray();
          {
            writer.Uint(d_selectedDistribution.first.s);
            writer.Uint(d_selectedDistribution.second.s);
          }
          writer.EndArray();
          writer.String("val");
          writer.StartArray();
          {
            writer.Uint(d_selectedDistribution.first.v);
            writer.Uint(d_selectedDistribution.second.v);
          }
          writer.EndArray();
        }
        writer.EndObject();
      }
      writer.EndObject();
    }
  }
}
