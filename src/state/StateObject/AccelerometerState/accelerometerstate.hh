// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "AccelerometerModel/accelerometermodel.hh"

#include <Eigen/Core>

namespace bold
{
  namespace state {
    class AccelerometerState : public StateObject<AccelerometerState> {
    public:
      AccelerometerState(Eigen::Vector3i raw, AccelerometerModel const &accelerometerModel);

      /// Raw raw value of the accelerometer, in range [0,1023] corresponding to [-4,4] g.
      Eigen::Vector3i const &raw() const { return d_raw; }

      /// The converted accelerometer output, in gs.
      Eigen::Vector3d const &gs() const { return d_gs; }

    private:
      friend class StateObject<AccelerometerState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      Eigen::Vector3i d_raw;
      Eigen::Vector3d d_gs;
    };


    inline AccelerometerState::AccelerometerState(Eigen::Vector3i raw, AccelerometerModel const &accelerometerModel)
        : d_raw(std::move(raw)) {
      d_gs = Eigen::Vector3d(
          accelerometerModel.valueToGs(raw.x()),
          accelerometerModel.valueToGs(raw.y()),
          accelerometerModel.valueToGs(raw.z())
      );
    }

    template<typename TBuffer>
    void AccelerometerState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("rawAcc");
        writer.StartArray();
        {
          writer.Int(d_raw.x());
          writer.Int(d_raw.y());
          writer.Int(d_raw.z());
        }
        writer.EndArray();

        writer.String("acc");
        writer.StartArray();
        {
          writer.Double(d_gs.x());
          writer.Double(d_gs.y());
          writer.Double(d_gs.z());
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
