// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>

#include "state/StateObject/stateobject.hh"
#include "CM730Snapshot/cm730snapshot.hh"
#include "JointId/jointid.hh"
#include "MX28Snapshot/mx28snapshot.hh"
#include "src/util/assert.hh"

namespace bold
{
  namespace state {

    class HardwareState : public StateObject<HardwareState> {
    public:
      HardwareState(std::unique_ptr<CM730Snapshot const> cm730State,
                    std::vector<std::unique_ptr<MX28Snapshot const>> mx28States,
                    uint32_t rxBytes,
                    uint32_t txBytes,
                    uint32_t motionCycleNumber)
          : d_cm730State(std::move(cm730State)),
            d_mx28States(std::move(mx28States)),
            d_rxBytes(rxBytes),
            d_txBytes(txBytes),
            d_motionCycleNumber(motionCycleNumber) {
        ASSERT(d_mx28States.size() == 20);
      }

      CM730Snapshot const &getCM730State() const {
        return *d_cm730State;
      }

      MX28Snapshot const &getMX28State(uint8_t jointId) const {
        ASSERT(jointId >= (uint8_t) JointId::MIN && jointId <= (uint8_t) JointId::MAX);
        ASSERT(d_mx28States.size() >= jointId);

        return *d_mx28States[jointId - 1];
      }

      uint32_t getReceivedBytes() const { return d_rxBytes; }

      uint32_t getTransmittedBytes() const { return d_txBytes; }

      uint32_t getMotionCycleNumber() const { return d_motionCycleNumber; }

    private:
      friend class StateObject<HardwareState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      std::unique_ptr<CM730Snapshot const> d_cm730State;
      std::vector<std::unique_ptr<MX28Snapshot const>> d_mx28States;
      uint32_t d_rxBytes;
      uint32_t d_txBytes;
      uint32_t d_motionCycleNumber;
    };

    template<typename TBuffer>
    inline void HardwareState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("cycle");
        writer.Uint64(d_motionCycleNumber);

        writer.String("acc");
        writer.StartArray();
        writer.Double(d_cm730State->acc.x());
        writer.Double(d_cm730State->acc.y());
        writer.Double(d_cm730State->acc.z());
        writer.EndArray();

        writer.String("rawGyro");
        writer.StartArray();
        writer.Int(d_cm730State->gyroRaw.x());
        writer.Int(d_cm730State->gyroRaw.y());
        writer.Int(d_cm730State->gyroRaw.z());
        writer.EndArray();

        writer.String("gyro");
        writer.StartArray();
        writer.Double(d_cm730State->gyro.x());
        writer.Double(d_cm730State->gyro.y());
        writer.Double(d_cm730State->gyro.z());
        writer.EndArray();

        writer.String("eye");
        writer.StartArray();
        writer.Double(d_cm730State->eyeColor.x());
        writer.Double(d_cm730State->eyeColor.y());
        writer.Double(d_cm730State->eyeColor.z());
        writer.EndArray();

        writer.String("forehead");
        writer.StartArray();
        writer.Double(d_cm730State->foreheadColor.x());
        writer.Double(d_cm730State->foreheadColor.y());
        writer.Double(d_cm730State->foreheadColor.z());
        writer.EndArray();

        writer.String("led2");
        writer.Bool(d_cm730State->isLed2On);
        writer.String("led3");
        writer.Bool(d_cm730State->isLed3On);
        writer.String("led4");
        writer.Bool(d_cm730State->isLed4On);

        writer.String("volts");
        writer.Double(d_cm730State->voltage);

        writer.String("rxBytes");
        writer.Uint64(d_rxBytes);
        writer.String("txBytes");
        writer.Uint64(d_txBytes);

        writer.String("joints");
        writer.StartArray();
        for (auto const &mx28 : d_mx28States) {
          writer.StartObject();
          {
            writer.String("id");
            writer.Int(mx28->id);
//        writer.String("movingSpeedRPM");
//        writer.Int(mx28->movingSpeedRPM);
            writer.String("val");
            writer.Int(mx28->presentPositionValue);
            writer.String("rpm");
            writer.Double(mx28->presentSpeedRPM);
            writer.String("load");
            writer.Double(mx28->presentLoad);
            writer.String("temp");
            writer.Int(mx28->presentTemp);
            writer.String("volts");
            writer.Double(mx28->presentVoltage);
          }
          writer.EndObject();
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
