// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>

#include "state/StateObject/stateobject.hh"
#include "OcclusionRay/occlusionray.hh"
#include "src/geometry2/Geometry/LineSegment/linesegment.hh"
#include "src/util/Maybe/maybe.hh"

namespace bold
{
  typedef std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d>> Vector2dVector;

  namespace state {

    class CameraFrameState : public StateObject<CameraFrameState> {
    public:
      CameraFrameState(util::Maybe <Eigen::Vector2d> ballObservation,
                       Vector2dVector goalObservations,
                       Vector2dVector teamMateObservations,
                       geometry2::LineSegment2i::Vector observedLineSegments,
                       std::vector<OcclusionRay < uint16_t>>

    occlusionRays,
      long totalPixelCount,
      long processedPixelCount,
          ulong
      thinkCycleNumber)
      :

      d_ballObservation (ballObservation),
      d_goalObservations(goalObservations),
      d_teamMateObservations(teamMateObservations),
      d_observedLineSegments(observedLineSegments),
      d_occlusionRays(occlusionRays),
      d_totalPixelCount(totalPixelCount),
      d_processedPixelCount(processedPixelCount),
      d_thinkCycleNumber(thinkCycleNumber) {}

      util::Maybe <Eigen::Vector2d> getBallObservation() const { return d_ballObservation; }

      bool isBallVisible() const { return d_ballObservation.hasValue(); }

      Vector2dVector getGoalObservations() const { return d_goalObservations; }

      Vector2dVector getTeamMateObservations() const { return d_teamMateObservations; }

      int getGoalObservationCount() const { return d_goalObservations.size(); }

      std::vector<OcclusionRay < uint16_t>> getOcclusionRays() const { return d_occlusionRays; }

      geometry2::LineSegment2i::Vector getObservedLineSegments() const { return d_observedLineSegments; }

      long getTotalPixelCount() const { return d_totalPixelCount; }

      long getProcessedPixelCount() const { return d_processedPixelCount; }

      float getProcessedPixelRatio() const { return (float) d_processedPixelCount / d_totalPixelCount; }

      ulong getThinkCycleNumber() const { return d_thinkCycleNumber; }

    private:
      friend class StateObject<CameraFrameState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      util::Maybe <Eigen::Vector2d> d_ballObservation;
      Vector2dVector d_goalObservations;
      Vector2dVector d_teamMateObservations;
      geometry2::LineSegment2i::Vector d_observedLineSegments;
      std::vector<OcclusionRay < uint16_t>> d_occlusionRays;
      long d_totalPixelCount;
      long d_processedPixelCount;
      ulong d_thinkCycleNumber;
    };

    template<typename TBuffer>
    inline void CameraFrameState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("thinkCycle");
        writer.Uint64(d_thinkCycleNumber);

        writer.String("ball");
        if (d_ballObservation.hasValue()) {
          writer.StartArray();
          writer.Double(d_ballObservation->x());
          writer.Double(d_ballObservation->y());
          writer.EndArray();
        } else {
          writer.Null();
        }

        writer.String("goals");
        writer.StartArray();
        {
          for (auto const &goalPos : d_goalObservations) {
            writer.StartArray();
            writer.Double(goalPos.x());
            writer.Double(goalPos.y());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("teammates");
        writer.StartArray();
        {
          for (auto const &teamMatePos : d_teamMateObservations) {
            writer.StartArray();
            writer.Double(teamMatePos.x());
            writer.Double(teamMatePos.y());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("lines");
        writer.StartArray();
        {
          for (geometry2::LineSegment2i const &lineSeg : d_observedLineSegments) {
            writer.StartArray();
            writer.Double(lineSeg.p1().x());
            writer.Double(lineSeg.p1().y());
            writer.Double(lineSeg.p2().x());
            writer.Double(lineSeg.p2().y());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("occlusionRays");
        writer.StartArray();
        {
          for (auto const &ray : d_occlusionRays) {
            writer.StartArray();
            writer.Uint(ray.near().x());
            writer.Uint(ray.near().y());
            writer.Uint(ray.far().x());
            writer.Uint(ray.far().y());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("totalPixelCount");
        writer.Uint64(d_totalPixelCount);

        writer.String("processedPixelCount");
        writer.Uint64(d_processedPixelCount);
      }
      writer.EndObject();
    }
  }
}
