// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bodystate.hh"

#include "config/Config/config.hh"
#include "MX28/mx28.hh"
#include "BodyModel/bodymodel.hh"

#include "state/State/state.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "state/StateObject/OrientationState/orientationstate.hh"

#include "motion/core/BodyControl/bodycontrol.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::core;
using namespace bold::state;
using namespace bold::util;
using namespace bold::geometry2;
using namespace std;
using namespace Eigen;

LimbPosition::LimbPosition(shared_ptr<Limb const> limb, Affine3d const &transform)
    : BodyPartPosition{transform},
      d_limb{move(limb)} {
  ASSERT(d_limb);
}

Point3d LimbPosition::getCentreOfMassPosition() const {
  return getTransform() * getLimb()->com;
}

////////////////////////////////////////////////////////////////////////////////

JointPosition::JointPosition(shared_ptr<Joint const> joint, Affine3d const &transform, double angleRads)
    : BodyPartPosition{transform},
      d_joint{move(joint)},
      d_angleRads{angleRads} {
  ASSERT(d_joint);
}

////////////////////////////////////////////////////////////////////////////////

BodyState::BodyState(BodyModel const &bodyModel,
                     array<double, 23> const &angles,
                     array<short, 21> const &positionValueDiffs,
                     ulong cycleNumber)
    : d_positionValueDiffById(positionValueDiffs),
      d_torso{},
      d_jointById{},
      d_limbByName{},
      d_centreOfMass{0, 0, 0},
      d_isCentreOfMassComputed{false},
      d_motionCycleNumber{cycleNumber} {
  initialise(bodyModel, angles);
}

BodyState::BodyState(BodyModel const &bodyModel,
                     HardwareState const &hardwareState,
                     BodyControl const &bodyControl,
                     ulong cycleNumber)
    : d_torso{},
      d_jointById{},
      d_limbByName{},
      d_centreOfMass{0, 0, 0},
      d_isCentreOfMassComputed{false},
      d_motionCycleNumber{cycleNumber} {
  // Add three extra as:
  // - we don't use index 0
  // - we add two extra joints for the camera's calibration pan & tilt within the head
  array<double, 23> angles;
  angles[0] = 0;

  for (uint8_t jointId = (uint8_t) JointId::MIN; jointId <= (uint8_t) JointId::MAX; jointId++) {
    auto const &joint = hardwareState.getMX28State(jointId);
    auto const &jointControl = bodyControl.getJoint((JointId) jointId);

    angles[jointId] = joint.presentPosition;
    d_positionValueDiffById[jointId] =
        (short) jointControl.getValue() + jointControl.getModulationOffset() - joint.presentPositionValue;
  }

  static auto tiltSetting = Config::getSetting<double>("camera.calibration.tilt-angle-degrees");
  static auto panSetting = Config::getSetting<double>("camera.calibration.pan-angle-degrees");

  // Set the camera head tilt according to the configured angle
  angles[(uint8_t) JointId::CAMERA_CALIB_TILT] = Math::degToRad(tiltSetting->getValue());
  angles[(uint8_t) JointId::CAMERA_CALIB_PAN] = Math::degToRad(panSetting->getValue());

  initialise(bodyModel, angles);
}

Point3d const &BodyState::getCentreOfMass() const {
  if (!d_isCentreOfMassComputed) {
    double totalMass = 0.0;
    Vector3d weightedSum(0, 0, 0);

    visitLimbs([&](shared_ptr<LimbPosition const> limbPosition) {
      double mass = limbPosition->getLimb()->mass;
      totalMass += mass;
      weightedSum += mass * (limbPosition->getCentreOfMassPosition() - Point3d::ORIGIN);
    });

    d_centreOfMass = Point3d::ORIGIN + weightedSum / totalMass;
    d_isCentreOfMassComputed = true;
  }

  return d_centreOfMass;
}

shared_ptr<LimbPosition const> BodyState::getLimb(string const &name) const {
  // NOTE cannot use '[]' on a const map
  auto const &i = d_limbByName.find(name);
  if (i == d_limbByName.end()) {
    Log::error("BodyState::getJoint") << "Invalid limb name: " << name;
    throw runtime_error("Invalid limb name: " + name);
  }
  return i->second;
}

shared_ptr<JointPosition const> BodyState::getJoint(JointId jointId) const {
  ASSERT(jointId >= JointId::MIN && jointId <= JointId::MAX);
  return d_jointById[(uint8_t) jointId];
}

void BodyState::visitJoints(function<void(shared_ptr<JointPosition const> const &)> visitor) const {
  for (uint8_t jointId = (uint8_t) JointId::MIN; jointId <= (uint8_t) JointId::MAX; jointId++)
    visitor(d_jointById[(uint8_t) jointId]);
}

void BodyState::visitLimbs(function<void(shared_ptr<LimbPosition const> const &)> visitor) const {
  for (auto const &pair : d_limbByName)
    visitor(pair.second);
}

Affine3d BodyState::determineFootAgentTr(bool leftFoot) const {
  auto footTorsoTr = getLimb(leftFoot ? "left-foot" : "right-foot")->getTransform().inverse();

  return Math::alignUp(footTorsoTr);
}

shared_ptr<BodyState const> BodyState::zero(BodyModel const &bodyModel, ulong thinkCycleNumber) {
  array<double, 23> angles;
  angles.fill(0);

  // Tilt the head up slightly, so that we can see the horizon in the image (better for testing)
  angles[(int) JointId::HEAD_TILT] = MX28::degs2Value(20.0);

  array<short, 21> positionValueDiffs;
  positionValueDiffs.fill(0);

  return make_shared<BodyState>(bodyModel, angles, positionValueDiffs, thinkCycleNumber);
}

void BodyState::initialise(BodyModel const &bodyModel, array<double, 23> const &angles) {
  //
  // Build all Joint and Limb transforms
  //
  d_torso = allocate_aligned_shared<LimbPosition const>(bodyModel.getRoot(), Affine3d::Identity());

  list<shared_ptr<BodyPartPosition const>> partQueue;
  partQueue.push_back(d_torso);

  while (!partQueue.empty()) {
    shared_ptr<BodyPartPosition const> part = partQueue.front();
    partQueue.pop_front();

    if (shared_ptr<LimbPosition const> limbPosition = dynamic_pointer_cast<LimbPosition const>(part)) {
      //
      // LIMB: Determine transformation of all connected joints by applying  proper translation.
      //

      auto const &limb = limbPosition->getLimb();
      d_limbByName[limb->name] = limbPosition;

      // Loop over all joints extending outwards from this limb
      for (auto &joint : limb->joints) {
        // Transformation = that of limb plus translation to the joint

        double angle = angles[(uint8_t) joint->id];

        auto jointPosition = allocate_aligned_shared<JointPosition>(joint, limbPosition->getTransform() *
                                                                           Translation3d(joint->anchors.first), angle);
        partQueue.push_back(jointPosition);
      }
    } else {
      //
      // JOINT: Determine transformation of joint, apply rotation, then subsequent translation to child part.
      //

      shared_ptr<JointPosition const> jointPosition = dynamic_pointer_cast<JointPosition const>(part);

      auto joint = jointPosition->getJoint();
      d_jointById[(uint8_t) joint->id] = jointPosition;

      Affine3d childTransform
          = jointPosition->getTransform()
            * AngleAxisd(joint->rotationOrigin + jointPosition->getAngleRads(), joint->axis)
            * Translation3d(-joint->anchors.second);

      ASSERT(joint->childPart);

      if (shared_ptr<Joint const> childJoint = dynamic_pointer_cast<Joint const>(joint->childPart)) {
        double childJointAngle = angles[(uint8_t) childJoint->id];
        auto childJointPosition = allocate_aligned_shared<JointPosition>(childJoint, childTransform, childJointAngle);
        partQueue.push_back(childJointPosition);
      } else if (shared_ptr<Limb const> childLimb = dynamic_pointer_cast<Limb const>(joint->childPart)) {
        auto childLimbPosition = allocate_aligned_shared<LimbPosition>(childLimb, childTransform);
        partQueue.push_back(childLimbPosition);
      } else {
        Log::error("BodyState::initialise") << "Unknown body part type with name=" << joint->childPart->name;
      }
    }
  }

  //
  // Other bits
  //

  auto const &leftFootLimb = getLimb("left-foot");
  auto const &rightFootLimb = getLimb("right-foot");
  auto const &camera = getLimb("camera");

  double zl = leftFootLimb->getTransform().translation().z();
  double zr = rightFootLimb->getTransform().translation().z();

  auto lowestFoot = zl < zr ? leftFootLimb : rightFootLimb;

  // TODO this is an approximation
  d_torsoHeight = -std::min(zl, zr);

  static Setting<bool> *useOrientation = Config::getSetting<bool>("spatialiser.use-orientation");

  Affine3d agentTorsoRot;
  auto const &orientation = State::get<OrientationState>();
  if (useOrientation->getValue() && orientation) {
    agentTorsoRot = orientation->withoutYaw();
  } else {
    // TODO this rotation includes any yaw from the leg which isn't wanted
    agentTorsoRot = lowestFoot->getTransform().inverse().rotation();
  }

  Affine3d const &torsoCameraTr = camera->getTransform();

  // This is a special transform that gives the position of the camera in
  // the agent's frame, taking any rotation of the torso into account
  // considering the orientation of the foot (which is assumed to be flat.)
  auto agentTorsoTr = Translation3d(0, 0, d_torsoHeight) * agentTorsoRot;
  d_agentCameraTr = agentTorsoTr * torsoCameraTr;

  // We use the inverse a lot, so calculate it once here.
  d_cameraAgentTr = d_agentCameraTr.inverse();
}
