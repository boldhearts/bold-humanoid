// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <map>
#include <memory>
#include <array>
#include <Eigen/Geometry>

#include "state/StateObject/stateobject.hh"
#include "BodyPart/bodypart.hh"
#include "JointId/jointid.hh"
#include "src/geometry2/Point/point.hh"

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class BodyControl;
    }
  }
  
  class BodyModel;

  namespace state {

    class HardwareState;

    class BodyPartPosition {
    public:
      /** The transformation matrix of this body part.
      *
      * This matrix contains the position and orientation of the body part
      * relative to the torso.
      *
      * Using the returned Affine3d to transform a vector will convert
      * from the frame of this body part into the frame of the torso.
      */
      Eigen::Affine3d const &getTransform() const { return d_transform; }

      /** Get the position of the body part, relative to the torso. */
      geometry2::Point3d getPosition() const {
        return d_transform.translation().head<3>();
      }

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    protected:
      BodyPartPosition(Eigen::Affine3d const &transform)
          : d_transform(transform) {}

      virtual ~BodyPartPosition() = default;

      // TODO rename partTorsoTransform (and check that's correct!)
      Eigen::Affine3d d_transform;
    };

    class LimbPosition : public BodyPartPosition {
    public:
      LimbPosition(std::shared_ptr<Limb const> limb, Eigen::Affine3d const &transform);

      std::shared_ptr<Limb const> const &getLimb() const { return d_limb; }

      geometry2::Point3d getCentreOfMassPosition() const;

    private:
      std::shared_ptr<Limb const> d_limb;
    };

    class JointPosition : public BodyPartPosition {
    public:
      JointPosition(std::shared_ptr<Joint const> joint, Eigen::Affine3d const &transform, double angleRads);

      std::shared_ptr<Joint const> const &getJoint() const { return d_joint; }

      /// @returns the joint's axis direction vector in the agent coordinate system
      Eigen::Vector3d getAxisVec() const { return d_transform * d_joint->axis; }

      double getAngleRads() const { return d_angleRads; }

      double getAngleDegs() const { return Math::radToDeg(d_angleRads); }

    private:
      std::shared_ptr<Joint const> d_joint;
      double d_angleRads;
    };

    /** Models the kinematic chain of the robot's body.
     *
     * The coordinate frame of body parts has x pointing to the right,
     * y pointing forwards, and z pointing up, relative to the agent's forward
     * facing direction.
     *
     * The body is modelled using subclasses of BodyPart:
     * - Limb represents a rigid body part
     * - Joint represents a single revolute joint that connects two body parts
     *
     * The torso is the root of the kinematic chain, from which five chains
     * extend: the two legs, two arms and the head.
     *
     * All body parts know their transform, which contains the translation and
     * rotation of the part's coordinate frame, in the frame of the torso.
     *
     * For more detail on the naming and use of transforms, see EigenTests.hh.
     */
    class BodyState : public StateObject<BodyState> {
    public:
      static std::shared_ptr<BodyState const> zero(BodyModel const &bodyModel, ulong thinkCycleNumber = 0);

      static Eigen::Vector3d distanceBetween(BodyPartPosition const &p1, BodyPartPosition const &p2) {
        return p2.getPosition() - p1.getPosition();
      }

      /// Initialise with the specified angles, in radians. Indexed by JointId (i.e. 0 is ignored.)
      BodyState(
          BodyModel const &bodyModel,
          std::array<double, 23> const &angles,
          std::array<short, 21> const &positionValueDiffs,
          ulong cycleNumber);

      BodyState(
          BodyModel const &bodyModel,
          HardwareState const &hardwareState,
          motion::core::BodyControl const &bodyControl,
          ulong motionCycleNumber);

      std::shared_ptr<LimbPosition const> getLimb(std::string const &name) const;

      std::shared_ptr<JointPosition const> getJoint(JointId jointId) const;

      void visitJoints(std::function<void(std::shared_ptr<JointPosition const> const &)> visitor) const;

      void visitLimbs(std::function<void(std::shared_ptr<LimbPosition const> const &)> visitor) const;

      /** Transformation describing camera frame in agent frame
       *
       * To be used used to transform camera coordinates to agent
       * coordinates
       */
      Eigen::Affine3d const &getAgentCameraTransform() const { return d_agentCameraTr; }

      /** Transformation describing agent frame in camera frame
       *
       * To be used to transform agent coordinates to camera
       * coordinates
       */
      Eigen::Affine3d const &getCameraAgentTransform() const { return d_cameraAgentTr; }

      geometry2::Point3d const &getCentreOfMass() const;

      /** Height of torso center above the ground
       *
       * Assumes the lowest foot is flat on the floor */
      double getTorsoHeight() const { return d_torsoHeight; }

      std::array<short, 21> const &getPositionValueDiffById() const { return d_positionValueDiffById; }

      Eigen::Affine3d determineFootAgentTr(bool leftFoot) const;

    private:
      friend class StateObject<BodyState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      /// Initialise with the specified angles (radians), and position errors (values)
      /// Indexed by JointId (i.e. 0 is ignored.), including camera tilt angle
      void initialise(BodyModel const &bodyModel, std::array<double, 23> const &angles);

      std::array<short, 21> d_positionValueDiffById;

      double d_torsoHeight;

      std::shared_ptr<LimbPosition const> d_torso;

      std::array<std::shared_ptr<JointPosition const>, 23> d_jointById;
      std::map<std::string, std::shared_ptr<LimbPosition const>> d_limbByName;

      Eigen::Affine3d d_cameraAgentTr;
      Eigen::Affine3d d_agentCameraTr;

      mutable geometry2::Point3d d_centreOfMass;
      mutable bool d_isCentreOfMassComputed;

      ulong d_motionCycleNumber;

    public:
      // Needed when having fixed sized Eigen member
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    template<typename TBuffer>
    inline void BodyState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("motion-cycle");
        writer.Uint64(d_motionCycleNumber);

        writer.String("angles");
        writer.StartArray();
        {
          for (uint8_t j = (uint8_t) JointId::MIN; j <= (uint8_t) JointId::MAX; j++) {
            auto const &it = d_jointById[j];
            writer.Double(it->getAngleRads());
          }
        }
        writer.EndArray();

        writer.String("errors");
        writer.StartArray();
        {
          for (uint8_t j = (uint8_t) JointId::MIN; j <= (uint8_t) JointId::MAX; j++)
            writer.Int(d_positionValueDiffById[j]);
        }
        writer.EndArray();

        auto com = getCentreOfMass();
        writer.String("com");
        writer.StartArray();
        writer.Double(com.x());
        writer.Double(com.y());
        writer.Double(com.z());
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
