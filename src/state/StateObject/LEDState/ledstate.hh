// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "src/colour/colour.hh"

namespace bold
{
  namespace state {
    class LEDState : public StateObject<LEDState> {
    public:
      LEDState(colour::BGR eyeColour, colour::BGR foreheadColour, bool redLed, bool greenLed, bool blueLed)
          : d_eyeColour(eyeColour),
            d_foreheadColour(foreheadColour),
            d_redLed(redLed),
            d_greenLed(greenLed),
            d_blueLed(blueLed) {}

    private:
      friend class StateObject<LEDState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      colour::BGR d_eyeColour;
      colour::BGR d_foreheadColour;
      bool d_redLed;
      bool d_greenLed;
      bool d_blueLed;
    };

    template<typename TBuffer>
    inline void LEDState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("eyeColour");
        writer.StartArray();
        {
          writer.Int(d_eyeColour.r);
          writer.Int(d_eyeColour.g);
          writer.Int(d_eyeColour.b);
        }
        writer.EndArray();
        writer.String("foreheadColour");
        writer.StartArray();
        {
          writer.Int(d_foreheadColour.r);
          writer.Int(d_foreheadColour.g);
          writer.Int(d_foreheadColour.b);
        }
        writer.EndArray();
        writer.String("led");
        writer.StartArray();
        {
          writer.Bool(d_redLed);
          writer.Bool(d_greenLed);
          writer.Bool(d_blueLed);
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
