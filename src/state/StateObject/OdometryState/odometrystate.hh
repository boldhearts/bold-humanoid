// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

#include <Eigen/Geometry>

namespace bold
{
  namespace state {
    class OdometryState : public StateObject<OdometryState> {
    public:
      OdometryState(Eigen::Affine3d transform)
          : d_transform{std::move(transform)} {}

      /** Gets the cumulative transform of the agent frame
       *
       * The returned transformation describes the agent frame at t = 0
       * (A0) wrt the current frame (At). i.e. AtA0, transforming a
       * vector v in A0 by AtA0 * v0 gives that vector in the current
       * agent frame.
       *
       * Users of this value can compute their own delta values through
       * $\delta = A_tA_{t-1} = A_tA_0 * A_{t-1}A0^{-1}$
       */
      Eigen::Affine3d const &getTransform() const { return d_transform; };

    private:
      friend class StateObject<OdometryState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      Eigen::Affine3d d_transform;
    };

    template<typename TBuffer>
    inline void OdometryState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("tr");
        writer.StartArray();
        for (unsigned j = 0; j < 4; ++j) {
          for (unsigned i = 0; i < 4; ++i)
            writer.Double(d_transform.matrix()(i, j));
          writer.String(" ");
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
