// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

namespace bold {
  namespace state {
    class MessageCountState : public StateObject<MessageCountState> {
    public:
      MessageCountState(unsigned gameControllerMessageCount,
                        unsigned ignoredMessageCount,
                        unsigned sentTeamMessageCount,
                        unsigned receivedTeamMessageCount,
                        unsigned sentDrawbridgeMessageCount)
          : d_gameControllerMessageCount(gameControllerMessageCount),
            d_ignoredMessageCount(ignoredMessageCount),
            d_sentTeamMessageCount(sentTeamMessageCount),
            d_receivedTeamMessageCount(receivedTeamMessageCount),
            d_sentDrawbridgeMessageCount(sentDrawbridgeMessageCount) {}


      unsigned int getGameControllerMessageCount() const { return d_gameControllerMessageCount; }

      unsigned int getIgnoredMessageCount() const { return d_ignoredMessageCount; }

      unsigned int getSentTeamMessageCount() const { return d_sentTeamMessageCount; }

      unsigned int getReceivedTeamMessageCount() const { return d_receivedTeamMessageCount; }

      unsigned int getSentDrawbridgeMessageCount() const { return d_sentDrawbridgeMessageCount; }

    private:
      friend class StateObject<MessageCountState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      unsigned d_gameControllerMessageCount;
      unsigned d_ignoredMessageCount;
      unsigned d_sentTeamMessageCount;
      unsigned d_receivedTeamMessageCount;
      unsigned d_sentDrawbridgeMessageCount;
    };

    template<typename TBuffer>
    inline void MessageCountState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("gameControllerMessages");
        writer.Int(d_gameControllerMessageCount);
        writer.String("ignoredMessages");
        writer.Int(d_ignoredMessageCount);
        writer.String("sentTeamMessages");
        writer.Int(d_sentTeamMessageCount);
        writer.String("receivedTeamMessages");
        writer.Int(d_receivedTeamMessageCount);
        writer.String("sentDrawbridgeMessages");
        writer.Int(d_sentDrawbridgeMessageCount);
      }
      writer.EndObject();
    }
  }
}
