// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

#include "Clock/clock.hh"
#include "util/assert.hh"
#include "util/Log/log.hh"
#include "config/Config/config.hh"

#include <cstdint>
#include <stdexcept>
#include <vector>
#include <iosfwd>

namespace bold {
  class GameStateDecoderVersion7;
  class GameStateDecoderVersion8;
  class GameStateDecoderVersion12;

  namespace state {
    enum class PlayMode : uint8_t {
      INITIAL = 0,
      READY = 1,
      SET = 2,
      PLAYING = 3,
      FINISHED = 4
    };

    enum class PeriodType : uint8_t {
      NORMAL = 0,
      PENALTY_SHOOTOUT = 1,
      OVERTIME = 2,
      TIMEOUT = 3,
      DIRECT_FREEKICK = 4,
      INDIRECT_FREEKICK = 5
    };

    enum class PenaltyType : uint8_t {
      NONE = 0,
      ILLEGAL_ATTACK = 3,
      ILLEGAL_DEFENSE = 4,
      SUBSTITUTE = 14,
      MANUAL = 15,
      BALL_MANIPULATION = 30,
      PHYSICAL_CONTACT = 31,
      PICKUP_OR_INCAPABLE = 34,
      SERVICE = 35
    };

    enum class League : uint8_t {
      SPL = 0x01,
      SPLDropIn = 0x02,
      HumanoidKidSize = 0x11,
      HumanoidTeenSize = 0x12,
      HumanoidAdultSize = 0x13
    };

    enum class TeamColor : uint8_t {
      Blue = 0,
      Red = 1
    };

    enum class Team : uint8_t {
      Us,
      Them
    };

    enum class GameResult : uint8_t {
      Undecided = 0,
      Victory = 1,
      Loss = 2,
      Draw = 3
    };

    enum class GameControllerRobotStatus : uint8_t {
      MANUALLY_PENALISED = 0,
      MANUALLY_UNPENALISED = 1,
      ALIVE = 2
    };

    enum class RobotStatusMessageType : uint8_t {
      MANUALLY_PENALISED = 0,
      MANUALLY_UNPENALISED = 1,
      ALIVE = 2
    };

    std::string getPlayModeName(PlayMode playMode);

    std::string getPeriodTypeName(PeriodType periodType);

    std::string getPenaltyTypeName(PenaltyType penaltyType);

    std::string getLeagueName(League league);

    std::string getTeamColorName(TeamColor teamColor);

    std::string getGameResultName(GameResult gameResult);

    std::string getRobotStatusMessageTypeName(RobotStatusMessageType status);

    class PlayerData {
    public:
      PlayerData(PenaltyType penaltyType, uint8_t secondsUntilPenaltyLifted)
          : d_penaltyType(penaltyType),
            d_secondsUntilPenaltyLifted(secondsUntilPenaltyLifted) {}

      PlayerData() = default;

      bool hasPenalty() const { return d_penaltyType != PenaltyType::NONE; }

      PenaltyType getPenaltyType() const { return d_penaltyType; }

      /** An estimate of time till unpenalised */
      uint8_t getSecondsUntilPenaltyLifted() const { return d_secondsUntilPenaltyLifted; }

    private:
      PenaltyType d_penaltyType;
      uint8_t d_secondsUntilPenaltyLifted;
    };

    class TeamData {
    public:
      TeamData() = default;

      uint8_t getTeamNumber() const { return d_teamNumber; }

      TeamColor getTeamColor() const { return d_teamColour; }

      bool isBlueTeam() const { return d_teamColour == TeamColor::Blue; }

      uint8_t getScore() const { return d_score; }

      uint8_t getPenaltyShotCount() const { return d_penaltyShot; }

      bool wasPenaltySuccessful(uint8_t number) const {
        ASSERT(number < d_penaltyShot);
        return ((1 << number) & d_singleShots) != 0;
      }

      PlayerData const &getPlayer(uint8_t unum) const {
        ASSERT(unum > 0 && unum <= d_players.size());
        return d_players[unum - 1];
      }

      static constexpr uint8_t PLAYER_COUNT = 11;

    private:
      friend class bold::GameStateDecoderVersion7;

      friend class bold::GameStateDecoderVersion8;

      friend class bold::GameStateDecoderVersion12;

      uint8_t d_teamNumber;   // Unique team number
      TeamColor d_teamColour; // Colour of the team
      uint8_t d_score;        // Team's score
      uint8_t d_penaltyShot;  // Penalty shot counter
      uint16_t d_singleShots; // Bits represent penalty shot success
      std::vector<PlayerData> d_players; // The team's players
    };

    class GameState : public StateObject<GameState> {
    public:
      GameState() = default;

      PlayMode getPlayMode() const { return d_playMode; }

      uint8_t getVersion() const { return d_version; }

      uint8_t getPacketNumber() const { return d_packetNumber; }

      uint8_t getPlayersPerTeam() const { return d_playersPerTeam; }

      bool isFirstHalf() const { return d_isFirstHalf; }

      /** Index of the next team to kick off. Either zero or one. */
      uint8_t getNextKickOffTeamIndex() const { return d_nextKickOffTeamIndex; }

      bool isPenaltyShootout() const { return d_periodType == PeriodType::PENALTY_SHOOTOUT; }

      bool isOvertime() const { return d_periodType == PeriodType::OVERTIME; }

      bool isTimeout() const { return d_periodType == PeriodType::TIMEOUT; }

      bool isClockRunning() const { return d_playMode == PlayMode::PLAYING; }

      /** Identifies the team having the last drop in (0 blue, 1 red, 2 no drop in yet). */
      uint8_t getLastDropInTeamColorNumber() const { return d_lastDropInTeamColorNumber; }

      int16_t getSecondsSinceLastDropIn(Clock const &clock) const {
        return d_secondsSinceLastDropIn + (isClockRunning() ? clock.getSecondsSince(d_receivedAt) : 0);
      }

      int16_t getSecondsRemaining(Clock const &clock) const {
        return d_secondsRemaining - (isClockRunning() ? clock.getSecondsSince(d_receivedAt) : 0);
      }

      int16_t getSecondaryTime(Clock const &clock) const {
        int16_t secondaryTime = d_secondaryTime;
        if (isClockRunning())
          secondaryTime -= clock.getSecondsSince(d_receivedAt);
        return secondaryTime > 0 ? secondaryTime : (int16_t) 0;
      }

      bool isWithinTenSecondsOfKickOff(Team team, Clock const &clock) const;

      TeamData const &getTeam1() const { return d_team1; }

      TeamData const &getTeam2() const { return d_team2; }

      TeamData const &getMyTeam() const {
        static auto teamNumber = (uint8_t) config::Config::getStaticValue<int>("team-number");
        return getTeam(teamNumber);
      }

      TeamData const &getOpponentTeam() const {
        static auto teamNumber = (uint8_t) config::Config::getStaticValue<int>("team-number");
        return getTeamIndex(teamNumber) == 0 ? d_team2 : d_team1;
      }

      PlayerData const &getMyPlayerInfo() const {
        static int unum = config::Config::getStaticValue<int>("uniform-number");
        return getMyTeam().getPlayer(unum);
      }

      double getAgeMillis(Clock const &clock) const { return clock.getMillisSince(d_receivedAt); }

      GameResult getGameResult() const;

    private:
      friend class bold::GameStateDecoderVersion7;

      friend class bold::GameStateDecoderVersion8;

      friend class bold::GameStateDecoderVersion12;

      friend class StateObject<GameState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      TeamData const &getTeam(uint8_t teamNumber) const {
        return getTeamIndex(teamNumber) == 0 ? d_team1 : d_team2;
      }

      uint8_t getTeamIndex(uint8_t teamNumber) const {
        if (d_team1.getTeamNumber() == teamNumber)
          return (uint8_t) 0;
        if (d_team2.getTeamNumber() == teamNumber)
          return (uint8_t) 1;

        // We should never reach this point as the GameStateReceiver should not propagate meesages which do not
        // apply to our team.
        util::Log::error("GameState::getTeamIndex") << "Attempt to get index for unknown team number " << teamNumber;
        throw std::runtime_error("Attempt to get index for unknown team number.");
      }

      uint8_t d_packetNumber;              // Sequence number of the packet (overflows from 255 to 0)
      uint8_t d_playersPerTeam;            // The number of players on a team
      uint8_t d_version;                   // Version of the GameState protocol used
      PlayMode d_playMode;               // The game's play mode (initial, ready, set, play, finished)
      bool d_isFirstHalf;                // Whether the first half (1) or second half (0), for both normal and extra game periods
      uint8_t d_nextKickOffTeamIndex;      // Index of the next team to kick off (0 or 1)
      PeriodType d_periodType;           // The type of game period (normal, extra, penalties, timeout)
      uint8_t d_lastDropInTeamColorNumber; // Color of the team that caused the last drop in (or 2 if no drop in yet)
      int16_t d_secondsSinceLastDropIn;    // Number of seconds passed since the last drop in (or -1 if no drop in yet)
      int16_t d_secondsRemaining;          // Estimate of number of seconds remaining in the half
      int16_t d_secondaryTime;             // Sub-time (remaining in ready state, etc.) in seconds

      TeamData d_team1;
      TeamData d_team2;

      Clock::Timestamp d_receivedAt;
    };

    template<typename TBuffer>
    inline void GameState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      auto clock = SystemClock{};

      writer.StartObject();
      {
        writer.String("playMode");
        writer.String(getPlayModeName(getPlayMode()).c_str());
        writer.String("packet");
        writer.Uint(getPacketNumber());
        writer.String("playerPerTeam");
        writer.Uint(getPlayersPerTeam());
        writer.String("isFirstHalf");
        writer.Bool(isFirstHalf());
        writer.String("nextKickOffTeamIndex");
        writer.Uint(getNextKickOffTeamIndex());
        writer.String("isPenaltyShootOut");
        writer.Bool(isPenaltyShootout());
        writer.String("isOvertime");
        writer.Bool(isOvertime());
        writer.String("isTimeout");
        writer.Bool(isTimeout());
        writer.String("lastDropInTeamColor");
        writer.Uint(getLastDropInTeamColorNumber());
        writer.String("secSinceDropIn");
        writer.Int(getSecondsSinceLastDropIn(clock));
        writer.String("secondsRemaining");
        writer.Int(getSecondsRemaining(clock));
        writer.String("secondsSecondaryTime");
        writer.Int(getSecondaryTime(clock));

        auto writeTeam = [&writer, this](TeamData const &team) {
          writer.StartObject();
          {
            writer.String("num");
            writer.Uint(team.getTeamNumber());
            writer.String("isBlue");
            writer.Bool(team.isBlueTeam());
            writer.String("score");
            writer.Uint(team.getScore());
            writer.String("penaltyShotCount");
            writer.Uint(team.getPenaltyShotCount());

            writer.String("players");
            writer.StartArray();
            {
              for (uint8_t p = 1; p <= getPlayersPerTeam(); ++p) {
                auto const &player = team.getPlayer(p);
                writer.StartObject();
                {
                  writer.String("penalty");
                  if (player.getPenaltyType() == PenaltyType::NONE) {
                    writer.Null();
                  } else {
                    try {
                      writer.String(getPenaltyTypeName(player.getPenaltyType()).c_str());
                    } catch (std::runtime_error const &e) {
                      util::Log::error("GameState::writeJsonInternal") << "Error getting penalty type name: "
                                                                       << e.what();
                      writer.String("<INVALID>");
                    }

                    writer.String("penaltySecondsRemaining");
                    writer.Uint(player.getSecondsUntilPenaltyLifted());
                  }
                }
                writer.EndObject();
              }
            }
            writer.EndArray();
          }
          writer.EndObject();
        };

        writer.String("myTeam");
        writeTeam(getMyTeam());

        writer.String("opponentTeam");
        writeTeam(getOpponentTeam());
      }
      writer.EndObject();
    }
  }
}
