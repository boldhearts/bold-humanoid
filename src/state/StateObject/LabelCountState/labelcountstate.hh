// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>
#include <map>
#include <memory>

#include "state/StateObject/stateobject.hh"
#include "vision/PixelLabel/pixellabel.hh"

namespace bold {
  namespace vision {
    enum class LabelClass : uint8_t;
  }

  namespace state {
    class LabelCountState : public StateObject<LabelCountState> {
    public:
      LabelCountState(std::map<std::shared_ptr<vision::PixelLabel>, uint> labelCounts)
          : d_labelCounts(labelCounts) {}

    private:
      friend class StateObject<LabelCountState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      std::map<std::shared_ptr<vision::PixelLabel>, uint> d_labelCounts;
    };

    template<typename TBuffer>
    inline void LabelCountState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("labels");
        writer.StartArray();
        {
          for (auto const &pair : d_labelCounts) {
            auto label = pair.first;
            unsigned const &count = pair.second;
            writer.StartObject();
            {
              writer.String("name");
              writer.String(label->getName().c_str());
              writer.String("id");
              writer.Uint(uint8_t(label->getClass()));
              writer.String("count");
              writer.Uint(count);
            }
            writer.EndObject();
          }
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
