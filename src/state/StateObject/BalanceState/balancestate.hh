// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "Balance/balance.hh"

namespace bold {
  namespace state {

    class BalanceState : public StateObject<BalanceState> {
    public:
      BalanceState(BalanceOffset const &offsets)
          : d_offsets(offsets) {}

      BalanceOffset const &offsets() const { return d_offsets; }

    private:
      friend class StateObject<BalanceState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      BalanceOffset d_offsets;
    };

    template<typename TBuffer>
    inline void BalanceState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("offsets");
        writer.StartObject();
        {
          writer.String("hipPitch");
          writer.Int(d_offsets.hipPitch);
          writer.String("hipRoll");
          writer.Int(d_offsets.hipRoll);
          writer.String("knee");
          writer.Int(d_offsets.knee);
          writer.String("anklePitch");
          writer.Int(d_offsets.anklePitch);
          writer.String("ankleRoll");
          writer.Int(d_offsets.ankleRoll);
        }
        writer.EndObject();
      }
      writer.EndObject();
    }
  }
}
