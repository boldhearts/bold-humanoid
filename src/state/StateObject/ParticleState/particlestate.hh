// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

#include <memory>
#include <vector>
#include <Eigen/Core>

namespace bold
{
  namespace state {
    /** Holds a set of ParticleFilter particles at a given point in time.
     */
    class ParticleState : public StateObject<ParticleState> {
    public:
      ParticleState(Eigen::MatrixXd const &particles, double preNormWeightSum, double smoothedPreNormWeightSum,
                    double uncertainty)
          : d_particles{particles},
            d_preNormWeightSum{preNormWeightSum},
            d_smoothedPreNormWeightSum{smoothedPreNormWeightSum},
            d_uncertainty{uncertainty} {}

      Eigen::MatrixXd const &getParticles() const { return d_particles; }

      double getPreNormWeightSum() const { return d_preNormWeightSum; }

      double getSmoothedPreNormWeightSum() const { return d_smoothedPreNormWeightSum; }

      double getUncertainty() const { return d_uncertainty; }

    private:
      friend class StateObject<ParticleState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      Eigen::MatrixXd d_particles;
      double d_preNormWeightSum;
      double d_smoothedPreNormWeightSum;
      double d_uncertainty;
    };

    template<typename TBuffer>
    void ParticleState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("particles");
        writer.StartArray();
        {
          for (int i = 0; i < d_particles.cols(); ++i) {
            writer.StartArray();

            auto particle = d_particles.col(i);
            writer.Double(particle.x()); // x
            writer.Double(particle.y()); // y
            writer.Double(atan2(particle(3), particle(2))); // theta
            JsonWriter::swapNaN(writer, particle(4)); // weight

            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("pnwsum");
        writer.Double(d_preNormWeightSum);
        writer.String("pnwsumsmooth");
        writer.Double(d_smoothedPreNormWeightSum);
        writer.String("uncertainty");
        writer.Double(d_uncertainty);
      }
      writer.EndObject();
    }
  }
}
