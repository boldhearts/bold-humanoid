// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "teamstate.hh"

#include "config/Config/config.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

bool PlayerState::isMe() const
{
  static uint8_t myUniformNumber = (uint8_t)Config::getStaticValue<int>("uniform-number");
  static uint8_t myTeamNumber = (uint8_t)Config::getStaticValue<int>("team-number");

  return uniformNumber == myUniformNumber && teamNumber == myTeamNumber;
}

double PlayerState::getAgeMillis(Clock const& clock) const
{
  return clock.getMillisSince(updateTime);
}

std::ostream& bold::state::operator<<(std::ostream &stream, PlayerRole const& role)
{
  return stream << getPlayerRoleString(role);
}

std::string bold::state::getPlayerRoleString(PlayerRole role)
{
  switch (role)
  {
    case PlayerRole::Idle: return "Idle";
    case PlayerRole::Keeper: return "Keeper";
    case PlayerRole::Supporter: return "Supporter";
    case PlayerRole::Striker: return "Striker";
    case PlayerRole::Defender: return "Defender";
    case PlayerRole::PenaltyKeeper: return "PenaltyKeeper";
    case PlayerRole::PenaltyStriker: return "PenaltyStriker";
    case PlayerRole::Other: return "Other";
    case PlayerRole::KickLearner: return "KickLearner";
    case PlayerRole::BallCircler: return "BallCircler";
    case PlayerRole::WhistleListener: return "WhistleListener";
    case PlayerRole::DemoStriker: return "DemoStriker";
    default: return "Unknown";
  }
}

std::string bold::state::getPlayerActivityString(PlayerActivity activity)
{
  switch (activity)
  {
    case PlayerActivity::Positioning: return "Positioning";
    case PlayerActivity::ApproachingBall: return "ApproachingBall";
    case PlayerActivity::AttackingGoal: return "AttackingGoal";
    case PlayerActivity::Waiting: return "Waiting";
    case PlayerActivity::Other: return "Other";
    default: return "Unknown";
  }
}

std::string bold::state::getPlayerStatusString(PlayerStatus status)
{
  switch (status)
  {
    case PlayerStatus::Inactive: return "Inactive";
    case PlayerStatus::Active: return "Active";
    case PlayerStatus::Penalised: return "Penalised";
    case PlayerStatus::Paused: return "Paused";
    default: return "Unknown";
  }
}

vector<PlayerState> TeamState::getBallObservers(function<bool(PlayerState const&)> filter) const
{
  vector<PlayerState> observers = {};

  for (PlayerState const& player : d_playerStates)
  {
    if (player.isMe())
      continue;

    if (!filter(player))
      continue;

    if (!player.ballRelative.hasValue())
      continue;

    if (player.status == PlayerStatus::Penalised)
      continue;

    if (player.role == PlayerRole::Keeper)
      continue;

    observers.push_back(player);
  }

  return observers;
}

bool TeamState::isTeamMateInActivity(PlayerActivity activity, function<bool(PlayerState const&)> filter) const
{
  for (PlayerState const& player : d_playerStates)
  {
    if (player.isMe())
      continue;

    if (!filter(player))
      continue;

    if (player.status == PlayerStatus::Penalised)
      continue;

    if (player.activity == activity)
      return true;
  }

  return false;
}

FieldSide TeamState::getKeeperBallSideEstimate(Clock const& clock) const
{
  auto keeper = getKeeperState();

  // If we see a goal, and have ball observation data from the keeper which is recent enough
  if (keeper == nullptr || keeper->getAgeMillis(clock) > 10000 || !keeper->ballRelative.hasValue())
    return FieldSide::Unknown;

  // ASSUME the keeper is roughly in the middle of the penalty area

  double keeperDistFromGoalLine = FieldMap::getGoalAreaLengthX() / 2.0;

  const double maxPositionMeasurementError = 0.4; // TODO review this experimentally

  double keeperBallDist = keeper->ballRelative->norm();

  // Distance from the keeper within which the ball is unambiguously on our side of the field
  double oursThreshold = (FieldMap::getFieldLengthX() / 2.0) - keeperDistFromGoalLine - maxPositionMeasurementError;

  if (keeperBallDist < oursThreshold)
    return FieldSide::Ours;

  // Distance from the keeper above which the ball is unambiguously on the opponent's side of the field
  double theirsThreshold = Vector2d(
    (FieldMap::getFieldLengthX() / 2.0) - keeperDistFromGoalLine + maxPositionMeasurementError,
    FieldMap::getFieldLengthY() / 2.0
  ).norm();

  if (keeperBallDist > theirsThreshold)
    return FieldSide::Theirs;

  return FieldSide::Unknown;
}
