// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "Drawing/drawing.hh"
#include "JsonWriter/jsonwriter.hh"

#include <memory>
#include <vector>

namespace bold
{
  namespace state {
    class DrawingState : public StateObject<DrawingState> {
    public:
      DrawingState(Draw::ItemVector items)
          : d_drawingItems(std::move(items)) {}

    private:
      friend class StateObject<DrawingState>;

      template<typename TBuffer>
      inline void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      Draw::ItemVector d_drawingItems;
    };

    template<typename TBuffer>
    inline void DrawingState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      writer.String("items");
      writer.StartArray();

      for (auto const &item : d_drawingItems) {
        writer.StartObject();
        writer.String("type");
        writer.Uint((uint) item->type);
        writer.String("frame");
        writer.Uint((uint) item->frame);

        ASSERT((int) item->type != 0);

        // TODO refactor out common code, maybe with another base class

        if (item->type == DrawingItemType::Line) {
          auto lineDrawing = static_cast<LineDrawing const &>(*item);
          writer.String("p1");
          writer.StartArray();
          writer.Double(lineDrawing.line.p1().x());
          writer.Double(lineDrawing.line.p1().y());
          writer.EndArray();
          writer.String("p2");
          writer.StartArray();
          writer.Double(lineDrawing.line.p2().x());
          writer.Double(lineDrawing.line.p2().y());
          writer.EndArray();

          if (lineDrawing.alpha > 0 && lineDrawing.alpha < 1) {
            writer.String("a");
            writer.Double(lineDrawing.alpha);
          }

          if (lineDrawing.lineWidth > 0 && lineDrawing.lineWidth != 1) {
            writer.String("w");
            writer.Double(lineDrawing.lineWidth);
          }

          if (lineDrawing.colour.b != 0 || lineDrawing.colour.g != 0 || lineDrawing.colour.r != 0) {
            writer.String("rgb");
            JsonWriter::rgb(writer, lineDrawing.colour);
          }
        } else if (item->type == DrawingItemType::Circle) {
          auto circle = static_cast<CircleDrawing const &>(*item);
          writer.String("c");
          writer.StartArray();
          writer.Double(circle.centre.x());
          writer.Double(circle.centre.y());
          writer.EndArray();
          writer.String("r");
          writer.Double(circle.radius);

          if (circle.fillAlpha > 0 && circle.fillAlpha < 1) {
            writer.String("fa");
            writer.Double(circle.fillAlpha);
          }
          if (circle.strokeAlpha > 0 && circle.strokeAlpha < 1) {
            writer.String("sa");
            writer.Double(circle.strokeAlpha);
          }
          if (circle.fillColour.b != 0 || circle.fillColour.g != 0 || circle.fillColour.r != 0) {
            writer.String("frgb");
            JsonWriter::rgb(writer, circle.fillColour);
          }
          if (circle.strokeColour.b != 0 || circle.strokeColour.g != 0 || circle.strokeColour.r != 0) {
            writer.String("srgb");
            JsonWriter::rgb(writer, circle.strokeColour);
          }
          if (circle.lineWidth > 0 && circle.lineWidth != 1) {
            writer.String("w");
            writer.Double(circle.lineWidth);
          }
        } else if (item->type == DrawingItemType::Polygon) {
          auto poly = static_cast<PolygonDrawing const &>(*item);
          writer.String("p");
          JsonWriter::polygon(writer, poly.polygon);

          if (poly.fillAlpha > 0 && poly.fillAlpha < 1) {
            writer.String("fa");
            writer.Double(poly.fillAlpha);
          }
          if (poly.strokeAlpha > 0 && poly.strokeAlpha < 1) {
            writer.String("sa");
            writer.Double(poly.strokeAlpha);
          }
          if (poly.fillColour.b != 0 || poly.fillColour.g != 0 || poly.fillColour.r != 0) {
            writer.String("frgb");
            JsonWriter::rgb(writer, poly.fillColour);
          }
          if (poly.strokeColour.b != 0 || poly.strokeColour.g != 0 || poly.strokeColour.r != 0) {
            writer.String("srgb");
            JsonWriter::rgb(writer, poly.strokeColour);
          }
          if (poly.lineWidth > 0 && poly.lineWidth != 1) {
            writer.String("w");
            writer.Double(poly.lineWidth);
          }
        }

        writer.EndObject();
      }

      writer.EndArray();
      writer.EndObject();
    }
  }
}
