// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <vector>

#include "JointId/jointid.hh"
#include "MX28Alarm/mx28alarm.hh"

#include "state/StateObject/stateobject.hh"
#include "CM730Snapshot/cm730snapshot.hh"
#include "MX28Snapshot/mx28snapshot.hh"
#include "util/assert.hh"

namespace bold {
  namespace state {

    class StaticHardwareState : public StateObject<StaticHardwareState> {
    public:
      StaticHardwareState(std::shared_ptr<StaticCM730State const> cm730State,
                          std::vector<std::shared_ptr<StaticMX28State const>> mx28States)
          : d_cm730State(cm730State),
            d_mx28States(mx28States) {
        ASSERT(d_mx28States.size() == (uint8_t) JointId::MAX);
      }

      std::shared_ptr<StaticCM730State const> getCM730State() const {
        return d_cm730State;
      }

      std::shared_ptr<StaticMX28State const> getMX28State(uint8_t jointId) const {
        ASSERT(jointId >= (uint8_t) JointId::MIN && jointId <= (uint8_t) JointId::MAX);

        return d_mx28States[jointId - 1];
      }

    private:
      friend class StateObject<StaticHardwareState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      std::shared_ptr<StaticCM730State const> d_cm730State;
      std::vector<std::shared_ptr<StaticMX28State const>> d_mx28States;
    };

    template<typename TBuffer>
    inline void StaticHardwareState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      auto writeAlarm = [&writer](std::string name, MX28Alarm const &alarm) {
        writer.String(name.c_str());
        writer.StartArray();
        {
          for (auto const &setName : alarm.getSetNames())
            writer.String(setName.c_str());
        }
        writer.EndArray();
      };

      writer.StartObject();
      {
        writer.String("id");
        writer.Int(d_cm730State->dynamixelId);
        writer.String("baud");
        writer.Int(d_cm730State->baudBPS);
        writer.String("firmwareVersion");
        writer.Int(d_cm730State->firmwareVersion);
        writer.String("modelNumber");
        writer.Int(d_cm730State->modelNumber);
        writer.String("returnDelayTimeMicroSeconds");
        writer.Int(d_cm730State->returnDelayTimeMicroSeconds);
        writer.String("statusRetLevel");
        writer.Int(d_cm730State->statusRetLevel);

        writer.String("joints");
        writer.StartArray();
        for (std::shared_ptr<StaticMX28State const> mx28 : d_mx28States) {
          writer.StartObject();
          {
            writer.String("id");
            writer.Int(mx28->id);
            writer.String("modelNumber");
            writer.Int(mx28->modelNumber);
            writer.String("firmwareVersion");
            writer.Int(mx28->firmwareVersion);
            writer.String("baud");
            writer.Int(mx28->baudBPS);
            writer.String("returnDelayTimeMicroSeconds");
            writer.Int(mx28->returnDelayTimeMicroSeconds);
            writer.String("angleLimitCW");
            writer.Double(mx28->angleLimitCW);
            writer.String("angleLimitCCW");
            writer.Double(mx28->angleLimitCCW);
            writer.String("tempLimitHighCelsius");
            writer.Int(mx28->tempLimitHighCelsius);
            writer.String("voltageLimitLow");
            writer.Double(mx28->voltageLimitLow);
            writer.String("voltageLimitHigh");
            writer.Double(mx28->voltageLimitHigh);
            writer.String("maxTorque");
            writer.Int(mx28->maxTorque);
            writer.String("statusRetLevel");
            writer.Int(mx28->statusRetLevel);
            writeAlarm("alarmLed", mx28->alarmLed);
            writeAlarm("alarmShutdown", mx28->alarmShutdown);
            writer.String("torqueEnable");
            writer.Bool(mx28->torqueEnable);
            writer.String("isEepromLocked");
            writer.Bool(mx28->isEepromLocked);
          }
          writer.EndObject();
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
