// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

#include "state/StateObject/stateobject.hh"
#include "JointId/jointid.hh"

namespace bold {
  namespace motion {
    namespace core {
      class BodyControl;
    }
  }

  namespace state {

    class BodyControlState : public StateObject<BodyControlState> {
    public:
      struct JointControlState {
        uint16_t value;
        short modulation;
        uint8_t pGain;
        uint8_t iGain;
        uint8_t dGain;
      };

      BodyControlState(std::shared_ptr<motion::core::BodyControl> bodyControl, ulong motionCycleNumber);

      JointControlState const &getJoint(JointId jointId) const { return d_jointStates[(uint8_t) jointId - 1]; }

    private:
      friend class StateObject<BodyControlState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      ulong d_motionCycleNumber;
      JointControlState d_jointStates[(uint8_t) JointId::MAX];
    };

    template<typename TBuffer>
    inline void BodyControlState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("cycle");
        writer.Uint64(d_motionCycleNumber);

        writer.String("joints");
        writer.StartArray();
        {
          for (uint8_t j = (uint8_t) JointId::MIN; j <= (uint8_t) JointId::MAX; j++) {
            writer.StartObject();
            {
              writer.String("v");
              writer.Uint(d_jointStates[j - 1].value);
              writer.String("m");
              writer.Int(d_jointStates[j - 1].modulation);
              writer.String("p");
              writer.Uint(d_jointStates[j - 1].pGain);
              writer.String("i");
              writer.Uint(d_jointStates[j - 1].iGain);
              writer.String("d");
              writer.Uint(d_jointStates[j - 1].dGain);
            }
            writer.EndObject();
          }
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
