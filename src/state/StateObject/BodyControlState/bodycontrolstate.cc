// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bodycontrolstate.hh"

#include "motion/core/BodyControl/bodycontrol.hh"
#include "JointId/jointid.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::motion::core;
using namespace rapidjson;
using namespace std;

BodyControlState::BodyControlState(shared_ptr<BodyControl> bodyControl, ulong motionCycleNumber)
: d_motionCycleNumber(motionCycleNumber)
{
  for (uint8_t j = (uint8_t)JointId::MIN; j <= (uint8_t)JointId::MAX; j++)
  {
    auto const& joint = bodyControl->getJoint((JointId)j);

    d_jointStates[j - 1].value = joint.getValue();
    d_jointStates[j - 1].modulation = joint.getModulationOffset();
    d_jointStates[j - 1].pGain = joint.getPGain();
    d_jointStates[j - 1].iGain = joint.getIGain();
    d_jointStates[j - 1].dGain = joint.getDGain();
  }
}
