// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

#include "state/StateObject/stateobject.hh"
#include "state/StateObject/TeamState/teamstate.hh"

namespace bold {
  typedef unsigned long ulong;

  class BehaviourControl;

  namespace state {

    class BehaviourControlState : public StateObject<BehaviourControlState> {
    public:
      BehaviourControlState(ulong motionCycleNumber, PlayerRole playerRole, PlayerActivity playerActivity,
                            PlayerStatus playerStatus)
          : d_motionCycleNumber(motionCycleNumber),
            d_playerRole(playerRole),
            d_playerActivity(playerActivity),
            d_playerStatus(playerStatus) {}

      ulong getMotionCycleNumber() const { return d_motionCycleNumber; }

      PlayerRole getPlayerRole() const { return d_playerRole; }

      PlayerActivity getPlayerActivity() { return d_playerActivity; }

      PlayerStatus getPlayerStatus() const { return d_playerStatus; }

    private:
      friend class StateObject<BehaviourControlState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      ulong d_motionCycleNumber;
      PlayerRole d_playerRole;
      PlayerActivity d_playerActivity;
      PlayerStatus d_playerStatus;
    };

    template<typename TBuffer>
    inline void BehaviourControlState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("role");
        writer.Uint(static_cast<uint>(d_playerRole));
        writer.String("activity");
        writer.Uint(static_cast<uint>(d_playerActivity));
        writer.String("status");
        writer.Uint(static_cast<uint>(d_playerStatus));
      }
      writer.EndObject();
    }
  }
}
