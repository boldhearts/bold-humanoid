// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

#include <Eigen/Core>
#include "src/geometry2/Point/point.hh"

namespace bold
{
  namespace state {
    class StabilityState : public StateObject<StabilityState> {
    public:
      StabilityState(bool stable, Eigen::Vector3d instabilityMeasure, geometry2::Point3d referencePoint)
          : d_stable{stable},
            d_instabilityMeasure{std::move(instabilityMeasure)},
            d_referencePoint{referencePoint} {}

      bool amStable() const { return d_stable; }

      Eigen::Vector3d getInstabilityMeasure() const { return d_instabilityMeasure; }

      geometry2::Point3d getReferencePoint() const { return d_referencePoint; }

    private:
      friend class StateObject<StabilityState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      bool d_stable;
      Eigen::Vector3d d_instabilityMeasure;
      geometry2::Point3d d_referencePoint;
    };

    template<typename TBuffer>
    void StabilityState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("stable");
        writer.Bool(d_stable);

        writer.String("instability");
        writer.StartArray();
        {
          writer.Double(d_instabilityMeasure.x());
          writer.Double(d_instabilityMeasure.y());
          writer.Double(d_instabilityMeasure.z());
        }
        writer.EndArray();

        writer.String("reference");
        writer.StartArray();
        {
          writer.Double(d_referencePoint.x());
          writer.Double(d_referencePoint.y());
          writer.Double(d_referencePoint.z());
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
