// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"

#include <vector>
#include <cmath>

namespace bold {
  namespace state {

    class AudioPowerSpectrumState : public StateObject<AudioPowerSpectrumState> {
    public:
      AudioPowerSpectrumState(double maxFrequency, std::vector<float> dbs);

      unsigned getIndexForFreqHz(double frequencyHz) const;

      unsigned getMaxIndex() const { return d_dbs.size() - 1; }

      float getDecibelsByIndex(uint index) const { return d_dbs[index]; }

    private:
      friend class StateObject<AudioPowerSpectrumState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      double d_maxFrequency;
      std::vector<float> d_dbs;
    };

    template<typename TBuffer>
    inline void AudioPowerSpectrumState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("maxHertz");
        writer.Double(d_maxFrequency);
        writer.String("dbLevels");
        writer.StartArray();
        {
          for (float const &db : d_dbs) {
            if (std::isinf(db))
              writer.Int(-200);
            else
              writer.Double(db);
          }
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
