// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "audiopowerspectrumstate.hh"

using namespace bold;
using namespace bold::state;
using namespace rapidjson;
using namespace std;

AudioPowerSpectrumState::AudioPowerSpectrumState(double maxFrequency, std::vector<float> dbs)
    : d_maxFrequency(maxFrequency),
      d_dbs(dbs) {}

uint AudioPowerSpectrumState::getIndexForFreqHz(double frequencyHz) const {
  return (uint) (frequencyHz * d_dbs.size() / d_maxFrequency);
}
