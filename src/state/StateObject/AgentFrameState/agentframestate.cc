// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "agentframestate.hh"

#include "src/geometry2/Operator/Predicate/Contains/contains.hh"
#include "src/geometry2/Operator/Intersection/intersection.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace bold::geometry2;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

AgentFrameState::AgentFrameState(
  util::Maybe<Point3d> ballObservation,
  Point3d::Vector goalObservations,
  Point3d::Vector teamMateObservations,
  LineSegment3d::Vector observedLineSegments,
  vector<LineJunction, aligned_allocator<LineJunction>> observedLineJunctions,
  util::Maybe<Polygon2d> visibleFieldPoly,
  OcclusionRay<double>::Vector occlusionRays,
  ulong thinkCycleNumber)
: d_ballObservation(move(ballObservation)),
  d_goalObservations(move(goalObservations)),
  d_teamMateObservations(move(teamMateObservations)),
  d_observedLineSegments(move(observedLineSegments)),
  d_observedLineJunctions(move(observedLineJunctions)),
  d_visibleFieldPoly(move(visibleFieldPoly)),
  d_occlusionRays(move(occlusionRays)),
  d_thinkCycleNumber(thinkCycleNumber)
{}

Maybe<Point3d> AgentFrameState::getClosestGoalObservation() const
{
  if (d_goalObservations.empty())
    return util::Maybe<Point3d>::empty();

  auto closestGoalDist = numeric_limits<double>::max();
  util::Maybe<Point3d> closest;

  for (auto const& obs : d_goalObservations)
  {
    auto dist = obs.head<2>().norm();
    if (dist < closestGoalDist)
    {
      closestGoalDist = dist;
      closest = make_maybe(obs);
    }
  }

  return closest;
}

bool AgentFrameState::shouldSeeAgentFrameGroundPoint(Point2d const& groundAgent) const
{
  // TODO accept a 3d vector, and use similar triangles to transform such that z == 0
  return contains(*d_visibleFieldPoly, groundAgent);
}

double AgentFrameState::getOcclusionDistance(double angle) const
{
  // Compute the position at which where 'angle' could be inserted without changing the ordering.
  // This means that *(lower - 1) is below, and *lower is above.
  auto lower = lower_bound(
    d_occlusionRays.begin(),
    d_occlusionRays.end(),
    angle,
    [](OcclusionRay<double> const& ray, double const& a)
    {
      return Math::angleToPoint(ray.near()) < a;
    });

  if (lower == d_occlusionRays.begin() || lower == d_occlusionRays.end())
    return numeric_limits<double>::quiet_NaN();

  LineSegment2d occlusionEdge{(lower - 1)->near(), lower->near()};
  LineSegment2d ray(Point2d::Zero(), Point2d{-sin(angle), cos(angle)});

  double u{0.0};
  double t{0.0};
  auto i = Intersection()(ray, occlusionEdge, &t, &u);

  return t <= 0 ? numeric_limits<double>::quiet_NaN() : t;
}

bool AgentFrameState::isNearBall(Point2d const& point, double maxDistance) const
{
  return d_ballObservation.hasValue() && ((point - d_ballObservation->toDim<2>()).norm() < maxDistance);
}

bool AgentFrameState::isNearGoal(Point2d const& point, double maxDistance) const
{
  for (auto const& goal : d_goalObservations)
  {
    if ((point - goal.toDim<2>()).norm() < maxDistance)
      return true;
  }
  return false;
}

Maybe<Polygon2d> AgentFrameState::getOcclusionPoly() const
{
  return getOcclusionPoly(d_occlusionRays);
}

Maybe<Polygon2d> AgentFrameState::getOcclusionPoly(OcclusionRay<double>::Vector const& occlusionRays)
{
  auto nearPointsVec = Point2d::Vector();
  auto farPointsVec = Point2d::Vector{};
  for (auto const& ray : occlusionRays)
  {
    nearPointsVec.push_back(ray.near());
    farPointsVec.push_back(ray.far());
  }
  std::reverse(begin(nearPointsVec), end(nearPointsVec));

  auto pointsVec = Point2d::Vector{};
  for (auto const& p : nearPointsVec)
    pointsVec.push_back(p);
  for (auto const& p : farPointsVec)
    pointsVec.push_back(p);

  return Polygon2d{pointsVec};
}
