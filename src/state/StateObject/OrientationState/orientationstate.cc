// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "orientationstate.hh"

using namespace bold;
using namespace bold::state;
using namespace Eigen;
using namespace rapidjson;

OrientationState::OrientationState(Quaterniond const &quaternion)
    : d_quaternion(quaternion) {
  ASSERT(quaternion.w() != 0 || quaternion.x() != 0 || quaternion.y() != 0 || quaternion.z() != 0);
  ASSERT(!std::isnan(quaternion.w()));
  ASSERT(!std::isnan(quaternion.x()));
  ASSERT(!std::isnan(quaternion.y()));
  ASSERT(!std::isnan(quaternion.z()));

  Affine3d rotation(d_quaternion);
  Vector3d axisX1 = rotation.matrix().col(0).head<3>();
  d_yaw = atan2(axisX1.y(), axisX1.x());

  rotation = AngleAxisd(-d_yaw, Vector3d::UnitZ()) * rotation;

  Vector3d axisY = rotation.matrix().col(1).head<3>();
  d_pitch = atan2(axisY.z(), axisY.y());

  rotation = AngleAxisd(-d_pitch, Vector3d::UnitX()) * rotation;

  Vector3d axisX2 = rotation.matrix().col(0).head<3>();
  d_roll = atan2(axisX2.z(), axisX2.x());
}

Affine3d OrientationState::withoutYaw() const {
  return AngleAxisd(-d_yaw, Vector3d::UnitZ()) * Affine3d(d_quaternion);
}
