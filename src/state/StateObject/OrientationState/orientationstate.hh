// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "JsonWriter/jsonwriter.hh"

#include <Eigen/Geometry>

namespace bold {
  namespace state {
    class OrientationState : public StateObject<OrientationState> {
    public:
      OrientationState(Eigen::Quaterniond const &quaternion);

      Eigen::Quaterniond const &getQuaternion() const { return d_quaternion; };

      double getPitchAngle() const { return d_pitch; }

      double getRollAngle() const { return d_roll; }

      double getYawAngle() const { return d_yaw; }

      Eigen::Affine3d withoutYaw() const;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    private:
      friend class StateObject<OrientationState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      Eigen::Quaterniond d_quaternion;
      double d_pitch;
      double d_roll;
      double d_yaw;
    };

    template<typename TBuffer>
    inline void OrientationState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("quaternion");
        writer.StartArray();
        JsonWriter::swapNaN(writer, d_quaternion.x());
        JsonWriter::swapNaN(writer, d_quaternion.y());
        JsonWriter::swapNaN(writer, d_quaternion.z());
        JsonWriter::swapNaN(writer, d_quaternion.w());
        writer.EndArray();

        writer.String("pitch");
        JsonWriter::swapNaN(writer, d_pitch);
        writer.String("roll");
        JsonWriter::swapNaN(writer, d_roll);
        writer.String("yaw");
        JsonWriter::swapNaN(writer, d_yaw);
      }
      writer.EndObject();
    }
  }
}
