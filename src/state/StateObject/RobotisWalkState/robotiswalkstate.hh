// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/stateobject.hh"
#include "WalkEngine/RobotisWalkEngine/robotiswalkengine.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

namespace bold
{
  namespace state {
    class RobotisWalkState : public StateObject<RobotisWalkState> {
    public:
      RobotisWalkState(double targetX, double targetY, double targetTurn, double targetHipPitch,
                       double lastXDelta, double lastYDelta, double lastTurnDelta, double lastHipPitchDelta,
                       motion::modules::WalkModule const &walkModule,
                       RobotisWalkEngine const &walkEngine);

      // From walk module

      bool isRunning() const { return d_isRunning; }

      motion::modules::WalkStatus getStatus() const { return d_status; }

    private:
      friend class StateObject<RobotisWalkState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      // From walk module
      bool d_isRunning;
      motion::modules::WalkStatus d_status;

      // From walk engine
      double d_targetX;
      double d_targetY;
      double d_targetTurn;
      double d_targetHipPitch;
      double d_currentX;
      double d_currentY;
      double d_currentTurn;
      double d_currentHipPitch;
      double d_lastXDelta;
      double d_lastYDelta;
      double d_lastTurnDelta;
      double d_lastHipPitchDelta;
      RobotisWalkEngine::WalkPhase d_currentPhase;
      double d_bodySwingY;
      double d_bodySwingZ;
    };

    template<typename TBuffer>
    inline void RobotisWalkState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("running");
        writer.Bool(d_isRunning);
        writer.String("status");
        writer.Int((int) d_status);

        writer.String("target");
        writer.StartArray();
        {
          writer.Double(d_targetX);
          writer.Double(d_targetY);
          writer.Double(d_targetTurn);
        }
        writer.EndArray();

        writer.String("current");
        writer.StartArray();
        {
          writer.Double(d_currentX);
          writer.Double(d_currentY);
          writer.Double(d_currentTurn);
          writer.Double(d_currentHipPitch);
        }
        writer.EndArray();

        writer.String("delta");
        writer.StartArray();
        {
          writer.Double(d_lastXDelta);
          writer.Double(d_lastYDelta);
          writer.Double(d_lastTurnDelta);
          writer.Double(d_lastHipPitchDelta);
        }
        writer.EndArray();

        writer.String("phase");
        writer.Int((int) d_currentPhase);

        writer.String("hipPitch");
        writer.StartObject();
        writer.String("target");
        writer.Double(d_targetHipPitch);
        writer.String("current");
        writer.Double(d_currentHipPitch);
        writer.String("delta");
        writer.Double(d_lastHipPitchDelta);
        writer.EndObject();

        writer.String("bodySwingY");
        writer.Double(d_bodySwingY);
        writer.String("bodySwingZ");
        writer.Double(d_bodySwingZ);
      }
      writer.EndObject();
    }
  }
}
