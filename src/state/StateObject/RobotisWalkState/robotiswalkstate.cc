// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkstate.hh"

using namespace bold;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace rapidjson;
using namespace std;

RobotisWalkState::RobotisWalkState(
    double targetX, double targetY, double targetTurn, double targetHipPitch,
    double lastXDelta, double lastYDelta, double lastTurnDelta, double lastHipPitchDelta,
    WalkModule const &walkModule,
    RobotisWalkEngine const &walkEngine)
    : d_isRunning(walkModule.isRunning()),
      d_status(walkModule.getStatus()),
      d_targetX(targetX),
      d_targetY(targetY),
      d_targetTurn(targetTurn),
      d_targetHipPitch(targetHipPitch),
      d_currentX(walkEngine.getXMoveAmplitude()),
      d_currentY(walkEngine.getYMoveAmplitude()),
      d_currentTurn(walkEngine.getAMoveAmplitude()),
      d_currentHipPitch(walkEngine.getHipPitchOffset()),
      d_lastXDelta(lastXDelta),
      d_lastYDelta(lastYDelta),
      d_lastTurnDelta(lastTurnDelta),
      d_lastHipPitchDelta(lastHipPitchDelta),
      d_currentPhase(walkEngine.getCurrentPhase()),
      d_bodySwingY(walkEngine.getBodySwingY()),
      d_bodySwingZ(walkEngine.getBodySwingZ()) {}
