// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

#include <cstdio>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "roundtable/WebSocketBuffer/websocketbuffer.hh"

namespace bold {
  namespace state {

    class StateObjectBase {
    public:
      virtual void writeJson(rapidjson::Writer<rapidjson::StringBuffer> &writer) const = 0;

      virtual void writeJson(rapidjson::Writer<roundtable::WebSocketBuffer> &writer) const = 0;

      virtual void writeJson(rapidjson::PrettyWriter<rapidjson::StringBuffer> &writer) const = 0;

      template<typename TBuffer, typename TState>
      static void writeJsonOrNull(rapidjson::Writer<TBuffer> &writer,
                                  std::shared_ptr<TState const> const &stateObject) {
        static_assert(std::is_base_of<StateObjectBase, TState>::value,
                      "TState must be a descendant of StateObject");

        if (stateObject)
          stateObject->writeJson(writer);
        else
          writer.Null();
      }

    protected:
      virtual ~StateObjectBase() = default;
    };

    /** Base class for all state objects in the system.
     *
     * State objects model some kind of state, including but not limited
     * to state about the hardware, behaviour, environment, game, team
     * mates, etc.
     *
     * Instances are registered with the State class, and are immutable
     * thereafter. This allows safe use of state objects across threads.
     */
    template<class Derived>
    class StateObject : public StateObjectBase {
    public:
      void writeJson(rapidjson::Writer<rapidjson::StringBuffer> &writer) const override {
        static_cast<Derived const *>(this)->writeJsonInternal(writer);
      }

      void writeJson(rapidjson::Writer<roundtable::WebSocketBuffer> &writer) const override {
        static_cast<Derived const *>(this)->writeJsonInternal(writer);
      }

      void writeJson(rapidjson::PrettyWriter<rapidjson::StringBuffer> &writer) const override {
        static_cast<Derived const *>(this)->writeJsonInternal(writer);
      }

      template<class Writer>
      void writeJson(Writer &writer) const {
        static_cast<Derived const *>(this)->writeJsonInternal(writer);
      }
    };
  }
}
