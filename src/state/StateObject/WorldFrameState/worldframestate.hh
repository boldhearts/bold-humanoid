// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>

#include <vector>

#include "state/StateObject/stateobject.hh"
#include "AgentPosition/agentposition.hh"
#include "OcclusionRay/occlusionray.hh"
#include "src/geometry2/Geometry/LineSegment/linesegment.hh"
#include "src/geometry2/Geometry/MultiPoint/Polygon/polygon.hh"
#include "src/util/Maybe/maybe.hh"

namespace bold
{
  namespace state {
    class WorldFrameState : public StateObject<WorldFrameState> {
    public:
      WorldFrameState(util::Maybe <geometry2::Point3d> ballObservation,
                      geometry2::Point3d::Vector goalObservations,
                      geometry2::LineSegment3d::Vector observedLineSegments,
                      util::Maybe <geometry2::Polygon2d> visibleFieldPoly,
                      OcclusionRay<double>::Vector occlusionRays,
                      AgentPosition position)
          : d_ballObservation(ballObservation),
            d_goalObservations(goalObservations),
            d_observedLineSegments(observedLineSegments),
            d_visibleFieldPoly(visibleFieldPoly),
            d_occlusionRays(occlusionRays),
            d_position(position) {}

      util::Maybe <geometry2::Point3d> getBallObservation() const { return d_ballObservation; }

      geometry2::Point3d::Vector getGoalObservations() const { return d_goalObservations; }

      geometry2::LineSegment3d::Vector getObservedLineSegments() const { return d_observedLineSegments; }

      util::Maybe <geometry2::Polygon2d> getVisibleFieldPoly() const { return d_visibleFieldPoly; }

      OcclusionRay<double>::Vector getOcclusionRays() const { return d_occlusionRays; }

      AgentPosition getPosition() const { return d_position; }

      bool isBallVisible() const { return d_ballObservation.hasValue(); }

    private:
      friend class StateObject<WorldFrameState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      util::Maybe <geometry2::Point3d> d_ballObservation;
      geometry2::Point3d::Vector d_goalObservations;
      geometry2::LineSegment3d::Vector d_observedLineSegments;
      util::Maybe <geometry2::Polygon2d> d_visibleFieldPoly;
      OcclusionRay<double>::Vector d_occlusionRays;
      AgentPosition d_position;
    };

    template<typename TBuffer>
    inline void WorldFrameState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      writer.StartObject();
      {
        writer.String("pos");
        writer.StartArray();
        {
          writer.Double(d_position.x());
          writer.Double(d_position.y());
          writer.Double(d_position.theta());
        }
        writer.EndArray();

        writer.String("ball");
        if (d_ballObservation.hasValue()) {
          writer.StartArray();
          writer.Double(d_ballObservation->x());
          writer.Double(d_ballObservation->y());
          writer.Double(d_ballObservation->z());
          writer.EndArray();
        } else {
          writer.Null();
        }

        writer.String("goals");
        writer.StartArray();
        {
          for (auto const &goalPos : d_goalObservations) {
            writer.StartArray();
            writer.Double(goalPos.x());
            writer.Double(goalPos.y());
            writer.Double(goalPos.z());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("lines");
        writer.StartArray();
        {
          for (geometry2::LineSegment3d const &lineSeg : d_observedLineSegments) {
            writer.StartArray();
            writer.Double(lineSeg.p1().x());
            writer.Double(lineSeg.p1().y());
            writer.Double(lineSeg.p1().z());
            writer.Double(lineSeg.p2().x());
            writer.Double(lineSeg.p2().y());
            writer.Double(lineSeg.p2().z());
            writer.EndArray();
          }
        }
        writer.EndArray();

        writer.String("visibleFieldPoly");
        writer.StartArray();
        {
          if (d_visibleFieldPoly.hasValue()) {
            for (auto const &vertex : d_visibleFieldPoly.value()) {
              writer.StartArray();
              writer.Double(vertex.x());
              writer.Double(vertex.y());
              writer.EndArray();
            }
          }
        }
        writer.EndArray();

        writer.String("occlusionRays");
        writer.StartArray();
        {
          for (auto const &ray : d_occlusionRays) {
            // Should be enough to check a single value for NaN
            if (std::isnan(ray.far().x()))
              continue;

            writer.StartArray();
            writer.Double(ray.near().x());
            writer.Double(ray.near().y());
            writer.Double(ray.far().x());
            writer.Double(ray.far().y());
            writer.EndArray();
          }
        }
        writer.EndArray();
      }
      writer.EndObject();
    }
  }
}
