// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <memory>
#include <vector>

#include "state/StateObject/stateobject.hh"
#include "motion/core/MotionModule/motionmodule.hh"
#include "motion/core/MotionRequest/motionrequest.hh"
#include "motion/core/MotionTask/motiontask.hh"
#include "ThreadUtil/threadutil.hh"
#include "util/assert.hh"

namespace bold
{
  namespace state {
    class MotionTaskState : public StateObject<MotionTaskState> {
    public:
      MotionTaskState(
          std::shared_ptr<std::vector<std::pair<motion::core::MotionModule *, std::shared_ptr<motion::core::JointSelection>>>> moduleJointSelection,
          std::shared_ptr<motion::core::MotionTask> headTask,
          std::shared_ptr<motion::core::MotionTask> armsTask,
          std::shared_ptr<motion::core::MotionTask> legsTask,
          std::vector<std::shared_ptr<motion::core::MotionTask>> allTasks
      )
          : d_moduleJointSelection(moduleJointSelection),
            d_headTask(headTask),
            d_armsTask(armsTask),
            d_legsTask(legsTask),
            d_allTasks(allTasks) {
        ASSERT(ThreadUtil::isThinkLoopThread());
      }

      bool isEmpty() const { return d_moduleJointSelection->size() == 0; }

      /// Provides which modules to step, and what JointSelection to pass them
      std::shared_ptr<std::vector<std::pair<motion::core::MotionModule *, std::shared_ptr<motion::core::JointSelection>>>>
      getModuleJointSelection() const { return d_moduleJointSelection; }

    private:
      friend class StateObject<MotionTaskState>;

      template<typename TBuffer>
      void writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const;

      std::shared_ptr<std::vector<std::pair<motion::core::MotionModule *, std::shared_ptr<motion::core::JointSelection>>>> d_moduleJointSelection;
      std::shared_ptr<motion::core::MotionTask> d_headTask;
      std::shared_ptr<motion::core::MotionTask> d_armsTask;
      std::shared_ptr<motion::core::MotionTask> d_legsTask;
      std::vector<std::shared_ptr<motion::core::MotionTask>> d_allTasks;
    };

    template<typename TBuffer>
    inline void MotionTaskState::writeJsonInternal(rapidjson::Writer<TBuffer> &writer) const {
      /*
        {
          head: [
            {
              module: "head",
              priority: 2,
              committed: false
            },
            {
              module: "walk",
              priority: 1,
              committed: true
            }
          ],
          arms: [
            {
              module: "walk",
              priority: 2,
              committed: true
            },
            {
              module: "motion-script",
              description: "Salute",
              priority: 1,
              committed: false
            }
          ],
          legs: [
            {
              module: "walk",
              priority: 2,
              committed: true
            },
            {
              module: "motion-script",
              description: "Salute",
              priority: 1,
              committed: false
            }
          ]
        }
      */

      auto writeSection = [this, &writer](std::string name, motion::core::BodySectionId section,
                                          std::shared_ptr<motion::core::MotionTask> const &selected) {
        writer.String(name.c_str());
        writer.StartArray();
        {
          for (std::shared_ptr<motion::core::MotionTask> const &task : d_allTasks) {
            if (task->getSection() != section)
              continue;

            writer.StartObject();
            {
              writer.String("module");
              writer.String(task->getModule()->getName().c_str());
              writer.String("priority");
              writer.Int((int) task->getPriority());
              writer.String("committed");
              writer.Bool(task->isCommitted());
              writer.String("selected");
              writer.Bool(task == selected);
            }
            writer.EndObject();
          }
        }
        writer.EndArray();
      };

      writer.StartObject();
      {
        writeSection("head", motion::core::BodySectionId::Head, d_headTask);
        writeSection("arms", motion::core::BodySectionId::Arms, d_armsTask);
        writeSection("legs", motion::core::BodySectionId::Legs, d_legsTask);
      }
      writer.EndObject();
    }
  }
}
