// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <atomic>
#include <memory>
#include <vector>

#include "stateobserver.hh"

#include "state/State/state.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "src/util/assert.hh"

namespace bold
{
  namespace state {
    class StateObjectBase;

    //
    // TypedStateObserver class template
    //

    template<typename TState>
    class TypedStateObserver : public StateObserver {
    public:
      TypedStateObserver(std::string observerName, ThreadId callbackThread)
          : StateObserver(observerName, callbackThread),
            d_typeIndex(typeid(TState)) {
        static_assert(std::is_base_of<StateObjectBase, TState>::value, "T must be a descendant of StateObject");
        d_types.push_back(d_typeIndex);
      }

      virtual ~TypedStateObserver() = default;

      void observe(util::SequentialTimer &timer) override {
        ASSERT(ThreadUtil::getThreadId() == d_callbackThreadId);
        std::shared_ptr<StateObjectBase const> state = State::getByTypeIndex(d_typeIndex);
        std::shared_ptr<TState const> typedState = std::dynamic_pointer_cast<TState const>(state);
        observeTyped(typedState, timer);
      }

      virtual void observeTyped(std::shared_ptr<TState const> const &state, util::SequentialTimer &timer) = 0;

    private:
      std::type_index d_typeIndex;
    };
  }
}
