// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "stateobserver.hh"

#include "src/util/assert.hh"

using namespace bold;
using namespace bold::state;
using namespace std;

StateObserver::StateObserver(string observerName, ThreadId callbackThread)
: d_types(),
  d_callbackThreadId(callbackThread),
  d_name(observerName)
{
  // Initially, not dirty
  d_lock.test_and_set(memory_order_acquire);
}

void StateObserver::setDirty() { d_lock.clear(memory_order_release); }

bool StateObserver::testAndClearDirty()
{
  ASSERT(ThreadUtil::getThreadId() == d_callbackThreadId);

  // Prevent race condition
  // If flag is false, then the state is dirty
  return !d_lock.test_and_set(memory_order_acquire);
}
