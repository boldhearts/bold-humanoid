// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "PatternGenerator/patterngenerator.hh"
#include <cmath>

namespace bold
{
  class SineGenerator : public PatternGenerator
  {
  public:
    SineGenerator()
      : SineGenerator(1.0, 0.0, 0.0, 0.0)
    {}

    SineGenerator(double period, double periodShift, double amplitude, double amplitudeShift)
      : PatternGenerator{period},
      d_periodShift{periodShift},
      d_amplitude{amplitude},
      d_amplitudeShift{amplitudeShift}
    {}

    void setPeriodShift(double periodShift) { d_periodShift = periodShift; }
    void setAmplitude(double amplitude) { d_amplitude = amplitude; }
    void setAmplitudeShift(double amplitudeShift) { d_amplitudeShift = amplitudeShift; }

    double getOutput(double time) override
    {
      return d_amplitude * sin(2.0 * M_PI / getPeriod() * time - d_periodShift) + d_amplitudeShift;
    }

  private:
    double d_periodShift;
    double d_amplitude;
    double d_amplitudeShift;
  };
}

