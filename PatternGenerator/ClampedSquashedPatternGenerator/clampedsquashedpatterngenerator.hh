// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "PatternGenerator/patterngenerator.hh"
#include <utility>
#include <cmath>
#include <vector>

namespace bold
{
  template<typename Base>
  class ClampedSquashedPatternGenerator : public Base
  {
  public:
    ClampedSquashedPatternGenerator() = default;

    template<typename... As>
    ClampedSquashedPatternGenerator(double patternRatio, bool halfPeriodClamp, As&&... args)
      : Base(std::forward<As>(args)...),
        d_patternRatio(patternRatio),
        d_halfPeriodClamp(halfPeriodClamp)
    {
      determineClampTimes();
    }

    void setPatternRatio(double ratio, bool halfPeriodClamp)
    {
      d_patternRatio = ratio; 
      d_halfPeriodClamp = halfPeriodClamp;
      determineClampTimes();
    }

    double baseTime(double time)
    {
      bool clamped = false;
      double t0 = 0.0;
      double bTime = time;
      for (auto t : d_clampTimes)
      {
        if (clamped && t <= time)
          bTime -= (t - t0);
        else if (!clamped && t < time)
          t0 = t;
        else if (clamped)
          bTime -= (time - t0);
          
        if (t >= time)
          break;

        clamped = !clamped;
      }
      return bTime / d_patternRatio;
    }

    double getOutput(double time) override
    {
      return 0.0;
    }

  private:
    void determineClampTimes()
    {
      double halfClampDuration = (1.0 - d_patternRatio) * Base::getPeriod() / 2;

      if (d_halfPeriodClamp)
        halfClampDuration /= 2;

      d_clampTimes.clear();
      d_clampTimes.push_back(0.0);
      d_clampTimes.push_back(halfClampDuration);
      if (d_halfPeriodClamp)
      {
        d_clampTimes.push_back(Base::getPeriod() / 2 - halfClampDuration);
        d_clampTimes.push_back(Base::getPeriod() / 2 + halfClampDuration);
      }
      d_clampTimes.push_back(Base::getPeriod() - halfClampDuration);
      d_clampTimes.push_back(Base::getPeriod());
    }

    double d_patternRatio;
    double d_halfPeriodClamp;

    std::vector<double> d_clampTimes;
  };

}
