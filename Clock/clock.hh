// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iosfwd>

namespace bold
{
  class Clock
  {
  public:
    /// A point in time, in microseconds
    using Timestamp = unsigned long long;

    virtual Timestamp getTimestamp() const = 0;

    double getMillis() const;

    double getSeconds() const;

    double getSecondsSince(Timestamp since) const;

    double getMillisSince(Timestamp since) const;

    std::string describeDurationSince(Timestamp timestamp) const;

    static double timestampToMillis(Timestamp timestamp);

    static double timestampToSeconds(Timestamp timestamp);

    static Timestamp millisToTimestamp(double millis);

    static Timestamp secondsToTimestamp(double seconds);

    static std::string describeDurationSeconds(double seconds);
  };



  inline double Clock::timestampToMillis(Timestamp timestamp)
  {
    return timestamp / 1e3;
  }

  inline double Clock::timestampToSeconds(Timestamp timestamp)
  {
    return timestamp / 1e6;
  }

  inline Clock::Timestamp Clock::millisToTimestamp(double millis)
  {
    return Timestamp(millis * 1e3);
  }

  inline Clock::Timestamp Clock::secondsToTimestamp(double seconds)
  {
    return Timestamp(seconds * 1e6);
  }

  class SystemClock : public Clock
  {
  public:
    Timestamp getTimestamp() const override;
  };

  class TimeTravelClock : public Clock
  {
  public:
    Timestamp getTimestamp() const override { return d_timestamp; }
    void setTimestamp(Timestamp timestamp) { d_timestamp = timestamp; }

    void advance(Timestamp delta) { d_timestamp += delta; }
    void advanceMillis(double delta) { advance(Clock::millisToTimestamp(delta)); }
    void advanceSeconds(double delta) { advance(Clock::secondsToTimestamp(delta)); }

  private:
    Timestamp d_timestamp;
  };
}
