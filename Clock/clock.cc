// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "clock.hh"

#include "src/util/Log/log.hh"

#include <string.h>
#include <sys/time.h>
#include <math.h>

using namespace bold;
using namespace bold::util;

double Clock::getMillis() const
{
  return timestampToMillis(getTimestamp());
}

double Clock::getSeconds() const
{
  return timestampToSeconds(getTimestamp());
}

double Clock::getSecondsSince(Timestamp since) const
{
  Timestamp now = getTimestamp();
#ifdef INCLUDE_ASSERTIONS
  if (now < since)
    Log::warning("Clock") << "Time reversed. now=" << now << " since=" << since << " delta=" << (now-since);
#endif
  return timestampToSeconds(now - since);
}

double Clock::getMillisSince(Timestamp since) const
{
  Timestamp now = getTimestamp();
#ifdef INCLUDE_ASSERTIONS
  if (now < since)
    Log::warning("Clock") << "Time reversed. now=" << now << " since=" << since << " delta=" << (now-since);
#endif
  return timestampToMillis(now - since);
}

std::string Clock::describeDurationSeconds(double seconds)
{
  seconds = fabs(seconds);

  int minutes = seconds / 60;
  int hours = seconds / (60 * 60);
  int days = seconds / (60 * 60 * 24);

  std::stringstream out;

  if (days > 2)
  {
    out << days << " day" << (days == 1 ? "" : "s");
  }
  else if (minutes > 90)
  {
    out << hours << " hour" << (hours == 1 ? "" : "s");
  }
  else if (seconds > 90)
  {
    out << minutes << " minute" << (minutes == 1 ? "" : "s");
  }
  else
  {
    out << seconds << " second" << (seconds == 1 ? "" : "s");
  }

  return out.str();
}

std::string Clock::describeDurationSince(Clock::Timestamp timestamp) const
{
  return describeDurationSeconds(getSecondsSince(timestamp));
}

Clock::Timestamp SystemClock::getTimestamp() const
{
  struct timeval now;
  if (gettimeofday(&now, 0) == -1)
    Log::warning("Clock::getTimestamp") << "Error returned by gettimeofday: " << strerror(errno) << " (" << errno << ")";
  return (Timestamp)now.tv_usec + ((Timestamp)now.tv_sec * (Timestamp)1000000);
}

