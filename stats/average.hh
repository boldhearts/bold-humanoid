// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "traits.hh"
#include <Eigen/Core>

namespace bold
{
  template<typename T>
  class Average
  {
  public:
    Average()
    {
      reset();
    }

    void reset()
    {
      d_count = 0;
      AveragingTraits::zero(d_sum);
      AveragingTraits::zero(d_average);
    }

    void add(T value)
    {
      d_sum = AveragingTraits::sum(d_sum, value);
      d_count++;
      d_average = AveragingTraits::div(d_sum, d_count);
    }

    T getAverage() const { return d_average; }

    int getCount() const { return d_count; }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  private:
    int d_count;
    T d_sum;
    T d_average;
  };
}
