// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/geometry2/Point/point.hh"

namespace bold
{
  
  /** Compile time test whether a type has a \a fill member
   *
   * Works thanks to SFINAE (Substitution Failure Is Not An Error): if
   * U::fill does not exist, \a decltype does not give a valid type to
   * substitute as an argument, which removes the first template
   * method from valid options. Otherwise, it passes and is chosen as
   * being more specific than the second overload.
   */
  template<typename T>
  struct has_fill_member
  {
    typedef char yes;
    typedef long no;

    template<typename U>
    static yes test(decltype(&U::fill));

    template<typename U>
    static no test(...);

    static const bool value = sizeof(test<T>(0)) == sizeof(yes);
  };

  /// Traits used to select correct method of zero-ing the used type
  struct AveragingTraits
  {
    template<typename T>
    static void zero(T& v, typename std::enable_if<has_fill_member<T>::value>::type* dummy = 0) { v.fill(0); }

    template<typename T>
    static void zero(T& v, typename std::enable_if<std::is_arithmetic<T>::value>::type* dummy = 0) { v = 0; }


    template<typename T>
    static T sum(T const& a, T const& b) { return a + b; }

    template<typename T, int DIM>
    static geometry2::Point<T, DIM> sum(geometry2::Point<T, DIM> const& a, geometry2::Point<T, DIM> const& b)
    {
      return a + (b - geometry2::Point<T, DIM>::ORIGIN);
    }


    template<typename T>
    static T diff(T const& a, T const& b) { return a - b; }

    template<typename T, int DIM>
    static geometry2::Point<T, DIM> diff(geometry2::Point<T, DIM> const& a, geometry2::Point<T, DIM> const& b)
    {
      return a - (b - geometry2::Point<T, DIM>::ORIGIN);
    }


    template<typename T, typename U>
    static T div(T const& a, U const& b) { return a / b; }

    template<typename T, int DIM, typename U>
    static geometry2::Point<T, DIM> div(geometry2::Point<T, DIM> const& a, U const& b)
    {
      return geometry2::Point<T, DIM>::ORIGIN +  (a - geometry2::Point<T, DIM>::ORIGIN) / b;
    }


    template<typename T>
    static T sqrt(T const& v, typename std::enable_if<has_fill_member<T>::value>::type* dummy = 0) { return v.array().sqrt(); }

    template<typename T>
    static T sqrt(T const& v, typename std::enable_if<std::is_arithmetic<T>::value>::type* dummy = 0) { return sqrt(v); }


    template<typename T>
    static T mult(T const& v1, T const& v2, typename std::enable_if<has_fill_member<T>::value>::type* dummy = 0)
    {
      return v1.array() * v2.array();
    }

    template<typename T>
    static T mult(T const& v1, T const& v2, typename std::enable_if<std::is_arithmetic<T>::value>::type* dummy = 0)
    {
      return v1 * v2;
    }
  };
}
