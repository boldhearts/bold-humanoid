// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iterator>
#include <cmath>

namespace bold
{
  template<typename T, typename Iterator>
  T average(Iterator from, Iterator end)
  {
    int count = 0;
    T sum = {0};

    for (auto it = from; it < end; it++)
    {
      sum += *it;
      count++;
    }

    return sum / count;
  }

  template<typename T, typename Iterator>
  T variance(Iterator from, Iterator end)
  {
    static_assert(std::is_floating_point<T>::value, "Only floating point types supported at present");

    T average = ::bold::average<T>(from, end);
    T sumDevs = {0};
    int count = 0;

    for (auto it = from; it < end; it++)
    {
      sumDevs += pow(*it - average, 2);
      count++;
    }

    T variance = sumDevs/count;

    return variance;
  }

  template<typename T, typename Iterator>
  T stdDev(Iterator from, Iterator end)
  {
    static_assert(std::is_floating_point<T>::value, "Only floating point types supported at present");

    return sqrt(::bold::variance<T>(from, end));
  }
}
