// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cmath>
#include <vector>
#include <stdexcept>
#include <string.h>
#include <Eigen/Core>

#include "traits.hh"
#include "src/util/assert.hh"

namespace bold
{
  template<typename T>
  class MovingAverage
  {
  public:
    MovingAverage(uint16_t windowSize);

    int count() const { return d_length; }
    int getWindowSize() const { return d_windowSize; }
    bool isMature() const { return d_length == d_windowSize; }

    T next(T value);

    void reset();

    T getAverage() const { return d_avg; }

    T calculateStdDev();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  private:
    std::vector<T, Eigen::aligned_allocator<T>> d_items;
    uint16_t d_length;
    int d_nextPointer;
    T d_sum;
    T d_avg;
    uint16_t d_windowSize;
  };


  template<typename T>
  MovingAverage<T>::MovingAverage(uint16_t windowSize)
    : d_items(windowSize),
      d_length(0),
      d_nextPointer(0),
      d_windowSize(windowSize)
  {
    if (windowSize == 0)
      throw new std::runtime_error("Cannot have zero window size.");
    AveragingTraits::zero(d_sum);
  }
  
  template<typename T>
  T MovingAverage<T>::next(T value)
  {
    if (d_length == d_windowSize)
      d_sum = AveragingTraits::diff(d_sum, d_items[d_nextPointer]);
    else
      d_length++;

    d_items[d_nextPointer] = value;
    d_sum = AveragingTraits::sum(d_sum, value);
    d_nextPointer = (d_nextPointer + 1) % d_windowSize;
    
    d_avg = AveragingTraits::div(d_sum, int{d_length});
    
    return d_avg;
  }
  
  template<typename T>
  void MovingAverage<T>::reset()
  {
    d_length = 0;
    d_nextPointer = 0;
    AveragingTraits::zero(d_sum);
  }

  template<typename T>
  T MovingAverage<T>::calculateStdDev()
  {
    // TODO unit test this
    T sum;
    AveragingTraits::zero(sum);
    for (int i = 0; i < d_length; i++)
    {
      int index = (d_nextPointer - i - 1) % d_windowSize;
      if (index < 0)
        index += d_windowSize;
      ASSERT(index >= 0 && index < d_length);
      T diff = d_items[index] - d_avg;
      sum = AveragingTraits::sum(sum, AveragingTraits::mult(diff, diff));
    }
    return AveragingTraits::sqrt(AveragingTraits::div(sum, d_length));
  }

  template <>
  inline Eigen::Vector3d MovingAverage<Eigen::Vector3d>::calculateStdDev()
  {
    Eigen::Matrix3d sum = Eigen::Matrix3d::Zero();
    for (int i = 0; i < d_length; i++)
    {
      int index = (d_nextPointer - i - 1) % d_windowSize;
      if (index < 0)
        index += d_windowSize;
      ASSERT(index >= 0 && index < d_length);
      Eigen::Vector3d diff = d_items[index] - d_avg;
      sum += diff * diff.transpose();
    }
    Eigen::Vector3d r = sum.diagonal();
    for (int i = 0; i < r.size(); i++)
      r[i] = sqrt(r[i]);
    return r / d_length;
  }

  template <>
  inline Eigen::Vector2d MovingAverage<Eigen::Vector2d>::calculateStdDev()
  {
    Eigen::Matrix2d sum = Eigen::Matrix2d::Zero();
    for (int i = 0; i < d_length; i++)
    {
      int index = (d_nextPointer - i - 1) % d_windowSize;
      if (index < 0)
        index += d_windowSize;
      ASSERT(index >= 0 && index < d_length);
      Eigen::Vector2d diff = d_items[index] - d_avg;
      sum += diff * diff.transpose();
    }
    Eigen::Vector2d r = sum.diagonal();
    for (int i = 0; i < r.size(); i++)
      r[i] = sqrt(r[i]);
    return r / d_length;
  }
}
