// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "behaviourcontrol.hh"

#include "Agent/agent.hh"
#include "state/State/state.hh"
#include "state/StateObject/BehaviourControlState/behaviourcontrolstate.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

BehaviourControl::BehaviourControl(Agent &agent)
    : d_agent(agent),
      d_playMode{PlayMode::INITIAL},
      d_playerRole{PlayerRole::Idle},
      d_playerActivity{PlayerActivity::Positioning},
      d_playerStatus{PlayerStatus::Inactive},
      d_playerRoleSetThisCycle{false},
      d_playerStatusSetThisCycle{false},
      d_playerActivitySetThisCycle{false},
      d_playModeSetThisCycle{false} {
}

void BehaviourControl::updateStateObject() {
  d_playerRoleSetThisCycle = false;
  d_playerStatusSetThisCycle = false;
  d_playerActivitySetThisCycle = false;
  d_playModeSetThisCycle = false;
  State::make<BehaviourControlState>(d_agent.getThinkCycleNumber(), d_playerRole, d_playerActivity, d_playerStatus);
}

void BehaviourControl::setPlayerRole(PlayerRole role) {
  if (role == d_playerRole)
    return;
  if (d_playerRoleSetThisCycle)
    Log::warning("BehaviourControl::setPlayerRole") << "PlayerRole set multiple times during think cycle, with values "
                                                    << getPlayerRoleString(d_playerRole) << " and "
                                                    << getPlayerRoleString(role);
  d_playerRole = role;
  d_playerRoleSetThisCycle = true;
}

void BehaviourControl::setPlayerStatus(PlayerStatus status) {
  if (status == d_playerStatus)
    return;
  if (d_playerStatusSetThisCycle) {
    Log::warning("BehaviourControl::setPlayerStatus")
        << "PlayerStatus set multiple times during think cycle, with values " << getPlayerStatusString(d_playerStatus)
        << " and " << getPlayerStatusString(status);
  }
  d_playerStatus = status;
  d_playerStatusSetThisCycle = true;
}

void BehaviourControl::setPlayerActivity(PlayerActivity activity) {
  if (activity == d_playerActivity)
    return;
  if (d_playerActivitySetThisCycle) {
    Log::warning("BehaviourControl::setPlayerActivity")
        << "PlayerActivity set multiple times during think cycle, with values "
        << getPlayerActivityString(d_playerActivity) << " and " << getPlayerActivityString(activity);
  }
  d_playerActivity = activity;
  d_playerActivitySetThisCycle = true;
}

void BehaviourControl::setPlayMode(PlayMode playMode) {
  if (playMode == d_playMode)
    return;
  if (d_playModeSetThisCycle) {
    Log::warning("BehaviourControl::setPlayMode") << "PlayMode set multiple times during think cycle, with values "
                                                  << getPlayModeName(d_playMode) << " and "
                                                  << getPlayModeName(playMode);
  }
  d_playMode = playMode;
  d_playModeSetThisCycle = true;
}
