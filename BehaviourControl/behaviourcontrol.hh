// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "state/StateObject/GameState/gamestate.hh"
#include "state/StateObject/TeamState/teamstate.hh"

namespace bold {
  class Agent;

  class BehaviourControl {
  public:
    BehaviourControl(Agent &agent);

    state::PlayerRole getPlayerRole() const { return d_playerRole; }

    state::PlayerStatus getPlayerStatus() const { return d_playerStatus; }

    state::PlayerActivity getPlayerActivity() const { return d_playerActivity; }

    state::PlayMode getPlayMode() const { return d_playMode; }

    void setPlayerRole(state::PlayerRole role);

    void setPlayerStatus(state::PlayerStatus status);

    void setPlayerActivity(state::PlayerActivity activity);

    void setPlayMode(state::PlayMode playMode);

    void updateStateObject();

  private:
    Agent &d_agent;
    state::PlayMode d_playMode;
    state::PlayerRole d_playerRole;
    state::PlayerActivity d_playerActivity;
    state::PlayerStatus d_playerStatus;

    bool d_playerRoleSetThisCycle;
    bool d_playerStatusSetThisCycle;
    bool d_playerActivitySetThisCycle;
    bool d_playModeSetThisCycle;
  };
}
