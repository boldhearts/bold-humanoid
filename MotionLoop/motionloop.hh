// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>
#include <memory>
#include <sigc++/signal.h>

#include "AccelerometerModel/accelerometermodel.hh"
#include "FSRModel/fsrmodel.hh"
#include "GyroModel/gyromodel.hh"
#include "JointId/jointid.hh"

#include "util/Loop/loop.hh"
#include "util/LoopRegulator/loopregulator.hh"

namespace bold {
  namespace motion {
    namespace core {
      class BodyControl;
    }
  }

  namespace util {
    class SequentialTimer;
  }

  class BodyModel;

  class BulkRead;

  class BulkReadTable;

  class CM730;

  class CM730CommsModule;

  class LEDControl;

  namespace state {
    class HardwareState;
  }

  enum class MX28Table : uint8_t;

  class MotionLoop : public util::Loop {
  public:
    static constexpr uint8_t TIME_UNIT_MS = 8;

    MotionLoop(std::shared_ptr<LEDControl> ledControl,
               std::shared_ptr<BodyModel> bodyModel,
               std::shared_ptr<Clock> clock);

    void addCommsModule(std::shared_ptr<CM730CommsModule> const &module);

    /// Signal raised whenever the motion loop fails to read from the CM730.
    /// Callback is passed the number of consecutive failures.
    sigc::signal<void, uint> onReadFailure;

  private:
    void onLoopStart() override;

    void onStep(unsigned cycleNumber) override;

    void onStopped() override;

    void initialiseHardwareTables();

    void checkPower(size_t cycleNumber);

    void togglePowerIfNeeded();

    void toggleTorqueIfNeeded();

    void updateLEDs();

    void writeByteWithRetry(BulkReadTable const &table, uint8_t jointId, MX28Table address, uint8_t value,
                            size_t retryCount);

    void writeWordWithRetry(BulkReadTable const &table, uint8_t jointId, MX28Table address, uint16_t value,
                            size_t retryCount);

    bool ping(uint8_t jointId);

    bool applyJointMotionTasks(util::SequentialTimer &t);

    bool writeJointData(util::SequentialTimer &t);

    std::shared_ptr<state::HardwareState const> readHardwareState(util::SequentialTimer &t);

    std::shared_ptr<state::HardwareState const> readHardwareStateFake(util::SequentialTimer &t);

    bool updateStaticHardwareState();

    std::shared_ptr<Clock> d_clock;

    std::unique_ptr<CM730> d_cm730;
    std::shared_ptr<LEDControl> d_ledControl;
    std::shared_ptr<BodyModel> d_bodyModel;
    std::shared_ptr<motion::core::BodyControl> d_bodyControl;
    std::unique_ptr<BulkRead> d_dynamicBulkRead;
    std::unique_ptr<BulkRead> d_staticBulkRead;

    std::array<GyroModel, 3> d_gyroModels;
    AccelerometerModel d_accelerometerModel;
    FSRModel d_fsrModel;

    std::vector<std::shared_ptr<CM730CommsModule>> d_commsModules;

    util::LoopRegulator d_loopRegulator;

    /// Set of static offsets to be added to all target positions sent to hardware.
    /// May be used to compensate for angular positional errors.
    /// See also the offset_tuner project.
    int d_offsets[(uint8_t) JointId::MAX + 1];

    /// Whether we have connected to a CM730 subcontroller.
    bool d_haveBody;
    /// Whether we have FSR foot sensors.
    bool d_haveFSR;
    /// Whether the CM730 subcontroller is powered. Updated every N cycles.
    bool d_isCM730PowerEnabled;
    /// Whether the loop has read any values yet.
    bool d_readYet;

    bool d_staticHardwareStateUpdateNeeded;
    unsigned d_consecutiveReadFailureCount;

    bool d_powerChangeNeeded;
    bool d_powerChangeToValue;
    bool d_torqueChangeNeeded;
    bool d_torqueChangeToValue;
  };
}
