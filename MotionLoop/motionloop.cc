// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionloop.hh"

#include "BulkRead/bulkread.hh"
#include "BulkReadTable/bulkreadtable.hh"
#include "CM730/cm730.hh"
#include "CM730CommsModule/cm730commsmodule.hh"
#include "CM730Table/cm730table.hh"
#include "CM730Platform/CM730Linux/cm730linux.hh"
#include "CM730Snapshot/cm730snapshot.hh"
#include "FSR/fsr.hh"
#include "GyroModel/gyromodel.hh"
#include "LEDControl/ledcontrol.hh"
#include "MX28Snapshot/mx28snapshot.hh"
#include "state/State/state.hh"
#include "state/StateObject/AccelerometerState/accelerometerstate.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/BodyControlState/bodycontrolstate.hh"
#include "state/StateObject/FSRState/fsrstate.hh"
#include "state/StateObject/GyroCalibrationState/gyrocalibrationstate.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "state/StateObject/StaticHardwareState/statichardwarestate.hh"
#include "state/StateObject/TimingState/timingstate.hh"
#include "state/StateObject/MotionTaskState/motiontaskstate.hh"

#include "config/Config/config.hh"

#include "motion/core/BodyControl/bodycontrol.hh"
#include "motion/core/JointSelection/jointselection.hh"
#include "motion/core/MotionModule/motionmodule.hh"

#include "roundtable/Action/action.hh"

#include "util/binary.hh"

#include <rapidjson/prettywriter.h>
#include <rapidjson/filewritestream.h>

#ifndef MOTION_LOOP_POLICY
#define MOTION_LOOP_POLICY SCHED_RR
#endif

using namespace bold;
using namespace bold::config;
using namespace bold::motion::core;
using namespace bold::roundtable;
using namespace bold::state;
using namespace bold::util;
using namespace std;

constexpr uint8_t MotionLoop::TIME_UNIT_MS;

MotionLoop::MotionLoop(shared_ptr<LEDControl> ledControl, shared_ptr<BodyModel> bodyModel, std::shared_ptr<Clock> clock)
  : Loop("Motion Loop", clock, MOTION_LOOP_POLICY, -1),
    d_clock(move(clock)),
    d_ledControl(move(ledControl)),
    d_bodyModel(move(bodyModel)),
    d_haveBody(false),
    d_isCM730PowerEnabled(false),
    d_readYet(false),
    d_staticHardwareStateUpdateNeeded(true),
    d_consecutiveReadFailureCount(0),
    d_powerChangeNeeded(false),
    d_powerChangeToValue(false),
    d_torqueChangeNeeded(false),
    d_torqueChangeToValue(false)
{
  d_haveFSR = Config::getStaticValue<bool>("hardware.fsr.enabled");

  d_loopRegulator.setIntervalMicroseconds(TIME_UNIT_MS * 1000);

  d_bodyControl = make_shared<BodyControl>();

  auto fsrDeviceIds = array<uint8_t, 2>{ {FSR::ID_FSR_L, FSR::ID_FSR_R} };
  d_dynamicBulkRead =
    make_unique<BulkRead>(CM730::ID_CM,
                          fsrDeviceIds,
                          (uint8_t)CM730Table::DXL_POWER, (uint8_t)CM730Table::VOLTAGE,
                          (uint8_t)MX28Table::PRESENT_POSITION_L, (uint8_t)MX28Table::PRESENT_TEMPERATURE,
                          d_haveFSR ? (uint8_t)FSRTable::FSR1_L : 0, d_haveFSR ? (uint8_t)FSRTable::FSR_Y : 0);
  
  d_staticBulkRead =
    make_unique<BulkRead>(CM730::ID_CM,
                          fsrDeviceIds,
                          (uint8_t)CM730Table::MODEL_NUMBER_L, (uint8_t)CM730Table::RETURN_LEVEL,
                          (uint8_t)MX28Table::MODEL_NUMBER_L, (uint8_t)MX28Table::LOCK,
                          d_haveFSR ? (uint8_t)FSRTable::MODEL_NUMBER_L : 0, d_haveFSR ? (uint8_t)FSRTable::RETURN_LEVEL : 0);
  
  Action::addAction("hardware.query-static-hardware-state", "Query static HW state", [this] { d_staticHardwareStateUpdateNeeded = true; });
//   Config::addAction("hardware.cm730-power-on",  "CM730 On",  [this] { d_powerChangeToValue = true;  d_powerChangeNeeded = true; });
//   Config::addAction("hardware.cm730-power-off", "CM730 Off", [this] { d_powerChangeToValue = false; d_powerChangeNeeded = true; });
  Action::addAction("hardware.motor-torque-on",  "Torque On",  [this] { d_torqueChangeToValue = true;  d_torqueChangeNeeded = true; });
  Action::addAction("hardware.motor-torque-off", "Torque Off", [this] { d_torqueChangeToValue = false; d_torqueChangeNeeded = true; });

  d_offsets[0] = 0;
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    stringstream path;
    path << "hardware.offsets." << JointName::getJsonName(jointId);
    Config::getSetting<int>(path.str())->track([this, jointId](int value)
    {
      d_offsets[jointId] = value;
      d_bodyControl->getJoint((JointId)jointId).notifyOffsetChanged();
    });
  }
}

void MotionLoop::addCommsModule(shared_ptr<CM730CommsModule> const& module)
{
  ASSERT(module);
  d_commsModules.push_back(module);
}

void MotionLoop::onLoopStart()
{
  // Connect to hardware subcontroller
  auto cm730DevicePath = Config::getStaticValue<string>("hardware.cm730-path");
  Log::info("MotionLoop::start") << "Using CM730 Device Path: " << cm730DevicePath;
  d_cm730 = unique_ptr<CM730>(new CM730(make_unique<CM730Linux>(cm730DevicePath, d_clock)));

  d_readYet = false;

  ThreadUtil::setThreadId(ThreadId::MotionLoop);

  d_haveBody = d_cm730->connect();

  if (!d_haveBody)
  {
    Log::warning("MotionLoop::threadMethod") << "Motion loop running without a body";
  }
  else
  {
    // Set all CM730/MX28 values to a known set of initial values before continuing
    initialiseHardwareTables();

    if (!d_cm730->torqueEnable(true))
      Log::error("MotionLoop::threadMethod") << "Error enabling torque";
  }

  d_loopRegulator.start();
}

void MotionLoop::initialiseHardwareTables()
{
  ASSERT(d_haveBody);

  Log::verbose("MotionLoop::initialiseHardwareTables") << "Starting";

  //
  // Read current EEPROM values.
  // Avoid writing to EEPROM unless necessary.
  //

  BulkRead eepromBulkRead(CM730::ID_CM,
                          { {FSR::ID_FSR_L, FSR::ID_FSR_R} },
                          (uint8_t)CM730Table::MODEL_NUMBER_L, (uint8_t)CM730Table::RETURN_LEVEL,
                          (uint8_t)MX28Table::MODEL_NUMBER_L, (uint8_t)MX28Table::ALARM_SHUTDOWN,
                          d_haveFSR ? (uint8_t)FSRTable::MODEL_NUMBER_L : 0, d_haveFSR ? (uint8_t)FSRTable::RETURN_LEVEL : 0);

  Log::trace("MotionLoop::initialiseHardwareTables") << "eepromBulkRead.rxLength: " << eepromBulkRead.getRxLength();

  while (true)
  {
    auto res = d_cm730->bulkRead(&eepromBulkRead);
    if (res == CommResult::SUCCESS)
      break;
    Log::warning("MotionLoop::initialiseHardwareTables")
      << "Communication problem (" << getCommResultName(res) << ") during bulk read. Retrying...";
  }

  //
  // Prepare values to be written
  //

  // Under which circumstances do we want the LED to light up?
  MX28Alarm alarmLed;
  alarmLed.setOverheatedFlag();
  alarmLed.setOverloadFlag();

  // Under which circumstances do we want the MX28 to automatically shut down?
  MX28Alarm alarmShutdown;
  alarmShutdown.setOverheatedFlag();
  alarmShutdown.setOverloadFlag();

  int tempLimit = Config::getStaticValue<int>("hardware.limits.temperature-centigrade");
  Range<double> voltageRange = Config::getStaticValue<Range<double>>("hardware.limits.voltage-range");

  //
  // Loop through all MX28s
  //
  const int retryCount = 10;

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    if (!ping(jointId))
      continue;

    stringstream path;
    path << "hardware.limits.angle-limits." << JointName::getJsonName(jointId);
    Range<double> rangeDegs = Config::getStaticValue<Range<double>>(path.str());

    auto const& table = eepromBulkRead.getBulkReadData(jointId);

    writeByteWithRetry(table, jointId, MX28Table::RETURN_DELAY_TIME, 0, retryCount);
    writeByteWithRetry(table, jointId, MX28Table::RETURN_LEVEL, 2, retryCount);
    writeWordWithRetry(table, jointId, MX28Table::CW_ANGLE_LIMIT_L, MX28::degs2Value(rangeDegs.min()), retryCount);
    writeWordWithRetry(table, jointId, MX28Table::CCW_ANGLE_LIMIT_L, MX28::degs2Value(rangeDegs.max()), retryCount);
    writeByteWithRetry(table, jointId, MX28Table::HIGH_LIMIT_TEMPERATURE, MX28::centigrade2Value(tempLimit), retryCount);
    writeByteWithRetry(table, jointId, MX28Table::LOW_LIMIT_VOLTAGE, MX28::voltage2Value(voltageRange.min()), retryCount);
    writeByteWithRetry(table, jointId, MX28Table::HIGH_LIMIT_VOLTAGE, MX28::voltage2Value(voltageRange.max()), retryCount);
    writeWordWithRetry(table, jointId, MX28Table::MAX_TORQUE_L, MX28::MAX_TORQUE, retryCount);
    writeByteWithRetry(table, jointId, MX28Table::ALARM_LED, alarmLed.getFlags(), retryCount);
    writeByteWithRetry(table, jointId, MX28Table::ALARM_SHUTDOWN, alarmShutdown.getFlags(), retryCount);
  }

  Log::info("MotionLoop::initialiseHardwareTables") << "All MX28 data tables initialised";
}

void MotionLoop::onStep(unsigned cycleNumber)
{
  auto clock = make_shared<SystemClock>();
  SequentialTimer t(clock);

  // Rate the limit at which we make this call to the CM730.
  checkPower(cycleNumber);
  t.timeEvent("Check Power");

  // When unpowered, don't perform any update at all in the cycle
  if (d_haveBody && !d_isCM730PowerEnabled)
    return;

  // Don't write until we've read
  if (d_readYet)
  {
    if (d_haveBody)
    {
      togglePowerIfNeeded();
      toggleTorqueIfNeeded();
    }

    // Let motion modules update body control
    t.enter("Apply Motion Module");
    applyJointMotionTasks(t);
    t.exit();

    // Write to MX28s
    bool anythingChanged = d_haveBody
      ? writeJointData(t)
      : true;

    // Update BodyControlState
    if (anythingChanged)
    {
      State::make<BodyControlState>(d_bodyControl, cycleNumber);
      t.timeEvent("Set BodyControlState");
      d_bodyControl->clearModulation();
    }

    // Update LEDs
    updateLEDs();
    t.timeEvent("Write to CM730");
  }

  auto hw = d_haveBody
    ? readHardwareState(t)
    : readHardwareStateFake(t);

  // Ensure we were able to read something
  if (hw == nullptr)
    return;

  State::set(hw);

  State::make<BodyState>(*d_bodyModel, *hw, *d_bodyControl, cycleNumber);
  t.timeEvent("Update BodyState");

  if (!d_readYet)
  {
    // TODO should probably update the body control from hardware measurements whenever torque is enabled
    d_bodyControl->updateFromHardwareState(hw);
    State::make<BodyControlState>(d_bodyControl, cycleNumber);
    d_readYet = true;
  }

  // Run all state observers
  t.enter("Observers");
  State::callbackObservers(ThreadId::MotionLoop, t);
  t.exit();

  // Run all additional modules that communicate with CM730
  // Assume that comms modules cannot run when we are running without a body (debugging)
  if (d_haveBody)
  {
    t.enter("Comms");
    for (auto const& commsModule : d_commsModules)
    {
      t.enter(commsModule->getName());
      commsModule->step(d_cm730, t, cycleNumber);
      t.exit();
    }
    t.exit();
  }

  d_loopRegulator.wait();
  t.timeEvent("Sleep");

  // Set timing data for the motion cycle
  State::make<MotionTimingState>(t.flush(), cycleNumber, getFps());
}

void MotionLoop::checkPower(size_t cycleNumber)
{
  // Rate the limit at which we make this call to the CM730.
  if (cycleNumber % 60 == 0 && d_haveBody)
    d_isCM730PowerEnabled = d_cm730->isPowerEnabled();
}

void MotionLoop::togglePowerIfNeeded()
{
  if (d_powerChangeNeeded)
  {
    if (!d_cm730->powerEnable(d_powerChangeToValue))
      Log::error("MotionLoop::onStep") << "Error setting power to " << d_powerChangeToValue;
    d_powerChangeNeeded = false;
  }    
}

void MotionLoop::toggleTorqueIfNeeded()
{
  if (d_torqueChangeNeeded)
  {
    if (!d_cm730->torqueEnable(d_torqueChangeToValue))
      Log::error("MotionLoop::onStep") << "Error setting torque to " << d_torqueChangeToValue;
    d_torqueChangeNeeded = false;
  }
}

void MotionLoop::updateLEDs()
{
  if (d_haveBody)
  {
    uint16_t forehead = d_ledControl->getForeheadColourShort();
    uint16_t eye = d_ledControl->getEyeColourShort();
    
    vector<uint8_t> parameters{
      CM730::ID_CM,
        d_ledControl->getPanelLedByte(),
        binary::getLowByte(forehead),
        binary::getHighByte(forehead),
        binary::getLowByte(eye),
        binary::getHighByte(eye)
        };
    
    d_cm730->syncWrite((uint8_t)CM730Table::LED_PANEL, parameters.size(), 1, parameters);
    
    d_ledControl->clearDirtyFlags();
  }
  else
    d_ledControl->clearDirtyFlags();
}

void MotionLoop::onStopped()
{
  if (d_haveBody)
  {
    if (!d_cm730->torqueEnable(false))
      Log::error("MotionLoop::threadMethod") << "Error disabling torque";
  }

  // Destroy the CM730 object on the motion thread
  d_cm730.reset();
}

bool MotionLoop::applyJointMotionTasks(SequentialTimer& t)
{
  auto tasks = State::get<MotionTaskState>();

  if (!tasks || tasks->isEmpty())
    return false;

  for (pair<MotionModule*, shared_ptr<JointSelection>> const& pair : *tasks->getModuleJointSelection())
  {
    MotionModule* module = pair.first;
    shared_ptr<JointSelection> jointSelection = pair.second;

    ASSERT(module);
    ASSERT(jointSelection);

    module->step(*jointSelection);

    if (jointSelection->hasHead())
      module->applyHead(d_bodyControl->getHeadSection());

    if (jointSelection->hasArms())
      module->applyArms(d_bodyControl->getArmSection());

    if (jointSelection->hasLegs())
      module->applyLegs(d_bodyControl->getLegSection());

    t.timeEvent(module->getName());
  }

  return true;
}

bool MotionLoop::writeJointData(SequentialTimer& t)
{
  //
  // WRITE UPDATE
  //

  uint8_t dirtyDeviceCount = 0;
  Range<MX28Table> addrRange;
  for (BodySectionControl const& section : d_bodyControl->getBodySections())
    for (auto const& joint : section.getJoints())
    {
      if (joint.isDirty())
      {
        dirtyDeviceCount++;
        addrRange.expand(joint.getModifiedAddressRange());
      }
    }

  if (dirtyDeviceCount != 0)
  {
    // Prepare the parameters of a SyncWrite instruction
    int bytesPerDevice = 1 + (uint8_t)addrRange.size() + 1;

    vector<uint8_t> parameters(dirtyDeviceCount * bytesPerDevice);
    int n = 0;
    for (BodySectionControl& section : d_bodyControl->getBodySections())
      for (auto& joint : section.getJoints())
      {
        if (joint.isDirty())
        {
          parameters[n++] = joint.getId();

          if (addrRange.contains(MX28Table::D_GAIN))   parameters[n++] = joint.getDGain();
          if (addrRange.contains(MX28Table::I_GAIN))   parameters[n++] = joint.getIGain();
          if (addrRange.contains(MX28Table::P_GAIN))   parameters[n++] = joint.getPGain();
          if (addrRange.contains(MX28Table::RESERVED)) parameters[n++] = 0;

          if (addrRange.contains(MX28Table::GOAL_POSITION_L))
          {
            ASSERT(addrRange.contains(MX28Table::GOAL_POSITION_H));

            // Specify the goal position, and apply any modulation and calibration offset
            int goalPosition = MX28::clampValue(
              joint.getValue() +
              joint.getModulationOffset() +
              d_offsets[joint.getId()]
              );

            uint16_t value = MX28::clampValue(goalPosition);
            parameters[n++] = binary::getLowByte(value);
            parameters[n++] = binary::getHighByte(value);
          }

          joint.clearDirty();
        }
      }
    ASSERT(n == dirtyDeviceCount * bytesPerDevice);

    //
    // Send the SyncWrite message, if anything changed
    //

    d_cm730->syncWrite((uint8_t)addrRange.min(), bytesPerDevice, dirtyDeviceCount, parameters);
  }

  t.timeEvent("Write to MX28s");

  return dirtyDeviceCount != 0;
}

void writeHardwareStateJsonFile()
{
  using namespace rapidjson;

  auto state = State::get<StaticHardwareState>();

  ASSERT(state);

  FILE* file = fopen("eeprom.json", "w");

  char writeBuffer[65536];
  FileWriteStream stream(file, writeBuffer, sizeof(writeBuffer));
  PrettyWriter<FileWriteStream> writer(stream);
  state->writeJson(writer);
  stream.Flush();
  fclose(file);
}

shared_ptr<HardwareState const> MotionLoop::readHardwareState(SequentialTimer& t)
{
  ASSERT(d_haveBody);

  //
  // READ DATA
  //

  if (d_staticHardwareStateUpdateNeeded)
  {
    if (updateStaticHardwareState())
    {
      writeHardwareStateJsonFile();
      d_staticHardwareStateUpdateNeeded = false;
    }
    t.timeEvent("Read StaticHardwareState");
  }

  CommResult res = d_cm730->bulkRead(d_dynamicBulkRead.get());
  t.timeEvent("Read from CM730");

  if (res != CommResult::SUCCESS)
  {
    Log::warning("MotionLoop::process") << "CM730 bulk read failed (" << getCommResultName(res) << ")";
    onReadFailure(++d_consecutiveReadFailureCount);
    return nullptr;
  }

  if (d_consecutiveReadFailureCount)
    d_consecutiveReadFailureCount--;

  auto gyroCalibrationState = State::get<GyroCalibrationState>();
  if (gyroCalibrationState)
  {
    // TODO: we only have to do this once
    auto zeroRateLevel = gyroCalibrationState->getZeroRateLevel();
    d_gyroModels[0].setZeroRateLevel(zeroRateLevel.x());
    d_gyroModels[1].setZeroRateLevel(zeroRateLevel.y());
    d_gyroModels[2].setZeroRateLevel(zeroRateLevel.z());
  }

  auto cm730Snapshot = make_unique<CM730Snapshot const>(d_dynamicBulkRead->getBulkReadData(CM730::ID_CM),
                                                        d_gyroModels, d_accelerometerModel);

  auto mx28Snapshots = vector<unique_ptr<MX28Snapshot const>>();
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    mx28Snapshots.push_back(make_unique<MX28Snapshot const>(jointId, d_dynamicBulkRead->getBulkReadData(jointId), d_offsets[jointId]));

  //
  // MAKE SENSOR STATES
  //

  State::make<AccelerometerState>(cm730Snapshot->accRaw, d_accelerometerModel);
  
  if (d_haveFSR)
    State::make<FSRState>(d_dynamicBulkRead->getBulkReadData(FSR::ID_FSR_L),
                          d_dynamicBulkRead->getBulkReadData(FSR::ID_FSR_R),
                          d_fsrModel);

  //
  // UPDATE HARDWARE STATE
  //

  auto rxBytes = d_cm730->getReceivedByteCount();
  auto txBytes = d_cm730->getTransmittedByteCount();

  auto hw = make_shared<HardwareState const>(move(cm730Snapshot), move(mx28Snapshots), rxBytes, txBytes, getCycleNumber());
  t.timeEvent("Update HardwareState");
  return hw;
}

bool MotionLoop::updateStaticHardwareState()
{
  ASSERT(d_haveBody);

  CommResult res = d_cm730->bulkRead(d_staticBulkRead.get());

  if (res != CommResult::SUCCESS)
  {
    Log::warning("MotionLoop::updateStaticHardwareState") << "Bulk read failed -- skipping update of StaticHardwareState";
    return false;
  }

  auto cm730State = make_shared<StaticCM730State>(d_staticBulkRead->getBulkReadData(CM730::ID_CM));

  auto mx28States = vector<shared_ptr<StaticMX28State const>>();
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    mx28States.push_back(make_shared<StaticMX28State>(jointId, d_staticBulkRead->getBulkReadData(jointId)));

  State::make<StaticHardwareState>(cm730State, mx28States);
  return true;
}

shared_ptr<HardwareState const> MotionLoop::readHardwareStateFake(SequentialTimer& t)
{
  ASSERT(!d_haveBody);

  //
  // Dummy StaticHardwareState
  //

  if (d_staticHardwareStateUpdateNeeded)
  {
    auto mx28States = vector<shared_ptr<StaticMX28State const>>();
    for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    {
      auto mx28State = make_shared<StaticMX28State>(jointId);
      mx28State->alarmLed = MX28Alarm();
      mx28State->alarmShutdown = MX28Alarm();
      mx28State->torqueEnable = true;
      mx28States.push_back(mx28State);
    }

    auto cm730State = make_shared<StaticCM730State>();
    State::make<StaticHardwareState>(cm730State, mx28States);

    d_staticHardwareStateUpdateNeeded = false;
    t.timeEvent("Read StaticHardwareState");
  }

  //
  // Dummy FSRState
  //

  if (d_haveFSR)
  {
    auto fakeFSRBulkReadTable = BulkReadTable{};
    fakeFSRBulkReadTable.setStartAddress((uint8_t)FSRTable::FSR1_L);
    fakeFSRBulkReadTable.setLength((uint8_t)FSRTable::FSR_Y - (uint8_t)FSRTable::FSR1_L + 1);
  
    auto fsrData = fakeFSRBulkReadTable.getData();
    fsrData[(uint8_t)FSRTable::FSR1_H] = 
      (1 + sin(d_clock->getSeconds() * M_PI)) * 32;
    ;
    fsrData[(uint8_t)FSRTable::FSR2_H] = 
      (1 + sin(d_clock->getSeconds() * M_PI)) * 64;

    fsrData[(uint8_t)FSRTable::FSR3_H] = 
      (1 + sin(d_clock->getSeconds() * M_PI)) * 96;

    fsrData[(uint8_t)FSRTable::FSR4_H] = 
      (1 + sin(d_clock->getSeconds() * M_PI)) * 128;

    fsrData[(uint8_t)FSRTable::FSR_X] =
      (1 + sin(d_clock->getSeconds() * M_PI)) * 127;
    fsrData[(uint8_t)FSRTable::FSR_Y] =
      (1 + cos(d_clock->getSeconds() * M_PI)) * 127;

    State::make<FSRState>(fakeFSRBulkReadTable,
                          fakeFSRBulkReadTable,
                          d_fsrModel);
  }

  //
  // Dummy HardwareState
  //

  auto cm730State = new CM730Snapshot();
  cm730State->acc = Eigen::Vector3d(0, 0, 5);
  cm730State->accRaw = Eigen::Vector3i(512, 512, 768);
  cm730State->eyeColor = d_ledControl->getEyeColour().toRgbUnitVector();
  cm730State->foreheadColor = d_ledControl->getForeheadColour().toRgbUnitVector();
  cm730State->gyro = Eigen::Vector3d(0, 0, 0);
  cm730State->gyroRaw = Eigen::Vector3i(512, 512, 512);
  cm730State->isLed2On = d_ledControl->isRedPanelLedLit();
  cm730State->isLed3On = d_ledControl->isBluePanelLedLit();
  cm730State->isLed4On = d_ledControl->isGreenPanelLedLit();
  cm730State->isModeButtonPressed = false;
  cm730State->isPowered = true;
  cm730State->isStartButtonPressed = false;
  cm730State->voltage = 12.3f;

  vector<unique_ptr<MX28Snapshot const>> mx28States;

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    auto const& jointControl = d_bodyControl->getJoint((JointId)jointId);
    double rads = jointControl.getRadians();

    auto mx28State = new MX28Snapshot(jointId);
    mx28State->presentLoad = 0;
    mx28State->presentPosition = rads;
    mx28State->presentPositionValue = MX28::rads2Value(rads);
    mx28State->presentSpeedRPM = 0;
    mx28State->presentTemp = 40;
    mx28State->presentVoltage = 12;
    mx28States.push_back(unique_ptr<MX28Snapshot const>(mx28State));
  }

  auto hw = make_shared<HardwareState const>(unique_ptr<CM730Snapshot const>(cm730State), move(mx28States), 0, 0, getCycleNumber());

  //
  // Dummy AccelerometerState
  //
  
  State::make<AccelerometerState>(cm730State->accRaw, d_accelerometerModel);

  t.timeEvent("Update HardwareState (Dummy)");
  return hw;
}

void MotionLoop::writeByteWithRetry(BulkReadTable const& table, uint8_t jointId, MX28Table address, uint8_t value, size_t retryCount)
{
  // Don't write anything if the value is already set correctly
  if (table.readByte(address) == value)
    return;

  Log::info("MotionLoop::initialiseHardwareTables")
    << "Writing value " << (int)value
    << " to EEPROM address '" << MX28::getAddressName(address)
    << "' of " << JointName::getEnumName(jointId);

  MX28Alarm error;
  auto res = d_cm730->writeByteWithRetry(jointId, address, value, retryCount, &error);
  if (res == CommResult::SUCCESS)
  {
    if (error.hasError())
      Log::error("MotionLoop::initialiseHardwareTables")
        << "Error reported by " << JointName::getEnumName(jointId)
        << " when writing value " << value
        << " to " << MX28::getAddressName(address) << ": " << error;
  }
  else
  {
    Log::error("MotionLoop::initialiseHardwareTables")
      << "Communication problem writing " << MX28::getAddressName(address)
      << " to " << JointName::getEnumName(jointId)
      << " after " << retryCount << " retries: " << getCommResultName(res);
  }
}

void MotionLoop::writeWordWithRetry(BulkReadTable const& table, uint8_t jointId, MX28Table address, uint16_t value, size_t retryCount)
{
  // Don't write anything if the value is already set correctly
  if (table.readWord(address) == value)
  {
    Log::verbose("Motionloop::initialiseHardwareTables") << "Value " << (int)value << " already set correctly at EEPROM address '" << MX28::getAddressName(address) << "' of " << JointName::getEnumName(jointId);
    return;
  }

  Log::info("MotionLoop::initialiseHardwareTables") << "Writing value " << (int)value << " to EEPROM address '" << MX28::getAddressName(address) << "' of " << JointName::getEnumName(jointId);

  MX28Alarm error;
  auto res = d_cm730->writeWordWithRetry(jointId, address, value, retryCount, &error);
  if (res == CommResult::SUCCESS)
  {
    if (error.hasError())
      Log::error("MotionLoop::initialiseHardwareTables")
        << "Error reported by " << JointName::getEnumName(jointId)
        << " when writing value " << value
        << " to " << MX28::getAddressName(address)
        << ": " << error;
  }
  else
  {
    Log::error("MotionLoop::initialiseHardwareTables")
      << "Communication problem writing " << MX28::getAddressName(address)
      << " to " << JointName::getEnumName(jointId)
      << " after " << retryCount
      << " retries: " << getCommResultName(res);
  }
}

bool MotionLoop::ping(uint8_t jointId)
{
  MX28Alarm error;
  CommResult res = d_cm730->ping(jointId, &error);

  if (res != CommResult::SUCCESS)
  {
    Log::error("MotionLoop::initialiseHardwareTables") << "Communication problem pinging " << JointName::getEnumName(jointId) << ": " << getCommResultName(res);
    return false;
  }

  if (error.hasError())
  {
    Log::error("MotionLoop::initialiseHardwareTables") << "Error when pinging " << JointName::getEnumName(jointId) << ": " << error;
    return false;
  }

  return true;
}
