#!/usr/bin/env bash

if [[ $1 == "help" ]] ; then
    echo "   bh set-network NETWORK N [M...]"
    echo "          set wireless network settings to those in interfaces.d/NETWORK"
    exit
fi

if [[ $# -eq 0 ]] ; then
   echo "Must specify a network and at least one robot number"
   exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo "Must specify at least one robot number"
    exit 1
fi

source ${BOLD_HUMANOID_DIR}/bh.d/_functions

set_network()
{
    echo "Setting network..." &&
    ssh darwin@$(darwin_hostname $2) "echo $1 > network" &&
    echo "Restarting network..." &&
    ssh darwin@$(darwin_hostname $2) "sudo ifdown wlan0 && sudo ifup wlan0" &&
    echo "Restarting Avahi..." &&
    ssh darwin@$(darwin_hostname $2) "sudo service avahi-daemon restart"

    if [ $? -eq 0 ]; then
        report_success $2
    else
        report_failure $2
    fi
}

export -f set_network

set_network $1 $2

# parallel -j0 set_network $1 ::: ${@:2}
