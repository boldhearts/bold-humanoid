#!/usr/bin/env bash

if [[ $1 == "help" ]] ; then
    echo "   bh deploy [TAG] N [M...]"
    echo "          Deploy all necessary files to the target robot(s), with optional tag"
    exit
fi

if [[ $# -eq 0 ]] ; then
    echo "Must specify at least one robot number"
    exit 1
fi

re='^[0-9]+$'

if [[ $1 =~ $re ]]; then
    DIR="."
    DARWINS=${@:1}
elif [ "$1" = "test" ]; then
    DIR="."
    DARWINS="test"
else
    DIR="deployment-$1"
    DARWINS=${@:2}
fi

# load common functions
if [ ! -f ${BOLD_HUMANOID_DIR}/bh.d/_functions ]; then
    echo "BOLD_HUMANOID_DIR" not set
    exit 1
fi
source ${BOLD_HUMANOID_DIR}/bh.d/_functions

remote_deploy()
{
    RSYNC_ARGS=$1
    DIR=$2
    DARWIN=$3
    remote_say $DARWIN "Starting full deployment"

    if [ "$DARWIN" = "test" ]; then
        DIR=""
        HOST=/tmp/deploy
        mkdir -p /tmp/deploy:
    else
        HOST=darwin@$(darwin_hostname $DARWIN)
        ssh $HOST mkdir -p $DIR
        ssh $HOST "[ -f $DIR/configuration-agent.json ] || cp configuration-agent.json $DIR/configuration-agent.json"
    fi

    rsync $RSYNC_ARGS --recursive --delete   \
        www/dist/                                   \
        $HOST:$DIR/www                              \
    &&                                              \
    rsync $RSYNC_ARGS --recursive --relative $(deployment_files) $HOST:$DIR/

    if [ $? -eq 0 ]; then
        if [ "$DARWIN" != "test" ]; then
            ssh $HOST "echo $DIR > active-deployment"
        fi
        remote_say $DARWIN "Full deployment complete"
        report_result $DARWIN 0
    else
        report_result $DARWIN 1
    fi
}

export -f remote_deploy

if [[ $DARWINS =~ $re ]]; then
    remote_deploy "--progress --compress" $DIR $DARWINS
else
    parallel -j0 remote_deploy "--quiet --compress" $DIR ::: $DARWINS
fi
