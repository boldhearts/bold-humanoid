// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/util/Maybe/maybe.hh"
#include "JointId/jointid.hh"
#include <map>

namespace bold
{
  /** Collection of forward and inverse kinematics functions
   */
  class Kinematics
  {
  public:
    /** Inverse kinematics to calculate joint anggles to achieve foot position
     *
     * @param pos target position for the foot, relative to foot
     * position with fully stretched leg, i.e. when all joint angles
     * are 0, in mm
     * 
     * @param angles target pitch, roll and yaw angels of the foot,
     * relative to the torso
     * 
     * @returns maybe the resulting target angles for the 6 leg
     * joints, or an empty value if no solution is found
     */
    static util::Maybe<Eigen::Matrix<double, 6, 1>> legAnglesForTarget(Eigen::Vector3d const& pos, Eigen::Vector3d const& angels);

    static util::Maybe<std::map<JointId, double>> targetAnglesForStandingWithOffsets(Eigen::Vector3d const& offsets, double hipPitch);

  private:
    static constexpr double THIGH_LENGTH = 93.0; // mm
    static constexpr double CALF_LENGTH = 93.0; // mm
    static constexpr double ANKLE_LENGTH = 33.5; // mm
    static constexpr double LEG_LENGTH = THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH;
  };
}
