// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "kinematics.ih"

constexpr double Kinematics::THIGH_LENGTH;
constexpr double Kinematics::CALF_LENGTH;
constexpr double Kinematics::ANKLE_LENGTH;
constexpr double Kinematics::LEG_LENGTH;

Maybe<Matrix<double, 6, 1>> Kinematics::legAnglesForTarget(Vector3d const& pos, Vector3d const& angles)
{
  using MaybeMatrix = Maybe<Matrix<double, 6, 1>>;

  Matrix<double, 6, 1> out;

  // bodyFoot = bodyFootCent0 * footCent0FootCentR
  auto hipFootTr = Translation3d(pos - Vector3d(0, 0, LEG_LENGTH)) *
    AngleAxisd(angles.x(), Vector3d::UnitX()) *
    AngleAxisd(angles.y(), Vector3d::UnitY()) *
    AngleAxisd(angles.z(), Vector3d::UnitZ());

  auto hipFootMat = hipFootTr.matrix();
  Vector3d hipToAnkleVec(pos.x() + hipFootMat(0, 2) * ANKLE_LENGTH,
                         pos.y() + hipFootMat(1, 2) * ANKLE_LENGTH,
                         (pos.z() - LEG_LENGTH + hipFootMat(2, 2) * ANKLE_LENGTH));


  // Get Knee
  auto kneeAngle = Math::triangleAngle(hipToAnkleVec.norm(), THIGH_LENGTH, CALF_LENGTH, false);
  if (!kneeAngle.hasValue())
    return MaybeMatrix::empty();

  out(3) = kneeAngle.value();

  // Get Ankle Roll
  if (hipFootTr.matrix().determinant() == 0)
    return MaybeMatrix::empty();

  auto footHipTr = hipFootTr.inverse();
  auto footToHipVec = footHipTr.translation();
  
  auto fhLen = footToHipVec.tail<2>().norm();
  auto ahLen = (footToHipVec.tail<2>() - Vector2d(0.0, ANKLE_LENGTH)).norm();
  auto ankleRollAngle = Math::triangleAngle(fhLen, ahLen, ANKLE_LENGTH);
  out(5) = footToHipVec.y() < 0.0 ? -ankleRollAngle.value() : ankleRollAngle.value();

  // Get Hip Yaw
  auto ankleFootTr = Translation3d(0, 0, -ANKLE_LENGTH) *
                              AngleAxisd(out(5), Vector3d::UnitX());
  if (ankleFootTr.matrix().determinant() == 0.0)
    return MaybeMatrix::empty();
  auto footAnkleTr = ankleFootTr.inverse();

  auto hipAnkleTr = hipFootTr * footAnkleTr;
  auto hipAnkleMat = hipAnkleTr.matrix();
  auto hipYawAngle = std::atan2(-hipAnkleMat(0, 1), hipAnkleMat(1, 1));
  if (std::isinf(hipYawAngle))
    return MaybeMatrix::empty();
  out(0) = hipYawAngle;

  // Get Hip Roll
  auto hipRollAngle = std::atan2(hipAnkleMat(2, 1),
                                 -hipAnkleMat(0, 1) * sin(hipYawAngle) + hipAnkleMat(1, 1) * cos(hipYawAngle));
  if (std::isinf(hipRollAngle))
    return MaybeMatrix::empty();
  out(1) = hipRollAngle;


  // Get Hip Pitch and Ankle Pitch
  double theta = std::atan2(hipAnkleMat(0, 2) * cos(hipYawAngle) + hipAnkleMat(1, 2) * sin(hipYawAngle),
                            hipAnkleMat(0, 0) * cos(hipYawAngle) + hipAnkleMat(1, 0) * sin(hipYawAngle));
  if (std::isinf(theta))
    return MaybeMatrix::empty();

  auto k = sin(kneeAngle.value()) * CALF_LENGTH;
  auto l = -THIGH_LENGTH - cos(kneeAngle.value()) * CALF_LENGTH;
  auto m = cos(hipYawAngle) * hipToAnkleVec.x() + sin(hipYawAngle) * hipToAnkleVec.y();
  auto n = cos(hipRollAngle) * hipToAnkleVec.z() + sin(hipYawAngle) * sin(hipRollAngle) * hipToAnkleVec.x() -
                              cos(hipYawAngle) * sin(hipRollAngle) * hipToAnkleVec.y();
  auto s = (k * n + l * m) / (k * k + l * l);
  auto _c = (n - k * s) / l;

  auto hipPitchAngle = std::atan2(s, _c);
  if (std::isinf(hipPitchAngle))
    return MaybeMatrix::empty();
  out(2) = hipPitchAngle;
  out(4) = theta - kneeAngle.value() - hipPitchAngle;

  return make_maybe(out);
}

