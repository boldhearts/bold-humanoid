// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "kinematics.ih"

Maybe<std::map<JointId, double>> Kinematics::targetAnglesForStandingWithOffsets(Eigen::Vector3d const& offsets, double hipPitch)
{
  auto angles = map<JointId, double>{};
  angles[JointId::R_SHOULDER_PITCH] = Math::degToRad(-45);
  angles[JointId::L_SHOULDER_PITCH] = Math::degToRad(45);
  angles[JointId::R_SHOULDER_ROLL] = Math::degToRad(-17);
  angles[JointId::L_SHOULDER_ROLL] = Math::degToRad(17);
  angles[JointId::R_ELBOW] = Math::degToRad(29);
  angles[JointId::L_ELBOW] = Math::degToRad(-29);

  auto maybeLegAngles = Kinematics::legAnglesForTarget(offsets, Vector3d::Zero());
  if (!maybeLegAngles)
  {
    Log::warning("StandModule::applyLegs") << "No solution for offsets " << offsets.transpose();
    return util::Maybe<map<JointId, double>>::empty();
  }

  for (auto const& legAngles : maybeLegAngles)
  {
    angles[JointId::L_HIP_YAW] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_HIP_YAW] *
      legAngles(0);

    angles[JointId::L_HIP_ROLL] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_HIP_ROLL] *
      legAngles(1);

    angles[JointId::L_HIP_PITCH] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_HIP_PITCH] *
      (hipPitch + legAngles(2));

    angles[JointId::L_KNEE] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_KNEE] *
      legAngles(3);

    angles[JointId::L_ANKLE_PITCH] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_ANKLE_PITCH] *
      legAngles(4);

    angles[JointId::L_ANKLE_ROLL] = 
      DarwinBodyModel::jointAxisDirections[JointId::L_ANKLE_ROLL] *
      legAngles(5);


    angles[JointId::L_HIP_YAW] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_HIP_YAW] *
      legAngles(0);

    angles[JointId::R_HIP_ROLL] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_HIP_ROLL] *
      legAngles(1);

    angles[JointId::R_HIP_PITCH] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_HIP_PITCH] *
      (hipPitch + legAngles(2));

    angles[JointId::R_KNEE] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_KNEE] *
      legAngles(3);

    angles[JointId::R_ANKLE_PITCH] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_ANKLE_PITCH] *
      legAngles(4);

    angles[JointId::R_ANKLE_ROLL] = 
      DarwinBodyModel::jointAxisDirections[JointId::R_ANKLE_ROLL] *
      legAngles(5);
  }
  return make_maybe(angles);  
}

