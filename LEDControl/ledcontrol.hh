// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "colour/colour.hh"
#include "colour/BGR/bgr.hh"
#include "config/Config/config.hh"

namespace bold
{
  class LEDControl
  {
  public:
    LEDControl()
    : d_isPanelLedDirty(false),
      d_isEyeDirty(false),
      d_isForeheadDirty(false),
      d_panelLedByte(0),
      d_eyeColourShort(0),
      d_foreheadColourShort(0),
      d_eyeColour(0,0,0),
      d_foreheadColour(0,0,0)
    {}

    bool isDirty() const { return d_isPanelLedDirty || d_isEyeDirty || d_isForeheadDirty; }
    bool isPanelLedDirty() const { return d_isPanelLedDirty; }
    bool isEyeDirty()      const { return d_isEyeDirty; }
    bool isForeheadDirty() const { return d_isForeheadDirty; }

    void clearDirtyFlags()
    {
      d_isPanelLedDirty = false;
      d_isEyeDirty = false;
      d_isForeheadDirty = false;
    }

    bool isRedPanelLedLit()   const { return (d_panelLedByte & 1) != 0; }
    bool isBluePanelLedLit()  const { return (d_panelLedByte & 2) != 0; }
    bool isGreenPanelLedLit() const { return (d_panelLedByte & 4) != 0; }

    colour::BGR getEyeColour()      const { return d_eyeColour; }
    colour::BGR getForeheadColour() const { return d_foreheadColour; }

    uint16_t getEyeColourShort()      const { return d_eyeColourShort; }
    uint16_t getForeheadColourShort() const { return d_foreheadColourShort; }
    uint8_t  getPanelLedByte()        const { return d_panelLedByte; }

    void setPanelLedStates(bool red, bool blue, bool green)
    {
      static auto enabledSetting = config::Config::getSetting<bool>("hardware.leds.enable-panel");

      uint8_t ledFlags = 0;

      if (enabledSetting->getValue())
      {
        if (red)
          ledFlags |= 1;

        if (blue)
          ledFlags |= 2;

        if (green)
          ledFlags |= 4;
      }

      if (ledFlags != d_panelLedByte)
      {
        d_isPanelLedDirty = true;
        d_panelLedByte = ledFlags;
      }
    }

    void setEyeColour(colour::BGR const& colour)
    {
      static auto enabledSetting = config::Config::getSetting<bool>("hardware.leds.enable-eyes");

      uint16_t shortValue = !enabledSetting->getValue()
        ? 0
        : (colour.r >> 3) |
          ((colour.g >> 3) << 5) |
          ((colour.b >> 3) << 10);

      if (d_eyeColourShort != shortValue)
      {
        d_isEyeDirty = true;
        d_eyeColour = colour;
        d_eyeColourShort = shortValue;
      }
    }

    void setForeheadColour(colour::BGR const& colour)
    {
      static auto enabledSetting = config::Config::getSetting<bool>("hardware.leds.enable-forehead");

      uint16_t shortValue = !enabledSetting->getValue()
        ? 0
        : (colour.r >> 3) |
          ((colour.g >> 3) << 5) |
          ((colour.b >> 3) << 10);

      if (d_foreheadColourShort != shortValue)
      {
        d_isForeheadDirty = true;
        d_foreheadColour = colour;
        d_foreheadColourShort = shortValue;
      }
    }

  private:
    bool d_isPanelLedDirty;
    bool d_isEyeDirty;
    bool d_isForeheadDirty;

    uint8_t d_panelLedByte;
    uint16_t d_eyeColourShort;
    uint16_t d_foreheadColourShort;

    colour::BGR d_eyeColour;
    colour::BGR d_foreheadColour;
  };
}
