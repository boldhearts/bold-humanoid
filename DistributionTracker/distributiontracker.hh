// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <stdexcept>
#include <cmath>

namespace bold
{
  class DistributionTracker
  {
  public:
    DistributionTracker()
    : d_sum(0),
      d_sumOfSquares(0),
      d_count(0)
    {}

    /** Returns all state of this {@link DistributionTracker}. */
    void reset() { d_sum = d_sumOfSquares = d_count = 0; };

    /** Integrates an observation into this tracker's internal state. */
    void add(double d)
    {
      if (std::isnan(d))
        throw std::runtime_error("Cannot add NaN to a DistributionTracker");
      d_sum += d;
      d_sumOfSquares += d*d;
      d_count++;
    }

    /** Returns the average of observed values. */
    double average() const
    {
      return d_count == 0 ? NAN : d_sum/d_count;
    }

    /** Returns the population variance for observed values. */
    double variance() const
    {
      double avg = average();
      return d_count == 0 ? NAN : d_sumOfSquares/d_count - avg*avg;
    }

    /** Returns the population standard deviation for observed values. */
    double stdDev() const
    {
      return sqrt(variance());
    }

    /** Returns the number of observed values. */
    unsigned count() const { return d_count; }

    /** Returns the sum of observed values. */
    double sum() const { return d_sum; }

    /** Returns the sum of squared observed values. */
    double sumOfSquares() const { return d_sumOfSquares; }

  private:
    double d_sum;
    double d_sumOfSquares;
    unsigned d_count;
  };
}
