// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>
#include <functional>
#include <memory>

#include "AgentPosition/agentposition.hh"
#include "filters/Filter/filter.hh"
#include "stats/movingaverage.hh"
#include "stats/lowpassfilter.hh"

namespace bold
{
  namespace config
  {
    template<typename> class Setting;
  }

  enum class FilterType
  {
    Particle = 0,
    Kalman = 1,
    UnscentedKalman = 2
  };

  class Localiser
  {
  public:
    Localiser();

    void update();

    AgentPosition position() const { return d_pos; }
    AgentPosition smoothedPosition() const { return d_smoothedPos; }
    double uncertainty() const { return d_uncertainty; }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  private:
    typedef Eigen::Vector4d FilterState;

    std::pair<FilterState, double> generateState();

    void predict();
    void updateSmoothedPos();
    void updateStateObject();

    bool d_haveLastAgentTransform;
    Eigen::Affine3d d_lastAgentTransform;
    Eigen::Quaterniond d_lastQuaternion;
    double d_preNormWeightSum;
    LowPassFilter d_preNormWeightSumFilter;

    bool d_shouldRandomise;

    AgentPosition d_pos;
    AgentPosition d_smoothedPos;
    MovingAverage<Eigen::Vector4d> d_avgPos;
    double d_uncertainty;

//    config::Setting<bool>* d_useLines;
//    config::Setting<int>* d_minGoalsNeeded;
    config::Setting<double>* d_defaultKidnapWeight;
    config::Setting<double>* d_penaltyKidnapWeight;
    config::Setting<bool>* d_enablePenaltyRandomise;
//    config::Setting<bool>* d_enableDynamicError;

    FilterType d_filterType;
    std::shared_ptr<Filter<4>> d_filter;

    std::function<double()> d_fieldXRng;
    std::function<double()> d_fieldYRng;
    std::function<double()> d_goalAreaXRng;
    std::function<double()> d_goalAreaYRng;
    std::function<double()> d_thetaRng;
  };
}
