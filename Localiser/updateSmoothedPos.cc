// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "localiser.ih"

void Localiser::updateSmoothedPos()
{
  double thetaX = cos(d_pos.theta());
  double thetaY = sin(d_pos.theta());
  auto smoothed = d_avgPos.next(Eigen::Vector4d(d_pos.x(), d_pos.y(), thetaX, thetaY));

  double newTheta = atan2(smoothed[3], smoothed[2]);

  d_smoothedPos = AgentPosition(smoothed.x(), smoothed.y(), newTheta);
}
