// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/geometry2/Geometry/LineSegment/linesegment.hh"
#include "src/util/Maybe/maybe.hh"

#include <memory>
#include <Eigen/Core>
#include <vector>

namespace bold
{
  class Spatialiser;

  struct LineJunction
  {
    enum class Type
    {
      X,
      T,
      L,
      NONE
    };


    LineJunction()
      : position{}, type{Type::NONE}, angle{0.0}
    {}

    LineJunction(LineJunction const& other) = default;
    
    LineJunction(geometry2::Point2d p, Type t, double a)
      : position{std::move(p)},
      type{t},
      angle{a}
    {}

    geometry2::Point2d position;
    Type type;
    double angle;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  class LineJunctionFinder
  {
  public:

    LineJunctionFinder() = default;

    std::vector<LineJunction, Eigen::aligned_allocator<LineJunction>> findLineJunctions(geometry2::LineSegment3d::Vector const& lineSegments);

    util::Maybe<LineJunction> tryFindLineJunction(geometry2::LineSegment3d const& segment1,
                                                  geometry2::LineSegment3d const& segment2,
                                                  double distToEndThreshold = 0.2);
  };
}
