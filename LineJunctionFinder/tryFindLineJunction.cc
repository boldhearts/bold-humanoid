// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "linejunctionfinder.ih"

Maybe<LineJunction> LineJunctionFinder::tryFindLineJunction(LineSegment3d const& segment1, LineSegment3d const& segment2, double distToEndThreshold)
{
  auto segment2d1 = segment1.toDim<2>();
  auto segment2d2 = segment2.toDim<2>();

  auto length1 = segment2d1.length();
  auto length2 = segment2d2.length();

  double t{-1.0};
  double u{-1.0};

  Intersection{}(segment2d1, segment2d2, &t, &u);
  
  // Lines were parallel, no crossing
  if (t < 0)
    return util::Maybe<LineJunction>::empty();

  bool on1 = t >= 0.0 && t <= 1.0;
  bool on2 = u >= 0.0 && u <= 1.0;

  auto distToEnd1 = std::min(std::abs(t), std::abs(1 - t)) * length1;
  auto distToEnd2 = std::min(std::abs(u), std::abs(1 - u)) * length2;

  bool atEnd1 = distToEnd1 < distToEndThreshold;
  bool atEnd2 = distToEnd2 < distToEndThreshold;

  LineJunction junction;
  junction.type = LineJunction::Type::NONE;

  if (on1 && on2 && !atEnd1 && !atEnd2)
    junction.type = LineJunction::Type::X;
  else if ((on1 && !atEnd1 && atEnd2) ||
           (on2 && !atEnd2 && atEnd1))
    junction.type = LineJunction::Type::T;
  else if (atEnd1 && atEnd2)
    junction.type = LineJunction::Type::L;

  if (junction.type != LineJunction::Type::NONE)
  {
    junction.position = segment2d1.p1() + segment2d1.delta() * t;
    junction.angle = segment2d1.angleBetween(segment2d2);
    return make_maybe(junction);
  }
  else
    return util::Maybe<LineJunction>::empty();
}

