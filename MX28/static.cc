// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mx28.hh"

using namespace bold;

constexpr uint16_t MX28::MIN_VALUE;
constexpr uint16_t MX28::CENTER_VALUE;
constexpr uint16_t MX28::MAX_VALUE;

constexpr uint16_t MX28::MAX_TORQUE;

constexpr double MX28::MIN_DEGS;
constexpr double MX28::MAX_DEGS;
constexpr double MX28::MIN_RADS;
constexpr double MX28::MAX_RADS;

constexpr double MX28::RATIO_VALUE2DEGS;
constexpr double MX28::RATIO_DEGS2VALUE;

constexpr double MX28::RATIO_VALUE2RADS;
constexpr double MX28::RATIO_RADS2VALUE;

constexpr double MX28::RATIO_VALUE2RPM;
constexpr double MX28::RATIO_RPM2VALUE;

constexpr double MX28::RATIO_VALUE2TORQUE;
constexpr double MX28::RATIO_TORQUE2VALUE;

