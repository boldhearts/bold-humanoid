// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mx28.hh"

#include "Math/math.hh"
#include "src/util/assert.hh"

using namespace bold;
using namespace std;

uint16_t MX28::getMirrorValue(uint16_t value)
{
  return (MAX_VALUE + 1 - value) & MAX_VALUE;
}

double MX28::getMirrorAngle(double angle)
{
  return -angle;
}

uint16_t MX28::degs2Value(double angle)
{
  return static_cast<uint16_t>(
    Math::clamp(static_cast<int>(round(angle * RATIO_DEGS2VALUE)),
                -CENTER_VALUE,
                MAX_VALUE - CENTER_VALUE) +
    CENTER_VALUE);
}

double MX28::value2Degs(uint16_t value)
{
  return static_cast<double>(static_cast<int>(value) - static_cast<int>(CENTER_VALUE)) * RATIO_VALUE2DEGS;
}

uint16_t MX28::rads2Value(double angle)
{
  return static_cast<uint16_t>(
    Math::clamp(static_cast<int>(round(angle * RATIO_RADS2VALUE)),
                -CENTER_VALUE,
                MAX_VALUE - CENTER_VALUE) +
    CENTER_VALUE);
}

double MX28::value2Rads(uint16_t value)
{
  return static_cast<double>((static_cast<int>(value) -
                              static_cast<int>(CENTER_VALUE))) * RATIO_VALUE2RADS;
}

int16_t MX28::radsDelta2ValueDelta(double angle)
{
  return static_cast<int16_t>(angle * RATIO_RADS2VALUE);
}

double MX28::valueDelta2RadsDelta(int16_t value)
{
  return static_cast<double>(static_cast<int>(value) * RATIO_VALUE2RADS);
}

uint16_t MX28::clampValue(int value)
{
  return static_cast<uint16_t>(Math::clamp(value, 0, static_cast<int>(MAX_VALUE)));
}

/*
// TODO compare these old conversions with those being used for accuracy before
deleting this commented code

double MX28::angleValueToRads(uint16_t value)
{
return ((int)value - 0x800) * (M_PI / 0x800);
}

double MX28::valueToRPM(uint16_t value)
{
// Value is between 0 & 1023 (0x3FF). Unit of 0.053 rpm.
// If zero, maximum RPM of motor is used without speed control.
// If 1023, about 54RPM (CW)
// Between 1024 * 2047, values are CCW.
// That is, bit 10 indicates the direction. Therefore both 0 and 1024 equal
zero RPM.
if (value < 1024)
return value * 0.052733333;
else
return ((int)value - 1024) * 0.052733333;
}
*/

uint16_t MX28::rpm2Value(double speed)
{
  int temp = (static_cast<int>(fabs(speed) * RATIO_RPM2VALUE)) & 0x3FF;
  if (speed < 0)
    temp |= 0x400;
  return temp;
}

double MX28::value2Rpm(uint16_t value)
{
  double temp = (value & 0x3FF) * RATIO_VALUE2RPM;
  if (value & 0x400)
    temp = -temp;
  return temp;
}

uint16_t MX28::torque2Value(double speed)
{
  int temp = (static_cast<int>(fabs(speed) * RATIO_TORQUE2VALUE)) & 0x3FF;
  if (speed < 0)
    temp |= 0x400;
  return temp;
}

double MX28::value2Torque(uint16_t value)
{
  double temp = (value & 0x3FF) * RATIO_VALUE2TORQUE;
  if (value & 0x400)
    temp = -temp;
  return temp;
}

uint8_t MX28::centigrade2Value(int degreesCentigrade)
{
  ASSERT(degreesCentigrade > 0);
  ASSERT(degreesCentigrade < 256);
  return degreesCentigrade;
}

int MX28::value2Centigrade(uint8_t value)
{
  return value;
}

uint8_t MX28::voltage2Value(double volts)
{
  ASSERT(volts > 0);
  ASSERT(volts < 25);
  return static_cast<uint8_t>(volts * 10);
}

double MX28::value2Voltage(uint8_t value)
{
  return value / 10.0;
}

double MX28::value2Load(uint16_t value)
{
  return (value < 1024 ? value : value - 1024) / 1023.0;
}

std::string MX28::getAddressName(MX28Table address)
{
  switch (address) {
  case MX28Table::MODEL_NUMBER_L:
    return "MODEL_NUMBER";
  case MX28Table::VERSION:
    return "VERSION";
  case MX28Table::ID:
    return "ID";
  case MX28Table::BAUD_RATE:
    return "BAUD_RATE";
  case MX28Table::RETURN_DELAY_TIME:
    return "RETURN_DELAY_TIME";
  case MX28Table::CW_ANGLE_LIMIT_L:
    return "CW_ANGLE_LIMIT";
  case MX28Table::CCW_ANGLE_LIMIT_L:
    return "CCW_ANGLE_LIMIT";
  case MX28Table::SYSTEM_DATA2:
    return "SYSTEM_DATA2";
  case MX28Table::LOW_LIMIT_VOLTAGE:
    return "LOW_LIMIT_VOLTAGE";
  case MX28Table::MAX_TORQUE_L:
    return "MAX_TORQUE";
  case MX28Table::RETURN_LEVEL:
    return "RETURN_LEVEL";
  case MX28Table::ALARM_LED:
    return "ALARM_LED";
  case MX28Table::ALARM_SHUTDOWN:
    return "ALARM_SHUTDOWN";
  case MX28Table::OPERATING_MODE:
    return "OPERATING_MODE";
  case MX28Table::LOW_CALIBRATION_L:
    return "LOW_CALIBRATION";
  case MX28Table::HIGH_CALIBRATION_L:
    return "HIGH_CALIBRATION";
  case MX28Table::TORQUE_ENABLE:
    return "TORQUE_ENABLE";
  case MX28Table::LED:
    return "LED";
  case MX28Table::D_GAIN:
    return "D_GAIN";
  case MX28Table::I_GAIN:
    return "I_GAIN";
  case MX28Table::P_GAIN:
    return "P_GAIN";
  case MX28Table::RESERVED:
    return "RESERVED";
  case MX28Table::GOAL_POSITION_L:
    return "GOAL_POSITION";
  case MX28Table::MOVING_SPEED_L:
    return "MOVING_SPEED";
  case MX28Table::TORQUE_LIMIT_L:
    return "TORQUE_LIMIT";
  case MX28Table::PRESENT_POSITION_L:
    return "PRESENT_POSITION";
  case MX28Table::PRESENT_SPEED_L:
    return "PRESENT_SPEED";
  case MX28Table::PRESENT_LOAD_L:
    return "PRESENT_LOAD";
  case MX28Table::PRESENT_VOLTAGE:
    return "PRESENT_VOLTAGE";
  case MX28Table::PRESENT_TEMPERATURE:
    return "PRESENT_TEMPERATURE";
  case MX28Table::REGISTERED_INSTRUCTION:
    return "REGISTERED_INSTRUCTION";
  case MX28Table::PAUSE_TIME:
    return "PAUSE_TIME";
  case MX28Table::MOVING:
    return "MOVING";
  case MX28Table::LOCK:
    return "LOCK";
  case MX28Table::PUNCH_L:
    return "PUNCH";
  case MX28Table::RESERVED4:
    return "RESERVED4";
  case MX28Table::RESERVED5:
    return "RESERVED5";
  case MX28Table::POT_L:
    return "POT";
  case MX28Table::PWM_OUT_L:
    return "PWM_OUT";
  case MX28Table::P_ERROR_L:
    return "P_ERROR";
  case MX28Table::I_ERROR_L:
    return "I_ERROR";
  case MX28Table::D_ERROR_L:
    return "D_ERROR";
  case MX28Table::P_ERROR_OUT_L:
    return "P_ERROR_OUT";
  case MX28Table::I_ERROR_OUT_L:
    return "I_ERROR_OUT";
  case MX28Table::D_ERROR_OUT_H:
    return "D_ERROR_OUT";

  default:
    return "(UNKNOWN)";
  }
}
