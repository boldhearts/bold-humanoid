// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>
#include <memory>

#include "CM730CommsModule/cm730commsmodule.hh"
#include "CM730/cm730.hh"
#include "JointId/jointid.hh"
#include "MX28Alarm/mx28alarm.hh"

namespace bold
{
  class Voice;

  class MX28HealthChecker : public CM730CommsModule
  {
  public:
    MX28HealthChecker(std::shared_ptr<Voice> voice);

    void step(std::unique_ptr<CM730>& cm730, util::SequentialTimer& t, unsigned long motionCycleNumber) override;

  private:
    std::array<bool, (uint8_t)JointId::MAX + 1> d_isTorqueEnabledByJointId;
    std::array<CommResult, (uint8_t)JointId::MAX + 1> d_commResultByJointId;
    std::array<MX28Alarm, (uint8_t)JointId::MAX + 1> d_alarmByJointId;
    std::shared_ptr<Voice> d_voice;
    uint8_t d_jointId = (uint8_t)JointId::MIN;
  };
}
