// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mx28healthchecker.hh"

#include "CM730/cm730.hh"
#include "JointId/jointid.hh"
#include "MX28/mx28.hh"
#include "MX28Alarm/mx28alarm.hh"
#include "src/util/Log/log.hh"
#include "Voice/voice.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

MX28HealthChecker::MX28HealthChecker(shared_ptr<Voice> voice)
: CM730CommsModule("MX28 Health Checker"),
  d_voice(voice),
  d_jointId((uint8_t)JointId::MIN)
{
  // Initially assume that all joints have torque enabled
  std::fill(begin(d_isTorqueEnabledByJointId), end(d_isTorqueEnabledByJointId), true);
  std::fill(begin(d_commResultByJointId), end(d_commResultByJointId), CommResult::SUCCESS);
  std::fill(begin(d_alarmByJointId), end(d_alarmByJointId), MX28Alarm());
}

void MX28HealthChecker::step(unique_ptr<CM730>& cm730, SequentialTimer& t, ulong motionCycleNumber)
{
  // Roughly four times a second
  if (motionCycleNumber % 32 != 0)
    return;

  uint8_t isTorqueEnabled;
  MX28Alarm alarm;

  CommResult res
    = d_commResultByJointId[d_jointId]
      = cm730->readByte(d_jointId, MX28Table::TORQUE_ENABLE, &isTorqueEnabled, &alarm);

  if (res == CommResult::SUCCESS)
  {
    d_alarmByJointId[d_jointId] = alarm;

    if (isTorqueEnabled != d_isTorqueEnabledByJointId[d_jointId])
    {
      // Something changed
      d_isTorqueEnabledByJointId[d_jointId] = isTorqueEnabled;

      // Make an announcement
      stringstream message;
      message << "Torque " << (isTorqueEnabled ? "enabled" : "disabled") << " for " << JointName::getNiceName(d_jointId);

      if (alarm.hasError())
        message << " with error " << alarm;

      // Log it
      Log::error("MotionLoop::step") << message.str();

      // Say it out loud too
      if (d_voice->queueLength() < 4)
        d_voice->say(message.str());
    }
  }

  // Wrap around to the next joint
  if (++d_jointId > (uint8_t)JointId::MAX)
    d_jointId = (uint8_t)JointId::MIN;

  t.timeEvent("Check Joint Health");
}
