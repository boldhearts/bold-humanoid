// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "config/Config/config.hh"
#include "Math/math.hh"

namespace bold
{
  /** Gyroscope model
   */
  class GyroModel
  {
  public:
    /** Create a gyro model from configuration
     */
    GyroModel()
      : GyroModel(config::Config::getStaticValue<double>("hardware.gyro.mdps-per-digit"),
                  config::Config::getStaticValue<double>("hardware.gyro.dps-range"),
                  config::Config::getStaticValue<int>("hardware.gyro.digital-range"))
    {}

    /** Create a gyro model with specific sensitivity and range
     * @param mDPS_per_digit Gyroscope sensitivity, in millidegrees per second per digit
     * @param measurementRange Full scale [-range, +range], in degrees per second
     * @param digitalRange Full scale [0, range) of digital readings
     */
    GyroModel(double mDPS_per_digit, double dpsRange, unsigned digitalRange)
      : d_mDPS_per_digit(mDPS_per_digit),
        d_dpsRange(dpsRange),
        d_digitalRange(digitalRange),
        d_zeroRateLevel(digitalRange / 2)
    {}

    void setZeroRateLevel(uint16_t zeroRateLevel)
    {
      d_zeroRateLevel = zeroRateLevel;
    }

    /// Convert raw gyro reading value to degrees per second
    double valueToDPS(uint16_t value) const;

    /// Convert raw gyro reading value to radians per second
    double valueToRPS(uint16_t value) const;

  private:
    double d_mDPS_per_digit;
    double d_dpsRange;
    int d_digitalRange;
    uint16_t d_zeroRateLevel;
  };

  
  inline double GyroModel::valueToDPS(uint16_t value) const
  {
    ASSERT(value < d_digitalRange);
    return Math::clamp((int{value} - d_zeroRateLevel) * d_mDPS_per_digit / 1000,
                       -d_dpsRange, d_dpsRange);
  }

  inline double GyroModel::valueToRPS(uint16_t value) const
  {
    ASSERT(value < d_digitalRange);
    return Math::degToRad(valueToDPS(value));
  }
}
