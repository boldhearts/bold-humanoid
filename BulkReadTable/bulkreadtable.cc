// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bulkreadtable.hh"

#include "src/util/assert.hh"
#include "src/util/binary.hh"

using namespace bold;
using namespace bold::util;

BulkReadTable::BulkReadTable()
: d_startAddress(0),
  d_length(0)
{
  d_table.fill(0);
}

uint8_t BulkReadTable::readByte(uint8_t address) const
{
  ASSERT((uint8_t)address >= (uint8_t)d_startAddress && (uint8_t)address < ((uint8_t)d_startAddress + d_length));

  return d_table.at(address);
}

uint16_t BulkReadTable::readWord(uint8_t address) const
{
  ASSERT(address >= d_startAddress && address < (d_startAddress + d_length - 1));

  return binary::makeWord(d_table[address], d_table[address + 1]);
}
