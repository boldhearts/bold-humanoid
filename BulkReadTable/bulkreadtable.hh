// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <array>
#include "MX28/mx28.hh"

namespace bold
{
  enum class CM730Table : uint8_t;
  enum class FSRTable : uint8_t;

  class BulkReadTable
  {
  public:
    BulkReadTable();

    inline uint8_t readByte(MX28Table address)  const { return readByte((uint8_t)address); }
    inline uint8_t readByte(CM730Table address) const { return readByte((uint8_t)address); }
    inline uint8_t readByte(FSRTable address) const { return readByte((uint8_t)address); }

    inline uint16_t readWord(MX28Table address)  const { return readWord((uint8_t)address); };
    inline uint16_t readWord(CM730Table address) const { return readWord((uint8_t)address); };
    inline uint16_t readWord(FSRTable address) const { return readWord((uint8_t)address); };

    uint8_t getStartAddress() const { return d_startAddress; }
    void setStartAddress(uint8_t address) { d_startAddress = address; }
    uint8_t getLength() const { return d_length; }
    void setLength(uint8_t length) { d_length = length; }
    uint8_t* getData() { return d_table.data(); }

  private:
    uint8_t readByte(uint8_t address) const;
    uint16_t readWord(uint8_t address) const;

    uint8_t d_startAddress;
    uint8_t d_length;
    std::array<uint8_t,(uint8_t)MX28Table::MAXNUM_ADDRESS> d_table;
  };

}
