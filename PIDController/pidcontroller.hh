// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>

namespace bold
{
  class PIDController
  {
  public:
    PIDController(double pGain, double iGain, double dGain)
      : d_pGain{pGain},
      d_iGain{iGain},
      d_dGain{dGain},
      d_iTerm{0.0},
      d_lastError{0.0},
      d_iTermAbsMax{1e6}
    {}

    void setPGain(double pGain) { d_pGain = pGain; }
    double getPGain() const { return d_pGain; }

    void setIGain(double iGain) { d_iGain = iGain; }
    double getIGain() const { return d_iGain; }

    void setDGain(double dGain) { d_dGain = dGain; }
    double getDGain() const { return d_dGain; }

    void setGains(double pGain, double iGain, double dGain);
    std::array<double, 3> getGains() const { return {{d_pGain, d_iGain, d_dGain}}; }

    void setIntegralAbsMax(double max) { d_iTermAbsMax = max; }

    double getIntegral() const { return d_iTerm; }

    double step(double error);

    void reset() { d_iTerm = d_lastError = 0.0; }

  private:
    double d_pGain;
    double d_iGain;
    double d_dGain;

    double d_iTerm;
    double d_lastError;

    double d_iTermAbsMax;
  };

  inline void PIDController::setGains(double pGain, double iGain, double dGain)
  {
    d_pGain = pGain;
    d_iGain = iGain;
    d_dGain = dGain;
  }
}
