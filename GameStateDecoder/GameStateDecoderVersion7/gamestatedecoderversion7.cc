// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "gamestatedecoderversion7.hh"

#include "state/StateObject/GameState/gamestate.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

constexpr int PlayerCount = 11;

shared_ptr<GameState const> GameStateDecoderVersion7::decode(BufferReader &reader) const {
  auto game = make_shared<GameState>();

  // In version 7, the packet number was 32 bit. The caller reads the first only (LSB), so
  // we must skip the remaining three.
  reader.skip(3);

  game->d_version = 7;
  game->d_packetNumber = 0;
  game->d_secondaryTime = 0;

  game->d_playersPerTeam = reader.read<uint8_t>();
  game->d_playMode = (PlayMode) reader.read<uint8_t>();
  game->d_isFirstHalf = (bool) reader.read<uint8_t>();
  game->d_nextKickOffTeamIndex = (bool) reader.read<uint8_t>();
  game->d_periodType = (PeriodType) reader.read<uint8_t>();
  game->d_lastDropInTeamColorNumber = reader.read<uint8_t>();
  game->d_secondsSinceLastDropIn = reader.read<uint16_t>();
  game->d_secondsRemaining = (int16_t) reader.read<uint32_t>();

  auto readTeam = [&]() -> TeamData {
    TeamData team;

    team.d_penaltyShot = 0;
    team.d_singleShots = 0;

    team.d_teamNumber = reader.read<uint8_t>();
    team.d_teamColour = (TeamColor) reader.read<uint8_t>();

    // Skip the goal colour, as goals are the same colour nowadays
    reader.skip(1);

    team.d_score = reader.read<uint8_t>();

    vector<PlayerData> players;
    players.resize(game->d_playersPerTeam);
    for (uint8_t i = 0; i < game->d_playersPerTeam; i++) {
      const PenaltyType penaltyType = (PenaltyType) reader.read<uint16_t>();
      const uint8_t secondsUntilPenaltyLifted = (uint8_t) reader.read<uint16_t>();
      players[i] = PlayerData(penaltyType, secondsUntilPenaltyLifted);
    }
    team.d_players = move(players);

    const size_t skipCount = (size_t) ((PlayerCount - game->d_playersPerTeam) * 4);
    reader.skip(skipCount);

    return team;
  };

  game->d_team1 = readTeam(); // Blue
  game->d_team2 = readTeam(); // Red

  return game;
}
