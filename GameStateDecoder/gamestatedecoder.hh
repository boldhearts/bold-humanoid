// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <memory>

#include "src/util/BufferReader/bufferreader.hh"

namespace bold {
  namespace state {
    class GameState;
  }

  class GameStateDecoder {
  protected:
    GameStateDecoder(uint8_t version, uint32_t messageSize)
        : d_version(version),
          d_messageSize(messageSize) {}

  public:
    uint8_t getSupportedVersion() const { return d_version; }

    uint32_t getMessageSize() const { return d_messageSize; }

    virtual std::shared_ptr<state::GameState const> decode(util::BufferReader &reader) const = 0;

  private:
    uint8_t d_version;
    uint32_t d_messageSize;
  };
}
