// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "BehaviourControl/behaviourcontrol.hh"
#include "config/Config/config.hh"
#include "Option/FSMOption/fsmoption.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/GameState/gamestate.hh"
#include "state/StateObject/TeamState/teamstate.hh"
#include "util/conditionals.hh"

namespace bold
{
  //
  // Utility functions for setting status flags as we enter states
  //

  auto setPlayerActivityInStates = [](Agent* agent, state::PlayerActivity activity, std::vector<std::shared_ptr<FSMState>> states)
  {
    for (std::shared_ptr<FSMState>& state : states)
      state->onEnter.connect([agent,activity] { agent->getBehaviourControl()->setPlayerActivity(activity); });
  };

  auto setPlayerStatusInStates = [](Agent* agent, state::PlayerStatus status, std::vector<std::shared_ptr<FSMState>> states)
  {
    for (std::shared_ptr<FSMState>& state : states)
      state->onEnter.connect([agent,status] { agent->getBehaviourControl()->setPlayerStatus(status); });
  };

  //
  // CONDITIONALS
  //

  // TODO any / all / true functions

  // GENERAL FUNCTIONS

  auto ballVisibleCondition = []
  {
    return state::State::get<state::CameraFrameState>()->isBallVisible();
  };

  auto ballNotVisibleCondition = []
  {
    return state::State::get<state::CameraFrameState>()->isBallVisible();
  };

  auto ballTooFarToKick = []
  {
    // TODO use filtered ball position
    auto ballObs = state::State::get<state::AgentFrameState>()->getBallObservation();
    static auto maxKickDistance = config::Config::getSetting<double>("kick.max-ball-distance");
    return ballObs && ballObs->head<2>().norm() > maxKickDistance->getValue();
  };

  // TODO review this one-size-fits-all approach on a case-by-case basis below
  auto ballFoundConditionFactory = [](std::shared_ptr<Clock> clock)
  {
    return util::trueForMillis(1000, clock, ballVisibleCondition);
  };
  auto ballLostConditionFactory = [](std::shared_ptr<Clock> clock)
  {
    return util::trueForMillis(1000, clock, util::negate(ballVisibleCondition));
  };

  auto isPenalised = []
  {
    auto gameState = state::State::get<state::GameState>();
    return gameState && gameState->getMyPlayerInfo().hasPenalty();
  };

  auto nonPenalisedPlayMode = [](state::PlayMode playMode)
  {
    return [playMode]
    {
      auto gameState = state::State::get<state::GameState>();
      return gameState && !gameState->getMyPlayerInfo().hasPenalty() && gameState->getPlayMode() == playMode;
    };
  };

  auto ballIsStoppingDistance = []
  {
    // Approach ball until we're within a given distance
    // TODO use filtered ball position
    auto ballObs = state::State::get<state::AgentFrameState>()->getBallObservation();
    static auto stoppingDistance = config::Config::getSetting<double>("options.approach-ball.stop-distance");
    return ballObs && (ballObs->head<2>().norm() < stoppingDistance->getValue());
  };
}
