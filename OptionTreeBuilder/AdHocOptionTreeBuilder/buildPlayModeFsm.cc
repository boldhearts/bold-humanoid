// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "Option/GameOver/gameover.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/StaticBalance/staticbalance.hh"
#include "agent/ButtonObserver/buttonobserver.hh"
#include "conditionals.hh"

#include "motion/modules/HeadModule/headmodule.hh"

using namespace bold;
using namespace bold::state;
using namespace std;

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildPlayModeFsm(Agent* agent,
                                                               shared_ptr<Clock> clock,
                                                               shared_ptr<Option> whilePlayingOption)
{
  // OPTIONS

  auto sit =
    Option::make_shared<MotionScriptOption>("sit-down-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/sit-down.json", true);
  auto standUp =
    Option::make_shared<MotionScriptOption>("stand-up-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-ready-auto.json", true);
  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto gameOver =
    Option::make_shared<GameOver>("game-over",
                                  agent->getMotionScriptModule(),
                                  agent->getVoice());
  auto stopAndSitSequence =
    Option::make_shared<SequenceOption>("stop-then-sit-sequence",
                                        Option::OptionVector{{ stopWalking, sit }});
  auto staticBalance =
    Option::make_shared<StaticBalance>("static-balance",
                                       agent->getStandModule());

  auto pauseSequence =
    Option::make_shared<SequenceOption>("pause-sequence",
                                        Option::OptionVector{{ stopWalking, standUp, staticBalance }},
                                        SequenceOption::REPEAT_LAST);
  auto stopThenGameOverSequence =
    Option::make_shared<SequenceOption>("pause-stop-then-game-over-sequence",
                                        Option::OptionVector{{ stopWalking, standUp, gameOver, sit }});

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "play-mode");

  // STATES

  auto initialState = fsm->newState("initial", { stopAndSitSequence });
  auto readyState = fsm->newState("ready", { stopAndSitSequence }, false, true);
  auto setState = fsm->newState("set", { pauseSequence });
  auto playingState = fsm->newState("playing", { whilePlayingOption });
  auto penalisedState = fsm->newState("penalised", { stopAndSitSequence });
  auto unpenalisedState = fsm->newState("unpenalised", { whilePlayingOption });
  auto finishedState = fsm->newState("finished", { stopThenGameOverSequence });

  // STATUSES

  // In the Win FSM, any state other than 'playing' corresponds to the 'waiting' activity.
  setPlayerActivityInStates(agent, PlayerActivity::Waiting, { initialState, readyState, setState, penalisedState, });

  // In the Win FSM, any state other than 'playing' and 'penalised' corresponds to the 'inactive' status.
  setPlayerStatusInStates(agent, PlayerStatus::Inactive, { initialState, readyState, setState, });

  setPlayerStatusInStates(agent, PlayerStatus::Penalised, { penalisedState });

  initialState    ->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::INITIAL);   agent->getHeadModule()->moveToHome(); });
  readyState      ->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::READY);     agent->getHeadModule()->moveToHome(); });
  setState        ->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::SET);       agent->getHeadModule()->moveToHome(); });
  playingState    ->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::PLAYING);                                         });
  unpenalisedState->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::PLAYING);                                         });
  finishedState   ->onEnter.connect([agent] { agent->getBehaviourControl()->setPlayMode(PlayMode::FINISHED);                                        });
  penalisedState  ->onEnter.connect([agent] {                                                                 agent->getHeadModule()->moveToHome(); });

  // TRANSITIONS

  //
  // PLAY MODE BUTTON
  //

  shared_ptr<ButtonTracker> modeButton = agent->getButtonObserver()->track(Button::Left);

  // TODO allow manual override of all states even when GC present

  initialState
    ->transitionTo(readyState, "left-button")
    ->when([modeButton, clock] { return !State::get<GameState>() && modeButton->isPressedForMillis(80, *clock); });

  readyState
    ->transitionTo(setState, "left-button")
    ->when([modeButton, clock] { return !State::get<GameState>() && modeButton->isPressedForMillis(80, *clock); });

  setState
    ->transitionTo(playingState, "left-button")
    ->when([modeButton, clock] { return !State::get<GameState>() && modeButton->isPressedForMillis(80, *clock); });

  penalisedState
    ->transitionTo(unpenalisedState, "left-button")
    ->when([modeButton, clock] { return !State::get<GameState>() && modeButton->isPressedForMillis(80, *clock); });

  finishedState
    ->transitionTo(initialState, "left-button")
    ->when([modeButton, clock] { return !State::get<GameState>() && modeButton->isPressedForMillis(80, *clock); });

  //
  // GAME CONTROLLER PLAY MODE
  //

  fsm
    ->wildcardTransitionTo(initialState, "gc-initial")
    ->when(nonPenalisedPlayMode(PlayMode::INITIAL));

  fsm
    ->wildcardTransitionTo(readyState, "gc-ready")
    ->when(nonPenalisedPlayMode(PlayMode::READY));

  fsm
    ->wildcardTransitionTo(setState, "gc-set")
    ->when(nonPenalisedPlayMode(PlayMode::SET));

  fsm
    ->wildcardTransitionTo(playingState, "gc-playing")
    ->when(nonPenalisedPlayMode(PlayMode::PLAYING));

  fsm
    ->wildcardTransitionTo(penalisedState, "gc-penalised")
    ->when([fsm,unpenalisedState] { return isPenalised() && fsm->getCurrentState() != unpenalisedState; });

  fsm
    ->wildcardTransitionTo(setState, "gc-set")
    ->when(nonPenalisedPlayMode(PlayMode::SET));

  fsm
    ->wildcardTransitionTo(finishedState, "gc-finished")
    ->when(nonPenalisedPlayMode(PlayMode::FINISHED));

  return fsm;
}
