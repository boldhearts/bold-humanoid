// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Option/FSMOption/fsmoption.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/WaitForWhistle/waitforwhistle.hh"

using namespace bold;
using namespace Eigen;
using namespace std;

/// The robot will approach the ball then circle it
shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildWhistleListenerFsm(Agent* agent, shared_ptr<Clock> clock)
{
  // OPTIONS

  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto standStraight =
    Option::make_shared<MotionScriptOption>("stand-straight",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-straight.json");
  auto signal =
    Option::make_shared<MotionScriptOption>("signal",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/signal-left-side.json");
  auto waitForWhistle =
    Option::make_shared<WaitForWhistle>("wait-for-whistle");

  auto stopAndStand =
    Option::make_shared<SequenceOption>("stop-and-stand",
                                       Option::OptionVector{{ stopWalking, standStraight }});
  
  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "whistle-listener");

  // STATES

  auto standState = fsm->newState("stand", { stopAndStand }, false/*endState*/, true/*startState*/);
  auto waitForWhistleState = fsm->newState("wait-for-whistle", { waitForWhistle });
  auto signalState = fsm->newState("signal", { signal });

  // TRANSITIONS

  standState
    ->transitionTo(waitForWhistleState, "standing")
    ->whenTerminated();

  // start approaching the ball when we have the confidence that it's really there
  waitForWhistleState
    ->transitionTo(signalState, "heard")
    ->whenTerminated();

  // stop walking to ball once we're close enough
  signalState
    ->transitionTo(standState, "reset")
    ->after(chrono::seconds(2), clock);

  return fsm;
}
