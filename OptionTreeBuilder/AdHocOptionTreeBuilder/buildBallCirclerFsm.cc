// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Option/ApproachBall/approachball.hh"
#include "Option/CircleBall/circleball.hh"
#include "Option/FSMOption/fsmoption.hh"
#include "Option/LocateBall/locateball.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "conditionals.hh"

using namespace bold;
using namespace bold::util;
using namespace Eigen;
using namespace std;

/// The robot will approach the ball then circle it
shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildBallCirclerFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // OPTIONS

  auto standUp =
    Option::make_shared<MotionScriptOption>("stand-up",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-ready-auto.json");
  auto locateBall =
    Option::make_shared<LocateBall>("locate-ball",
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    clock,
                                    LookAround::speedIfBallVisible(0.15));
  auto lookAtBall =
    Option::make_shared<LookAtBall>("look-at-ball",
                                    agent->getCameraModel(),
                                    agent->getHeadModule());
  auto approachBall =
    Option::make_shared<ApproachBall>("approach-ball",
                                      agent->getWalkModule(),
                                      agent->getBehaviourControl(),
                                      draw);
  auto circleBall =
    Option::make_shared<CircleBall>("circle-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    draw);
  auto stopWalking  =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "ball-circler");

  // STATES

  auto standUpState = fsm->newState("stand-up", { standUp }, false/*endState*/, true/*startState*/);
  auto locateBallState = fsm->newState("locate-ball", { locateBall });
  auto approachBallState = fsm->newState("approach-ball", { approachBall, lookAtBall });
  auto atBallState = fsm->newState("at-ball", { stopWalking });
  auto circleBallState = fsm->newState("circle-ball", { circleBall });
  auto doneState = fsm->newState("done", { stopWalking });

  // TRANSITIONS

  standUpState
    ->transitionTo(locateBallState, "standing")
    ->whenTerminated();

  // start approaching the ball when we have the confidence that it's really there
  locateBallState
    ->transitionTo(approachBallState, "found-ball")
    ->when([] { return stepUpDownThreshold(10, ballVisibleCondition); });

  // stop walking to ball once we're close enough
  approachBallState
    ->transitionTo(atBallState, "near-ball")
    ->when(ballIsStoppingDistance);

  approachBallState
    ->transitionTo(locateBallState, "lost-ball")
    ->when([clock]{ return ballLostConditionFactory(clock); });

  atBallState
    ->transitionTo(circleBallState, "start-circling")
    ->when([approachBallState,circleBall, clock]
      {
        if (approachBallState->timeSinceStart(*clock) >= std::chrono::seconds(3))
        {
          static auto rng = Math::createUniformRng(0, M_PI, true);
          double rads = rng();
          Vector2d pos = rng() > (M_PI/2.0) ? Vector2d(-0.065, 0.106) : Vector2d(0.065, 0.106);
          Log::info("ballCircler") << "Turning " << Math::radToDeg(rads) << " degrees with ball position " << pos.x() << "," << pos.y();
          circleBall->setTurnParams(rads, pos);
          return true;
        }
        return false;
      });

  circleBallState
    ->transitionTo(doneState, "done")
    ->whenTerminated();

  doneState
    ->transitionTo(standUpState)
    ->when(ballNotVisibleCondition);

  return fsm;
}
