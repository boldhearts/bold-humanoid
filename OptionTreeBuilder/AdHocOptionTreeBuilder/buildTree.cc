// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "Option/ActionOption/actionoption.hh"
#include "Option/DispatchOption/dispatchoption.hh"
#include "Option/FSMOption/fsmoption.hh"
#include "Option/GetUpOption/getupoption.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StayStanding/staystanding.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/UntilShutdown/untilshutdown.hh"

using namespace bold;
using namespace bold::state;
using namespace std;

shared_ptr<OptionTree> AdHocOptionTreeBuilder::buildTree(Agent* agent, std::shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // OPTIONS

  auto sitArmsBack =
    Option::make_shared<MotionScriptOption>("sit-down-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/sit-down-arms-back.json");
  auto sit =
    Option::make_shared<MotionScriptOption>("sit-down-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/sit-down.json");
  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto stopAgent =
    Option::make_shared<ActionOption>("stop-agent",
                                      [agent] { agent->stop(); });
  auto shutdownSequence =
    Option::make_shared<SequenceOption>("shutdown-sequence",
                                        Option::OptionVector{{ stopWalking, sitArmsBack, stopAgent }});

  auto keeperFsm = buildKeeperFsm(agent, clock, draw);
  auto strikerFsm = buildStrikerFsm(agent, clock, draw);
  auto supporterFsm = buildSupporterFsm(agent, clock, draw);
  auto penaltyStrikerFsm = buildPenaltyStrikerFsm(agent, clock, draw);
  auto penaltyKeeperFsm = buildPenaltyKeeperFsm(agent, clock, draw);
  auto kickLearnerFsm = buildKickLearnerFsm(agent, clock);
  auto ballCirclerFsm = buildBallCirclerFsm(agent, clock, draw);
  auto demoStrikerFsm = buildDemoStrikerFsm(agent, clock, draw);

  auto behaviourControl = agent->getBehaviourControl();
  auto performRole =
    Option::make_shared<DispatchOption<PlayerRole>>("perform-role",
                                                    [behaviourControl] {
                                                      return behaviourControl->getPlayerRole();
                                                    });
  performRole->setOption(PlayerRole::Keeper, keeperFsm);
  performRole->setOption(PlayerRole::Striker, strikerFsm);
  performRole->setOption(PlayerRole::Supporter, supporterFsm);
  performRole->setOption(PlayerRole::PenaltyStriker, penaltyStrikerFsm);
  performRole->setOption(PlayerRole::PenaltyKeeper, penaltyKeeperFsm);
  performRole->setOption(PlayerRole::KickLearner, kickLearnerFsm);
  performRole->setOption(PlayerRole::BallCircler, ballCirclerFsm);
  performRole->setOption(PlayerRole::DemoStriker, demoStrikerFsm);
  // Build the top-level FSM

  auto getUpScript =
    Option::make_shared<GetUpOption>("get-up",
                                     agent->getFallDetector(),
                                     agent->getMotionScriptModule());

  auto getUp = Option::make_shared<SequenceOption>("get-up-sequence", Option::OptionVector{getUpScript});

  auto stayStandingOption =
    Option::make_shared<StayStanding>("stay-standing",
                                      performRole,
                                      getUp,
                                      agent->getBehaviourControl(),
                                      agent->getWalkModule(),
                                      agent->getFallDetector());
                                                      
  auto respectPlayModeFsm = buildPlayModeFsm(agent, clock, stayStandingOption);
  auto allowPauseFsm = buildPauseStateFsm(agent, clock, respectPlayModeFsm);
  auto untilShutdown =
    Option::make_shared<UntilShutdown>("until-shutdown",
                                       agent,
                                       allowPauseFsm,
                                       shutdownSequence);
  auto boot =
    Option::make_shared<SequenceOption>("boot",
                                        Option::OptionVector{{ sit, untilShutdown }});
  // BUILD TREE

  auto tree = make_shared<OptionTree>(boot);

  // Register all FSMs with the tree so that we can debug them via Round Table
  tree->registerFsm(allowPauseFsm);
  tree->registerFsm(respectPlayModeFsm);
  tree->registerFsm(keeperFsm);
  tree->registerFsm(strikerFsm);
  tree->registerFsm(supporterFsm);
  tree->registerFsm(penaltyKeeperFsm);
  tree->registerFsm(kickLearnerFsm);
  tree->registerFsm(ballCirclerFsm);

  return tree;
}
