// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Kick/kick.hh"
#include "Option/ApproachBall/approachball.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LocateBall/locateball.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/StaticBalance/staticbalance.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/TrackBall/trackball.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/TeamState/teamstate.hh"
#include "conditionals.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::colour;
using namespace bold::state;
using namespace Eigen;
using namespace std;

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildKeeperFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // KICKS

  vector<shared_ptr<Kick const>> kicks = { Kick::getById("forward-left"), Kick::getById("forward-right") };

  // OPTIONS

  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto staticBalance =
    Option::make_shared<StaticBalance>("static-balance",
                                       agent->getStandModule());
  auto locateBall =
    Option::make_shared<LocateBall>("locate-ball",
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    clock,
                                    LookAround::speedIfBallVisible(0.15, 0.5),
                                    30,
                                    5);
  auto bigStepLeft =
    Option::make_shared<MotionScriptOption>("big-step-left",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/step-left-big.json");
  auto bigStepRight =
    Option::make_shared<MotionScriptOption>("big-step-right",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/step-right-big.json");
  auto approachBall =
    Option::make_shared<ApproachBall>("approach-ball",
                                      agent->getWalkModule(),
                                      agent->getBehaviourControl(),
                                      draw);
  auto kickMotion =
    Option::make_shared<MotionScriptOption>("kick",
                                            agent->getMotionScriptModule());
  auto clearGoalKick =
    Option::make_shared<MotionScriptOption>("clear-goal-kick",
                                            agent->getMotionScriptModule());
  auto trackBall =
    Option::make_shared<TrackBall>("track-ball", draw);

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "keeper");

  // STATES

  auto standUpState = fsm->newState("stand-up", { staticBalance }, false/*endState*/, true/*startState*/);
  auto locateBallState = fsm->newState("locate-ball", { stopWalking, locateBall, trackBall });
  auto bigStepLeftState = fsm->newState("big-step-left", { bigStepLeft });
  auto bigStepRightState = fsm->newState("big-step-right", { bigStepRight });
  auto approachBallState = fsm->newState("approach-ball", { approachBall, trackBall });
  auto clearGoalKickState = fsm->newState("clear-goal-kick", { clearGoalKick });

  setPlayerActivityInStates(agent, PlayerActivity::Waiting, { standUpState, locateBallState });
  setPlayerActivityInStates(agent, PlayerActivity::Positioning, { bigStepLeftState, bigStepRightState });

  // TRANSITIONS

  standUpState
    ->transitionTo(locateBallState, "standing")
    ->whenTerminated();

  /*
  static Bounds2d leftBallStepArea(Vector2d(-0.75, 0.25), Vector2d(-0.3, 1.0));
  static Bounds2d rightBallStepArea(Vector2d(0.3, 0.25), Vector2d(0.75, 1.0));

  locateBallState
    ->transitionTo(bigStepLeftState, "ball-left")
    ->when([clock]
    {
      return trueForMillis(1000, clock, []
      {
        Draw::fillPolygon(Frame::Agent, leftBallStepArea, BGR::white, 0.3, BGR::black, 0.0, 0);
        auto ball = State::get<AgentFrameState>()->getBallObservation();
        return ball && leftBallStepArea.contains(ball->head<2>());
      });
    });

  locateBallState
    ->transitionTo(bigStepRightState, "ball-right")
    ->when([clock]
    {
      return trueForMillis(1000, clock, []
      {
        Draw::fillPolygon(Frame::Agent, rightBallStepArea, BGR::white, 0.3, BGR::black, 0.0, 0);
        auto ball = State::get<AgentFrameState>()->getBallObservation();
        return ball && rightBallStepArea.contains(ball->head<2>());
      });
    });
  */

  bigStepLeftState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  bigStepRightState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  locateBallState
    ->transitionTo(clearGoalKickState, "can-kick")
    ->when([clearGoalKick,kicks]
    {
      auto ball = State::get<AgentFrameState>()->getBallObservation();

      if (!ball)
        return false;

      // Take the first possible kick
      // TODO should we take the one that travels the furthest?
      for (auto const& kick : kicks)
      {
        if (kick->estimateEndPos(ball->head<2>()).hasValue())
        {
          // We can perform this kick
          clearGoalKick->setMotionScript(kick->getMotionScript());
          return true;
        }
      }
      return false;
    });

  clearGoalKickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();


  auto ballIsNear = []
  {
    // Approach ball until we're within a given distance
    // TODO use filtered ball position
    auto ballObs = State::get<AgentFrameState>()->getBallObservation();
    static auto nearDistance = 0.5;
    return ballObs && (ballObs->head<2>().norm() < nearDistance);
  };

  auto ballIsStoppingDistance = []
  {
    // Approach ball until we're within a given distance
    // TODO use filtered ball position
    auto ballObs = State::get<AgentFrameState>()->getBallObservation();
    static auto stoppingDistance = Config::getSetting<double>("options.approach-ball.stop-distance");
    return ballObs && (ballObs->head<2>().norm() < stoppingDistance->getValue());
  };

  locateBallState
    ->transitionTo(approachBallState, "ball-near")
    ->when(ballIsNear);

  approachBallState
    ->transitionTo(clearGoalKickState)
    ->when(ballIsStoppingDistance);

  approachBallState
    ->transitionTo(locateBallState, "lost-ball")
    ->when([clock] { return ballLostConditionFactory(clock); });

  return fsm;
}
