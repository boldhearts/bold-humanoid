// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Option/KeepPosition/keepposition.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SearchBall/searchball.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "conditionals.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildSupporterFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // OPTIONS

  auto standUp =
    Option::make_shared<MotionScriptOption>("stand-up-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-ready-auto.json");
  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto lookForBall =
    Option::make_shared<LookAround>("look-for-ball",
                                    agent->getHeadModule(),
                                    135.0,
                                    clock,
                                    LookAround::speedIfBallVisible(0.15));
  auto lookAtBall =
    Option::make_shared<LookAtBall>("look-at-ball",
                                    agent->getCameraModel(),
                                    agent->getHeadModule());
  auto keepPosition =
    Option::make_shared<KeepPosition>("keep-position",
                                      PlayerRole::Supporter,
                                      agent->getWalkModule(),
                                      agent->getBehaviourControl(),
                                      clock, draw);
  auto searchBall =
    Option::make_shared<SearchBall>("search-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule());

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "supporter");

  // STATES

  auto standUpState = fsm->newState("stand-up", { standUp }, false/*endState*/, true/*startState*/);
  auto lookForBallState = fsm->newState("look-for-ball", { stopWalking, lookForBall });
  auto lookAtBallState = fsm->newState("look-at-ball", { stopWalking, lookAtBall });
  auto circleToFindLostBallState = fsm->newState("look-for-ball-circling", { searchBall });
  auto keepPositionState = fsm->newState("keep-position", { keepPosition });

  setPlayerActivityInStates(agent, PlayerActivity::Waiting, { standUpState, lookForBallState, lookForBallState, lookAtBallState });

  // TRANSITIONS

  standUpState
    ->transitionTo(lookForBallState, "standing")
    ->whenTerminated();

  lookForBallState
    ->transitionTo(lookAtBallState, "found")
    ->when([clock]{ return ballFoundConditionFactory(clock); });

  lookAtBallState
    ->transitionTo(lookForBallState, "lost")
    ->when([clock]{ return ballLostConditionFactory(clock); });

  lookAtBallState
    ->transitionTo(keepPositionState, "found")
    ->when([] { return stepUpDownThreshold(10, ballVisibleCondition); });

  keepPositionState
    ->transitionTo(lookForBallState, "lost-ball")
    ->when([clock]{ return ballLostConditionFactory(clock); });

  // walk a circle if we don't find the ball within some time limit
  lookForBallState
    ->transitionTo(circleToFindLostBallState, "lost-ball-long")
    ->after(chrono::seconds(8), clock);

  // after 10 seconds of circling, look for the ball again
  circleToFindLostBallState
    ->transitionTo(lookForBallState, "done")
    ->after(chrono::seconds(10), clock);

  // stop turning if the ball comes into view
  circleToFindLostBallState
    ->transitionTo(lookAtBallState, "found")
    ->when([] { return stepUpDownThreshold(5, ballVisibleCondition); });

  return fsm;
}
