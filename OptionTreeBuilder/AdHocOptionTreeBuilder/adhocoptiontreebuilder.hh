// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "OptionTreeBuilder/optiontreebuilder.hh"

namespace bold
{
  class Agent;

  class AdHocOptionTreeBuilder
  {
  public:
    std::shared_ptr<OptionTree> buildTree(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);

  private:
    std::shared_ptr<FSMOption> buildPauseStateFsm(Agent* agent, std::shared_ptr<Clock> clock,
                                                  std::shared_ptr<Option> whileUnpausedOption);
    std::shared_ptr<FSMOption> buildStayStandingFsm(Agent* agent, std::shared_ptr<Clock> clock,
                                                    std::shared_ptr<Option> whileStandingOption);
    std::shared_ptr<FSMOption> buildPlayModeFsm(Agent* agent,
                                                std::shared_ptr<Clock> clock,
                                                std::shared_ptr<Option> whilePlayingOption);


    std::shared_ptr<FSMOption> buildDemoStrikerFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildStrikerFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildSupporterFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildKeeperFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildPenaltyStrikerFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildPenaltyKeeperFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildKickLearnerFsm(Agent* agent, std::shared_ptr<Clock> clock);
    std::shared_ptr<FSMOption> buildBallCirclerFsm(Agent* agent, std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw);
    std::shared_ptr<FSMOption> buildWhistleListenerFsm(Agent* agent, std::shared_ptr<Clock> clock);
  };
}
