// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Option/ApproachBall/approachball.hh"
#include "Option/AtBall/atball.hh"
#include "Option/BuildStationaryMap/buildstationarymap.hh"
#include "Option/CircleBall/circleball.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/LookAtFeet/lookatfeet.hh"
#include "Option/LocateBall/locateball.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SearchBall/searchball.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "Option/Support/support.hh"
#include "state/State/state.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/GameState/gamestate.hh"
#include "state/StateObject/StationaryMapState/stationarymapstate.hh"
#include "src/util/Log/log.hh"
#include "conditionals.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace bold::util;
using namespace Eigen;
using namespace std;

auto shouldYieldToOtherAttacker = [](Clock const& clock)
{
  auto team = State::get<TeamState>();
  auto agentFrame = State::get<AgentFrameState>();

  if (!team || !agentFrame || !agentFrame->isBallVisible())
    return false;

  bool isTeamMateAttacking = team->isTeamMateInActivity(PlayerActivity::AttackingGoal, [&clock](PlayerState const& player) {
      return player.getAgeMillis(clock) <= 5000;
    });

  if (isTeamMateAttacking)
    return true;

  double ballDist = (*agentFrame->getBallObservation() - Point3d::ORIGIN).norm();
  auto ballObservers = team->getBallObservers([&clock](PlayerState const& player) { return player.getAgeMillis(clock) < 5000; });
  bool isTeamMateApproachingBallAndNearer = false;
  for (auto player : ballObservers)
  {
    if (player.activity == PlayerActivity::ApproachingBall && player.ballRelative->norm() < ballDist)
    {
      isTeamMateApproachingBallAndNearer = true;
      break;
    }
  }
  return isTeamMateApproachingBallAndNearer;
};

auto isWithinTenSecondsOfTheirKickOff = [](Clock const& clock)
{
  auto game = State::get<GameState>();
  return game && game->isWithinTenSecondsOfKickOff(Team::Them, clock);
};

auto isWithinTenSecondsOfOurKickOff = [](Clock const& clock)
{
  auto game = State::get<GameState>();
  return game && game->isWithinTenSecondsOfKickOff(Team::Us, clock);
};

// Indicates whether at this moment we observe the ball straight in front of us,
// and in line with our goal such that we should walk directly to it, and kick
// straight without looking around once we reach the ball.
// Unlike the building of the stationary map, this is based on a single cycle's
// observation and so may be used during motion (when not stationary.)
auto isPerfectLineForAttack = [](Clock const& clock)
{
  static auto isPerfectLineEnabled = Config::getSetting<bool>("options.perfect-line.enabled");

  if (!isPerfectLineEnabled->getValue())
    return false;

  auto agentFrame = State::get<AgentFrameState>();
  if (!agentFrame->isBallVisible())
    return false;

  auto goals = agentFrame->getGoalObservations();
  if (goals.size() != 2)
    return false;

  // ASSUME we are looking at the ball
  // Must be looking approximately straight ahead (the ball is directly in front of us)
  double panAngle = State::get<BodyState>(StateTime::CameraImage)->getJoint(JointId::HEAD_PAN)->getAngleRads();
  if (fabs(Math::radToDeg(panAngle)) > 5.0)
    return false;

  // Verify goals are approximately the correct distance apart
  double goalDist = (goals[0] - goals[1]).norm();
  if (fabs(goalDist - FieldMap::getGoalY()) > FieldMap::getGoalY()/3.0)
    return false;

  // If we have team data...
  auto team = State::get<TeamState>();
  if (team && team->getKeeperState())
  {
    GoalLabel label = GoalLabel::Unknown;

    auto post1Pos = Average<geometry2::Point2d>{};
    post1Pos.add(goals[0].toDim<2>());
    auto post2Pos = Average<geometry2::Point2d>{};
    post2Pos.add(goals[1].toDim<2>());

    auto keeper = team->getKeeperState();

    if (keeper && keeper->ballRelative.hasValue())
    {
      auto agentBallPos = agentFrame->getBallObservation();
      label = StationaryMapState::labelGoalByKeeperBallPosition(
        post1Pos,
        post2Pos,
        *keeper->ballRelative,
        agentBallPos.hasValue() ? agentBallPos->toDim<2>() : Point2d{0, 0});
    }

    if (label == GoalLabel::Unknown)
      label = StationaryMapState::labelGoalByKeeperBallDistance(post1Pos, post2Pos, team->getKeeperBallSideEstimate(clock));

    if (label == GoalLabel::Ours)
      return false;
  }

  // The goals must appear to either side of the ball
  double ballX = agentFrame->getBallObservation()->x();
  bool isRight0 = goals[0].x() > ballX;
  bool isRight1 = goals[1].x() > ballX;
  if (isRight0 == isRight1)
    return false;

  // All checks pass - transition to direct attack
  return true;
};

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildStrikerFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // OPTIONS

  auto buildStationaryMap = Option::make_shared<BuildStationaryMap>("build-stationary-map",
                                                                    agent->getVoice());

  auto standUp =
    Option::make_shared<MotionScriptOption>("stand-up-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-ready-auto.json",
                                            /*ifNotInFinalPose*/true);
  auto leftKick = make_shared<MotionScriptOption>("left-kick-script",
                                                  agent->getMotionScriptModule(),
                                                  "./motionscripts/kick-left.json");
  auto rightKick = make_shared<MotionScriptOption>("right-kick-script",
                                                   agent->getMotionScriptModule(),
                                                   "./motionscripts/kick-right.json");

  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto locateBall =
    Option::make_shared<LocateBall>("locate-ball",
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    clock,
                                    LookAround::speedIfBallVisible(0.15));
  auto approachBall =
    Option::make_shared<ApproachBall>("approach-ball",
                                      agent->getWalkModule(),
                                      agent->getBehaviourControl(),
                                      draw);
  auto kickMotion =
    Option::make_shared<MotionScriptOption>("kick",
                                            agent->getMotionScriptModule());
  auto atBall =
    Option::make_shared<AtBall>("at-ball",
                                agent->getHeadModule(),
                                clock);
  auto lookAtBall =
    Option::make_shared<LookAtBall>("look-at-ball",
                                    agent->getCameraModel(),
                                    agent->getHeadModule());
  auto lookAtFeet =
    Option::make_shared<LookAtFeet>("look-at-feet",
                                    agent->getHeadModule());
  auto circleBall =
    Option::make_shared<CircleBall>("circle-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    draw);
  auto searchBall =
    Option::make_shared<SearchBall>("search-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule());
//  auto awaitTheirKickOff = Option::make<AwaitTheirKickOff>("await-their-kick-off");
  auto support =
    Option::make_shared<Support>("support",
                                 agent->getWalkModule(),
                                 draw);

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "striker");

  // STATES

  auto standUpState = fsm->newState("stand-up", { standUp }, /*endState*/false, /*startState*/true);
  auto locateBallState = fsm->newState("locate-ball", { stopWalking, buildStationaryMap, locateBall });
  auto locateBallCirclingState = fsm->newState("locate-ball-circling", { searchBall });
  auto approachBallState = fsm->newState("approach-ball", { approachBall, lookAtBall });
  auto atBallState = fsm->newState("at-ball", { stopWalking, buildStationaryMap, atBall });
  auto turnAroundBallState = fsm->newState("turn-around-ball", { circleBall });
  auto kickForwardsState = fsm->newState("kick-forwards", { stopWalking, lookAtFeet });
  auto leftKickState = fsm->newState("left-kick", { leftKick });
  auto rightKickState = fsm->newState("right-kick", { rightKick });
  auto kickState = fsm->newState("kick", { Option::make_shared<SequenceOption>("stop-walking-and-kick-sequence", Option::OptionVector{{ stopWalking, kickMotion }}) });
  auto yieldState = fsm->newState("yield", { stopWalking, lookAtBall });
//  auto awaitTheirKickOffState = fsm->newState("await-their-kick-off", { stopWalking, locateBall, awaitTheirKickOff });
  auto supportState = fsm->newState("support", { support, lookAtBall });

  // NOTE we set either ApproachingBall or AttackingGoal in approachBall option directly
  //  setPlayerActivityInStates(agent, PlayerActivity::ApproachingBall, { approachBallState });
  setPlayerActivityInStates(agent, PlayerActivity::Waiting, { standUpState/*, awaitTheirKickOffState*/, locateBallCirclingState, locateBallState, yieldState, supportState });

  setPlayerActivityInStates(agent, PlayerActivity::AttackingGoal, { atBallState, turnAroundBallState, kickForwardsState });

  // TRANSITIONS

//  standUpState
//    ->transitionTo(awaitTheirKickOffState)
//    ->when(isWithinTenSecondsOfTheirKickOff);

  standUpState
    ->transitionTo(locateBallState, "standing")
    ->whenTerminated();

//  awaitTheirKickOffState
//    ->transitionTo(locateBallState)
//    ->whenTerminated();

  // start approaching the ball when we have the confidence that it's really there
  locateBallState
    ->transitionTo(approachBallState, "found-ball")
    ->when([] { return stepUpDownThreshold(10, ballVisibleCondition); });

  // walk a circle if we don't find the ball within some time limit
  locateBallState
    ->transitionTo(locateBallCirclingState, "lost-ball-long")
    ->after(chrono::seconds(12), clock);

  // after 4 seconds of circling, look for the ball again
  locateBallCirclingState
    ->transitionTo(locateBallState, "done")
    ->after(chrono::seconds(4), clock);

  // stop turning if the ball comes into view
  locateBallCirclingState
    ->transitionTo(locateBallState, "found-ball")
    ->when([] { return stepUpDownThreshold(5, ballVisibleCondition); });

  approachBallState
    ->transitionTo(locateBallState, "lost-ball")
    ->when([clock] { return ballLostConditionFactory(clock); });

  // Let another player shine if they're closer and attempting to score
  approachBallState
    ->transitionTo(supportState, "support")
    ->when([clock] { return stepUpDownThreshold(10, [clock]() { return shouldYieldToOtherAttacker(*clock); }); });

  // stop walking to ball once we're close enough
  approachBallState
    ->transitionTo(atBallState, "near-ball")
    ->when(ballIsStoppingDistance);

  yieldState
    ->transitionTo(locateBallState, "resume")
    ->when([clock] { return stepUpDownThreshold(10, util::negate([clock] { return shouldYieldToOtherAttacker(*clock); })); });

  supportState
    ->transitionTo(locateBallState, "resume")
    ->when([clock] { return stepUpDownThreshold(10, util::negate([clock] { return shouldYieldToOtherAttacker(*clock); })); });

  //
  // AT-BALL EXIT TRANSITIONS
  //

  /*
  atBallState
    ->transitionTo(locateBallState, "continue-drible")
    ->when([]
           {
             auto map = State::get<StationaryMapState>();
             return (map && map->canKick());
           });
  */

  atBallState
    ->transitionTo(kickState, "can-kick")
    ->when([kickMotion]
    {
      auto map = State::get<StationaryMapState>();
      if (map && map->canKick())
      {
        auto kick = map->getSelectedKick();
        if (kick != nullptr)
        {
          kickMotion->setMotionScript(kick->getMotionScript());
          return true;
        }
      }
      return false;
    });

  atBallState
    ->transitionTo(turnAroundBallState)
    ->when([circleBall,kickMotion]
    {
      auto map = State::get<StationaryMapState>();
      if (!map)
        return false;
      double turnAngle = map->getTurnAngleRads();
      if (turnAngle == 0)
        return false;
      // TODO provide onResetBefore virtual on Option, and capture this there
      circleBall->setTurnParams(turnAngle, map->getTurnBallPos());

      auto kick = map->getTurnForKick();
      ASSERT(kick);
      kickMotion->setMotionScript(kick->getMotionScript());

      return true;
    });

  // limit how long we will look for the goal
  atBallState
//    ->transitionTo(locateBallState, "give-up")
    ->transitionTo(kickForwardsState, "give-up")
    ->after(chrono::seconds(7), clock);

//  atBallState
//    ->transitionTo(kickForwardsState, "give-up")
//    ->after(chrono::seconds(0), clock);

  // If we notice the ball is too far to dribble, abort
  atBallState
    ->transitionTo(locateBallState, "ball-too-far")
    ->when([] { return stepUpDownThreshold(6, ballTooFarToKick); });

  kickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  turnAroundBallState
    ->transitionTo(kickState, "done")
    ->whenTerminated();

  turnAroundBallState
    ->transitionTo(locateBallState, "ball-too-far")
    ->when([] { return stepUpDownThreshold(10, ballTooFarToKick); });

  turnAroundBallState
    ->transitionTo(locateBallState, "lost-ball")
    ->when([clock] { return ballLostConditionFactory(clock); });

  //
  // KICK FORWARDS
  //

  // If we notice the ball is too far to kick, abort kick
  kickForwardsState
    ->transitionTo(locateBallState, "ball-too-far")
    ->when([] { return stepUpDownThreshold(10, ballTooFarToKick); });

  // TODO if ball too central, step to left/right slightly, or use different kick

  kickForwardsState
    ->transitionTo(leftKickState, "ball-left")
    ->when([lookAtFeet,kickForwardsState,clock]
           {
             // Look at feet for one second
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;

             // Wait until we've finished looking down
             if (!kickForwardsState->allOptionsTerminated())
               return false;

             if (lookAtFeet->hasPosition())
             {
               auto ballPos = lookAtFeet->getAverageBallPositionAgentFrame();
               if (ballPos.x() < 0)
               {
                 Log::info("kickForwardsState->leftKickState") << "Kicking with left foot when ball at (" << ballPos.x() << "," << ballPos.y() << ")";
                 return true;
               }
             }
             return false;
           });

  kickForwardsState
    ->transitionTo(rightKickState, "ball-right")
    ->when([lookAtFeet,kickForwardsState,clock]
           {
             // Look at feet for one second
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;

             // Wait until we've finished looking down
             if (!kickForwardsState->allOptionsTerminated())
               return false;

             if (lookAtFeet->hasPosition())
             {
               auto ballPos = lookAtFeet->getAverageBallPositionAgentFrame();
               if (ballPos.x() >= 0)
               {
                 Log::info("kickForwardsState->rightKickState") << "Kicking with right foot when ball at (" << ballPos.x() << "," << ballPos.y() << ")";
                 return true;
               }
             }
             return false;
           });

  kickForwardsState
    ->transitionTo(locateBallState, "ball-gone")
    ->when([kickForwardsState,clock]
           {
             // TODO create and use 'all' operator
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;

             // Wait until we've finished looking down
             return kickForwardsState->allOptionsTerminated();
           });

  leftKickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  rightKickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

 return fsm;
}
