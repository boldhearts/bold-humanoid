// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Drawing/drawing.hh"
#include "FieldMap/fieldmap.hh"
#include "Option/FSMOption/fsmoption.hh"
#include "Option/LocateBall/locateball.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

#include "geometry2/Operator/Predicate/Contains/contains.hh"
#include "motion/scripts/MotionScript/motionscript.hh"
#include "util/conditionals.hh"

using namespace bold;
using namespace bold::colour;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::motion::scripts;
using namespace bold::state;
using namespace bold::util;
using namespace Eigen;
using namespace std;

shared_ptr<FSMOption>
AdHocOptionTreeBuilder::buildPenaltyKeeperFsm(Agent *agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw) {
  // SCRIPTS

  auto diveLeftScript = MotionScript::fromFile("./motionscripts/dive-left.json");
  auto diveRightScript = MotionScript::fromFile("./motionscripts/dive-left.json");
//  auto diveSplitsScript = MotionScript::fromFile("./motionscripts/dive-splits.json");

  auto signalLeftScript = MotionScript::fromFile("./motionscripts/signal-left-side.json");
  auto signalRightScript = MotionScript::fromFile("./motionscripts/signal-right-side.json");
//  auto signalBothScript = MotionScript::fromFile("./motionscripts/signal-both-sides.json");

  // OPTIONS

  auto standUp =
      Option::make_shared<MotionScriptOption>("stand-up-script",
                                              agent->getMotionScriptModule(),
                                              "./motionscripts/stand-ready-auto.json");
  auto diveLeft =
      Option::make_shared<MotionScriptOption>("dive-left",
                                              agent->getMotionScriptModule());
  auto diveRight =
      Option::make_shared<MotionScriptOption>("dive-right",
                                              agent->getMotionScriptModule());
//  auto diveSplits =
  Option::make_shared<MotionScriptOption>("splits",
                                          agent->getMotionScriptModule());
  auto stopWalking =
      Option::make_shared<StopWalking>("stop-walking",
                                       agent->getWalkModule());
  auto locateBall =
      Option::make_shared<LocateBall>("locate-ball",
                                      agent->getHeadModule(),
                                      agent->getCameraModel(),
                                      clock,
                                      LookAround::speedIfBallVisible(0.15, 0.5),
                                      30,
                                      5);

  auto fsm =
      Option::make_shared<FSMOption>(agent->getVoice(),
                                     clock,
                                     "penalty-keeper");

  // SETTINGS

  Config::getSetting<bool>("keeper.dive.indicate-only")->track([=](bool indicateOnly) {
    diveLeft->setMotionScript(indicateOnly ? signalLeftScript : diveLeftScript);
    diveRight->setMotionScript(indicateOnly ? signalRightScript : diveRightScript);
//    diveSplits->setMotionScript(indicateOnly ? signalBothScript : diveSplitsScript);
  });

  // STATES

  auto standUpState = fsm->newState("stand-up", {standUp}, false/*endState*/, true/*startState*/);
  auto locateBallState = fsm->newState("locate-ball", {stopWalking, locateBall});
  auto leftDiveState = fsm->newState("left-dive", {diveLeft});
  auto rightDiveState = fsm->newState("right-dive", {diveRight});

  // TRANSITIONS

  standUpState
      ->transitionTo(locateBallState, "standing")
      ->whenTerminated();

  const static double goalWidth = FieldMap::getGoalY();
  static auto leftBallDiveArea = AlignedBox2d{Vector2d{-goalWidth / 1.5, -0.2},
                                              Vector2d{-0.1, 1.0}};
  static auto rightBallDiveArea = AlignedBox2d{Vector2d{0.1, -0.2},
                                               Vector2d{goalWidth / 1.5, 1.0}};

  locateBallState
      ->transitionTo(leftDiveState, "ball-left")
      ->when([draw] {
        return stepUpDownThreshold(3, [draw] {
          draw->fillPolygon(Frame::Agent, Polygon2d{leftBallDiveArea},
                            BGR::red(), 0.3, BGR::black(), 0.0, 0);
          auto ball = State::get<AgentFrameState>()->getBallObservation();
          return ball && contains(Polygon2d{leftBallDiveArea}, ball->toDim<2>());
        });
      });

  locateBallState
      ->transitionTo(rightDiveState, "ball-right")
      ->when([draw] {
        return stepUpDownThreshold(3, [draw] {
          draw->fillPolygon(Frame::Agent, Polygon2d{rightBallDiveArea},
                            BGR::red(), 0.3, BGR::red(), 0.0, 0);
          auto ball = State::get<AgentFrameState>()->getBallObservation();
          return ball && contains(Polygon2d{rightBallDiveArea}, ball->toDim<2>());
        });
      });

  leftDiveState
      ->transitionTo(locateBallState, "done")
      ->whenTerminated();

  rightDiveState
      ->transitionTo(locateBallState, "done")
      ->whenTerminated();

  return fsm;
}
