// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "Option/FSMOption/fsmoption.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "agent/ButtonObserver/buttonobserver.hh"

#include "motion/modules/HeadModule/headmodule.hh"
#include "roundtable/Action/action.hh"

using namespace bold;
using namespace bold::motion::modules;
using namespace bold::roundtable;
using namespace bold::state;
using namespace std;

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildPauseStateFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Option> whileUnpausedOption)
{
  // TODO this is probably simple enough to allow making it an Option subclass rather than an FSM

  shared_ptr<ButtonTracker> pauseButton = agent->getButtonObserver()->track(Button::Middle);

  // OPTIONS

  auto sit =
    Option::make_shared<MotionScriptOption>("sit-down-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/sit-down.json");
  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto stopAndSitSequence =
    Option::make_shared<SequenceOption>("stop-then-sit-sequence",
                                        Option::OptionVector{{ stopWalking, sit }});

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "allow-pause");

  // STATES

  auto unpausedState = fsm->newState("unpaused", { whileUnpausedOption }, false, true);
  auto pausedState = fsm->newState("paused", { stopAndSitSequence });

  pausedState->onEnter.connect([agent] {
    agent->getBehaviourControl()->setPlayerActivity(PlayerActivity::Waiting);
    agent->getBehaviourControl()->setPlayerStatus(PlayerStatus::Paused);
    agent->getHeadModule()->moveToHome();
  });

  // TRANSITIONS

  fsm
    ->wildcardTransitionTo(pausedState, "middle-button")
    ->when([pauseButton, clock] { return pauseButton->isPressedForMillis(200, *clock); });

  pausedState
    ->transitionTo(unpausedState, "middle-button")
    ->when([pauseButton, clock] { return pauseButton->isPressedForMillis(80, *clock); });

  return fsm;
}
