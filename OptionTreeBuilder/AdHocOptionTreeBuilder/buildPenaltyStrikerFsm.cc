// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adhocoptiontreebuilder.hh"

#include "Option/ApproachBall/approachball.hh"
#include "Option/AtBall/atball.hh"
#include "Option/CircleBall/circleball.hh"
#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/LookAtFeet/lookatfeet.hh"
#include "Option/LocateBall/locateball.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "Option/SearchBall/searchball.hh"
#include "Option/StopWalking/stopwalking.hh"
#include "conditionals.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace Eigen;
using namespace std;

shared_ptr<FSMOption> AdHocOptionTreeBuilder::buildPenaltyStrikerFsm(Agent* agent, shared_ptr<Clock> clock, shared_ptr<Draw> draw)
{
  // OPTIONS

  auto standUp =
    Option::make_shared<MotionScriptOption>("stand-up-script",
                                            agent->getMotionScriptModule(),
                                            "./motionscripts/stand-ready-auto.json",
                                            /*ifNotInFinalPose*/true);

  auto leftKick = make_shared<MotionScriptOption>("left-kick-script",
                                                  agent->getMotionScriptModule(),
                                                  "./motionscripts/kick-left.json");
  auto rightKick = make_shared<MotionScriptOption>("right-kick-script",
                                                   agent->getMotionScriptModule(),
                                                   "./motionscripts/kick-right-grass.json");

  auto stopWalking =
    Option::make_shared<StopWalking>("stop-walking",
                                     agent->getWalkModule());
  auto locateBall =
    Option::make_shared<LocateBall>("locate-ball",
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    clock,
                                    LookAround::speedIfBallVisible(0.15));
  auto approachBall =
    Option::make_shared<ApproachBall>("approach-ball",
                                      agent->getWalkModule(),
                                      agent->getBehaviourControl(),
                                      draw);
  auto atBall =
    Option::make_shared<AtBall>("at-ball",
                                agent->getHeadModule(),
                                clock);
  auto lookAtBall =
    Option::make_shared<LookAtBall>("look-at-ball",
                                    agent->getCameraModel(),
                                    agent->getHeadModule());
  auto lookAtFeet =
    Option::make_shared<LookAtFeet>("look-at-feet",
                                    agent->getHeadModule());
  auto circleBall   =
    Option::make_shared<CircleBall>("circle-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule(),
                                    agent->getCameraModel(),
                                    draw);
  auto searchBall =
    Option::make_shared<SearchBall>("search-ball",
                                    agent->getWalkModule(),
                                    agent->getHeadModule());

  auto fsm =
    Option::make_shared<FSMOption>(agent->getVoice(),
                                   clock,
                                   "penalty-striker");

  // STATES

  auto standUpState = fsm->newState("stand-up", { standUp }, /*endState*/false, /*startState*/true);
  auto locateBallState = fsm->newState("locate-ball", { stopWalking, locateBall });
  auto locateBallCirclingState = fsm->newState("locate-ball-circling", { searchBall });
  auto approachBallState = fsm->newState("approach-ball", { approachBall, lookAtBall });
  auto atBallState = fsm->newState("at-ball", { stopWalking, atBall });
  auto kickForwardsState = fsm->newState("kick-forwards", { stopWalking, lookAtFeet });
  auto leftKickState = fsm->newState("left-kick", { leftKick });
  auto rightKickState = fsm->newState("right-kick", { rightKick });

  // NOTE we set either ApproachingBall or AttackingGoal in approachBall option directly
  //  setPlayerActivityInStates(agent, PlayerActivity::ApproachingBall, { approachBallState });
  setPlayerActivityInStates(agent, PlayerActivity::Waiting, { standUpState/*, awaitTheirKickOffState*/, locateBallCirclingState, locateBallState });

  setPlayerActivityInStates(agent, PlayerActivity::AttackingGoal, { atBallState });

  // TRANSITIONS

  standUpState
    ->transitionTo(locateBallState, "standing")
    ->whenTerminated();

  // start approaching the ball when we have the confidence that it's really there
  locateBallState
    ->transitionTo(approachBallState, "found-ball")
    ->when([] { return stepUpDownThreshold(10, ballVisibleCondition); });

  // walk a circle if we don't find the ball within some time limit
  locateBallState
    ->transitionTo(locateBallCirclingState, "lost-ball-long")
    ->after(chrono::seconds(12), clock);

  // after 4 seconds of circling, look for the ball again
  locateBallCirclingState
    ->transitionTo(locateBallState, "done")
    ->after(chrono::seconds(4), clock);

  // stop turning if the ball comes into view
  locateBallCirclingState
    ->transitionTo(locateBallState, "found-ball")
    ->when([] { return stepUpDownThreshold(5, ballVisibleCondition); });

  approachBallState
    ->transitionTo(locateBallState, "lost-ball")
    ->when([clock]{ return ballLostConditionFactory(clock); });

  // stop walking to ball once we're close enough
  approachBallState
    ->transitionTo(atBallState, "near-ball")
    ->when(ballIsStoppingDistance);

  atBallState
    ->transitionTo(kickForwardsState, "kick")
    ->after(chrono::seconds(0), clock);

  // If we notice the ball is too far to kick, abort kick
  kickForwardsState
    ->transitionTo(locateBallState, "ball-too-far")
    ->when([] { return stepUpDownThreshold(10, ballTooFarToKick); });

  kickForwardsState
    ->transitionTo(leftKickState, "ball-left")
    ->when([lookAtFeet,kickForwardsState,clock]
           {
             // Look at feet for one second
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;

             // Wait until we've finished looking down
             if (!kickForwardsState->allOptionsTerminated())
               return false;

             if (lookAtFeet->hasPosition())
             {
               auto ballPos = lookAtFeet->getAverageBallPositionAgentFrame();
               if (ballPos.x() < 0)
               {
                 Log::info("kickForwardsState->leftKickState") << "Kicking with left foot when ball at (" << ballPos.x() << "," << ballPos.y() << ")";
                 return true;
               }
             }
             return false;
           });

  kickForwardsState
    ->transitionTo(rightKickState, "ball-right")
    ->when([lookAtFeet,kickForwardsState,clock]
           {
             // Look at feet for one second
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;

             // Wait until we've finished looking down
             if (!kickForwardsState->allOptionsTerminated())
               return false;

             if (lookAtFeet->hasPosition())
             {
               auto ballPos = lookAtFeet->getAverageBallPositionAgentFrame();
               if (ballPos.x() >= 0)
               {
                 Log::info("kickForwardsState->rightKickState") << "Kicking with right foot when ball at (" << ballPos.x() << "," << ballPos.y() << ")";
                 return true;
               }
             }
             return false;
           });

  kickForwardsState
    ->transitionTo(locateBallState, "ball-gone")
    ->when([kickForwardsState,clock]
           {
             // TODO create and use 'all' operator
             if (kickForwardsState->secondsSinceStart(*clock) < 1)
               return false;
             
             // Wait until we've finished looking down
             return kickForwardsState->allOptionsTerminated();
           });

  leftKickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  rightKickState
    ->transitionTo(locateBallState, "done")
    ->whenTerminated();

  return fsm;
}
