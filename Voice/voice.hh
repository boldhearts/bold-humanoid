// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <initializer_list>
#include <iosfwd>

#include "src/util/ConsumerQueueThread/consumerqueuethread.hh"

namespace bold
{
  namespace config
  {
    template<typename> class Setting;
  }

  struct SpeechTask
  {
    std::string message;
    unsigned rate;
    bool pauseAfter;
  };

  class Voice
  {
  public:
    Voice();
    ~Voice();

    void say(std::string const& message);
    void say(SpeechTask const& task);

    void sayOneOf(std::initializer_list<std::string> const& messages);

    void stop();

    unsigned queueLength() const;

  private:
    Voice(const Voice&) = delete;
    Voice& operator=(const Voice&) = delete;

    void sayCallback(SpeechTask const& message);

    util::ConsumerQueueThread<SpeechTask> d_queue;
    config::Setting<int>* d_voiceID;
    config::Setting<std::string>* d_voiceName;
    config::Setting<int>* d_rate;
    config::Setting<int>* d_volume;
    config::Setting<int>* d_pitch;
    config::Setting<int>* d_range;
    config::Setting<int>* d_wordGapMs;
    config::Setting<int>* d_capitalAccentHz;
    bool d_initialised;
  };
}
