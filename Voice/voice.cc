// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "voice.hh"

#include "config/Config/config.hh"
#include "roundtable/Action/action.hh"

#include <espeak/speak_lib.h>
#include <random>

using namespace bold;
using namespace bold::config;
using namespace bold::roundtable;
using namespace bold::util;
using namespace std;

static const std::map<int,std::string const> voiceById = {
  { 0, "none" },
  { 1, "english-mb-en1" },
  { 2, "english-mb-en1+f3" },
  { 3, "english" },
  { 4, "en-scottish" },
  { 5, "english-north" },
  { 6, "english_rp" },
  { 7, "english_wmids" },
  { 8, "english-us" },
  { 9, "german" },
  { 10, "dutch" }
};

Voice::Voice()
  : d_queue("Voice", [this](SpeechTask const& task) { sayCallback(task); }),
  d_voiceID(Config::getSetting<int>("voice.id")),
  d_voiceName(Config::getSetting<string>("voice.name")),
  d_rate(Config::getSetting<int>("voice.rate")),
  d_volume(Config::getSetting<int>("voice.volume")),
  d_pitch(Config::getSetting<int>("voice.pitch")),
  d_range(Config::getSetting<int>("voice.range")),
  d_wordGapMs(Config::getSetting<int>("voice.word-gap-ms")),
  d_capitalAccentHz(Config::getSetting<int>("voice.capital-accent-hz")),
  d_initialised(false)
{
  static const string sayings[] = {
    "Hello", "Bold Hearts", "Hooray", "Oh, my",
    "The rain in spain falls mainly in the plain",
    "Use those hands, Gareth", "Good Hands Gareth",
    "Adventure on Ywain", "Ywain, you bastard",
    "Kick it, Tor",
    "Nim-way, you're some kind of lady",
    "Let's hear it for Oberon"
  };

  int sayingIndex = 1;
  for (auto const& saying : sayings)
  {
    stringstream id;
    id << "voice.speak.saying-" << sayingIndex++;
    Action::addAction(id.str(), saying, [this,saying] { say(saying); });
  }
}

Voice::~Voice()
{
  if (d_initialised)
    espeak_Terminate();
}

unsigned int Voice::queueLength() const
{
  return d_queue.size();
}

void Voice::say(string const& message)
{
  SpeechTask task = { message, (uint)d_rate->getValue(), true };
  say(task);
}

void Voice::say(SpeechTask const& task)
{
  Log::verbose("Voice::say") << "Enqueuing: " << task.message;
  d_queue.push(task);
}

void Voice::sayOneOf(initializer_list<string> const& messages)
{
  static default_random_engine randomness((uint)time(0));
  size_t index = randomness() % messages.size();
  string message = *(messages.begin() + index);
  say(message);
}

void Voice::sayCallback(SpeechTask const& task)
{
  if (!Config::getValue<bool>("voice.enabled"))
    return;

  if (!d_initialised)
  {
    int res = espeak_Initialize(
      AUDIO_OUTPUT_SYNCH_PLAYBACK, // plays audio data synchronously
      500,                         // length of buffers for synth function, in ms; not used
      NULL,                        // dir containing espeak-data, null for default
      0);                          // options are mostly for phoneme callbacks, so 0

    if (res == -1)
    {
      Log::error("Voice::sayCallback") << "Error initialising libespeak -- ignoring SpeechTask";
      return;
    }
    else
    {
      char const *path;
      auto version = string{espeak_Info(&path)};
      Log::verbose("Voice::Voice") << "espeak " << version << "(" << path << ")";

      espeak_SetParameter(espeakPUNCTUATION, espeakPUNCT_NONE, 0);

      d_initialised = true;
    }
  }

  int voiceId = d_voiceID->getValue();
  string voiceName = "";
  if (voiceId == 0)
    voiceName = d_voiceName->getValue();
  else
  {
    auto const it = voiceById.find(voiceId);
    if (it != voiceById.end())
      voiceName = it->second;
  }

  if (voiceName != "")
    espeak_SetVoiceByName(voiceName.c_str());

  // Set parameters each time, in case they change
  espeak_SetParameter(espeakRATE,     task.rate,                     0);
  espeak_SetParameter(espeakVOLUME,   d_volume->getValue(),          0);
  espeak_SetParameter(espeakPITCH,    d_pitch->getValue(),           0);
  espeak_SetParameter(espeakRANGE,    d_range->getValue(),           0);
  espeak_SetParameter(espeakWORDGAP,  d_wordGapMs->getValue() / 10,  0); // units of 10ms
  espeak_SetParameter(espeakCAPITALS, d_capitalAccentHz->getValue(), 0);

  auto const& message = task.message;

  Log::verbose("Voice") << "Saying: " << message;

  unsigned flags = espeakCHARS_AUTO; // AUTO      8 bit or UTF8 automatically
                                 //
  if (task.pauseAfter)           //
    flags |= espeakENDPAUSE;     // ENDPAUSE  sentence pause at end of text

  espeak_ERROR err = espeak_Synth(
    message.c_str(),   // text
    message.length(),  // size
    0,                 // position to start from
    POS_CHARACTER,     // whether above 0 pos is chars/word/sentences
    message.size(),    // end position, 0 indicating no end
    flags,             // as above
    nullptr,           // message identifier given to callback (unused)
    nullptr);          // user data, passed to the callback function (unused)

  if (err != EE_OK)
    Log::error("Voice::sayCallback") << "Error synthesising speech";

  Log::verbose("Voice::sayCallback") << "Message completed";
}

void Voice::stop()
{
  d_queue.stop();
}
