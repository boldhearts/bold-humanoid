// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <Eigen/Core>
#include "config/Config/config.hh"
#include "src/util/Maybe/maybe.hh"

namespace bold
{
  /** FSR foot sensor model
   */
  class FSRModel
  {
  public:
    enum Side
    {
      LEFT,
      RIGHT
    };

    FSRModel()
      : FSRModel(config::Config::getStaticValue<double>("hardware.fsr.sensor-offset-x"),
                 config::Config::getStaticValue<double>("hardware.fsr.sensor-offset-y"),
                 config::Config::getStaticValue<double>("hardware.fsr.min-load"),
                 config::Config::getStaticValue<double>("hardware.fsr.n-per-digit"))
    {}

    /** Create an FSR model with specific offsets and sensitivity
     * @param sensorOffsetX Distance of FSRs to foor centre along X axis, in meters
     * @param sensorOffsetY Distance of FSRs to foor centre along Y axis, in meters
     * @param minLoad Smallest load measurable by FSRs, in Newton
     * @param N_per_digit FSR sensitivity, in Newton per digit
     */
    FSRModel(double sensorOffsetX, double sensorOffsetY,
             double minLoad, double N_per_digit)
      : d_sensorOffsetX(sensorOffsetX),
        d_sensorOffsetY(sensorOffsetY),
        d_minLoad(minLoad),
        d_N_per_digit(N_per_digit)
    {}

    /** Convert raw resistor reading to load in Newton */
    double fsrValueToNs(uint16_t value) const;

    /** Convert raw COP reading to location to foot center in meters
     *
     * @param x X coordinate reading
     * @param y Y coordinate reading
     * @param side Whether the reading is for the left or the right foot
     * @returns 2D vector indication COP position in foot frame
     */
    util::Maybe<Eigen::Vector2d> centerValueTo2D(uint8_t x, uint8_t y, Side side) const;

  private:
    double d_sensorOffsetX;
    double d_sensorOffsetY;

    double d_minLoad;
    double d_N_per_digit;
  };

  inline double FSRModel::fsrValueToNs(uint16_t value) const
  {
    double N = value * d_N_per_digit;
    if (N < d_minLoad)
      return 0.0;
    return N;
  }

  inline bold::util::Maybe<Eigen::Vector2d> FSRModel::centerValueTo2D(uint8_t x, uint8_t y, bold::FSRModel::Side side) const
  {
    if (x == 255 || y == 255)
      return util::Maybe<Eigen::Vector2d>::empty();

    double stepX = 2 * d_sensorOffsetX / 254;
    double stepY = 2 * d_sensorOffsetY / 254;
    
    auto vec = Eigen::Vector2d{d_sensorOffsetX - x * stepX, -d_sensorOffsetY + y * stepY};
    if (side == LEFT)
      return util::Maybe<Eigen::Vector2d>{vec};
    else
      return util::Maybe<Eigen::Vector2d>{-vec};
  }
}
