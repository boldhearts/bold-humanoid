// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "Agent/agent.hh"
#include "Drawing/drawing.hh"
#include "ImageCodec/PngCodec/pngcodec.hh"
#include "OptionTreeBuilder/AdHocOptionTreeBuilder/adhocoptiontreebuilder.hh"
#include "RobotisMotionFile/robotismotionfile.hh"
#include "ThreadUtil/threadutil.hh"
#include "Version/version.hh"

#include "config/Config/config.hh"
#include "motion/scripts/MotionScript/motionscript.hh"
#include "util/ccolor.hh"
#include "util/LogAppender/logappender.hh"
#include "vision/Camera/ImageFeedCamera/imagefeedcamera.hh"
#include "vision/Camera/V4L2Camera/v4l2camera.hh"
#include "vision/CameraController/cameracontroller.hh"

#include "banners.hh"

#include <getopt.h>
#include <signal.h>
#include <syslog.h>

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace bold::vision;
using namespace std;

unique_ptr<Agent> agent;

void printUsage()
{
  cout << endl;
  cout << "Options:" << endl;
  cout << endl;
  cout << ccolor::fore::lightblue << "  -c <file> " << ccolor::fore::white << "use specified configuration file (or --config)" << endl;
  cout << ccolor::fore::lightblue << "  -m <file> " << ccolor::fore::white << "load robotis motion script (or --motion)" << endl;
  cout << ccolor::fore::lightblue << "    -p <page> " << ccolor::fore::white << "page in motion script [use " << ccolor::back::magenta << "before" << ccolor::back::console << " -m] (or --page)" << endl;
  cout << ccolor::fore::lightblue << "    -o <file> " << ccolor::fore::white << "output file [user " << ccolor::back::magenta << "after" << ccolor::back::console << " -m] (or --output)" << endl;
  cout << ccolor::fore::lightblue << "  -v        " << ccolor::fore::white << "verbose logging (or --verbose)" << endl;
  cout << ccolor::fore::lightblue << "  -vv       " << ccolor::fore::white << "trace logging" << endl;
  cout << ccolor::fore::lightblue << "  -h        " << ccolor::fore::white << "show these options (or --help)" << endl;
  cout << ccolor::fore::lightblue << "  -i <file> " << ccolor::fore::white << "use specified image file as camera feed (or --image)" << endl;
  cout << ccolor::fore::lightblue << "  --version " << ccolor::fore::white << "print git version details at time of build" << endl;
  cout << ccolor::reset;
}

void syslog(const char* msg)
{
  openlog("boldhumanoid", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
  syslog(LOG_NOTICE, "%s", msg);
  closelog();
}

void handleShutdownSignal(int sig)
{
  if (agent)
  {
    Log::info("boldhumanoid") << "Received signal '" << strsignal(sig) << "' (" << sig << ") - shutting down agent";
    syslog("Shutdown request received");
    agent->requestShutdown(sig == 2);
  }
}

void printBanner()
{
  if (ConsoleLogAppender::isStdOutRedirected())
  {
    cout << banners[rand() % banners.size()] << endl << endl;
  }
  else
  {
    cout << ccolor::bold << ccolor::fore::lightmagenta
        << banners[rand() % banners.size()]
        << endl << endl << ccolor::reset;
  }
}

void printVersion()
{
  cout << ccolor::fore::lightblue << "SHA1:        " << ccolor::reset << Version::GIT_SHA1 << endl
#if EIGEN_ALIGN
       << ccolor::fore::lightblue << "Eigen align: " << ccolor::reset << "Yes" << endl
#else
       << ccolor::fore::lightblue << "Eigen align: " << ccolor::reset << "No" << endl
#endif
       << ccolor::fore::lightblue << "Build type:  " << ccolor::reset << Version::BUILD_TYPE << endl
       << ccolor::fore::lightblue << "Build host:  " << ccolor::reset << Version::BUILT_ON_HOST_NAME << endl
#if INCLUDE_ASSERTIONS
       << ccolor::fore::lightblue << "Assertions:  " << ccolor::reset << "Yes" << endl
#else
       << ccolor::fore::lightblue << "Assertions:  " << ccolor::reset << "No" << endl
#endif
       << ccolor::fore::lightblue << "Commit date: " << ccolor::reset << Version::GIT_DATE << " (" << Version::describeTimeSinceGitDate() << ")" << endl
       << ccolor::fore::lightblue << "Message:     " << ccolor::reset << Version::GIT_COMMIT_SUBJECT << endl;
}

void logVersion()
{
  Log::info("BUILD") << Version::GIT_SHA1 << " (committed " << Version::describeTimeSinceGitDate() << ")";
#if INCLUDE_ASSERTIONS
  Log::info("ASSERTIONS") << "Yes";
#else
  Log::info("ASSERTIONS") << "No";
#endif
#if EIGEN_ALIGN
  Log::info("EIGEN_ALIGN") << "Yes";
#else
  Log::info("EIGEN_ALIGN") << "No";
#endif
  Log::info("BUILD_TYPE") << Version::BUILD_TYPE;
  Log::info("BUILT_ON_HOST_NAME") << Version::BUILT_ON_HOST_NAME << "\n";
}

int main(int argc, char **argv)
{
  syslog("Starting boldhumanoid process");

  ThreadUtil::setThreadId(ThreadId::Main);

  srand(static_cast<unsigned int>(time(0)));

  // defaults
  string configurationFile = "configuration-agent.json";
  string imageFeedFile = "";
  Log::minLevel = LogLevel::Info;

  int c;

  //
  // Process command line arguments
  //
  option longOptions[] = {
    {"config", required_argument, nullptr, 'c'},
    {"motion", required_argument, nullptr, 'm'},
    {"page", required_argument, nullptr, 'p'},
    {"output", required_argument, nullptr, 'o'},
    {"help", no_argument, nullptr, 'h'},
    {"image", required_argument, nullptr, 'i'},
    {"verbose", no_argument, nullptr, 'v'},
    {"version", no_argument, nullptr, 1},
    {0, 0, nullptr, 0}
  };

  int page = 0; // Note the page to be used in motion file
  shared_ptr<motion::scripts::MotionScript> motionScript = nullptr; // Store the last motion script loaded

  int verboseCount = 0;
  int optionIndex;

  while ((c = getopt_long(argc, argv, "c:m:p:o:h:i:w:v", longOptions, &optionIndex)) != -1)
  {
    switch (c)
    {
    case 'c':
      configurationFile = string{optarg};
      break;
    case 'm':
      motionScript = (new RobotisMotionFile(optarg))->toMotionScript(page);
      // TODO: Don't assume file is valid!
      break;
    case 'p':
      page = std::stoi(optarg);
      break;
    case 'o' :
      // NOTE: As conversion is required, close after.
      if(motionScript)
      {
        // TODO: This should probably be the motion script directory by
        //       default
        motionScript->writeJsonFile(optarg);
      }
      else // If there is no file to convert to/from, fail
      {
        exit(EXIT_FAILURE);
      }
      break;
    case 'h':
      printBanner();
      printUsage();
      exit(EXIT_SUCCESS);
    case 'i':
      imageFeedFile = string{optarg};
      break;
    case 'v':
      verboseCount++;
      break;
    case 1:
      printVersion();
      exit(EXIT_SUCCESS);
    case '?':
      // getopt_long already printed an error message
      printUsage();
      exit(EXIT_FAILURE);
    }
  }

  auto clock = make_shared<SystemClock>();
  auto draw = make_shared<Draw>();

  Log::addAppender(make_shared<ConsoleLogAppender>());

  if (verboseCount == 1)
    Log::minLevel = LogLevel::Verbose;
  else if (verboseCount == 2)
    Log::minLevel = LogLevel::Trace;

  auto startTime = clock->getTimestamp();

  printBanner();

  logVersion();

  Config::initialise("configuration-metadata.json", configurationFile);

  unique_ptr<Camera> camera = nullptr;
  if (imageFeedFile == "")
  {
    auto videoPath = Config::getValue<string>("hardware.video-path");
    camera = make_unique<V4L2Camera>(videoPath);
  }
  else
  {
    auto _camera = make_unique<ImageFeedCamera>(imageFeedFile);
    _camera->setFPS(30);
    camera = move(_camera);
  }

  auto cameraController = make_unique<CameraController>(*camera);

  agent.reset(new Agent(clock, draw, move(camera), move(cameraController)));

  AdHocOptionTreeBuilder optionTreeBuilder;
  agent->setOptionTree(optionTreeBuilder.buildTree(agent.get(), clock, draw));

  Config::initialisationCompleted();

  signal(SIGTERM, &handleShutdownSignal);
  signal(SIGINT, &handleShutdownSignal);

  Log::info("boldhumanoid") << "Running Agent";

  agent->run();

  Log::info("boldhumanoid") << "Finished after " << clock->describeDurationSince(startTime);

  syslog("Exiting boldhumanoid process");

  return EXIT_SUCCESS;
}
