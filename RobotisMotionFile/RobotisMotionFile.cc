// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotismotionfile.hh"

#include "JointId/jointid.hh"
#include "motion/scripts/MotionScript/motionscript.hh"
#include "util/Log/log.hh"

#include <cstdio>
#include <vector>
#include <stdexcept>

using namespace bold;
using namespace bold::motion::scripts;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

RobotisMotionFile::RobotisMotionFile(string const& filePath)
{
  FILE *file = fopen(filePath.c_str(), "r+b");

  if (file == 0)
  {
    Log::error("RobotisMotionFile::RobotisMotionFile") << "Can not open motion file: " << filePath;
    throw runtime_error("Can not open motion file");
  }

  fseek(file, 0, SEEK_END);

  long actualSize = ftell(file);
  long expectedSize = sizeof(RobotisMotionFile::Page) * ((int)MAX_PAGE_ID + 1);
  if (actualSize != expectedSize)
  {
    Log::error("RobotisMotionFile::RobotisMotionFile") << "Invalid motion file size for " << filePath << " expecting " << expectedSize << " but got " << actualSize;
    fclose(file);
    throw runtime_error("Invalid motion file size");
  }

  // NOTE page zero is unused (all zeroed) and fails checksum validation, so skip it
  for (unsigned pageIndex = 1; pageIndex <= MAX_PAGE_ID; pageIndex++)
  {
    long position = (long)(sizeof(RobotisMotionFile::Page)*pageIndex);

    // TODO do we even have to seek if we read the file sequentially?
    if (fseek(file, position, SEEK_SET) != 0)
    {
      Log::error("RobotisMotionFile::RobotisMotionFile") << "Error seeking file position: " << position;
      fclose(file);
      throw runtime_error("Error seeking file position");
    }

    if (fread(&d_pages[pageIndex], 1, sizeof(RobotisMotionFile::Page), file) != sizeof(RobotisMotionFile::Page))
    {
      Log::error("RobotisMotionFile::RobotisMotionFile") << "Error reading page index: " << pageIndex;
      fclose(file);
      throw runtime_error("Error reading page");
    }

    if (!d_pages[pageIndex].isChecksumValid())
    {
      Log::error("RobotisMotionFile::RobotisMotionFile") << "Checksum invalid for page index: " << pageIndex;
      fclose(file);
      throw runtime_error("Checksum invalid for page");
    }
  }

  fclose(file);
}

void RobotisMotionFile::toDotText(ostream& out) const
{
  out << "digraph MotionFile {" << endl;

  for (unsigned pageIndex = 0; pageIndex <= MAX_PAGE_ID; pageIndex++)
  {
    // Exit is an unused feature that allows graceful finish up after 'stop' requested, as opposed to brake.

    int nextIndex = d_pages[pageIndex].nextPageIndex;
    int repeatCount = d_pages[pageIndex].repeatCount;

    if (nextIndex)
      out << "    " << pageIndex << " -> " << nextIndex << ";" << endl;

    if (repeatCount > 1)
      out << "    " << pageIndex << " -> " << pageIndex << " [label=\"repeat " << repeatCount << "\"];" << endl;
  }

  for (unsigned pageIndex = 0; pageIndex <= MAX_PAGE_ID; pageIndex++)
  {
    auto name = d_pages[pageIndex].name;
    int stepCount = d_pages[pageIndex].stepCount;
    int speed = d_pages[pageIndex].speed;
    int acceleration = d_pages[pageIndex].accelerationTime;
    string schedule = d_pages[pageIndex].schedule == (uint8_t)Page::MotionScriptPageSchedule::SPEED_BASE ? "Speed" : "Time";

    if (stepCount != 0)
      out << "    " << pageIndex << " [label=\"" << name
          << "\\nspeed=" << speed << " sched=" << schedule
          << "\\nid=" << pageIndex << " steps=" << stepCount
          << "\\nacc=" << acceleration
          << "\"];" << endl;
  }

  out << "}" << endl;
}

vector<uint8_t> RobotisMotionFile::getSequenceRootPageIndices() const
{
  bool exists[(uint16_t)MAX_PAGE_ID + 1] = {0,};
  bool isTarget[(uint16_t)MAX_PAGE_ID + 1] = {0,};

  for (int pageIndex = 0; pageIndex <= MAX_PAGE_ID; pageIndex++)
  {
    auto page = d_pages[pageIndex];
    if (page.stepCount == 0)
      continue;
    exists[pageIndex] = true;
    if (page.nextPageIndex != 0)
      isTarget[page.nextPageIndex] = true;
  }

  vector<uint8_t> indices;
  for (int pageIndex = 0; pageIndex <= MAX_PAGE_ID; pageIndex++)
  {
    if (exists[pageIndex] && !isTarget[pageIndex])
      indices.push_back(pageIndex);
  }
  return indices;
}

shared_ptr<MotionScript> RobotisMotionFile::toMotionScript(uint8_t rootPageIndex)
{
  vector<shared_ptr<MotionScript::Stage>> stages;

  Page const* page = &d_pages[rootPageIndex];
  string name = page->name;
  shared_ptr<MotionScript::Stage> currentStage;

  bool startNewStage = true;
  while (true)
  {
    if (startNewStage)
    {
      startNewStage = false;
      currentStage = make_shared<MotionScript::Stage>();
      stages.push_back(currentStage);

      // NOTE we skip accelerationTime and schedule
      currentStage->repeatCount = page->repeatCount;
      currentStage->speed = page->speed;

      for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
        currentStage->pGains[jointId - 1] = page->getPGain(jointId);
    }

    for (uint8_t i = 0; i < page->stepCount; i++)
    {
      auto step = MotionScript::KeyFrame();

      step.pauseCycles = page->steps[i].pause;
      step.moveCycles = page->steps[i].time;
      for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
        step.values[jointId - 1] = page->steps[i].position[jointId];

      currentStage->keyFrames.push_back(step);
    }

    if (page->nextPageIndex == 0)
      break;

    // If next page and prior page share same parameters, don't start a new 'stage' object

    Page const& nextPage = d_pages[page->nextPageIndex];

    for (uint8_t j = (uint8_t)JointId::MIN; !startNewStage && j <= (uint8_t)JointId::MAX; j++)
      startNewStage |= page->slopes[j] != nextPage.slopes[j];

    startNewStage |=
        page->repeatCount != 1 ||
        nextPage.repeatCount != 1 ||
        nextPage.speed != page->speed ||
        nextPage.schedule != page->schedule ||
        nextPage.accelerationTime != page->accelerationTime;

    page = &nextPage;
  }

  return make_shared<MotionScript>(name, stages, true, true, true);
}

///////////////////////////////// PAGE FUNCTIONS

uint8_t RobotisMotionFile::Page::calculateChecksum() const
{
  uint8_t const* pt = (uint8_t const *)this;

  uint8_t checksum = 0x00;
  for (unsigned i = 0; i < sizeof(RobotisMotionFile::Page); i++)
  {
    checksum += *pt;
    pt++;
  }

  return checksum;
}

bool RobotisMotionFile::Page::isChecksumValid() const
{
  uint8_t calculated = calculateChecksum();

  // valid if the checksum calculation sums to 0xFF
  return calculated == 0xFF;
}

void RobotisMotionFile::Page::updateChecksum()
{
  // set to zero for the calculation
  checksum = 0x00;

  // calculate and update
  checksum = (uint8_t)(0xff - calculateChecksum());
}

void RobotisMotionFile::Page::reset()
{
  memset(this, 0, sizeof(RobotisMotionFile::Page));

  schedule = (uint8_t)MotionScriptPageSchedule::TIME_BASE; // default to time-base
  repeatCount = 1;
  speed = DEFAULT_SPEED;
  accelerationTime = DEFAULT_ACCELERATION;

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    slopes[jointId] = DEFAULT_SLOPE;

  for (int i = 0; i < MAXNUM_STEPS; i++)
  {
    for (int j = 0; j < MAXNUM_POSITIONS; j++)
      steps[i].position[j] = INVALID_BIT_MASK;

    steps[i].pause = 0;
    steps[i].time = 0;
  }

  updateChecksum();
}
