// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <vector>
#include <memory>

namespace bold
{
  namespace motion
  {
    namespace scripts
    {
      class MotionScript;
    }
  }
  
  /** Structure of the Robotis motion script files.
   *
   * Provides the ability to load a Robotis motion file and convert its contents
   * into the Bold Hearts script format.
   *
   * The file has 256 pages at 512 bytes per page, totalling 131,072 bytes.
   */
  class RobotisMotionFile
  {
  private:
    /// Page numbers run from 1 to 255
    static const uint8_t MAX_PAGE_ID = 255;

    struct Page
    {
      enum class MotionScriptPageSchedule : uint8_t
      {
        SPEED_BASE = 0,
        TIME_BASE = 0x0a
      };

      enum
      {
        INVALID_BIT_MASK    = 0x4000,
        TORQUE_OFF_BIT_MASK = 0x2000
      };

      static const uint8_t DEFAULT_SPEED = 32;
      static const uint8_t DEFAULT_ACCELERATION = 32;
      static const uint8_t DEFAULT_SLOPE = 0x55;
      static const MotionScriptPageSchedule DEFAULT_SCHEDULE = MotionScriptPageSchedule::TIME_BASE;

      bool isChecksumValid() const;
      uint8_t calculateChecksum() const;

      /// Steps numbered 0 to 6 (7 steps per page max)
      static const uint8_t MAXNUM_STEPS = 7;
      static const uint8_t MAXNUM_POSITIONS = 31;
      /// Page names may be no longer than 13 characters (14 including terminator)
      static const uint8_t MAXNUM_NAME = 13;

      inline uint8_t getPGain(uint8_t jointId) const { return (256 >> (slopes[jointId]>>4)) << 2; }
      void updateChecksum();
      void reset();

      //
      // Step Structure (total 64 bytes)
      //

      struct Step
      {
        uint16_t position[MAXNUM_POSITIONS]; // Joint position   0~61
        uint8_t pause;                       // Pause time       62
        uint8_t time;                        // Time             63
      };

      //
      // Page Structure (total 512 bytes)
      //

      // Fixed values across whole page:

      char name[MAXNUM_NAME+1];  // Name              0~13
      uint8_t reserved1;           // Reserved1         14
      uint8_t repeatCount;         // Repeat count      15
      uint8_t schedule;            // schedule          16
      uint8_t reserved2[3];        // reserved2         17~19
      uint8_t stepCount;           // Number of step    20
      uint8_t reserved3;           // reserved3         21
      uint8_t speed;               // Speed             22
      uint8_t reserved4;           // reserved4         23
      uint8_t accelerationTime;    // Acceleration time 24
      uint8_t nextPageIndex;       // Link to next      25
      uint8_t exit;                // Link to exit      26
      uint8_t reserved5[4];        // reserved5         27~30
      uint8_t checksum;            // checksum          31
      uint8_t slopes[31];          // CW/CCW slope      32~62
      uint8_t reserved6;           // reserved6         63

      // Data per-step

      Step steps[MAXNUM_STEPS];  // Page steps        64~511
    };

    Page d_pages[(int)MAX_PAGE_ID + 1];

  public:
    RobotisMotionFile(std::string const& filePath);

    /// Gets all populated pages that are not continuation targets of other pages.
    std::vector<uint8_t> getSequenceRootPageIndices() const;

    /// Writes DOT markup of a directed graph showing the relationship between pages within this file.
    void toDotText(std::ostream& out) const;

    std::shared_ptr<motion::scripts::MotionScript> toMotionScript(uint8_t rootPageIndex);

    bool save(std::string const& filePath) const;

//     bool saveToJsonFile(uint8_t rootPageIndex, std::string const& filePath) const;
  };
}
