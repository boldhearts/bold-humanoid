// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "BodyModel/bodymodel.hh"

#include <array>
#include <map>

namespace bold
{
  class DarwinBodyModel : public BodyModel
  {
  
  public:
    DarwinBodyModel(std::string model);

    std::shared_ptr<Limb const> const& getRoot() const override { return d_torso; }
    std::shared_ptr<Joint const> const& getJoint(JointId jointId) const override;
    std::shared_ptr<Joint const> const& getJoint(std::string name) const override;
    std::shared_ptr<Limb const> const& getLimb(std::string name) const override;

    geometry2::Polygon2d getFootSolePolygon() const override;
    
    void visitLimbs(std::function<void(std::shared_ptr<Limb const> const&)> visitor) const
    {
      for (auto const& pair : d_limbByName)
        visitor(pair.second);
    }

    static std::map<JointId, int> jointAxisDirections;

  private:
    std::shared_ptr<Limb> createLimb(std::string name);
    std::shared_ptr<Joint> createJoint(JointId jointId, std::string name);

    std::shared_ptr<Limb const> d_torso;
    std::array<std::shared_ptr<Joint const>,23> d_jointById;
    std::map<std::string,std::shared_ptr<Joint const>> d_jointByName;
    std::map<std::string,std::shared_ptr<Limb const>> d_limbByName;
  };
}
