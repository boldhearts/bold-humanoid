// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/geometry2/Point/point.hh"
#include "Math/math.hh"
#include <Eigen/Core>


namespace bold
{
  template<typename T, int DIM = 2>
  class OcclusionRay
  {
  public:
    using Vector = std::vector<OcclusionRay<T, DIM>, Eigen::aligned_allocator<OcclusionRay<T, DIM>>>;

    OcclusionRay(geometry2::Point<T, DIM> near, geometry2::Point<T, DIM> far)
    : d_near(near),
      d_far(far)
    {}

    geometry2::Point<T, DIM> const& near() const { return d_near; }
    geometry2::Point<T, DIM> const& far() const { return d_far; }

    double constexpr norm() const { return (d_near - d_far).norm(); }
    double constexpr angle() const { return Math::angleToPoint(d_near); }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  private:
    geometry2::Point<T, DIM> d_near;
    geometry2::Point<T, DIM> d_far;
  };
}
