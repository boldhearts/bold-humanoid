SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_C_COMPILER     arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER   arm-linux-gnueabihf-g++)
SET(CMAKE_FIND_ROOT_PATH /usr/arm-linux-gnueabihf)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
