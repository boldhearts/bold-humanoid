// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <array>
#include "JointId/jointid.hh"
#include "TxRxPacket/txpacket.hh"
#include "BulkReadTable/bulkreadtable.hh"

namespace bold
{
  class BulkRead
  {
  public:
    /// Specifies that communication is to all connected devices
    static constexpr uint8_t ID_BROADCAST = 254;
    static constexpr uint8_t INSTRUCTION = 146;

    BulkRead(uint8_t cmDeviceID,
             std::array<uint8_t, 2> fsrDeviceIDs,
             uint8_t cmMin, uint8_t cmMax,
             uint8_t mxMin, uint8_t mxMax,
             uint8_t fsrMin = 0, uint8_t fsrMax = 0);

    BulkReadTable& getBulkReadData(uint8_t id);
    TxPacketBase const& getTxPacket() { return d_txPacket; }
    void clearError() { d_error = (uint8_t)-1; }
    void setError(uint8_t error) { d_error = error; }
    uint8_t getError() const { return d_error; }
    unsigned getRxLength() const { return d_rxLength; }

  private:
    void writeDeviceRequest(BulkReadTxPacketBuilder&,
                            uint8_t deviceId,
                            uint8_t startAddress, uint8_t endAddress,
                            uint8_t dataIdx);

    uint8_t d_cmDeviceId;
    bool d_withFSR;
    std::array<uint8_t, 2> d_fsrDeviceIDs;
    
    /// Position 0 for the CM730, last 2 for FSRs and the rest for MX28s
    std::array<BulkReadTable, 1 + (uint8_t)JointId::DEVICE_COUNT + 2> d_data;
    BulkReadTxPacket d_txPacket;

    unsigned d_rxLength;
    // TODO is this an MX28Alarm?
    uint8_t d_error;
  };
}

