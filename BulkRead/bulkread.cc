// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "bulkread.hh"

#include "BulkReadTable/bulkreadtable.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

BulkRead::BulkRead(uint8_t cmDeviceID,
                   array<uint8_t, 2> fsrDeviceIDs,
                   uint8_t cmMin, uint8_t cmMax,
                   uint8_t mxMin, uint8_t mxMax,
                   uint8_t fsrMin, uint8_t fsrMax)
  : d_cmDeviceId(cmDeviceID),
    d_fsrDeviceIDs(fsrDeviceIDs),
    d_txPacket(TxPacketBase::ID_BROADCAST, 0),
    d_error((uint8_t)-1)
{
  d_withFSR = fsrMin != 0 || fsrMax != 0;

  // Build a place for the data we read back
  d_data.fill(BulkReadTable());

  // We will receive 6 bytes per device plus an amount varying with the requested address range (added in below)
  d_rxLength = (1 + (unsigned)JointId::DEVICE_COUNT + (d_withFSR ? 2 : 0)) * 6;

  auto builder = BulkReadTxPacketBuilder(TxPacketBase::ID_BROADCAST);

  writeDeviceRequest(builder, cmDeviceID, cmMin, cmMax, 0);

  static_assert((uint8_t)JointId::MIN == 1, "Lowest JointId must be 1 for the data packing scheme to work. CM730 is at index 0.");

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    writeDeviceRequest(builder, jointId, mxMin, mxMax, jointId);

  if (d_withFSR)
  {
    auto dataIdx = (uint8_t)JointId::MAX + 1;
    for (auto id : fsrDeviceIDs)
      writeDeviceRequest(builder, id, fsrMin, fsrMax, dataIdx++);
  }

  d_txPacket = builder.getPacket();
  d_txPacket.setChecksum();
}

void BulkRead::writeDeviceRequest(BulkReadTxPacketBuilder& builder, uint8_t deviceId,
                                  uint8_t startAddress, uint8_t endAddress,
                                  uint8_t dataIdx)
  {
    ASSERT(startAddress < endAddress);

    uint8_t requestedByteCount = endAddress - startAddress + (uint8_t)1;

    Log::trace("BulkRead::writeDeviceRequest") << "Bytes requested: " <<
      (unsigned)endAddress << " - " << (unsigned)startAddress << " = " << (unsigned)requestedByteCount <<
      ", for device " << (unsigned)deviceId;

    d_rxLength += requestedByteCount;

    Log::trace("BulkRead::writeDeviceRequest") << "rxLength: " << d_rxLength;

    builder.addDeviceRequest(deviceId, startAddress, endAddress);

    auto& table = d_data.at(dataIdx);
    table.setStartAddress(startAddress);
    table.setLength(requestedByteCount);
  };

BulkReadTable& BulkRead::getBulkReadData(uint8_t id)
{
  ASSERT(id != 0);
  ASSERT(id == d_cmDeviceId || id == d_fsrDeviceIDs[0] || id == d_fsrDeviceIDs[1] ||
         (id >= (uint8_t)JointId::MIN && id <= (uint8_t)JointId::MAX));

  return d_data.at(id == d_cmDeviceId ? 0 : 
                   id == d_fsrDeviceIDs[0] ? (uint8_t)JointId::MAX + 1:
                   id == d_fsrDeviceIDs[1] ? (uint8_t)JointId::MAX + 2:
                   id);
}
