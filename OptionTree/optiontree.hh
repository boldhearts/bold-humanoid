// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <map>
#include <memory>
#include <vector>
#include <set>

namespace bold
{
  class FSMOption;
  class Option;

  /**
   * The option tree models the complete behaviour of the agent.
   * It is executed each think cycle after sensor data has been processed.
   * Consequences of its running include motion, network messages and internal state updates.
   */
  class OptionTree
  {
  public:
    OptionTree(std::shared_ptr<Option> root);

    /**
     * Executes the option tree, from the root down.
     */
    void run();

    /**
     * Registers an FSM option with the tree such that it may be debugged in Round Table.
     */
    void registerFsm(std::shared_ptr<FSMOption> fsm);

    /**
     * Gets all previously registered FSM options.
     */
    std::vector<std::shared_ptr<FSMOption>> getFSMs() const;

  private:
    std::map<std::string, std::shared_ptr<FSMOption>> d_fsmOptions;
    std::set<std::shared_ptr<Option>> d_optionsLastCycle;
    const std::shared_ptr<Option> d_root;
  };
}
