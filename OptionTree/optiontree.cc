// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "optiontree.hh"

#include "state/State/state.hh"
#include "state/StateObject/OptionTreeState/optiontreestate.hh"
#include "Option/FSMOption/fsmoption.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

OptionTree::OptionTree(std::shared_ptr<Option> root)
    : d_root(root) {}

void OptionTree::run() {
  vector<shared_ptr<Option>> ranOptions;
  vector<FSMStateSnapshot> fsmStates;

  StringBuffer buffer;
  Writer<StringBuffer> writer(buffer);
  writer.SetMaxDecimalPlaces(3);

  function<void(shared_ptr<Option>)> runOption;
  runOption = [this, &runOption, &writer, &ranOptions, &fsmStates](shared_ptr<Option> option) {
    Log::trace("OptionTree::run") << "Running " << option->getTypeName() << " option: " << option->getId();

    writer.StartObject();

    writer.String("id");
    writer.String(option->getId().c_str());
    writer.String("type");
    writer.String(option->getTypeName().c_str());

    // Remove options that run from the set of last cycle
    auto it = d_optionsLastCycle.find(option);
    if (it != d_optionsLastCycle.end())
      d_optionsLastCycle.erase(it);

    // Run it
    writer.String("run");
    writer.StartObject();
    vector<shared_ptr<Option>> subOptions = option->runPolicy(writer);
    writer.EndObject();

    // Recur through any sub-options
    if (!subOptions.empty()) {
      writer.String("children");
      writer.StartArray();
      for (auto const &child : subOptions)
        runOption(child);
      writer.EndArray();
    }

    writer.EndObject();

    // Remember the fact that we ran it
    ranOptions.push_back(option);

    shared_ptr<FSMOption> fsmOption = std::dynamic_pointer_cast<FSMOption, Option>(option);
    if (fsmOption)
      fsmStates.emplace_back(fsmOption->getId(), fsmOption->getCurrentState()->name);
  };

  runOption(d_root);

  // NOTE this is a bit weird/wasteful... we should generate the Document
  //      above and pass it instead of creating the string then parsing it...
  auto doc = make_unique<Document>();
  doc->Parse<0, UTF8<>>(buffer.GetString());

  State::make<OptionTreeState>(ranOptions, std::move(doc), fsmStates);

  // Reset options that ran in the last cycle but not in this one
  for (auto const &o : d_optionsLastCycle)
    o->reset();

  d_optionsLastCycle.clear();
  d_optionsLastCycle.insert(ranOptions.begin(), ranOptions.end());
}

void OptionTree::registerFsm(std::shared_ptr<FSMOption> fsm) {
  if (d_fsmOptions.count(fsm->getId())) {
    Log::error("OptionTree::registerFsm") << "Attempted to register FSM with id='" << fsm->getId()
                                          << "' more than once";
    throw runtime_error("Attempt to register FSM ID more than once");
  }

  d_fsmOptions[fsm->getId()] = fsm;

  // Validate the FSM
  if (!fsm->getStartState()) {
    Log::error("OptionTree::registerFsm") << "Attempt to add an FSMOption with ID '" << fsm->getId()
                                          << "' which has no start state";
    throw std::runtime_error("Attempt to add an FSMOption which has no start state");
  }

}

vector<shared_ptr<FSMOption>> OptionTree::getFSMs() const {
  vector<shared_ptr<FSMOption>> fsmOptions;
  for (auto const &fsm : d_fsmOptions)
    fsmOptions.push_back(fsm.second);
  return fsmOptions;
}
