// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

import * as color from './color';

export interface IHsvColor
{
    hue: number[];
    sat: number[];
    val: number[];
}

export function calculateColour(val: IHsvColor)
{
    var hAvg = (val.hue[0] + val.hue[1]) / 2,
        h = val.hue[0] < val.hue[1] ? hAvg : (hAvg + (255/2)) % 255,
        s = (val.sat[0] + val.sat[1]) / 2,
        v = (val.val[0] + val.val[1]) / 2;

    return new color.Hsv(h/255, s/255, v/255).toString();
}
