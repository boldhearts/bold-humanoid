// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../libs/lodash.d.ts" />
/// <reference path="../libs/require.d.ts" />

import {IObservable} from './IObservable';
import {ICloseable}  from './ICloseable';

export class Setting implements IObservable
{
    public path: string;
    public type: string;
    public isReadOnly: boolean;
    public defaultValue: any;
    public value: any;
    public description: string;
    public enumValues: {text:string;value:string}[];
    public min: number;
    public max: number;

    private callbacks: {(value:any):void}[] = [];

    constructor(settingData)
    {
        this.path = settingData.path;
        this.type = settingData.type;
        this.isReadOnly = settingData.readonly;
        this.defaultValue = settingData.default;
        this.value = settingData.value;
        this.description = settingData.description;

        if (this.type === "enum") {
            this.enumValues = settingData.values;
        }
        else if (this.type === "int" || this.type === "double") {
            this.min = settingData.min;
            this.max = settingData.max;
        }
    }

    public getDescription()
    {
        if (this.description)
            return this.description;

        var i = this.path.lastIndexOf('.');
        var desc = this.path.substr(i + 1).replace(/-/g, ' ');
        return desc.charAt(0).toUpperCase() + desc.slice(1)
    }

    public getValue()
    {
        return this.value;
    }

    public setValue(value)
    {
        require('./control').send({type: "setting", path: this.path, value: value});
    }

    public __setValue(value)
    {
        this.value = value;
        _.each(this.callbacks, callback => callback(value));
    }

    public track(callback): ICloseable
    {
        callback(this.value);
        this.callbacks.push(callback);

        return {
            close: () =>
            {
                var i = this.callbacks.indexOf(callback);
                if (i !== -1)
                    this.callbacks.splice(i, 1);
            }
        };
    }
}

export class LocalSetting extends Setting
{
    public setValue(value)
    {
        this.__setValue(value);
    }
    
}
