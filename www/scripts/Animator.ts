// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

export class Animator
{
    private needsRender: boolean;
    private stopAnimation: boolean;

    constructor(private callback: ()=>void)
    {}

    public start(): void
    {
        this.needsRender = true;
        this.stopAnimation = false;

        this.animate();
    }

    public stop(): void
    {
        this.stopAnimation = true;
    }

    public setRenderNeeded(): void
    {
        this.needsRender = true;
    }

    private animate()
    {
        if (this.stopAnimation)
            return;

        window.requestAnimationFrame(this.animate.bind(this));

        if (this.needsRender)
        {
            this.callback();
            this.needsRender = false;
        }
    }
}
