// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

export function polyfill(e: MouseEvent)
{
    if (typeof(e.offsetX) !== 'undefined' && typeof(e.offsetY) !== 'undefined')
        return;

    var target = <HTMLElement>(e.target || e.srcElement),
        rect = target.getBoundingClientRect();

    e.offsetX = e.clientX - rect.left;
    e.offsetY = e.clientY - rect.top;
}