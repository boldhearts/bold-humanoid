// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

import {IObservable} from '../IObservable';
import {ICloseable} from '../ICloseable';

export class Trackable<T>
{
    private callbacks: { (value: T, oldValue?: T): void; }[];

    constructor(private value: T = null)
    {
        this.callbacks = [];
    }

    public track(callback: (value: T, oldValue?: T) => void): ICloseable
    {
        this.callbacks.push(callback);
        if (typeof (this.value) !== 'undefined' && this.value !== null)
            callback(this.value, undefined);

        return { close: () => this.removeCallback(callback) };
    }

    public removeCallback(callback: (value: T, oldValue?: T) => void)
    {
        var index = this.callbacks.indexOf(callback);
        if (index === -1)
        {
            console.warn("Attempt to remove an unregistered callback from Trackable");
            return false;
        }
        this.callbacks.splice(index, 1);
        return true;
    }

    public setValue(value: T)
    {
        if (this.value === value)
            return;

        var oldValue = this.value;
        this.value = value;
        this.triggerChange(oldValue);
    }

    public getValue()
    {
        return this.value;
    }

    public triggerChange(oldValue?: T)
    {
        for (var i = 0; i < this.callbacks.length; i++) {
            this.callbacks[i](this.value, oldValue);
        }
    }

    public onchange(callback: (value: T)=>void): ICloseable
    {
        this.callbacks.push(callback);

        return { close: () => this.removeCallback(callback) };
    }
}
