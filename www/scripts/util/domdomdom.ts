// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 * @date 19 June 2014
 */

export function dom(...values: any[]): HTMLElement
{
    if (values.length === 0)
        return undefined;

    var element: HTMLElement;

    for (var i = 0; i < values.length; i++)
    {
        var val = values[i],
            valType = typeof(val);

        if (valType === "object")
        {
            if (val instanceof Element)
            {
                if (i === 0)
                {
                    element = val;
                }
                else
                {
                    console.assert(!!element);
                    element.appendChild(val);
                }
            }
            else
            {
                console.assert(!!element);
                var props = Object.getOwnPropertyNames(val);
                for (var i = 0; i < props.length; i++)
                {
                    if (element.style.hasOwnProperty(props[i]))
                        element.style[props[i]] = val[props[i]];
                }
            }
        }
        else if (valType === "string")
        {
            if (i === 0)
            {
                // String in the first position is a tag or tag/class
                var dotIndex = val.indexOf('.'),
                    hashIndex = val.indexOf('#');

                if (dotIndex === -1)
                {
                    // string is just a tag name
                    element = document.createElement(val);
                }
                else
                {
                    // String is a tag and a class
                    console.assert(dotIndex !== 0);
                    element = document.createElement(val.substring(0, dotIndex));
                    element.className = val.substring(dotIndex + 1);
                }
            }
            else
            {
                console.assert(element != null);
                element.textContent = val;
            }
        }
        else
        {
            console.warn("Unexpected value type: " + valType);
        }
    }

    return element;
}
