// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as geometry from '../util/geometry';

export class Trace
{
    private width: number;
    private height: number;
    private scale: number = 1;

    private dotSize: number = 6;
    private lineStyle: string = '#C44425';
    private fadeRate: number = 0.02;

    private canvas: HTMLCanvasElement;
    private overlayCanvas: HTMLCanvasElement;

    private context: CanvasRenderingContext2D;

    private lastPosition: geometry.IPoint2;

    public element: HTMLDivElement;

    constructor(width: number, height: number)
    {
        this.width = width;
        this.height = height;

        this.canvas = document.createElement('canvas');
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.context = this.canvas.getContext('2d');

        this.overlayCanvas = document.createElement('canvas');
        this.overlayCanvas.style.position = 'absolute';
        this.overlayCanvas.style.left = '0';
        this.overlayCanvas.width = this.width;
        this.overlayCanvas.height = this.height;
        
        this.element = document.createElement('div');
        this.element.className = '2d-trace';
        this.element.style.width = this.width + 'px';
        this.element.style.height = this.height + 'px';
        this.element.appendChild(this.canvas);
        this.element.appendChild(this.overlayCanvas);

        this.setScale(1);
    }

    public setScale(scale: number)
    {
        this.scale = scale;

        var ctx = this.overlayCanvas.getContext('2d');
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.translate(this.width / 2, this.height / 2);
        ctx.scale(this.scale, -this.scale);
    }

    public setDotSize(dotSize: number)
    {
        this.dotSize = dotSize;
    }

    public getOverlayCanvas()
    {
        return this.overlayCanvas;
    }

    public addValue(xValue: number, yValue: number)
    {
        var ctx = this.context;
        
        ctx.save();
        ctx.fillStyle = "rgba(0, 0, 0, " + (1.0 - this.fadeRate) + ")";
        ctx.globalCompositeOperation = "destination-in";
        ctx.fillRect(0, 0, this.width, this.height);
        ctx.restore();

        if (xValue && yValue)
        {
            ctx.save();
            ctx.translate(this.width / 2, this.height / 2);
            ctx.scale(this.scale, -this.scale);

            // draw line segment
            if (this.lastPosition) {
                ctx.strokeStyle = this.lineStyle;
                ctx.lineWidth = this.dotSize;
                ctx.lineCap = 'round';
                ctx.beginPath();
                ctx.moveTo(this.lastPosition.x, this.lastPosition.y);
                ctx.lineTo(xValue, yValue);
                ctx.stroke();
            }

            this.lastPosition = {x: xValue, y: yValue};
        }
        else
            this.lastPosition = null;

        ctx.restore();

    }
}
