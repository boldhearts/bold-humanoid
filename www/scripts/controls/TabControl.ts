// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts"/>

interface ITabItem
{
    thumb: HTMLElement;
    content: HTMLDivElement;
}

export class TabControl
{
    private thumbContainer: HTMLUListElement;
    private contentContainer: HTMLDivElement;
    private items: ITabItem[] = [];

    constructor(element: HTMLDListElement)
    {
        console.assert(!!element);

        // Create new elements
        var newElement = document.createElement('div');
        newElement.className = 'tab-control';
        this.contentContainer = document.createElement('div');
        this.thumbContainer = document.createElement('ul');
        this.thumbContainer.className = 'thumbs';

        // Insert them
        newElement.appendChild(this.thumbContainer);
        newElement.appendChild(this.contentContainer);
        element.parentNode.insertBefore(newElement, element);

        // Finally, remove the child element
        element.parentNode.removeChild(element);

        // Find all tabs, and build our tab items
        var nodes = _.filter<HTMLElement>(element.childNodes, n => n.nodeType === 1);
        for (var i = 0; i < nodes.length - 1; i += 2)
        {
            var dt = nodes[i],
                dd = nodes[i + 1];

            console.assert(dt.localName === 'dt' || dd.localName === 'dd');

            var thumb = document.createElement('li');
            thumb.dataset['index'] = (i / 2).toString();
            thumb.addEventListener('click', e => this.setSelectedIndex(parseInt((<HTMLLIElement>e.target).dataset['index'])));

            // Move the thumb
            while (dt.childNodes.length !== 0)
                thumb.appendChild(dt.childNodes[0]);

            this.thumbContainer.appendChild(thumb);

            var content = document.createElement('div');

            // Move the tab page
            while (dd.childNodes.length !== 0)
                content.appendChild(dd.childNodes[0]);

            this.items.push({thumb: thumb, content: content});
        }

        this.setSelectedIndex(0);
    }

    public setSelectedIndex(index: number)
    {
        // Remove any existing content
        while (this.contentContainer.childNodes.length !== 0)
            this.contentContainer.removeChild(this.contentContainer.children[0]);

        // Set all thumbs as unselected
        for (var i = 0; i < this.items.length; i++)
            this.items[i].thumb.classList.remove('selected');

        this.items[index].thumb.classList.add('selected');
        this.contentContainer.appendChild(this.items[index].content);
    }
}
