// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

import * as control from '../control';

export class HeadControls
{
    public element: HTMLDivElement;

    constructor()
    {
        this.element = document.createElement('div');
        this.element.className = "head-controls";

        control.buildAction('head-module.move-left', this.element);
        control.buildAction('head-module.move-up', this.element);
        control.buildAction('head-module.move-down', this.element);
        control.buildAction('head-module.move-right', this.element);

        control.buildAction('head-module.move-home', this.element);
        control.buildAction('head-module.move-zero', this.element);
    }
}
