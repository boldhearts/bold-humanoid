// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

import {dom} from "../util/domdomdom";

export class Legend
{
    public element: HTMLElement;

    constructor(items: {colour: string; name: string}[])
    {
        this.element = dom('ul.legend');

        for (var i = 0; i < items.length; i++)
            this.addItem(items[i].name,  items[i].colour);
    }

    private addItem(name: string, colour: string)
    {
        dom(this.element,
            dom("li.tile",
                dom("div.tile", {background: colour}),
                dom("div.label", name)
            )
        );
    }
}
