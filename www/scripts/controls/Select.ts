// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />

import {IObservable} from '../IObservable';
import {ICloseable} from '../ICloseable';

export class Select implements ICloseable
{
    private static nextIdCounter: number = 0;

    public element: HTMLSelectElement;
    private closeable: ICloseable;

    constructor(observable: IObservable, items: {value:any; text:string}[], id?: string)
    {
        this.element = document.createElement('select');
        this.element.id = id || 'select-number-' + (Select.nextIdCounter++);

        _.each(items, item =>
        {
            var option = document.createElement('option');
            option.text = item.text;
            option.selected = observable.getValue() == item.value;
            this.element.appendChild(option);
        });

        this.element.addEventListener('change', () => {
            observable.setValue(items[this.element.selectedIndex].value);
        });

        this.closeable = observable.track(value =>
        {
            for (var i = 0; i < items.length; i++)
            {
                if (items[i].value == value)
                {
                    this.element.selectedIndex = i;
                    return;
                }
            }
            console.assert(false && !!"Observed value changed to something not specified in original items");
        });
    }

    public close(): void
    {
        this.closeable.close();
        delete this.closeable;
    }
}
