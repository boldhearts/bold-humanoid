// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

import {Trackable} from '../util/Trackable';

export class Checkbox
{
    private static nextIdCounter: number = 0;

    public element: HTMLDivElement;

    constructor(labelText: string, trackable: Trackable<boolean>, id?: string)
    {
        this.element = document.createElement('div');
        this.element.className = 'checkbox control';

        if (!id)
            id = 'checkbox-number-' + (Checkbox.nextIdCounter++);

        var input = <HTMLInputElement>document.createElement('input');
        input.type = 'checkbox';
        input.id = id;
        input.addEventListener('change', e => trackable.setValue(input.checked));
        this.element.appendChild(input);

        var label = <HTMLLabelElement>document.createElement('label');
        label.htmlFor = id;
        label.textContent = labelText;
        this.element.appendChild(label);

        // TODO LEAK this closeable should be closed at some point
        trackable.track(checked => input.checked = checked);
    }
}
