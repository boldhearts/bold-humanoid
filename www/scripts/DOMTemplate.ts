// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../libs/handlebars.d.ts" />

var parser: DOMParser = new DOMParser();

export class DOMTemplate
{
    private template: (context?: any, options?: any) => string;
    private contentType: string;

    static forId(templateId: string, contentType?: string) : DOMTemplate
    {
        var templateElement = document.getElementById(templateId);

        if (!templateElement) {
            console.error('No element found with id', templateId);
            return;
        }

        return DOMTemplate.forText(templateElement.textContent);
    }

    static forText(templateText: string, contentType?: string): DOMTemplate
    {
        console.assert(!!templateText);

        var template = new DOMTemplate();
        template.contentType = contentType;
        template.template = Handlebars.compile(templateText);
        return template;
    }

    public create(data?: any): Node
    {
        var obj = parser.parseFromString(this.template(data), this.contentType || "text/html");

        console.assert(!!obj);

        if (this.contentType == null || this.contentType === "text/html") {
            console.assert(!!obj.body);
            if (obj.body.children.length !== 1)
                console.error("Template specifies " + obj.body.children.length + " children, where only one is allowed.");
            return obj.body.firstElementChild;
        }
        else {
            return obj;
        }
    }
}
