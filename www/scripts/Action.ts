// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../libs/require.d.ts" />

export class Action
{
    public label: string;
    public id: string;
    public hasArguments: boolean;

    constructor(actionData: {id: string; label: string; hasArguments: boolean})
    {
        this.id = actionData.id;
        this.label = actionData.label;
        this.hasArguments = actionData.hasArguments;
    }

    public activate(args?: any)
    {
        console.assert(this.hasArguments === !!args);

        var message: any = {
            type: "action",
            id: this.id
        };

        if (this.hasArguments)
            message.args = args;

        require('./control').send(message);
    }
}
