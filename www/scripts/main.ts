// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../libs/lodash.d.ts"/>
/// <reference path="../libs/jquery.d.ts"/>

import * as control from './control';
import * as constants from './constants';
import * as data from './data';
import * as util from './util';

import {ModuleHost} from './controls/ModuleHost';
import {Setting} from './Setting';

import {Agent2dModule} from './modules/Agent2dModule';
import {AudioModule} from './modules/AudioModule';
import {AnimatorModule} from './modules/AnimatorModule';
import {CameraModule} from './modules/CameraModule';
import {CommsModule} from './modules/CommsModule';
import {ConfigModule} from './modules/ConfigModule';
import {DrawbridgeModule} from './modules/DrawbridgeModule';
import {FSRModule} from './modules/FSRModule';
import {GameStateModule} from './modules/GameStateModule';
import {HardwareModule} from './modules/HardwareModule';
import {HistogramModule} from './modules/HistogramModule';
import {IMUModule} from './modules/IMUModule';
import {LoadModule} from './modules/LoadModule';
import {LocaliserModule} from './modules/LocaliserModule';
import {LogModule} from './modules/LogModule';
import {MotionScriptModule} from './modules/MotionScriptModule';
import {MotionTimingModule} from './modules/MotionTimingModule';
import {OptionTreeModule} from './modules/OptionTreeModule';
import {OrientationModule} from './modules/OrientationModule';
import {StateDumpModule} from './modules/StateDumpModule';
import {TeamModule} from './modules/TeamModule';
import {ThinkTimingModule} from './modules/ThinkTimingModule';
import {TrajectoryModule} from './modules/TrajectoryModule';
import {VisionModule} from './modules/VisionModule';
import {VoiceModule} from './modules/VoiceModule';
import {WalkModule} from './modules/WalkModule';
import {World2dModule} from './modules/World2dModule';
import {World3dModule} from './modules/World3dModule';

//        if (!WebGLDetector.webgl)
//            WebGLDetector.addGetWebGLMessage();

var loadUi = (settings?: Setting[]) =>
{
    if (settings)
        constants.update(settings);

    var moduleHost = new ModuleHost('#header-module-links');

    moduleHost.register(new CameraModule());
    moduleHost.register(new VisionModule());
    moduleHost.register(new World3dModule());
    moduleHost.register(new World2dModule());
    moduleHost.register(new Agent2dModule());
    moduleHost.register(new FSRModule());
    moduleHost.register(new ThinkTimingModule());
    moduleHost.register(new MotionTimingModule());
    moduleHost.register(new LocaliserModule());
    moduleHost.register(new WalkModule());
    moduleHost.register(new AudioModule());
    moduleHost.register(new TeamModule());
    moduleHost.register(new CommsModule());
    moduleHost.register(new HardwareModule());
    moduleHost.register(new HistogramModule());
    moduleHost.register(new IMUModule());
    moduleHost.register(new OrientationModule());
    moduleHost.register(new OptionTreeModule());
    moduleHost.register(new GameStateModule());
    moduleHost.register(new MotionScriptModule());
    moduleHost.register(new LoadModule());
    moduleHost.register(new TrajectoryModule());
    moduleHost.register(new VoiceModule());
    moduleHost.register(new AnimatorModule());
    moduleHost.register(new DrawbridgeModule());
    moduleHost.register(new ConfigModule());
    moduleHost.register(new StateDumpModule());
    moduleHost.register(new LogModule());

    $('#module-container').hide().fadeIn();
    $('#loading-indicator').fadeOut(function() { $(this).remove(); });

    moduleHost.load();
};

control.withSettings('', loadUi);

var disconnectLink = <HTMLAnchorElement>document.querySelector('div#header-content a.disconnect');
disconnectLink.addEventListener('click', e =>
{
    e.preventDefault();

    if (data.isAllDisconnected())
        data.reconnectAll();
    else
        data.disconnectAll();
});

data.onConnectionChanged(() =>
{
    disconnectLink.textContent = data.isAllDisconnected() ? "reconnect" : "disconnect";
});

var onerror = () =>
{
    // Allow manual override. Useful when developing Round Table when no agent
    // is available.
    if (window.location.search.indexOf('forceload') !== -1)
    {
        loadUi();
        return;
    }

    $('#loading-indicator').find('h1').text('No Connection');
    $('#bouncer').fadeOut(function() { $(this).remove(); });
};

control.connect(onerror);

document.getElementById('logo').addEventListener('click', e => util.toggleFullScreen());

// Not sure why this copy is required, but it seems to be...
var toggleNightMode = constants.toggleNightMode;
document.querySelector("#header h1").addEventListener('click', e => { toggleNightMode(); });

document.documentElement.classList.add("initialised");
