// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />

import * as constants from '../constants';
import * as control from '../control';
import * as data from '../data';
import * as math from '../util/math';
import * as state from '../state';
import * as util from '../util';

import {DOMTemplate} from '../DOMTemplate';
import {Module} from '../Module';

var template = DOMTemplate.forId('team-module-template');

export class TeamModule extends Module
{
    constructor()
    {
        super('team', 'team', {fullScreen: true});
    }

    public load(width: number)
    {
        this.closeables.add(new data.Subscription<state.Team>(
            constants.protocols.teamState,
            {
                onmessage: this.onTeamState.bind(this),
                onerror: e => this.element.style.opacity = '0.4'
            }
        ));
    }

    private onTeamState(data: state.Team)
    {
        util.clearChildren(this.element);

        if (data.players.length === 0)
            return;

        var newestTime = _.max<number>(_.map(data.players, player => player.updateTime));
        var myTeamNumber = _.findWhere<state.PlayerData>(data.players, player => player.isMe).team;

        data.players.sort((a,b) => a.unum < b.unum ? -1 : a.unum === b.unum ? 0 : 1);

        var playerViewModels = _.map<state.PlayerData,any>(
            data.players,
            player => ({
                unum: player.unum,
                team: player.team,
                activity: state.getPlayerActivityName(player.activity),
                state: state.getPlayerStatusName(player.status),
                role: state.getPlayerRoleName(player.role),
                pos: '[' + (player.pos[0] != null ? player.pos[0].toFixed(2) : 'null') + ', '
                         + (player.pos[1] != null ? player.pos[1].toFixed(2) : 'null') + '] '
                         + (player.pos[2] != null ? Math.round(math.radToDeg(player.pos[2])).toString() : 'null') + '°',
                posConfidence: player.posConfidence.toFixed(2),
                ballRelative: !player.ballRelative.length ? '-' : '[' + player.ballRelative[0].toFixed(2) + ', ' + player.ballRelative[1].toFixed(2) + ']',
                ballDistance: !player.ballRelative.length ? '-' : Math.sqrt(Math.pow(player.ballRelative[0], 2) + Math.pow(player.ballRelative[1], 2)).toFixed(2),
                age: (newestTime - player.updateTime).toString(),

                clazz: (player.isMe ? 'me ' : '')
                    + (newestTime - player.updateTime > 2000 ? 'old ' : '')
                    + (player.team === myTeamNumber ? 'teammate' : 'opponent')
            }));

        this.element.appendChild(template.create(playerViewModels));
    }
}
