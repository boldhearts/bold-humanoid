// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />

import * as constants from '../constants';
import * as data from '../data';

import {DOMTemplate} from '../DOMTemplate';
import {ICloseable} from '../ICloseable';
import {Module} from '../Module';

var moduleTemplate = DOMTemplate.forId('state-dump-module-template');

export class StateDumpModule extends Module
{
    private subscription: ICloseable;
    private textElement: HTMLDivElement;

    constructor()
    {
        super('state', 'state dump');
    }

    public load(width: number)
    {
        var templateRoot = <HTMLElement>moduleTemplate.create();

        this.element.appendChild(templateRoot);

        this.textElement = <HTMLDivElement>templateRoot.querySelector('div.json-text');

        var select = <HTMLSelectElement>templateRoot.querySelector('select');

        var none = document.createElement('option');
        none.value = '';
        none.text = '(None)';
        select.appendChild(none);

        _.each(constants.allStateProtocols, stateName =>
        {
            var option = document.createElement('option');
            option.value = stateName;
            option.text = stateName;
            select.appendChild(option);
        });

        select.addEventListener('change', () =>
        {
            var protocol = (<HTMLOptionElement>select.options[select.selectedIndex]).value;

            if (this.subscription)
                this.subscription.close();

            this.textElement.textContent = protocol ? 'Waiting for an update...' : '';

            if (protocol !== '') {
                this.subscription = new data.Subscription<any>(
                    protocol,
                    {
                        onmessage: this.onState.bind(this)
                    }
                );
            }
        });
    }

    public unload()
    {
        delete this.textElement;

        if (this.subscription)
            this.subscription.close();
    }

    private onState(data: any)
    {
        this.textElement.textContent = JSON.stringify(data, undefined, 2);
    }
}
