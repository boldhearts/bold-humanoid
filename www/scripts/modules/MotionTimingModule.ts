// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

import * as constants from '../constants';

import {Module} from '../Module';
import {TimingPane} from '../modules/TimingPane';

export class MotionTimingModule extends Module
{
    private pane: TimingPane;

    constructor()
    {
        super('motion-timing', 'motion timing', {fullScreen: true});
        this.pane = new TimingPane(constants.protocols.motionTimingState, 125/*fps*/);
    }

    public load(width: number)
    {
        this.pane.load(this.element);
        this.pane.onResized(width, this.isFullScreen.getValue());
    }

    public unload()
    {
        this.pane.unload();
    }

    public onResized(width: number, height: number, isFullScreen: boolean)
    {
        this.pane.onResized(width, isFullScreen);
    }
}
