// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />

import * as constants from '../constants';
import * as control from '../control';
import * as mapping from '../controls/mapping';
import * as geometry from '../util/geometry';

import {HeadControls} from '../controls/HeadControls';
import {Module} from '../Module';
import {Trackable} from '../util/Trackable';

export class World2dModule extends Module
{
    private map: mapping.Map;

    constructor()
    {
        super('world-2d', '2d world', {fullScreen: true});
    }

    public load(width: number)
    {
        var mapDiv = document.createElement('div');
        mapDiv.className = 'map-layer-container';

        var checkboxDiv = document.createElement('div');
        checkboxDiv.className = 'map-layer-checkboxes';

        var transform = new Trackable<geometry.Transform>();
        this.map = new mapping.Map(mapDiv, checkboxDiv, transform);

        var hoverInfo = document.createElement('div');
        hoverInfo.className = 'hover-info';

        this.map.hoverPoint.track(p => { hoverInfo.textContent = p ? p.x.toFixed(2) + ', ' + p.y.toFixed(2) : ''; });

        var localiserControlContainer = document.createElement('div');
        localiserControlContainer.className = 'localiser-controls';
        control.buildActions('localiser', localiserControlContainer);

        this.element.appendChild(mapDiv);
        this.element.appendChild(hoverInfo);
        this.element.appendChild(checkboxDiv);
        this.element.appendChild(new HeadControls().element);
        this.element.appendChild(localiserControlContainer);

        this.map.addLayer(new mapping.FieldLineLayer(transform));
        this.map.addLayer(new mapping.ParticleLayer(transform));
        this.map.addLayer(new mapping.ObservedLineLayer(transform));
        this.map.addLayer(new mapping.AgentPositionLayer(transform));
        this.map.addLayer(new mapping.VisibleFieldPolyLayer(transform));
        this.map.addLayer(new mapping.BallPositionLayer(transform));
        this.map.addLayer(new mapping.ObservedGoalLayer(transform));
        this.map.addLayer(new mapping.OcclusionAreaLayer(transform));
        this.map.addLayer(new mapping.TeamLayer(transform));

        var fieldAspect = constants.fieldY / constants.fieldX;
        this.map.setPixelSize(width, fieldAspect * width);
    }

    public unload()
    {
        this.map.unload();
    }

    public onResized(width: number, height: number, isFullScreen: boolean)
    {
        // Make space for bottom controls if fullscreen
        if (isFullScreen)
            height -= 50;
        else
            height = 480;

        this.map.setPixelSize(width, height);
    }
}
