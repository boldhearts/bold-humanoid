// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />
/// <reference path="../../libs/smoothie.d.ts" />

import * as constants from '../constants';
import * as control from '../control';
import * as data from '../data';
import * as state from '../state';

import {Module} from '../Module';

var chartOptions = {
    grid: {
        strokeStyle: 'rgb(40, 40, 40)',
        fillStyle: 'rgb(0, 0, 0)',
        lineWidth: 1,
        millisPerLine: 250,
        verticalSections: 6,
        sharpLines: true,
        borderVisible: false
    },
    labels: {
        fillStyle: '#ffffff'
    },
    minValue: 0
};

var chartHeight = 150;

export class CommsModule extends Module
{
    private canvas: HTMLCanvasElement;
    private chart: SmoothieChart;
    private ignoreSeries: TimeSeries;
    private gameSeries: TimeSeries;

    constructor()
    {
        super('comms', 'communication', {fullScreen: true});
    }

    public load(width: number)
    {
        var controls = document.createElement('div');
        control.buildSettings("game-controller", controls, this.closeables);
        this.element.appendChild(controls);

        this.chart = new SmoothieChart(chartOptions);
        this.canvas = document.createElement('canvas');
        this.canvas.height = chartHeight;
        this.canvas.width = width;
        this.element.appendChild(this.canvas);

        this.ignoreSeries = new TimeSeries();
        this.gameSeries = new TimeSeries();
        this.chart.addTimeSeries(this.ignoreSeries, { strokeStyle: 'rgb(128, 0, 0)', fillStyle: 'rgba(0, 255, 0, 0.3)', lineWidth: 1 });
        this.chart.addTimeSeries(this.gameSeries,   { strokeStyle: 'rgb(0, 0, 255)', fillStyle: 'rgba(0, 255, 0, 0.3)', lineWidth: 1 });
        this.chart.streamTo(this.canvas, /*delayMs*/ 0);

        this.closeables.add(new data.Subscription<state.MessageCount>(
            constants.protocols.messageCountState,
            {
                onmessage: this.onMessageCountState.bind(this)
            }
        ));

        this.closeables.add({ close: () => this.chart.stop() });
    }

    private onMessageCountState(data: state.MessageCount)
    {
        var time = new Date().getTime();

        this.ignoreSeries.append(time, data.ignoredMessages);
        this.gameSeries.append(time, data.gameControllerMessages);
    }

    public onResized(width: number, height: number, isFullScreen: boolean)
    {
        this.canvas.width = width;
    }
}
