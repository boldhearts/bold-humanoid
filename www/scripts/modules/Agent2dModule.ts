// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

/// <reference path="../../libs/lodash.d.ts" />

import * as constants from '../constants';
import * as control from '../control';
import * as geometry from '../util/geometry';
import * as mapping from '../controls/mapping';

import {HeadControls} from '../controls/HeadControls';
import {Module} from '../Module';
import {Trackable} from '../util/Trackable';

export class Agent2dModule extends Module
{
    private map: mapping.Map;

    constructor()
    {
        super('agent-2d', '2d agent', {fullScreen: true});
    }

    public load(width: number)
    {
        var mapDiv = document.createElement('div');
        mapDiv.className = 'map-layer-container';

        var checkboxDiv = document.createElement('div');
        checkboxDiv.className = 'map-layer-checkboxes';

        var transform = new Trackable<geometry.Transform>();

        this.map = new mapping.Map(mapDiv, checkboxDiv, transform);

        var hoverInfo = document.createElement('div');
        hoverInfo.className = 'hover-info';

        this.map.hoverPoint.track(p => { hoverInfo.innerHTML = p ? p.x.toFixed(2) + ', ' + p.y.toFixed(2) + '<br>' + Math.sqrt(p.x*p.x + p.y*p.y).toFixed(2) : ''; });

        var localiserControlContainer = document.createElement('div');
        localiserControlContainer.className = 'localiser-controls';
        control.buildActions('localiser', localiserControlContainer);

        this.element.appendChild(mapDiv);
        this.element.appendChild(hoverInfo);
        this.element.appendChild(checkboxDiv);
        this.element.appendChild(new HeadControls().element);
        this.element.appendChild(localiserControlContainer);

        this.map.addLayer(new mapping.AgentReferenceLayer(transform));
        this.map.addLayer(new mapping.AgentObservedLineLayer(transform));
        this.map.addLayer(new mapping.AgentObservedLineJunctionLayer(transform));
        this.map.addLayer(new mapping.AgentVisibleFieldPolyLayer(transform));
        this.map.addLayer(new mapping.AgentBallPositionLayer(transform));
        this.map.addLayer(new mapping.AgentObservedPlayerLayer(transform));
        this.map.addLayer(new mapping.AgentStationaryMapLayer(transform));
        this.map.addLayer(new mapping.AgentObservedGoalLayer(transform));
        this.map.addLayer(new mapping.AgentOcclusionAreaLayer(transform));
        this.map.addLayer(new mapping.AgentDrawingLayer(transform));

        this.map.setPixelSize(width, 600);
    }

    public onResized(width: number, height: number, isFullScreen: boolean)
    {
        // Make space for bottom controls if fullscreen
        if (isFullScreen)
            height -= width > 1024 ? 50 : 70;
        else
            height = 600;

        var scale = this.map.transform.getValue().getScale();
        this.map.transform.setValue(
            new geometry.Transform()
            .translate(width / 2, height / 2)
            .scale(scale, -scale));

        this.map.setPixelSize(width, height);
    }
}
