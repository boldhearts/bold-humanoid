// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes https://drewnoakes.com
 */

/// <reference path="../../libs/d3.d.ts" />

import * as data from '../data';
import * as state from '../state';
import * as constants from '../constants';

import {Module} from '../Module';

var countFormatter: (value: number) => string = d3.format(",d"),
    chartHeight = 240,
    chartWidth = 640,
    axisSpace = 25;

export class AudioModule extends Module
{
    private svg: SVGElement;
    private chart: D3.Selection;
    private xScale: D3.Scale.LinearScale;
    private yScale: D3.Scale.LinearScale;
    private xAxis: D3.Svg.Axis;
    private yAxis: D3.Svg.Axis;

    constructor()
    {
        super('audio', 'audio');
    }

    public load(width: number)
    {
        this.closeables.add(new data.Subscription<state.AudioPowerSpectrum>(
            constants.protocols.audioPowerSpectrumState,
            {
                onmessage: this.onAudioPowerSpectrumState.bind(this)
            }
        ));

        this.svg = <SVGElement>document.createElementNS(d3.ns.prefix.svg, 'svg');

        this.element.appendChild(this.svg);

        this.chart = d3.select(this.svg)
                .attr('class', 'chart')
                .attr('width', chartWidth)
                .attr('height', chartHeight);

        this.xScale = d3.scale.linear()
           .range([axisSpace, chartWidth - 10]);

        this.yScale = d3.scale.linear()
           .domain([50, 0])
           .range([15, chartHeight - axisSpace]);

        this.yAxis = d3.svg.axis().orient('left').scale(this.yScale).ticks(10);
        this.xAxis = d3.svg.axis().orient('bottom');

        d3.select(this.svg)
            .append('g')
            .attr('class', 'y-axis axis')
            .attr('transform', 'translate(' + axisSpace + ',0)')
            .call(this.yAxis);

        d3.select(this.svg)
            .append('g')
            .attr('class', 'x-axis axis')
            .attr('transform', 'translate(0,' + (chartHeight - axisSpace) + ')');

        d3.select(this.svg)
            .append('g')
            .attr('class', 'series');
    }

    public unload()
    {
        delete this.svg;
        delete this.chart;
        delete this.xScale;
        delete this.yScale;
    }

    private onAudioPowerSpectrumState(data: state.AudioPowerSpectrum)
    {
        var width = Math.max(2, chartWidth / data.dbLevels.length);

        var markerHeight = 5;

        this.xScale.domain([0, data.dbLevels.length]);

        // TODO try using a log scale for frequency
        // TODO only rebuild this scale when needed
        this.xAxis.scale(d3.scale.linear().range([axisSpace, chartWidth - 10]).domain([0, data.maxHertz]));
        d3.select(this.svg).select('g.x-axis').call(this.xAxis);

        var bars = this.chart.select('g.series')
            .selectAll('rect')
            .data(data.dbLevels);

        bars.enter()
            .append('rect')
            .attr('width', d => width)
            .attr('height', markerHeight)
            .attr('fill', d => 'blue');

        bars.exit().remove();

        bars.attr('y', d => this.yScale(Math.max(0,d)) - markerHeight)
            .attr('x', (d,i) => this.xScale(i));
    }
}
