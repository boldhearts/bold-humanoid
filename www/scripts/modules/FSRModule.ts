// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as constants from '../constants';
import * as data from '../data';
import * as state from '../state';

import {DOMTemplate} from '../DOMTemplate';
import {Module} from '../Module';
import {Trace} from '../controls/Trace';

var footWidth = 0.066;
var footLength = 0.104;
var toeY = 0.007;
var toeX = 0.015;
var sensorX = 0.025;
var sensorY = 0.041;


export class FSRModule extends Module
{
    private leftFootCOPTrace: Trace;
    private rightFootCOPTrace: Trace;

    private leftReadingCanvas: HTMLCanvasElement;
    private rightReadingCanvas: HTMLCanvasElement;

    constructor()
    {
        super('fsr', 'fsr');
    }

    public load(width: number)
    {
        var scale = 2000; // 1 px = 1/2000m = 0.5mm
        var addTrace = (name: string) =>
            {
                var trace = new Trace(footWidth * scale + 20, footLength * scale + 20);
                var div = document.createElement('div');
                var h2 = document.createElement('h2');

                div.className = 'trace-container';
                h2.textContent = name;
                div.appendChild(h2);
                div.appendChild(trace.element);
                trace.setScale(scale);
                trace.setDotSize(0.005);
                this.element.appendChild(div);
                return trace;
            }

        this.leftFootCOPTrace = addTrace('left');
        this.rightFootCOPTrace = addTrace('right');

        var addReadingCanvas = (trace: Trace) =>
            {
                var readingCanvas = document.createElement('canvas');
                readingCanvas.className = 'fsr-readings-canvas';
                readingCanvas.style.position = 'absolute';
                readingCanvas.style.left = '0';
                readingCanvas.width = trace.getOverlayCanvas().width
                readingCanvas.height = trace.getOverlayCanvas().height;
                var ctx = readingCanvas.getContext('2d');
                ctx.translate(readingCanvas.width / 2, readingCanvas.height / 2);
                ctx.scale(scale, -scale);

                trace.element.insertBefore(readingCanvas, trace.getOverlayCanvas());

                return readingCanvas;
            }

        this.leftReadingCanvas = addReadingCanvas(this.leftFootCOPTrace);
        this.rightReadingCanvas = addReadingCanvas(this.rightFootCOPTrace);

        this.drawFoot(-1);
        this.drawFoot(1);

        this.closeables.add(new data.Subscription<state.FSR>(
            constants.protocols.fsrState,
            {
                onmessage: this.onFSRState.bind(this)
            }
        ));
    }


    /**
     * @param side Which foot. -1 = left, 1 = right.
     */
    private drawFoot(side: number)
    {
        var ctx = (side == -1 ? this.leftFootCOPTrace : this.rightFootCOPTrace).
            getOverlayCanvas().getContext('2d');

        ctx.save();

        ctx.strokeStyle = 'rgba(0, 0, 0, 0.8)';
        ctx.lineWidth = 0.5 / 2000;
        ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';

        var footHalfWidth = footWidth / 2;
        var footHalfLength = footLength / 2;

        ctx.beginPath();
        ctx.moveTo(side * footHalfWidth, -footHalfLength);
        ctx.lineTo(-side * footHalfWidth, -footHalfLength);
        ctx.lineTo(-side * footHalfWidth, footHalfLength - toeY);
        ctx.quadraticCurveTo(-side * footHalfWidth, footHalfLength,
                             -side * (footHalfWidth - toeX), footHalfLength);
        ctx.lineTo(side * (footHalfWidth - toeX), footHalfLength);
        ctx.quadraticCurveTo(side * footHalfWidth, footHalfLength,
                             side * footHalfWidth, footHalfLength - toeY);
        ctx.closePath();
        ctx.fill();
        
        var radius = 0.005;
        for (var x = -1; x <= 1; x += 2)
        {
            for (var y = -1; y <= 1; y += 2)
            {
                ctx.moveTo(x * sensorX + radius, y * sensorY);
                ctx.arc(x * sensorX, y * sensorY,
                        radius, 0, Math.PI * 2, true);
            }
        }
        ctx.stroke();

        ctx.restore();
    }

    private drawReadings(data: state.FSR)
    {
        var maxReading = 4.0;
        var drawReading = (ctx: CanvasRenderingContext2D, x: number, y: number, v: number) =>
            {
                ctx.save();

                var radius = Math.min(0.005, 0.005 * v / maxReading);
                ctx.clearRect(x * sensorX - 0.005, y * sensorY - 0.005,
                              2 * 0.005, 2 * 0.005);

                ctx.beginPath()
                ctx.fillStyle = 'rgba(196, 68, 37, 0.8)'; 
                ctx.moveTo(x * sensorX + radius, y * sensorY);
                ctx.arc(x * sensorX, y * sensorY,
                        radius, 0, Math.PI * 2, true);
                ctx.fill();

                ctx.restore()
            } 

        var lctx = this.leftReadingCanvas.getContext('2d');
        var rctx = this.rightReadingCanvas.getContext('2d');

        if (data.left && data.left.readings)
        {
            drawReading(lctx, 1, -1, data.left.readings[0]);
            drawReading(lctx, -1, -1, data.left.readings[1]);
            drawReading(lctx, -1, 1, data.left.readings[2]);
            drawReading(lctx, 1, 1, data.left.readings[3]);
        }

        if (data.right && data.right.readings)
        {
            drawReading(rctx, -1, 1, data.right.readings[0]);
            drawReading(rctx, 1, 1, data.right.readings[1]);
            drawReading(rctx, 1, -1, data.right.readings[2]);
            drawReading(rctx, -1, -1, data.right.readings[3]);
        }
    }
    
    private onFSRState(data: state.FSR)
    {
        if (data.left && data.left.center)
            this.leftFootCOPTrace.addValue(data.left.center[0], data.left.center[1]);
        else
            this.leftFootCOPTrace.addValue(null, null);

        if (data.right && data.right.center)
            this.rightFootCOPTrace.addValue(data.right.center[0], data.right.center[1]);
        else
            this.rightFootCOPTrace.addValue(null, null);

        this.drawReadings(data);
    }
}
