// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

import * as control from '../control';

import {Checkbox} from '../controls/Checkbox';
import {Module} from '../Module';
import {Trackable} from '../util/Trackable';

var common = [
    "sit-down", "sit-down-arms-back", "stand-ready", "stand-straight", "zero",
    "kick-cross-left", "kick-cross-right", "kick-left", "kick-right"
];

export class MotionScriptModule extends Module
{
    private showAll: Trackable<boolean>;

    constructor()
    {
        super('motion-scripts', 'motion scripts');
    }

    public load(width: number)
    {
        this.showAll = new Trackable<boolean>(false);
        this.showAll.track(value => this.element.classList[value?'add':'remove']('show-all'));

        this.element.appendChild(new Checkbox('Show all', this.showAll).element);
        
        var container = document.createElement('div');
        container.className = 'control-container motion-script-controls';
        this.element.appendChild(container);

        control.buildActions('motion-script', container);

        var buttons = container.querySelectorAll('button');
        for (var i = 0; i < buttons.length; i++)
        {
            var button = <HTMLButtonElement>buttons[i];

            var scriptName = button.textContent;
            button.classList.add(common.indexOf(scriptName) !== -1 ? 'common' : 'rare');
        }

        this.element.appendChild(document.createElement('hr'));
        control.buildAction('motion-module.reload-scripts', this.element);
    }

    public unload()
    {
        delete this.showAll;
    }
}
