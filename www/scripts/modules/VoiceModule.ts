// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @author Drew Noakes http://drewnoakes.com
 */

import * as control from '../control';

import {Module} from '../Module';

export class VoiceModule extends Module
{
    constructor()
    {
        super('voice', 'voice');
    }

    public load(width: number)
    {
        var usage = document.createElement('div');
        usage.className = 'control-container';
        control.buildSetting('options.announce-fsm-states', usage, this.closeables);
        control.buildSetting('role-decider.announce-roles', usage, this.closeables);
        this.element.appendChild(usage);

        var voiceControls = document.createElement('div');
        voiceControls.className = 'control-container flow';
        control.buildSettings('voice', voiceControls, this.closeables);
        this.element.appendChild(voiceControls);

        var vocaliserControls = document.createElement('div');
        vocaliserControls.className = 'control-container flow';
        control.buildSettings('vocaliser', vocaliserControls, this.closeables);
        this.element.appendChild(vocaliserControls);

        var sayings = document.createElement('div');
        sayings.className = 'control-container';
        control.buildActions('voice.speak', sayings);
        this.element.appendChild(sayings);
    }
}
