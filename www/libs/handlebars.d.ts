// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Type definitions for Handlebars 1.0
// Project: http://handlebarsjs.com/
// Definitions by: Boris Yankov <https://github.com/borisyankov/>
// Definitions: https://github.com/borisyankov/DefinitelyTyped


declare var Handlebars: HandlebarsStatic;

interface HandlebarsStatic {
    registerHelper(name: string, fn: Function, inverse?: boolean): void;
    registerPartial(name: string, str: any): void;
    K(): void;
    createFrame(object: any): any;
    Exception(message: string): void;
    SafeString: typeof SafeString;
    parse(input: string): boolean;
    logger: Logger;
    log(level: number, obj: any): void;
    compile(input: any, options?: any): (context?: any, options?: any) => string;
    Logger: Logger;
}

declare class SafeString {
    constructor(str: string);
    static toString(): string;
}

interface Logger {
    DEBUG: number;
    INFO: number;
    WARN: number;
    ERROR: number;
    level: number;

    methodMap: { [level: number]: string };

    log(level: number, obj: string): void;
}

declare module "handlebars" {
    export = Handlebars;
}
