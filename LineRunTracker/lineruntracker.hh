// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>
#include <functional>

namespace bold
{
  /**
  * A state machine that calls back when it detects a run of one label sandwiched
  * between runs of another label.
  *
  * This type was designed for finding line segments in an image row/column.
  *
  * Hysteresis is used to address both noise and gaps at borders between the
  * two labels being tracked.
  */
  class LineRunTracker
  {
  public:
    LineRunTracker(
      uint8_t inLabel,
      uint8_t onLabel,
      uint16_t otherCoordinate,
      uint8_t hysteresisLimit,
      std::function<void(uint16_t const, uint16_t const, uint16_t const)> callback
    )
      : otherCoordinate(otherCoordinate),
	inLabel(inLabel),
	onLabel(onLabel),
	hysteresisLimit(hysteresisLimit),
	state(State::Out),
	startedAt(0),
	callback(callback),
	hysteresis(0)
    {}

    void reset()
    {
      state = State::Out;
    }

    void update(uint8_t label, uint16_t position);

    uint8_t getHysteresisLimit() const { return hysteresisLimit; }
    void setHysteresisLimit(uint8_t limit) { hysteresisLimit = limit; }

    uint16_t otherCoordinate;

  private:
    enum class State : uint8_t
    {
      // Not on the in-label, and not on the on-label after being on the in-label
      Out,
      // At the in-label (eg. green)
      In,
      // At the on-label, after being on the in-label (eg. white line)
      On
    };

    uint8_t inLabel; // eg: green
    uint8_t onLabel; // eg: white
    uint8_t hysteresisLimit;
    State state;
    uint16_t startedAt;
    std::function<void(uint16_t const, uint16_t const, uint16_t const)> callback;
    unsigned hysteresis;
  };

  inline void LineRunTracker::update(uint8_t label, uint16_t position)
  {
    switch (state)
    {
    case State::Out:
    {
      if (label == inLabel)
      {
	hysteresis = 0;
	state = State::In;
      }
      else
      {
	if (hysteresis != 0)
	  hysteresis--;
      }
      break;
    }
    case State::In:
    {
      if (label == onLabel)
      {
	state = State::On;
	hysteresis = 0;
	startedAt = position;
      }
      else if (label == inLabel)
      {
	if (hysteresis != hysteresisLimit)
	  hysteresis++;
      }
      else
      {
	if (hysteresis != 0)
	  hysteresis--;
	else
	  state = State::Out;
      }
      break;
    }
    case State::On:
    {
      if (label == inLabel)
      {
	// we completed a run!
	state = State::In;
	hysteresis = 0;
	callback(startedAt, position, otherCoordinate);
      }
      else if (label == onLabel)
      {
	if (hysteresis != hysteresisLimit)
	  hysteresis++;
      }
      else
      {
	if (hysteresis != 0)
	  hysteresis--;
	else
	  state = State::Out;
      }
      break;
    }
    }
  }
}
