// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

namespace bold
{
  class BehaviourControl;
  class Clock;
  class Voice;

  namespace config
  {
    template<typename> class Setting;
  }

  class RoleDecider
  {
  public:
    RoleDecider(std::shared_ptr<BehaviourControl> behaviourControl, std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock);

    void update();

  private:
    std::shared_ptr<BehaviourControl> d_behaviourControl;
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;

    config::Setting<int>* d_roleOverride;
    config::Setting<bool>* d_announceRoles;
  };
}
