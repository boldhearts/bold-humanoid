// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "LineFinder/linefinder.hh"
#include "geometry2/Geometry/LineSegment/linesegment.hh"

#include <memory>

namespace bold {
  namespace config {
    template<typename T>
    class Setting;
  }

  namespace vision {
    class CameraModel;
  }

  class ScanningLineFinder : public LineFinder {
  public:
    ScanningLineFinder(std::shared_ptr<vision::CameraModel> cameraModel);

    geometry2::LineSegment2i::Vector findLineSegments(std::vector<Eigen::Vector2i> &lineDots) override;

  private:
    std::shared_ptr<vision::CameraModel> d_cameraModel;

    // Minimum line segment length required
    config::Setting<double> *d_minLength;
    // Minimum ratio of dots / length required
    config::Setting<double> *d_minCoverage;
    // Maximum root mean square error allowed
    config::Setting<double> *d_maxRMSFactor;
    // Maximum distance between new point and segment head
    config::Setting<double> *d_maxHeadDist;
  };
}
