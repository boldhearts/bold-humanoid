// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "maskwalklinefinder.ih"

MaskWalkLineFinder::MaskWalkLineFinder()
: d_imageWidth(Config::getStaticValue<int>("camera.image-width")),
  d_imageHeight(Config::getStaticValue<int>("camera.image-height")),
  d_mask(d_imageHeight, d_imageWidth, CV_8UC1),
  d_trigTable()
{
  d_drThreshold         = Config::getSetting<double>("vision.line-detection.mask-walk.delta-r");
  d_dtThresholdDegs     = Config::getSetting<double>("vision.line-detection.mask-walk.delta-theta-degs");
  d_voteThreshold       = Config::getSetting<int>("vision.line-detection.mask-walk.min-votes");
  d_minLineLength       = Config::getSetting<int>("vision.line-detection.mask-walk.min-line-length");
  d_maxLineGap          = Config::getSetting<int>("vision.line-detection.mask-walk.max-line-gap");
  d_maxLineSegmentCount = Config::getSetting<int>("vision.line-detection.mask-walk.max-lines-returned");

  d_drThreshold->changed.connect([this](double value) { rebuild(); });
  d_dtThresholdDegs->changed.connect([this](double value) { rebuild(); });

  rebuild();
}
