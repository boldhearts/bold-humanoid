// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>
#include <Eigen/Core>

#include "LineFinder/linefinder.hh"

namespace bold
{
  namespace config
  {
    template<typename> class Setting;
  }

  class MaskWalkLineFinder : public LineFinder
  {
  public:
    MaskWalkLineFinder();

    void walkLine(Eigen::Vector2i const& start, float theta, bool forward, std::function<bool(int/*x*/,int/*y*/)> const& callback, uint8_t width = 1);

    geometry2::LineSegment2i::Vector findLineSegments(std::vector<Eigen::Vector2i>& lineDots) override;

  private:
    void rebuild();

    // configuration options
    config::Setting<double>* d_drThreshold;
    config::Setting<double>* d_dtThresholdDegs;
    config::Setting<int>* d_voteThreshold;
    config::Setting<int>* d_minLineLength;
    config::Setting<int>* d_maxLineGap;
    config::Setting<int>* d_maxLineSegmentCount;

    // constant
    const int d_imageWidth;
    const int d_imageHeight;

    // cached
    cv::Mat d_mask;
    cv::Mat d_accumulator;

    // calculated
    int d_tSteps;
    int d_rSteps;
    std::vector<float> d_trigTable;
  };
}
