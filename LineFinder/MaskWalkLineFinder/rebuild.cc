// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "maskwalklinefinder.ih"

void MaskWalkLineFinder::rebuild()
{
  auto dtThresholdRads = Math::degToRad(d_dtThresholdDegs->getValue());
  auto drThreshold = d_drThreshold->getValue();

  d_tSteps = cvRound(CV_PI / dtThresholdRads);
  d_rSteps = cvRound(((d_imageWidth + d_imageHeight) * 2 + 1) / drThreshold);

  d_trigTable = vector<float>(d_tSteps*2);
  float idr = 1 / drThreshold;
  for (int n = 0; n < d_tSteps; n++)
  {
    d_trigTable[n*2] = (float)(cos((double)n*dtThresholdRads) * idr);
    d_trigTable[n*2+1] = (float)(sin((double)n*dtThresholdRads) * idr);
  }

  d_accumulator = Mat::zeros(d_tSteps, d_rSteps, CV_32SC1);
}
