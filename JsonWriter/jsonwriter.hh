// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/colour/colour.hh"
#include "src/colour/BGR/bgr.hh"
#include "src/geometry2/Geometry/MultiPoint/Polygon/polygon.hh"

#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

namespace bold
{
  class JsonWriter
  {
  public:
    template<typename TBuffer>
    static void rgb(rapidjson::Writer<TBuffer>& writer, colour::BGR const& bgr)
    {
      writer.StartArray();
      writer.Uint(bgr.r);
      writer.Uint(bgr.g);
      writer.Uint(bgr.b);
      writer.EndArray();
    }

    template<typename TBuffer>
    static void polygon(rapidjson::Writer<TBuffer>& writer, geometry2::Polygon2d const& poly)
    {
      writer.StartArray();
      for (auto const& point : poly)
      {
        writer.StartArray();
        writer.Double(point.x());
        writer.Double(point.y());
        writer.EndArray();
      }
      writer.EndArray();
    }

    template<typename TBuffer>
    static void swapNaN(rapidjson::Writer<TBuffer>& writer, double d)
    {
      if (std::isnan(d))
        writer.Null();
      else
        writer.Double(d);
    };

  private:
    JsonWriter() = delete;
  };
}
