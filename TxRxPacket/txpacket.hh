// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>
#include <vector>
#include <iterator>
#include <algorithm>
#include <iomanip>

#include "txrxpacket.hh"

#include "JointId/jointid.hh"
#include "src/util/binary.hh"
#include "src/util/Log/log.hh"

namespace bold
{

  struct TxPacketBase : public TxRxPacket
  {
    enum : uint8_t
    {
      INSTR_PING = 1,
      INSTR_READ = 2,
      INSTR_WRITE = 3,
      INSTR_REG_WRITE = 4,
      INSTR_ACTION = 5,
      INSTR_RESET = 6,
      INSTR_DIGITAL_RESET = 7,
      INSTR_SYSTEM_READ = 12,
      INSTR_SYSTEM_WRITE = 13,
      INSTR_SYNC_WRITE = 131,
      INSTR_BULK_READ = 146
     };

    constexpr static uint8_t ID_BROADCAST = 254;

    virtual uint8_t const* getData() const = 0;

    uint8_t getID() const
    {
      return getData()[ADDR_ID];
    }

    uint8_t getInstruction() const
    {
      return getData()[ADDR_INSTRUCTION];
    }

    std::string getInstructionName() const
    {
      return getInstructionName(getData()[ADDR_INSTRUCTION]);
    }

    uint8_t getParameter(uint8_t idx) const
    {
      return getData()[ADDR_PARAMETER + idx];
    }

  protected:
    static std::string getInstructionName(uint8_t instruction)
    {
      switch (instruction)
      {
      case INSTR_PING:
        return "PING";
      case INSTR_READ:
        return "READ";
      case INSTR_WRITE:
        return "WRITE";
      case INSTR_REG_WRITE:
        return "REG_WRITE";
      case INSTR_ACTION:
        return "ACTION";
      case INSTR_RESET:
        return "RESET";
      case INSTR_SYNC_WRITE:
        return "SYNC_WRITE";
      case INSTR_BULK_READ:
        return "BULK_READ";
      default:
        return "UNKNOWN";
      };
    }
  };

  template<class BASE>
  struct TxPacket : public TxPacketBase
  {
    uint8_t const* getData() const override
    {
      return static_cast<BASE const*>(this)->content.data();
    }

    size_t size() const override
    {
      return static_cast<BASE const*>(this)->content.size();
    }

    uint8_t calculateChecksum() const override
    {
      auto baseThis = static_cast<BASE const*>(this);
      return util::binary::calculateChecksum(std::next(std::begin(baseThis->content), 2),
                                             std::prev(std::end(baseThis->content), 1));
    }

    void setChecksum()
    {
      auto baseThis = static_cast<BASE*>(this);
      baseThis->content[baseThis->content.size() - 1] = calculateChecksum();
      util::Log::trace("TxPacket::setCheckSum") << "Checksum: " <<
        std::hex << std::setfill('0') << std::setw(2) << (int)baseThis->content[baseThis->content.size() - 1];
    }

  protected:
    void init(uint8_t deviceID, uint8_t instruction)
    {
      auto baseThis = static_cast<BASE*>(this);
      baseThis->content[0] = baseThis->content[1] = 0xFF;
      baseThis->content[TxRxPacket::ADDR_ID] = deviceID;
      baseThis->content[TxRxPacket::ADDR_LENGTH] = baseThis->size() - TxRxPacket::ADDR_INSTRUCTION;
      baseThis->content[TxRxPacket::ADDR_INSTRUCTION] = instruction;
    }
  };


  template<uint8_t INSTRUCTION>
  struct DynamicTxPacket : public TxPacket<DynamicTxPacket<INSTRUCTION>>
  {
    DynamicTxPacket(uint8_t deviceID, size_t size)
      : content(size)
    {
      ASSERT(size > TxRxPacket::ADDR_INSTRUCTION);
      TxPacket<DynamicTxPacket<INSTRUCTION>>::init(deviceID, INSTRUCTION);
    }

    std::vector<uint8_t> content;
  };

  template<size_t SIZE, uint8_t INSTRUCTION>
  struct StaticTxPacket : public TxPacket<StaticTxPacket<SIZE, INSTRUCTION>>
  {
    StaticTxPacket(uint8_t deviceID)
    {
      static_assert(SIZE > TxRxPacket::ADDR_INSTRUCTION, "TxPackage too small");
      TxPacket<StaticTxPacket<SIZE, INSTRUCTION>>::init(deviceID, INSTRUCTION);
    }

    std::array<uint8_t, SIZE> content;
  };


  struct PingTxPacket :
    public StaticTxPacket<6, TxPacketBase::INSTR_PING>
  {
    PingTxPacket(uint8_t deviceID)
      : StaticTxPacket<6, 1>::StaticTxPacket(deviceID)
    {
      setChecksum();
    }
  };


  struct ReadTxPacket :
    public StaticTxPacket<8, TxPacketBase::INSTR_READ>
  {
    ReadTxPacket(uint8_t deviceID, uint8_t address, uint8_t length)
      : StaticTxPacket<8, INSTR_READ>(deviceID)
    {
      content[ADDR_PARAMETER] = address;
      content[ADDR_PARAMETER + 1] = length;
      setChecksum();
    }
  };

  struct ReadByteTxPacket :
    public ReadTxPacket
  {
    ReadByteTxPacket(uint8_t deviceID, uint8_t address)
      : ReadTxPacket(deviceID, address, 1)
    {}
  };

  struct ReadWordTxPacket :
    public ReadTxPacket
  {
    ReadWordTxPacket(uint8_t deviceID, uint8_t address)
      : ReadTxPacket(deviceID, address, 2)
    {}
  };

  
  struct WriteByteTxPacket :
    public StaticTxPacket<8, TxPacketBase::INSTR_WRITE>
  {
    WriteByteTxPacket(uint8_t deviceID, uint8_t address, uint8_t value)
      : StaticTxPacket<8, INSTR_WRITE>(deviceID)
    {
      content[ADDR_PARAMETER] = address;
      content[ADDR_PARAMETER + 1] = value;
      setChecksum();
    }
  };

  struct WriteWordTxPacket :
    public StaticTxPacket<9, TxPacketBase::INSTR_WRITE>
  {
    WriteWordTxPacket(uint8_t deviceID, uint8_t address, uint16_t value)
      : StaticTxPacket<9, INSTR_WRITE>(deviceID)
    {
      content[ADDR_PARAMETER] = address;
      content[ADDR_PARAMETER + 1] = util::binary::getLowByte(value);
      content[ADDR_PARAMETER + 2] = util::binary::getHighByte(value);
      setChecksum();
    }
  };


  struct ResetTxPacket :
    public StaticTxPacket<6, TxPacketBase::INSTR_RESET>
  {
    ResetTxPacket(uint8_t deviceID)
      : StaticTxPacket<6, INSTR_RESET>::StaticTxPacket(deviceID)
    {
      setChecksum();
    }
  };


  struct SyncWriteTxPacket
    : public DynamicTxPacket<TxPacketBase::INSTR_SYNC_WRITE>
  {
    SyncWriteTxPacket(uint8_t deviceID, uint8_t fromAddress,
                      uint8_t bytesPerDevice, uint8_t deviceCount,
                      std::vector<uint8_t> const& parameters)
      : DynamicTxPacket(deviceID, 8 + bytesPerDevice * deviceCount)
    {
      content[ADDR_PARAMETER] = fromAddress;
      content[ADDR_PARAMETER + 1] = bytesPerDevice - 1;

      std::copy(std::begin(parameters), std::end(parameters),
                std::next(std::begin(content), ADDR_PARAMETER + 2));
      setChecksum();
    }
  };


  struct BulkReadTxPacket : 
    public DynamicTxPacket<TxPacketBase::INSTR_BULK_READ>
  {
    BulkReadTxPacket(uint8_t deviceID, uint8_t nDeviceRequests)
      : DynamicTxPacket(deviceID, ADDR_PARAMETER + 2 + 3 * nDeviceRequests)
    {
      content[ADDR_PARAMETER] = 0x0;
    }

  };

  struct BulkReadTxPacketBuilder
  {
    BulkReadTxPacketBuilder(uint8_t deviceID)
      : d_packet(deviceID, TxRxPacket::ADDR_PARAMETER +
                 1 + // 0x0
                 (1 + // CM730 device request
                  (size_t)JointId::DEVICE_COUNT + // MX28 device requests
                  2) // FSR requests
                 * 3 + 
                 1 // Checksum
        ),
        d_nDeviceRequests(0),
        d_contentCursor(TxRxPacket::ADDR_PARAMETER + 1)
    {
      d_packet.content[TxRxPacket::ADDR_PARAMETER] = 0x0;
    }

    void addDeviceRequest(uint8_t deviceId, uint8_t startAddress, uint8_t endAddress)
    {
      uint8_t requestedByteCount = endAddress - startAddress + (uint8_t)1;
      d_packet.content[d_contentCursor++] = requestedByteCount;
      d_packet.content[d_contentCursor++] = deviceId;
      d_packet.content[d_contentCursor++] = startAddress;      
      ++d_nDeviceRequests;
    }

    BulkReadTxPacket getPacket() 
    {
      auto packet = BulkReadTxPacket(d_packet.content[TxRxPacket::ADDR_ID], d_nDeviceRequests);
      std::copy_n(std::begin(d_packet.content), packet.content.size(), std::begin(packet.content));
      packet.content[TxRxPacket::ADDR_LENGTH] = packet.content.size() - TxRxPacket::ADDR_INSTRUCTION;
      packet.setChecksum();
      return packet;
    }

  private:
    DynamicTxPacket<TxPacketBase::INSTR_BULK_READ> d_packet;
    size_t d_nDeviceRequests;
    size_t d_contentCursor;
  };
}
