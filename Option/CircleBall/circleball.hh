// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "geometry2/Point/point.hh"

#include <Eigen/Core>

namespace bold {
  namespace motion {
    namespace modules {
      class WalkModule;

      class HeadModule;
    }
  }

  namespace vision {
    class CameraModel;
  }

  class LookAtFeet;

  class LookAtBall;

  class Draw;

  class CircleBall : public Option {
  public:
    CircleBall(std::string const &id,
               std::shared_ptr<motion::modules::WalkModule> walkModule,
               std::shared_ptr<motion::modules::HeadModule> headModule,
               std::shared_ptr<vision::CameraModel> cameraModel,
               std::shared_ptr<Draw> draw);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer> &writer) override;

    double hasTerminated() override;

    void setTurnParams(double turnAngleRads, geometry2::Point2d targetBallPos);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  private:
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    std::shared_ptr<LookAtFeet> d_lookAtFeet;
    std::shared_ptr<LookAtBall> d_lookAtBall;

    std::shared_ptr<Draw> d_draw;

    geometry2::Point2d d_targetBallPos;
    double d_targetYaw;
  };
}
