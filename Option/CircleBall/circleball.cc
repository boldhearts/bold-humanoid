// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "circleball.hh"

#include "config/Config/config.hh"
#include "Drawing/drawing.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/OrientationState/orientationstate.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "Option/LookAtFeet/lookatfeet.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace bold::vision;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

CircleBall::CircleBall(std::string const& id,
                       shared_ptr<WalkModule> walkModule,
                       shared_ptr<HeadModule> headModule,
                       shared_ptr<CameraModel> cameraModel,
                       shared_ptr<Draw> draw)
  : Option{id, "CircleBall"},
    d_walkModule{move(walkModule)},
    d_headModule{move(headModule)},
    d_lookAtFeet{make_shared<LookAtFeet>("look-at-feet", d_headModule)},
    d_lookAtBall{make_shared<LookAtBall>("look-at-ball", move(cameraModel), d_headModule)},
    d_draw{draw},
    d_targetBallPos{0.0, 0.15},
    d_targetYaw{0}
{}

vector<shared_ptr<Option>> CircleBall::runPolicy(Writer<StringBuffer>& writer)
{
  auto agentFrame = State::get<AgentFrameState>();

  ASSERT(agentFrame);

  if (!agentFrame->isBallVisible())
  {
    // Look at feet (where the ball is likely to be)
    return { d_lookAtFeet };
  }

  static Setting<double>* rotateUntilAngleDegs = Config::getSetting<double>("options.circle-ball.rotate-until-within-degrees");
  static Setting<double>* maxSpeedX            = Config::getSetting<double>("options.circle-ball.max-speed-x");
  static Setting<double>* maxSpeedY            = Config::getSetting<double>("options.circle-ball.max-speed-y");
  static Setting<double>* turnSpeedA           = Config::getSetting<double>("options.circle-ball.turn-speed-a");
  static Setting<double>* minTranslationSpeedX = Config::getSetting<double>("options.circle-ball.min-translation-speed-x");
  static Setting<double>* minTranslationSpeedY = Config::getSetting<double>("options.circle-ball.min-translation-speed-y");
  static Setting<double>* backUpDistance       = Config::getSetting<double>("options.circle-ball.back-up-distance");
  static Setting<double>* backUpSpeed          = Config::getSetting<double>("options.circle-ball.back-up-speed");

  auto observedBallPos = Point2d{agentFrame->getBallObservation()->head<2>()};
  Vector2d error = d_targetBallPos - observedBallPos;
  Vector2d errorNorm = error.normalized();

  // Scale turn based upon difference between current and target yaw
  auto orientation = State::get<OrientationState>();
  double yawDiffRads = Math::shortestAngleDiffRads(orientation->getYawAngle(), d_targetYaw);
//  cout << "[Circle ball] yaw=" << orientation->getYawAngle() << " target=" << d_targetYaw << " diff=" << yawDiffRads << " posError=" << error.transpose() << endl;
//  a = Math::lerp(fabs(yawDiffRads), 0.0, M_PI/3, 0.0, a);

  d_draw->line(Frame::Agent, LineSegment2d{Point2d::Zero(), d_targetBallPos}, colour::BGR(0, 128, 255));
  d_draw->lineAtAngle(Frame::Agent, d_targetBallPos, M_PI/2 + yawDiffRads, 1.0, colour::BGR(0, 128, 255));

  bool isLeftTurn = yawDiffRads < 0;

  double x, y, a;

  if (fabs(yawDiffRads) > Math::degToRad(rotateUntilAngleDegs->getValue()))
  {
    // Too much angular error, so rotate

    double turnDistanceFromBall = backUpDistance->getValue();

    if (observedBallPos.y() < turnDistanceFromBall)
    {
      // Too close to ball to turn, so step backwards
      y = -backUpSpeed->getValue();
      x = 0;
      a = 0;
    }
    else
    {
      // Far enough from ball to turn, so get to it

      // Always walk sideways, but not if error becomes too big
      x = (1.0 - Math::clamp(fabs(error.x()), 0.0, 1.0))
        * (isLeftTurn ? -maxSpeedX->getValue() : maxSpeedX->getValue());

      // Try to keep forward distance stable
      y = -Math::lerp(observedBallPos.y() - turnDistanceFromBall, 0.0, 0.2, 0.0, maxSpeedY->getValue());

      // Turn to keep ball centered
      double errorDir = errorNorm.x() > 0.0 ? 1.0 : -1.0;
      a = errorDir * Math::clamp(fabs(errorNorm.x()), 0.5, 1.0) * turnSpeedA->getValue();
    }
  }
  else
  {
    // Angle is fine, position for kick
    a = 0;
    x = -Math::lerp(error.x() / 0.2, minTranslationSpeedX->getValue(), maxSpeedX->getValue());
    y = -Math::lerp((error.y() + fabs(error.x())) / 0.2, minTranslationSpeedY->getValue(), maxSpeedY->getValue());
  }

  x = Math::clamp(x, -maxSpeedX->getValue(), maxSpeedX->getValue());
  y = Math::clamp(y, -maxSpeedY->getValue(), maxSpeedY->getValue());
  a = Math::clamp(a, -turnSpeedA->getValue(), turnSpeedA->getValue());

//  cout << "[Circle ball] " << x << "\t" << y << "\t" << a << endl;

  // NOTE x and y intentionally swapped. 'x' value is also negative as a result of the move
  // direction being inverted.
  d_walkModule->setMoveDir(y, -x);
  d_walkModule->setTurnAngle(a);

  writer.String("yawError");
  writer.Double(yawDiffRads);

  writer.String("posError");
  writer.StartArray();
  writer.Double(error.x());
  writer.Double(error.y());
  writer.EndArray(2);

  writer.String("moveDir");
  writer.StartArray();
  writer.Double(x);
  writer.Double(y);
  writer.EndArray(2);

  writer.String("turn");
  writer.Double(a);

  // Look at ball to make sure we don't lose track of it
  return { d_lookAtBall };
}

void CircleBall::setTurnParams(double turnAngleRads, Point2d targetBallPos)
{
  d_targetBallPos = targetBallPos;

  // Track starting orientation in order to know when we've reached our desired rotation
  auto orientation = State::get<OrientationState>();
  ASSERT(orientation);
  d_targetYaw = Math::normaliseRads(orientation->getYawAngle() + turnAngleRads);

  //cout << "setTurnParams turn=" << turnAngleRads << " targetYaw=" << d_targetYaw << " yaw=" << orientation->getYawAngle() << endl;
}

double CircleBall::hasTerminated()
{
  // First, assert we've turned enough
  double yawErrorRads = State::get<OrientationState>()->getYawAngle() - d_targetYaw;

  if (fabs(yawErrorRads) > Math::degToRad(10)) // TODO magic number!
    return false;

  // If we've turned enough, ensure we are positioned correctly to the ball

  auto agentFrame = State::get<AgentFrameState>();

  // Don't stop yet, first need to find the ball to know whether we are finished
  if (!agentFrame->isBallVisible())
    return false;

  Vector2d posError = d_targetBallPos - agentFrame->getBallObservation()->toDim<2>();

  //cout << "circleBall posError=" << posError.transpose() << " norm=" << posError.norm() << endl;

  return posError.norm() < 0.02; // TODO magic number
}
