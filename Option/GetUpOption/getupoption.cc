// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "getupoption.hh"

#include "agent/FallDetector/falldetector.hh"
#include "Option/MotionScriptOption/motionscriptoption.hh"
#include "config/Config/config.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

GetUpOption::GetUpOption(string const& id,
                         shared_ptr<FallDetector const> fallDetector, shared_ptr<MotionScriptModule> motionScriptModule)
  : Option{id, "GetUp"},
  cnt(0),
  d_fallDetector{move(fallDetector)}
{
  auto whenOnFrontScript = Config::getStaticValue<string>("options.get-up.when-on-front-script");
  auto whenOnBackScript = Config::getStaticValue<string>("options.get-up.when-on-back-script");

  //TODO making it adaptive for random "whenOnFrontScript" things, or better a don't need it
  d_forwardGetUp.push_back(make_shared<MotionScriptOption>("forward-get-up-script", motionScriptModule,"./motionscripts/" + whenOnFrontScript + ".json"));
  //d_forwardGetUp.push_back(make_shared<MotionScriptOption>("forward-get-up-script", motionScriptModule,"./motionscripts/get-up-from-front-2.json"));
  
  d_backwardGetUp.push_back(make_shared<MotionScriptOption>("backward-get-up-script", motionScriptModule,"./motionscripts/" + whenOnBackScript + ".json"));
  //d_backwardGetUp.push_back(make_shared<MotionScriptOption>("backward-get-up-script", motionScriptModule,"./motionscripts/get-up-from-back-2.json"));

  d_rollLeftToFront = make_shared<MotionScriptOption>("roll-left-to-front-script", motionScriptModule,
                                                      "./motionscripts/roll-left-to-front.json");
  d_rollRightToFront = make_shared<MotionScriptOption>("roll-right-to-front-script", motionScriptModule,
                                                       "./motionscripts/roll-right-to-front.json");
}

vector<shared_ptr<Option>> GetUpOption::runPolicy(Writer<StringBuffer>& writer)
{
  auto fallState = d_fallDetector->getFallenState();

  if (d_activeScript)
  {
    if (d_activeScript->hasTerminated() == 1.0 && fallState != FallState::STANDUP)
    {
      // We finished playing a script, but still are not standing -- try again
      d_activeScript->reset();
      d_activeScript = nullptr;
      cnt++; 
      Log::verbose("boldhumanoid") << "Standup Iteration changed to: " << cnt;
    }
  }

  if (d_activeScript == nullptr)
  {
    // First time to run. Determine the direction of the fall and choose the appropriate script to run
    switch (fallState)
    {

      case FallState::BACKWARD: d_activeScript = d_backwardGetUp[cnt%d_backwardGetUp.size()];    break;
      case FallState::FORWARD:  d_activeScript = d_forwardGetUp[cnt%d_forwardGetUp.size()];     break;
      case FallState::LEFT:     d_activeScript = d_rollLeftToFront;  break;
      case FallState::RIGHT:    d_activeScript = d_rollRightToFront; break;
      case FallState::STANDUP:
        // Somehow we've been asked to run when already standing.
        return {};
    }
    
    
  }

  return {d_activeScript};
}

void GetUpOption::reset()
{
  d_forwardGetUp[cnt%d_forwardGetUp.size()]->reset();
  d_backwardGetUp[cnt%d_backwardGetUp.size()]->reset();
  d_rollLeftToFront->reset();
  d_rollRightToFront->reset();

  d_activeScript = nullptr;
}

double GetUpOption::hasTerminated()
{
  bool standing = d_fallDetector->getFallenState() == FallState::STANDUP;

  if (d_activeScript)
    return standing && d_activeScript->hasTerminated() ? 1.0 : 0.0;

  return standing ? 1.0 : 0.0;
}
