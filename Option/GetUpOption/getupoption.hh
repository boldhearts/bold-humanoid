// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class MotionScriptModule;
    }
  }
  
  class FallDetector;
  class MotionScriptOption;
  
  class GetUpOption : public Option
  {
  public:
    GetUpOption(std::string const& id,
                std::shared_ptr<FallDetector const> fallDetector,
                std::shared_ptr<motion::modules::MotionScriptModule> motionScriptModule);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void reset() override;

    double hasTerminated() override;

  private:
    
    unsigned int cnt;
       
    std::shared_ptr<FallDetector const> d_fallDetector;
    std::shared_ptr<MotionScriptOption> d_activeScript;

    std::vector<std::shared_ptr<MotionScriptOption>> d_forwardGetUp;
    std::vector<std::shared_ptr<MotionScriptOption>> d_backwardGetUp;
    std::shared_ptr<MotionScriptOption> d_backwardGetUp1;
    std::shared_ptr<MotionScriptOption> d_backwardGetUp2;    
    std::shared_ptr<MotionScriptOption> d_rollLeftToFront;
    std::shared_ptr<MotionScriptOption> d_rollRightToFront;
  };
}
