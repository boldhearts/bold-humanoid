// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fsmoption.hh"

#include <algorithm>

using namespace bold;
using namespace std;

FSMState::FSMState(string const& name, vector<shared_ptr<Option>> options, bool isFinal)
: name(name),
  isFinal(isFinal),
  options(options),
  onEnter(),
  startTimestamp()
{}

void FSMState::start(Clock const& clock)
{
  startTimestamp = clock.getTimestamp();

  // For transitions with condition factories, invoke them to clear out any accumulated state
  for (shared_ptr<FSMTransition> const& transition : transitions)
  {
    if (transition->conditionFactory)
      transition->condition = transition->conditionFactory();
  }

  onEnter();
}

std::chrono::duration<double> FSMState::timeSinceStart(Clock const& clock) const
{
  return std::chrono::duration<double>(clock.getSecondsSince(startTimestamp));
}

double FSMState::secondsSinceStart(Clock const& clock) const
{
  return clock.getSecondsSince(startTimestamp);
}

bool FSMState::allOptionsTerminated() const
{
  // NOTE this isn't recursive, so if a state's child returns a sub-option that hasn't terminated, the parent must currently handle this
  return all_of(options.begin(), options.end(), [](shared_ptr<Option> const& o) { return o->hasTerminated(); });
}

shared_ptr<FSMTransition> FSMState::transitionTo(shared_ptr<FSMState>& targetState, string name)
{
  shared_ptr<FSMTransition> t = make_shared<FSMTransition>(name);
  t->parentState = shared_from_this();
  t->childState = targetState;
  transitions.push_back(t);
  return t;
}
