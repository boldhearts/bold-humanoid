// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fsmoption.hh"

#include "config/Config/config.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"
#include "Voice/voice.hh"

#include "roundtable/Action/action.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::roundtable;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

FSMOption::FSMOption(shared_ptr<Voice> voice, shared_ptr<Clock> clock, string const& id)
: Option(id, "FSM"),
  d_voice(move(voice)),
  d_clock(move(clock))
{
  stringstream pausePath;
  pausePath << "options.fsms." << id << ".paused";

  stringstream pauseDesc;
  pauseDesc << "Pause " << id;

  d_paused = new BoolSetting(pausePath.str(), false, pauseDesc.str());
  d_paused->setValue(false);

  Config::addSetting(d_paused);

  stringstream jumpPath;
  jumpPath << "options.fsms." << id << ".goto";

  stringstream jumpDesc;
  jumpDesc << "Set state for " << id << " FSM";

  Action::addAction(jumpPath.str(), jumpDesc.str(), [this](Value* args)
  {
    if (!args || !args->IsObject())
    {
      Log::warning("FSMOption::goto") << "Invalid request JSON. Must be an object.";
      return;
    }

    auto stateMember = args->FindMember("state");
    if (stateMember == args->MemberEnd() || !stateMember->value.IsString())
    {
      Log::warning("FSMOption::goto") << "Invalid request JSON. Must have a 'state' property of type 'string'.";
      return;
    }
    const char* stateName = stateMember->value.GetString();

    // find state having this name
    auto state = getState(string(stateName));
    if (!state)
    {
      Log::warning("FSMOption::goto") << "Invalid request to go to unknown state: " << stateName;
      return;
    }

    setCurrentState(state);
  });
}

void FSMOption::reset()
{
  // Clear the current state. runPolicy will set it to the starting state when next invoked.
  d_curState = nullptr;
}

shared_ptr<FSMState> FSMOption::getState(string name) const
{
  auto it = std::find_if(
    d_states.begin(),
    d_states.end(),
    [&name](shared_ptr<FSMState> const& state) { return state->name == name; });

  if (it == d_states.end())
    return nullptr;

  return *it;
}

void FSMOption::setCurrentState(shared_ptr<FSMState> state)
{
  d_curState = state;
  d_curState->start(*d_clock);

  static Setting<bool>* announceFsmStates = Config::getSetting<bool>("options.announce-fsm-states");
  static Setting<int>* announceRate = Config::getSetting<int>("options.announce-rate-wpm");
  if (announceFsmStates->getValue())
  {
    if (d_voice->queueLength() < 2)
    {
      SpeechTask task = { state->name, (uint)announceRate->getValue(), false };
      d_voice->say(task);
    }
  }
}

double FSMOption::hasTerminated()
{
  return d_curState && d_curState->isFinal ? 1.0 : 0.0;
}

void FSMOption::addState(std::shared_ptr<FSMState> state, bool startState)
{
  d_states.push_back(state);
  if (startState)
  {
    ASSERT(!d_startState);
    d_startState = state;
  }
}

std::shared_ptr<FSMTransition> FSMOption::wildcardTransitionTo(std::shared_ptr<FSMState> targetState, std::string name)
{
  std::shared_ptr<FSMTransition> t = std::make_shared<FSMTransition>(name);
  t->childState = targetState;
  d_wildcardTransitions.push_back(t);
  return t;
}
