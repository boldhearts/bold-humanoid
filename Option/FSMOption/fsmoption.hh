// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "Clock/clock.hh"
#include "roundtable/WebSocketBuffer/websocketbuffer.hh"

#include <memory>
#include <iosfwd>
#include <vector>
#include <functional>
#include <chrono>
#include <rapidjson/writer.h>
#include <sigc++/signal.h>

namespace bold
{
  struct FSMState;
  namespace config
  {
    template<typename> class Setting;
  }
  
  class Voice;

  struct FSMTransition
  {
    FSMTransition(std::string const& name);

    std::string name;

    /// Condition that needs to be fulfilled to fire
    std::function<bool()> condition;

    /// A function that creates a new condition function each time the source
    /// FSMState is entered. This prevents conditions from carrying state
    /// between usages.
    std::function<std::function<bool()>()> conditionFactory;

    /// Function to be called when this transition is fired
    sigc::signal<void> onFire;

    /// State this transition goes from
    std::shared_ptr<FSMState> parentState;
    /// State this transition results in
    std::shared_ptr<FSMState> childState;

    FSMTransition* when(std::function<bool()> condition);
    FSMTransition* when(std::function<std::function<bool()>()> conditionFactory);
    FSMTransition* whenTerminated();
    FSMTransition* notify(std::function<void()> callback);
    FSMTransition* after(std::chrono::milliseconds time, std::shared_ptr<Clock> clock);
  };

  struct FSMState : public std::enable_shared_from_this<FSMState>
  {
    FSMState(std::string const& name, std::vector<std::shared_ptr<Option>> options, bool isFinal = false);

    /// State name
    std::string name;
    /// Whether this state is a final state
    bool isFinal;
    /// Options to return while in this state
    std::vector<std::shared_ptr<Option>> options;
    /// Outgoing transitions
    std::vector<std::shared_ptr<FSMTransition>> transitions;

    sigc::signal<void> onEnter;

    Clock::Timestamp startTimestamp;

    void start(Clock const& clock);

    std::chrono::duration<double> timeSinceStart(Clock const& clock) const;

    double secondsSinceStart(Clock const& clock) const;

    bool allOptionsTerminated() const;

    std::shared_ptr<FSMTransition> transitionTo(std::shared_ptr<FSMState>& targetState, std::string name = "");
  };

  /** Finite State Machine
   */
  class FSMOption : public Option
  {
  public:
    FSMOption(std::shared_ptr<Voice> voice, std::shared_ptr<Clock> clock, std::string const& id);

    double hasTerminated() override;

    void reset() override;

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void addState(std::shared_ptr<FSMState> state, bool startState = false);

    std::shared_ptr<FSMState> getState(std::string name) const;
    std::shared_ptr<FSMState const> getCurrentState() const { return d_curState; }

    std::shared_ptr<FSMState> newState(std::string const& name,
                         std::vector<std::shared_ptr<Option>> options,
                         bool finalState = false,
                         bool startState = false)
    {
      auto s = std::make_shared<FSMState>(name, options, finalState);
      addState(s, startState);
      return s;
    }

    std::shared_ptr<FSMState> const& getStartState() const { return d_startState; }

    std::shared_ptr<FSMTransition> wildcardTransitionTo(std::shared_ptr<FSMState> targetState, std::string name = "");

    std::string toDot() const;

    void toJson(rapidjson::Writer<rapidjson::StringBuffer>& writer) const { toJsonInternal(writer); }
    void toJson(rapidjson::Writer<roundtable::WebSocketBuffer>& writer) const { toJsonInternal(writer); }

  private:
    template<typename TBuffer>
    void toJsonInternal(rapidjson::Writer<TBuffer>& writer) const;

    void setCurrentState(std::shared_ptr<FSMState> state);

    std::vector<std::shared_ptr<FSMState>> d_states;
    std::vector<std::shared_ptr<FSMTransition>> d_wildcardTransitions;
    std::shared_ptr<FSMState> d_startState;
    std::shared_ptr<FSMState> d_curState;
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;
    config::Setting<bool>* d_paused;
  };

  template<typename TBuffer>
  void FSMOption::toJsonInternal(rapidjson::Writer<TBuffer>& writer) const
  {
    /*
     {
       "name": "win",
       "start": "startup",
       "states": [
         { "id": "startup" }
       ],
       "transitions": [
         { "from": "startup", "to": "ready", "label": "init" }
       ],
       "wildcardTransitions": [
         { "to": "ready", "label": "init" }
       ]
     }
    */
    writer.StartObject();
    {
      writer.String("name");
      writer.String(getId().c_str());
      writer.String("start");
      writer.String(d_startState->name.c_str());

      writer.String("states");
      writer.StartArray();
      {
        for (auto const& state : d_states)
        {
          writer.StartObject();
          {
            writer.String("id");
            writer.String(state->name.c_str());
          }
          writer.EndObject();
        }
      }
      writer.EndArray();

      writer.String("transitions");
      writer.StartArray();
      {
        for (auto const& state : d_states)
        {
          for (auto const& transition : state->transitions)
          {
            writer.StartObject();
            {
              writer.String("id");
              writer.String(transition->name.c_str());
              writer.String("from");
              writer.String(state->name.c_str());
              writer.String("to");
              writer.String(transition->childState->name.c_str());
            }
            writer.EndObject();
          }
        }
      }
      writer.EndArray();

      writer.String("wildcardTransitions");
      writer.StartArray();
      {
        for (auto const& transition : d_wildcardTransitions)
        {
          writer.StartObject();
          {
            writer.String("id");
            writer.String(transition->name.c_str());
            writer.String("to");
            writer.String(transition->childState->name.c_str());
          }
          writer.EndObject();
        }
      }
      writer.EndArray();
    }
    writer.EndObject();
  }
}
