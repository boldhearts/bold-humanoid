// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fsmoption.hh"

using namespace bold;
using namespace std;

FSMTransition::FSMTransition(string const& name)
: name(name)
{}

FSMTransition* FSMTransition::when(function<bool()> condition)
{
  this->conditionFactory = nullptr;
  this->condition = condition;
  return this;
}

FSMTransition* FSMTransition::when(function<function<bool()>()> conditionFactory)
{
  this->conditionFactory = conditionFactory;
  this->condition = nullptr;
  return this;
}

FSMTransition* FSMTransition::whenTerminated()
{
  this->condition = [this] { return parentState->allOptionsTerminated(); };
  return this;
}

FSMTransition* FSMTransition::notify(function<void()> callback)
{
  this->onFire.connect(callback);
  return this;
}

FSMTransition* FSMTransition::after(chrono::milliseconds time, shared_ptr<Clock> clock)
{
  return when([this,time, clock] { return parentState->timeSinceStart(*clock) >= time; });
}
