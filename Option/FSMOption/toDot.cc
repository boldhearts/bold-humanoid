// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "fsmoption.hh"

#include <algorithm>
#include <sstream>

#include "src/util/assert.hh"

using namespace bold;
using namespace std;

string FSMOption::toDot() const
{
  ostringstream out;
  out << "digraph "  << getId() << "{" << endl;

  ASSERT(d_startState && "FSM must have a starting state");

  list<shared_ptr<FSMState>> stateQueue{d_startState};
  list<shared_ptr<FSMState>> visitedStates;

  while (!stateQueue.empty())
  {
    auto state = stateQueue.front();
    stateQueue.pop_front();
    if (find(visitedStates.begin(), visitedStates.end(), state) == visitedStates.end())
    {
      out << state->name << "[";
      if (state == d_startState)
      {
        out << "shape=doublecircle";
      }
      out << "]" << endl;

      for (auto transition : state->transitions)
      {
        out << state->name << " -> " << transition->childState->name;
        if (transition->name != "")
          out << " [label=" << transition->name << "]";
        out << endl;

        stateQueue.push_back(transition->childState);
      }
      visitedStates.push_back(state);
    }
  }

  out << "}" << endl;

  return out.str();
}
