// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "support.hh"

#include "Option/WalkTo/walkto.hh"

#include "config/Config/config.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "Math/math.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace std;
using namespace Eigen;

Support::Support(string const& id,
                 shared_ptr<WalkModule> walkModule,
                 shared_ptr<Draw> draw)
  : Option{id, "Support"}
{
  d_yieldDistance = Config::getSetting<double>("options.support.yield-distance");
  d_walkTo = make_shared<WalkTo>(id + ".walkto", move(walkModule), move(draw));
}

Option::OptionVector Support::runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer)
{
  auto agentFrame = State::get<AgentFrameState>();

  if (!agentFrame->isBallVisible())
    return {};

  auto ballPos = agentFrame->getBallObservation()->toDim<2>();
  auto ballDir = (ballPos - Point2d::ORIGIN).normalized();

  writer.String("ballPos");
  writer.StartArray();
  writer.Double(ballPos.x());
  writer.Double(ballPos.y());
  writer.EndArray(2);

  auto yieldPos = ballPos - Vector2d{d_yieldDistance->getValue() * ballDir};

  double ballAngleRads = Math::angleToPoint(ballPos);

  d_walkTo->setTargetPosition(yieldPos, ballAngleRads);
  return {d_walkTo};
}
