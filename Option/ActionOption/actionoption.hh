// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include <functional>

namespace bold
{
  class ActionOption : public Option
  {
  public:
    ActionOption(std::string const& id, std::function<void()> action)
    : Option(id, "Action"),
      d_action(action),
      d_needsRunning(true)
    {}

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void reset() override;

    double hasTerminated() override;

  private:
    std::function<void()> d_action;
    bool d_needsRunning;
  };
}
