// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

#include <iosfwd>

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class MotionRequest;
    }

    namespace modules {
      class MotionScriptModule;
    }
    namespace scripts {
      class MotionScript;
    }
  }
  

  /// An option that requests a particular motion script be played when its
  /// policy is run.  Note that the motion scheduler may reject the request to
  /// play the script if the relevant body sections are already engaged.
  ///
  /// Indicates termination only when the script completes.
  class MotionScriptOption : public Option
  {
  public:
    MotionScriptOption(std::string const& id, std::shared_ptr<motion::modules::MotionScriptModule> const& motionScriptModule, std::string const& fileName = "", bool ifNotFinalPose = false);

    /// Returns a truthy value when the script has completed execution.
    double hasTerminated() override;

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    /// Clears any previous attempt at running a motion script. Subsequent
    /// calls to runPolicy will make a new request of the motion task scheduler.
    void reset() override;

    void setMotionScript(std::shared_ptr<motion::scripts::MotionScript const> script) { d_script = script; }

  private:
    using Writer = rapidjson::Writer<rapidjson::StringBuffer>;
    void writeHasExisting(Writer& writer);
    void writeTerminated(Writer& writer);
    void writeStatus(Writer& writer);
    void writeSkip(Writer& writer);

    std::shared_ptr<motion::modules::MotionScriptModule> d_motionScriptModule;
    /// The script associated with this MotionScriptOption
    std::shared_ptr<motion::scripts::MotionScript const> d_script;
    /// The most recent MotionRequest issued to the MotionScriptModule
    std::shared_ptr<motion::core::MotionRequest const> d_request;
    bool d_hasTerminated;
    bool d_ifNotFinalPose;
  };


  inline double MotionScriptOption::hasTerminated()
  {
    return d_hasTerminated ? 1.0 : 0.0;
  }

  inline void MotionScriptOption::reset()
  {
    d_hasTerminated = false;
    d_request = nullptr;
  }

}
