// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motionscriptoption.ih"

vector<shared_ptr<Option>> MotionScriptOption::runPolicy(Writer& writer)
{
  writeHasExisting(writer);

  if (d_hasTerminated)
  {
    writeTerminated(writer);
    return {};
  }

  ASSERT(d_script != nullptr);

  if (!d_request)
  {
    // This is the first execution since being reset
    if (d_ifNotFinalPose && MotionScriptRunner::isInFinalPose(d_script))
    {
      // Don't run.
      d_request = nullptr;
      d_hasTerminated = true;
      writeSkip(writer);
      writeTerminated(writer);
      return {};
    }

    d_request = d_motionScriptModule->run(d_script);

    // TODO fix << operator for MotionRequestStatus
    Log::verbose("MotionScriptOption::runPolicy") << "Motion script '" << getId() << "' requested with status: " << MotionRequest::getStatusName(d_request->getStatus());
    writeStatus(writer);
  }
  else
  {
    // We're continuing a previous motion request. Check its status.
    writeStatus(writer);

    switch (d_request->getStatus())
    {
    case MotionRequest::Status::Completed:
      d_hasTerminated = true;
      break;
    case MotionRequest::Status::Ignored:
      Log::verbose("MotionScriptOption::runPolicy") << "Retrying ignored motion request for script: " << d_script->getName();
      d_request = d_motionScriptModule->run(d_script);
      break;
    default:
      break;
    }
  }

  writeTerminated(writer);
  return {};
}
