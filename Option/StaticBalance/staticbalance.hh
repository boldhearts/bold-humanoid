// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "stats/movingaverage.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class StandModule;
    }
  }
  
  class Balance;

  class StaticBalance : public Option
  {
  public:
    StaticBalance(std::string const& id, std::shared_ptr<motion::modules::StandModule> standModule);
    
    OptionVector runPolicy() override;

    void reset() override;

  private:
    std::shared_ptr<motion::modules::StandModule> d_standModule;
    std::shared_ptr<Balance> d_balance;

    MovingAverage<double> d_controlSmoother;
    
    config::Setting<double>* d_xOffset;
    config::Setting<double>* d_yOffset;
    config::Setting<double>* d_zOffset;
    config::Setting<double>* d_hipPitch;
  };
  
}
