// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "staticbalance.hh"

#include "config/Config/config.hh"
#include "Math/math.hh"
#include "Balance/PIDBalance/COPBalance/copbalance.hh"
#include "Balance/PIDBalance/StabilityReferenceBalance/stabilityreferencebalance.hh"
#include "MX28/mx28.hh"

#include "motion/modules/StandModule/standmodule.hh"

#include <Eigen/Core>

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::util;
using namespace std;
using namespace Eigen;

StaticBalance::StaticBalance(string const& id, std::shared_ptr<StandModule> standModule)
  : Option{id, "static-balance"},
    d_standModule{move(standModule)},
    d_controlSmoother{5}
{
  d_xOffset = Config::getSetting<double>("walk-engine.params.x-offset");
  d_yOffset = Config::getSetting<double>("walk-engine.params.y-offset");
  d_zOffset = Config::getSetting<double>("walk-engine.params.z-offset");
  d_hipPitch = Config::getSetting<double>("walk-module.hip-pitch.stable-angle");

  auto haveFSR = Config::getStaticValue<bool>("hardware.fsr.enabled");
  if (haveFSR)
    d_balance = make_shared<COPBalance>();
  else
    d_balance = make_shared<StabilityReferenceBalance>();
}

Option::OptionVector StaticBalance::runPolicy()
{
  auto balanceOffsets = d_balance->computeCorrection(0);
  auto control = MX28::valueDelta2RadsDelta(balanceOffsets.hipPitch);
  control = d_controlSmoother.next(control);
  Log::verbose("StaticBalance::runPolicy") << "Control: " << control;
  auto offsets = Vector3d{d_xOffset->getValue(), d_yOffset->getValue(), d_zOffset->getValue()};

  d_standModule->stand(offsets, control - Math::degToRad(d_hipPitch->getValue()));

  return {};
}

void StaticBalance::reset()
{
  d_balance->reset();
}
