// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "locateball.hh"

#include "Option/LookAround/lookaround.hh"
#include "Option/LookAtBall/lookatball.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace bold::vision;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

LocateBall::LocateBall(string const& id,
                       shared_ptr<HeadModule> headModule, shared_ptr<CameraModel> cameraModel, shared_ptr<Clock> clock,
                       function<double(uint)> speedCallback, unsigned maxCount, unsigned thresholdCount)
: Option(id, "LocateBall"),
  d_headModule(move(headModule)),
  d_clock(move(clock)),
  d_lookAroundOption(make_shared<LookAround>("look-around-for-ball", d_headModule,
                                             Config::getStaticValue<double>("options.locate-ball.side-angle"),
                                             d_clock, speedCallback)),
  d_lookAtBallOption(make_shared<LookAtBall>("look-at-ball", cameraModel, d_headModule)),
  d_visibleCount(0),
  d_stepCount(0),
  d_maxCount(maxCount),
  d_thresholdCount(thresholdCount)
{}

vector<shared_ptr<Option>> LocateBall::runPolicy(Writer<StringBuffer>& writer)
{
  d_stepCount++;

  auto agentFrame = State::get<AgentFrameState>();

  if (!agentFrame)
    return {};

  auto priorVisibleCount = d_visibleCount;

  if (agentFrame->isBallVisible())
  {
    if (d_visibleCount < d_maxCount)
      d_visibleCount++;
  }
  else
  {
    if (d_visibleCount > 0)
      d_visibleCount--;
  }

  // If we've just started this option and have seen a ball, don't look away from it!
  if (d_stepCount < d_maxCount && d_visibleCount != 0)
    return {d_lookAtBallOption};

  // If we are generally seeing a ball, fixate on it
  if (d_visibleCount > d_thresholdCount)
  {
    // If we have just started fixating on a ball...
    if (priorVisibleCount <= d_thresholdCount)
    {
      // Record the point of the scan we were at, and the time at which we stopped
      d_stoppedAtPhase = d_lookAroundOption->getPhase();
      d_stoppedScanningAt = d_clock->getTimestamp();
    }

    return {d_lookAtBallOption};
  }
  else if (priorVisibleCount > d_thresholdCount)
  {
    // We have lost sight of the ball
    // If we only recently saw it...
    if (d_clock->getSecondsSince(d_stoppedScanningAt) < 2.0)
    {
      // Continue scanning for it from the point we left off at
      d_lookAroundOption->setPhase(d_stoppedAtPhase);
    }
  }

  // Otherwise we are not very confident we're looking at a ball, so keep looking.
  return {d_lookAroundOption};
}

void LocateBall::reset()
{
  d_visibleCount = 0;
  d_stepCount = 0;
  d_lookAroundOption->reset();
  d_lookAtBallOption->reset();
}
