// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Clock/clock.hh"
#include "Option/option.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
      {
       class HeadModule;
      }
  }

  template<typename> class Setting;

  namespace vision {
    class CameraModel;
  }

  class LookAround;
  class LookAtBall;

  /** Controls the head while standing still, in order to find the ball.
  */
  class LocateBall : public Option
  {
  public:
    LocateBall(std::string const& id,
               std::shared_ptr<motion::modules::HeadModule> headModule, std::shared_ptr<vision::CameraModel> cameraModel, std::shared_ptr<Clock> clock,
               std::function<double(unsigned)> speedCallback = nullptr, unsigned maxCount = 10, unsigned thresholdCount = 5);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void reset() override;

  private:
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    std::shared_ptr<Clock> d_clock;
    std::shared_ptr<LookAround> d_lookAroundOption;
    std::shared_ptr<LookAtBall> d_lookAtBallOption;
    unsigned d_visibleCount;
    unsigned d_stepCount;
    unsigned d_maxCount;
    unsigned d_thresholdCount;
    double d_stoppedAtPhase;
    Clock::Timestamp d_stoppedScanningAt;
  };
}
