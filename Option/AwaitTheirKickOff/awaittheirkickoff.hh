// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "stats/movingaverage.hh"
#include "src/util/Maybe/maybe.hh"

#include <Eigen/Core>

namespace bold
{
  class Clock;
  template<typename> class Setting;

  /** Terminates only when the opponent's kick-off time elapses, or the ball is moved away from its initial position. */
  class AwaitTheirKickOff : public Option
  {
  public:
    AwaitTheirKickOff(std::string id, std::shared_ptr<Clock> clock);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    double hasTerminated() override;

    void reset() override;

  private:
    std::shared_ptr<Clock> d_clock;
    
    /// Smoothed ball position estimate
    MovingAverage<Eigen::Vector2d> d_ballPosition;
    /// Mature estimate of the ball's initial position, against which later mature estimates will be compared
    util::Maybe<Eigen::Vector2d> d_ballInitialPosition;
    /// Setting that specifies the distance the ball must move in order to terminate this option
    config::Setting<double>* d_requiredMoveDistance;
  };
}
