// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "awaittheirkickoff.hh"

#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/GameState/gamestate.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using namespace rapidjson;
using namespace Eigen;

AwaitTheirKickOff::AwaitTheirKickOff(string id, shared_ptr<Clock> clock)
    : Option(id, "AwaitTheirKickOff"),
      d_clock(move(clock)),
      d_ballPosition((uint16_t) Config::getStaticValue<int>("options.await-their-kick-off.ball-pos-window-size")),
      d_ballInitialPosition(),
      d_requiredMoveDistance(Config::getSetting<double>("options.await-their-kick-off.required-ball-movement")) {}

vector<shared_ptr<Option>> AwaitTheirKickOff::runPolicy(Writer<StringBuffer> &writer) {
  // Observe the ball position
  auto agentFrame = State::get<AgentFrameState>();

  ASSERT(agentFrame);

  if (agentFrame->isBallVisible()) {
    // Integrate the observed position
    d_ballPosition.next(agentFrame->getBallObservation()->head<2>());

    if (!d_ballInitialPosition.hasValue()) {
      // No initial ball position estimate yet.
      if (d_ballPosition.isMature()) {
        // We have enough observations to make our estimate
        d_ballInitialPosition = d_ballPosition.getAverage();

        Log::info("AwaitTheirKickOff::runPolicy") << "Initial ball position estimate made: "
                                                  << d_ballInitialPosition->x() << ", " << d_ballInitialPosition->y();

        // Reset our average and start building another which we can use to transition out of waiting when
        // we detect the ball has moved.
        d_ballPosition.reset();
      }
    }
  }

  return {};
}

double AwaitTheirKickOff::hasTerminated() {
  auto game = State::get<GameState>();

  // We should have used GameState to get into this state in the first place!
  ASSERT(game);

  // If the time period has elapsed, then terminate this state
  if (!game->isWithinTenSecondsOfKickOff(Team::Them, *d_clock)) {
    Log::info("AwaitTheirKickOff::hasTerminated") << "Ten second period has elapsed";
    return 1.0;
  }

  // Has the ball moved far enough to abort the 10 second wait?
  if (d_ballInitialPosition.hasValue() && d_ballPosition.isMature()) {
    // We have an initial position estimate, and now have a mature estimate of the current ball's position
    auto distance = (*d_ballInitialPosition - d_ballPosition.getAverage()).norm();

    if (distance > d_requiredMoveDistance->getValue()) {
      Log::info("AwaitTheirKickOff::hasTerminated") << "Ball has moved " << distance
                                                    << " from it's initial position to: "
                                                    << d_ballPosition.getAverage().x() << ", "
                                                    << d_ballPosition.getAverage().y();
      return 1.0;
    }
  }

  return 0.0;
}

void AwaitTheirKickOff::reset() {
  d_ballPosition.reset();
  d_ballInitialPosition.reset();
}
