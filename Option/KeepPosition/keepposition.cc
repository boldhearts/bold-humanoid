// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "keepposition.hh"

#include "Option/ApproachBall/approachball.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using namespace rapidjson;
using namespace Eigen;

KeepPosition::KeepPosition(string id, PlayerRole role,
                           shared_ptr<WalkModule> walkModule,
                           shared_ptr<BehaviourControl> behaviourControl,
                           shared_ptr<Clock> clock,
                           shared_ptr<Draw> draw)
    : Option{id, "KeepPosition"},
      d_walkModule{move(walkModule)},
      d_role{role},
      d_clock{move(clock)},
      d_approachBall{make_shared<ApproachBall>("approachBallKeepingPosition",
                                               d_walkModule,
                                               behaviourControl,
                                               move(draw))},
      d_supporterSpacing{Config::getSetting<double>("options.keep-position.spacing")} {}

vector<shared_ptr<Option>> KeepPosition::runPolicy(Writer<StringBuffer> &writer) {
  // TODO require certain delta if not walking in order to start walking -- don't try and correct by 3cm for example

  auto team = State::get<TeamState>();

  if (!team || team->empty()) {
    Log::warning("KeepPosition::runPolicy") << "Should not be keeping position when no team mates visible";
    return {};
  }

  auto agentFrame = State::get<AgentFrameState>();
  if (!agentFrame->isBallVisible()) {
    d_walkModule->stop();
    return {};
  }

  //
  // Determine our rank among teammates
  //

  auto observers = team->getBallObservers([this](PlayerState const &player) {
    return d_clock->getMillisSince(player.updateTime) <= 5000;
  });

  sort(observers.begin(), observers.end(),
       [](PlayerState const &a, PlayerState const &b) -> bool {
         return a.ballRelative->norm() > b.ballRelative->norm();
       });

  int rank = 1;
  double dist = (*(agentFrame->getBallObservation()) - geometry2::Point3d::ORIGIN).norm();

  for (PlayerState const &player : observers) {
    if (player.ballRelative->norm() < dist)
      rank++;
  }

  // Calculate max distance from ball as multiple of supporter spacing distance
  auto maxDistance = d_supporterSpacing->getValue() * rank;

  // Ensure we are at at least the max distance from the ball

  if (dist < maxDistance) {
    d_walkModule->stop();
    return {};
  }

  d_approachBall->setStopDistance(maxDistance);

  return {d_approachBall};
}
