// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "state/StateObject/TeamState/teamstate.hh"

namespace bold {
  namespace motion {
    namespace modules {
      class WalkModule;
    }
  }

  class ApproachBall;

  class BehaviourControl;

  template<typename>
  class Setting;

  class Draw;

  class KeepPosition : public Option {
  public:
    KeepPosition(std::string id, state::PlayerRole role,
                 std::shared_ptr<motion::modules::WalkModule> walkModule,
                 std::shared_ptr<BehaviourControl> behaviourControl,
                 std::shared_ptr<Clock> clock,
                 std::shared_ptr<Draw> draw);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer> &writer) override;

  private:
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    state::PlayerRole d_role;
    std::shared_ptr<Clock> d_clock;
    std::shared_ptr<ApproachBall> d_approachBall;
    config::Setting<double> *d_supporterSpacing;
  };
}
