// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

namespace bold
{
  class Agent;

  // TODO would a generic 'until' option that takes a std::function be useful?

  /** Executes the specified sub-option until the agent shuts down. */
  class UntilShutdown : public Option
  {
  public:
    UntilShutdown(std::string id, Agent* agent, std::shared_ptr<Option> beforeShutdown, std::shared_ptr<Option> afterShutdown);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    double hasTerminated() override;

  private:
    Agent* d_agent;
    std::shared_ptr<Option> d_beforeShutdown;
    std::shared_ptr<Option> d_afterShutdown;
  };
}
