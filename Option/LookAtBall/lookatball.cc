// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "lookatball.hh"

#include "state/State/state.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"

#include "motion/modules/HeadModule/headmodule.hh"
#include "vision/CameraModel/cameramodel.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace bold::vision;
using namespace Eigen;
using namespace std;
using namespace rapidjson;

LookAtBall::LookAtBall(std::string const &id, std::shared_ptr<CameraModel> cameraModel,
                       std::shared_ptr<HeadModule> headModule)
    : Option(id, "LookAtBall"),
      d_cameraModel(cameraModel),
      d_headModule(headModule),
      d_gain(Config::getSetting<double>("options.look-at-ball.gain")),
      d_minOffset(Config::getSetting<double>("options.look-at-ball.offset-min")),
      d_maxOffset(Config::getSetting<double>("options.look-at-ball.offset-max")) {}

vector<shared_ptr<Option>> LookAtBall::runPolicy(Writer<StringBuffer> &writer) {
  auto const &ballObs = State::get<CameraFrameState>()->getBallObservation();

  if (!ballObs.hasValue()) {
    writer.String("ball");
    writer.Null();
//     Log::warning("LookAtBall::runPolicy") << "No ball observation in AgentFrame yet LookAtBall was run";
    return {};
  }

  static unsigned w = d_cameraModel->imageWidth();
  static unsigned h = d_cameraModel->imageHeight();

  static Vector2d centerPx = Vector2d(w, h) / 2;
  static double happ = d_cameraModel->rangeHorizontalDegs() / w;
  static double vapp = d_cameraModel->rangeVerticalDegs() / h;

  Vector2d ballPos = ballObs->head<2>();

  double r = d_gain->getValue();
  Vector2d offset = (ballPos - centerPx) * r;

  offset.x() *= happ; // pixel per angle
  offset.y() *= vapp; // pixel per angle

  double maxOffset = d_maxOffset->getValue();
  double minOffset = d_minOffset->getValue();

  offset = offset.cwiseMin(Vector2d(maxOffset, maxOffset)).cwiseMax(Vector2d(-maxOffset, -maxOffset));

//   cout << "offset: " << offset.transpose() << endl;

  if (offset.norm() < minOffset) {
    // The head is roughly looking at the ball so don't bother moving and
    // reset any accumulated state in the tracking logic (PID build up.)
    d_headModule->initTracking();
  } else {
    d_headModule->moveTracking(offset.x(), offset.y());
  }

  writer.String("offset");
  writer.StartArray();
  writer.Double(offset.x());
  writer.Double(offset.y());
  writer.EndArray(2);

  return {};
}

void LookAtBall::reset() {
  d_headModule->initTracking();
}
