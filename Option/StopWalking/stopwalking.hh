// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class WalkModule;
    }
  }

  class StopWalking : public Option
  {
  public:
    StopWalking(std::string const& id, std::shared_ptr<motion::modules::WalkModule> walkModule, bool stopImmediately = false)
    : Option(id, "StopWalking"),
      d_walkModule(walkModule),
      d_stopImmediately(stopImmediately)
    {}

    double hasTerminated() override;

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

  private:
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    bool d_stopImmediately;
  };
}
