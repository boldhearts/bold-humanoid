// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "searchball.ih"

#include "config/Config/config.hh"
#include "state/StateObject/BodyState/bodystate.hh"

using namespace Eigen;
using namespace bold::config;
using namespace bold::state;

vector<shared_ptr<Option>> SearchBall::runPolicy(Writer<StringBuffer>& writer)
{
  auto body = State::get<BodyState>(StateTime::CameraImage);
  double currentPanAngleDegs = body->getJoint(JointId::HEAD_PAN)->getAngleDegs();
  double currentTiltAngleDegs = body->getJoint(JointId::HEAD_TILT)->getAngleDegs();

  static Setting<double>* turnSpeedSetting = Config::getSetting<double>("options.search-ball.turn-speed");
  static Setting<double>* maxTargetHeightSetting = Config::getSetting<double>("options.search-ball.max-target-height");
  static Setting<double>* minTargetHeightSetting = Config::getSetting<double>("options.search-ball.min-target-height");
  static Setting<double>* maxTargetSideSetting = Config::getSetting<double>("options.search-ball.max-target-side");
  static Setting<double>* speedXSetting = Config::getSetting<double>("options.search-ball.speed-x");
  static Setting<double>* speedYSetting = Config::getSetting<double>("options.search-ball.speed-y");

  double turnSpeed = turnSpeedSetting->getValue();
  double maxTargetHeight = maxTargetHeightSetting->getValue();
  double minTargetHeight = minTargetHeightSetting->getValue();
  double maxTargetSide = maxTargetSideSetting->getValue();
  double speedX = speedXSetting->getValue();
  double speedY = speedYSetting->getValue();

  // make sure head is fully turned in the direction we are turning to maximize our chances of seeing the ball
  if (d_isLeftTurn ? currentPanAngleDegs <= (maxTargetSide - speedX) : currentPanAngleDegs >= (-maxTargetSide + speedX))
  {
    // utilize this opportunity to reset ourselves to be looking for the top first
    d_searchTop = true;

    // make head go towards correct side, overshoot
    d_headModule->moveToDegs(d_isLeftTurn ? maxTargetSide + speedX : -maxTargetSide - speedX, currentTiltAngleDegs);
  }
  else
  {
    // if we are too far one way, force the need to correct movement
    if (currentTiltAngleDegs >= (maxTargetHeight - 2))
      d_searchTop = false;

    if (currentTiltAngleDegs <= (minTargetHeight + 2))
      d_searchTop = true;

    // start searching once we have maxed out to one side
    if (d_searchTop)
    {
      if (currentTiltAngleDegs <= (maxTargetHeight - speedY))
      {
        // top not reached so aim for it, overshoot
        d_headModule->moveToDegs(currentPanAngleDegs, maxTargetHeight + speedY);
      }
      else
      {
        // top reached, turn this around next time
        d_searchTop = false;
      }
    }
    else
    {
      if (currentTiltAngleDegs >= (minTargetHeight + speedY))
      {
        // bottom not reached so aim for it, overshoot
        d_headModule->moveToDegs(currentPanAngleDegs, minTargetHeight - speedY);
      }
      else
      {
        // bottom reached, turn this around next time
        d_searchTop = true;
      }
    }

    // finally we need to turn ourselves
    d_walkModule->setTurnAngle(d_isLeftTurn ? turnSpeed : -turnSpeed);
  }

  // return nothing
  return {};
}

void SearchBall::setIsLeftTurn(bool leftTurn)
{
  d_isLeftTurn = leftTurn;
}

void SearchBall::reset()
{
//   d_walkModule->reset();
}
