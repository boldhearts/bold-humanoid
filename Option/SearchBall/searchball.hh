// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class WalkModule;
      class HeadModule;
    }
  }
  
  class SearchBall : public Option
  {
  public:
    SearchBall(std::string const& id, std::shared_ptr<motion::modules::WalkModule> walkModule, std::shared_ptr<motion::modules::HeadModule> headModule)
    : Option(id, "SearchBall"),
      d_walkModule(walkModule),
      d_headModule(headModule),
      d_isLeftTurn(true),
      d_searchTop(true)
    {}

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void setIsLeftTurn(bool leftTurn);

    void reset() override;

  private:
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    bool d_isLeftTurn;
    bool d_searchTop;
  };
}
