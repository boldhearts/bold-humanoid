// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "staystanding.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "agent/FallDetector/falldetector.hh"
#include "Option/SequenceOption/sequenceoption.hh"
#include "Option/StopWalking/stopwalking.hh"

using namespace bold;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace std;

StayStanding::StayStanding(string const& id,
                           shared_ptr<Option> whileStandingOption,
                           shared_ptr<Option> getUpOption,
                           shared_ptr<BehaviourControl> behaviourControl,
                           shared_ptr<WalkModule> walkModule,
                           shared_ptr<FallDetector const> fallDetector)
  : Option{id, "StayStanding"},
    d_whileStandingOption{move(whileStandingOption)},
    d_behaviourControl{move(behaviourControl)},
    d_fallDetector{move(fallDetector)}
{
  auto stopWalkingImmediately = make_shared<StopWalking>("stop-walking", walkModule, true);
  d_getUpSequence =
    Option::make<SequenceOption>("get-up-sequence",
                                 OptionVector{{ stopWalkingImmediately, getUpOption }});
}

Option::OptionVector StayStanding::runPolicy()
{
  if (d_gettingUp)
  {
    if (!d_getUpSequence->hasTerminated())
      return {d_getUpSequence};
    else
      updateStatus(false);
  }

  auto hasFallen = d_fallDetector->getFallenState() != FallState::STANDUP;

  if (hasFallen)
  {
    updateStatus(true);
    return {d_getUpSequence};
  }
  else
    return {d_whileStandingOption};
}

void StayStanding::updateStatus(bool getUp)
{
  if (getUp)
  {
    d_gettingUp = true;
    d_behaviourControl->setPlayerStatus(PlayerStatus::Inactive);
    d_behaviourControl->setPlayerActivity(PlayerActivity::Waiting);
  }
  else
  {
    d_gettingUp = false;
    d_behaviourControl->setPlayerStatus(PlayerStatus::Active);
    d_getUpSequence->reset();
  }
}
