// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include <iosfwd>

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class WalkModule;
    }
  }
  
  class BehaviourControl;
  class FallDetector;

  class StayStanding : public Option
  {
  public:
    StayStanding(std::string const& id,
                 std::shared_ptr<Option> whileStandingOption,
                 std::shared_ptr<Option> getUpOption,
                 std::shared_ptr<BehaviourControl> behaviourControl,
                 std::shared_ptr<motion::modules::WalkModule> walkModule,
                 std::shared_ptr<FallDetector const> fallDetector);

    OptionVector runPolicy() override;

  private:
    void updateStatus(bool getUp);

    std::shared_ptr<Option> d_whileStandingOption;
    std::shared_ptr<Option> d_getUpSequence;

    std::shared_ptr<BehaviourControl> d_behaviourControl;
    std::shared_ptr<FallDetector const> d_fallDetector;

    bool d_gettingUp;
  };
}
