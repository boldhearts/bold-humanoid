// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "walkto.hh"

#include "config/Config/config.hh"
#include "Drawing/drawing.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

using namespace std;
using namespace bold;
using namespace bold::colour;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::motion::modules;
using namespace Eigen;

WalkTo::WalkTo(string const& id,
               shared_ptr<WalkModule> walkModule, shared_ptr<Draw> draw)
  : Option{id, "WalkTo"},
    d_targetPos{0.0, 0.0},
    d_targetAngle{0.0},
    d_turnDist{0.5},
    d_walkModule{walkModule},
    d_draw{draw}
{
  d_turnScale          = Config::getSetting<double>("options.walkto.turn-speed-scale");
  d_maxForwardSpeed    = Config::getSetting<double>("options.walkto.max-forward-speed");
  d_minForwardSpeed    = Config::getSetting<double>("options.walkto.min-forward-speed");
  d_maxSidewaysSpeed   = Config::getSetting<double>("options.walkto.max-sideways-speed");
  d_minSidewaysSpeed   = Config::getSetting<double>("options.walkto.min-sideways-speed");
  d_brakeDistance      = Config::getSetting<double>("options.walkto.brake-distance");
  d_lowerTurnLimitDegs = Config::getSetting<double>("options.walkto.lower-turn-limit-degs");
  d_upperTurnLimitDegs = Config::getSetting<double>("options.walkto.upper-turn-limit-degs");

}

Option::OptionVector WalkTo::runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer)
{
  ASSERT(d_upperTurnLimitDegs->getValue() > d_lowerTurnLimitDegs->getValue());
  ASSERT(d_brakeDistance->getValue() != 0);

  d_draw->circle(Frame::Agent, d_targetPos, 0.1, BGR::darkBlue(), 0.8, 1.0);
  d_draw->line(Frame::Agent, LineSegment2d{Point2d::Zero(), d_targetPos}, BGR::darkBlue(), 0.8, 1.0);

  double walkDist = (d_targetPos - Point2d::ORIGIN).norm();

//  static auto stoppingDistance = Config::getSetting<double>("options.approach-ball.stop-distance");

  writer.String("walkDist");
  writer.Double(walkDist);

  double speedScaleDueToDistance = Math::clamp(walkDist / d_brakeDistance->getValue(), 0.0, 1.0);

  writer.String("distSpeed");
  writer.Double(speedScaleDueToDistance);

  double faceAngle = 0;
  if (walkDist < d_turnDist)
    faceAngle = d_targetAngle;
  else
  {
    faceAngle = Math::angleToPoint(d_targetPos);
    if (fabs(faceAngle) > M_PI / 2.0)
      faceAngle = Math::normaliseRads(faceAngle + M_PI);
  }

  double speedScaleDueToAngle = Math::lerp(fabs(faceAngle),
                                           Math::degToRad(d_lowerTurnLimitDegs->getValue()),
                                           Math::degToRad(d_upperTurnLimitDegs->getValue()),
                                           1.0,
                                           0.0);

  writer.String("angleSpeed");
  writer.Double(speedScaleDueToAngle);

  double xSpeedScale = Math::clamp(speedScaleDueToDistance * speedScaleDueToAngle * d_targetPos.y(), 0.0, 1.0);
  double ySpeedScale = Math::clamp(speedScaleDueToDistance * speedScaleDueToAngle * d_targetPos.x(), 0.0, 1.0);

  double xSpeed = Math::lerp(xSpeedScale,
                             d_minForwardSpeed->getValue(),
                             d_maxForwardSpeed->getValue());

  double ySpeed = -Math::lerp(ySpeedScale,
                             d_minSidewaysSpeed->getValue(),
                             d_maxSidewaysSpeed->getValue());

  // unspecified units
  double turnSpeed = faceAngle * d_turnScale->getValue();

  // x is forward, y is left
  d_walkModule->setMoveDir(xSpeed, ySpeed);
  d_walkModule->setTurnAngle(turnSpeed);

  writer.String("moveDir");
  writer.StartArray();
  writer.Double(xSpeed);
  writer.Double(ySpeed);
  writer.EndArray(2);

  writer.String("turn");
  writer.Double(turnSpeed);

  return {};

}


