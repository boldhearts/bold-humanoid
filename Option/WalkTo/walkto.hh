// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

#include "src/geometry2/Point/point.hh"
#include <Eigen/Core>

namespace bold
{
  namespace motion
  {
    namespace modules
      {
       class WalkModule;
      }
  }
  
  class Draw;
  
  class WalkTo : public Option
  {
  public:
    WalkTo(std::string const& id,
           std::shared_ptr<motion::modules::WalkModule> walkModule, std::shared_ptr<Draw> draw);

    OptionVector runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    void setTargetPosition(geometry2::Point2d targetPos, double targetAngle)
    {
      d_targetPos = targetPos;
      d_targetAngle = targetAngle;
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  private:
    geometry2::Point2d d_targetPos;
    double d_targetAngle;
    double d_turnDist;

    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    std::shared_ptr<Draw> d_draw;
    
    config::Setting<double>* d_turnScale;
    config::Setting<double>* d_maxForwardSpeed;
    config::Setting<double>* d_minForwardSpeed;
    config::Setting<double>* d_maxSidewaysSpeed;
    config::Setting<double>* d_minSidewaysSpeed;
    config::Setting<double>* d_brakeDistance;
    config::Setting<double>* d_lowerTurnLimitDegs;
    config::Setting<double>* d_upperTurnLimitDegs;
  };
}
