// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "src/util/Log/log.hh"

#include <sstream>
#include <map>
#include <set>
#include <memory>

namespace bold
{
  template<typename T>
  class DispatchOption : public Option
  {
  public:
    DispatchOption(std::string id, std::function<T()> keyGetter)
    : Option(id, "Dispatch"),
      d_keyGetter(keyGetter)
    {}

    void setOption(T key, std::shared_ptr<Option> option)
    {
      d_optionByKey.insert(make_pair(key, option));
    }

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override
    {
      T key = d_keyGetter();

      std::stringstream keyStr;
      keyStr << key;
      writer.String("key");
      writer.String(keyStr.str().c_str());

      auto it = d_optionByKey.find(key);
      if (it == d_optionByKey.end())
      {
        static std::set<T> unknownKeys;
        if (unknownKeys.find(key) == unknownKeys.end())
        {
          util::Log::error("DispatchOption::runPolicy") << "No option available for key " << key;
          unknownKeys.insert(key);
        }
        writer.String("found");
        writer.Bool(false);
        return {};
      }

      writer.String("found");
      writer.Bool(true);
      return { it->second };
    }

  private:
    std::function<T()> d_keyGetter;
    std::map<T,std::shared_ptr<Option>> d_optionByKey;
  };
}
