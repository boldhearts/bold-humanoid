// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "state/StateObject/GameState/gamestate.hh"

namespace bold {
  namespace motion {
    namespace modules {
      class MotionScriptModule;
    }
  }

  class MotionScriptOption;

  class Voice;

  class GameOver : public Option {
  public:
    GameOver(std::string const &id, std::shared_ptr<motion::modules::MotionScriptModule> motionScriptModule,
             std::shared_ptr<Voice> voice);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer> &writer) override;

    void reset() override;

    double hasTerminated() override;

  private:
    std::shared_ptr<MotionScriptOption> d_motionScriptOption;
    std::shared_ptr<Voice> d_voice;
    state::GameResult d_result;
    bool d_isScriptPlaying;
    unsigned d_playCount;
    bool d_hasTerminated;
  };
}
