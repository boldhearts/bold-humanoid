// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "Clock/clock.hh"

#include <functional>
#include <memory>

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class HeadModule;
    }
  }
  
  template<typename> class Setting;

  struct LookAroundStage
  {
    LookAroundStage(double durationSeconds, double tiltAngle, double panAngle)
    : durationSeconds(durationSeconds), tiltAngle(tiltAngle), panAngle(panAngle)
    {}

    double durationSeconds;
    double tiltAngle;
    double panAngle;
  };

  class LookAround : public Option
  {
  public:
    LookAround(std::string const& id, std::shared_ptr<motion::modules::HeadModule> headModule, double sideAngle, std::shared_ptr<Clock> clock, std::function<double(uint)> speedCallback = nullptr);

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    virtual void reset();

    void setPhase(double phase);

    double getPhase() const { return d_phase; };

    static std::function<double(uint)> speedIfBallVisible(double scaleWhenVisible, double scaleWhenNotVisible = 1.0, double loopExp = 0.5);

    static double speedForLoop(uint loopCount, double loopExp = 0.0);


  private:
    std::vector<LookAroundStage> d_stages;
    std::function<double(uint)> d_speedCallback;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;

    std::shared_ptr<Clock> d_clock;

    /// The amount the pan speed is to be increased per step after being lowered by the speed callback
    config::Setting<double>* d_speedStep;

    Clock::Timestamp d_lastTime;
    double d_lastSpeed;
    double d_phase;

    /// The number of full cycles the head has completed since the option was last reset. Zero during the first loop.
    unsigned d_loopCount;
  };
}
