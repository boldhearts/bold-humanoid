// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <vector>
#include <memory>
#include <string>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include "config/Config/config.hh"
#include "util/memory.hh"

namespace bold
{
  /** Option
   *
   * An option is an abstract description of an action that may be
   * temporally extended and semi-Markovian. This means it may be
   * active for any length of time, and its policy may rely not only
   * on the current state, but on any part of history. An option is
   * formed by a three-tuple @f$<I,\beta,\mu>@f, where:
   *
   * * @f$I@f$ is the set of states in which this option can be active
   * * @f$\beta@f$ gives the probability of this option terminating now
   * * @f$\mu@f$ is a policy over (sub)-options that selects which
   *   action to take at which time
   *
   */
  class Option
  {
  public:
    using OptionVector = std::vector<std::shared_ptr<Option>>;

    Option(std::string const& id, std::string const& typeName)
    : d_id(id),
      d_typeName(typeName)
    {}

    virtual ~Option() = default;

    /** Get this option's ID
     */
    std::string getId() const { return d_id; }

    /** Get the type of this option, as a string
     */
    std::string getTypeName() const { return d_typeName; }

    /** Check the probability this option having terminated
     *
     * @return the probability that this option has reached its
     * goal. Default: always 1.0, i.e. single step action
     */
    virtual double hasTerminated() { return 1; }

    virtual void reset() {}

    /** Select this option to be run
     *
     * If this option returns an empty vector, this indicates that it
     * is a primitive option. Override this version to be able to send
     * JSON data to round table.
     *
     * @returns the sub-option selected by the policy of this option;
     * Default: empty vector
     */
    virtual OptionVector runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) { return runPolicy(); }

    /** Select this option to be run
     *
     * If this option returns an empty vector, this indicates that it
     * is a primitive option.
     *
     * @returns the sub-option selected by the policy of this option;
     * Default: empty vector
     */
    virtual OptionVector runPolicy() { return OptionVector{}; }

    template<typename T, typename... Args>
    static std::unique_ptr<T> make(Args&&... args)
    {
      return std::make_unique<T>(std::forward<Args>(args)...);
    }
    
    template<typename T, typename... Args>
    static std::shared_ptr<T> make_shared(Args&&... args)
    {
      return Option::make<T>(std::forward<Args>(args)...);
    }

  protected:
    /** Get a setting for this option
     *
     * Helper method to easily get settings under `options.typename`
     * path
     */
    template<typename T>
    config::Setting<T>* getSetting(std::string const& path) const
    {
      return config::Config::getSetting<T>("options." + d_typeName + "." + path);
    }

  private:
    std::string d_id;
    std::string d_typeName;
  };
}
