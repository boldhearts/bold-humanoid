// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "waitforwhistle.hh"

#include "config/Config/config.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace std;
using namespace rapidjson;

WaitForWhistle::WaitForWhistle(string id)
: Option(id, "WaitForWhistle")
{
  reset();

  d_windowType = Config::getSetting<WindowFunctionType>("whistle-detection.window-function");
  d_windowType->track([this] (WindowFunctionType type) { if (d_listener) d_listener->setWindowFunction(type); });
}

vector<shared_ptr<Option>> WaitForWhistle::runPolicy(Writer<StringBuffer>& writer)
{
  if (!d_initialised)
  {
    d_listener = make_unique<WhistleListener>();
    if (!d_listener->initialise())
    {
      // Failed to initialise sound device, so release and return (already logged)
      d_listener.reset();
      return {};
    }
    d_listener->setWindowFunction(d_windowType->getValue());
    d_initialised = true;
  }

  if (d_listener->step())
    d_terminated = true;

  return {};
}

double WaitForWhistle::hasTerminated()
{
  return d_terminated ? 1.0 : 0.0;
}

void WaitForWhistle::reset()
{
  d_initialised = false;
  d_terminated = false;

  if (d_listener)
    d_listener.reset();
}
