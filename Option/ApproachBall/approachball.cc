// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "approachball.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "Drawing/drawing.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

#include "geometry2/Geometry/MultiPoint/Polygon/polygon.hh"
#include "geometry2/Operator/Predicate/Contains/contains.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

using namespace bold;
using namespace bold::colour;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

vector<shared_ptr<Option>> ApproachBall::runPolicy(Writer<StringBuffer>& writer)
{
  static auto turnScale          = Config::getSetting<double>("options.approach-ball.turn-speed-scale");
  static auto maxForwardSpeed    = Config::getSetting<double>("options.approach-ball.max-forward-speed");
  static auto minForwardSpeed    = Config::getSetting<double>("options.approach-ball.min-forward-speed");
  static auto brakeDistance      = Config::getSetting<double>("options.approach-ball.brake-distance");
  static auto lowerTurnLimitDegs = Config::getSetting<double>("options.approach-ball.lower-turn-limit-degs");
  static auto upperTurnLimitDegs = Config::getSetting<double>("options.approach-ball.upper-turn-limit-degs");

  ASSERT(upperTurnLimitDegs->getValue() > lowerTurnLimitDegs->getValue());
  ASSERT(brakeDistance->getValue() != 0);

  auto agentFrame = State::get<AgentFrameState>();

  if (!agentFrame->isBallVisible())
  {
    writer.String("ballPos");
    writer.Null();
    return {};
  }

  //
  // WALK TOWARDS BALL
  //

  auto ballPos = Point2d{agentFrame->getBallObservation()->head<2>()};

  writer.String("ballPos");
  writer.StartArray();
  writer.Double(ballPos.x());
  writer.Double(ballPos.y());
  writer.EndArray(2);

  double ballDist = (ballPos - Point2d::ORIGIN).norm();

  // If we're close to the ball, tell team mates that we're attacking
  d_behaviourControl->setPlayerActivity(
    ballDist < 0.5
      ? PlayerActivity::AttackingGoal
      : PlayerActivity::ApproachingBall);

  static auto stoppingDistance = Config::getSetting<double>("options.approach-ball.stop-distance");

  // Subtract the stopping distance here so that the bot doesn't stop too suddenly
  double walkDist = d_useCustomStopDistance
    ? ballDist - d_stopDistance
    : ballDist - stoppingDistance->getValue();

  if (walkDist < 0)
    walkDist = 0;

  writer.String("ballDist");
  writer.Double(ballDist);

  writer.String("walkDist");
  writer.Double(walkDist);

  double speedScaleDueToDistance = Math::clamp(walkDist/brakeDistance->getValue(), 0.0, 1.0);

  writer.String("distSpeed");
  writer.Double(speedScaleDueToDistance);

  Point2d target = ballPos + Vector2d(0.04, 0);
  double ballAngleRads = Math::angleToPoint(ballPos);
  double targetAngleRads = Math::angleToPoint(target);

  double speedScaleDueToAngle = Math::lerp(fabs(targetAngleRads),
                                           Math::degToRad(lowerTurnLimitDegs->getValue()),
                                           Math::degToRad(upperTurnLimitDegs->getValue()),
                                           1.0,
                                           0.0);

  writer.String("angleSpeed");
  writer.Double(speedScaleDueToAngle);

  double xSpeedScale = speedScaleDueToDistance * speedScaleDueToAngle;
  ASSERT(xSpeedScale >= 0.0);
  ASSERT(xSpeedScale <= 1.0);
  double xSpeed = Math::lerp(xSpeedScale,
                             minForwardSpeed->getValue(),
                             maxForwardSpeed->getValue());

  ASSERT(xSpeed >= minForwardSpeed->getValue());

  // unspecified units
  double turnSpeed = targetAngleRads * turnScale->getValue();

  double ySpeed = 0;

  //
  // AVOID OBSTACLES
  //

  static auto avoidObstacles         = Config::getSetting<bool>("options.approach-ball.avoid-obstacles.enabled");
  static auto laneWidth              = Config::getSetting<double>("options.approach-ball.avoid-obstacles.lane-width");
  static auto occlusionBrakeDistance = Config::getSetting<double>("options.approach-ball.avoid-obstacles.brake-distance");
  static auto minForwardSpeedScale   = Config::getSetting<double>("options.approach-ball.avoid-obstacles.min-fwd-scale");
  static auto avoidTurnSpeed         = Config::getSetting<double>("options.approach-ball.avoid-obstacles.turn-speed");
  static auto sideStepSpeed          = Config::getSetting<double>("options.approach-ball.avoid-obstacles.side-step-speed");
  static auto ignoreNearBallDistance = Config::getSetting<double>("options.approach-ball.avoid-obstacles.ignore-near-ball-dist");

  if (avoidObstacles->getValue())
  {
    // Determine the polygon of the direct lane to the ball
    Vector2d perp(Math::findPerpendicularVector(target - Point2d::ORIGIN).normalized() * (laneWidth->getValue() / 2.0));

    Point2d::Vector lanePoints;
    lanePoints.push_back(target + perp);
    lanePoints.push_back(Point2d::ORIGIN + perp);
    lanePoints.push_back(Point2d::ORIGIN - perp);
    lanePoints.push_back(target - perp);

    auto lanePoly = Polygon2d{lanePoints};

    d_draw->fillPolygon(Frame::Agent, lanePoly, BGR::blue(), 0.1, BGR::blue(), 0.5, 1.5);

    // Iterate through the occlusion rays
    double minDistInLane = numeric_limits<double>::max();
    double leftPenaltySum = 0, rightPenaltySum = 0;
    for (auto const& ray : agentFrame->getOcclusionRays())
    {
      // Only consider occlusion points within our lane
      if (!contains(lanePoly, ray.near()))
        continue;

      // Ignore occlusion points which are quite close to the ball
      if (agentFrame->isNearBall(ray.near(), ignoreNearBallDistance->getValue()))
        continue;

      // Draw for debugging
      d_draw->circle(Frame::Agent, ray.near(), 0.02, BGR::lightBlue(), 1, 0.8);

      auto nearDist = (ray.near() - Point2d::ORIGIN).norm();
      // Remember the minimum distance to an obstacle in the lane
      minDistInLane = std::min(minDistInLane, nearDist);

      // Turn penalty
      double penalty = Math::clamp((ballDist - nearDist) / ballDist, 0.0, 1.0);
      if (Math::angleToPoint(ray.near()) < ballAngleRads)
        leftPenaltySum += penalty;
      else
        rightPenaltySum += penalty;
    }

    // Check there's a difference between sides (also caters for zeroes which causes NaN)
    if (leftPenaltySum != rightPenaltySum)
    {
      // Turn away from the occluded side
      double turnRatio = leftPenaltySum < rightPenaltySum
        ? -1 + (leftPenaltySum / rightPenaltySum)
        :  1 - (rightPenaltySum / leftPenaltySum);
      turnSpeed += turnRatio * avoidTurnSpeed->getValue();
      ySpeed += turnRatio * sideStepSpeed->getValue();
    }

    // Slow down if there is an occlusion close in front of us
    double brakeDist = occlusionBrakeDistance->getValue();
    if (minDistInLane < brakeDist)
      xSpeed *= max(minDistInLane / brakeDist, minForwardSpeedScale->getValue());
  }

  ASSERT(xSpeed <= maxForwardSpeed->getValue());

  d_walkModule->setMoveDir(xSpeed, ySpeed);
  d_walkModule->setTurnAngle(turnSpeed);

  writer.String("moveDir");
  writer.StartArray();
  writer.Double(xSpeed);
  writer.Double(ySpeed);
  writer.EndArray(2);

  writer.String("turn");
  writer.Double(turnSpeed);

  return {};
}

double ApproachBall::hasTerminated()
{
  // Approach ball until we're within a given distance
  // TODO use filtered ball position
  auto ballObs = State::get<AgentFrameState>()->getBallObservation();
  static auto stoppingDistance = Config::getSetting<double>("options.approach-ball.stop-distance");
  return (ballObs && (ballObs->head<2>().norm() < stoppingDistance->getValue()) ? 1.0 : 0.0);
}
