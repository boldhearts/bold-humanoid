// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class WalkModule;
    }
  }
  
  class BehaviourControl;
  class Draw;
  
  class ApproachBall : public Option
  {
  public:
    ApproachBall(std::string const& id,
                 std::shared_ptr<motion::modules::WalkModule> walkModule,
                 std::shared_ptr<BehaviourControl> behaviourControl,
                 std::shared_ptr<Draw> draw)
      : Option{id, "ApproachBall"},
        d_walkModule{std::move(walkModule)},
        d_behaviourControl{std::move(behaviourControl)},
        d_draw{std::move(draw)}
    {}

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;
    
    double hasTerminated() override;

    void setStopDistance(double stopDistance)
    {
      d_useCustomStopDistance = true;
      d_stopDistance = stopDistance;
    }

  private:
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    std::shared_ptr<BehaviourControl> d_behaviourControl;

    std::shared_ptr<Draw> d_draw;
    
    bool d_useCustomStopDistance;
    double d_stopDistance;
  };
}
