// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class WalkModule;
    }
  }
  
  class OdoWalkTo : public Option
  {
  public:
    OdoWalkTo(std::string const& id, std::shared_ptr<motion::modules::WalkModule> walkModule);

    void setTarget(Eigen::Vector2d targetPos, Eigen::Vector2d targetFaceDir, double maxDist);

    double hasTerminated() override;

    OptionVector runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

  private:
    void updateProgress();

    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    Eigen::Vector2d d_targetPos;
    Eigen::Vector2d d_targetFaceDir;

    double d_maxDist;
    Eigen::Affine3d d_lastOdoReading;
    Eigen::Affine3d d_progress;
  };
}
