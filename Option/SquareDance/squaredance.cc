// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "squaredance.hh"

#include "Option/OdoWalkTo/odowalkto.hh"

#include "motion/modules/WalkModule/walkmodule.hh"

using namespace bold;
using namespace bold::motion::modules;
using namespace std;
using namespace Eigen;

SquareDance::SquareDance(string const& id, shared_ptr<WalkModule> walkModule)
  : Option(id, "SquareDance"),
    d_stage{FORWARD},
    d_odoWalkTo{make_shared<OdoWalkTo>(id + ".odowalkto", walkModule)}
{
}

Option::OptionVector SquareDance::runPolicy()
{
  if (d_odoWalkTo->hasTerminated() > 0)
  {
    d_stage = (Stage)(d_stage + 1);
    if (d_stage == RESTART)
      d_stage = FORWARD;
    
    
    d_odoWalkTo->setTarget(Vector2d(1.0, 0.0),Vector2d(1.0, 0.0), 0.2);

    /*
    switch (d_stage)
    {
    case FORWARD:
      break;
    case RIGHT:
      d_odoWalkTo->setTargetPos(Vector3d(1.0, 0.0, 0.0), 0.2);
      break;
    case BACKWARD:
      d_odoWalkTo->setTargetPos(Vector3d(0.0, -1.0, 0.0), 0.2);
      break;
    case LEFT:
      d_odoWalkTo->setTargetPos(Vector3d(-1.0, 0.0, 0.0), 0.2);
      break;
    }
    */
  }

  return {d_odoWalkTo};
}
