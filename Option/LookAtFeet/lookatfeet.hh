// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "stats/movingaverage.hh"

#include <Eigen/Core>

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class HeadModule;
    }
  }
  
  namespace config
  {
    template<typename> class Setting;
  }


  class LookAtFeet : public Option
  {
  public:
    LookAtFeet(std::string const& id, std::shared_ptr<motion::modules::HeadModule> headModule);

    void reset() override { d_avgBallPos.reset(); }

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    bool hasPosition() const { return d_avgBallPos.count() != 0; }

    geometry2::Point3d getAverageBallPositionAgentFrame() const;

  private:
    MovingAverage<geometry2::Point3d> d_avgBallPos;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    config::Setting<double>* d_panDegs;
    config::Setting<double>* d_tiltDegs;
  };
}
