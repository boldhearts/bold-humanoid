// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "lookatfeet.hh"

#include "config/Config/config.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

#include "motion/modules/HeadModule/headmodule.hh"
#include "util/Log/log.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::motion::modules;
using namespace bold::state;
using namespace bold::util;
using namespace bold::geometry2;
using namespace Eigen;
using namespace std;
using namespace rapidjson;

LookAtFeet::LookAtFeet(std::string const& id, std::shared_ptr<HeadModule> headModule)
  : Option(id, "LookAtFeet"),
    d_avgBallPos(30),
    d_headModule(headModule)
{
  d_panDegs = Config::getSetting<double>("options.look-at-feet.head-pan-degs");
  d_tiltDegs = Config::getSetting<double>("options.look-at-feet.head-tilt-degs");
}

vector<shared_ptr<Option>> LookAtFeet::runPolicy(Writer<StringBuffer>& writer)
{
  d_headModule->moveToDegs(d_panDegs->getValue(), d_tiltDegs->getValue());

  auto ballObs = State::get<AgentFrameState>()->getBallObservation();

  if (ballObs)
  {
    Log::verbose("LookAtFeet::runPolicy") << "Ball pos in agent frame " << *ballObs;
    d_avgBallPos.next(*ballObs);
    writer.String("ball");
    writer.StartArray();
    writer.Double(ballObs->x());
    writer.Double(ballObs->y());
    writer.EndArray(2);
  }
  else
  {
    writer.String("ball");
    writer.Null();
  }

  return {};
}

Point3d LookAtFeet::getAverageBallPositionAgentFrame() const
{
  if (d_avgBallPos.count() == 0)
  {
    Log::error("LookAtFeet::getBallPositionAgentFrame") << "No ball observations available";
    throw runtime_error("No ball observations available");
  }

  return d_avgBallPos.getAverage();
}
