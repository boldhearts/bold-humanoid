// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "sequenceoption.hh"

#include "src/util/Log/log.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

SequenceOption::SequenceOption(std::string const& id, std::vector<std::shared_ptr<Option>> sequence, Mode mode)
  : Option{id, "SequenceOption"},
    d_sequence{move(sequence)},
    d_index{-1},
    d_mode{mode}
{
  if (d_sequence.size() == 0)
  {
    Log::error("SequenceOption::SequenceOption") << "Must specify at least one option in sequence: " << id;
    throw runtime_error("Must specify at least one option in the sequence");
  }
}

void SequenceOption::reset()
{
  d_index = -1;
}

double SequenceOption::hasTerminated()
{
  return d_mode == ONCE && d_index == static_cast<int>(d_sequence.size()) ? 1.0 : 0.0;
}

vector<shared_ptr<Option>> SequenceOption::runPolicy()
{
  if (d_index == -1)
  {
    auto option = d_sequence[0];
    option->reset();
    d_index++;
    return {option};
  }

  while (true)
  {
    if (d_index == static_cast<int>(d_sequence.size()))
    {
      switch (d_mode)
      {
      case ONCE:
        return {};
        break;
      case LOOP:
        d_index = 0;
        break;
      case REPEAT_LAST:
        return { d_sequence.back() };
        break;
      }
    }

    auto option = d_sequence[d_index];

    if (option->hasTerminated() == 1.0)
      d_index++;
    else
      return {option};
  }
}
