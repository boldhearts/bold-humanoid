// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "buildstationarymap.hh"

#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace Eigen;
using namespace rapidjson;
using namespace std;

#include "Voice/voice.hh"
#include "config/Config/config.hh"

// TODO decrease the 'score' of an estimate if it should be seen, but isn't

BuildStationaryMap::BuildStationaryMap(string const& id, std::shared_ptr<Voice> voice)
: Option(id, "BuildStationaryMap"),
  d_voice(voice)
{}

vector<shared_ptr<Option>> BuildStationaryMap::runPolicy(Writer<StringBuffer>& writer)
{
  auto agentFrame = State::get<AgentFrameState>();

  ASSERT(agentFrame);

  bool hasChange = false;

  // TODO use agentFrame->shouldSeeAgentFrameGroundPoint(...) to decrease scores

  if (agentFrame->isBallVisible())
  {
    integrate(d_ballEstimates, agentFrame->getBallObservation().value().head<2>(), StationaryMapState::BallMergeDistance);
    hasChange = true;
  }

  for (auto const& goal : agentFrame->getGoalObservations())
  {
    integrate(d_goalEstimates, goal.head<2>(), StationaryMapState::GoalPostMergeDistance);
    hasChange = true;
  }

  for (auto const& teammate : agentFrame->getTeamMateObservations())
  {
    integrate(d_teammateEstimates, teammate.head<2>(), StationaryMapState::TeammateMergeDistance);
    hasChange = true;
  }

  // TODO: Only update state object on occlusion map change
  // Walk all occlusion rays
  for (auto const& ray : agentFrame->getOcclusionRays())
  {
    d_occlusionMap.add(ray); // TODO: Remove.
//    if (d_occlusionMap.add(ray))
//      hasChange = true;
  }

  if (hasChange || !State::get<StationaryMapState>())
    updateStateObject();

//  writer.String("ball").Bool(agentFrame->isBallVisible());
//  writer.String("goals").Uint(agentFrame->getGoalObservations().size());
//  writer.String("keepers").Uint(agentFrame->getTeamMateObservations().size());
//  writer.String("change").Bool(hasChange);

  return {};
}

void BuildStationaryMap::updateStateObject() const
{
  auto map = State::make<StationaryMapState>(d_ballEstimates, d_goalEstimates, d_teammateEstimates, d_occlusionMap);

  static Setting<bool>* announceTurn = Config::getSetting<bool>("options.announce-stationary-map-action");

  if (announceTurn->getValue() && map->getTurnAngleRads() != 0 && d_voice->queueLength() < 2)
  {
    stringstream msg;
    int degrees = (int)Math::radToDeg(map->getTurnAngleRads());
    msg << "Turning " << abs(degrees) << " degree" << (abs(degrees) == 1 ? "" : "s")
        << (degrees < 0 ? " right" : degrees > 0 ? " left" : "")
        << " to kick " << map->getTurnForKick()->getId();
    d_voice->say(msg.str());
  }
}

void BuildStationaryMap::reset()
{
  d_ballEstimates.clear();
  d_goalEstimates.clear();
  d_teammateEstimates.clear();
  d_occlusionMap.reset();

  updateStateObject();
}

void BuildStationaryMap::integrate(StationaryMapState::PositionEstimates& estimates, geometry2::Point2d pos, double mergeDistance)
{
  for (auto& estimate : estimates)
  {

    double dist = (estimate.getAverage()  - pos).norm();
    if (dist <= mergeDistance)
    {
      // Merge into existing estimate and return
      estimate.add(pos);
      return;
    }
  }

  // No estimate matched, so create a new one
  estimates.emplace_back();
  estimates[estimates.size() - 1].add(pos);
}
