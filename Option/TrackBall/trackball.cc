// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "trackball.hh"

#include "config/Config/config.hh"
#include "Drawing/drawing.hh"
#include "FieldMap/fieldmap.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"

using namespace bold;
using namespace bold::colour;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace rapidjson;
using namespace std;

TrackBall::TrackBall(string id,
                     shared_ptr<Draw> draw)
  : Option{id, "TrackBall"},
    d_draw{move(draw)},
    d_slowAverage{uint16_t(Config::getStaticValue<int>("options.track-ball.slow-window-size"))},
    d_fastAverage{uint16_t(Config::getStaticValue<int>("options.track-ball.fast-window-size"))}
{}

void TrackBall::reset()
{
  d_slowAverage.reset();
  d_fastAverage.reset();
}

vector<shared_ptr<Option>> TrackBall::runPolicy(Writer<StringBuffer>& writer)
{
  auto agentFrame = State::get<AgentFrameState>();

  ASSERT(agentFrame);

  auto ball = agentFrame->getBallObservation();

  if (!ball.hasValue())
  {
    reset();
    return {};
  }

  // Integrate the ball observation
  auto slow = d_slowAverage.next(ball->toDim<2>());
  auto fast = d_fastAverage.next(ball->toDim<2>());

  if (!d_slowAverage.isMature() || !d_fastAverage.isMature())
    return {};

  d_draw->circle(Frame::Agent, slow, FieldMap::getBallRadius() * 0.5, BGR::orange(), 0.4, 1);
  d_draw->circle(Frame::Agent, fast, FieldMap::getBallRadius(), BGR::orange(), 0.8, 2);
  d_draw->line  (Frame::Agent, LineSegment2d{slow, fast}, BGR::orange(), 0.8, 2);

  return {};
}
