// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Option/option.hh"
#include "stats/movingaverage.hh"

#include <Eigen/Core>

namespace bold
{
  class Draw;
  
  class TrackBall : public Option
  {
  public:
    TrackBall(std::string id,
              std::shared_ptr<Draw> draw);

    void reset() override;

    std::vector<std::shared_ptr<Option>> runPolicy(rapidjson::Writer<rapidjson::StringBuffer>& writer) override;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  private:
    std::shared_ptr<Draw> d_draw;
    
    MovingAverage<geometry2::Point2d> d_slowAverage;
    MovingAverage<geometry2::Point2d> d_fastAverage;
  };
}
