// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <opencv/cv.h>

#include "vision/BlobPipeline/blobpipeline.hh"
#include "vision/BlobMerger/blobmerger.hh"
#include "vision/FieldEdgeBlobFilter/fieldedgeblobfilter.hh"
#include "state/State/state.hh"
#include "state/StateObject/FieldEdgeState/fieldedgestate.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"

using namespace std;
using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::vision;

using BRun = bold::vision::Run;

TEST (BlobPipelineTests, emptyPipeline)
{
  auto pipeline = BlobPipeline{};
  auto img = cv::Mat(640, 480, CV_8UC3);
  
  auto result = pipeline.run({}, img);
  EXPECT_EQ( 0, result.size() );

  auto blob = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};
  result = pipeline.run({blob}, img);
  EXPECT_EQ( 1, result.size() );
  EXPECT_EQ( blob, result[0] );
}

TEST(BlobPipelineTests, filter)
{
  auto pipeline = BlobPipeline{};
  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto acceptAll = [](Blob const& b) { return true; };
  auto rejectAll = [](Blob const& b) { return false; };

  auto acceptAllPipeline = pipeline.filter(acceptAll);
  auto rejectAllPipeline = pipeline.filter(rejectAll);

  EXPECT_EQ( 1, acceptAllPipeline.run({blob}, img).size() );
  EXPECT_EQ( 0, rejectAllPipeline.run({blob}, img).size() );
}

TEST(BlobPipelineTests, map)
{
  auto pipeline = BlobPipeline{};
  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto trim = [](Blob const& blob) {
    auto newRunset = set<BRun>{};
    for (auto const& run : blob.runs())
      newRunset.emplace(run.startX() + 1,
                        run.endX() - 1,
                        run.y());
    return Blob{newRunset};
  };

  auto trimPipeline = pipeline.map(trim);
  auto expectedBlob = Blob{{
      BRun{1, 9, 0},
      BRun{1, 9, 1}
    }};
  EXPECT_EQ( expectedBlob, trimPipeline.run({blob}, img)[0] );
}

TEST(BlobPipelineTests, flatMap)
{
  auto pipeline = BlobPipeline{};
  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto split = [](Blob const& blob) {
    auto newRunset1 = set<BRun>{};
    auto newRunset2 = set<BRun>{};
    auto iter = blob.runs().begin();
    for (unsigned i = 0; i < blob.runs().size() / 2; ++i)
      newRunset1.insert(*iter++);
    for (unsigned i = blob.runs().size() / 2; i < blob.runs().size(); ++i)
      newRunset2.insert(*iter++);
    return Blob::Vector{{Blob{newRunset1}, Blob{newRunset2}}};
  };

  auto splitPipeline = pipeline.flatMap(split);
  auto result = splitPipeline.run({blob}, img);
  
  EXPECT_EQ( 2, result.size() );
}

TEST(BlobPipelineTests, fold)
{
  auto pipeline = BlobPipeline{};
  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob1 = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto blob2 = Blob{{
      BRun{11, 20, 0},
      BRun{11, 20, 1}
    }};

  auto blob3 = Blob{{
      BRun{30, 40, 0},
      BRun{30, 40, 1}
    }};

  auto merge = [](Blob::Vector const& mergeTargets, Blob const& blob)
  {
    auto newMerged = Blob::Vector{};
    auto newBlob = blob;
    for (auto const& target : mergeTargets)
    {
      auto dist = newBlob.bounds().exteriorDistance(target.bounds());
      if (dist <= 1)
        newBlob.merge(target);
      else
        newMerged.push_back(target);
    }
    newMerged.push_back(newBlob);
    return newMerged;
  };

  auto foldPipeline = pipeline.foldLeft(merge);

  auto result = foldPipeline.run({blob1, blob2, blob3}, img);

  EXPECT_EQ( 2, result.size() );

  auto expectedBlob1 = Blob{{
      BRun{0, 10, 0},
      BRun{11, 20, 0},
      BRun{0, 10, 1},
      BRun{11, 20, 1}
    }};

  auto expectedBlob2 = Blob{{
      BRun{30, 40, 0},
      BRun{30, 40, 1}
    }};
  
  EXPECT_EQ(expectedBlob1, result[0]);
  EXPECT_EQ(expectedBlob2, result[1]);
}

TEST(BlobPipelineTests, BlobMerger_nopreds)
{
  auto merger = BlobMerger{};
  auto mergePipeline = BlobPipeline{}.foldLeft(merger);

  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob1 = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto blob2 = Blob{{
      BRun{11, 20, 0},
      BRun{11, 20, 1}
    }};

  auto blob3 = Blob{{
      BRun{30, 40, 0},
      BRun{30, 40, 1}
    }};

  auto result = mergePipeline.run({blob1, blob2, blob3}, img);

  EXPECT_EQ( 3, result.size() );
  EXPECT_EQ( blob1, result[0] );
  EXPECT_EQ( blob2, result[1] );
  EXPECT_EQ( blob3, result[2] );
}


TEST(BlobPipelineTests, BlobMerger_preds)
{
  auto merger = BlobMerger{};
  merger.addPredicate([](Blob const& a, Blob const& b) {
      auto dist = a.bounds().exteriorDistance(b.bounds());
      return dist <= 1;
    });

  auto mergePipeline = BlobPipeline{}.foldLeft(merger);

  auto img = cv::Mat(640, 480, CV_8UC3);

  auto blob1 = Blob{{
      BRun{0, 10, 0},
      BRun{0, 10, 1}
    }};

  auto blob2 = Blob{{
      BRun{11, 20, 0},
      BRun{11, 20, 1}
    }};

  auto blob3 = Blob{{
      BRun{30, 40, 0},
      BRun{30, 40, 1}
    }};

  auto result = mergePipeline.run({blob1, blob2, blob3}, img);
  
  EXPECT_EQ( 2, result.size() );

  auto expectedBlob1 = Blob{{
      BRun{0, 10, 0},
      BRun{11, 20, 0},
      BRun{0, 10, 1},
      BRun{11, 20, 1}
    }};

  auto expectedBlob2 = Blob{{
      BRun{30, 40, 0},
      BRun{30, 40, 1}
    }};
  
  EXPECT_EQ(expectedBlob1, result[0]);
  EXPECT_EQ(expectedBlob2, result[1]);
}

class FlatFieldEdge : public FieldEdge
{
public:
  uint16_t getYAt(uint16_t x) const override { return 240; }
};

TEST(BlobPipelineTests, FieldEdgeBlobFilter)
{
  auto img = cv::Mat(640, 480, CV_8UC3);

  auto edge = make_unique<FlatFieldEdge>();

  State::make<FieldEdgeState>(move(edge));

  auto maxBallFieldEdgeDistPixels = IntSetting{"maxBallFieldEdgeDistPixels",
                                               0, 480, false,
                                               "maxBallFieldEdgeDistPixels"};
  maxBallFieldEdgeDistPixels.setValue(0);
  
  auto edgePipeline = BlobPipeline{}.filter(FieldEdgeBlobFilter{&maxBallFieldEdgeDistPixels});

  // Single row blobs
  auto blobs = Blob::Vector{};
  for (uint16_t y = 64; y < 480; y += 64)
    blobs.push_back(Blob{{BRun{300, 340, y}}});

  auto result = edgePipeline.run(blobs, img);

  EXPECT_EQ(3, result.size());

  // On edge is fine
  EXPECT_EQ(1, edgePipeline.run({Blob{{BRun{300, 340, 240}}}}, img).size());

  // Over edge not
  EXPECT_EQ(0, edgePipeline.run({Blob{{BRun{300, 340, 241}}}}, img).size());

  // As long as some part is in it's fine
  EXPECT_EQ(1, edgePipeline.run({Blob{{
            BRun{300, 340, 240},
            BRun{300, 340, 241}
          }}}, img).size());

  // Allow being outside of the fieldedge by 10 pixels
  maxBallFieldEdgeDistPixels.setValue(10);
  EXPECT_EQ( 10, maxBallFieldEdgeDistPixels.getValue() );
  
  EXPECT_EQ(1, edgePipeline.run({Blob{{
            BRun{300, 340, 240},
            BRun{300, 340, 241}
          }}}, img).size());
  
  EXPECT_EQ(1, edgePipeline.run({Blob{{
            BRun{300, 340, 250}
          }}}, img).size());

  EXPECT_EQ(0, edgePipeline.run({Blob{{
            BRun{300, 340, 251}
          }}}, img).size());
}

TEST(BlobPipelineTests, FieldEdgeBlobFilter_noEdge)
{
  // If there is no field edge, all blobs pass
  // TODO: or should they all fail?
  State::set<FieldEdgeState>(nullptr);

  auto img = cv::Mat(640, 480, CV_8UC3);

  auto maxBallFieldEdgeDistPixels = IntSetting{"maxBallFieldEdgeDistPixels",
                                               0, 480, false,
                                               "maxBallFieldEdgeDistPixels"};
  maxBallFieldEdgeDistPixels.setValue(0);
  
  auto edgePipeline = BlobPipeline{}.filter(FieldEdgeBlobFilter{&maxBallFieldEdgeDistPixels});

  auto blobs = Blob::Vector{};
  for (uint16_t y = 64; y < 480; y += 64)
    blobs.push_back(Blob{{BRun{300, 340, y}}});

  auto result = edgePipeline.run(blobs, img);

  EXPECT_EQ(blobs.size(), result.size());
}
