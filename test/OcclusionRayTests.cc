// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "helpers.hh"

#include "OcclusionRay/occlusionray.hh"
#include "src/geometry2/Point/point.hh"

using namespace bold;
using namespace Eigen;
using namespace std;

TEST(OcclusionRayTests, construct)
{
  auto p1 = geometry2::Point2d{0, 0};
  auto p2 = geometry2::Point2d{1, 1};

  auto ray1 = OcclusionRay<double>(p1, p2);

  EXPECT_TRUE(PointsEqual(p1, ray1.near()));
  EXPECT_TRUE(PointsEqual(p2, ray1.far()));

  auto p3 = geometry2::Point3d{0, 0, 0};
  auto p4 = geometry2::Point3d{1, 1, 0};

  auto ray2 = OcclusionRay<double>(p3.toDim<2>(), p4.toDim<2>());

  EXPECT_TRUE(PointsEqual(p3.toDim<2>(), ray2.near()));
  EXPECT_TRUE(PointsEqual(p4.toDim<2>(), ray2.far()));

  // emplace_back does not use OcclusionRay::new, so will fail to assert correctly
  /* TODO: doesn't die reliably in test
  vector<OcclusionRay<double>> occlusionRays1;
  EXPECT_DEATH(
    { occlusionRays1.emplace_back(p1, p2) }
    , "unaligned_array_assert");
  */

  vector<OcclusionRay<double>, aligned_allocator<OcclusionRay<double>>> occlusionRays2;
  occlusionRays2.emplace_back(p1, p2);
  EXPECT_TRUE(PointsEqual(p1, occlusionRays2[0].near()));
  EXPECT_TRUE(PointsEqual(p2, occlusionRays2[0].far()));
}
