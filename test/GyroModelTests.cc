// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "GyroModel/gyromodel.hh"

using namespace bold;

TEST (GyroModelTests, valueToDPS)
{
  // Models according to raw Gyro readings
  // http://www.st.com/web/en/resource/technical/document/datasheet/CD00265057.pdf
  auto model1 = GyroModel(70, 2000, 1 << 16);
  EXPECT_EQ(0, model1.valueToDPS(1 << 15));

  EXPECT_EQ(-2000, model1.valueToDPS(0));
  EXPECT_EQ(2000, model1.valueToDPS((1 << 16) - 1));

  EXPECT_EQ(-700, model1.valueToDPS((1 << 15) - 10000));
  EXPECT_EQ(700, model1.valueToDPS((1 << 15) + 10000));

  auto model2 = GyroModel(17.50, 500, 1 << 16);
  EXPECT_EQ(0, model2.valueToDPS(1 << 15));

  EXPECT_EQ(-500, model2.valueToDPS(0));
  EXPECT_EQ(500, model2.valueToDPS((1 << 16) - 1));

  EXPECT_EQ(-175, model2.valueToDPS((1 << 15) - 10000));
  EXPECT_EQ(175, model2.valueToDPS((1 << 15) + 10000));

  auto model3 = GyroModel(8.75, 250, 1 << 16);
  EXPECT_EQ(0, model3.valueToDPS(1 << 15));

  EXPECT_EQ(-250, model3.valueToDPS(0));
  EXPECT_EQ(250, model3.valueToDPS((1 << 16) - 1));

  EXPECT_EQ(-87.5, model3.valueToDPS((1 << 15) - 10000));
  EXPECT_EQ(87.5, model3.valueToDPS((1 << 15) + 10000));

  // Models based on Robotis CM730 firmware
  // Values are scaled from 16 to 10 bits, and multiplied by 5/4
  auto mdpsPerDigit4 = 70.0 * 64 * 4 / 5;
  ASSERT_EQ(3584.0, mdpsPerDigit4);

  auto model4 = GyroModel(mdpsPerDigit4, 2000 * 4 / 5, 1 << 10);
  EXPECT_EQ(0, model4.valueToDPS(1 << 9));

  EXPECT_EQ(-1600, model4.valueToDPS(0));
  EXPECT_EQ(1600, model4.valueToDPS((1 << 10) - 1));

  EXPECT_EQ(-mdpsPerDigit4 * 100 / 1000, model4.valueToDPS((1 << 9) - 100));
  EXPECT_EQ(mdpsPerDigit4 * 100 / 1000, model4.valueToDPS((1 << 9) + 100));  
}
