// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <opencv2/core/core.hpp>
#include <vector>

#include "roundtable/Action/action.hh"

#include "vision/LabelTeacher/labelteacher.hh"
#include "vision/PixelLabel/HistogramPixelLabel/histogrampixellabel.hh"

using namespace std;
using namespace bold;
using namespace bold::vision;

TEST (LabelTeacherTests, init)
{
  roundtable::Action::clearActions();
  
  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  auto _labels = teacher.getLabels();

  EXPECT_EQ(2, _labels.size());
}

TEST (LabelTeacherTests, setYUVTrainImage)
{
  roundtable::Action::clearActions();

  cv::Mat trainImage{640, 480, CV_8UC3};

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  teacher.setYUVTrainImage(trainImage);
}

TEST (LabelTeacherTests, setSeedPoint)
{
  roundtable::Action::clearActions();

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  auto point = Eigen::Vector2i{320, 240};

  teacher.setSeedPoint(point);
}

TEST (LabelTeacherTests, floodFillEmpty)
{
  roundtable::Action::clearActions();

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  cv::Mat trainImage = cv::Mat::zeros(640, 480, CV_8UC3);
  teacher.setYUVTrainImage(trainImage);
  teacher.setSeedPoint(Eigen::Vector2i{0, 0});

  auto mask = teacher.floodFill();

  EXPECT_EQ ( trainImage.rows, mask.rows );
  EXPECT_EQ ( trainImage.cols, mask.cols );

  for (int i = 0; i < mask.rows; ++i)
    for (int j = 0; j < mask.cols; ++j)
      EXPECT_EQ ( 255, mask.at<uint8_t>(i, j) ) << i << " " << j;
}

TEST (LabelTeacherTests, floodFillColor)
{
  roundtable::Action::clearActions();

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  cv::Mat trainImage = cv::Mat::zeros(640, 480, CV_8UC3);
  cv::rectangle(trainImage, cv::Rect(0, 0, 100, 100), cv::Scalar(255, 0, 0), CV_FILLED);
  teacher.setYUVTrainImage(trainImage);
  teacher.setSeedPoint(Eigen::Vector2i{0, 0});

  auto mask = teacher.floodFill();
  for (int i = 0; i < mask.rows; ++i)
    for (int j = 0; j < mask.cols; ++j)
      if (i < 100 && j < 100)
        EXPECT_EQ ( 255, mask.at<uint8_t>(i, j) ) << i << " " << j;
      else
        EXPECT_EQ ( 0, mask.at<uint8_t>(i, j) ) << i << " " << j;
}

TEST (LabelTeacherTests, floodFillNoColor)
{
  roundtable::Action::clearActions();

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
    make_shared<HistogramPixelLabel<6>>("two", LabelClass::FIELD)
  };
  LabelTeacher teacher{labels};

  cv::Mat trainImage = cv::Mat::zeros(640, 480, CV_8UC3);
  cv::rectangle(trainImage, cv::Rect(0, 0, 100, 100), cv::Scalar(255, 0, 0), CV_FILLED);
  teacher.setYUVTrainImage(trainImage);
  teacher.setSeedPoint(Eigen::Vector2i{320, 240});

  auto mask = teacher.floodFill();
  for (int i = 0; i < mask.rows; ++i)
    for (int j = 0; j < mask.cols; ++j)
      if (i < 100 && j < 100)
        EXPECT_EQ ( 0, mask.at<uint8_t>(i, j) ) << i << " " << j;
      else
        EXPECT_EQ ( 255, mask.at<uint8_t>(i, j) ) << i << " " << j;
}

TEST (LabelTeacherTests, trainWithoutImage)
{
  roundtable::Action::clearActions();

  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
  };
  LabelTeacher teacher{labels};
  auto mask = cv::Mat::zeros(640, 480, CV_8UC1);
  teacher.train(0, mask);
}

/*
TEST (LabelTeacherTests, DISABLED_train)
{
  auto labels = vector<shared_ptr<PixelLabel>>{
    make_shared<HistogramPixelLabel<6>>("one", LabelClass::BALL),
  };
  LabelTeacher teacher{labels};

  cv::Mat trainImage = cv::Mat::zeros(640, 480, CV_8UC3);
  cv::rectangle(trainImage, cv::Rect(0, 0, 100, 100), cv::Scalar(128, 0, 0), CV_FILLED);
  teacher.setYUVTrainImage(trainImage);
  teacher.setSeedPoint(Eigen::Vector2i{0, 0});

  auto mask = teacher.floodFill();

  teacher.train(0, mask);

  auto _labels = teacher.getLabels();

  auto labelOne = _labels[0];

  EXPECT_EQ ( 100 * 100, labelOne.getTotalCount() );

  EXPECT_EQ ( colour::HSV(128, 0, 0), labelOne.modalColour() );

  EXPECT_EQ ( 1.0f, labelOne.labelProb(colour::HSV(128, 0, 0)) );
  EXPECT_EQ ( 0.0f, labelOne.labelProb(colour::HSV(0, 0, 0)) );
  EXPECT_EQ ( 0.0f, labelOne.labelProb(colour::HSV(255, 0, 0)) );

}
*/
