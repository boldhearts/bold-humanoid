// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "helpers.hh"
#include "Spatialiser/spatialiser.hh"
#include "LineJunctionFinder/linejunctionfinder.hh"

#include "vision/CameraModel/cameramodel.hh"

using namespace bold;
using namespace bold::geometry2;
using namespace bold::util;
using namespace bold::vision;
using namespace std;
using namespace Eigen;

shared_ptr<Spatialiser> createTestSpatialiser(unsigned imageWidth = 11, unsigned imageHeight = 11, double rangeVertical = 90, double rangeHorizontal = 90)
{
  shared_ptr<CameraModel> cameraModel = allocate_aligned_shared<CameraModel>(imageWidth, imageHeight, rangeVertical, rangeHorizontal);
  return make_shared<Spatialiser>(cameraModel);
}

TEST (LineJunctionFinder, findJunctions)
{
  auto spatialiser = createTestSpatialiser();

  LineJunctionFinder finder{};

  // X junctions
  auto segment1 = LineSegment3d{Point3d{-1.0, 0.0, 0.0}, Point3d{1.0, 0.0, 0.0}};
  auto segment2 = LineSegment3d{Point3d{0.0, -1.0, 0.0}, Point3d{0.0, 1.0, 0.0}};

  auto junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::X );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(0.0, 0.0) ) );


  segment1 = LineSegment3d{Point3d{-1.0, -1.0, 0.0}, Point3d{1.0,  1.0, 0.0}};
  segment2 = LineSegment3d{Point3d{-1.0,  1.0, 0.0}, Point3d{1.0, -1.0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::X );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(0.0, 0.0) ) );


  // T junctions
  segment1 = LineSegment3d{Point3d{-1.0, 0.0, 0.0}, Point3d{1.0,  0.0, 0.0}};
  segment2 = LineSegment3d{Point3d{0.0,  0.0, 0.0}, Point3d{0.0,  1.0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::T );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(0.0, 0.0) ) );


  segment1 = LineSegment3d{Point3d{-1.0, -1.0, 0.0}, Point3d{1.0,  1.0, 0.0}};
  segment2 = LineSegment3d{Point3d{0.0,  0.0, 0.0}, Point3d{1.0,  -1.0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::T );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(0.0, 0.0) ) );


  // L junctions
  segment1 = LineSegment3d{Point3d{0.0, 0.0, 0.0}, Point3d{0.0,  1.0, 0.0}};
  segment2 = LineSegment3d{Point3d{0.0,  0.0, 0.0}, Point3d{1.0,  0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::L );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(0.0, 0.0) ) );


  segment1 = LineSegment3d{Point3d{0.0, 0.0, 0.0}, Point3d{1.0,  1.0, 0.0}};
  segment2 = LineSegment3d{Point3d{2.0,  0.0, 0.0}, Point3d{1.0, 1.0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( junction.hasValue() );

  EXPECT_EQ( junction->type, LineJunction::Type::L );
  EXPECT_TRUE( PointsEqual( junction->position, Point2d(1.0, 1.0) ) );


  // No junction
  segment1 = LineSegment3d{Point3d{0.0, 0.0, 0.0}, Point3d{1.0,  0.0, 0.0}};
  segment2 = LineSegment3d{Point3d{0.5,  0.21, 0.0}, Point3d{0.5, 1.0, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2, 0.2);

  EXPECT_TRUE ( !junction.hasValue() );


  segment1 = LineSegment3d{Point3d{0.0, 0.0, 0.0}, Point3d{0.5,  0.5, 0.0}};
  segment2 = LineSegment3d{Point3d{2.0,  0.0, 0.0}, Point3d{1.5, 0.5, 0.0}};

  junction = finder.tryFindLineJunction(segment1, segment2);

  EXPECT_TRUE ( !junction.hasValue() );
}
