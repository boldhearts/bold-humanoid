// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "helpers.hh"

#include "src/geometry2/Point/point.hh"
#include "src/geometry2/Geometry/LineSegment/linesegment.hh"
#include "src/geometry2/Geometry/MultiPoint/Polygon/polygon.hh"
#include "src/geometry2/Geometry/MultiPoint/LineString/linestring.hh"

#include "src/geometry2/Operator/ConvexHull/grahamconvexhull.hh"
#include "src/geometry2/Operator/ConvexHull/andrewconvexhull.hh"
#include "src/geometry2/Operator/Intersection/intersection.hh"
#include "src/geometry2/Operator/Predicate/Contains/contains.hh"
#include "src/geometry2/Operator/Predicate/IsCCW/isccw.hh"

#include "src/util/Maybe/maybe.hh"
#include "Math/math.hh"

#include <Eigen/Geometry>

using namespace bold::geometry2;
using namespace bold::util;
using namespace bold;
using namespace Eigen;


TEST(GeometryTests, Point)
{
  auto point1 = Point2i{0, 1};
  EXPECT_EQ(0, point1.x());
  EXPECT_EQ(1, point1.y());

  auto point2 = Point3d{0, 1, 2};
  EXPECT_EQ(0.0, point2.x());
  EXPECT_EQ(1.0, point2.y());
  EXPECT_EQ(2.0, point2.z());

  auto maybePoint1 = make_maybe(point1);
  auto maybePoint2 = make_maybe(point1);

  EXPECT_EQ(maybePoint1, maybePoint2);
}

TEST(GeometryTests, PointVectorOperations)
{
  auto point1 = Point2i{0, 0};

  EXPECT_EQ(Point2i::ORIGIN, point1);
  EXPECT_EQ(Point2i(1, 2), Point2i::ORIGIN + Vector2i(1,2));
  EXPECT_EQ(Vector2i(1, 2), Point2i(1, 2) - Point2i::ORIGIN);
  
  auto point2 = Point2i{1, 1};
  EXPECT_EQ(Eigen::Vector2i(1, 1), point2 - point1);
  EXPECT_EQ(Point2i(2, 3), point2 + Vector2i(1, 2));

  point1 - Vector2i(1, 1);
}

TEST(GeometryTests, PointTransformations)
{
  auto point1 = Point2d{1.0, 1.0};
  auto rotate90deg = Eigen::Rotation2Dd(.5 * M_PI);

  auto point2 = Point2d::ORIGIN + rotate90deg * (point1 - Point2d::ORIGIN);
  EXPECT_TRUE(PointsEqual(Point2d(-1.0, 1.0), point2));
  auto point3 = Point2d::ORIGIN + rotate90deg * (point2 - Point2d::ORIGIN);
  EXPECT_TRUE(PointsEqual(Point2d(-1.0, -1.0), point3));
  auto point4 = Point2d::ORIGIN + rotate90deg * (point3 - Point2d::ORIGIN);
  EXPECT_TRUE(PointsEqual(Point2d(1.0, -1.0), point4));
}

TEST(GeometryTests, LineSegment)
{
  auto segment1 = LineSegment2d{Point2d{1, 1}, Point2d{2, 2}};

  EXPECT_TRUE(PointsEqual(Point2d{1, 1}, segment1.p1()));
  EXPECT_TRUE(PointsEqual(Point2d{2, 2}, segment1.p2()));

  EXPECT_NEAR(1.414213562373095, segment1.length(), 1e-6);

  auto expectedBounds = AlignedBox2d(Vector2d(1, 1), Vector2d(2, 2));
  EXPECT_TRUE(expectedBounds.isApprox(segment1.getBounds()));

  auto p1 = Point3d{1, 1, 1};
  auto p2 = Point3d{2, 2, 2};
  
  auto segment2 = LineSegment<double, 3>{p1, p2};
}

TEST(GeometryTests, LineSegment_center)
{
  EXPECT_TRUE(PointsEqual(Point2i(50, 0), LineSegment2i(Point2i(0, 0), Point2i(100, 0)).center()));
  EXPECT_TRUE(PointsEqual(Point2i(5, 5), LineSegment2i(Point2i(0, 0), Point2i(10, 10)).center()));
  EXPECT_TRUE(PointsEqual(Point2i(5, -5), LineSegment2i(Point2i(0, 0), Point2i(10, -10)).center()));
  EXPECT_TRUE(PointsEqual(Point2i(2, 1), LineSegment2i(Point2i(1, 0), Point2i(3, 2)).center()));
  EXPECT_TRUE(PointsEqual(Point2i(2, 1), LineSegment2i(Point2i(1, 0), Point2i(4, 3)).center()));
}

TEST(GeometryTests, LineSegment_delta)
{
  EXPECT_EQ(Vector2i(50, 100), LineSegment2i(Point2i(50, 0), Point2i(100, 100)).delta());
  EXPECT_EQ(Vector2i(-10, -20), LineSegment2i(Point2i(40, 30), Point2i(30, 10)).delta());
  EXPECT_EQ(Vector2i(2, 1), LineSegment2i(Point2i(1, 0), Point2i(3, 1)).delta());
}

TEST(GeometryTests, LineSegment_slope)
{
  EXPECT_EQ (0, LineSegment2i(Point2i(0,0), Point2i(100,0)).slope());
  EXPECT_EQ (1, LineSegment2i(Point2i(0,0), Point2i(10,10)).slope());
  EXPECT_EQ (-1, LineSegment2i(Point2i(0,0), Point2i(10,-10)).slope());
  EXPECT_EQ (0.5, LineSegment2i(Point2i(1,0), Point2i(3,1)).slope());

  // Vertical lines have infinite gradient
  EXPECT_EQ (std::numeric_limits<double>::infinity(),
             LineSegment2i(Point2i(0,0), Point2i(0,10)).slope());
}

TEST(GeometryTests, LineSegment_angle)
{
  EXPECT_EQ(0, LineSegment2d(Point2d(0, 0), Point2d(1, 0)).angle());
  EXPECT_EQ(M_PI_4, LineSegment2d(Point2d(0, 0), Point2d(1, 1)).angle());
  EXPECT_EQ(M_PI_2, LineSegment2d(Point2d(1, 0), Point2d(1, 1)).angle());
  EXPECT_EQ(M_PI, LineSegment2d(Point2d(1, 1), Point2d(0, 1)).angle());
  EXPECT_EQ(atan(0.5), LineSegment2d(Point2d(1, 0), Point2d(3, 1)).angle());

  EXPECT_EQ(0, LineSegment3d(Point3d(0, 0, 0), Point3d(1, 0, 0)).angle());
  EXPECT_EQ(M_PI_4, LineSegment3d(Point3d(0, 0, 0), Point3d(1, 0, 1)).angle());
  EXPECT_EQ(M_PI_2, LineSegment3d(Point3d(1, 0, 0), Point3d(1, 0, 1)).angle());
  EXPECT_EQ(M_PI, LineSegment3d(Point3d(1, 0, 1), Point3d(0, 0, 1)).angle());
  EXPECT_EQ(atan(0.5), LineSegment3d(Point3d(1, 0, 0), Point3d(3, 0, 1)).angle());
}

TEST(GeometryTests, LineSegment_heightAt)
{
  auto segment = LineSegment2i(Point2i(0, 10), Point2i(100, 60));

  EXPECT_EQ(10, segment.heightAt(Point<int, 1>(0)));
  EXPECT_EQ(60, segment.heightAt(Point<int, 1>(100)));

  EXPECT_EQ(35, segment.heightAt(Point<int, 1>(50)));

  EXPECT_EQ(10, segment.heightAt(Point<int, 1>(1)));
  EXPECT_EQ(11, segment.heightAt(Point<int, 1>(2)));
  EXPECT_EQ(59, segment.heightAt(Point<int, 1>(98)));
  EXPECT_EQ(59, segment.heightAt(Point<int, 1>(99)));
}

TEST(GeometryTests, LineSegment_isPointAbove_horizontal)
{
  auto segment1 = LineSegment2i(Point2i(0, 10), Point2i(100, 10));

  EXPECT_FALSE(segment1.isPointAbove(Point2i(0, 0)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(100, 0)));

  EXPECT_TRUE(segment1.isPointAbove(Point2i(0, 20)));
  EXPECT_TRUE(segment1.isPointAbove(Point2i(50, 20)));
  EXPECT_TRUE(segment1.isPointAbove(Point2i(100, 20)));

  EXPECT_FALSE(segment1.isPointAbove(Point2i(0, 10)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(50, 10)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(100, 10)));
}

TEST(GeometryTests, LineSegment_isPointAbove_diagonal)
{
  auto segment1 = LineSegment2i(Point2i(0, 0), Point2i(100, 100));

  EXPECT_FALSE(segment1.isPointAbove(Point2i(0, 0)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(100, 100)));

  EXPECT_TRUE(segment1.isPointAbove(Point2i(0, 20)));
  EXPECT_TRUE(segment1.isPointAbove(Point2i(50, 70)));
  EXPECT_TRUE(segment1.isPointAbove(Point2i(99, 100)));

  EXPECT_FALSE(segment1.isPointAbove(Point2i(10, 10)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(50, 50)));
  EXPECT_FALSE(segment1.isPointAbove(Point2i(99, 99)));
  
  auto segment2 = LineSegment2i(Point2i(0, 100), Point2i(100, 0));

  EXPECT_FALSE(segment2.isPointAbove(Point2i(0, 100)));
  EXPECT_FALSE(segment2.isPointAbove(Point2i(100, 0)));

  EXPECT_TRUE(segment2.isPointAbove(Point2i(100, 20)));
  EXPECT_TRUE(segment2.isPointAbove(Point2i(50, 70)));
  EXPECT_TRUE(segment2.isPointAbove(Point2i(1, 100)));

  EXPECT_FALSE(segment2.isPointAbove(Point2i(90, 10)));
  EXPECT_FALSE(segment2.isPointAbove(Point2i(50, 50)));
  EXPECT_FALSE(segment2.isPointAbove(Point2i(1, 99)));
}

TEST(GeometryTests, LineSegment_isPointAbove_horizon)
{
  EXPECT_FALSE( LineSegment2i(Point2i(0, 404), Point2i(639, 411)).isPointAbove(Point2i(639, 406)) );
  
  EXPECT_TRUE( LineSegment2i(Point2i(0, 306), Point2i(639, 587)).isPointAbove(Point2i(0, 441)) );
  EXPECT_FALSE( LineSegment2i(Point2i(0, 306), Point2i(639, 587)).isPointAbove(Point2i(639, 440)) );
}

TEST(GeometryTests, LineSegment_angleBetween)
{
  EXPECT_NEAR(M_PI_2,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0), Point2d(1, 0))),
              0.000001);

  EXPECT_NEAR(M_PI_2,
               LineSegment2d(Point2d(0, 0), Point2d(0, 2)).angleBetween(LineSegment2d(Point2d(2, 2), Point2d(4, 2))),
               0.000001);

  EXPECT_NEAR(0,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0),Point2d(0, -1))),
              0.000001);

  EXPECT_NEAR(M_PI_2,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0), Point2d(1, 0))),
              0.000001);

  EXPECT_NEAR(M_PI_4,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0),Point2d(1, 1))),
              0.000001);
  
  EXPECT_NEAR(M_PI_4,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0), Point2d(-1, 1))),
              0.000001);
  
  EXPECT_NEAR(M_PI_4,
              LineSegment2d(Point2d(0, 0),Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0),Point2d(-1, -1))),
              0.000001);
  
  EXPECT_NEAR(M_PI/4,
              LineSegment2d(Point2d(0, 0), Point2d(0, 1)).angleBetween(LineSegment2d(Point2d(0, 0),Point2d(1, -1))),
              0.000001);
}

TEST(GeometryTests, LineSegment_add)
{
  EXPECT_TRUE(LinesEqual(LineSegment2d(Point2d(1, 1), Point2d(2, 2)),
                         LineSegment2d(Point2d(0, 0), Point2d(1, 1)) + Vector2d(1, 1)));

  EXPECT_TRUE(LinesEqual(LineSegment2d(Point2d(-1, -1), Point2d(0, 0)),
                       LineSegment2d(Point2d(0, 0), Point2d(1, 1)) - Vector2d(1, 1)));
}

TEST(GeometryTests, LineSegment_cast)
{
  EXPECT_TRUE(LinesEqual(LineSegment2i(Point2i(1, 1), Point2i(2, 2)),
                         LineSegment2d(Point2d(1, 1), Point2d(2, 2)).cast<int>()));
  EXPECT_TRUE(LinesEqual(LineSegment2i(Point2i(1, 1), Point2i(2, 2)),
                         LineSegment2d(Point2d(1.1, 1.5), Point2d(2.4, 2.9)).cast<int>()));
}

TEST(GometryTests, LineSegment_LineSegment_intersection)
{
  // Perpendicular
  EXPECT_EQ(Maybe<Point2i>(Point2i(1, 1)),
            intersection(LineSegment2i(Point2i(1, 0), Point2i(1, 2)),
                         LineSegment2i(Point2i(0, 1), Point2i(2, 1))));

  // Perpendicularg
  EXPECT_EQ(Maybe<Point2i>(Point2i(5, 5)),
            intersection(LineSegment2i(Point2i(0, 10), Point2i(10, 0)),
                         LineSegment2i(Point2i(0, 0), Point2i(10, 10))));

  // Touching (V shape)
  EXPECT_EQ(Maybe<Point2i>(Point2i(0, 0)),
            intersection(LineSegment2i(Point2i(-1, 1), Point2i(0, 0)),
                         LineSegment2i(Point2i(0, 0), Point2i(1, 1))));

  // Touching (T shape)
  EXPECT_EQ(Maybe<Point2i>(Point2i(0, 1)),
            intersection(LineSegment2i(Point2i(-1, 1), Point2i(1, 1)),
                         LineSegment2i(Point2i(0, 0), Point2i(0, 1))));
  EXPECT_EQ(Maybe<Point2i>(Point2i(1, 1)),
            intersection(LineSegment2i(Point2i(0, 1), Point2i(2, 1)),
                         LineSegment2i(Point2i(1, 1), Point2i(1, 2))));
  
  // Touching (L shape)
  EXPECT_EQ(Maybe<Point2i>(Point2i(-1, 1)),
            intersection(LineSegment2i(Point2i(-2, 1), Point2i(-1, 1)),
                         LineSegment2i(Point2i(-1, 1), Point2i(-1, 2))));
  EXPECT_EQ(Maybe<Point2i>(Point2i(-1, 1)),
            intersection(LineSegment2i(Point2i(-1, 1), Point2i(-2, 1)),
                         LineSegment2i(Point2i(-1, 2), Point2i(-1, 1))));
  EXPECT_EQ(Maybe<Point2i>(Point2i(-1, 1)),
            intersection(LineSegment2i(Point2i(-2, 1), Point2i(-1, 1)),
                         LineSegment2i(Point2i(-1, 2), Point2i(-1, 1))));
  EXPECT_EQ(Maybe<Point2i>(Point2i(1, 1)),
            intersection(LineSegment2i(Point2i(1, 1), Point2i(1, 2)),
                         LineSegment2i(Point2i(1, 1), Point2i(2, 1))));
  EXPECT_EQ(Maybe<Point2i>(Point2i(0, 0)),
            intersection(LineSegment2i(Point2i(0, 0), Point2i(0, 1)),
                         LineSegment2i(Point2i(0, 0), Point2i(1, 0))));
  
  // Parallel (vertical)g
  EXPECT_EQ(Maybe<Point2i>::empty(),
            intersection(LineSegment2i(Point2i(1, 1), Point2i(1, 2)),
                         LineSegment2i(Point2i(2, 0), Point2i(2, 1))));
  EXPECT_EQ(Maybe<Point2i>::empty(),
            intersection(LineSegment2i(Point2i(1, 1), Point2i(1, 2)),
                         LineSegment2i(Point2i(2, 0), Point2i(2, 1))));
  
  // Parallel (horizontal)
  EXPECT_EQ(Maybe<Point2i>::empty(),
            intersection(LineSegment2i(Point2i(0, 0), Point2i(1, 0)),
                         LineSegment2i(Point2i(0, 1), Point2i(1, 1))));
  
  // Colinear
  EXPECT_EQ(Maybe<Point2i>::empty(),
            intersection(LineSegment2i(Point2i(0, 0), Point2i(1, 0)),
                         LineSegment2i(Point2i(2, 0), Point2i(3, 0))));
  EXPECT_EQ(Maybe<Point2i>::empty(),
            intersection(LineSegment2i(Point2i(1, 1), Point2i(2, 2)),
                         LineSegment2i(Point2i(3, 3), Point2i(4, 4))));
  
  EXPECT_EQ( Maybe<Point2i>::empty(),
             intersection(LineSegment2i(Point2i(0, 0), Point2i(0, 10)),
                          LineSegment2i(Point2i(5, 5), Point2i(15, 5))) );
}

TEST(GeometryTests, DISABLED_colinear_intersecting_linesegments)
{
  // this case doesn't work -- is it important?
  EXPECT_EQ(Maybe<Point2i>(Point2i(2,2)),
            intersection(LineSegment2i(Point2i(1, 1), Point2i(2, 2)),
                         LineSegment2i(Point2i(2, 2), Point2i(3, 3))));  
}

TEST (GeometryTests, DISABLED_linesegment_yIntersection)
{
  // Should linesegment have this? Or should it have a toLine which
  // returns an Eigen::ParametrizedLine and do intersect with that?

  /*
  EXPECT_EQ (0,    LineSegment2i(Vector2i(0,0),   Vector2i(100,0)).yIntersection());
  EXPECT_EQ (10,   LineSegment2i(Vector2i(10,10), Vector2i(20,10)).yIntersection());
  EXPECT_EQ (-1,   LineSegment2i(Vector2i(1,0),   Vector2i(2,1)).yIntersection());
  EXPECT_EQ (-0.5, LineSegment2i(Vector2i(1,0),   Vector2i(3,1)).yIntersection());

  // Vertical lines have no yIntersection
  EXPECT_TRUE (std::isnan(LineSegment2i(Vector2i(10,0), Vector2i(10,10)).yIntersection()));
  */
}

TEST (GeometryTests, Polygon)
{
  auto points = Point2i::Vector{};

  points.emplace_back(0, 0);
  points.emplace_back(0, 1);
  points.emplace_back(1, 0);

  auto poly = Polygon<int, 2>(points);

  ASSERT_EQ( 3, poly.coords().size() );
  EXPECT_TRUE( PointsEqual(Point2i(0, 0), poly.coords()[0]) );
  EXPECT_TRUE( PointsEqual(Point2i(0, 1), poly.coords()[1]) );
  EXPECT_TRUE( PointsEqual(Point2i(1, 0), poly.coords()[2]) );
}

TEST (GeometryTests, Polygon_fromBox)
{
  auto box = AlignedBox2d{Vector2d{10, 10}, Vector2d{20, 20}};
  auto polygon = Polygon2d{box};

  EXPECT_TRUE(AlignedBoxesEqual(box, polygon.getBounds()));
  EXPECT_EQ(4, polygon.coords().size());
  EXPECT_EQ(Point2d(10, 10), polygon.coords()[0]);
  EXPECT_EQ(Point2d(20, 10), polygon.coords()[1]);
  EXPECT_EQ(Point2d(20, 20), polygon.coords()[2]);
  EXPECT_EQ(Point2d(10, 20), polygon.coords()[3]);
}

TEST (GeometryTests, Polygon_bounds)
{
  auto square = Polygon2i({
      Point2i{0, 0},
      Point2i{0, 2},
      Point2i{2, 2},
      Point2i{2, 0}
    });

  EXPECT_TRUE(Eigen::AlignedBox2i(Vector2i(0, 0), Vector2i(2, 2)).isApprox(square.getBounds()));

  auto diamond = Polygon2i({
      Point2i{0, 0},
      Point2i{-1, 1},
      Point2i{0, 2},
      Point2i{1, 1}
    });

  EXPECT_TRUE(Eigen::AlignedBox2i(Vector2i(-1, 0), Vector2i(1, 2)).isApprox(diamond.getBounds()));
}

TEST (GeometryTests, Polygon_center)
{
  auto square = Polygon2i({
      Point2i{0, 0},
      Point2i{0, 2},
      Point2i{2, 2},
      Point2i{2, 0}
    });

  EXPECT_TRUE(PointsEqual(Point2i(1, 1), square.center()));

  auto diamond = Polygon2i({
      Point2i{0, 0},
      Point2i{-1, 1},
      Point2i{0, 2},
      Point2i{1, 1}
    });

  EXPECT_TRUE(PointsEqual(Point2i(0, 1), diamond.center()));
}

TEST (GeometryTests, Polygon_boundary)
{
  auto square = Polygon2i({
      Point2i{0, 0},
      Point2i{0, 2},
      Point2i{2, 2},
      Point2i{2, 0}
    });

  auto squareBoundary = square.boundary();
  EXPECT_EQ( 5, squareBoundary.coords().size() );
  EXPECT_EQ( Point2i(0, 0), squareBoundary.coords()[4] );
}

TEST (GeometryTests, Polygon2i_squareContains)
{
  auto square = Polygon2i({
      Point2i{0, 0},
      Point2i{0, 2},
      Point2i{2, 2},
      Point2i{2, 0}
    });

  EXPECT_TRUE( contains(square, Point2i(0, 0)) );
  EXPECT_TRUE( contains(square, Point2i(0, 1)) );
  EXPECT_TRUE( contains(square, Point2i(1, 1)) );

  EXPECT_FALSE( contains(square, Point2i(2, 1)) );
  EXPECT_FALSE( contains(square, Point2i(2, 2)) );
  EXPECT_FALSE( contains(square, Point2i(3, 2)) );
  EXPECT_FALSE( contains(square, Point2i(3, 3)) );
  EXPECT_FALSE( contains(square, Point2i(2, 3)) );
  EXPECT_FALSE( contains(square, Point2i(0, 3)) );
  EXPECT_FALSE( contains(square, Point2i(3, 0)) );
  EXPECT_FALSE( contains(square, Point2i(-1, -1)) );
  EXPECT_FALSE( contains(square, Point2i(-1, 1)) );
  EXPECT_FALSE( contains(square, Point2i(1, -1)) );
}

TEST (GeometryTests, Polygon2f_squareContains)
{
  auto square = Polygon2f({
      Point2f{0, 0},
      Point2f{0, 2},
      Point2f{2, 2},
      Point2f{2, 0}
    });

  EXPECT_TRUE( contains(square, Point2f(0, 0)) );
  EXPECT_TRUE( contains(square, Point2f(0, 1)) );
  EXPECT_TRUE( contains(square, Point2f(1, 1)) );
  EXPECT_TRUE( contains(square, Point2f(1.99999, 1)) );
  EXPECT_TRUE( contains(square, Point2f(1.99999, 1.99999)) );

  EXPECT_FALSE( contains(square, Point2f(2, 1)) );
  EXPECT_FALSE( contains(square, Point2f(2, 2)) );
  EXPECT_FALSE( contains(square, Point2f(3, 2)) );
  EXPECT_FALSE( contains(square, Point2f(3, 3)) );
  EXPECT_FALSE( contains(square, Point2f(2, 3)) );
  EXPECT_FALSE( contains(square, Point2f(0, 3)) );
  EXPECT_FALSE( contains(square, Point2f(3, 0)) );
  EXPECT_FALSE( contains(square, Point2f(-1, -1)) );
  EXPECT_FALSE( contains(square, Point2f(-1, 1)) );
  EXPECT_FALSE( contains(square, Point2f(1, -1)) );
}

TEST (GeometryTests, Polygon2d_diamondContains)
{
  auto diamond = Polygon2d({
      Point2d{0, 0},
      Point2d{-1, 1},
      Point2d{0, 2},
      Point2d{1, 1}
    });

  // vertices
  EXPECT_TRUE( contains(diamond, Point2d(-1, 1)) );
  EXPECT_FALSE( contains(diamond, Point2d(0, 0)) );
  EXPECT_FALSE( contains(diamond, Point2d(1, 1)) );
  EXPECT_FALSE( contains(diamond, Point2d(0, 2)) );

  // clearly inside
  EXPECT_TRUE( contains(diamond, Point2d(0, 1)) );
  EXPECT_TRUE( contains(diamond, Point2d(0, 0.001)) );

  // clearly outside
  EXPECT_FALSE( contains(diamond, Point2d(1, 0)) );
  EXPECT_FALSE( contains(diamond, Point2d(1, 1)) );
}

TEST (GeometryTests, Polygon2d_triangleContains)
{
  auto triangle = Polygon2d({
      Point2d{0, 0},
      Point2d{1, 0},
      Point2d{0, 1}
    });

  EXPECT_TRUE( contains(triangle, Point2d(0, 0)) );
  EXPECT_TRUE( contains(triangle, Point2d(0.0001, 0.0001)) );
  EXPECT_TRUE( contains(triangle, Point2d(0.0005, 0.9990)) );
  EXPECT_TRUE( contains(triangle, Point2d(0.9990, 0.0005)) );
  EXPECT_TRUE( contains(triangle, Point2d(0.4999, 0.4999)) );

  EXPECT_FALSE( contains(triangle, Point2d(0.0005, 1)) );
  EXPECT_FALSE( contains(triangle, Point2d(1, 0.0005)) );
  EXPECT_FALSE( contains(triangle, Point2d(0.5001, 0.5001)) );
  EXPECT_FALSE( contains(triangle, Point2d(1, 1)) );
  EXPECT_FALSE( contains(triangle, Point2d(-0.0001, -0.0001)) );
  EXPECT_FALSE( contains(triangle, Point2d(-0.0001, 0.0001)) );
  EXPECT_FALSE( contains(triangle, Point2d(0.0001, -0.0001)) );
}

TEST (GeometryTests, Polygon2d_LineSegment2d_intersection)
{
  auto unitSquare = Polygon2d({
      Point2d{0, 0},
      Point2d{1, 0},
      Point2d{1, 1},
      Point2d{0, 1}
    });

  // Exactly corner to corner, diagonally (touching all four line segments)
  auto result = intersection(unitSquare, LineSegment2d{Point2d{0, 0}, Point2d{1, 1}});
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0, 0), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(1, 1), result->p2()));

  // Inside, completely contained
  result = intersection(unitSquare, LineSegment2d(Point2d(0.25, 0.25), Point2d(0.75, 0.75)));
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0.25, 0.25), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(0.75, 0.75), result->p2()));

  // From inside (corner) to outside, running through diagonally opposite corner (touching all four line segments)
  result = intersection(unitSquare, LineSegment2d(Point2d(0, 0), Point2d(5, 5)));
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0, 0), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(1, 1), result->p2()));

  // Completely inside to outside, touching one line segment
  result = intersection(unitSquare, LineSegment2d(Point2d(0.5, 0.5), Point2d(1.5, 0.5)));
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0.5, 0.5), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(1, 0.5), result->p2()));

  // Run horizontally through cube, intersecting both left and right edges
  result = intersection(unitSquare, LineSegment2d(Point2d(-1, 0.5), Point2d(2, 0.5)));
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0, 0.5), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(1, 0.5), result->p2()));

  // Run diagonally through cube, bisecting the top left quadrant
  result = intersection(unitSquare, LineSegment2d(Point2d(-0.5, 0), Point2d(1, 1.5)));
  ASSERT_TRUE(result.hasValue());
  ASSERT_TRUE(PointsEqual(Point2d(0, 0.5), result->p1()));
  ASSERT_TRUE(PointsEqual(Point2d(0.5, 1), result->p2()));
}

TEST (GeometryTests, LineString)
{
  auto coords = Point2i::Vector({
      Point2i{0, 0},
      Point2i{1, 1},
      Point2i{2, -1},
      Point2i{3, 0}});

  auto lineString = LineString2i{coords};

  ASSERT_EQ( 4, lineString.coords().size() );
  EXPECT_TRUE( PointsEqual(Point2i(0, 0), lineString.coords()[0]) );
  EXPECT_TRUE( PointsEqual(Point2i(1, 1), lineString.coords()[1]) );
  EXPECT_TRUE( PointsEqual(Point2i(2, -1), lineString.coords()[2]) );
  EXPECT_TRUE( PointsEqual(Point2i(3, 0), lineString.coords()[3]) );
}

TEST (GeometryTests, LineString_bounds)
{
  auto coords = Point2i::Vector({
      Point2i{0, 0},
      Point2i{1, 1},
      Point2i{2, -1},
      Point2i{3, 0}});

  auto lineString = LineString2i{coords};
  EXPECT_TRUE(Eigen::AlignedBox2i(Vector2i(0, -1), Vector2i(3, 1)).isApprox(lineString.getBounds()));
}

TEST (GeometryTests, IsCCW)
{
  auto isCCW = IsCCW();
  
  auto coords1 = Point2i::Vector({
      Point2i{0, 0}, Point2i{1, 0}, Point2i{1, 1}
    });

  EXPECT_TRUE( isCCW(LineString2i(coords1)) );

  std::reverse(begin(coords1), end(coords1));
  EXPECT_FALSE( isCCW(LineString2i(coords1)) );

  auto coords2 = Point2i::Vector({
      Point2i{0, 0}, Point2i{1, 0}, Point2i{2, 0}
    });
  EXPECT_FALSE( isCCW(LineString2i(coords2)) );

  auto coords3 = Point2i::Vector({
      Point2i{0, 0}, Point2i{1, 0}
    });
  EXPECT_FALSE( isCCW(LineString2i(coords3)) );

  auto coords4 = Point2d::Vector({
      Point2d{-0.0808803, -0.0647004},
      Point2d{-0.0148898, -0.0648103},
      Point2d{-0.0147274, 0.0391876}
    });
  EXPECT_TRUE( isCCW(LineString2d(coords4)) );
}

TEST (GeometryTests, MultiPoint_grahamConvexHull)
{  
  auto coords1 = Point2i::Vector({
      Point2i{0, 0}, Point2i{2, 1}, Point2i{1, 1}
    });

  auto multiPoint1 = MultiPoint2i{coords1};

  auto convexHull = GrahamConvexHull<int>();
  
  auto hull1 = convexHull(multiPoint1);

  // Expect hull CCW from lowest point
  EXPECT_EQ( 3, hull1.coords().size() );
  EXPECT_TRUE( PointsEqual(coords1[0], hull1.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords1[1], hull1.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords1[2], hull1.coords()[2]) );

  auto coords2 = Point2i::Vector({
      Point2i{0, 1}, Point2i{1, 0}, Point2i{2, 1}
    });

  auto multiPoint2 = MultiPoint2i{coords2};

  auto hull2 = convexHull(multiPoint2);

  EXPECT_EQ( 3, hull2.coords().size() );
  EXPECT_TRUE( PointsEqual(coords2[1], hull2.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords2[2], hull2.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords2[0], hull2.coords()[2]) );
}

TEST (GeometryTests, MultiPoint_andrewConvexHull)
{
  auto coords1 = Point2i::Vector({
      Point2i{0, 0}, Point2i{2, 1}, Point2i{1, 1}
    });

  auto multiPoint1 = MultiPoint2i{coords1};

  auto convexHull2i = AndrewConvexHull<int>();
  
  auto hull1 = convexHull2i(multiPoint1);

  // Expect hull CCW from left most point
  EXPECT_EQ( 3, hull1.coords().size() );
  EXPECT_TRUE( PointsEqual(coords1[0], hull1.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords1[1], hull1.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords1[2], hull1.coords()[2]) );

  auto coords2 = Point2i::Vector({
      Point2i{1, 0}, Point2i{0, 1}, Point2i{2, 1}
    });

  auto multiPoint2 = MultiPoint2i{coords2};

  auto hull2 = convexHull2i(multiPoint2);

  EXPECT_EQ( 3, hull2.coords().size() );
  EXPECT_TRUE( PointsEqual(coords2[1], hull2.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords2[0], hull2.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords2[2], hull2.coords()[2]) );

  EXPECT_EQ(2, convexHull2i.getLowerUpperPivot());

  auto convexHull2d = AndrewConvexHull<double>();
  auto coords3 = Point2d::Vector({
      Point2d{ 0.0154824, -0.0630731},
      Point2d{ 0.0814818, -0.0632557},
      Point2d{0.0817773, 0.0407126},
      Point2d{0.0157779, 0.0408952},
      Point2d{-0.0148898, -0.0648103},
      Point2d{-0.0808803, -0.0647004},
      Point2d{-0.0807179, 0.0392975},
      Point2d{-0.0147274, 0.0391876}
    });

  auto multiPoint3 = MultiPoint2d{coords3};
  Polygon2d hull3 = convexHull2d(multiPoint3);

  EXPECT_EQ(6, hull3.coords().size());

  auto expectedCoords = Point2d::Vector({
      Point2d{-0.0808803, -0.0647004},
      Point2d{-0.0148898, -0.0648103},
      Point2d{ 0.0814818, -0.0632557},
      Point2d{0.0817773, 0.0407126},
      Point2d{0.0157779, 0.0408952},
      Point2d{-0.0807179, 0.0392975}
    });

  for (size_t i = 0; i < 6; ++i)
    EXPECT_TRUE(PointsEqual(expectedCoords[i], hull3.coords()[i]));
}

TEST (GeometryTests, LineString_convexHull)
{
  auto convexHull = GrahamConvexHull<int>();

  auto coords1 = Point2i::Vector({
      Point2i{0, 0}, Point2i{2, 1}, Point2i{1, 1}
    });
      
  auto lineString1 = LineString2i{coords1};
  
  auto hull1 = convexHull(lineString1);
  
  // Expect hull CCW from lowest point
  EXPECT_EQ( 3, hull1.coords().size() );
  EXPECT_TRUE( PointsEqual(coords1[0], hull1.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords1[1], hull1.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords1[2], hull1.coords()[2]) );
  
  auto coords2 = Point2i::Vector({
      Point2i{2, 1}, Point2i{1, 0}, Point2i{0, 1}
    });
  
  auto lineString2 = LineString2i{coords2};
  
  auto hull2 = convexHull(lineString2);
  
  EXPECT_EQ( 3, hull2.coords().size() );
  EXPECT_TRUE( PointsEqual(coords2[1], hull2.coords()[0]) );
  EXPECT_TRUE( PointsEqual(coords2[0], hull2.coords()[1]) );
  EXPECT_TRUE( PointsEqual(coords2[2], hull2.coords()[2]) );
}

TEST (GeometryTests, AndrewConvexHull_halfHull_topUnchanged)
{
  auto coords = Point2i::Vector({
      Point2i{0, 0}, Point2i{1, 1}, Point2i{2, 1}
    });

  auto multiPoint = MultiPoint2i{coords};

  auto convexHull = AndrewConvexHull<int>();

  auto hullBoundary = convexHull(multiPoint).boundary();

  auto hullTop = LineString2i(std::next(hullBoundary.coords().begin(),
                                        convexHull.getLowerUpperPivot()),
                              hullBoundary.coords().end());

  
  EXPECT_EQ( 3, hullTop.coords().size() );

  // Hull is formed counter clockwise
  EXPECT_EQ( coords[2], hullTop.coords()[0] );
  EXPECT_EQ( coords[1], hullTop.coords()[1] );
  EXPECT_EQ( coords[0], hullTop.coords()[2] );
}

TEST (GeometryTests, AndrewConvexHull_halfHull_topChanged)
{
  auto coords = Point2i::Vector({
      Point2i{0, 0}, Point2i{1, 0}, Point2i{2, 1}
    });

  auto multiPoint = MultiPoint2i{coords};

  auto convexHull = AndrewConvexHull<int>();

  auto hullBoundary = convexHull(multiPoint).boundary();

  auto hullTop = LineString2i(std::next(hullBoundary.coords().begin(),
                                        convexHull.getLowerUpperPivot()),
                              hullBoundary.coords().end());

  
  EXPECT_EQ( 2, hullTop.coords().size() );

  // Hull is formed counter clockwise
  EXPECT_EQ( coords[2], hullTop.coords()[0] );
  // coords[1] is skipped
  EXPECT_EQ( coords[0], hullTop.coords()[1] );
}

TEST (GeometryTests, AndrewConvexHull_halfHull_bottomUnchanged)
{
  auto coords = Point2i::Vector({
      Point2i{0, 1}, Point2i{1, 0}, Point2i{2, 1}
    });

  auto multiPoint = MultiPoint2i{coords};

  auto convexHull = AndrewConvexHull<int>();

  auto hullBoundary = convexHull(multiPoint).boundary();

  auto hullBottom = LineString2i(hullBoundary.coords().begin(),
                                 std::next(hullBoundary.coords().begin(),
                                           convexHull.getLowerUpperPivot() + 1));

  EXPECT_EQ( 3, hullBottom.coords().size() );

  // Hull is formed counter clockwise
  EXPECT_EQ( coords[0], hullBottom.coords()[0] );
  EXPECT_EQ( coords[1], hullBottom.coords()[1] );
  EXPECT_EQ( coords[2], hullBottom.coords()[2] );
}

TEST (GeometryTests, Geometry_toDim)
{
  auto coords = Point3d::Vector({
      Point3d{0, 1, 1}, Point3d{1, 0, 2}, Point3d{2, 1, 3}
    });

  auto lineSegment3 = LineSegment3d{coords[0], coords[1]};
  auto polygon3 = Polygon3d{coords};

  auto lineSegment2 = lineSegment3.toDim<2>();
  auto polygon2 = polygon3.toDim<3>();
}
