// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "src/util/BufferReader/bufferreader.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

TEST (BufferReaderTests, readInt8)
{
  vector<char> bytes = {1,2,3,4,5,6};
  BufferReader reader(bytes.data());

  EXPECT_EQ(1, reader.read<int8_t>());
  EXPECT_EQ(2, reader.read<int8_t>());
  EXPECT_EQ(3, reader.read<int8_t>());
  EXPECT_EQ(4, reader.read<int8_t>());
  EXPECT_EQ(5, reader.read<int8_t>());
  EXPECT_EQ(6, reader.read<int8_t>());
}

TEST (BufferReaderTests, readInt16)
{
  vector<char> bytes = {1,2,3,4,5,6};
  BufferReader reader(bytes.data());

  EXPECT_EQ(0x0201, reader.read<int16_t>());
  EXPECT_EQ(0x0403, reader.read<int16_t>());
  EXPECT_EQ(0x0605, reader.read<int16_t>());
}

TEST (BufferReaderTests, readInt32)
{
  vector<char> bytes = {1,2,3,4,5,6,7,8};
  BufferReader reader(bytes.data());

  EXPECT_EQ(0x04030201, reader.read<int32_t>());
  EXPECT_EQ(0x08070605, reader.read<int32_t>());
}
