// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "motion/scripts/MotionScript/motionscript.hh"

using namespace bold::motion::scripts;

auto sitScriptJson = R"END(
{
  "name": "sit-down",
  "stages": [
    {
      "keyFrames": [
        {
          "moveCycles": 125,
          "values": {
            "shoulder-pitch": 2048,
            "shoulder-roll": 1671,
            "elbow": 983,
            "hip-yaw": 2048,
            "hip-roll": 2048,
            "hip-pitch": 1377,
            "knee": 3513,
            "ankle-pitch": 2843,
            "ankle-roll": 2077,
            "head-pan": 2050,
            "head-tilt": 2374
          }
        }
      ]
    },
    {
      "pGains": {
        "shoulder-pitch": 2,
        "shoulder-roll": 2,
        "elbow": 2,
        "knee": 2,
        "head-pan": 2,
        "head-tilt": 2
      },
      "keyFrames": [
        {
          "moveCycles": 3,
          "values": {
            "shoulder-pitch": 2048,
            "shoulder-roll": 1671,
            "elbow": 983,
            "hip-yaw": 2048,
            "hip-roll": 2048,
            "hip-pitch": 1377,
            "knee": 3513,
            "ankle-pitch": 2843,
            "ankle-roll": 2077,
            "head-pan": 2050,
            "head-tilt": 2374
          }
        }
      ]
    }
  ]
}
)END";

auto zeroScriptJson = R"END(
{
  "name": "zero",
  "stages": [
    {
      "keyFrames": [
        {
          "moveCycles": 125,
          "values": {
            "shoulder-pitch": 2048,
            "shoulder-roll": 2048,
            "elbow": 2048,
            "hip-yaw": 2048,
            "hip-roll": 2048,
            "hip-pitch": 2048,
            "knee": 2048,
            "ankle-pitch": 2048,
            "ankle-roll": 2048,
            "head-pan": 2048,
            "head-tilt": 2048
          }
        }
      ]
    }
  ]
}
)END";

TEST( MotionScriptTests, fromStream )
{
  auto script = MotionScript::fromStream(rapidjson::StringStream{zeroScriptJson});

  EXPECT_EQ( "zero", script->getName() );
  EXPECT_EQ( 1, script->getStages().size() );
  EXPECT_EQ( 1, script->getStages()[0]->keyFrames.size() );

  script = MotionScript::fromStream(rapidjson::StringStream{sitScriptJson});

  EXPECT_EQ( "sit-down", script->getName() );
  EXPECT_EQ( 2, script->getStages().size() );
  EXPECT_EQ( 1, script->getStages()[0]->keyFrames.size() );
  EXPECT_EQ( 1, script->getStages()[1]->keyFrames.size() );

  EXPECT_EQ( 2, script->getStages()[1]->getPGain(bold::JointId::L_KNEE) );
  EXPECT_EQ( 2, script->getStages()[1]->getPGain((uint8_t)bold::JointId::L_KNEE) );
}

TEST( MotionScriptTests, copy )
{
  auto script1 = MotionScript::fromStream(rapidjson::StringStream{zeroScriptJson});
  auto script2 = MotionScript::fromStream(rapidjson::StringStream{sitScriptJson});

  auto script1ptr = script1;
  
  *script1 = *script2;
  // Script should be same
  EXPECT_EQ( "sit-down", script1->getName() );
  EXPECT_EQ( 2, script1->getStages().size() );
  EXPECT_EQ( 1, script1->getStages()[0]->keyFrames.size() );
  EXPECT_EQ( 1, script1->getStages()[1]->keyFrames.size() );

  EXPECT_EQ( 2, script1->getStages()[1]->getPGain(bold::JointId::L_KNEE) );
  EXPECT_EQ( 2, script1->getStages()[1]->getPGain((uint8_t)bold::JointId::L_KNEE) );

  // Other pointer to script should have the same values too
  EXPECT_EQ( "sit-down", script1ptr->getName() );
  EXPECT_EQ( 2, script1ptr->getStages().size() );
  EXPECT_EQ( 1, script1ptr->getStages()[0]->keyFrames.size() );
  EXPECT_EQ( 1, script1ptr->getStages()[1]->keyFrames.size() );

  EXPECT_EQ( 2, script1ptr->getStages()[1]->getPGain(bold::JointId::L_KNEE) );
  EXPECT_EQ( 2, script1ptr->getStages()[1]->getPGain((uint8_t)bold::JointId::L_KNEE) );
}
