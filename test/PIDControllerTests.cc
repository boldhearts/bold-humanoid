// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "PIDController/pidcontroller.hh"

using namespace bold;
using namespace std;

TEST( PIDControllerTests, configure )
{
  auto controller = PIDController{0.9, 0.6, 0.3};

  EXPECT_EQ( 0.9, controller.getPGain() );
  EXPECT_EQ( 0.6, controller.getIGain() );
  EXPECT_EQ( 0.3, controller.getDGain() );
  auto expectedArray = array<double, 3>{{0.9, 0.6, 0.3}};
  EXPECT_EQ( expectedArray, controller.getGains() );

  controller.setPGain(1.0);
  EXPECT_EQ( 1.0, controller.getPGain() );

  controller.setIGain(1.0);
  EXPECT_EQ( 1.0, controller.getIGain() );

  controller.setDGain(1.0);
  EXPECT_EQ( 1.0, controller.getDGain() );

  controller.setGains(0.1, 0.2, 0.3);
  expectedArray = array<double, 3>{{0.1, 0.2, 0.3}};
  EXPECT_EQ( expectedArray, controller.getGains() );
}

TEST( PIDControllerTests, PController )
{
  auto controller = PIDController{0.9, 0.0, 0.0};

  EXPECT_EQ( 0.0, controller.step(0.0) );
  EXPECT_EQ( 0.9, controller.step(1.0) );
  EXPECT_EQ( -0.9, controller.step(-1.0) );
}

TEST( PIDControllerTests, IController )
{
  auto controller = PIDController{0.0, 0.9, 0.0};

  EXPECT_EQ( 0.0, controller.step(0.0) );
  EXPECT_EQ( 0.9, controller.step(1.0) );
  EXPECT_EQ( 0.0, controller.step(-1.0) );
  EXPECT_EQ( 0.9, controller.step(1.0) );
  EXPECT_EQ( 1.8, controller.step(1.0) );
}

TEST( PIDControllerTests, DController )
{
  auto controller = PIDController{0.0, 0.0, 0.9};

  EXPECT_EQ( 0.0, controller.step(0.0) );
  EXPECT_EQ( 0.9, controller.step(1.0) );
  EXPECT_EQ( -1.8, controller.step(-1.0) );
  EXPECT_EQ( 1.8, controller.step(1.0) );
  EXPECT_EQ( 0.0, controller.step(1.0) );
}
