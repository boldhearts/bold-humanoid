// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "FSRModel/fsrmodel.hh"
#include "helpers.hh"

using namespace bold;
using namespace Eigen;

TEST (FSRModelTests, construction)
{
  FSRModel{};
  FSRModel{0.025, 0.041, 0.493, 1e-3};
}

TEST (FSRModelTests, fsrValueToNs)
{
  // Models based on Robotis descriptions
  // http://support.robotis.com/en/product/darwin-op/references/reference/hardware_specifications/electronics/optional_components/fsr.htm
  auto model = FSRModel{0.025, 0.041, 0.493, 1e-3};
  
  EXPECT_EQ( 0.0, model.fsrValueToNs(0) );

  EXPECT_EQ( 0.0, model.fsrValueToNs(492) );

  EXPECT_EQ( 0.493, model.fsrValueToNs(493) );

  EXPECT_EQ( 1, model.fsrValueToNs(1000) );

  EXPECT_EQ( 9.8, model.fsrValueToNs(9800) );
}

TEST (FSRModelTests, centerValueToN)
{
  auto offsetX = 0.025;
  auto offsetY = 0.041;

  auto model = FSRModel{offsetX, offsetY, 0.493, 1e-3};

  EXPECT_EQ( util::Maybe<Vector2d>::empty(), model.centerValueTo2D(255, 255, FSRModel::LEFT) );
  EXPECT_EQ( util::Maybe<Vector2d>::empty(), model.centerValueTo2D(255, 255, FSRModel::RIGHT) );

  // 0,0 on left foot is at back right
  EXPECT_TRUE( VectorsEqual(Vector2d(offsetX, -offsetY), model.centerValueTo2D(0, 0, FSRModel::LEFT).value()) );
  // 0,0 on right foot is at front left
  EXPECT_TRUE( VectorsEqual(Vector2d(-offsetX, offsetY), model.centerValueTo2D(0, 0, FSRModel::RIGHT).value()) );

  // 254,254 on left foot is at front left
  EXPECT_TRUE( VectorsEqual(Vector2d(offsetX, -offsetY), model.centerValueTo2D(0, 0, FSRModel::LEFT).value()) );
  // 254,254 on right foot is at front left
  EXPECT_TRUE( VectorsEqual(Vector2d(-offsetX, offsetY), model.centerValueTo2D(0, 0, FSRModel::RIGHT).value()) );

  // 127 should be at center
  EXPECT_TRUE( VectorsEqual(Vector2d::Zero(), model.centerValueTo2D(127, 127, FSRModel::LEFT).value()) );
  EXPECT_TRUE( VectorsEqual(Vector2d::Zero(), model.centerValueTo2D(127, 127, FSRModel::RIGHT).value()) );
}

