// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "AccelerometerModel/accelerometermodel.hh"

using namespace bold;

TEST (AccelerometerModelTests, valueToGs)
{
  // Models according to raw Accelerometer readings
  // http://www.st.com/web/en/resource/technical/document/datasheet/CD00213470.pdf
  auto model1 = AccelerometerModel(1.0 / (1 << 4), 2.0, 1 << 16);
  EXPECT_FLOAT_EQ(0.0, model1.valueToGs(1 << 15));

  EXPECT_FLOAT_EQ(-2.0, model1.valueToGs(0));
  EXPECT_FLOAT_EQ(2.0, model1.valueToGs((1 << 16) - 1));

  EXPECT_FLOAT_EQ(-1.0, model1.valueToGs((1 << 15) - 1000 * (1 << 4)));
  EXPECT_FLOAT_EQ(1.0, model1.valueToGs((1 << 15) + 1000 * (1 << 4)));

  auto model2 = AccelerometerModel(2.0 / (1 << 4), 4.0, 1 << 16);
  EXPECT_FLOAT_EQ(0.0, model2.valueToGs(1 << 15));

  EXPECT_FLOAT_EQ(-4.0, model2.valueToGs(0));
  EXPECT_FLOAT_EQ(4.0, model2.valueToGs((1 << 16) - 1));

  EXPECT_FLOAT_EQ(-2.0, model2.valueToGs((1 << 15) - 1000 * (1 << 4)));
  EXPECT_FLOAT_EQ(2.0, model2.valueToGs((1 << 15) + 1000 * (1 << 4)));

  auto model3 = AccelerometerModel(3.9 / (1 << 4), 8.0, 1 << 16);
  EXPECT_FLOAT_EQ(0.0, model3.valueToGs(1 << 15));

  EXPECT_FLOAT_EQ(-7.9872, model3.valueToGs(0));
  EXPECT_FLOAT_EQ(7.98695625, model3.valueToGs((1 << 16) - 1));

  EXPECT_FLOAT_EQ(-3.9, model3.valueToGs((1 << 15) - 1000 * (1 << 4)));
  EXPECT_FLOAT_EQ(3.9, model3.valueToGs((1 << 15) + 1000 * (1 << 4)));

  // Models based on Robotis CM730 firmware
  // Values are scaled from 16 to 10 bits
  auto mGsPerDigit4 = 2.0 / (1 << 4) * 64;
  ASSERT_EQ(8.0, mGsPerDigit4);

  auto model4 = AccelerometerModel(mGsPerDigit4, 4.0, 1 << 10);
  EXPECT_FLOAT_EQ(0.0, model4.valueToGs(1 << 9));
  
  EXPECT_FLOAT_EQ(-4.0, model4.valueToGs(0));
  EXPECT_FLOAT_EQ(4.0, model4.valueToGs((1 << 10) - 1));

  EXPECT_FLOAT_EQ(-mGsPerDigit4 * 100 / 1000, model4.valueToGs((1 << 9) - 100));
  EXPECT_FLOAT_EQ(mGsPerDigit4 * 100 / 1000, model4.valueToGs((1 << 9) + 100));
}
