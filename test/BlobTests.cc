// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <algorithm>
#include "helpers.hh"
#include "vision/LabelledImageProcessor/BlobDetector/blobdetector.hh"

using namespace std;
using namespace bold;
using namespace bold::vision;
using namespace Eigen;

TEST (BlobTests, runSetToBlob)
{
  set<bold::vision::Run> runSet;
  runSet.insert(bold::vision::Run(4,5,0));
  runSet.insert(bold::vision::Run(3,6,1));
  runSet.insert(bold::vision::Run(2,7,2));
  runSet.insert(bold::vision::Run(1,8,3));
  runSet.insert(bold::vision::Run(0,9,4));
  runSet.insert(bold::vision::Run(1,8,5));
  runSet.insert(bold::vision::Run(2,7,6));
  runSet.insert(bold::vision::Run(3,6,7));
  runSet.insert(bold::vision::Run(4,5,8));

  auto blob = Blob{runSet};

  EXPECT_EQ ( 50, blob.area() );
  EXPECT_EQ ( Vector2f(4.5, 4), blob.mean() );
  EXPECT_EQ ( Vector2i(0,0), blob.bounds().min() );
  EXPECT_EQ ( Vector2i(9,8), blob.bounds().max() );
  EXPECT_TRUE ( AlignedBoxesEqual(AlignedBox2i(Vector2i(0, 0), Vector2i(9, 8)), blob.bounds()) );
}

TEST (BlobTests, compare)
{
  auto runSet1 = set<bold::vision::Run>{};
  for (unsigned y = 0; y < 10; ++y)
    runSet1.emplace(0, 9, y);
  auto blob1 = Blob{runSet1};
  
  auto runSet2 = set<bold::vision::Run>{};
  for (unsigned y = 0; y < 6; ++y)
    runSet2.emplace(0, 5, y);
  auto blob2 = Blob{runSet2};

  auto runSet3 = set<bold::vision::Run>{};
  for (unsigned y = 3; y < 9; ++y)
    runSet3.emplace(3, 8, y);
  auto blob3 = Blob{runSet3};

  EXPECT_TRUE ( blob2 < blob1 ); // Smaller area
  EXPECT_TRUE ( blob3 < blob1 ); // Smaller area
  EXPECT_TRUE ( blob2 < blob3 ); // Below
}

TEST (BlobTests, equal)
{
  auto runSet1 = set<bold::vision::Run>{};
  for (unsigned y = 0; y < 10; ++y)
    runSet1.emplace(0, 9, y);
  auto blob1 = Blob{runSet1};
  auto blob2 = Blob{runSet1};

  EXPECT_EQ( blob1, blob2 );

  auto runSet3 = set<bold::vision::Run>{};
  for (unsigned y = 3; y < 9; ++y)
    runSet3.emplace(3, 8, y);
  auto blob3 = Blob{runSet3};

  EXPECT_NE( blob3, blob1 );
}

TEST (BlobTests, merge)
{
  auto runSet1 = set<bold::vision::Run>{};
  for (unsigned y = 0; y < 6; ++y)
    runSet1.emplace(0, 5, y);
  auto blob1 = Blob{runSet1};

  auto runSet2 = set<bold::vision::Run>{};
  for (unsigned y = 8; y < 14; ++y)
    runSet2.emplace(8, 13, y);
  auto blob2 = Blob{runSet2};

  auto runSet3 = set<bold::vision::Run>{};
  set_union(begin(runSet1), end(runSet1),
            begin(runSet2), end(runSet2),
            std::inserter(runSet3, begin(runSet3)));
  auto blob3 = Blob{runSet3};
  
  blob1.merge(blob2);

  EXPECT_EQ(blob3.area(), blob1.area());
  EXPECT_EQ(72, blob1.area());

  EXPECT_TRUE(VectorsEqual(blob3.mean(), blob1.mean()));
  EXPECT_TRUE(VectorsEqual(Vector2f(6.5, 6.5), blob1.mean()));

  EXPECT_EQ(Vector2i(0, 0), blob1.bounds().min());
  EXPECT_EQ(blob3.bounds().min(), blob1.bounds().min());

  EXPECT_EQ(Vector2i(13, 13), blob1.bounds().max());
  EXPECT_EQ(blob3.bounds().max(), blob1.bounds().max());

  EXPECT_EQ(blob3.runs(), blob1.runs());
}
