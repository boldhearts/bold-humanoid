// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "CM730/cm730.hh"
#include "src/util/binary.hh"

using namespace bold;
using namespace bold::util;

TEST (CM730Tests, conversions)
{
  EXPECT_EQ( (unsigned char)0xFF, binary::getHighByte(0xFF88) );
  EXPECT_EQ( (unsigned char)0x88, binary::getLowByte(0xFF88) );
  EXPECT_EQ( 0xFF88, binary::makeWord(0x88, 0xFF) );

  // Colours are encoded as BGR with 5 bits per channel
  // The 3 LSB of each channel are dropped

  EXPECT_EQ( 0x7C1F, CM730::color2Value(0xFF, 0x00, 0xFF) ); // 11111 00000 11111
  EXPECT_EQ( 0x5764, CM730::color2Value(0x20, 0xD8, 0xA8) ); // 10101 11011 00100
  EXPECT_EQ( 0x5764, CM730::color2Value(0x21, 0xD9, 0xA9) ); // same as prior, but with diff in LSB which is lost
}

TEST (CM730Tests, flipImuValue)
{
  EXPECT_EQ( 512,  CM730::flipImuValue(512) );
  EXPECT_EQ( 1023, CM730::flipImuValue(0) );
  EXPECT_EQ( 1,    CM730::flipImuValue(1023) );
}
