// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "src/util/SchmittTrigger/schmitttrigger.hh"

using namespace bold;
using namespace bold::util;

TEST (SchmittTriggerTests, basics)
{
  SchmittTrigger<int> trigger(0, 10, true);

  EXPECT_EQ ( 0, trigger.getLowThreshold() );
  EXPECT_EQ ( 10, trigger.getHighThreshold() );

  EXPECT_TRUE ( trigger.isHigh() );

  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(5) );

  EXPECT_TRUE ( trigger.isHigh() );

  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(1) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(9) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(10) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(5) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(15) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(10) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(5) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(20) );
  EXPECT_TRUE ( trigger.isHigh() );
  EXPECT_EQ ( SchmittTriggerTransition::Low, trigger.next(-2) );
  EXPECT_FALSE ( trigger.isHigh() );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(-2) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(5) );
  EXPECT_EQ ( SchmittTriggerTransition::None, trigger.next(-2) );
  EXPECT_EQ ( SchmittTriggerTransition::High, trigger.next(10) );
  EXPECT_TRUE ( trigger.isHigh() );
  EXPECT_EQ ( SchmittTriggerTransition::Low, trigger.next(0) );
  EXPECT_EQ ( SchmittTriggerTransition::High, trigger.next(11) );
  EXPECT_EQ ( SchmittTriggerTransition::Low, trigger.next(-1) );
}
