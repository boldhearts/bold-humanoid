// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "Kinematics/kinematics.hh"
#include "helpers.hh"

using namespace bold;
using namespace Eigen;

TEST(KinematicsTest, legAnglesForTarget)
{
  auto maybeAngles = Kinematics::legAnglesForTarget(Vector3d::Zero(), Vector3d::Zero());
  EXPECT_TRUE(maybeAngles.hasValue());
  EXPECT_TRUE(MatricesEqual(Matrix<double, 6, 1>::Zero(), maybeAngles.value()));

  maybeAngles = Kinematics::legAnglesForTarget(Vector3d(0, 0, -1), Vector3d::Zero());
  EXPECT_FALSE(maybeAngles.hasValue());
}

TEST(KinematicsTest, legAnglesForTarget_up)
{
  auto maybeAngles = Kinematics::legAnglesForTarget(Vector3d(0, 0, 20), Vector3d::Zero());
  EXPECT_TRUE( maybeAngles.hasValue() );
  auto angles = *maybeAngles;
  // No yaw
  EXPECT_FLOAT_EQ( 0.0, angles(0) );
  // No roll
  EXPECT_FLOAT_EQ( 0.0, angles(1) );
  EXPECT_FLOAT_EQ( 0.0, angles(5) );
  // Hip and foot pitch equal
  EXPECT_FLOAT_EQ( angles(2), angles(4) );
  // Hip + ankle cancel out knee
  EXPECT_FLOAT_EQ( 0.0, angles(2) + angles(3) + angles(4) );
}

TEST(KinematicsTest, legAnglesForTarget_upfb)
{
  for (auto i = -19; i <= 19; i += 2)
  {
    auto maybeAngles = Kinematics::legAnglesForTarget(Vector3d(i, 0, 20), Vector3d::Zero());
    EXPECT_TRUE( maybeAngles.hasValue() );
    auto angles = *maybeAngles;

    // No yaw
    EXPECT_FLOAT_EQ( 0.0, angles(0) );
    // No roll
    EXPECT_FLOAT_EQ( 0.0, angles(1) );
    EXPECT_FLOAT_EQ( 0.0, angles(5) );
    // Hip and foot pitch not equal
    EXPECT_NE( angles(2), angles(4) );
    // Hip + ankle cancel out knee
    EXPECT_NEAR( 0.0, angles(2) + angles(3) + angles(4), 1e-12 );

    // Ankle pitch larger than hip
    if (i < 0)
      EXPECT_GT( std::abs(angles(4)), std::abs(angles(2)) );
    else
      EXPECT_LT( std::abs(angles(4)), std::abs(angles(2)) );
  }
}
