// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>


#include <functional>
#include <sigc++/connection.h>
#include <sigc++/signal.h>

using namespace std;

TEST (SignalTests, nothingConnected)
{
  sigc::signal<void> sig;

  sig();
}

TEST (SignalTests, singleConnected)
{
  sigc::signal<void> sig;

  bool wasCalled = false;

  sig.connect([&] { wasCalled = true; });

  EXPECT_FALSE ( wasCalled );

  sig();

  EXPECT_TRUE ( wasCalled );
}

TEST (SignalTests, multipleConnected)
{
  sigc::signal<void> sig;

  int count = 0;

  sig.connect([&] { count++; });
  sig.connect([&] { count++; });

  sig();

  EXPECT_EQ ( 2, count );
}

TEST (SignalTests, connections)
{
  sigc::signal<void> sig;

  int count = 0;

  sigc::connection conn = sig.connect([&] { count++; });

  sig();

  EXPECT_EQ ( 1, count );

  EXPECT_TRUE ( conn.connected() );

  conn.block();

  sig();

  EXPECT_EQ ( 1, count );
  EXPECT_TRUE ( conn.blocked() );

  conn.unblock();

  EXPECT_EQ ( 1, count );
  EXPECT_FALSE ( conn.blocked() );

  sig();

  EXPECT_EQ ( 2, count );
}
