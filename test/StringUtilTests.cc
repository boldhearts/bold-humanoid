// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "util/string.hh"

using namespace bold::util;

TEST(StringUtilTests, startsWithTrue)
{
  EXPECT_TRUE(stringutil::startsWith("foobar", "foo"));
  EXPECT_TRUE(stringutil::startsWith("foobar", "f"));
  EXPECT_TRUE(stringutil::startsWith("foobar", ""));
  EXPECT_TRUE(stringutil::startsWith("foobar", "foobar"));
}

TEST(StringUtilTests, startsWithFalse)
{
  EXPECT_FALSE(stringutil::startsWith("foobar", "oo"));
  EXPECT_FALSE(stringutil::startsWith("foobar", "foobar2"));
  EXPECT_FALSE(stringutil::startsWith("foobar", "bar"));
}

TEST(StringUtilTests, endsWithTrue)
{
  EXPECT_TRUE(stringutil::endsWith("foobar", "bar"));
  EXPECT_TRUE(stringutil::endsWith("foobar", "r"));
  EXPECT_TRUE(stringutil::endsWith("foobar", ""));
  EXPECT_TRUE(stringutil::endsWith("foobar", "foobar"));
}

TEST(StringUtilTests, endsWithFalse)
{
  EXPECT_FALSE(stringutil::endsWith("foobar", "oo"));
  EXPECT_FALSE(stringutil::endsWith("foobar", "2foobar"));
  EXPECT_FALSE(stringutil::endsWith("foobar", "foo"));
}
