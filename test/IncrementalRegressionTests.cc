// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "IncrementalRegression/incrementalregression.hh"

using namespace std;
using namespace bold;
using namespace Eigen;

TEST (IncrementalRegressionTests, basic)
{
  IncrementalRegression regression;
  regression.setSqError(1.0);

  EXPECT_EQ( regression.getNPoints(), 0 );

  auto point1 = Vector2f{0, 0};
  auto point2 = Vector2f{1, 1};
  auto point3 = Vector2f{2, 3};

  regression.addPoint(point1);
  EXPECT_EQ( regression.getNPoints(), 1);
  EXPECT_EQ( regression.head(), point1 );

  regression.addPoint(point2);
  regression.solve();
  EXPECT_EQ( regression.getNPoints(), 2);
  EXPECT_EQ( regression.head(), point2 );

  EXPECT_TRUE( regression.fit(point3) < 1.0f );
  
  auto lineSegment = regression.getLineSegment();

  EXPECT_EQ( lineSegment.p1(), point1 );
  EXPECT_EQ( lineSegment.p2(), point2 );

  EXPECT_EQ( regression.fit(Vector2f{0, 0}), 0 );
  EXPECT_EQ( regression.fit(Vector2f{0.5, 0.5}), 0 );
  EXPECT_EQ( regression.fit(Vector2f{1, 1}), 0 );
  EXPECT_EQ( regression.fit(Vector2f{2, 2}), 0 );
}
