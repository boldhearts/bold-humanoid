// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <iostream>

#include "src/util/Maybe/maybe.hh"

using namespace bold;
using namespace bold::util;

TEST( MaybeTests, make_maybe )
{
  auto maybeInt = make_maybe(1);
  EXPECT_TRUE( maybeInt.hasValue() );
  EXPECT_EQ( 1, maybeInt.value() );
  EXPECT_EQ( 1, *maybeInt );

  auto maybeDouble = make_maybe(1.5);
  EXPECT_TRUE( maybeDouble.hasValue() );
  EXPECT_EQ( 1.5, maybeDouble.value() );
  EXPECT_EQ( 1.5, *maybeDouble );
}

TEST( MaybeTests, move )
{
  auto maybeInt = make_maybe(1);
  auto movedMaybeInt = std::move(maybeInt);

  EXPECT_TRUE( movedMaybeInt.hasValue() );
  EXPECT_EQ( 1, movedMaybeInt.value() );
  EXPECT_FALSE( maybeInt.hasValue() );
}

TEST( MaybeTests, rangefor )
{
  auto maybeInt = make_maybe(1);
  for(auto i : maybeInt)
    EXPECT_EQ( 1, i );

  auto noInt = Maybe<int>::empty();
  EXPECT_NO_THROW(
    for(auto i : noInt)
      throw std::runtime_error("this should not happen");
    );

}
