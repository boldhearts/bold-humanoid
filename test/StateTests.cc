// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>


#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/TimingState/timingstate.hh"

#include <thread>
#include <vector>

using namespace bold;
using namespace bold::geometry2;
using namespace bold::state;
using namespace std;
using namespace Eigen;

// NOTE these tests ensure the same threading characteristics on development and production environments

TEST (StateTests, threadedAccess)
{
  int loopCount = 50000;

  thread producer([&]
  {
    for (int i = 0; i < loopCount; i++)
    {
      auto eventTimings = make_shared<vector<EventTiming>>();
      eventTimings->emplace_back("Event 1", 0.1);
      eventTimings->emplace_back("Event 2", 0.2);
      State::make<MotionTimingState>(eventTimings, 1, 12.34);
    }
  });

  bool seenState = false;

  thread consumer([&]
  {
    for (int i = 0; i < loopCount; i++)
    {
      auto const& state = State::get<MotionTimingState>();

      if (state)
      {
        EXPECT_EQ(2, state->getTimings()->size());
        seenState = true;
      }
    }
  });

  producer.join();
  consumer.join();

  EXPECT_TRUE(seenState);
}

TEST (StateTests, setAndGet)
{
  auto eventTimings = make_shared<vector<EventTiming>>();
  eventTimings->emplace_back("Event 1", 0.1);
  eventTimings->emplace_back("Event 2", 0.2);
  State::make<MotionTimingState>(eventTimings, 2, 12.34);

  shared_ptr<MotionTimingState const> state = State::get<MotionTimingState>();

  EXPECT_NE ( nullptr, state );
  EXPECT_EQ(2, state->getTimings()->size());
  EXPECT_EQ(12.34, state->getAverageFps());
}

TEST(StateTests, CameraFrameState)
{
  auto goalObservations = Vector2dVector{};
  goalObservations.emplace_back(0, 0);

  auto teamMateObservations = Vector2dVector{};
  teamMateObservations.emplace_back(0, 0);

  auto observedLineSegments = LineSegment2i::Vector{};
  observedLineSegments.emplace_back(Point2i{0, 0},
                                    Point2i{1, 1});

  auto occlusionRays = vector<OcclusionRay<uint16_t>>{};
  occlusionRays.emplace_back(
    Point<uint16_t, 2>{0, 0},
    Point<uint16_t, 2>{1, 1}
    );
  
  auto cameraFrame = CameraFrameState(
    util::Maybe<Vector2d>::empty(),
    goalObservations,
    teamMateObservations,
    observedLineSegments,
    occlusionRays,
    0, 0, 0);
}

TEST(StateTests, AgentFrameState)
{
  auto occlusionRays = OcclusionRay<double>::Vector{};
  occlusionRays.push_back(
    OcclusionRay<double>{
      Point2d{0, 0},
        Point2d{0, 1}
    }
    );
  occlusionRays.push_back(
    OcclusionRay<double>{
      Point2d{1, 0},
        Point2d{1, 1}
    }
    );

  auto poly = AgentFrameState::getOcclusionPoly(occlusionRays);
  
}
