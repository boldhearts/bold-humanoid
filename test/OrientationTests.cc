// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "geometry2/Orientation/orientation.hh"
#include "helpers.hh"

using namespace bold::geometry2;
using namespace Eigen;
using namespace std;

TEST(OrientationTests, construction)
{
  auto orientation = Orientation<double, 3>{};
  auto quaternion = orientation.getQuaternion();

  EXPECT_EQ( 1.0, quaternion.w() );
  EXPECT_EQ( Eigen::Vector3d::Zero(), quaternion.vec() );
}

TEST(OrientationTests, gyro_halfPiXHighFreq)
{
  auto orientation = Orientation<double, 3>{};
  for (unsigned i = 0; i < 512; ++i)
    orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, 1.0 / 512);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitX()).matrix(),
                            orientation.getQuaternion().matrix()));
}


TEST(OrientationTests, gyro_halfPiX)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, 1.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitX()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_halfPiY)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0.5 * M_PI, 0}, 1.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitY()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_halfPiZ)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0, 0.5 * M_PI}, 1.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitZ()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_piX)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, 2.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(M_PI, Vector3d::UnitX()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_piY)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0.5 * M_PI, 0}, 2.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(M_PI, Vector3d::UnitY()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_piZ)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0, 0.5 * M_PI}, 2.0);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(M_PI, Vector3d::UnitZ()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_quartPiX)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, .5);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.25 * M_PI, Vector3d::UnitX()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_quartPiY)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0.5 * M_PI, 0}, .5);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.25 * M_PI, Vector3d::UnitY()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_quartPiZ)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0, 0, 0.5 * M_PI}, .5);
  
  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(0.25 * M_PI, Vector3d::UnitZ()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_multiAxis)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{1.0, 1.0, 1.0}.normalized() * 2.0 / 3.0 * M_PI, 1.0);

  EXPECT_TRUE(MatricesEqual(Eigen::AngleAxisd(2.0 / 3.0 * M_PI, Vector3d(1.0, 1.0, 1.0).normalized()).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, gyro_zero)
{
  auto orientation = Orientation<double, 3>{};
  Vector4d oldCoeffs = orientation.getQuaternion().coeffs();
  orientation.integrate(Vector3d::Zero(), 0.008);
  
  EXPECT_TRUE(MatricesEqual(oldCoeffs, orientation.getQuaternion().coeffs()));
}

TEST(OrientationTests, gyro_chained)
{
  auto orientation = Orientation<double, 3>{};
  orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, 1.0);
  orientation.integrate(Vector3d{0, 0, 0.5 * M_PI}, 1.0);

  EXPECT_TRUE(MatricesEqual((Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitX()) *
                             Eigen::AngleAxisd(0.5 * M_PI, Vector3d::UnitZ())).matrix(),
                            orientation.getQuaternion().matrix()));
}

TEST(OrientationTests, merged_no_error)
{
  auto orientation = Orientation<double, 3>{};
  auto q0 = orientation.getQuaternion();
  orientation.integrate(Vector3d::Zero(), 1.0, Vector3d{0.0, 0.0, 1.0}, 0.1);

  EXPECT_TRUE(VectorsEqual(q0.coeffs(), orientation.getQuaternion().coeffs()));
}

TEST(OrientationTests, merged_turned_no_error)
{
  auto orientation = Orientation<double, 3>{};
  // Turn 90 deg around x axis
  orientation.integrate(Vector3d{0.5 * M_PI, 0, 0}, 1.0);
  auto q1 = orientation.getQuaternion();
  // Gravity is now along y axis
  orientation.integrate(Vector3d::Zero(), 1.0, Vector3d{0.0, 1.0, 0.0}, 0.1);

  EXPECT_TRUE(VectorsEqual(q1.coeffs(), orientation.getQuaternion().coeffs()));
}

TEST(OrientationTests, merged_90deg_error)
{
  auto orientation = Orientation<double, 3>{};

  // Gravity is now along y axis
  orientation.integrate(Vector3d::Zero(), 1.0, Vector3d{0.0, 1.0, 0.0}, 0.1);

  EXPECT_FLOAT_EQ(1.0, orientation.getQuaternion().norm());
  EXPECT_LT(0.0, orientation.getQuaternion().x());

  // Test convergence after 20 seconds
  for (unsigned i = 0; i < 10000; ++i)
    orientation.integrate(Vector3d::Zero(), 1.0 / 500, Vector3d{0.0, 1.0, 0.0}, 0.1);

  // Quaternion element is sin(theta / 2)
  EXPECT_NEAR(sin(.25 * M_PI), orientation.getQuaternion().coeffs().x(), 0.00002);
}
