// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "vision/Camera/ImageFeedCamera/imagefeedcamera.hh"
#include "util/memory.hh"

#include <chrono>
#include <iostream>

using namespace bold::vision;

TEST(ImageFeedCameraTests, capture)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-yuv.png");
  camera->open();
  camera->start();
  auto img = camera->capture();

  EXPECT_EQ( 640, img.cols );
  EXPECT_EQ( 480, img.rows );
  
  camera->stop();
}

TEST(ImageFeedCameraTests, noSuchFile)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-foo.png");
  EXPECT_THROW(camera->open(), std::runtime_error);
}

TEST(ImageFeedCameraTests, requestSize)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-yuv.png");
  camera->open();
  camera->requestSize(320, 240);
  camera->start();
  auto img = camera->capture();

  EXPECT_EQ( 320, img.cols );
  EXPECT_EQ( 240, img.rows );
  
  camera->stop();
}

TEST(ImageFeedCameraTests, requestSizeBeforeOpen)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-yuv.png");
  camera->requestSize(320, 240);
  camera->open();
  camera->start();
  auto img = camera->capture();

  EXPECT_EQ( 320, img.cols );
  EXPECT_EQ( 240, img.rows );
  
  camera->stop();
}

TEST(ImageFeedCameraTests, DISABLED_fps100)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-yuv.png");
  camera->requestSize(320, 240);
  camera->open();
  camera->setFPS(100);
  camera->start();

  auto start = std::chrono::steady_clock::now();
  for (unsigned i = 0; i < 10; ++i)
    camera->capture();
  auto end = std::chrono::steady_clock::now();

  auto elapsedSeconds = std::chrono::duration<double>(end - start);
  
  EXPECT_LT( 0.1, elapsedSeconds.count() );
  EXPECT_GT( 0.11, elapsedSeconds.count() );
  camera->stop();
}

TEST(ImageFeedCameraTests, editImage)
{
  auto camera = std::make_unique<ImageFeedCamera>("../resources/test-yuv.png");
  camera->requestSize(320, 240);
  camera->open();
  camera->start();
  auto img = camera->capture();
  auto pixelBefore = img.at<uint8_t>(0);
  img.at<uint8_t>(0) = pixelBefore + 1;

  auto img2 = camera->capture();
  EXPECT_EQ(pixelBefore, img2.at<uint8_t>(0));
}
