// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <vector>

#include "src/util/ConsumerQueueThread/consumerqueuethread.hh"
#include "Clock/clock.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

TEST (ConsumerQueueThreadTests, basics)
{
  vector<int> values;

  auto testThreadId = this_thread::get_id();

  ConsumerQueueThread<int> queue("Test queue", [&values,&testThreadId](int i)
  {
    ASSERT_NE( testThreadId, this_thread::get_id() ) << "Should be on a different thread";
    values.push_back(i);
  });

  auto loopCount = unsigned{100};

  for (auto i = unsigned{0}; i < loopCount; i++)
  {
    queue.push(i);
    if (i % 20 == 0)
      this_thread::sleep_for(chrono::milliseconds(5));
  }

  queue.stop(); // joins

  ASSERT_TRUE( values.size() > 0 );
  ASSERT_TRUE( values.size() <= loopCount );

  for (auto i = unsigned{0}; i < values.size(); i++)
  {
    ASSERT_EQ ( i, values[i] );
  }
}

TEST (ConsumerQueueThreadTests, DISABLED_doesntBlockPusher)
{
  int callCount = 0;
  auto clock = make_shared<SystemClock>();

  ConsumerQueueThread<int> queue("Test queue", [&](int i)
  {
    this_thread::sleep_for(chrono::milliseconds(1));
    callCount++;
  });

  int loopCount = 50;

  for (auto i = 0; i < loopCount; i++)
    queue.push(1);

  ASSERT_LT ( callCount, loopCount );
  auto start = clock->getTimestamp();
  this_thread::sleep_for(chrono::milliseconds(loopCount / 4));
  ASSERT_GT ( callCount, 0 );
  ASSERT_LT ( callCount, loopCount ) << "Slept for " << clock->getMillisSince(start) << " ms";

  start = clock->getTimestamp();
  while (callCount < loopCount)
  {
    if (clock->getSecondsSince(start) > 1.0)
      FAIL() << "Timed out waiting for all items to complete";
    this_thread::sleep_for(chrono::milliseconds(1));
  }

  ASSERT_EQ ( loopCount, callCount );

  queue.stop(); // joins
}
