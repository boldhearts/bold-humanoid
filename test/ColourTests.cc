// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "colour/BGR/bgr.hh"
#include "colour/HSV/hsv.hh"
#include "colour/YCbCr/ycbcr.hh"
#include "colour/HSVRange/hsvrange.hh"

using namespace bold;

TEST (ColourTests, bgr2hsv)
{
  colour::BGR red(0,0,255);
  colour::BGR blue(255,0,0);
  colour::BGR green(0,255,0);
  colour::BGR yellow(0,255,255);
  colour::BGR yellow1(0,255,254);
  colour::BGR yellow2(0,254,255);
  colour::BGR cyan(255,255,0);
  colour::BGR magenta(255,0,255);
  colour::BGR gray(128,128,128);
  colour::BGR black(0,0,0);
  colour::BGR white(255,255,255);

  EXPECT_EQ ( colour::HSV(0, 255, 255), red.toHSV() );
  EXPECT_EQ ( colour::HSV(170, 255, 255), blue.toHSV() );
  EXPECT_EQ ( colour::HSV(85, 255, 255), green.toHSV() );
  EXPECT_EQ ( colour::HSV(43, 255, 255), yellow.toHSV() );
  // Since yellow should have hue 85/2=42.5, which is rounded to 43,
  // small deviation towards g (increase of hue) should map to the
  // same hue as yellow, whereas small deviation towards red should
  // decrease hue
  EXPECT_EQ ( yellow.toHSV(), yellow1.toHSV() );
  EXPECT_EQ ( yellow.toHSV().h - 1, yellow2.toHSV().h );
  EXPECT_EQ ( colour::HSV(128 /*127.5*/, 255, 255), cyan.toHSV() );
  // This gives hue=212, should give hue=213, but any attempt to get it to that breaks other tests
  EXPECT_EQ ( colour::HSV(212 /*212.5*/, 255, 255), magenta.toHSV() );
  EXPECT_EQ ( colour::HSV(0, 0, 128), gray.toHSV() );
  EXPECT_EQ ( colour::HSV(0, 0, 0), black.toHSV() );
  EXPECT_EQ ( colour::HSV(0, 0, 255), white.toHSV() );

  // These are a series of RGB -> HSV conversions according to Gimp
  // The numbers shown in comments come from Gimp, and our values (which are different)
  // are shown in the 'expected' values

  // There are some differences between the values calculated from Gimp and those
  // from our algorithm, but larger than 2/255 in any one channel

  // Gimp says 259,62,84 (HSV) -> 183,158,214
  EXPECT_EQ ( colour::HSV(183, 159, 213), colour::BGR(213,80,123).toHSV() );
  // Gimp says 341,62,84 (HSV) -> 241,158,214
  EXPECT_EQ ( colour::HSV(242, 159, 213), colour::BGR(123,80,213).toHSV() );
  // Gimp says 23,100,78 (HSV) -> 16,255,198
  EXPECT_EQ ( colour::HSV( 16, 255, 200), colour::BGR(0,78,200).toHSV() );
  // Gimp says 212,92,77 (HSV) -> 150,234,196
  EXPECT_EQ ( colour::HSV(150, 235, 196), colour::BGR(196,100,15).toHSV() );
  // Gimp says 162,57,78 (HSV) -> 114,145,198
  EXPECT_EQ ( colour::HSV(115, 145, 200), colour::BGR(166,200,86).toHSV() );
  // Gimp says 351,100,93 (HSV) -> 247,255,237
  EXPECT_EQ ( colour::HSV(249, 255, 236), colour::BGR(36,0,236).toHSV() );
}

TEST (ColourTests, hsv2bgr)
{
  colour::HSV red(0,255,255);
  colour::HSV green(85,255,255);
  colour::HSV blue(170,255,255);
  colour::HSV yellow1(42,255,255);
  colour::HSV yellow2(43,255,255);
  colour::HSV cyan1(127,255,255);
  colour::HSV cyan2(128,255,255);
  colour::HSV magenta1(212,255,255);
  colour::HSV magenta2(213,255,255);
  colour::HSV gray(0,0,128);
  colour::HSV black(0,0,0);
  colour::HSV white(0,0,255);

  EXPECT_EQ ( colour::BGR(0,0,255), red.toBGR() );
  EXPECT_EQ ( colour::BGR(255,0,0), blue.toBGR() );
  EXPECT_EQ ( colour::BGR(0,255,0), green.toBGR() );
  EXPECT_EQ ( colour::BGR(0,252,255), yellow1.toBGR() );
  EXPECT_EQ ( colour::BGR(0,255,252), yellow2.toBGR() );
  EXPECT_EQ ( colour::BGR(252,255,0), cyan1.toBGR() );
  EXPECT_EQ ( colour::BGR(255,252,0), cyan2.toBGR() );
  EXPECT_EQ ( colour::BGR(255,0,252), magenta1.toBGR() );
  EXPECT_EQ ( colour::BGR(252,0,255), magenta2.toBGR() );
  EXPECT_EQ ( colour::BGR(128,128,128), gray.toBGR() );
  EXPECT_EQ ( colour::BGR(0,0,0), black.toBGR() );
  EXPECT_EQ ( colour::BGR(255,255,255), white.toBGR() );
}

TEST (ColourTests, hsv2bgr2hsv)
{
  colour::HSV red(0,255,255);
  colour::HSV green(85,255,255);
  colour::HSV blue(170,255,255);
  colour::HSV yellow1(42,255,255);
  colour::HSV yellow2(43,255,255);
  colour::HSV cyan1(127,255,255);
  colour::HSV cyan2(128,255,255);
  colour::HSV magenta1(212,255,255);
  colour::HSV magenta2(213,255,255);
  colour::HSV gray(0,0,128);
  colour::HSV black(0,0,0);
  colour::HSV white(0,0,255);
  colour::HSV reddish(254,255,255);

  EXPECT_EQ ( red, red.toBGR().toHSV() );
  EXPECT_EQ ( blue, blue.toBGR().toHSV() );
  EXPECT_EQ ( green, green.toBGR().toHSV() );
  EXPECT_EQ ( yellow1, yellow1.toBGR().toHSV() );
  EXPECT_EQ ( yellow2, yellow2.toBGR().toHSV() );
  EXPECT_EQ ( cyan1, cyan1.toBGR().toHSV() );
  EXPECT_EQ ( cyan2, cyan2.toBGR().toHSV() );
  EXPECT_EQ ( magenta1, magenta1.toBGR().toHSV() );
  EXPECT_EQ ( magenta2, magenta2.toBGR().toHSV() );
  EXPECT_EQ ( gray, gray.toBGR().toHSV() );
  EXPECT_EQ ( black, black.toBGR().toHSV() );
  EXPECT_EQ ( white, white.toBGR().toHSV() );
  EXPECT_EQ ( reddish, reddish.toBGR().toHSV() );
}

TEST(ColourTests, ycbcr2bgr)
{
  EXPECT_EQ( colour::YCbCr(0, 128, 128).toBGRFloat(), colour::YCbCr(0, 128, 128).toBGRInt());
  EXPECT_EQ( colour::YCbCr(128, 0, 0).toBGRFloat(), colour::YCbCr(128, 0, 0).toBGRInt());
  EXPECT_EQ( colour::YCbCr(128, 128, 128).toBGRFloat(), colour::YCbCr(128, 128, 128).toBGRInt());
  EXPECT_EQ( colour::YCbCr(128, 255, 255).toBGRFloat(), colour::YCbCr(128, 255, 255).toBGRInt());
  EXPECT_EQ( colour::YCbCr(255, 128, 128).toBGRFloat(), colour::YCbCr(255, 128, 128).toBGRInt() );

  EXPECT_EQ( colour::BGR(0, 0, 0), colour::YCbCr(0, 128, 128).toBGRInt() );
  EXPECT_EQ( colour::BGR(128, 128, 128), colour::YCbCr(128, 128, 128).toBGRInt() );
  EXPECT_EQ( colour::BGR(255, 255, 255), colour::YCbCr(255, 128, 128).toBGRInt() );
}

TEST (ColourTests, hsvRange)
{
  colour::HSVRange rangeAll(0, 255, 0, 255, 0, 255);

  EXPECT_TRUE ( rangeAll.contains(colour::HSV(0, 0, 0)) );
  EXPECT_TRUE ( rangeAll.contains(colour::HSV(128, 0, 0)) );
  EXPECT_TRUE ( rangeAll.contains(colour::HSV(255, 0, 0)) );
  EXPECT_TRUE ( rangeAll.contains(colour::HSV(0, 128, 128)) );
  EXPECT_TRUE ( rangeAll.contains(colour::HSV(128, 255, 128)) );
  EXPECT_TRUE ( rangeAll.contains(colour::HSV(255, 128, 255)) );

  colour::HSVRange rangeOne(0, 0, 0, 0, 0, 0);

  EXPECT_TRUE  ( rangeOne.contains(colour::HSV(0, 0, 0)) );
  EXPECT_FALSE ( rangeOne.contains(colour::HSV(128, 0, 0)) );
  EXPECT_FALSE ( rangeOne.contains(colour::HSV(255, 0, 0)) );
  EXPECT_FALSE ( rangeOne.contains(colour::HSV(0, 128, 128)) );
  EXPECT_FALSE ( rangeOne.contains(colour::HSV(128, 255, 128)) );
  EXPECT_FALSE ( rangeOne.contains(colour::HSV(255, 128, 255)) );

  colour::HSVRange rangeHWrap(235, 20, 0, 255, 0, 255);

  EXPECT_TRUE  (rangeHWrap.contains(colour::HSV(0,128,128)) );
  EXPECT_TRUE  (rangeHWrap.contains(colour::HSV(20,128,128)) );
  EXPECT_FALSE (rangeHWrap.contains(colour::HSV(21,128,128)) );
  EXPECT_TRUE  (rangeHWrap.contains(colour::HSV(235,128,128)) );
  EXPECT_FALSE (rangeHWrap.contains(colour::HSV(234,128,128)) );

  colour::HSVRange rangeSRangeOutOfBounds(118, 138, 100, 255, 0, 255);

  EXPECT_TRUE  (rangeSRangeOutOfBounds.contains(colour::HSV(128,200,128)) );
  EXPECT_TRUE  (rangeSRangeOutOfBounds.contains(colour::HSV(128,255,128)) );
  EXPECT_TRUE  (rangeSRangeOutOfBounds.contains(colour::HSV(128,100,128)) );
  EXPECT_FALSE (rangeSRangeOutOfBounds.contains(colour::HSV(128,99,128)) );
  EXPECT_FALSE (rangeSRangeOutOfBounds.contains(colour::HSV(128,0,128)) );

  EXPECT_EQ(127, colour::HSVRange(0, 255, 0,0, 0,0).getHMid());
  EXPECT_EQ(5,   colour::HSVRange(0, 10, 0,0, 0,0).getHMid());
  EXPECT_EQ(255, colour::HSVRange(250, 5, 0,0, 0,0).getHMid());
  EXPECT_EQ(5,   colour::HSVRange(250, 15, 0,0, 0,0).getHMid());

  EXPECT_EQ(0,   colour::HSVRange(0,0, 0,0, 0,0).getSMid());
  EXPECT_EQ(127, colour::HSVRange(0,0, 0,255, 0,0).getSMid());

  EXPECT_EQ(0,   colour::HSVRange(0,0, 0,0, 0,0).getVMid());
  EXPECT_EQ(127, colour::HSVRange(0,0, 0,0, 0,255).getVMid());
}

TEST (ColourTests, hsvRangeContaining )
{
  auto range = colour::HSVRange(128,129, 128,128, 128,128);

  EXPECT_EQ ( colour::HSVRange(64,129, 128,128, 128,128), range.containing(colour::HSV(64, 128, 128)) );
  EXPECT_EQ ( colour::HSVRange(128,192, 128,128, 128,128), range.containing(colour::HSV(192, 128, 128)) );
  EXPECT_EQ ( colour::HSVRange(128,129, 64,128, 128,128), range.containing(colour::HSV(128, 64, 128)) );
  EXPECT_EQ ( colour::HSVRange(128,129, 128,192, 128,128), range.containing(colour::HSV(128, 192, 128)) );
  EXPECT_EQ ( colour::HSVRange(128,129, 128,128, 64,128), range.containing(colour::HSV(128, 128, 64)) );
  EXPECT_EQ ( colour::HSVRange(128,129, 128,128, 128,192), range.containing(colour::HSV(128, 128, 192)) );

  range = colour::HSVRange(0,1, 128,128, 128,128);
  EXPECT_EQ ( colour::HSVRange(0,64, 128,128, 128,128), range.containing(colour::HSV(64, 128, 128)) );
  EXPECT_EQ ( colour::HSVRange(0,128, 128,128, 128,128), range.containing(colour::HSV(128, 128, 128)) );
  EXPECT_EQ ( colour::HSVRange(192,1, 128,128, 128,128), range.containing(colour::HSV(192, 128, 128)) );

  range = colour::HSVRange(128,128, 128,128, 128,128);
  EXPECT_EQ ( colour::HSVRange(64,128, 128,128, 128,128), range.containing(colour::HSV(64, 128, 128)) );
  EXPECT_EQ ( colour::HSVRange(128,192, 128,128, 128,128), range.containing(colour::HSV(192, 128, 128)) );

}
