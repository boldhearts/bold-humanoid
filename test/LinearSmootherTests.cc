// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "Smoother/LinearSmoother/linearsmoother.hh"

using namespace std;
using namespace bold;

TEST (LinearSmootherTests, basics)
{
  LinearSmoother s(0, 5);

  EXPECT_EQ ( 0, s.getCurrent() );
  EXPECT_EQ ( 0, s.getTarget() );
  EXPECT_EQ ( 0, s.getNext() );
  EXPECT_EQ ( 0, s.getNext() );
  EXPECT_EQ ( 0, s.getNext() );

  s.setTarget(22);

  EXPECT_EQ ( 5, s.getNext() );
  EXPECT_EQ ( 10, s.getNext() );
  EXPECT_EQ ( 15, s.getNext() );
  EXPECT_EQ ( 20, s.getNext() );
  EXPECT_EQ ( 22, s.getNext() );
  EXPECT_EQ ( 22, s.getNext() );
  EXPECT_EQ ( 22, s.getNext() );

  s.setTarget(-22);

  EXPECT_EQ ( 17, s.getNext() );
  EXPECT_EQ ( 12, s.getNext() );
  EXPECT_EQ ( 7, s.getNext() );
  EXPECT_EQ ( 2, s.getNext() );
  EXPECT_EQ ( -3, s.getNext() );
  EXPECT_EQ ( -8, s.getNext() );
  EXPECT_EQ ( -13, s.getNext() );
  EXPECT_EQ ( -18, s.getNext() );
  EXPECT_EQ ( -22, s.getNext() );
  EXPECT_EQ ( -22, s.getNext() );
  EXPECT_EQ ( -22, s.getNext() );

  s.setTarget(-21);

  EXPECT_EQ ( -21, s.getNext() );
  EXPECT_EQ ( -21, s.getNext() );
}
