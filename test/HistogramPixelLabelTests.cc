// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "colour/HSV/hsv.hh"
#include "vision/PixelLabel/HistogramPixelLabel/histogrampixellabel.hh"

#include <fstream>

using namespace std;
using namespace bold;
using namespace bold::vision;
using namespace Eigen;

TEST (HistogramPixelLabelTests, binSize)
{
  auto label1 = new HistogramPixelLabel<8>{"label1", LabelClass::BALL};
  EXPECT_EQ ( 1, label1->getBinSize() );
  EXPECT_EQ ( 256, label1->getNBins() );

  auto label2 = new HistogramPixelLabel<7>{"label2", LabelClass::BALL};
  EXPECT_EQ ( 2, label2->getBinSize() );
  EXPECT_EQ ( 128, label2->getNBins() );

  auto label3 = new HistogramPixelLabel<6>{"label3", LabelClass::BALL};
  EXPECT_EQ ( 4, label3->getBinSize() );
  EXPECT_EQ ( 64, label3->getNBins() );

  auto label4 = new HistogramPixelLabel<1>{"label4", LabelClass::BALL};
  EXPECT_EQ ( 128, label4->getBinSize() );
  EXPECT_EQ ( 2, label4->getNBins() );

  delete label1;
  delete label2;
  delete label3;
  delete label4;
}

TEST (HistogramPixelLabelTests, index)
{
  auto label = new HistogramPixelLabel<8>{"label", LabelClass::BALL};
  EXPECT_EQ ( colour::HSV(0, 0, 0), label->indexToHsv(0) );
  EXPECT_EQ ( colour::HSV(0, 0, 1), label->indexToHsv(1) );
  EXPECT_EQ ( colour::HSV(0, 1, 0), label->indexToHsv(256) );
  EXPECT_EQ ( colour::HSV(0, 1, 1), label->indexToHsv(257) );
  EXPECT_EQ ( colour::HSV(1, 0, 0), label->indexToHsv(65536) );

  EXPECT_EQ ( 0, label->hsvToIndex(0, 0, 0) );
  EXPECT_EQ ( 1, label->hsvToIndex(0, 0, 1) );
  EXPECT_EQ ( 256, label->hsvToIndex(0, 1, 0) );
  EXPECT_EQ ( 257, label->hsvToIndex(0, 1, 1) );
  EXPECT_EQ ( 65536, label->hsvToIndex(1, 0, 0) );

  delete label;
}

TEST (HistogramPixelLabelTests, empty)
{
  auto label = new HistogramPixelLabel<8>{"label", LabelClass::BALL};
  EXPECT_EQ ( 1.0f / (256 * 256 * 256), label->labelProb(colour::HSV(0, 0, 0)) );
  delete label;
}

TEST (HistogramPixelLabelTests, readWrite)
{
  auto label1 = new HistogramPixelLabel<6>{"label", LabelClass::BALL};
  for (unsigned i = 0; i < 100; ++i)
    label1->addSample(colour::HSV(128, 64, 224));

  ofstream out("/tmp/label.dat");
  label1->write(out);
  out.close();

  auto label2 = new HistogramPixelLabel<6>{"label", LabelClass::BALL};
  ifstream in("/tmp/label.dat");
  label2->read(in);
  in.close();

  EXPECT_EQ ( label1->getTotalCount(), label2->getTotalCount() );
  EXPECT_EQ ( label1->modalColour(), label2->modalColour() );

  delete label1;
  delete label2;
}

