// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "UDPSocket/udpsocket.hh"

#include <netinet/in.h>

using namespace bold;
using namespace std;

TEST (UDPSocketTests, DISABLED_communication)
{
  int receiverPort = 8765;
  int senderPort = 5678;

  UDPSocket sender;
  EXPECT_TRUE(sender.setBlocking(false));
  EXPECT_TRUE(sender.setBroadcast(true));
  EXPECT_TRUE(sender.setTarget("255.255.255.255", receiverPort));
  EXPECT_TRUE(sender.bind(senderPort));

  UDPSocket receiver;
  EXPECT_TRUE(receiver.setBlocking(true));
  EXPECT_TRUE(receiver.bind(receiverPort));

  EXPECT_TRUE(sender.send("Hello"));

  sockaddr_in from;
  int len = sizeof(from);
  char packet[100] = {0};
  int bytesRead = receiver.receiveFrom(packet, sizeof(packet), &from, &len);

  EXPECT_EQ(string("Hello").length(), bytesRead);
  EXPECT_EQ("Hello", string(packet));

  // use 'from' to send a message back to the originator
  receiver.setTarget(from);

  sender.setBlocking(true);

  EXPECT_TRUE(receiver.send("Hello yourself"));

  memset(packet, 0, sizeof(packet));

  bytesRead = sender.receive(packet, sizeof(packet));

  EXPECT_EQ(string("Hello yourself").length(), bytesRead);
  EXPECT_EQ("Hello yourself", string(packet));
}

TEST (UDPSocketTests, DISABLED_broadcastLoopback)
{
  int port = 8765;

  UDPSocket socket;

  EXPECT_TRUE(socket.setBlocking(true));
  EXPECT_TRUE(socket.setBroadcast(true));
  EXPECT_TRUE(socket.setTarget("255.255.255.255", port));
  EXPECT_TRUE(socket.bind(port));

  EXPECT_TRUE(socket.send("Hello"));

  char packet[100] = {0};
  int bytesRead = socket.receive(packet, sizeof(packet));

  EXPECT_EQ(string("Hello").length(), bytesRead);
  EXPECT_EQ("Hello", string(packet));
}
