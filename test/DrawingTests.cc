// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "Drawing/drawing.hh"
#include <Eigen/Geometry>

using namespace bold;
using namespace bold::geometry2;
using namespace Eigen;

TEST(DrawingTests, circle)
{
  auto draw = Draw{};

  draw.circle(Frame::Camera,
              Point2d{10.0, 10.0}, 5.0,
              colour::BGR{30, 60, 90}, 0.5,
              2.0);

  /*
  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& circle = static_cast<CircleDrawing const&>(*items[0]);

  EXPECT_EQ(Point2d(10.0, 10.0), circle.centre);
  EXPECT_EQ(5.0, circle.radius);
  EXPECT_EQ(colour::BGR(30, 60, 90), circle.strokeColour);
  EXPECT_EQ(0.5, circle.strokeAlpha);
  EXPECT_EQ(2.0, circle.lineWidth);
   */
}

TEST(DrawingTests, fillCircle)
{
  auto draw = Draw{};

  draw.fillCircle(Frame::Camera,
                  Point2d{10.0, 10.0}, 5.0,
                  colour::BGR{30, 60, 90}, 0.5,
                  colour::BGR{60, 90, 30}, 0.75,
                  2.0);

  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& circle = static_cast<CircleDrawing const&>(*items[0]);

  EXPECT_EQ(Point2d(10.0, 10.0), circle.centre);
  EXPECT_EQ(5.0, circle.radius);
  EXPECT_EQ(colour::BGR(30, 60, 90), circle.fillColour);
  EXPECT_EQ(0.5, circle.fillAlpha);
  EXPECT_EQ(colour::BGR(60, 90, 30), circle.strokeColour);
  EXPECT_EQ(0.75, circle.strokeAlpha);
  EXPECT_EQ(2.0, circle.lineWidth);
}

TEST(DrawingTests, lineSegment)
{
  auto draw = Draw{};

  draw.line(Frame::Camera,
            LineSegment2d{Point2d{10.0, 10.0}, Point2d{20.0, 20.0}},
            colour::BGR{30, 60, 90}, 0.5,
            2.0);

  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& lineDrawing = static_cast<LineDrawing const&>(*items[0]);

  EXPECT_EQ(Point2d(10.0, 10.0), lineDrawing.line.p1());
  EXPECT_EQ(Point2d(20.0, 20.0), lineDrawing.line.p2());
  EXPECT_EQ(colour::BGR(30, 60, 90), lineDrawing.colour);
  EXPECT_EQ(0.5, lineDrawing.alpha);
  EXPECT_EQ(2.0, lineDrawing.lineWidth);
}

TEST(DrawingTests, lineAtAngle)
{
  auto draw = Draw{};

  draw.lineAtAngle(Frame::Camera,
                   Point2d{10.0, 10.0}, .25 * M_PI, std::sqrt(200.0),
                   colour::BGR{30, 60, 90}, 0.5,
                   2.0);

  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& lineDrawing = static_cast<LineDrawing const&>(*items[0]);

  EXPECT_EQ(Point2d(10.0, 10.0), lineDrawing.line.p1());
  EXPECT_EQ(Point2d(20.0, 20.0), lineDrawing.line.p2());
  EXPECT_EQ(colour::BGR(30, 60, 90), lineDrawing.colour);
  EXPECT_EQ(0.5, lineDrawing.alpha);
  EXPECT_EQ(2.0, lineDrawing.lineWidth);
}

TEST(DrawingTests, polygon)
{
  auto draw = Draw{};

  auto poly = Polygon2d{AlignedBox2d{Vector2d{10, 10}, Vector2d{20, 20}}};
  
  draw.polygon(Frame::Camera,
               poly,
               colour::BGR{30, 60, 90}, 0.5,
               2.0);

  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& polygon = static_cast<PolygonDrawing const&>(*items[0]);

  EXPECT_EQ(colour::BGR(30, 60, 90), polygon.strokeColour);
  EXPECT_EQ(0.5, polygon.strokeAlpha);
  EXPECT_EQ(2.0, polygon.lineWidth);
}

TEST(DrawingTests, fillPolygon)
{
  auto draw = Draw{};

  auto poly = Polygon2d{AlignedBox2d{Vector2d{10, 10}, Vector2d{20, 20}}};
  
  draw.fillPolygon(Frame::Camera,
                   poly,
                   colour::BGR{30, 60, 90}, 0.5,
                   colour::BGR{60, 90, 30}, 0.75,
                   2.0);

  auto const& items = draw.getDrawingItems();

  EXPECT_EQ(1, items.size());

  auto const& polygon = static_cast<PolygonDrawing const&>(*items[0]);

  EXPECT_EQ(colour::BGR(30, 60, 90), polygon.fillColour);
  EXPECT_EQ(0.5, polygon.fillAlpha);
  EXPECT_EQ(colour::BGR(60, 90, 30), polygon.strokeColour);
  EXPECT_EQ(0.75, polygon.strokeAlpha);
  EXPECT_EQ(2.0, polygon.lineWidth);
}
