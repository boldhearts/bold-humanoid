// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "helpers.hh"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "vision/CameraModel/cameramodel.hh"

using namespace bold;
using namespace bold::vision;
using namespace Eigen;

TEST (CameraModelTests, directionForPixel)
{
  auto imageWidth = 11;
  auto imageHeight = 11;
  auto rangeVerticalDegs = 90;
  auto rangeHorizontalDegs = 90;

  CameraModel cameraModel1(imageWidth, imageHeight,
                           rangeVerticalDegs, rangeHorizontalDegs);

  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1, 0).normalized(),
                             cameraModel1.directionForPixel(Vector2d(5.5, 5.5)) ) );

  EXPECT_TRUE ( VectorsEqual(Vector3d(1, 1, 0).normalized(),
                             cameraModel1.directionForPixel(Vector2d( 0, 5.5)) ) );
  EXPECT_TRUE ( VectorsEqual(Vector3d(-1, 1, 0).normalized(),
                             cameraModel1.directionForPixel(Vector2d(11, 5.5)) ) );

  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1, -1).normalized(),
                             cameraModel1.directionForPixel(Vector2d(5.5,  0)) ) );
  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1,  1).normalized(),
                             cameraModel1.directionForPixel(Vector2d(5.5, 11)) ) );

  auto v = Vector3d( -1, 1, 0).normalized();
  v.x() *= 2.0/5.5;
  EXPECT_TRUE ( VectorsEqual(v.normalized(),
                             cameraModel1.directionForPixel(Vector2d(7.5,  5.5)) ) );

  rangeVerticalDegs = 45;
  rangeHorizontalDegs = 60;

  double th = tan(.5 * 60.0 / 180.0 * M_PI);
  double tv = tan(.5 * 45.0 / 180.0 * M_PI);

  CameraModel cameraModel2(imageWidth, imageHeight,
                           rangeVerticalDegs, rangeHorizontalDegs);

  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1, 0).normalized(),
                             cameraModel2.directionForPixel(Vector2d(5.5, 5.5)) ) );

  EXPECT_TRUE ( VectorsEqual(Vector3d(th, 1, 0).normalized(),
                             cameraModel2.directionForPixel(Vector2d( 0, 5.5)) ) );
  EXPECT_TRUE ( VectorsEqual(Vector3d(-th, 1, 0).normalized(),
                             cameraModel2.directionForPixel(Vector2d(11, 5.5)) ) );

  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1, -tv).normalized(),
                             cameraModel2.directionForPixel(Vector2d(5.5,  0)) ) );
  EXPECT_TRUE ( VectorsEqual(Vector3d(0, 1,  tv).normalized(),
                             cameraModel2.directionForPixel(Vector2d(5.5, 11)) ) );

}

TEST (CameraModelTests, pixelForDirection)
{
  auto imageWidth = 11;
  auto imageHeight = 11;
  auto rangeVerticalDegs = 90;
  auto rangeHorizontalDegs = 90;

  CameraModel cameraModel1(imageWidth, imageHeight, rangeVerticalDegs, rangeHorizontalDegs);

  EXPECT_EQ ( util::Maybe<Vector2d>::empty(), cameraModel1.pixelForDirection(Vector3d(0,  0, 0)) );
  EXPECT_EQ ( util::Maybe<Vector2d>::empty(), cameraModel1.pixelForDirection(Vector3d(1,  0, 0)) );
  EXPECT_EQ ( util::Maybe<Vector2d>::empty(), cameraModel1.pixelForDirection(Vector3d(0,  0, 1)) );
  EXPECT_EQ ( util::Maybe<Vector2d>::empty(), cameraModel1.pixelForDirection(Vector3d(1, -1, 1)) );

  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5, 5.5), *cameraModel1.pixelForDirection(Vector3d(0, 1, 0)) ) );

  EXPECT_TRUE ( VectorsEqual( Vector2d( 0, 5.5), *cameraModel1.pixelForDirection(Vector3d( 1, 1, 0)) ) );
  EXPECT_TRUE ( VectorsEqual( Vector2d(11, 5.5), *cameraModel1.pixelForDirection(Vector3d(-1, 1, 0)) ) );

  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5,  0), *cameraModel1.pixelForDirection(Vector3d(0, 1, -1)) ) );
  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5, 11), *cameraModel1.pixelForDirection(Vector3d(0, 2,  2)) ) );

  auto v = Vector3d(-1, 1, 0).normalized();
  v.x() *= 2.0/5.5;
  EXPECT_TRUE ( VectorsEqual( Vector2d(7.5, 5.5), *cameraModel1.pixelForDirection(v) ) );

  // 2 time range
  rangeVerticalDegs = 45;
  rangeHorizontalDegs = 60;

  CameraModel cameraModel2(imageWidth, imageHeight, rangeVerticalDegs, rangeHorizontalDegs);

  double th = tan(.5 * 60.0 / 180.0 * M_PI);
  double tv = tan(.5 * 45.0 / 180.0 * M_PI);

  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5, 5.5), *cameraModel2.pixelForDirection(Vector3d(0, 1, 0)) ) );

  EXPECT_TRUE ( VectorsEqual( Vector2d( 0, 5.5), *cameraModel2.pixelForDirection(Vector3d(th, 1, 0)) ) );
  EXPECT_TRUE ( VectorsEqual( Vector2d(11, 5.5), *cameraModel2.pixelForDirection(Vector3d(-th, 1, 0)) ) );

  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5,  0), *cameraModel2.pixelForDirection(Vector3d(0, 1, -tv)) ) );
  EXPECT_TRUE ( VectorsEqual( Vector2d(5.5, 11), *cameraModel2.pixelForDirection(Vector3d(0, 1, tv)) ) );
}
