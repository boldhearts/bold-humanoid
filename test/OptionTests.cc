// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "Option/CircleBall/circleball.hh"

using namespace bold;

TEST(OptionTests, CircleBall_new)
{
  auto option1 = new CircleBall{"circleball", nullptr, nullptr, nullptr, nullptr};
  auto option2 = new CircleBall{"circleball", nullptr, nullptr, nullptr, nullptr};
  auto option3 = new CircleBall{"circleball", nullptr, nullptr, nullptr, nullptr};
  auto option4 = new CircleBall{"circleball", nullptr, nullptr, nullptr, nullptr};
  delete option1;
  delete option2;
  delete option3;
  delete option4;
}

TEST(OptionTests, CircleBall_make)
{
  auto option1 = Option::make<CircleBall>("circleball", nullptr, nullptr, nullptr, nullptr);
  auto option2 = Option::make<CircleBall>("circleball", nullptr, nullptr, nullptr, nullptr);
  auto option3 = Option::make<CircleBall>("circleball", nullptr, nullptr, nullptr, nullptr);
  auto option4 = Option::make<CircleBall>("circleball", nullptr, nullptr, nullptr, nullptr);
}
