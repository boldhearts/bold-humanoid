// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "config/SettingReader/settingreader.hh"
#include "config/SettingBase/Setting/setting-implementations.hh"

#include <map>

using namespace bold;
using namespace bold::config;
using namespace std;

TEST(SettingReaderTests, IntSetting)
{
  auto reader = SettingReader{};
  auto setting = IntSetting{"foo.bar", 0, 10, false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": 6}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( 6, setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": 7}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( 7, setting.getValue() );
}

TEST(SettingReaderTests, EnumSetting)
{
  auto reader = SettingReader{};
  auto m = map<int, string>{};
  m[0] = "foo";
  m[1] = "bar";
  auto setting = EnumSetting{"foo.bar", m, false, "foobar"};
  auto doc = rapidjson::Document{};
  
  doc.Parse("{\"value\": 1}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( 1, setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": 0}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( 0, setting.getValue() );
}

TEST(SettingReaderTests, DoubleSetting)
{
  auto reader = SettingReader{};
  auto setting = DoubleSetting{"foo.bar", 0, 10, false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": 6.0}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( 6.0, setting.getValue() );

  doc.Parse("{\"value\": 5}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( 5.0, setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": 7.0}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( 7.0, setting.getValue() );
}

TEST(SettingReaderTests, BoolSetting)
{
  auto reader = SettingReader{};
  auto setting = BoolSetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": true}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( true, setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": false}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( false, setting.getValue() );
}

TEST(SettingReaderTests, HsvRangeSetting)
{
  auto reader = SettingReader{};
  auto setting = HsvRangeSetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": {\"hue\":[0,10],\"sat\":[1,11],\"val\":[2,12]}}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( colour::HSVRange(0, 10, 1, 11, 2, 12), setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": {\"hue\":[10,110],\"sat\":[11,111],\"val\":[12,112]}}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( colour::HSVRange(10, 110, 11, 111, 12, 112), setting.getValue() );
}

TEST(SettingReaderTests, DoubleRangeSetting)
{
  auto reader = SettingReader{};
  auto setting = DoubleRangeSetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": [0.0,10.0]}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( util::Range<double>(0, 10), setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": [10.0,20.0]}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( util::Range<double>(10, 20), setting.getValue() );
}

TEST(SettingReaderTests, StringSetting)
{
  auto reader = SettingReader{};
  auto setting = StringSetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": \"foo\"}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( string("foo"), setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": \"bar\"}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( string("bar"), setting.getValue() );
}

TEST(SettingReaderTests, StringArraySetting)
{
  auto reader = SettingReader{};
  auto setting = StringArraySetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": [\"foo\",\"bar\",\"baz\"]}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  vector<string> expectedValue = { string("foo"), string("bar"), string("baz") };
  EXPECT_EQ( expectedValue, setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": [\"fizz\",\"bang\"]}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  expectedValue = { string("fizz"), string("bang") };
  EXPECT_EQ( expectedValue, setting.getValue() );
}

TEST(SettingReaderTests, BgrColourSetting)
{
  auto reader = SettingReader{};
  auto setting = BgrColourSetting{"foo.bar", false, "foobar"};
  auto doc = rapidjson::Document{};

  doc.Parse("{\"value\": {\"r\":64,\"g\":32,\"b\":0}}");
  EXPECT_TRUE( reader.setValueFromJson(setting, doc["value"]) );
  EXPECT_EQ( colour::BGR(0, 32, 64), setting.getValue() );

  SettingBase& sb = setting;
  doc.Parse("{\"value\": {\"r\":20,\"g\":40,\"b\":60}}");
  EXPECT_TRUE( reader.setValueFromJson(sb, doc["value"]) );
  EXPECT_EQ( colour::BGR(60, 40, 20), setting.getValue() );
}
