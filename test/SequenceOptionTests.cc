// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "Option/option.hh"
#include "Option/SequenceOption/sequenceoption.hh"

using namespace bold;
using namespace std;

class TestOption : public Option
{
public:
  TestOption()
    : Option("test", "test"),
      d_shouldTerminate(false)
  {}

  void terminate() { d_shouldTerminate = true; }
  double hasTerminated() override
  {
    bool terminate = d_shouldTerminate;
    d_shouldTerminate = false;
    return terminate ? 1.0 : 0.0;
  }

  OptionVector runPolicy() override {
    return {};
  }

private:
  bool d_shouldTerminate;
};

TEST( SequenceOptionTests, once )
{
  auto o1 = make_shared<TestOption>();
  auto o2 = make_shared<TestOption>();

  auto seq = SequenceOption("seq", {o1, o2}, SequenceOption::ONCE);

  auto res1 = seq.runPolicy();
  EXPECT_EQ( 1, res1.size() );
  EXPECT_EQ( o1, res1[0] );

  auto res2 = seq.runPolicy();
  EXPECT_EQ( 1, res2.size() );
  EXPECT_EQ( o1, res2[0] );

  o1->terminate();

  auto res3 = seq.runPolicy();
  EXPECT_EQ( 1, res3.size() );
  EXPECT_EQ( o2, res3[0] );

  auto res4 = seq.runPolicy();
  EXPECT_EQ( 1, res4.size() );
  EXPECT_EQ( o2, res4[0] );
  
  o2->terminate();

  auto res5 = seq.runPolicy();
  EXPECT_TRUE( res5.empty() );
  EXPECT_EQ( 1.0, seq.hasTerminated() );
}

TEST( SequenceOptionTests, loop )
{
  auto o1 = make_shared<TestOption>();
  auto o2 = make_shared<TestOption>();

  auto seq = SequenceOption("seq", {o1, o2}, SequenceOption::LOOP);

  auto res1 = seq.runPolicy();
  EXPECT_EQ( 1, res1.size() );
  EXPECT_EQ( o1, res1[0] );

  auto res2 = seq.runPolicy();
  EXPECT_EQ( 1, res2.size() );
  EXPECT_EQ( o1, res2[0] );

  o1->terminate();

  auto res3 = seq.runPolicy();
  EXPECT_EQ( 1, res3.size() );
  EXPECT_EQ( o2, res3[0] );

  auto res4 = seq.runPolicy();
  EXPECT_EQ( 1, res4.size() );
  EXPECT_EQ( o2, res4[0] );
  
  o2->terminate();

  auto res5 = seq.runPolicy();
  EXPECT_EQ( 1, res5.size() );
  EXPECT_EQ( o1, res5[0] );

  auto res6 = seq.runPolicy();
  EXPECT_EQ( 1, res6.size() );
  EXPECT_EQ( o1, res6[0] );

  EXPECT_EQ( 0.0, seq.hasTerminated() );
}

TEST( SequenceOptionTests, repeatLast )
{
  auto o1 = make_shared<TestOption>();
  auto o2 = make_shared<TestOption>();

  auto seq = SequenceOption("seq", {o1, o2}, SequenceOption::REPEAT_LAST);

  auto res1 = seq.runPolicy();
  EXPECT_EQ( 1, res1.size() );
  EXPECT_EQ( o1, res1[0] );

  auto res2 = seq.runPolicy();
  EXPECT_EQ( 1, res2.size() );
  EXPECT_EQ( o1, res2[0] );

  o1->terminate();

  auto res3 = seq.runPolicy();
  EXPECT_EQ( 1, res3.size() );
  EXPECT_EQ( o2, res3[0] );

  auto res4 = seq.runPolicy();
  EXPECT_EQ( 1, res4.size() );
  EXPECT_EQ( o2, res4[0] );
  
  o2->terminate();

  auto res5 = seq.runPolicy();
  EXPECT_EQ( 1, res5.size() );
  EXPECT_EQ( o2, res5[0] );

  auto res6 = seq.runPolicy();
  EXPECT_EQ( 1, res6.size() );
  EXPECT_EQ( o2, res6[0] );

  EXPECT_EQ( 0.0, seq.hasTerminated() );
}
