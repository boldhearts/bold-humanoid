// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "src/util/BufferWriter/bufferwriter.hh"

using namespace bold;
using namespace bold::util;
using namespace std;

void testEqual(const char* buffer, vector<char> expected)
{
  for (size_t i = 0; i < expected.size(); i++)
    EXPECT_EQ(expected[i], buffer[i]);
}

TEST (BufferWriterTests, writeInt8)
{
  char buffer[6];
  BufferWriter writer(buffer);

  writer.write<int8_t>(1);
  writer.write<int8_t>(2);
  writer.write(int8_t(3));
  writer.write<uint8_t>(4);
  writer.write<uint8_t>(5);
  writer.write(uint8_t(6));

  testEqual(buffer, {1, 2, 3, 4, 5, 6});
}

TEST (BufferWriterTests, writeInt16)
{
  char buffer[6];
  BufferWriter writer(buffer);

  writer.write<int16_t>(0x0201);
  writer.write<int16_t>(0x0403);
  writer.write<int16_t>(int16_t(0x0605));

  testEqual(buffer, {1, 2, 3, 4, 5, 6});
}

TEST (BufferWriterTests, writeInt32)
{
  char buffer[8];
  BufferWriter writer(buffer);

  writer.write<int32_t>(0x04030201);
  writer.write(int32_t(0x08070605));

  testEqual(buffer, {1, 2, 3, 4, 5, 6, 7, 8});
}
