// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "helpers.hh"
#include "util/SequentialTimer/sequentialtimer.hh"

#include <memory>

using namespace bold;
using namespace bold::util;
using namespace std;

TEST(SequentialTimerTests, basics)
{
  auto clock = make_shared<TimeTravelClock>();
  SequentialTimer t{clock};
  clock->advanceMillis(5);
  t.timeEvent("2");
  t.timeEvent("0");
  auto items = *t.flush();

  EXPECT_EQ(2, items.size());

  EXPECT_EQ("2", items[0].first);
  EXPECT_EQ("0", items[1].first);

  EXPECT_NEAR(5, items[0].second, 2.0);
  EXPECT_NEAR(0, items[1].second, 2.0);

  EXPECT_TRUE(items[0].second > items[1].second);
}

TEST(SequentialTimerTests, enterExit)
{
  auto clock = make_shared<TimeTravelClock>();
  SequentialTimer t{clock};

  EXPECT_EQ("", t.getPrefix());

  t.timeEvent("1");

  t.enter("a");
  {
    EXPECT_EQ("a", t.getPrefix());
    clock->advanceMillis(5);

    t.timeEvent("2");

    t.enter("b");
    {
      EXPECT_EQ("a/b", t.getPrefix());
      clock->advanceMillis(5);

      t.timeEvent("3");
    }
    t.exit();

    EXPECT_EQ("a", t.getPrefix());

    clock->advanceMillis(5);
    t.timeEvent("4");
  }
  t.exit();
  EXPECT_EQ("", t.getPrefix());

  t.timeEvent("5");

  auto items = *t.flush();

  EXPECT_EQ(7, items.size());

  EXPECT_EQ("1", items[0].first);
  EXPECT_EQ("a/2", items[1].first);
  EXPECT_EQ("a/b/3", items[2].first);
  EXPECT_EQ("a/b", items[3].first);
  EXPECT_EQ("a/4", items[4].first);
  EXPECT_EQ("a", items[5].first);
  EXPECT_EQ("5", items[6].first);

  EXPECT_NEAR(
    items[1].second + items[3].second + items[4].second,
    items[5].second, 2); // within 2ms
}

TEST(SequentialTimerTests, nesting)
{
  auto clock = make_shared<TimeTravelClock>();
  SequentialTimer t{clock};
  clock->advanceMillis(5);
  t.timeEvent("a");

  t.enter("b");
  {
    t.enter("c");
    {
      t.enter("1");
      {
        clock->advanceMillis(7);
      }
      t.exit();

      clock->advanceMillis(5);
      t.timeEvent("2");
      clock->advanceMillis(2);
      t.timeEvent("3");
    }
    t.exit();
  }
  t.exit();

  auto items = *t.flush();

  EXPECT_EQ(6, items.size());

  EXPECT_EQ("a",     items[0].first); EXPECT_BETWEEN( 5,  7, items[0].second);
  EXPECT_EQ("b/c/1", items[1].first); EXPECT_BETWEEN( 7,  9, items[1].second);
  EXPECT_EQ("b/c/2", items[2].first); EXPECT_BETWEEN( 5,  7, items[2].second);
  EXPECT_EQ("b/c/3", items[3].first); EXPECT_BETWEEN( 2,  4, items[3].second);
  EXPECT_EQ("b/c",   items[4].first); EXPECT_BETWEEN(14, 17, items[4].second);
  EXPECT_EQ("b",     items[5].first); EXPECT_BETWEEN(14, 17, items[5].second);

  EXPECT_NEAR(
    items[1].second + items[2].second + items[3].second,
    items[4].second, 1);

  EXPECT_NEAR(
    items[4].second,
    items[5].second, 0.1);
}

TEST(SequentialTimerTests, nesting2)
{
  auto clock = make_shared<TimeTravelClock>();
  SequentialTimer t{clock};
  t.enter("a");
  {
    clock->advanceMillis(5);
    t.enter("b");
    {
      clock->advanceMillis(5);
      t.timeEvent("c");
      clock->advanceMillis(5);
    }
    t.exit();
  }
  t.exit();

  auto items = *t.flush();

  EXPECT_EQ(3, items.size());

  EXPECT_EQ("a/b/c", items[0].first); EXPECT_BETWEEN( 5,  7, items[0].second);
  EXPECT_EQ("a/b",   items[1].first); EXPECT_BETWEEN(10, 12, items[1].second);
  EXPECT_EQ("a",     items[2].first); EXPECT_BETWEEN(15, 17, items[2].second);
}

TEST(SequentialTimerTests, nesting3)
{
  auto clock = make_shared<TimeTravelClock>();
  SequentialTimer t{clock};
  clock->advanceMillis(5);
  clock->advanceMillis(50);
  t.timeEvent("a");
  clock->advanceMillis(50);
  t.enter("b"); // Should warn "Potential misuse of SequentialTimer: 50ms elapsed between last recording and enter"
  {
    clock->advanceMillis(50);
    t.enter("c"); // And again here
    {
      clock->advanceMillis(50);
    }
    t.exit();
    clock->advanceMillis(50);
  }
  t.exit();

  auto items = *t.flush();

  ASSERT_EQ(3, items.size());

  EXPECT_EQ("a",   items[0].first); EXPECT_BETWEEN( 50,  70, items[0].second);
  EXPECT_EQ("b/c", items[1].first); EXPECT_BETWEEN( 50,  70, items[1].second);
  EXPECT_EQ("b",   items[2].first); EXPECT_BETWEEN(150, 190, items[2].second);
}
