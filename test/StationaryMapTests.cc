// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <Eigen/Core>

#include "helpers.hh"
#include "config/Config/config.hh"
#include "FieldMap/fieldmap.hh"
#include "state/StateObject/StationaryMapState/stationarymapstate.hh"

#include <iostream>

using namespace std;
using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace Eigen;

TEST (GoalEstimateTests, estimateOppositeGoal)
{
  GoalEstimate goal(Point2d(-1, 0), Point2d(-1, 1), GoalLabel::Unknown);

  GoalEstimate opposite(goal.estimateOppositeGoal(GoalLabel::Ours));

  EXPECT_TRUE ( PointsEqual(Point2d(FieldMap::getFieldLengthX() - 1, 0), opposite.getPost1Pos()) );
  EXPECT_TRUE ( PointsEqual(Point2d(FieldMap::getFieldLengthX() - 1, 1), opposite.getPost2Pos()) );
  EXPECT_EQ ( GoalLabel::Ours, opposite.getLabel() );

  goal = { Point2d(1, 0), Point2d(1, 1), GoalLabel::Unknown };

  opposite = goal.estimateOppositeGoal(GoalLabel::Theirs);

  EXPECT_TRUE ( PointsEqual(Point2d(-FieldMap::getFieldLengthX() + 1, 0), opposite.getPost1Pos()) );
  EXPECT_TRUE ( PointsEqual(Point2d(-FieldMap::getFieldLengthX() + 1, 1), opposite.getPost2Pos()) );
  EXPECT_EQ ( GoalLabel::Theirs, opposite.getLabel() );

  goal = { Point2d{0, 1}, Point2d{1, 1}, GoalLabel::Unknown };

  opposite = goal.estimateOppositeGoal(GoalLabel::Theirs);

  EXPECT_TRUE ( PointsEqual(Point2d(0, -FieldMap::getFieldLengthX() + 1), opposite.getPost1Pos()) );
  EXPECT_TRUE ( PointsEqual(Point2d(1, -FieldMap::getFieldLengthX() + 1), opposite.getPost2Pos()) );
  EXPECT_EQ ( GoalLabel::Theirs, opposite.getLabel() );

  goal = { Point2d(-1, -1), Point2d(1, -1), GoalLabel::Unknown };

  opposite = goal.estimateOppositeGoal(GoalLabel::Theirs);

  EXPECT_TRUE ( PointsEqual(Point2d(-1, FieldMap::getFieldLengthX() - 1), opposite.getPost1Pos()) );
  EXPECT_TRUE ( PointsEqual(Point2d( 1, FieldMap::getFieldLengthX() - 1), opposite.getPost2Pos()) );
  EXPECT_EQ ( GoalLabel::Theirs, opposite.getLabel() );
}

TEST (GoalEstimateTests, getMidpoint)
{
  GoalEstimate goal(Point2d(0, 0), Point2d(1, 0), GoalLabel::Unknown);

  EXPECT_TRUE ( PointsEqual(Point2d(0.0, 0), goal.getMidpoint(0))   );
  EXPECT_TRUE ( PointsEqual(Point2d(0.1, 0), goal.getMidpoint(0.1)) );
  EXPECT_TRUE ( PointsEqual(Point2d(0.5, 0), goal.getMidpoint(0.5)) );
  EXPECT_TRUE ( PointsEqual(Point2d(1.0, 0), goal.getMidpoint(1.0)) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
Average<T> createAverage(T value, int count)
{
  Average<T> avg;
  for (; count > 0; count--)
    avg.add(value);
  return avg;
}

TEST (StationaryMapStateTests, pairGoalPosts)
{
  // TODO give a case where one of the posts could be paired with two of the others, yet one pairing is more probable

  auto goalY = FieldMap::getGoalY();

  auto posts = StationaryMapState::PositionEstimates{};

  posts.push_back(createAverage(Point2d(0, 0),
                                StationaryMapState::GoalSamplesNeeded));
  posts.push_back(createAverage(Point2d(0.1, 0.1),
                                StationaryMapState::GoalSamplesNeeded));
  posts.push_back(createAverage(Point2d(0, goalY),
                                StationaryMapState::GoalSamplesNeeded));
  posts.push_back(createAverage(Point2d(100, 100),
                                StationaryMapState::GoalSamplesNeeded));

  auto pairs = StationaryMapState::pairGoalPosts(posts);

  ASSERT_EQ (1, pairs.size());

  EXPECT_TRUE(PointsEqual(Point2d(0, 0), pairs[0].first.getAverage()));
  EXPECT_TRUE(PointsEqual(Point2d(0, goalY), pairs[0].second.getAverage()));


  auto uselessPosts = StationaryMapState::PositionEstimates{};
  uselessPosts.push_back(createAverage(Point2d(0, 0),
                                       StationaryMapState::GoalSamplesNeeded));
  uselessPosts.push_back(createAverage(Point2d(0.1, 0.1),
                                       StationaryMapState::GoalSamplesNeeded));
  uselessPosts.push_back(createAverage(Point2d(100, 100),
                                       StationaryMapState::GoalSamplesNeeded));

  ASSERT_EQ (0, StationaryMapState::pairGoalPosts(uselessPosts).size());
}

TEST (StationaryMapStateTests, labelGoalByKeeperBallDistance)
{
  auto goalY = FieldMap::getGoalY();
  auto fieldX = FieldMap::getFieldLengthX();

  auto post1 = createAverage(Point2d(1, 0),     StationaryMapState::GoalSamplesNeeded);
  auto post2 = createAverage(Point2d(1, goalY), StationaryMapState::GoalSamplesNeeded);

  ASSERT_EQ(GoalLabel::Ours,    StationaryMapState::labelGoalByKeeperBallDistance(post1, post2, FieldSide::Ours));
  ASSERT_EQ(GoalLabel::Unknown, StationaryMapState::labelGoalByKeeperBallDistance(post1, post2, FieldSide::Unknown));
  ASSERT_EQ(GoalLabel::Unknown, StationaryMapState::labelGoalByKeeperBallDistance(post1, post2, FieldSide::Theirs));

  post1 = createAverage(Point2d(1 + fieldX/2.0, 0),     StationaryMapState::GoalSamplesNeeded);
  post2 = createAverage(Point2d(1 + fieldX/2.0, goalY), StationaryMapState::GoalSamplesNeeded);

  ASSERT_EQ(GoalLabel::Unknown, StationaryMapState::labelGoalByKeeperBallDistance(post1, post2, FieldSide::Ours));

  // TODO if the ball is on our side and the goal is far enough away, then it's Theirs, not Unknown
}

TEST (StationaryMapStateTests, labelGoalByKeeperObservations)
{
  auto goalY = FieldMap::getGoalY();
  auto maxGoalieGoalDistance = Config::getValue<double>("vision.player-detection.max-goalie-goal-dist");

  auto post1 = createAverage(Point2d(1, 0),     StationaryMapState::GoalSamplesNeeded);
  auto post2 = createAverage(Point2d(1, goalY), StationaryMapState::GoalSamplesNeeded);

  Point2d goodKeeper1(1, goalY/2.0);
  Point2d goodKeeper2(1, goalY/2.0 + 0.99 * maxGoalieGoalDistance);
  Point2d goodKeeper3(1 + 0.99 * maxGoalieGoalDistance, goalY/2.0);
  Point2d goodKeeper4(1 - 0.99 * maxGoalieGoalDistance, goalY/2.0);

  Point2d badKeeper1(1, goalY/2.0 + 1.01 * maxGoalieGoalDistance);
  Point2d badKeeper2(1, goalY/2.0 - 1.01 * maxGoalieGoalDistance);
  Point2d badKeeper3(1 + 1.01 * maxGoalieGoalDistance, goalY/2.0);
  Point2d badKeeper4(1 - 1.01 * maxGoalieGoalDistance, goalY/2.0);

  auto keepers = StationaryMapState::PositionEstimates{};
  keepers.push_back(createAverage(badKeeper1, StationaryMapState::KeeperSamplesNeeded));
  keepers.push_back(createAverage(badKeeper2, StationaryMapState::KeeperSamplesNeeded));
  keepers.push_back(createAverage(badKeeper3, StationaryMapState::KeeperSamplesNeeded));
  keepers.push_back(createAverage(badKeeper4, StationaryMapState::KeeperSamplesNeeded));

  // Test with a set of keepers observations that are too far from the midpoint of the goal
  ASSERT_EQ(GoalLabel::Unknown, StationaryMapState::labelGoalByKeeperObservations(post1, post2, keepers));

  keepers[1] = createAverage(goodKeeper1, StationaryMapState::KeeperSamplesNeeded);
  ASSERT_EQ(GoalLabel::Ours, StationaryMapState::labelGoalByKeeperObservations(post1, post2, keepers));

  keepers[1] = createAverage(goodKeeper2, StationaryMapState::KeeperSamplesNeeded);
  ASSERT_EQ(GoalLabel::Ours, StationaryMapState::labelGoalByKeeperObservations(post1, post2, keepers));

  keepers[1] = createAverage(goodKeeper3, StationaryMapState::KeeperSamplesNeeded);
  ASSERT_EQ(GoalLabel::Ours, StationaryMapState::labelGoalByKeeperObservations(post1, post2, keepers));

  keepers[1] = createAverage(goodKeeper4, StationaryMapState::KeeperSamplesNeeded);
  ASSERT_EQ(GoalLabel::Ours, StationaryMapState::labelGoalByKeeperObservations(post1, post2, keepers));
}

TEST (StationaryMapStateTests, estimateWorldPositionForPoint)
{
  auto goalY = FieldMap::getGoalY();
  auto halfGoalY = goalY / 2.0;
  auto halfFieldX = FieldMap::getFieldLengthX() / 2.0;

  Point2d pos;

  // Test from middle of field
  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(halfGoalY, halfFieldX),
                                                          Point2d(-halfGoalY, halfFieldX),
                                                          Point2d::Zero(),
                                                          GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(0,0), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(halfGoalY, halfFieldX), Point2d(-halfGoalY, halfFieldX), Point2d::Zero(), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(0,0), pos));

  // Standing 1m in front of one goal post
  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d::Zero(), GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(-halfFieldX + 2, -halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(-goalY, 2), Point2d::Zero(), GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(-halfFieldX + 2, halfGoalY), pos));

  // ...and in front of the other post
  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d::Zero(), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(halfFieldX - 2, halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(-goalY, 2), Point2d::Zero(), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(halfFieldX - 2, -halfGoalY), pos));

  // Now specify points which are not the origin

  // Standing 1m in front of one goal post
  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d(0, 1), GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(-halfFieldX + 1, -halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(-goalY, 2), Point2d(0, 1), GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(-halfFieldX + 1, halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d(goalY, 1), GoalLabel::Ours);
  EXPECT_TRUE(PointsEqual(Point2d(-halfFieldX + 1, halfGoalY), pos));

  // ...and in front of the other post
  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d(0, 1), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(halfFieldX - 1, halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(-goalY, 2), Point2d(0, 1), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(halfFieldX - 1, -halfGoalY), pos));

  pos = StationaryMapState::estimateWorldPositionForPoint(Point2d(0, 2), Point2d(goalY, 2), Point2d(goalY, 1), GoalLabel::Theirs);
  EXPECT_TRUE(PointsEqual(Point2d(halfFieldX - 1, -halfGoalY), pos));
}

TEST (StationaryMapStateTests, labelGoalByKeeperBallPosition)
{
  auto goalY = FieldMap::getGoalY();
  auto halfGoalY = goalY/2.0;

  EXPECT_EQ(
    GoalLabel::Ours,
    StationaryMapState::labelGoalByKeeperBallPosition(
      createAverage(Point2d(-3 - halfGoalY, 3), 10),
      createAverage(Point2d(-3 + halfGoalY, 3), 10),
      Point2d(-3, 3),
      Point2d(-0.1, 0.2)));

  EXPECT_EQ(
    GoalLabel::Theirs,
    StationaryMapState::labelGoalByKeeperBallPosition(
      createAverage(Point2d(3 - halfGoalY, FieldMap::getFieldLengthX() - 3), 10),
      createAverage(Point2d(3 + halfGoalY, FieldMap::getFieldLengthX() - 3), 10),
      Point2d(-3, 3),
      Point2d(-0.1, 0.2)));

  // With some error...

  EXPECT_EQ(
    GoalLabel::Theirs,
    StationaryMapState::labelGoalByKeeperBallPosition(
      createAverage(Point2d(4 - halfGoalY, FieldMap::getFieldLengthX() - 3), 10),
      createAverage(Point2d(4 + halfGoalY, FieldMap::getFieldLengthX() - 3), 10),
      Point2d(-3, 3),
      Point2d(-0.1, 0.2)));
}
