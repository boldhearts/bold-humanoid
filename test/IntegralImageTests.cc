// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/IntegralImage/integralimage.hh"
#include "vision/PixelLabel/pixellabel.hh"

using namespace bold;
using namespace bold::geometry2;
using namespace bold::vision;
using namespace std;
using namespace Eigen;

TEST (IntegralImageTests, create)
{
  int rows = 10;
  int cols = 12;
  cv::Mat image(cv::Size(cols, rows), CV_8UC1);

  image = cv::Scalar(1);

  for (int x = 0; x < cols; x++)
  {
    for (int y = 0; y < rows; y++)
      ASSERT_EQ(1, image.at<uint8_t>(y, x));
  }

  auto integral = IntegralImage::create(image, nullptr);

  // 1 1 1 1
  // 1 1 1 1
  // 1 1 1 1

  // 1 2 3 4
  // 2 4 6 8
  // 3 6 9 12

  for (int y = 0; y < rows; y++)
  {
    for (int x = 0; x < cols; x++)
      ASSERT_EQ((x + 1) * (y + 1), integral->at(y, x)) << "Failed when x=" << x << ", y=" << y;
  }

  EXPECT_EQ ( 5 * 3, integral->getSummedArea(Point2i(2, 3), Point2i(6, 5)) );
  EXPECT_EQ ( 1, integral->getSummedArea(Point2i(2, 3), Point2i(2, 3)) );
  EXPECT_EQ ( rows * cols, integral->getSummedArea(Point2i(0, 0), Point2i(cols - 1, rows - 1)) );
  EXPECT_EQ ( 2 * 2, integral->getSummedArea(Point2i(0, 2), Point2i(1, 3)) );
  EXPECT_EQ ( 2 * 2, integral->getSummedArea(Point2i(2, 0), Point2i(3, 1)) );
  EXPECT_EQ ( 4 * 2, integral->getSummedArea(Point2i(0, 0), Point2i(3, 1)) );
  EXPECT_EQ ( 2 * 4, integral->getSummedArea(Point2i(0, 0), Point2i(1, 3)) );
}

TEST (IntegralImageTests, createFromLabelData)
{
  auto sampleMap = ImageSampleMap::all(4, 4);
  auto labelData = LabelledImageData{sampleMap};

  std::fill(labelData.labelsBegin(), labelData.labelsEnd(), uint8_t(LabelClass::UNKNOWN));

  for (auto y = uint16_t{0}; y < 4; ++y)
    labelData.emplace_row(labelData.labelsBegin() + y * 4, labelData.labelsBegin() + (y + 1) * 4,
                          y, Eigen::Matrix<uint8_t, 2, 1>(1, 1));
  
  // 0, 0
  *labelData.labelsBegin() = uint8_t(LabelClass::BALL);
  // 2, 2
  *(labelData.labelsBegin() + 10) = uint8_t(LabelClass::BALL);
  
  auto integral = IntegralImage::create(labelData, LabelClass::BALL);

  for (unsigned y = 0; y < 4; ++y)
    for (unsigned x = 0; x < 4; ++x)
      if (x < 2 || y < 2)
        EXPECT_EQ( 1, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
      else
        EXPECT_EQ( 2, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
}

TEST (IntegralImageTests, createFromLabelData_half)
{
  auto sampleMap = ImageSampleMap::half(8, 8);
  auto labelData = LabelledImageData{sampleMap};

  std::fill(labelData.labelsBegin(), labelData.labelsEnd(), uint8_t(LabelClass::UNKNOWN));

  for (auto y = uint16_t{0}; y < 4; ++y)
    labelData.emplace_row(labelData.labelsBegin() + y * 4,
                          labelData.labelsBegin() + (y + 1) * 4,
                          y * 2, Eigen::Matrix<uint8_t, 2, 1>(2, 2));
  
  // 0, 0
  *labelData.labelsBegin() = uint8_t(LabelClass::BALL);
  // 4, 4
  *(labelData.labelsBegin() + 10) = uint8_t(LabelClass::BALL);

  /*
    1 2 . .
    2 4 4 . . 
    . 4 . .
    . . .
      .     5 6 . .
            6 8 8 .
            . 8 . .
            . . .
   */
  auto integral = IntegralImage::create(labelData, LabelClass::BALL);

  for (unsigned y = 0; y < 8; ++y)
    for (unsigned x = 0; x < 8; ++x)
    {
      auto expected =
        x == 0 && y == 0 ? 1 :
        x == 0 || y == 0 ? 2 :
        x < 4 || y < 4 ? 4 :
        x == 4 && y == 4 ? 5 :
        x == 4 || y == 4 ? 6 :
        8;

      EXPECT_EQ( expected, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
    }
}

TEST (IntegralImageTests, createFromLabelData_varying)
{
  auto sampleMap = ImageSampleMap([](int y) {
      auto step = y < 4 ? 1u : 2u;
      return Eigen::Matrix<uint8_t,2,1>(step, step);
    }, 8, 8);

  auto labelData = LabelledImageData{sampleMap};

  std::fill(labelData.labelsBegin(), labelData.labelsEnd(), uint8_t(LabelClass::UNKNOWN));

  for (auto y = uint16_t{0}; y < 4; ++y)
    labelData.emplace_row(labelData.labelsBegin() + y * 8,
                          labelData.labelsBegin() + (y + 1) * 8,
                          y, Eigen::Matrix<uint8_t, 2, 1>(1, 1));
  
  for (auto y = uint16_t{4}; y < 6; ++y)
    labelData.emplace_row(labelData.labelsBegin() + 4 * 8 + (y - 4) * 4,
                          labelData.labelsBegin() + 4 * 8 + (y + 1 - 4) * 4,
                          4 + (y - 4) * 2, Eigen::Matrix<uint8_t, 2, 1>(2, 2));
  // 0, 0
  *labelData.labelsBegin() = uint8_t(LabelClass::BALL);
  // 0, 4
  *(labelData.labelsBegin() + 4) = uint8_t(LabelClass::BALL);

  /*
    1 1 1 1 2 2 2 2
    . . . . . . . .
    . . . . . . . .
   */
  auto integral = IntegralImage::create(labelData, LabelClass::BALL);

  for (unsigned y = 0; y < 8; ++y)
    for (unsigned x = 0; x < 8; ++x)
    {
      auto expected =
        x < 4 ? 1 : 2;

      EXPECT_EQ( expected, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
    }
}

TEST (IntegralImageTests, createFromLabelData_horizon)
{
  auto sampleMap = ImageSampleMap::all(4, 4);
  auto labelData = LabelledImageData{sampleMap};

  std::fill(labelData.labelsBegin(), labelData.labelsEnd(), uint8_t(LabelClass::UNKNOWN));

  // horizon at y = 3
  for (auto y = uint16_t{0}; y < 3; ++y)
    labelData.emplace_row(labelData.labelsBegin() + y * 4, labelData.labelsBegin() + (y + 1) * 4,
                          y, Eigen::Matrix<uint8_t, 2, 1>(1, 1));
  
  // 0, 0
  *labelData.labelsBegin() = uint8_t(LabelClass::BALL);
  // 2, 2
  *(labelData.labelsBegin() + 10) = uint8_t(LabelClass::BALL);
  
  auto integral = IntegralImage::create(labelData, LabelClass::BALL);

  for (unsigned y = 0; y < 4; ++y)
    for (unsigned x = 0; x < 4; ++x)
      if (x < 2 || y < 2)
        EXPECT_EQ( 1, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
      else
        EXPECT_EQ( 2, integral->at(y, x) ) << "Failed when x=" << x << ", y=" << y;
}
