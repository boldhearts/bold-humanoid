// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "src/vision/YUYVProcessor/yuyvprocessor.hh"
#include "src/util/Span/span.hh"

using namespace bold::vision;
using namespace bold::util;

TEST (YUYVProcessorTests, unsquashed)
{
  auto processor = YUYVProcessor{};

  auto pixelData = std::array<uint8_t, 4>{{0, 0, 0, 0}};
  auto outData = std::array<uint8_t, 6>{1};
  
  auto pixelDataView = Span<uint8_t>{pixelData};
  auto outView = Span<uint8_t>{outData};

  processor.process(outView, pixelDataView);

  for (auto d : outData)
    EXPECT_EQ( 0, d );

  pixelData = { 128, 128, 128, 128 };
  processor.process(outView, pixelDataView);

  for (auto d : outData)
    EXPECT_EQ( 128, d );
  
  pixelData = { 1, 2, 3, 4 };
  processor.process(outView, pixelDataView);

  EXPECT_EQ( 1, outData[0] );
  EXPECT_EQ( 2, outData[1] );
  EXPECT_EQ( 4, outData[2] );
  EXPECT_EQ( 3, outData[3] );
  EXPECT_EQ( 2, outData[4] );
  EXPECT_EQ( 4, outData[5] );
}

TEST (YUYVProcessorTests, squashed)
{
  auto processor = YUYVProcessor{true};

  auto pixelData = std::array<uint8_t, 4>{{0, 0, 0, 0}};
  auto outData = std::array<uint8_t, 3>{1};
  
  auto pixelDataView = Span<uint8_t>{pixelData};
  auto outView = Span<uint8_t>{outData};

  processor.process(outView, pixelDataView);

  for (auto d : outData)
    EXPECT_EQ( 0, d );

  pixelData = { 128, 128, 128, 128 };
  processor.process(outView, pixelDataView);

  for (auto d : outData)
    EXPECT_EQ( 128, d );
  
  pixelData = { 32, 64, 96, 128 };
  processor.process(outView, pixelDataView);

  EXPECT_EQ( 64, outData[0] );
  EXPECT_EQ( 64, outData[1] );
  EXPECT_EQ( 128, outData[2] );
}

using namespace bold;
