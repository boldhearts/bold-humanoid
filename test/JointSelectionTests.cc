// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "JointId/jointid.hh"
#include "motion/core/JointSelection/jointselection.hh"

using namespace std;
using namespace bold;
using namespace bold::motion::core;

TEST (JointSelectionTests, constructor)
{
  JointSelection sel = JointSelection(true, false, false);

  EXPECT_TRUE( sel.hasHead() );
  EXPECT_FALSE( sel.hasArms() );
  EXPECT_FALSE( sel.hasLegs() );

  sel = JointSelection(false, true, false);

  EXPECT_FALSE( sel.hasHead() );
  EXPECT_TRUE( sel.hasArms() );
  EXPECT_FALSE( sel.hasLegs() );

  sel = JointSelection(false, false, true);

  EXPECT_FALSE( sel.hasHead() );
  EXPECT_FALSE( sel.hasArms() );
  EXPECT_TRUE( sel.hasLegs() );
}

TEST (JointSelectionTests, indexer)
{
  JointSelection sel = JointSelection(true, false, false);

  EXPECT_TRUE( sel[(uint8_t)JointId::HEAD_PAN] );
  EXPECT_FALSE( sel[(uint8_t)JointId::L_ELBOW] );
  EXPECT_FALSE( sel[(uint8_t)JointId::L_HIP_PITCH] );

  sel = JointSelection(false, true, false);

  EXPECT_FALSE( sel[(uint8_t)JointId::HEAD_PAN] );
  EXPECT_TRUE( sel[(uint8_t)JointId::L_ELBOW] );
  EXPECT_FALSE( sel[(uint8_t)JointId::L_HIP_PITCH] );

  sel = JointSelection(false, false, true);

  EXPECT_FALSE( sel[(uint8_t)JointId::HEAD_PAN] );
  EXPECT_FALSE( sel[(uint8_t)JointId::L_ELBOW] );
  EXPECT_TRUE( sel[(uint8_t)JointId::L_HIP_PITCH] );
}
