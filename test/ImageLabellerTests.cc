// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "helpers.hh"

#include "colour/HSV/hsv.hh"
#include "vision/PixelLabel/RangePixelLabel/rangepixellabel.hh"
#include "vision/ImageLabeller/imagelabeller.hh"
#include "vision/ImageLabeller/PixelLabeller/LUTImageLabeller/lutimagelabeller.hh"

using namespace std;
using namespace bold;
using namespace bold::colour;
using namespace bold::vision;

TEST(ImageLabellerTests, labelPixel) {
  auto label1 = make_shared<RangePixelLabel>("foo",
                                             LabelClass::GOAL,
                                             HSVRange(0, 128, 0, 128, 0, 128));
  auto label2 = make_shared<RangePixelLabel>("bar",
                                             LabelClass::BALL,
                                             HSVRange(64, 192, 64, 192, 64, 192));

  auto labeller = make_unique<LUTImageLabeller<18>>(vector<shared_ptr<PixelLabel>>{{label1, label2}});

  // HSV: 0, 0, 32
  auto c1 = YCbCr(32, 128, 128);
  EXPECT_EQ(label1, labeller->labelPixel(c1));

  // HSV: 0, 0, 128
  auto c2 = YCbCr(128, 128, 128);
  EXPECT_EQ(label1, labeller->labelPixel(c2));

  // HSV: 94, 97, 94
  // In both ranges, should return first
  auto c3 = YCbCr(80, 120, 112);
  EXPECT_EQ(label1, labeller->labelPixel(c3));

  // HSV: 102, 96, 163
  auto c4 = YCbCr(140, 120, 100);
  EXPECT_EQ(label2, labeller->labelPixel(c4));

  // HSV: 0, 0, 140
  auto c5 = YCbCr(140, 128, 128);
  EXPECT_EQ(nullptr, labeller->labelPixel(c5));

}


TEST(ImageLabellerTests, labelImage) {
  auto label1 = make_shared<RangePixelLabel>("foo",
                                             LabelClass::GOAL,
                                             HSVRange(0, 128, 0, 128, 0, 128));
  auto label2 = make_shared<RangePixelLabel>("bar",
                                             LabelClass::BALL,
                                             HSVRange(64, 192, 64, 192, 64, 192));

  auto labeller = make_unique<LUTImageLabeller<18>>(vector<shared_ptr<PixelLabel>>{{label1, label2}});

  auto img = cv::Mat(2, 2, CV_8UC3);
  img.at<cv::Vec3b>(0, 0) = cv::Vec3b{32, 128, 128};
  img.at<cv::Vec3b>(0, 1) = cv::Vec3b{80, 120, 112};
  img.at<cv::Vec3b>(1, 0) = cv::Vec3b{140, 120, 100};
  img.at<cv::Vec3b>(1, 1) = cv::Vec3b{140, 128, 128};

  LabelClass expectedLabels[4] = {LabelClass::GOAL, LabelClass::GOAL,
                                  LabelClass::BALL, LabelClass::UNKNOWN};

  auto horizon = geometry2::LineSegment2i{geometry2::Point2i{0, 2}, geometry2::Point2i{1, 2}};

  auto labelData = labeller->labelImage(img,
                                        ImageSampleMap::all(2, 2),
                                        horizon);

  EXPECT_EQ(2, labelData.getLabelledRowCount());

  unsigned i = 0;
  for (auto const &row : labelData) {
    EXPECT_TRUE(VectorsEqual(Eigen::Matrix<uint8_t, 2, 1>(1, 1), row.granularity));
    auto step = row.granularity.x();
    for (auto label = row.begin(); label < row.end(); label += step) {
      auto expectedLabel = unsigned(expectedLabels[i++]);
      auto realLabel = unsigned{*label};
      EXPECT_EQ(expectedLabel, realLabel);
    }
  }
}
