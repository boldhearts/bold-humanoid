// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "agent/OrientationTracker/orientationtracker.hh"
#include "agent/Odometer/odometer.hh"
#include "roundtable/Action/action.hh"

using namespace bold;
using namespace bold::state;

TEST(StateObserverTests, OrientationTracker_new)
{
  roundtable::Action::clearActions();  
  auto tracker1 = new OrientationTracker{};
  roundtable::Action::clearActions();  
  auto tracker2 = new OrientationTracker{};
  roundtable::Action::clearActions();  
  auto tracker3 = new OrientationTracker{};
  roundtable::Action::clearActions();  
  auto tracker4 = new OrientationTracker{};
  delete tracker1;
  delete tracker2;
  delete tracker3;
  delete tracker4;
}

TEST(StateObserverTests, OrientationTracker_create)
{
  roundtable::Action::clearActions();  
  auto tracker1 = StateObserver::make<OrientationTracker>();
  roundtable::Action::clearActions();  
  auto tracker2 = StateObserver::make<OrientationTracker>();
  roundtable::Action::clearActions();  
  auto tracker3 = StateObserver::make<OrientationTracker>();
  roundtable::Action::clearActions();  
  auto tracker4 = StateObserver::make<OrientationTracker>();
}

TEST(StateObserverTests, Odometer_new)
{
  roundtable::Action::clearActions();
  auto odometer1 = new Odometer{};
  roundtable::Action::clearActions();  
  auto odometer2 = new Odometer{};
  roundtable::Action::clearActions();  
  auto odometer3 = new Odometer{};
  roundtable::Action::clearActions();  
  auto odometer4 = new Odometer{};
  delete odometer1;
  delete odometer2;
  delete odometer3;
  delete odometer4;
}

TEST(StateObserverTests, Odometer_create)
{
  roundtable::Action::clearActions();
  auto odometer1 = StateObserver::make<Odometer>();
  roundtable::Action::clearActions();  
  auto odometer2 = StateObserver::make<Odometer>();
  roundtable::Action::clearActions();  
  auto odometer3 = StateObserver::make<Odometer>();
  roundtable::Action::clearActions();  
  auto odometer4 = StateObserver::make<Odometer>();
}

