// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "CM730Snapshot/cm730snapshot.hh"
#include "MotionScriptRunner/motionscriptrunner.hh"
#include "MX28Snapshot/mx28snapshot.hh"
#include "state/State/state.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "ThreadUtil/threadutil.hh"

#include "motion/core/JointSelection/jointselection.hh"
#include "motion/core/MotionRequest/motionrequest.hh"
#include "motion/scripts/MotionScript/motionscript.hh"

#include <memory>
#include <vector>

#include <Eigen/Core>

using namespace bold;
using namespace bold::motion::core;
using namespace bold::motion::scripts;
using namespace bold::state;
using namespace Eigen;
using namespace std;

void pushStep(shared_ptr<MotionScript::Stage> stage, uint16_t value, uint8_t moveCycles, uint8_t pauseCycles = 0)
{
  MotionScript::KeyFrame step;

  step.moveCycles = moveCycles;
  step.pauseCycles = pauseCycles;

  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
    step.values[jointId-1] = value;

  stage->keyFrames.push_back(step);
}

// TODO unit test that runs through all motion script files on disk

TEST (DISABLED_MotionScriptRunnerTests, basics)
{
  ThreadUtil::setThreadId(ThreadId::MotionLoop);

  // TODO convenience method for populating a basic HardwareState object
  auto cm730State = make_unique<CM730Snapshot const>();
  auto mx28States = vector<unique_ptr<MX28Snapshot const>>();
  for (uint8_t id = 0; id < 20; id++) {
    auto mx28 = make_unique<MX28Snapshot>(id);
    mx28->presentPositionValue = 0;
    mx28States.push_back(move(mx28));
  }
  State::set<HardwareState>(make_shared<HardwareState>(move(cm730State), move(mx28States), 0, 0, 0));

  auto stage = make_shared<MotionScript::Stage>();

  pushStep(stage, 100, 5, 0);

  vector<shared_ptr<MotionScript::Stage>> stages = { stage };
  auto script = make_shared<MotionScript>("test-script", stages, true, true, true);

  MotionScriptRunner runner(script);

  EXPECT_EQ(MotionScriptRunner::Status::Pending, runner.getStatus());
  EXPECT_EQ(0, runner.getCurrentStageIndex());
  EXPECT_EQ(0, runner.getCurrentKeyFrameIndex());
  EXPECT_EQ(script, runner.getScript());

  EXPECT_TRUE(runner.step(*JointSelection::all()));

  EXPECT_EQ(MotionScriptRunner::Status::Running, runner.getStatus());

  while (runner.step(*JointSelection::all()))
  {}
}
