// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "src/util/binary.hh"

#include <array>

using namespace bold;
using namespace bold::util;
using namespace std;

TEST (BinaryTests, makeWord)
{
  EXPECT_EQ (0x0000, binary::makeWord(0x00, 0x00));
  EXPECT_EQ (0x00FF, binary::makeWord(0xFF, 0x00));
  EXPECT_EQ (0xFF00, binary::makeWord(0x00, 0xFF));
  EXPECT_EQ (0xFFFF, binary::makeWord(0xFF, 0xFF));
}

TEST (BinaryTests, getLowByte)
{
  EXPECT_EQ (0x00, binary::getLowByte(0x0000));
  EXPECT_EQ (0xFF, binary::getLowByte(0x00FF));
  EXPECT_EQ (0x00, binary::getLowByte(0xFF00));
  EXPECT_EQ (0xFF, binary::getLowByte(0xFFFF));
}

TEST (BinaryTests, getHighByte)
{
  EXPECT_EQ (0x00, binary::getHighByte(0x0000));
  EXPECT_EQ (0x00, binary::getHighByte(0x00FF));
  EXPECT_EQ (0xFF, binary::getHighByte(0xFF00));
  EXPECT_EQ (0xFF, binary::getHighByte(0xFFFF));
}

TEST (BinaryTests, calculateChecksum)
{
  // Test raw C array
  uint8_t cArrayBuffer[4] = {0x0F};
  EXPECT_EQ(0xF0, binary::calculateChecksum(cArrayBuffer,cArrayBuffer + 4));
  EXPECT_EQ(0xF0, binary::calculateChecksum(begin(cArrayBuffer), end(cArrayBuffer)));

  // Test std::array
  array<uint8_t, 4> stdArrayBuffer{{0x0F}};
  EXPECT_EQ(0xF0, binary::calculateChecksum(begin(stdArrayBuffer), end(stdArrayBuffer)));

  // Test const array
  array<uint8_t, 4> const constStdArrayBuffer{{0x0F}};
  EXPECT_EQ(0xF0, binary::calculateChecksum(begin(constStdArrayBuffer), end(constStdArrayBuffer)));

  // Test std::vector
  vector<uint8_t> stdVectorBuffer{{0x0F}};
  EXPECT_EQ(0xF0, binary::calculateChecksum(begin(stdVectorBuffer), end(stdVectorBuffer)));

  // Test actual package content examples
  uint8_t array1[] = {0xC8, 0x04, 0x03, 0x18, 0x01};
  EXPECT_EQ(0x17, binary::calculateChecksum(begin(array1), end(array1)));

  vector<uint8_t> stdVector1{{0xC8, 0x04, 0x03, 0x18, 0x01}};
  EXPECT_EQ(0x17, binary::calculateChecksum(begin(stdVector1), end(stdVector1)));


  uint8_t array2[] = {0xC8, 0x02, 0x00};
  EXPECT_EQ(0x35, binary::calculateChecksum(begin(array2), end(array2)));

  // Test without prefix
  uint8_t array3[] = {0xFF, 0xFF, 0xC8, 0x02, 0x00};
  EXPECT_EQ(0x35, binary::calculateChecksum(next(begin(array3), 2), std::prev(end(array3), 1)));

  vector<uint8_t> stdVector3{{0xFF, 0xFF, 0xC8, 0x02, 0x00}};
  EXPECT_EQ(0x35, binary::calculateChecksum(next(begin(stdVector3), 2), std::prev(end(stdVector3), 1)));
}
