// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "vision/Blob/blob.hh"
#include "vision/VisualCortex/visualcortex.hh"

using namespace std;
using namespace bold;
using namespace Eigen;

TEST (VisualCortexTests, shouldMergeBallBlobs)
{
  /*
    TODO: Redo test
  auto blob1 = vision::Blob(AlignedBox2i(Vector2i(130, 77), Vector2i(188, 126)),
                    20,
                    Vector2f(150, 100),
                    {});
  
  auto blob2 = vision::Blob(AlignedBox2i(Vector2i(120, 93), Vector2i(174, 137)),
                    20,
                    Vector2f(150, 110),
                    {});
  

  EXPECT_TRUE(VisualCortex::shouldMergeBallBlobs(blob1, blob2));
  */
}
