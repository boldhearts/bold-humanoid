// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "config/SettingWriter/settingwriter.hh"
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include <iostream>

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace rapidjson;
using namespace std;

template<typename T>
void ValueOutputEqual(string const& expected, T const& setting)
{
  SettingBase const& sb = setting;

  auto buffer2 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer2(buffer2);
  auto settingWriter = SettingWriter{};
  settingWriter.writeJsonValue(writer2, setting);

  auto buffer3 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer3(buffer3);
  settingWriter.writeJsonValue(writer3, sb);

  EXPECT_EQ( expected, string(buffer2.GetString()) );
  EXPECT_EQ( expected, string(buffer3.GetString()) );
}

template<typename T>
void MetadataOutputEqual(string const& expected, T const& setting)
{
  SettingBase const& sb = setting;

  auto buffer2 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer2(buffer2);
  auto settingWriter = SettingWriter{};
  writer2.StartObject();
  settingWriter.writeJsonMetadata(writer2, setting);
  writer2.EndObject();

  auto buffer3 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer3(buffer3);
  writer3.StartObject();
  settingWriter.writeJsonMetadata(writer3, sb);
  writer3.EndObject();

  EXPECT_EQ( expected, string(buffer2.GetString()) );
  EXPECT_EQ( expected, string(buffer3.GetString()) );
}

template<typename T>
void FullOutputEqual(string const& expected, T const& setting)
{
  SettingBase const& sb = setting;

  auto buffer2 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer2(buffer2);
  auto settingWriter = SettingWriter{};
  settingWriter.writeFullJson(writer2, setting);

  auto buffer3 = StringBuffer{};
  Writer<rapidjson::StringBuffer> writer3(buffer3);
  settingWriter.writeFullJson(writer3, sb);

  EXPECT_EQ( expected, string(buffer2.GetString()) );
  EXPECT_EQ( expected, string(buffer3.GetString()) );
}


TEST(SettingWriterTests, IntSetting)
{
  auto setting = IntSetting{"foo.bar", 0, 10, false, "foobar"};
  setting.setValue(5);
  
  ValueOutputEqual("5", setting);
  MetadataOutputEqual("{\"initial\":5,\"min\":0,\"max\":10}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"int\",\"description\":\"foobar\",\"value\":5,\"initial\":5,\"min\":0,\"max\":10}", setting);
}

TEST(SettingWriterTests, EnumSetting)
{
  auto m = map<int, string>{};
  m[0] = "foo";
  m[1] = "bar";
    
  auto setting = EnumSetting{"foo.bar", m, false, "foobar"};
  setting.setValue(1);

  ValueOutputEqual("1", setting);
  MetadataOutputEqual("{\"initial\":1,\"values\":[{\"text\":\"foo\",\"value\":0},{\"text\":\"bar\",\"value\":1}]}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"enum\",\"description\":\"foobar\",\"value\":1,\"initial\":1,\"values\":[{\"text\":\"foo\",\"value\":0},{\"text\":\"bar\",\"value\":1}]}", setting);
}

TEST(SettingWriterTests, DoubleSetting)
{
  auto setting = DoubleSetting{"foo.bar", 0, 10, false, "foobar"};
  setting.setValue(5);

  ValueOutputEqual("5.0", setting);
  MetadataOutputEqual("{\"initial\":5.0,\"min\":0.0,\"max\":10.0}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"double\",\"description\":\"foobar\",\"value\":5.0,\"initial\":5.0,\"min\":0.0,\"max\":10.0}", setting);
}

TEST(SettingWriterTests, BoolSetting)
{
  auto setting = BoolSetting{"foo.bar", false, "foobar"};
  setting.setValue(true);

  ValueOutputEqual("true", setting);
  MetadataOutputEqual("{\"initial\":true}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"bool\",\"description\":\"foobar\",\"value\":true,\"initial\":true}", setting);
}

TEST(SettingWriterTests, HsvRangeSetting)
{
  auto setting = HsvRangeSetting{"foo.bar", false, "foobar"};
  setting.setValue(colour::HSVRange{0, 10, 1, 11, 2, 12});

  ValueOutputEqual("{\"hue\":[0,10],\"sat\":[1,11],\"val\":[2,12]}", setting);
  MetadataOutputEqual("{\"initial\":{\"hue\":[0,10],\"sat\":[1,11],\"val\":[2,12]}}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"hsv-range\",\"description\":\"foobar\",\"value\":{\"hue\":[0,10],\"sat\":[1,11],\"val\":[2,12]},\"initial\":{\"hue\":[0,10],\"sat\":[1,11],\"val\":[2,12]}}", setting);
}

TEST(SettingWriterTests, DoubleRangeSetting)
{
  auto setting = DoubleRangeSetting{"foo.bar", false, "foobar"};
  setting.setValue(Range<double>{0, 10});

  ValueOutputEqual("[0.0,10.0]", setting);
  MetadataOutputEqual("{\"initial\":[0.0,10.0]}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"double-range\",\"description\":\"foobar\",\"value\":[0.0,10.0],\"initial\":[0.0,10.0]}", setting);
}

TEST(SettingWriterTests, StringSetting)
{
  auto setting = StringSetting{"foo.bar", false, "foobar"};
  setting.setValue("baz");

  ValueOutputEqual("\"baz\"", setting);
  MetadataOutputEqual("{\"initial\":\"baz\"}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"string\",\"description\":\"foobar\",\"value\":\"baz\",\"initial\":\"baz\"}", setting);
}

TEST(SettingWriterTests, StringArraySetting)
{
  auto setting = StringArraySetting{"foo.bar", false, "foobar"};
  setting.setValue({"foo", "bar", "baz"});

  ValueOutputEqual("[\"foo\",\"bar\",\"baz\"]", setting);
  MetadataOutputEqual("{\"initial\":[\"foo\",\"bar\",\"baz\"]}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"string[]\",\"description\":\"foobar\",\"value\":[\"foo\",\"bar\",\"baz\"],\"initial\":[\"foo\",\"bar\",\"baz\"]}", setting);
}

TEST(SettingWriterTests, BgrColourSetting)
{
  auto setting = BgrColourSetting{"foo.bar", false, "foobar"};
  setting.setValue(colour::BGR{0, 32, 64});

  ValueOutputEqual("{\"r\":64,\"g\":32,\"b\":0}", setting);
  MetadataOutputEqual("{\"initial\":{\"r\":64,\"g\":32,\"b\":0}}", setting);
  FullOutputEqual("{\"path\":\"foo.bar\",\"type\":\"bgr-colour\",\"description\":\"foobar\",\"value\":{\"r\":64,\"g\":32,\"b\":0},\"initial\":{\"r\":64,\"g\":32,\"b\":0}}", setting);
}
