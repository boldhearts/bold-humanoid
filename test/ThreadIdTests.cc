// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>


#include "ThreadUtil/threadutil.hh"

#include <thread>

using namespace bold;
using namespace std;

// NOTE these tests ensure the same threading characteristics on development and production environments

TEST (ThreadIdTests, threadIdAssignment)
{
  auto makeLoop = [](ThreadId threadId)
  {
    return [threadId]
    {
      ThreadUtil::setThreadId(threadId);

      EXPECT_EQ( threadId, ThreadUtil::getThreadId() );
    };
  };

  int threadCount = 10;

  ThreadUtil::setThreadId((ThreadId)0);

  vector<thread> threads;
  for (int t = 1; t <= threadCount; t++)
    threads.emplace_back(makeLoop((ThreadId)t));

  for (auto& thread : threads)
    thread.join();

  EXPECT_EQ( (ThreadId)0, ThreadUtil::getThreadId() );
}
