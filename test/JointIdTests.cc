// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "JointId/jointid.hh"

using namespace std;
using namespace bold;

TEST (JointPairsTests, getPartner)
{
  EXPECT_EQ(JointId::R_SHOULDER_PITCH, JointPairs::getPartner(JointId::L_SHOULDER_PITCH));
  EXPECT_EQ(JointId::R_HIP_PITCH, JointPairs::getPartner(JointId::L_HIP_PITCH));
  EXPECT_EQ(JointId::L_KNEE, JointPairs::getPartner(JointId::R_KNEE));
}

TEST (JointPairsTests, isBase)
{
  EXPECT_TRUE ( JointPairs::isBase(JointId::R_SHOULDER_PITCH) );
  EXPECT_FALSE( JointPairs::isBase(JointId::L_SHOULDER_PITCH) );
  EXPECT_TRUE ( JointPairs::isBase(JointId::R_HIP_PITCH) );
  EXPECT_FALSE( JointPairs::isBase(JointId::L_HIP_PITCH) );
  EXPECT_TRUE ( JointPairs::isBase(JointId::R_KNEE) );
}
