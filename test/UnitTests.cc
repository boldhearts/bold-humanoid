// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "Agent/agent.hh"

#include "config/Config/config.hh"
#include "state/State/state.hh"
#include "FieldMap/fieldmap.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;
using namespace bold::util;

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);

  Log::minLevel = LogLevel::Warning;

  for (int i = 0; i < argc; i++)
  {
    if (strcmp("-v", argv[i]) == 0)
      Log::minLevel = LogLevel::Verbose;
  }

  State::initialise();

  Agent::registerStateTypes();

  Config::initialise("../configuration-metadata.json", "../configuration-team.json");

  Config::setPermissible();

  FieldMap::initialise();

  return RUN_ALL_TESTS();
}
