// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "WalkEngine/RobotisWalkEngine/robotiswalkengine.hh"
#include "ThreadUtil/threadutil.hh"
#include "motion/core/BodySectionControl/bodysectioncontrol.hh"

#include <iostream>

using namespace bold;
using namespace bold::motion::core;
using namespace std;

TEST(RobotisWalkEngineTests, initialisation)
{
  ThreadUtil::setThreadId(ThreadId::MotionLoop);

  auto engine = RobotisWalkEngine();

  EXPECT_TRUE( engine.canStopNow() );
  EXPECT_EQ( 0.0, engine.getXMoveAmplitude() );
  EXPECT_EQ( 0.0, engine.getYMoveAmplitude() );
  EXPECT_EQ( 0.0, engine.getAMoveAmplitude() );

  EXPECT_EQ( engine.getCurrentPhase(), RobotisWalkEngine::WalkPhase::LeftUp );
  EXPECT_EQ( engine.getBodySwingY(), 0.0);
  EXPECT_EQ( engine.getBodySwingZ(), 0.0);

  HeadSectionControl head;
  ArmSectionControl arm;
  LegSectionControl leg;

  engine.applyHead(head);
  engine.applyArms(arm);
  engine.applyLegs(leg);

  head.visitJoints([](JointControl& j) {
      EXPECT_EQ( 0.0, j.getRadians() );
    });
}

TEST(RobotisWalkEngineTests, reset)
{
  ThreadUtil::setThreadId(ThreadId::MotionLoop);

  auto engine = RobotisWalkEngine();
  engine.setYMoveAmplitude(1.0);

  for (size_t i = 0; i < 20; ++i)
    engine.step();

  EXPECT_FALSE( engine.canStopNow() );

  engine.reset();

  EXPECT_EQ( 0.0, engine.getXMoveAmplitude() );
  EXPECT_EQ( 0.0, engine.getYMoveAmplitude() );
  EXPECT_EQ( 0.0, engine.getAMoveAmplitude() );

  EXPECT_TRUE( engine.canStopNow() );
  EXPECT_EQ( engine.getCurrentPhase(), RobotisWalkEngine::WalkPhase::LeftUp );
  EXPECT_EQ( engine.getBodySwingY(), 0.0);
  EXPECT_EQ( engine.getBodySwingZ(), 0.0);
}
