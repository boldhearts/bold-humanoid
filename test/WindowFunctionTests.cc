// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include "helpers.hh"
#include "src/util/WindowFunction/windowfunction.hh"

using namespace std;
using namespace bold;
using namespace bold::util;

TEST (WindowFunctionTests, rectangle)
{
  auto windowFunction = WindowFunction::create<RectangleWindowFunction>(5);
  auto ones = vector<float>(5);
  std::fill(ones.begin(), ones.end(), 1.0);
  windowFunction->apply(ones.begin(), ones.end());

  EXPECT_EQ(1.0, ones[0]);
  EXPECT_EQ(1.0, ones[1]);
  EXPECT_EQ(1.0, ones[2]);
  EXPECT_EQ(1.0, ones[3]);
  EXPECT_EQ(1.0, ones[4]);
}

TEST (WindowFunctionTests, triangular)
{
  auto windowFunction = WindowFunction::create<TriangularWindowFunction>(5);
  auto ones = vector<float>(5);
  std::fill(ones.begin(), ones.end(), 1.0);
  windowFunction->apply(ones.begin(), ones.end());

  EXPECT_EQ(0.0, ones[0]);
  EXPECT_EQ(0.5, ones[1]);
  EXPECT_EQ(1.0, ones[2]);
  EXPECT_EQ(0.5, ones[3]);
  EXPECT_EQ(0.0, ones[4]);
}
