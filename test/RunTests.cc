// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "vision/LabelledImageProcessor/BlobDetector/blobdetector.hh"

using namespace std;
using namespace bold::vision;

Run makeRun(uint16_t startX, uint16_t endX, uint16_t y = 1)
{
  auto run = Run{startX, endX, y};

  EXPECT_EQ ( y, run.y() );
  EXPECT_EQ ( startX, run.startX() );
  EXPECT_EQ ( endX, run.endX() );
  EXPECT_EQ ( endX - startX + 1, run.length() );

  return run;
}

TEST (RunTests, length)
{
  EXPECT_EQ ( 1, makeRun(0, 0).length() );
  EXPECT_EQ ( 1, makeRun(1, 1).length() );
  EXPECT_EQ ( 2, makeRun(1, 2).length() );
  EXPECT_EQ ( 3, makeRun(1, 3).length() );
}

TEST (RunTests, overlaps)
{
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(5, 15)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(15, 25)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(11, 19)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(0, 10)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(10, 10)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(20, 20)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(20, 30)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(0, 30)) );
  EXPECT_TRUE ( makeRun(10, 10).overlaps(makeRun(10, 10)) );

  // touching at a diagonal (8 connected)
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(21, 30)) );
  EXPECT_TRUE ( makeRun(10, 20).overlaps(makeRun(0, 9)) );

  // completely separate
  EXPECT_FALSE ( makeRun(10, 20).overlaps(makeRun(22, 30)) );
  EXPECT_FALSE ( makeRun(10, 20).overlaps(makeRun(0, 8)) );
}
