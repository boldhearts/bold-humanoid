// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "PatternGenerator/SineGenerator/sinegenerator.hh"
#include "PatternGenerator/ClampedSquashedPatternGenerator/clampedsquashedpatterngenerator.hh"

using namespace bold;

TEST(PatternGeneratorTests, SineGenerator)
{
  auto sine0 = SineGenerator(2 * M_PI, 0.0, 1.0, 0.0);
  EXPECT_NEAR( 0.0, sine0.getOutput(0.0), 1e-12 );
  EXPECT_NEAR( 0.0, sine0.getOutput(M_PI), 1e-12 );
  EXPECT_NEAR( 0.0, sine0.getOutput(2 * M_PI), 1e-12 );

  EXPECT_NEAR( 1.0, sine0.getOutput(M_PI / 2.0), 1e-12 );
  EXPECT_NEAR( -1.0, sine0.getOutput(M_PI * 3.0 / 2.0), 1e-12 );

  auto sine1 = SineGenerator(2 * M_PI, M_PI / 2, 1.0, 0.0);
  EXPECT_NEAR( -1.0, sine1.getOutput(0.0), 1e-12 );
  EXPECT_NEAR( 1.0, sine1.getOutput(M_PI), 1e-12 );
  EXPECT_NEAR( -1.0, sine1.getOutput(2 * M_PI), 1e-12 );

  EXPECT_NEAR( 0.0, sine1.getOutput(M_PI / 2.0), 1e-12 );
  EXPECT_NEAR( 0.0, sine1.getOutput(M_PI * 3.0 / 2.0), 1e-12 );

  auto sine2 = SineGenerator(2 * M_PI, 0.0, 2.0, 0.0);
  EXPECT_NEAR( 0.0, sine2.getOutput(0.0), 1e-12 );
  EXPECT_NEAR( 0.0, sine2.getOutput(M_PI), 1e-12 );
  EXPECT_NEAR( 0.0, sine2.getOutput(2 * M_PI), 1e-12 );

  EXPECT_NEAR( 2.0, sine2.getOutput(M_PI / 2.0), 1e-12 );
  EXPECT_NEAR( -2.0, sine2.getOutput(M_PI * 3.0 / 2.0), 1e-12 );

  auto sine3 = SineGenerator(2 * M_PI, 0.0, 1.0, 1.0);
  EXPECT_NEAR( 1.0, sine3.getOutput(0.0), 1e-12 );
  EXPECT_NEAR( 1.0, sine3.getOutput(M_PI), 1e-12 );
  EXPECT_NEAR( 1.0, sine3.getOutput(2 * M_PI), 1e-12 );

  EXPECT_NEAR( 2.0, sine3.getOutput(M_PI / 2.0), 1e-12 );
  EXPECT_NEAR( 0.0, sine3.getOutput(M_PI * 3.0 / 2.0), 1e-12 );
}

TEST(PatternGeneratorTests, ClampedSquashedPatternGenerator_baseTime)
{
  auto clamped0 = ClampedSquashedPatternGenerator<SineGenerator>(1.0, false,
                                                                 2 * M_PI, 0.0, 1.0, 0.0);

  EXPECT_NEAR( 0.0, clamped0.baseTime(0.0), 1e-12 );
  EXPECT_NEAR( M_PI_2, clamped0.baseTime(M_PI_2), 1e-12 );
  EXPECT_NEAR( M_PI, clamped0.baseTime(M_PI), 1e-12 );
  EXPECT_NEAR( 3 * M_PI_2, clamped0.baseTime(3 * M_PI_2), 1e-12 );
  EXPECT_NEAR( 2 * M_PI, clamped0.baseTime(2 * M_PI), 1e-12 );

  auto clamped1 = ClampedSquashedPatternGenerator<SineGenerator>(1.0, true,
                                                                 2 * M_PI, 0.0, 1.0, 0.0);

  EXPECT_NEAR( 0.0, clamped1.baseTime(0.0), 1e-12 );
  EXPECT_NEAR( M_PI_2, clamped1.baseTime(M_PI_2), 1e-12 );
  EXPECT_NEAR( M_PI, clamped1.baseTime(M_PI), 1e-12 );
  EXPECT_NEAR( 3 * M_PI_2, clamped1.baseTime(3 * M_PI_2), 1e-12 );
  EXPECT_NEAR( 2 * M_PI, clamped1.baseTime(2 * M_PI), 1e-12 );


  auto clamped2 = ClampedSquashedPatternGenerator<SineGenerator>(0.9, false,
                                                                 2.0, 0.0, 1.0, 0.0);

  EXPECT_NEAR( 0.0, clamped2.baseTime(0.0), 1e-12 );
  EXPECT_NEAR( 0.0, clamped2.baseTime(0.1), 1e-12 );
  EXPECT_LT( 0.0, clamped2.baseTime(0.101) );
  EXPECT_NEAR( 0.1 / 0.9, clamped2.baseTime(0.2), 1e-12 );
  EXPECT_NEAR( 1.0, clamped2.baseTime(1.0), 1e-12 );
  EXPECT_NEAR( 1.7 / 0.9, clamped2.baseTime(1.8), 1e-12 );
  EXPECT_GT( 2.0, clamped2.baseTime(1.899) );
  EXPECT_NEAR( 2.0, clamped2.baseTime(1.9), 1e-12 );
  EXPECT_NEAR( 2.0, clamped2.baseTime(2.0), 1e-12 );

}
