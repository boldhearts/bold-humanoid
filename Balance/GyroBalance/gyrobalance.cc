// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "gyrobalance.hh"

#include "config/Config/config.hh"
#include "state/State/state.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "WalkEngine/RobotisWalkEngine/robotiswalkengine.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::config;
using namespace bold::util;

GyroBalance::GyroBalance(std::shared_ptr<WalkEngine> walkEngine)
{
  d_accOffsetRL    = Config::getSetting<double>("balance.gyro.acc-offset-rl");
  d_accOffsetFB    = Config::getSetting<double>("balance.gyro.acc-offset-fb");
  d_walkEngine     = walkEngine;
  d_kneeGain       = Config::getSetting<double>("balance.gyro.knee-gain");
  d_anklePitchGain = Config::getSetting<double>("balance.gyro.ankle-pitch-gain");
  d_hipRollGain    = Config::getSetting<double>("balance.gyro.hip-roll-gain");
  d_ankleRollGain  = Config::getSetting<double>("balance.gyro.ankle-roll-gain");
  d_targetHipPitch = Config::getSetting<double>("walk-module.hip-pitch.stable-angle");
}

BalanceOffset GyroBalance::computeCorrection(double targetPitchRads)
{
  auto hw = State::get<HardwareState>();

  ASSERT(hw);

  auto acc = hw->getCM730State().acc;
  auto gyro = hw->getCM730State().gyro;
  
  /*
   * NOTE: Acc data meaning:
   *   -y -> Forward
   *   +y -> Backward
   *   -x -> Right
   *   +x -> Left
   */
  
  double accRL = -acc.x();
  double accFB = acc.y();
  double gyroRL = -gyro.y();
  double gyroFB = gyro.x();
  
  double corrRL = accRL;
  double corrFB = accFB;
  
  double targetRL = d_accOffsetRL->getValue();
  double targetFB = d_accOffsetFB->getValue();
  
  auto walkPhase = ((RobotisWalkEngine*)(d_walkEngine.get()))->getCurrentPhase();
  
  switch(walkPhase){
    case RobotisWalkEngine::WalkPhase::LeftDown:
      targetRL += 0;
      targetFB += 0;
      break;
    case RobotisWalkEngine::WalkPhase::LeftUp:
      targetRL += 0;
      targetFB += 0;
      break;
    case RobotisWalkEngine::WalkPhase::RightDown:
      targetRL += 0;
      targetFB += 0;
      break;
    case RobotisWalkEngine::WalkPhase::RightUp:
      targetRL += 0;
      targetFB += 0;
      break;
    default :
      /* Do nothing, phase unsupported */
      Log::error("GyroBalance") << "Unsupported WalkPhase";
  }
  
  /* NOTE: Don't use too much of the acc data due to drift in walk - care should
   *       be taken in it's use */
  
  corrRL = ((targetRL - accRL) * 0.0)
         + gyroRL;
  corrFB = ((targetFB - accFB) * 0.25)
         + gyroFB;

  auto correction = BalanceOffset();
  
  correction.hipRoll    = static_cast<short>(round(-corrRL * d_hipRollGain->getValue()));
  correction.knee       = static_cast<short>(round(-corrFB * d_kneeGain->getValue()));
  correction.anklePitch = static_cast<short>(round( corrFB * d_anklePitchGain->getValue()));
  correction.ankleRoll  = static_cast<short>(round(-corrRL * d_ankleRollGain->getValue()));

  return correction;
}
