// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>

namespace bold
{
  enum class BalanceMode
  {
    None = 0,
    Gyro = 1,
    Orientation = 2,
    COP = 3
  };

  struct BalanceOffset
  {
    int16_t hipPitch;
    int16_t hipRoll;
    int16_t knee;
    int16_t anklePitch;
    int16_t ankleRoll;
  };

  class Balance
  {
  public:
    virtual BalanceOffset computeCorrection(double targetPitchRads) = 0;

    virtual void reset() {}
  };
}
