// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "orientationbalance.hh"

#include "config/Config/config.hh"
#include "state/State/state.hh"
#include "state/StateObject/OrientationState/orientationstate.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::state;

OrientationBalance::OrientationBalance()
{
  d_kneeGain       = Config::getSetting<double>("balance.orientation.knee-gain");
  d_anklePitchGain = Config::getSetting<double>("balance.orientation.ankle-pitch-gain");
  d_hipRollGain    = Config::getSetting<double>("balance.orientation.hip-roll-gain");
  d_ankleRollGain  = Config::getSetting<double>("balance.orientation.ankle-roll-gain");
}

BalanceOffset OrientationBalance::computeCorrection(double targetPitchRads)
{
  auto orientation = State::get<OrientationState>();

  ASSERT(orientation);

  double rollError = -orientation->getRollAngle();
  double pitchError = -targetPitchRads - orientation->getPitchAngle();

  std::cout << "targetPitch=" << (int)Math::radToDeg(targetPitchRads)
            << " currPitch="  << (int)Math::radToDeg(-orientation->getPitchAngle())
            << " pitchError=" << (int)Math::radToDeg(pitchError)
            << " rollErr="    << (int)Math::radToDeg(rollError)
            << std::endl;

  auto correction = BalanceOffset();

  correction.hipRoll    = static_cast<short>(round(-rollError * d_hipRollGain->getValue()));
  correction.knee       = static_cast<short>(round(-pitchError * d_kneeGain->getValue()));
  correction.anklePitch = static_cast<short>(round( pitchError * d_anklePitchGain->getValue()));
  correction.ankleRoll  = static_cast<short>(round(-rollError * d_ankleRollGain->getValue()));

  return correction;
}
