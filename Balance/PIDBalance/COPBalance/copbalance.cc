// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "copbalance.hh"

#include "state/State/state.hh"
#include "state/StateObject/FSRState/fsrstate.hh"
#include "state/StateObject/BodyState/bodystate.hh"

#include <Eigen/Core>
#include <numeric>

using namespace bold;
using namespace bold::state;
using namespace bold::util;
using namespace std;
using namespace Eigen;

COPBalance::COPBalance()
  : PIDBalance{"cop"}
{
}

double COPBalance::determineError()
{
  auto fsrState = State::get<FSRState>();
  if (!fsrState)
    return 0.0;

  auto bodyState = State::get<BodyState>();

  auto centers = vector<Vector2d, Eigen::aligned_allocator<Vector2d>>{};

  auto maybeCenterL = fsrState->getCenterL();
  auto maybeCenterR = fsrState->getCenterR();

  if (maybeCenterL.hasValue())
    centers.push_back( maybeCenterL.value());

  if (maybeCenterR.hasValue())
    centers.push_back(maybeCenterR.value());

  auto COP = accumulate(begin(centers), end(centers), Vector2d{0,0});
  if (centers.size() > 0)
    COP /= centers.size();

  return COP.y();
}
