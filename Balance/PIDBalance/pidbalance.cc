// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "pidbalance.hh"

#include "config/Config/config.hh"
#include "src/util/Log/log.hh"
#include "MX28/mx28.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::util;
using namespace std;

PIDBalance::PIDBalance(string const& name)
  : d_controller{0.0, 0.0, 0.0}
{
  Config::getSetting<double>("balance." + name +".p-gain")->track([this](double v) { d_controller.setPGain(v); });
  Config::getSetting<double>("balance." + name +".i-gain")->track([this](double v) { d_controller.setIGain(v); });
  Config::getSetting<double>("balance." + name +".d-gain")->track([this](double v) { d_controller.setDGain(v); });
  Config::getSetting<double>("balance." + name +".i-max")->track([this](double v) { d_controller.setIntegralAbsMax(v); });
}

BalanceOffset PIDBalance::computeCorrection(double targetPitchRads)
{
  auto error = determineError();

  auto control = d_controller.step(error); 

  Log::verbose("PIDBalance::computeCorrection") << "Control: \t" << control;

  auto offsets = BalanceOffset({0});
  offsets.hipPitch = MX28::radsDelta2ValueDelta(control);

  return offsets;
}

