// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Balance/balance.hh"
#include "PIDController/pidcontroller.hh"

namespace bold
{
  class PIDBalance : public Balance
  {
  public:
    /**
     * @param name Name of this controller, used to look up PID settings. The following settings are required:
     *    balance.name.p-gain
     *    balance.name.i-gain
     *    balance.name.d-gain
     *    balance.name.i-max
     */
    PIDBalance(std::string const& name);

    virtual ~PIDBalance() = default;

    BalanceOffset computeCorrection(double targetPitchRads) override;
    
    void reset() override { d_controller.reset(); }

  protected:
    /** Determine control error
     *
     * @returns error to be fed into PID controller. Higher error (roughly) results in increase in hip pitch.
     */
    virtual double determineError() = 0;
    
  private:
    PIDController d_controller;
  };
}
