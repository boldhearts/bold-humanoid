// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>

namespace bold
{
  enum class ThreadId
  {
    Main = 1,
    MotionLoop = 2,
    ThinkLoop = 3,
    DataStreamer = 4
  };

  class ThreadUtil
  {
  public:
    static ThreadId getThreadId() { return d_threadId; }
    static void setThreadId(ThreadId threadId);

    static bool isMotionLoopThread(bool logError = true);
    static bool isThinkLoopThread(bool logError = true);
    static bool isDataStreamerThread(bool logError = true);

    static std::string getThreadName();

  private:
    static thread_local ThreadId d_threadId;
  };
}
