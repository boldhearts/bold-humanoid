// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "threadutil.hh"

#include "src/util/Log/log.hh"

#include <sstream>
#include <pthread.h>

using namespace bold;
using namespace bold::util;
using namespace std;

thread_local ThreadId ThreadUtil::d_threadId;

void ThreadUtil::setThreadId(ThreadId threadId)
{
  d_threadId = threadId;

  if (pthread_setname_np(pthread_self(), getThreadName().c_str()) != 0)
    Log::error("ThreadUtil::setThreadId") << "Error setting thread name";
}

std::string ThreadUtil::getThreadName()
{
  switch (d_threadId)
  {
    case ThreadId::MotionLoop: return "Motion Loop";
    case ThreadId::ThinkLoop: return "Think Loop";
    case ThreadId::DataStreamer: return "Data Streamer";
    case ThreadId::Main: return "Main";
    default:
      stringstream s;
      s << "Unknown (" << (int)d_threadId << ")";
      return s.str();
  }
}

bool ThreadUtil::isMotionLoopThread(bool logError)
{
  if (d_threadId == ThreadId::MotionLoop)
    return true;
  if (logError)
    Log::error("ThreadUtil::isMotionLoopThread") << "Expected Motion Loop thread but was: " << getThreadName();
  return false;
}

bool ThreadUtil::isThinkLoopThread(bool logError)
{
  if (d_threadId == ThreadId::ThinkLoop)
    return true;
  if (logError)
    Log::error("ThreadUtil::isThinkLoopThread") << "Expected Think Loop thread but was: " << getThreadName();
  return false;
}

bool ThreadUtil::isDataStreamerThread(bool logError)
{
  if (d_threadId == ThreadId::DataStreamer)
    return true;
  if (logError)
    Log::error("ThreadUtil::isDataStreamerThread") << "Expected Data Streamer thread but was: " << getThreadName();
  return false;
}
