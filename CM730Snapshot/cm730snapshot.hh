// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <Eigen/Core>

#define HW_CM730_GYRO_MIN -512
#define HW_CM730_GYRO_MID    0
#define HW_CM730_GYRO_MAX  512

namespace bold
{
  class BulkReadTable;
  class GyroModel;
  class AccelerometerModel;

  // http://support.robotis.com/en/product/darwin-op/references/reference/hardware_specifications/electronics/sub_controller_(cm-730).htm

  // TODO rename as CM730State

  class CM730Snapshot
  {
  public:
    bool isPowered;
    /// Red LED
    bool isLed2On;
    /// Blue LED
    bool isLed3On;
    /// Green LED
    bool isLed4On;
    Eigen::Vector3d eyeColor;
    Eigen::Vector3d foreheadColor;
    bool isModeButtonPressed;
    bool isStartButtonPressed;
    /// The converted gyroscope output, in radians per second.
    Eigen::Vector3d gyro;
    /// The converted accelerometer output, in gs.
    Eigen::Vector3d acc;
    float voltage;
    /// Raw raw value of the gyroscope, in range [0,1023] corresponding to [-1600,1600] degrees per second.
    Eigen::Vector3i gyroRaw;
    /// Raw raw value of the accelerometer, in range [0,1023] corresponding to [-4,4] g.
    Eigen::Vector3i accRaw;

    CM730Snapshot() = default;

    CM730Snapshot(BulkReadTable const& data,
                  std::array<GyroModel, 3> const& gyroModels, AccelerometerModel const& accelerometerModel);

    /** Returns the gyroscope value, in hardware units, but balanced around the midpoint.
     *
     * Values may be positive or negative.
     */
    Eigen::Vector3i getBalancedGyroValue() const
    {
      return this->gyroRaw - Eigen::Vector3i(HW_CM730_GYRO_MAX, HW_CM730_GYRO_MAX, HW_CM730_GYRO_MAX);
    }
  };

  class StaticCM730State
  {
  public:
    unsigned short modelNumber;
    unsigned char firmwareVersion;
    unsigned char dynamixelId;
    unsigned int baudBPS;
    unsigned int returnDelayTimeMicroSeconds;

    /** Controls when a status packet is returned in response to an instruction.
    *
    * 0 - only for PING command
    * 1 - only for READ command
    * 2 - for all commands
    *
    * Note that a status packet is never returned for broadcast instructions.
    */
    unsigned char statusRetLevel;

    // skip dynamic addresses in the table -- they are captured in CM730Snapshot

    StaticCM730State() = default;

    StaticCM730State(BulkReadTable const& data);
  };
}
