// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/public/session.h"

#include "vision/ImageLabeller/TFImageLabeller/tfimagelabeller.hh"

#include "util/Log/log.hh"
#include "util/LogAppender/logappender.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "vision/ImageSampleMap/imagesamplemap.hh"
#include "vision/LabelledImageData/labelledimagedata.hh"
#include "vision/LabelledImageProcessor/CartoonPainter/cartoonpainter.hh"

#include "Clock/clock.hh"
#include "ImageCodec/PngCodec/pngcodec.hh"

#include <opencv2/opencv.hpp>

using namespace tensorflow;

using bold::geometry2::LineSegment2i;
using bold::geometry2::Point2i;
using bold::util::Log;
using bold::util::ConsoleLogAppender;
using bold::util::SequentialTimer;
using bold::vision::TFImageLabeller;
using bold::vision::ImageSampleMap;
using bold::vision::CartoonPainter;

int main(int argc, char *argv[])
{
  Log::addAppender(std::make_shared<ConsoleLogAppender>());
  auto clock = std::make_shared<bold::SystemClock>();
  
  tensorflow::port::InitMain(argv[0], &argc, &argv);

  Log::info() << "Starting";

  std::string graphFileName = std::string{argv[1]};
  std::string inputLayer = std::string{argv[2]};
  std::string outputLayer = std::string{argv[3]};
  std::string imageFileName = std::string{argv[4]};
  auto nIters = atoi(argv[5]);
  
  Log::info() << "Creating labeller with graph: " << graphFileName << "...";
  
  auto labeller = TFImageLabeller{graphFileName, inputLayer, outputLayer};

  Log::info() << "Reading image: " << imageFileName << "...";
  auto codec = bold::PngCodec{false};
  auto colourImage = cv::Mat{};
  codec.read(imageFileName, colourImage);
  //cv::Mat colourImage = cv::imread(imageFileName, CV_LOAD_IMAGE_COLOR);

  Log::info() << colourImage.cols << " x " << colourImage.rows;

  auto width = colourImage.cols;
  auto height = colourImage.rows;

  auto sampleMap = ImageSampleMap::all(width, height);
  auto horizon = LineSegment2i{Point2i{0, height}, Point2i{width, height}};
  
  Log::info() << "Labelling...";
  auto timer = SequentialTimer{clock};
  timer.enter("label");
  auto result = labeller.labelImage(colourImage, sampleMap, horizon, &timer);
  timer.exit();
  
  auto eventTimes = timer.flush();
  for (auto const& timing : *eventTimes)
    Log::info() << timing.second << "\t" << timing.first;
  
  for (int i = 0; i < nIters; ++i)
  {
    auto timer = SequentialTimer{clock};
    timer.enter("label");
    auto result = labeller.labelImage(colourImage, sampleMap, horizon, &timer);
    timer.exit();
    
    auto ets = timer.flush();
    eventTimes->insert(eventTimes->end(), ets->begin(), ets->end());
    for (auto const& timing : *ets)
      Log::info() << timing.second << "\t" << timing.first;
  }

  auto timingsByPrefix = std::multimap<std::string, double>{};
  timingsByPrefix.insert(eventTimes->begin(), eventTimes->end());
  auto keyEnd = timingsByPrefix.begin();

  Log::info("TIMING") << "step\tmean\tstd";
  for (auto keyBegin = timingsByPrefix.begin(); keyBegin != timingsByPrefix.end();
       keyBegin = keyEnd)
  {
    keyEnd = timingsByPrefix.upper_bound(keyBegin->first);
    auto n = std::distance(keyBegin, keyEnd);

    auto timings = std::vector<double>(n);
    std::transform(keyBegin, keyEnd, timings.begin(),
                   [](std::pair<std::string, double> const& t) { return t.second; });
    auto sum = std::accumulate(timings.begin(), timings.end(), 0.0);
    auto mean = sum / n;
    auto diffs = std::vector<double>(n);
    std::transform(timings.begin(), timings.end(), diffs.begin(),
                   [mean](double t) { return t - mean; });
    auto squaredSum = std::inner_product(diffs.begin(), diffs.end(), diffs.begin(), 0.0);
    auto std = std::sqrt(squaredSum / n);

    Log::info("TIMING") << keyBegin->first << "\t" << mean << "\t" << std;
  }
  
  Log::info() << "Drawing image...";
  auto painter = CartoonPainter{uint16_t(width), uint16_t(height)};
  painter.process(result, nullptr);

  auto const& cartoon = painter.mat();
  cv::imwrite("labelled.png", cartoon);
  
  Log::info() << "Done!";
}
