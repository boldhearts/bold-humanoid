// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DisjointSet/disjointset.hh"
#include <iostream>
#include <cmath>

using namespace bold;
using namespace std;

#define N 1000000

bool flipCoin()
{
  return (rand() % 20) == 0;
}

int main()
{
  // TODO convert this into unit tests

  DisjointSet<int> set;

  vector<int> ints(N);
  iota(ints.begin(), ints.end(), 1);
  random_shuffle(ints.begin(), ints.end());

  int someSeenEven = -1;
  int someSeenOdd = -1;
  for (unsigned i = 0; i < N; ++i)
  {
    set.insert(ints[i]);

    if (ints[i] % 2 == 0)
    {
      if (someSeenEven != -1)
        set.merge(ints[i], someSeenEven);
      if (someSeenEven == -1 || flipCoin())
        someSeenEven = ints[i];
    }

    if (ints[i] % 2 != 0)
    {
      if (someSeenOdd != -1)
        set.merge(ints[i], someSeenOdd);
      if (someSeenOdd == -1 || flipCoin())
        someSeenOdd = ints[i];
    }
  }

  auto subSets = set.getSubSets();
  cout << "Number of subsets: " << subSets.size() << endl;
  for (auto subSet : subSets)
    cout << "Size: " << subSet.size() << endl;

/*

  auto elements = set.getElements();
  cout << "Number of elements: " << elements.size() << endl;

  auto subsets = set.getSubSets();

  cout << "Number of subsets: " << subsets.size() << endl;
*/
}
