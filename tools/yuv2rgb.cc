// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>

#include "ImageCodec/PngCodec/pngcodec.hh"
#include "src/colour/colour.hh"
#include <opencv2/opencv.hpp>

using namespace std;
using namespace bold;

void printUsage(string const& name);

int main(int argc, char const** argv)
{
  cout << argc << endl;

  if (argc < 2)
  {
    printUsage(argv[0]);
    return -1;
  }

  auto codec = PngCodec{};
  auto yuvImage = cv::Mat{};

  auto inFileName =  string{argv[1]};
  auto res = codec.read(inFileName, yuvImage);

  if (!res)
  {
    cout << "Failed reading image " << argv[1] << endl;
    return -1;
  }

  auto bgrImage = yuvImage.clone();
  
  for (int i = 0; i < bgrImage.rows; ++i)
    for (auto ptr = bgrImage.ptr(i); ptr < bgrImage.ptr(i) + 3 * bgrImage.cols; ptr += 3)
      colour::yCbCrToBgrInPlace(ptr);

  auto base = inFileName.substr(0, inFileName.find_last_of('.'));

  auto outFileName = base + "-rgb.png";
  cout << outFileName << endl;
  cv::imwrite(outFileName, bgrImage);
}


void printUsage(string const& name)
{
  cout << "Usage: " << name << " yuvimage.png" << endl;
}
