// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "motion/scripts/MotionScript/motionscript.hh"

#include <iostream>

using namespace std;
using namespace bold::motion::scripts;

int main(int argc, char** argv)
{
  if (argc < 3)
  {
    cerr << "Usage: " << argv[0] << " <motionscript.json> <script name>\n";
    return -1;
  }

  auto script = MotionScript::fromFile(argv[1]);

  script->getMirroredScript(string(argv[2]))->writeJsonFile(string(string(argv[1]) + ".mirrored"));
}
