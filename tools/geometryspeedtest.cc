// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "src/geometry2/Point/point.hh"

#include <Eigen/Geometry>
#include <iostream>
#include <chrono>

using namespace bold::geometry2;
using namespace Eigen;
using namespace std;

int main(int argc, char const* const* const argv)
{
  auto n = 1000u;
  if (argc == 2)
    n = std::atoi(argv[1]);
  
  auto vec = Vector2d(1.0, 1.0);
  auto point = Point2d(1.0, 1.0);

  auto rotation = Rotation2Dd(.5 * M_PI);

  auto clock = chrono::system_clock();
  auto start = clock.now();
  
  for (unsigned i = 0; i < n; ++i)
    vec = rotation * vec;

  auto loopend1 = clock.now();
  auto duration1 = loopend1 - start;
  
  cout << "Vector transform score (lower is better):\t" << duration1.count() << endl;

  start = clock.now();
  
  for (unsigned i = 0; i < n; ++i)
    point = Point2d::ORIGIN + rotation * (point - Point2d::ORIGIN);

  auto loopend2 = clock.now();
  auto duration2 = loopend2 - start;

  cout << "Point transform score (lower is better):\t" << duration2.count() << endl;
}
