// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <opencv2/opencv.hpp>
#include <chrono>
#include <iostream>

#include "vision/ImageLabeller/PixelLabeller/LUTImageLabeller/lutimagelabeller.hh"
#include "vision/IntegralImage/integralimage.hh"
#include "vision/ImageLabeller/imagelabeller.hh"
#include "vision/PixelLabel/RangePixelLabel/rangepixellabel.hh"
#include "util/memory.hh"

using namespace std;
using namespace bold;
using namespace bold::colour;
using namespace bold::vision;

unsigned timeOpenCV(cv::Mat image, unsigned nIter)
{
  auto clock = chrono::system_clock();
  auto opencvStart = clock.now();
  cv::Mat cvOutImage = cv::Mat(641, 461, CV_32SC1);

  for (unsigned i = 0; i < nIter; ++i)
    cv::integral(image, cvOutImage);
  
  auto opencvEnd = clock.now();
  auto opencvDuration = opencvEnd - opencvStart;

  auto timePerIter = opencvDuration.count() / nIter;
  cout << "OpenCV took:\t" << timePerIter << "\n";
  return timePerIter;
}

unsigned timeBoldFromImage(cv::Mat image, unsigned nIter)
{
  auto clock = chrono::system_clock();
  auto boldStart = clock.now();
  auto integralImage = make_unique<IntegralImage>();
  
  for (unsigned i = 0; i < nIter; ++i)
    integralImage = IntegralImage::create(image, move(integralImage));
  auto boldEnd = clock.now();
  auto boldDuration = boldEnd - boldStart;

  auto timePerIter = boldDuration.count() / nIter;
  cout << "Bold from image took:\t" << timePerIter << "\n";
  return timePerIter;
}

unsigned timeBoldFromLabels(cv::Mat image, unsigned nIter, ImageSampleMap const& sampleMap)
{
  // Label image
  auto label1 = make_shared<RangePixelLabel>("foo",
                                             LabelClass::GOAL,
                                             HSVRange(0, 128, 0, 128, 0, 128));
  auto labeller = make_unique<LUTImageLabeller<18>>(vector<shared_ptr<PixelLabel>>{{label1}});
  auto horizon = geometry2::LineSegment2i{geometry2::Point2i{0, 480}, geometry2::Point2i{639, 480}};
  auto labelData = labeller->labelImage(image, sampleMap, horizon);

  auto clock = chrono::system_clock();
  auto boldStart = clock.now();
  auto integralImage = make_unique<IntegralImage>();
  
  for (unsigned i = 0; i < nIter; ++i)
    integralImage = IntegralImage::create(labelData, LabelClass::GOAL, move(integralImage));
  
  auto boldEnd = clock.now();
  auto boldDuration = boldEnd - boldStart;

  auto timePerIter = boldDuration.count() / nIter;
  cout << "Bold from labels took:\t" << timePerIter << "\n";
  return timePerIter;
}

int main()
{
  cout << "OpenCV SSE supported: " << cv::checkHardwareSupport(CV_CPU_SSE2) << "\n";

  cv::setUseOptimized(true);
  
  unsigned nIter = 10000;

  // Create random image
  cv::Mat image = cv::Mat(480, 640, CV_8UC1);
  cv::randu(image, cv::Scalar::all(0), cv::Scalar::all(255));

  auto opencvDuration = timeOpenCV(image, nIter);
  auto boldDuration = timeBoldFromImage(image, nIter);
  cout << "Time saved: " << 100.0 * (1.0 - 1.0 * boldDuration / opencvDuration) << " %\n";

  auto boldFromLabelsFullDuration = timeBoldFromLabels(image, nIter, ImageSampleMap::all(640, 480));
  auto boldFromLabelsHalfDuration = timeBoldFromLabels(image, nIter, ImageSampleMap::half(640, 480));
  
}
