// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <time.h>
#include <sys/time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "Clock/clock.hh"
#include "vision/CameraModel/cameramodel.hh"
#include "vision/LabelledImageProcessRunner/labelledimageprocessrunner.hh"
#include "vision/LabelledImageProcessor/BlobDetector/blobdetector.hh"
#include "vision/LabelledImageProcessor/CartoonPainter/cartoonpainter.hh"
#include "vision/LabelledImageProcessor/LabelCounter/labelcounter.hh"
#include "vision/LabelledImageProcessor/LineDotter/linedotter.hh"
#include "LineRunTracker/lineruntracker.hh"
#include "LineFinder/MaskWalkLineFinder/maskwalklinefinder.hh"
#include "LineFinder/ScanningLineFinder/scanninglinefinder.hh"
#include "Painter/painter.hh"
#include "PixelFilterChain/pixelfilterchain.hh"
#include "util/SequentialTimer/sequentialtimer.hh"
#include "state/State/state.hh"
#include "state/StateObject/LabelCountState/labelcountstate.hh"

#include "geometry2/Geometry/LineSegment/linesegment.hh"
#include "util/meta.hh"
#include "vision/ImageLabeller/PixelLabeller/LUTImageLabeller/lutimagelabeller.hh"
#include "vision/ImageSampleMap/imagesamplemap.hh"
#include "vision/PixelLabel/RangePixelLabel/rangepixellabel.hh"
#include "vision/PixelLabel/HistogramPixelLabel/histogrampixellabel.hh"

using namespace std;
using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace Eigen;

/*
void doHistogramPixels(cv::Mat const& img)
{
  HistogramPixelLabel<5> label{"label"};


  for (int y = 0; y < img.rows; ++y)
  {
    auto const* row = img.ptr<uint8_t>(y);
    for (int x = 0; x < img.cols; x += img.channels())
    {
      colour::BGR bgr;
      bgr.b = row[x + 0];
      bgr.g = row[x + 1];
      bgr.r = row[x + 2];

      colour::HSV hsv = colour::BGR2hsv(bgr);
      label.addSample(hsv);
    }
  }

/ *
  auto rng = Math::createNormalRng(128, 64);

  for (unsigned i = 0; i < 1000000; ++i)
  {
    colour::HSV hsv;
    hsv.h = rng();
    hsv.s = rng();
    hsv.v = rng();
    label.addSample(hsv);
  }
* /

  auto hsImg = label.getHSImage();
  cv::imwrite("hsimg.png", hsImg);
  auto hvImg = label.getHVImage();
  cv::imwrite("hvimg.png", hvImg);
  auto svImg = label.getSVImage();
  cv::imwrite("svimg.png", svImg);
}

*/

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    cout <<" Usage: passtest <rgb-image> [configuration]" << endl;
    return -1;
  }

  string configurationFile = "configuration.json";
  if (argc > 2)
    configurationFile = string(argv[2]);
  cout << "Using config file: " << configurationFile << endl;

  Config::initialise("configuration-metadata.json", configurationFile);

  int loopCount = 100;

  auto inputFileName = argv[1];

  // Load the BGR image
  cout << "Reading " << inputFileName << endl;
  cv::Mat colourImage = cv::imread(inputFileName, CV_LOAD_IMAGE_COLOR);

  if (!colourImage.data)
  {
    cout << "Could not open or find the image" << endl;
    return -1;
  }

  // Convert YCbCr to BGR
  PixelFilterChain chain;
  chain.pushFilter(&colour::yCbCrToBgrInPlace);
  chain.applyFilters(colourImage);

  //doHistogramPixels(colourImage);

  // Initialise random seed
  std::srand(unsigned(std::time(0)));

  uint16_t imageWidth = colourImage.cols;
  uint16_t imageHeight = colourImage.rows;

  //
  // FIXED START UP INITIALISATION
  //

  auto clock = make_shared<SystemClock>();

  auto t = clock->getTimestamp();

  // Build colour ranges for segmentation
  auto goalLabel  = make_shared<RangePixelLabel>("Goal",  LabelClass::GOAL, Config::getValue<colour::HSVRange>("vision.pixel-labels.goal"));
  auto ballLabel  = make_shared<RangePixelLabel>("Ball",  LabelClass::BALL, Config::getValue<colour::HSVRange>("vision.pixel-labels.ball"));
  auto fieldLabel = make_shared<RangePixelLabel>("Field", LabelClass::FIELD, Config::getValue<colour::HSVRange>("vision.pixel-labels.field"));
  auto lineLabel  = make_shared<RangePixelLabel>("Line",  LabelClass::LINE, Config::getValue<colour::HSVRange>("vision.pixel-labels.line"));
  auto cyanLabel  = make_shared<RangePixelLabel>("Cyan",  LabelClass::CYAN, Config::getValue<colour::HSVRange>("vision.pixel-labels.cyan"));
  auto magentaLabel  = make_shared<RangePixelLabel>("Magenta", LabelClass::MAGENTA, Config::getValue<colour::HSVRange>("vision.pixel-labels.magenta"));

  cout << "Using labels:" << endl
       << "  " << *ballLabel << endl
       << "  " << *goalLabel << endl
       << "  " << *fieldLabel << endl
       << "  " << *lineLabel << endl
       << "  " << *cyanLabel << endl
       << "  " << *magentaLabel << endl;

  vector<shared_ptr<PixelLabel>> labels = { goalLabel, ballLabel, fieldLabel, lineLabel, cyanLabel, magentaLabel };

  State::registerStateType<LabelCountState>("LabelCount");

  // Resources for labelling
  // TODO: this will crash
  auto imageLabeller = new LUTImageLabeller<18>(labels);

  const vector<shared_ptr<PixelLabel>> blobPixelLabels = { ballLabel, goalLabel, cyanLabel, magentaLabel };
  auto blobDetectPass = make_shared<BlobDetector>(imageWidth, imageHeight, blobPixelLabels);

  // Resources for finding line dots
  auto lineDotPass = make_shared<LineDotter>(imageWidth, fieldLabel, lineLabel);

  // Resources for creating a labelled image
  auto cartoonPass = make_shared<CartoonPainter>(imageWidth, imageHeight);

  // Resources for counting the number of labels
  auto labelCountPass = make_shared<LabelCounter>(labels);

  // Build the pass runner
  LabelledImageProcessRunner passRunner;
  passRunner.addHandler(lineDotPass);
  passRunner.addHandler(blobDetectPass);
  passRunner.addHandler(cartoonPass);
  passRunner.addHandler(labelCountPass);

  MaskWalkLineFinder maskWalkLineFinder;

  auto cameraModel = allocate_aligned_shared<CameraModel>();
  ScanningLineFinder scanningLineFinder(cameraModel);

  cout << "Startup took " << clock->getMillisSince(t) << " ms" << endl;

  SequentialTimer timer{clock};

  auto granularityFunction = [](uint16_t y) { uint8_t g = y/100 + 1; return Matrix<uint8_t,2,1>(g, g); };
  ImageSampleMap sampleMap(granularityFunction, imageWidth, imageHeight);

  //
  // IMAGE LABELLING
  //
  auto horizon = LineSegment2i{Point2i{0, colourImage.rows}, Point2i{colourImage.cols - 1, colourImage.rows}};
  t = clock->getTimestamp();
  for (int i = 0; i < loopCount; i++)
    imageLabeller->labelImage(colourImage, sampleMap, horizon, &timer);
  cout << "Labelled " << loopCount << " times. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl;

  auto labelData = imageLabeller->labelImage(colourImage, sampleMap, horizon, &timer);

  //
  // IMAGE PASS
  //

  t = clock->getTimestamp();
  for (int i = 0; i < loopCount; i++)
    passRunner.run(labelData, &timer);
  cout << "[simple pass] Passed " << loopCount << " times. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl;

  //
  // DETECT BLOBS
  //
  t = clock->getTimestamp();
  for (int i = 0; i < loopCount; i++)
    blobDetectPass->detectBlobs(timer);
  cout << "BlobDetectPass::detectBlobs ran " << loopCount << " times. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl;
  

  //
  // FIND LINES (MaskWalkLineFinder)
  //
  t = clock->getTimestamp();
  LineSegment2i::Vector maskWalkLines;
  for (int i = 0; i < loopCount; i++)
    maskWalkLines = maskWalkLineFinder.findLineSegments(lineDotPass->lineDots);
  cout << "MaskWalkLineFinder   ran " << loopCount << " times. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl;

  //
  // FIND LINES (ScanningLineFinder)
  //
  t = clock->getTimestamp();
  LineSegment2i::Vector scanningLines;
  for (int i = 0; i < loopCount; i++)
    scanningLines = scanningLineFinder.findLineSegments(lineDotPass->lineDots);
  cout << "ScanningLineFinder   ran " << loopCount << " times. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl;


  //
  // PRINT SUMMARIES
  //
  cout << "Finished " << loopCount << " passes. Average time: " << (clock->getMillisSince(t)/loopCount) << " ms" << endl
       << "Found:" << endl
       << "    " << lineDotPass->lineDots.size() << " line dots" << endl;

  auto blobsByLabel = blobDetectPass->getDetectedBlobs();
  for (auto const& pixelLabel : blobPixelLabels)
    cout << "    " << blobsByLabel[pixelLabel].size() << " " << pixelLabel->getName() << " blob(s)" << endl;
  //for (auto const& pair : labelCountPass->getCounts())
  //  cout << "    " << pair.second << " " << pair.first->getName() << " pixels" << endl;

  //
  // DRAW LABELLED 'CARTOON' IMAGE
  //
  imwrite("labelled.png", cartoonPass->mat());

  //
  // DRAW OUTPUT IMAGE
  //

  vector<colour::BGR> colours = {
    colour::BGR::blue(),
    colour::BGR::green(),
    colour::BGR::red(),
    colour::BGR::yellow(),
    colour::BGR::magenta(),
    colour::BGR::cyan()
  };

  // Draw line segments
//   LineSegment2i::Vector segments = lineFinder.find(lineDotPass->lineDots);
//   cout << "    " << segments.size() << " line segments" << endl;
   for (LineSegment2i const& segment : scanningLines)
   {
     cout << segment << endl;
     Painter::draw(segment, colourImage, colour::BGR(192,0,0).toScalar(), 2);
   }

  // Draw line dots
//   cv::Mat lineDotImageGray(colourImage.size(), CV_8U);
//   lineDotImageGray = Scalar(0);
  if (lineDotPass->lineDots.size() != 0)
  {
    cv::Mat lineDotImageColour = cv::Mat::zeros(colourImage.size(), CV_8UC3);
    colour::BGR red(0, 0, 255);
    for (Vector2i const& lineDot : lineDotPass->lineDots)
    {
//       lineDotImageGray.at<uint8_t>(lineDot.y(), lineDot.x()) = 255;
      colourImage.at<colour::BGR>(lineDot.y(), lineDot.x()) = red;
      lineDotImageColour.at<colour::BGR>(lineDot.y(), lineDot.x()) = red;
    }
    imwrite("line-dots.png", lineDotImageColour);
//     imwrite("line-dots-gray.bmp", lineDotImageGray);
  }

  // Draw lines
  cout << "    " << maskWalkLines.size() << " line(s) via MaskWalkLineFinder" << endl;
  for (LineSegment2i const& line : maskWalkLines)
  {
    cout << "      " << line << endl;
    //line.draw(colourImage, colours[colourIndex++ % colours.size()], 2);
  }
  cout << "    " << scanningLines.size() << " line(s) via ScanningLineFinder" << endl;
  for (LineSegment2i const& line : scanningLines)
  {
    cout << "      " << line << endl;
    //line.draw(colourImage, colours[colourIndex++ % colours.size()], 2);
  }

  // Draw blobs
  for (auto const& pixelLabel : blobPixelLabels)
    for (bold::vision::Blob const& blob : blobsByLabel[pixelLabel])
    {
      auto rect = cv::Rect(blob.bounds().min().x(), blob.bounds().min().y(),
                           blob.bounds().sizes().x(), blob.bounds().sizes().y());
      auto blobColor = pixelLabel->modalColour().toBGR()/*.invert()*/.toScalar();
      cv::rectangle(colourImage, rect, blobColor);
    }

  // Save output image
  imwrite("output.png", colourImage);

//   // Try using OpenCV's line detection
//   vector<Vec4i> cvLines;
//   cv::HoughLinesP(lineDotImageGray, cvLines, 2, 0.5*CV_PI/180, 10, 30, 40);
//   cout << "    " << cvLines.size() << " line(s) via OpenCV" << endl;
//   colourIndex = 0;
//   for (auto const& line : cvLines)
//   {
//     cv::line(colourImage,
//              Point(line[0], line[1]),
//              Point(line[2], line[3]),
//              colours[colourIndex++ % colours.size()].toScalar());//, 1, CV_AA);
//   }
//   imwrite("cv-lines.png", colourImage);
//   waitKey();

  return 0;
}
