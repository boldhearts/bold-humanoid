#!/usr/bin/env bash

$([ -f active-deployment ] && cat active-deployment || echo .)/boldhumanoid >> log/log-$(date "+%Y-%m-%d") 2>&1
