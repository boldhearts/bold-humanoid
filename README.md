![Bold Hearts Logo](https://robocuplab.herts.ac.uk/boldhearts/bold-humanoid/wikis/bold-hearts-logo-640x289.png)

# bold-humanoid

This is the main source repository for the Bold Hearts's kid-size league agent.

See the [wiki](https://robocuplab.herts.ac.uk/boldhearts/bold-humanoid/wikis/home) for more details.