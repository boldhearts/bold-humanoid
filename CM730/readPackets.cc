// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cm730.ih"

#include "TxRxPacket/txpacket.hh"

int findPacketHeaderIndex(const uint8_t* data, unsigned length)
{
  // The packet must start with 0xFFFF. Walk through until we find it.

  bool hasFlag = false;

  for (unsigned index = 0; index < length; index++)
  {
    if (data[index] == 0xFF)
    {
      if (hasFlag)
        return index - 1;
      hasFlag = true;
      continue;
    }
    else
    {
      hasFlag = false;
    }
  }

  return -1;
}

CommResult CM730::readPackets(uint8_t* buffer, const unsigned bufferLength, std::function<bool(uint8_t const*)> callback)
{
  unsigned receivedCount = 0;

  while (true)
  {
    const unsigned bytesNeeded = bufferLength - receivedCount;

    Log::trace("CM730::readPackets") << "Bytes needed: " << bytesNeeded;

    if (bytesNeeded != 0)
    {
      int bytesRead = d_platform->readPort(&buffer[receivedCount], bytesNeeded);

      if (bytesRead > 0)
      {
        auto l = Log::trace("CM730::readPackets");
        l << " RX[" << bytesRead << "]" << hex << setfill('0');
        for (int n = 0; n < bytesRead; n++)
          l << " " << setw(2) << (int)buffer[receivedCount + n];
      }

      if (bytesRead < 0)
      {
        Log::error("CM730::readPackets") << "Error reading from CM730: " << strerror(errno) << " (" << errno << ")";
        return CommResult::TX_FAIL;
      }
      else
      {
        receivedCount += bytesRead;
      }
    }

    if (receivedCount == (uint)bufferLength)
    {
      // We have the required number of bytes. Now check if it looks valid.

      int i = findPacketHeaderIndex(buffer, bufferLength);

      if (i == -1)
      {
        Log::warning("CM730::readPackets") << "No header found in response data";
        return CommResult::RX_CORRUPT;
      }

      if (i != 0)
      {
        // Move bytes forwards in buffer, so that the header is aligned in byte zero.
        // Note that where multiple packets are located consecutively, we only perform this adjustment
        // for the first one.
        std::copy(
          &buffer[i],
          &buffer[bufferLength + 1],
          &buffer[0]);

        // Reduce the count of bytes received, so that we request more bytes when we loop around again
        receivedCount -= i;
        continue;
      }

      // We've found our first packet to start processing the message

      ASSERT(i == 0);

      uint8_t const* head = buffer;
      unsigned bytesLeft = bufferLength;

      while (bytesLeft != 0)
      {
        // Validate checksum
        auto length = head[TxRxPacket::ADDR_LENGTH];
        auto calculatedChecksum = binary::calculateChecksum(head + 2, head + TxRxPacket::ADDR_INSTRUCTION + length  - 1);

        if (findPacketHeaderIndex(head, bufferLength) != 0)
        {
          Log::warning("CM730::readPackets") << "Invalid packet header";
          return CommResult::RX_CORRUPT;
        }

        if (calculatedChecksum == - 1)
        {
          Log::warning("CM730::readPackets") << "Invalid packet length advertised";
          return CommResult::RX_CORRUPT;
        }

        // Now that the length has been verified, look it up
        auto observedChecksum = head[TxPacketBase::ADDR_LENGTH + head[TxPacketBase::ADDR_LENGTH]];

        if (observedChecksum != calculatedChecksum)
        {
          Log::warning("CM730::readPackets") << "Invalid checksum: " << hex << (int)observedChecksum << ", expected: " << (int)calculatedChecksum;
          return CommResult::RX_CORRUPT;
        }

        Log::trace("CM730::readPackets") << "Processing packet " << (int)head[TxPacketBase::ADDR_ID]
            << " checksum: " << hex << setfill('0') << setw(2) << (int)observedChecksum << dec;

        bool moreToProcess = callback(head);

        if (!moreToProcess)
          return CommResult::SUCCESS;

        unsigned packetLength = TxPacketBase::ADDR_LENGTH + head[TxPacketBase::ADDR_LENGTH] + 1; // preamble, data and checksum
        head += packetLength;
        bytesLeft -= packetLength;
      }

      return CommResult::SUCCESS;
    }

    if (d_platform->isPacketTimeout())
    {
      Log::error("CM730::readPackets")
        << "Timeout waiting for bulk read response (" << d_platform->getPacketTimeoutMillis()
        << " ms) -- " << receivedCount << " of " << bufferLength << " bytes read";
      return receivedCount == 0 ? CommResult::RX_TIMEOUT : CommResult::RX_CORRUPT;
    }
  }
}
