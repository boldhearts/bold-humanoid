// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <Eigen/Core>
#include <vector>

#include "CM730Platform/cm730platform.hh"
#include "CM730Table/cm730table.hh"
#include "MX28/mx28.hh"
#include "util/assert.hh"

namespace bold
{
  class BulkRead;
  class MX28Alarm;
  struct TxPacketBase;
  
  /// Communication results
  enum class CommResult
  {
    /// Successful communication with Dynamixel
    SUCCESS,
    /// Problems with Instruction Packet
    TX_CORRUPT,
    /// Port error, failed to send Instruction Packet
    TX_FAIL,
    /// Timeout Status, failed to receive Packet (check connection)
    RX_TIMEOUT,
    /// Status Packet error (bad communications link)
    RX_CORRUPT
  };

  std::string getCommResultName(CommResult res);

  class CM730
  {
  public:
    /// ID of CM730 sub-controller
    static constexpr uint8_t ID_CM = 200;

    //
    // ----------------- Static members
    //

    /// Pack integer RGB values into 16 bits
    static constexpr uint16_t color2Value(uint8_t red, uint8_t green, uint8_t blue);

    /// Get inverse of IMU value
    static uint16_t flipImuValue(uint16_t value);

    /// Unpack 16 bit color to floating point RGB values
    static Eigen::Vector3d shortToColour(uint16_t s);

    //
    // ----------------- Instance members
    //

    CM730(std::unique_ptr<CM730Platform> platform);
    ~CM730();

    /// Links CM730, returning true on success.
    bool connect();

    /// Releases CM730, returning true on success.
    bool disconnect();

    /// True if connected to the CM730.
    bool isConnected() const { return d_platform->isPortOpen(); }

    /// Changes the communication baud rate, returning true on success.
    bool changeBaud(unsigned baud);

    /// Enable or disable torque for all joints, returning true on success.
    bool torqueEnable(bool enable);

    /// Enable or disable the CM730 power, returning true on success.
    bool powerEnable(bool enable);

    /// Ask the CM730 if the power is enabled.
    bool isPowerEnabled();


    unsigned long getReceivedByteCount() const { return d_platform->getReceivedByteCount(); }
    unsigned long getTransmittedByteCount() const { return d_platform->getTransmittedByteCount(); }
    void resetByteCounts() { d_platform->resetByteCounts(); }


    /// Check the existence of device on Dynamixel bus with selected id
    CommResult ping(uint8_t id, MX28Alarm* error);

    /// Restores the state of the specified device to the factory default setting.
    CommResult reset(uint8_t id);

    /// Reads a byte from the CM730 control table
    CommResult readByte(CM730Table address, uint8_t *value, MX28Alarm* error);

    /// Reads a byte from the specified dynamixel device
    CommResult readByte(uint8_t id, MX28Table address, uint8_t *value, MX28Alarm* error);

    /// Reads two bytes from the CM730 control table
    CommResult readWord(uint8_t id, uint8_t address, uint16_t *pValue, MX28Alarm* error);

    /// Reads a consecutive range of bytes from the CM730 control table
    CommResult readTable(uint8_t id, uint8_t fromAddress, uint8_t toAddress, uint8_t *table, MX28Alarm* error);

    // Executes a bulk read operation, as specified in bulkRead
    CommResult bulkRead(BulkRead* bulkRead);

    /// Writes a byte into the control table for the specified Dynamixel device
    CommResult writeByte(uint8_t id, MX28Table address, uint8_t value, MX28Alarm* error);

    /// Writes a byte into the control table for the CM730
    CommResult writeByte(CM730Table address, uint8_t value, MX28Alarm* error);

    /// Writes a byte into the control table for the specified Dynamixel device, retrying on failure
    CommResult writeByteWithRetry(uint8_t id, MX28Table address, uint8_t value, size_t retryCount, MX28Alarm* error);
    
    /// Writes two bytes into the control table for the specified Dynamixel device
    CommResult writeWord(uint8_t id, MX28Table address, uint16_t value, MX28Alarm* error);

    /// Writes two bytes into the control table for the CM730
    CommResult writeWord(CM730Table address, uint16_t value, MX28Alarm* error);

    /// Writes two bytes into the control table for the specified Dynamixel device, retrying on failure
    CommResult writeWordWithRetry(uint8_t id, MX28Table address, uint16_t value, size_t retryCount, MX28Alarm* error);
    
    /** Simultaneously write consecutive table values to one ore more devices.
     *
     * This command can be used to control several Dynamixels with one
     * instruction packet transmission.  Similarly, it can be used to
     * write multiple consecutive values to a single device, such as
     * the CM730.  In combination, it may be used to write different
     * values across the same addresses on multiple MX28s, which is
     * very useful for motion control.
     *
     * When this command is used, several commands are transmitted at
     * once, reducing communication overhead.  The start address in
     * the control table and the number of values to write is the same
     * across all devices, though the values may differ by device.
     *
     * SyncWrite is a broadcast message, so no Status packet is
     * expected in response, hence no error code.
     *
     * @param fromAddress starting address within the control table
     * @param bytesPerDevice number of consecutive values to read from
     *        the control table, inclusive
     * @param deviceCount the number of Dynamixel devices to write to
     * @param parameters parameters to be written, of length
     *        (number * bytesPerDevice)
     */
    CommResult syncWrite(uint8_t fromAddress, uint8_t bytesPerDevice, uint8_t deviceCount,
                         std::vector<uint8_t> const& parameters);

  private:
    /// Reads a byte from the specified dynamixel device
    CommResult readByte(uint8_t id, uint8_t address, uint8_t *value, MX28Alarm* error);

    /// Writes a byte into the control table for the specified Dynamixel device
    CommResult writeByte(uint8_t id, uint8_t address, uint8_t value, MX28Alarm* error);

    /// Writes two bytes into the control table for the specified Dynamixel device
    CommResult writeWord(uint8_t id, uint8_t address, uint16_t value, MX28Alarm* error);

    std::unique_ptr<CM730Platform> d_platform;
    bool d_isPowerEnableRequested;

    /**
     * @param bulkRead populated if txpacket[INSTRUCTION] == INST_BULK_READ, otherwise nullptr.
     */
    CommResult txRxPacket(TxPacketBase const& txpacket, uint8_t* rxpacket,
                          BulkRead* bulkRead = nullptr);

    CommResult txPacket(TxPacketBase const& txpacket);

    CommResult readPackets(uint8_t* buffer, unsigned bufferLength,
                           std::function<bool(uint8_t const*)> callback);
  };


  
  inline constexpr uint16_t CM730::color2Value(uint8_t red, uint8_t green, uint8_t blue)
  {
    return uint16_t(((blue >> 3) << 10) | ((green >> 3) << 5) | (red >> 3));
  }

  inline uint16_t CM730::flipImuValue(uint16_t value)
  {
    ASSERT(value <= 1023);
    if (value == 0)
      return 1023;
    return 1023 - value + 1;
  }

  inline Eigen::Vector3d CM730::shortToColour(uint16_t s)
  {
    return Eigen::Vector3d(
      ( s        & 0x1F) / 31.0,
      ((s >> 5)  & 0x1F) / 31.0,
      ((s >> 10) & 0x1F) / 31.0);
  }

}
