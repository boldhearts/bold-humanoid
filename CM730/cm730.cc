// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cm730.ih"

#include "BulkRead/bulkread.hh"
#include "MX28Alarm/mx28alarm.hh"
#include "JointId/jointid.hh"
#include "TxRxPacket/txpacket.hh"

//// Static members

constexpr uint8_t CM730::ID_CM;

string bold::getCommResultName(CommResult res)
{
  switch (res)
  {
    case CommResult::SUCCESS:     return "SUCCESS";
    case CommResult::TX_CORRUPT:  return "TX_CORRUPT";
    case CommResult::TX_FAIL:     return "TX_FAIL";
    case CommResult::RX_TIMEOUT:  return "RX_TIMEOUT";
    case CommResult::RX_CORRUPT:  return "RX_CORRUPT";
    default:                      return "UNKNOWN";
  }
}

//// Instance members

CM730::CM730(unique_ptr<CM730Platform> platform)
: d_platform(move(platform)),
  d_isPowerEnableRequested(false)
{}

CM730::~CM730()
{
  disconnect();
}

bool CM730::connect()
{
  return d_platform->openPort() && powerEnable(true);
}

bool CM730::disconnect()
{
  if (d_platform->isPortOpen())
  {
    Log::verbose("CM730::disconnect") << "Disconnecting from CM730";

    // Set eye/panel LEDs to indicate disconnection
    MX28Alarm alarm1;
    MX28Alarm alarm2;
    MX28Alarm alarm3;
    writeWord(CM730Table::LED_HEAD_L, CM730::color2Value(0, 255, 0), &alarm1);
    writeWord(CM730Table::LED_EYE_L,  CM730::color2Value(0,   0, 0), &alarm2);
    writeByte(CM730Table::LED_PANEL,  0, &alarm3);

    MX28Alarm alarm = alarm1.getFlags() | alarm2.getFlags() | alarm3.getFlags();

    if (alarm.hasError())
    {
      Log::error("CM730::disconnect") << "Error setting eye/head/panel LED colours: " << alarm;
      return false;
    }

    if (!powerEnable(false))
    {
      Log::error("CM730::disconnect") << "Error turning power off";
      return false;
    }

    if (!d_platform->closePort())
    {
      Log::error("CM730::disconnect") << "Error closing port";
      return false;
    }
  }

  return true;
}

bool CM730::changeBaud(unsigned baud)
{
  if (!d_platform->setBaud(baud))
  {
    Log::error("CM730::changeBaud") << "Failed to change baudrate to " << baud;
    return false;
  }

  return powerEnable(true);
}

bool CM730::torqueEnable(bool enable)
{
  Log::info("CM730::torqueEnable") << "" << (enable ? "Enabling" : "Disabling") << " all joint torque";

  bool allSuccessful = true;
  MX28Alarm error;
  for (uint8_t jointId = (uint8_t)JointId::MIN; jointId <= (uint8_t)JointId::MAX; jointId++)
  {
    auto res = writeByte(jointId, (uint8_t)MX28Table::TORQUE_ENABLE, enable ? 1 : 0, &error);

    if (res != CommResult::SUCCESS)
    {
      Log::error("CM730::torqueEnable") << "Comm error for " << JointName::getEnumName(jointId) << " (" << (int)jointId << "): " << getCommResultName(res);
      allSuccessful = false;
    }

    if (error.hasError())
    {
      Log::error("CM730::torqueEnable") << "Error for " << JointName::getEnumName(jointId) << " (" << (int)jointId << "): " << error;
      allSuccessful = false;
    }
  }
  return allSuccessful;
}

bool CM730::isPowerEnabled()
{
  return d_isPowerEnableRequested;
//   uint8_t value;
//   MX28Alarm alarm;
//
//   static bool commError = false;
//
//   if (readByte(CM730::ID_CM, CM730Table::DXL_POWER, &value, &alarm) != CommResult::SUCCESS)
//   {
//     if (!commError)
//     {
//       Log::error("CM730::isPowerEnabled") << "Comm error reading CM730 power level";
//       commError = true;
//     }
//     return false;
//   }
//   else
//   {
//     commError = false;
//   }
//
//   if (alarm.hasError())
//   {
//     Log::error("CM730::isPowerEnabled") << "Error reading CM730 power level: " << alarm;
//     return false;
//   }
//
//   return value == 1;
}

bool CM730::powerEnable(bool enable)
{
  Log::info("CM730::powerEnable") << "Turning CM730 power " << (enable ? "on" : "off");

  MX28Alarm alarm;
  if (writeByte(CM730Table::DXL_POWER, enable ? 1 : 0, &alarm) != CommResult::SUCCESS)
  {
    Log::error("CM730::powerEnable") << "Comm error turning CM730 power " << (enable ? "on" : "off");
    return false;
  }

  if (alarm.hasError())
  {
    Log::error("CM730::powerEnable") << "Error turning CM730 power " << (enable ? "on" : "off") << ": " << alarm;
    return false;
  }

  d_isPowerEnableRequested = enable;

  // TODO why is this sleep here?
  d_platform->sleep(300); // milliseconds

  return true;
}

CommResult CM730::ping(uint8_t id, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  PingTxPacket txpacket(id);
  uint8_t rxpacket[6];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS && id != TxPacketBase::ID_BROADCAST)
  {
    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::reset(uint8_t id)
{
  ASSERT(d_platform->isPortOpen());

  ResetTxPacket txpacket(id);
  uint8_t rxpacket[6];

  return txRxPacket(txpacket, rxpacket);
}


CommResult CM730::readByte(CM730Table address, uint8_t* value, MX28Alarm* error)
{
  return readByte(ID_CM, (uint8_t)address, value, error);
}

CommResult CM730::readByte(uint8_t id, MX28Table address, uint8_t* value, MX28Alarm* error)
{
  return readByte(id, (uint8_t)address, value, error);
}

CommResult CM730::readByte(uint8_t id, uint8_t address, uint8_t *pValue, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  ReadByteTxPacket txpacket(id, address);
  uint8_t rxpacket[7];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS)
  {
    *pValue = rxpacket[TxPacketBase::ADDR_PARAMETER];
    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::readWord(uint8_t id, uint8_t address, uint16_t *pValue, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  ReadWordTxPacket txpacket(id, address);
  uint8_t rxpacket[8];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS)
  {
    *pValue = binary::makeWord(rxpacket[TxPacketBase::ADDR_PARAMETER], rxpacket[TxPacketBase::ADDR_PARAMETER + 1]);

    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::readTable(uint8_t id, uint8_t fromAddress, uint8_t toAddress, uint8_t *table, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  int length = toAddress - fromAddress + 1;

  ReadTxPacket txpacket(id, fromAddress, length);
  uint8_t rxpacket[6 + length];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS)
  {
    for (int i = 0; i < length; i++)
      table[fromAddress + i] = rxpacket[TxPacketBase::ADDR_PARAMETER + i];

    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::bulkRead(BulkRead* bulkRead)
{
  ASSERT(d_platform->isPortOpen());

  uint8_t rxpacket[bulkRead->getRxLength()];

  bulkRead->clearError();

  ASSERT(bulkRead->getTxPacket().getData()[TxPacketBase::ADDR_LENGTH] != 0);

  return txRxPacket(bulkRead->getTxPacket(), rxpacket, bulkRead);
}


CommResult CM730::writeByte(uint8_t id, MX28Table address, uint8_t value, MX28Alarm* error)
{
  ASSERT((uint8_t)address < (uint8_t)MX28Table::MAXNUM_ADDRESS);
  return writeByte(id, (uint8_t)address, value, error);
}

CommResult CM730::writeByte(CM730Table address, uint8_t value, MX28Alarm* error)
{
  ASSERT((uint8_t)address < (uint8_t)CM730Table::MAXNUM_ADDRESS);
  return writeByte(ID_CM, (uint8_t)address, value, error);
}

CommResult CM730::writeByte(uint8_t id, uint8_t address, uint8_t value, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  WriteByteTxPacket txpacket(id, address, value);
  uint8_t rxpacket[6];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS && id != TxPacketBase::ID_BROADCAST)
  {
    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::writeByteWithRetry(uint8_t id, bold::MX28Table address, uint8_t value, size_t retryCount, bold::MX28Alarm *error)
{
  size_t failCount = 0;
  while (true)
  {
    CommResult res = writeByte(id, address, value, error);

    if (res == CommResult::SUCCESS)
      return CommResult::SUCCESS;
    else
    {
      failCount++;
      if (failCount == retryCount)
        return res;
      usleep(50000); // 50ms
    }
  }
}

CommResult CM730::writeWord(uint8_t id, MX28Table address, uint16_t value, MX28Alarm* error)
{
  ASSERT((uint8_t)address < (uint8_t)MX28Table::MAXNUM_ADDRESS);
  return writeWord(id, (uint8_t)address, value, error);
}

CommResult CM730::writeWord(CM730Table address, uint16_t value, MX28Alarm* error)
{
  ASSERT((uint8_t)address < (uint8_t)CM730Table::MAXNUM_ADDRESS);
  return writeWord(ID_CM, (uint8_t)address, value, error);
}

CommResult CM730::writeWord(uint8_t id, uint8_t address, uint16_t value, MX28Alarm* error)
{
  ASSERT(d_platform->isPortOpen());

  WriteWordTxPacket txpacket(id, address, value);
  uint8_t rxpacket[6];

  CommResult result = txRxPacket(txpacket, rxpacket);

  if (result == CommResult::SUCCESS && id != TxPacketBase::ID_BROADCAST)
  {
    if (error != nullptr)
      *error = rxpacket[ERRBIT];
  }

  return result;
}

CommResult CM730::writeWordWithRetry(uint8_t id, MX28Table address, uint16_t value, size_t retryCount, MX28Alarm* error)
{
  size_t failCount = 0;
  while (true)
  {
    CommResult res = writeWord(id, address, value, error);

    if (res == CommResult::SUCCESS)
      return CommResult::SUCCESS;
    else
    {
      failCount++;
      if (failCount == retryCount)
          return res;
      usleep(50000); // 50ms
    }
  }
}

CommResult CM730::syncWrite(uint8_t fromAddress,
                            uint8_t bytesPerDevice, uint8_t deviceCount,
                            std::vector<uint8_t>  const& parameters)
{
  ASSERT(d_platform->isPortOpen());
  ASSERT(bytesPerDevice != 0);
  ASSERT(bytesPerDevice - 1 <= 255);
  ASSERT(deviceCount != 0);
  ASSERT(fromAddress < (uint8_t)MX28Table::MAXNUM_ADDRESS);
  unsigned paramLength = bytesPerDevice * deviceCount;
  ASSERT(paramLength + 4 <= 255);
  unsigned txSize = 8 + paramLength;
  if (txSize > 143)
    Log::warning("CM730::syncWrite") << "Packet of length " << txSize << " exceeds the Dynamixel's inbound buffer size (" << (int)deviceCount << " devices, " << (int)bytesPerDevice << " bytes per device)";

  SyncWriteTxPacket txpacket(TxPacketBase::ID_BROADCAST, fromAddress,
                             bytesPerDevice, deviceCount,
                             parameters);

  // Sync write instructions do not receive status packet responses, so no buffer is needed.
  uint8_t* rxpacket = nullptr;

  return txRxPacket(txpacket, rxpacket);
}
