// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cm730.ih"

#include "TxRxPacket/txpacket.hh"
#include "BulkRead/bulkread.hh"

CommResult CM730::txRxPacket(TxPacketBase const& txpacket, uint8_t* rxpacket, BulkRead* bulkRead)
{
  ASSERT(ThreadUtil::isMotionLoopThread());
  ASSERT(d_platform->isPortOpen());

  //
  // WRITE DATA
  //

  {
    auto l = Log::trace("CM730::txRxPacket");
    l << "Transmitting '" << txpacket.getInstructionName() << "' instruction";
    l << "   TX[" << txpacket.size() << "]";
    l << hex << setfill('0');
    for (size_t n = 0; n < txpacket.size(); n++)
      l << " " << setw(2) << (int)txpacket.getData()[n];
    l << dec;
  }

  if (txpacket.size() >= (MAXNUM_TXPARAM + 6))
  {
    Log::error("CM730::txRxPacket") << "Attempt to write " << txpacket.size() << " bytes, which is more than MAXNUM_TXPARAM+6";
    return CommResult::TX_CORRUPT;
  }

  // Throw away any unprocessed inbound bytes lingering in the buffer
  d_platform->clearPort();

  // Send the instruction packet
  int bytesWritten = d_platform->writePort(txpacket.getData(), txpacket.size());

  if (bytesWritten < 0)
  {
    Log::error("CM730::txRxPacket") << "Error writing to CM730 port: " << strerror(errno) << " (" << errno << ")";
    return CommResult::TX_FAIL;
  }
  else if ((uint)bytesWritten != txpacket.size())
  {
    Log::warning("CM730::txRxPacket") << "Failed to write entire packet to port: " << bytesWritten << " of " << txpacket.size() << " written";
    return CommResult::TX_FAIL;
  }

  //
  // READ DATA
  //

  CommResult commResult;

  if (txpacket.getID() != TxPacketBase::ID_BROADCAST)
  {
    unsigned rxPacketLength = txpacket.getInstruction() == TxPacketBase::INSTR_READ
      ? txpacket.getParameter(1) + 6
      : 6;

    d_platform->setPacketTimeout(txpacket.size());

    // We will only have one packet in the response, and so long as it has valid structure,
    // the communication was successful. The response doesn't contain anything to process.
    commResult = readPackets(rxpacket, rxPacketLength, [](uint8_t const* packet) { return false; });
  }
  else if (txpacket.getInstruction() == TxPacketBase::INSTR_BULK_READ)
  {
    ASSERT(bulkRead);

    Log::trace("CM730::txRxPacket") << "Bulk read, reading " << bulkRead->getRxLength() << " bytes";

    bulkRead->setError(rxpacket[ERRBIT]);

    uint8_t deviceCount = 1 + (uint8_t)JointId::DEVICE_COUNT + 2;

    d_platform->setPacketTimeout(static_cast<uint>(bulkRead->getRxLength() * 1.5));

    auto devicePacketCallback = [this,&deviceCount,&bulkRead](uint8_t const* packet) -> bool
    {
      // Copy data from the packet to BulkReadTable
      auto& table = bulkRead->getBulkReadData(packet[TxPacketBase::ADDR_ID]);

      // The number of data bytes is equal to the packet's advertised length, minus two (checksum and length bytes)
      const uint8_t dataByteCount = packet[TxPacketBase::ADDR_LENGTH] - (uint8_t)2;

      ASSERT(table.getStartAddress() + dataByteCount < (uint8_t)MX28Table::MAXNUM_ADDRESS);

      std::copy(
        &packet[TxPacketBase::ADDR_PARAMETER],
        &packet[TxPacketBase::ADDR_PARAMETER + dataByteCount + 1],
        table.getData() + table.getStartAddress());

      deviceCount--;
      return deviceCount != 0;
    };

    // We will have one response packet per device, and we'll be called back once per
    commResult = readPackets(rxpacket, bulkRead->getRxLength(), devicePacketCallback);
  }
  else
  {
    // Broadcast message, always successful as no response expected (?)
    commResult = CommResult::SUCCESS;
  }

  Log::trace("CM730::txRxPacket") << "Round trip in " << setprecision(2) << d_platform->getPacketTime()
                                  << "ms  (" << getCommResultName(commResult) << ")";

  return commResult;
}
