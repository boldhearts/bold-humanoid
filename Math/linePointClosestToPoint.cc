// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "math.ih"

geometry2::Point2d Math::linePointClosestToPoint(geometry2::LineSegment2d const& segment, geometry2::Point2d const &point)
{
  auto lVect = segment.delta();

  auto v = lVect.normalized();

  auto s = findPerpendicularVector(v);

  auto const& l0 = segment.p1();

  // There probably is a more efficient formula.
  double u;
  if (s.y() == 0)
    u = (l0.y() - point.y()) / -v.y();
  else
    u = ((s.x() / s.y())*(l0.y() - point.y()) + (point.x() - l0.x()))/(v.x() - (s.x() / s.y()) * v.y());

  // When no perpendicular line is posible within the segment, use
  // the closest endpoint.
  if (u > lVect.norm() || u < 0)
  {
    if ((l0 - point).norm() < ((l0 + lVect) - point).norm())
      return l0;
    else
      return l0 + lVect;
  }

  return l0 + v * u;
}
