// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "math.ih"

Maybe<geometry2::Point3d> Math::intersectRayWithPlane(geometry2::Point3d const& position,
                                                      Vector3d const& direction,
                                                      Vector4d const& plane)
{
  // x = pos.x + t * dir.x
  //
  // 0 = a * x + b * y + c * z + d = 0
  //
  // 0 = a * pos.x + a * t * dir.x +
  //     b * pos.y + b * t * dir.y +
  //     c * pos.z + c * t * dir.z +
  //     d;
  //
  // t * (a * dir.x + b * dir.y + c * dir.z) = - a * pos.x - b * pos.y - c * pos.z - d
  //
  // t = (- a * pos.x - b * pos.y - c * pos.z - d) / (a * dir.x + b * dir.y + c * dir.z)

  double denom = (plane.x() * direction.x() + plane.y() * direction.y() + plane.z() * direction.z());

  if (denom == 0)
    return util::Maybe<geometry2::Point3d>::empty();

  double t = - (plane.x() * position.x() + plane.y() * position.y() + plane.z() * position.z() + plane(3)) / denom;

  if (t < 0)
    return util::Maybe<geometry2::Point3d>::empty();

  return make_maybe(position + (direction * t));
}
