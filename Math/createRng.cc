// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "math.ih"

function<double()> Math::createUniformRng(double min, double max, bool randomSeed)
{
  uniform_real_distribution<double> distribution(min, max);

  if (randomSeed)
  {
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    return bind(distribution, default_random_engine(seed));
  }

  return bind(distribution, default_random_engine());
}

function<double()> Math::createNormalRng(double mean, double stddev, bool randomSeed)
{
  normal_distribution<double> distribution(mean, stddev);

  if (randomSeed)
  {
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    return bind(distribution, default_random_engine(seed));
  }

  return bind(distribution, default_random_engine());
}
