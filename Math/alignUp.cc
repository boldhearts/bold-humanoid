// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "math.ih"

Affine3d Math::alignUp(Affine3d const& transform)
{
  auto tUp = transform.matrix().col(2).head<3>();
  auto nUp = Vector3d{0, 0, 1};

  auto rotationAngle = acos(tUp.dot(nUp));
  if (rotationAngle < 1e-6)
    return transform;

  auto rotationAxis = tUp.cross(nUp).normalized();

  Affine3d result = AngleAxisd(rotationAngle, rotationAxis) * transform;
  result.translation() = transform.translation();
  return result;
}
