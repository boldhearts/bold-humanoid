// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/util/assert.hh"
#include "src/util/Maybe/maybe.hh"
#include "src/geometry2/Point/point.hh"
#include "src/geometry2/Geometry/LineSegment/linesegment.hh"

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <functional>
#include <vector>

namespace bold
{
  namespace geometry2
  {
    template<typename T>
    class LineSegment2;
  }

  class Math
  {
  public:
    static util::Maybe<geometry2::Point3d> intersectRayWithGroundPlane(geometry2::Point3d const& position,
                                                                       Eigen::Vector3d const& direction,
                                                                       double const planeZ);

    // TODO: use Eigen::ParamaterizedPlane
    static util::Maybe<geometry2::Point3d> intersectRayWithPlane(geometry2::Point3d const& position,
                                                                 Eigen::Vector3d const& direction,
                                                                 Eigen::Vector4d const& plane);

    static geometry2::Point2d linePointClosestToPoint(geometry2::LineSegment2d const& segment,
                                                      geometry2::Point2d const& point);
    
    // TODO what if 'vector' has zero length? should this return 'Maybe<Vector2d>'?
    static Eigen::Vector2d findPerpendicularVector(Eigen::Vector2d const& vector);

    static std::function<double()> createUniformRng(double min, double max, bool randomSeed = true);
    static std::function<double()> createNormalRng(double mean, double stddev, bool randomSeed = true);

    static constexpr double degToRad(double degrees) { return (degrees * M_PI) / 180.0; }
    static constexpr double radToDeg(double radians) { return (radians / M_PI) * 180.0; }

    static double smallestAngleBetween(Eigen::Vector2d v1, Eigen::Vector2d v2);

    static Eigen::Affine3d alignUp(Eigen::Affine3d const& transform);

    template<typename T>
    static constexpr T clamp(T val, T min, T max)
    {
      return val < min
        ? min
        : val > max
          ? max
          : val;
    }

    /**
      * Maps @link input to a value in the range from @link lowerOutput to @link upperOutput.
      * If @link input is outside the range from @link lower to @link upper, then @link lowerOutput or @link upperOutput are returned,
      * otherwise the value is linearly interpolated.
      * Note that @link lower must be less than @link upper, but that there is no restriction on values of @link lowerOutput and @link upperOutput.
      */
    template<typename T>
    static T lerp(double const& input, double const& lower, double const& upper, T const& lowerOutput, T const& upperOutput)
    {
      if (unlikely(upper <= lower))
        throw std::runtime_error("lower must be less than upper");

      double ratio = (input - lower) / (upper - lower);
      ratio = clamp(ratio, 0.0, 1.0);

      return lowerOutput + (upperOutput - lowerOutput) * ratio;
    }

    template<typename T>
    static constexpr T lerp(double const& ratio, T const& lowerOutput, T const& upperOutput)
    {
      return lowerOutput + (upperOutput - lowerOutput) * ratio;
    }

    /** Constrains the angle to range [-PI,PI). */
    static double normaliseRads(double rads)
    {
      rads = fmod(rads + M_PI, 2*M_PI);
      if (rads < 0)
          rads += 2*M_PI;
      return rads - M_PI;
    }

    /** Angle spanned by rotation between two angles
     *
     * The rotation is measured by rotating from @a a1 to @a2 in
     * positive direction (counter clockwise for right hand system).
     */
    static double angleDiffRads(double a1, double a2)
    {
      double rads = a2 - a1;
      if (rads < 0.0)
        rads += 2.0 * M_PI;
      return rads;
    }

    /** Absolute distance between two angles in radians
     *
     * e.g.:
     * |pi - .5 pi| = |.5 pi - pi| = .5 pi
     * | -.1 po - .1pi | = | .1pi - -.1 pi | = .2 pi
     * | -.9 pi - .9 pi | = | .9 pi - -.9pi | = .2 pi
     */
    static double shortestAngleDiffRads(double a1, double a2)
    {
      // The fmod() function computes the floating-point remainder of
      // dividing x by y.  The return value is x - n * y, where n is
      // the quotient of x / y, rounded toward zero to an integer.
      double d = fmod(a2 - a1, 2 * M_PI);

      d += (d > M_PI)
        ? -2 * M_PI
        : d <= -M_PI
          ? 2 * M_PI
          : 0;

      return d;
    }

    /** Returns the angle to a point, as defined in the agent frame,
     * where zero is straight ahead and positive is to the left
     * (counter-clockwise). */
    template<int N>
    static double angleToPoint(geometry2::Point<double, N> const& point)
    {
      static_assert(N > 1, "Point must have at least two dimensions");
      return ::atan2(-point.x(), point.y());
    }

    /** Returns the point at the given angle and distance, as defined
     * in the agent frame, where zero is straight ahead and positive
     * is to the left (counter-clockwise). */
    static inline geometry2::Point2d pointAtAngle(double angle, double distance)
    {
      return geometry2::Point2d(cos(angle) * distance, sin(angle) * distance);
    }

    /** Returns mean of angles
     *
     * TODO: Algorithm at:
     * http://www.codeproject.com/Articles/190833/Circular-Values-Math-and-Statistics-with-Cplusplus
     * seems to give more intuitive results
     */
    static double angularMean(std::vector<double> const& angles)
    {
      double x = 0.0;
      double y = 0.0;
      for (auto a : angles)
      {
        x += sin(a);
        y += cos(a);
      }
      return atan2(x / angles.size(), y / angles.size());
    }

    /** Calculate angle in triangle
     *
     * Uses the cosine rule to calculate angle of corner opposite @sideA
     */
    static util::Maybe<double> triangleAngle(double sideA, double sideB, double sideC, bool saturate = true)
    {
      auto op = (sideA * sideA - sideB * sideB - sideC * sideC) / (2 * sideB * sideC);

      if(std::abs(op) > 1.0)
      {
        if (!saturate)
          return util::Maybe<double>::empty();

        op = std::max(op, -1.0);
        op = std::min(op, 1.0);
      }

      return util::make_maybe(acos(op));
    }

  private:
    Math() = delete;
  };
}
