// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <alsa/asoundlib.h>
#include <fftw3.h>
#include <iosfwd>

#include "stats/movingaverage.hh"
#include "src/util/WindowFunction/windowfunction.hh"

namespace bold
{
  class WhistleListener
  {
  public:
    WhistleListener();

    WhistleListener(std::string deviceName, unsigned sampleRate, unsigned sampleCount, uint16_t slowSmoothingLength, uint16_t fastSmoothingLength);

    ~WhistleListener();

    bool initialise();

    bool step();

    void setWindowFunction(util::WindowFunctionType type);

  private:
    const std::string d_deviceName;
    unsigned d_sampleRate;
    unsigned d_sampleCount;
    std::vector<MovingAverage<float>> d_slowSmoothers;
    std::vector<MovingAverage<float>> d_fastSmoothers;
    std::unique_ptr<util::WindowFunction> d_windowFunction;
    snd_pcm_t* d_pcm;
    float* d_samples;
    fftwf_complex* d_frequencies;
    fftwf_plan d_fftwPlan;
  };
}
