// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "debugger.hh"

#include "Agent/agent.hh"
#include "BehaviourControl/behaviourcontrol.hh"
#include "LEDControl/ledcontrol.hh"
#include "state/State/state.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/LEDState/ledstate.hh"
#include "state/StateObject/MessageCountState/messagecountstate.hh"
#include "state/StateObject/StationaryMapState/stationarymapstate.hh"
#include "agent/ButtonObserver/buttonobserver.hh"
#include "Voice/voice.hh"
#include "src/colour/HSV/hsv.hh"

using namespace bold;
using namespace bold::state;
using namespace bold::colour;
using namespace std;

Debugger::Debugger(Agent* agent,
                   shared_ptr<Clock> clock,
                   shared_ptr<BehaviourControl> behaviourControl,
                   shared_ptr<LEDControl> ledControl,
                   shared_ptr<Voice> voice,
                   shared_ptr<ButtonObserver> buttonObserver)
  : d_agent(agent),
    d_clock(move(clock)),
    d_behaviourControl(behaviourControl),
    d_ledControl(ledControl),
    d_voice(voice),
    d_leftButtonTracker(buttonObserver->track(Button::Left)),
    d_showDazzle(false)
{}

void Debugger::update()
{
  //
  // Update Hardware LEDs
  //

  auto const& stationaryMap = State::get<StationaryMapState>();
  auto const& cameraFrame = State::get<CameraFrameState>();
  auto const& messageCount = State::get<MessageCountState>();

  if (stationaryMap)
    d_ledControl->setPanelLedStates(
      /*red  */ stationaryMap->hasEnoughBallObservations(),
      /*blue */ messageCount->getGameControllerMessageCount() != 0,
      /*green*/ stationaryMap->getSatisfactoryGoalPostCount() != 0
    );
  else if (cameraFrame)
    d_ledControl->setPanelLedStates(
      /*red  */ cameraFrame->getBallObservation().hasValue(),
      /*blue */ messageCount->getGameControllerMessageCount() != 0,
      /*green*/ cameraFrame->getGoalObservations().size() != 0
    );
  else
    d_ledControl->setPanelLedStates(false, false, false);

  //
  // Eye and forehead colours
  //

  BGR eyeColour = BGR::orange();
  BGR foreheadColour = BGR::orange();

  if (d_agent->isShutdownRequested())
  {
    eyeColour = BGR::black();
    foreheadColour = BGR::black();
  }
  else if (d_showDazzle)
  {
    static auto prng = Math::createUniformRng(0, 1);
    static MovingAverage<double> smoothedHue(3);
    static MovingAverage<double> smoothedValue(3);

    double hue = smoothedHue.next(prng());
    double value = smoothedValue.next(prng());

    BGR colour = HSV(int(hue * 255), 255, int(value * 255)).toBGR();

    foreheadColour = colour;
    eyeColour = colour;
  }
  else
  {
    // Set forehead colour
    if (d_behaviourControl->getPlayerStatus() == PlayerStatus::Paused)
      foreheadColour = BGR::grey();
    else if (d_behaviourControl->getPlayerStatus() == PlayerStatus::Penalised)
      foreheadColour = BGR::lightRed();
    else
    {
      switch (d_behaviourControl->getPlayMode())
      {
      case PlayMode::INITIAL:
        foreheadColour = BGR::darkBlue();
        break;
      case PlayMode::READY:
        foreheadColour = BGR::lightBlue();
        break;
      case PlayMode::SET:
        foreheadColour = BGR::yellow();
        break;
      case PlayMode::PLAYING:
        foreheadColour = BGR::lightGreen();
        break;
      case PlayMode::FINISHED:
        foreheadColour = BGR(30,30,30);
        break;
      }
    }

    // Set eye colour
    switch (d_behaviourControl->getPlayerRole())
    {
      case PlayerRole::Idle:
        eyeColour = BGR(64,0,0);
        break;
      case PlayerRole::Defender:
        eyeColour = BGR(200,0,0);
        break;
      case PlayerRole::Supporter:
        eyeColour = BGR(0,200,0);
        break;
      case PlayerRole::Striker:
      case PlayerRole::PenaltyStriker:
        eyeColour = BGR(148,0,211);
        break;
      case PlayerRole::Keeper:
      case PlayerRole::PenaltyKeeper:
        eyeColour = BGR(64,64,64);
        break;
      case PlayerRole::Other:
      default:
        eyeColour = BGR(139,0,139);
        break;
    }
  }

  auto modulateColor = [](colour::BGR const& bgr, uint8_t const& v)
  {
    auto hsv = bgr.toHSV();
    hsv.v = v;
    return hsv.toBGR();
  };

  double seconds = d_clock->getSeconds();
  d_ledControl->setEyeColour(modulateColor(eyeColour, fabs(sin(seconds*2)) * 255));
  d_ledControl->setForeheadColour(modulateColor(foreheadColour, fabs(sin(seconds*3)) * 255));

  //
  // Update state object
  //

  State::make<LEDState>(
    d_ledControl->getEyeColour(),
    d_ledControl->getForeheadColour(),
    d_ledControl->isRedPanelLedLit(),
    d_ledControl->isGreenPanelLedLit(),
    d_ledControl->isBluePanelLedLit());

  // Look for left button presses while paused
  if (d_leftButtonTracker->isPressedForMillis(20, *d_clock) && d_behaviourControl->getPlayerStatus() == PlayerStatus::Paused)
  {
    auto hw = State::get<HardwareState>();
    auto team = State::get<TeamState>();

    stringstream msg;
    msg << hw->getCM730State().voltage << " volts.";

    // TODO need to ensure old teammate data is removed from TeamState
    if (team && team->players().size() > 1)
    {
      msg << " I hear team mates ";
      for (auto const& mate : team->players())
        if (!mate.isMe())
          msg << (int)mate.uniformNumber << ", ";
      msg << ".";
    }
    else
      msg << " No team mates.";

    d_voice->say(msg.str());
  }
}
