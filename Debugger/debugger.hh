// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

namespace bold
{
  class Agent;
  class BehaviourControl;
  class ButtonObserver;
  class ButtonTracker;
  class Clock;
  class LEDControl;
  class Voice;

  // TODO this class's name suggests something grander than its reality

  class Debugger
  {
  public:
    Debugger(Agent* agent,
             std::shared_ptr<Clock> clock,
             std::shared_ptr<BehaviourControl> behaviourControl,
             std::shared_ptr<LEDControl> ledControl,
             std::shared_ptr<Voice> voice,
             std::shared_ptr<ButtonObserver> buttonObserver);

    void showDazzle(bool showDazzle) { d_showDazzle = showDazzle; }

    /** Update DebugState. Called at the end of the think cycle. */
    void update();

  private:
    Agent* d_agent;
    std::shared_ptr<Clock> d_clock;
    std::shared_ptr<BehaviourControl> d_behaviourControl;
    std::shared_ptr<LEDControl> d_ledControl;
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<ButtonTracker> d_leftButtonTracker;

    bool d_showDazzle;
  };
}
