// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "agent.hh"

#include "motion/core/MotionTaskScheduler/motiontaskscheduler.hh"
#include "motion/modules/HeadModule/headmodule.hh"
#include "motion/modules/MotionScriptModule/motionscriptmodule.hh"
#include "motion/modules/StandModule/standmodule.hh"
#include "motion/modules/WalkModule/walkmodule.hh"
#include "motion/scripts/MotionScriptLibrary/motionscriptlibrary.hh"

#include "BehaviourControl/behaviourcontrol.hh"
#include "BulkRead/bulkread.hh"
#include "BodyModel/DarwinBodyModel/darwinbodymodel.hh"
#include "CM730CommsModule/MX28HealthChecker/mx28healthchecker.hh"
#include "Debugger/debugger.hh"
#include "DrawBridgeComms/drawbridgecomms.hh"
#include "GameStateDecoder/GameStateDecoderVersion7/gamestatedecoderversion7.hh"
#include "GameStateDecoder/GameStateDecoderVersion8/gamestatedecoderversion8.hh"
#include "GameStateDecoder/GameStateDecoderVersion12/gamestatedecoderversion12.hh"
#include "GameStateReceiver/gamestatereceiver.hh"
#include "Kick/kick.hh"
#include "LEDControl/ledcontrol.hh"
#include "OptionTree/optiontree.hh"
#include "MessageCounter/messagecounter.hh"
#include "MotionLoop/motionloop.hh"
#include "RemoteControl/remotecontrol.hh"
#include "RoleDecider/roledecider.hh"
#include "state/State/state.hh"
#include "state/StateObject/AccelerometerState/accelerometerstate.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/AudioPowerSpectrumState/audiopowerspectrumstate.hh"
#include "state/StateObject/BalanceState/balancestate.hh"
#include "state/StateObject/BehaviourControlState/behaviourcontrolstate.hh"
#include "state/StateObject/BodyControlState/bodycontrolstate.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/DrawingState/drawingstate.hh"
#include "state/StateObject/FieldEdgeState/fieldedgestate.hh"
#include "state/StateObject/FSRState/fsrstate.hh"
#include "state/StateObject/GyroCalibrationState/gyrocalibrationstate.hh"
#include "state/StateObject/HardwareState/hardwarestate.hh"
#include "state/StateObject/LabelCountState/labelcountstate.hh"
#include "state/StateObject/LabelTeacherState/labelteacherstate.hh"
#include "state/StateObject/LEDState/ledstate.hh"
#include "state/StateObject/MessageCountState/messagecountstate.hh"
#include "state/StateObject/MotionTaskState/motiontaskstate.hh"
#include "state/StateObject/OdometryState/odometrystate.hh"
#include "state/StateObject/OrientationState/orientationstate.hh"
#include "state/StateObject/OptionTreeState/optiontreestate.hh"
#include "state/StateObject/ParticleState/particlestate.hh"
#include "state/StateObject/StabilityState/stabilitystate.hh"
#include "state/StateObject/StaticHardwareState/statichardwarestate.hh"
#include "state/StateObject/StationaryMapState/stationarymapstate.hh"
#include "state/StateObject/TimingState/timingstate.hh"
#include "state/StateObject/RobotisWalkState/robotiswalkstate.hh"
#include "state/StateObject/WorldFrameState/worldframestate.hh"

#include "agent/ButtonObserver/buttonobserver.hh"
#include "agent/FallDetector/AccelerometerFallDetector/accelerometerfalldetector.hh"
#include "agent/FallDetector/OrientationFallDetector/orientationfalldetector.hh"
#include "agent/GyroCalibrator/gyrocalibrator.hh"
#include "agent/HandlerHelper/handlerhelper.hh"
#include "agent/HealthAndSafety/healthandsafety.hh"
#include "agent/JamDetector/jamdetector.hh"
#include "agent/Odometer/odometer.hh"
#include "agent/OrientationTracker/orientationtracker.hh"
#include "agent/OpenTeamCommunicator/openteamcommunicator.hh"
#include "agent/StabilityObserver/stabilityobserver.hh"
#include "agent/StationaryMapper/stationarymapper.hh"
#include "agent/SuicidePill/suicidepill.hh"
#include "agent/Vocaliser/vocaliser.hh"
#include "Spatialiser/spatialiser.hh"
#include "Voice/voice.hh"

#include "roundtable/RoundTableServer/roundtableserver.hh"
#include "vision/Camera/camera.hh"
#include "vision/CameraController/cameracontroller.hh"
#include "vision/CameraModel/cameramodel.hh"
#include "vision/ImageLabeller/imagelabeller.hh"
#include "vision/VisualCortex/visualcortex.hh"

using namespace std;
using namespace bold;
using namespace bold::config;
using namespace bold::motion::core;
using namespace bold::motion::modules;
using namespace bold::motion::scripts;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;

Agent::Agent(shared_ptr<Clock> clock, shared_ptr<Draw> draw,
             unique_ptr<Camera> camera, unique_ptr<CameraController> cameraController)
    : Loop{"Think Loop", clock},
      d_isShutdownRequested{false},
      d_teamNumber{uint8_t(Config::getStaticValue<int>("team-number"))},
      d_uniformNumber{uint8_t(Config::getStaticValue<int>("uniform-number"))},
      d_teamColour{Config::getStaticValue<TeamColour>("team-colour")},
      d_model{Config::getStaticValue<string>("model")},
      d_clock{move(clock)},
      d_draw{move(draw)},
      d_camera{move(camera)},
      d_cameraController{move(cameraController)},
      d_startTime{d_clock->getTimestamp()} {
  ThreadUtil::setThreadId(ThreadId::ThinkLoop);

  Log::info("Agent::Agent") << "Name:    " << Config::getStaticValue<string>("player-name");
  Log::info("Agent::Agent") << "Uniform: " << (int) d_uniformNumber;
  Log::info("Agent::Agent") << "Team:    " << (int) d_teamNumber;
  Log::info("Agent::Agent") << "Colour:  " << (d_teamColour == TeamColour::Cyan ? "Cyan" : "Magenta");
  Log::info("Agent::Agent") << "Model   " << d_model;

  State::initialise();

  FieldMap::initialise();

  Kick::loadAll();

  d_bodyModel = make_shared<DarwinBodyModel>(d_model);

  d_voice = make_shared<Voice>();
  d_behaviourControl = make_shared<BehaviourControl>(*this);

  registerStateTypes();

  Log::logGameState = true;

  d_buttonObserver = make_shared<ButtonObserver>();

  auto debugControl = make_shared<LEDControl>();
  d_debugger = make_shared<Debugger>(this, d_clock, d_behaviourControl, debugControl, d_voice, d_buttonObserver);

  // Prepare the motion schedule, that coordinates which motions are carried out
  d_motionSchedule = make_shared<MotionTaskScheduler>();

  // Create motion modules
  d_headModule = make_shared<HeadModule>(d_motionSchedule);
  d_walkModule = make_shared<WalkModule>(d_motionSchedule, MotionLoop::TIME_UNIT_MS);
  d_motionScriptModule = make_shared<MotionScriptModule>(d_motionSchedule);
  d_standModule = make_shared<StandModule>(d_motionSchedule);

  d_motionScriptLibrary = make_shared<MotionScriptLibrary>("./motionscripts");
  MotionScriptModule::createActions(*d_motionScriptLibrary, *d_motionScriptModule);

  // Create StateObservers
  d_gyroCalibrator = StateObserver::make<GyroCalibrator>();
  d_healthAndSafety = StateObserver::make<HealthAndSafety>(d_voice, d_clock);
  d_jamTracker = StateObserver::make<JamDetector>(d_voice);
  d_odometer = StateObserver::make<Odometer>();
  d_orientationTracker = StateObserver::make<OrientationTracker>();
  d_suicidePill = StateObserver::make<SuicidePill>(this, d_clock, d_debugger);
  d_stabilityObserver = StateObserver::make<StabilityObserver>(d_bodyModel);
  d_teamCommunicator = StateObserver::make<OpenTeamCommunicator>(d_behaviourControl, d_clock);
  d_vocaliser = StateObserver::make<Vocaliser>(d_voice);
  auto stationaryMapper = StateObserver::make<StationaryMapper>(d_voice, d_behaviourControl);
  auto handlerHelper = StateObserver::make<HandlerHelper>(d_voice, d_clock, d_behaviourControl);

  // Register StateObservers
  State::registerObserver(d_buttonObserver);
  State::registerObserver(d_vocaliser);
  State::registerObserver(d_gyroCalibrator);
  State::registerObserver(d_healthAndSafety);
  State::registerObserver(d_jamTracker);
  State::registerObserver(d_suicidePill);
  State::registerObserver(d_odometer);
  State::registerObserver(d_stabilityObserver);
  State::registerObserver(d_teamCommunicator);
  State::registerObserver(d_orientationTracker);
  State::registerObserver(move(stationaryMapper));
  State::registerObserver(move(handlerHelper));

  // Special handling for fall detection based upon config
  switch (Config::getStaticValue<FallDetectorTechnique>("fall-detector.technique")) {
    case FallDetectorTechnique::Accelerometer: {
      auto fallDetector = make_shared<AccelerometerFallDetector>(d_voice, d_clock);
      d_fallDetector = fallDetector;
      State::registerObserver(fallDetector);
      break;
    }
    case FallDetectorTechnique::Orientation: {
      auto fallDetector = make_shared<OrientationFallDetector>(d_voice, d_clock);
      d_fallDetector = fallDetector;
      State::registerObserver(fallDetector);
      break;
    }
  }

  d_cameraModel = allocate_aligned_shared<CameraModel>();

  d_spatialiser = make_shared<Spatialiser>(d_cameraModel);

  d_roleDecider = make_shared<RoleDecider>(d_behaviourControl, d_voice, d_clock);

  // Create camera
  d_camera->open();
  d_cameraController->createSettings();

  // Configure camera
  unsigned width = d_cameraModel->imageWidth();
  unsigned height = d_cameraModel->imageHeight();
  if (!d_camera->requestSize(width, height))
    Log::error() << "[Agent::initCamera] Requesting camera size " << width << "x" << height << " failed";

  // Start capturing camera images
  d_camera->start();

  d_roundTableServer = make_shared<roundtable::RoundTableServer>(d_clock, d_motionScriptLibrary);

  d_visualCortex = make_shared<VisualCortex>(d_cameraModel, d_spatialiser, d_clock, *d_roundTableServer);

  d_messageCounter = make_shared<MessageCounter>();
  d_gameStateReceiver = make_shared<GameStateReceiver>(d_messageCounter, d_voice, d_clock);
  d_gameStateReceiver->addDecoder(make_unique<GameStateDecoderVersion7>());
  d_gameStateReceiver->addDecoder(make_unique<GameStateDecoderVersion8>());
  d_gameStateReceiver->addDecoder(make_unique<GameStateDecoderVersion12>());

  d_remoteControl = make_shared<RemoteControl>(this);

  if (Config::getStaticValue<bool>("drawbridge.enabled"))
    d_drawBridgeComms = make_shared<DrawBridgeComms>(this, d_behaviourControl, d_messageCounter);

  d_debugger->update();

  d_motionLoop = make_shared<MotionLoop>(debugControl, d_bodyModel, d_clock);

  d_motionLoop->addCommsModule(make_shared<MX28HealthChecker>(d_voice));

  d_motionLoop->onReadFailure.connect([this](uint count) -> void {
    // If we were unable to read once during the last second, then we've lost
    // the CM730. This is probably because someone pressed the hardware reset
    // button on the back. In this case, stop the agent. The upstart service
    // should restart it immediately during competitions.
    // TODO consider reconnecting to the CM730, as we already have a means of exiting the robot by holding down two buttons
    // TODO place read failure timeout duration in config
    if (count > 1000 / 8) // 1 second
    {
      Log::error("Agent") << "MotionLoop has been unable to read " << count << " times, so shutting down";
      exit(EXIT_FAILURE);
    }
  });

  d_roundTableServer->start();
}

void Agent::onLoopStart() {
  ThreadUtil::setThreadId(ThreadId::ThinkLoop);
}

void Agent::onStep(unsigned cycleNumber) {
  ASSERT(ThreadUtil::isThinkLoopThread());

  Log::trace("Agent::think") << "Starting think cycle " << cycleNumber << " --------------------------";

  auto clock = make_shared<SystemClock>();

  SequentialTimer t(clock);

  //
  // Capture the image (YCbCr format)
  // This should be the very first thing done in the think loop, to
  // ensure a BodyState snapshot is available
  //
  t.enter("Image Capture");
  cv::Mat image = d_camera->capture();
  t.exit();

  //
  // Update Spatialiser internals
  //
  d_spatialiser->updateZeroGroundPixelTransform();

  //
  // Process the image
  //
  t.enter("Vision");
  d_visualCortex->integrateImage(image, t, cycleNumber);
  t.exit();

  d_visualCortex->streamDebugImage(image, t);

  //
  // Listen for any game control data
  //
  d_gameStateReceiver->receive();
  t.timeEvent("Integrate Game Control");

  //
  // Populate agent frame from camera frame
  //
  d_spatialiser->updateCameraToAgent();
  t.timeEvent("Camera to Agent Frame");

  //
  // Populate world frame from agent frame
  //
  //d_spatialiser->updateAgentToWorld(d_localiser->smoothedPosition());
  //t.timeEvent("Agent to World Frame");

  //
  // Attempt to receive from other agents
  //
  d_teamCommunicator->receiveData();
  t.timeEvent("Team Receive");

  //
  // Decide role, updating BehaviourControl
  //
  d_roleDecider->update();
  t.timeEvent("Role Decision");

  //
  // Run the option tree to execute behaviour
  //
  d_optionTree->run();
  t.timeEvent("Option Tree");

  //
  // Process remote control (eg. joystick)
  //
  d_remoteControl->update();
  t.timeEvent("Remote Control");

  //
  // Update some per-think-cycle state objects
  //

  d_debugger->update();
  t.timeEvent("Update Debugger");

  d_messageCounter->updateStateObject();
  t.timeEvent("Update Message Count");

  //
  // Refresh MotionTaskState, if one is needed
  //
  d_motionSchedule->update();
  t.timeEvent("Update Motion Schedule");

  //
  // Publish a snapshot of the behaviour control object
  //
  d_behaviourControl->updateStateObject();
  t.timeEvent("Publish Behaviour Control State");

  //
  // Invoke observers which requested to be called back from the think loop
  //
  t.enter("Observers");
  State::callbackObservers(ThreadId::ThinkLoop, t);
  t.exit();

  //
  // Send a message for drawbridge use within matches, containing status of the agent.
  //
  if (d_drawBridgeComms && cycleNumber % 30 == 0)
    d_drawBridgeComms->publish();

  // Flush out any drawing commands
  d_draw->flushToStateObject();
  t.timeEvent("Flush Drawing Items");

  //
  // Set timing data for the think cycle
  //
  static FPS<30> fps(d_clock);
  State::make<ThinkTimingState>(t.flush(), cycleNumber, fps.next());

  Log::trace("Agent::think") << "Ending think cycle " << cycleNumber;
}

void Agent::registerStateTypes() {
  State::registerStateType<AccelerometerState>("Accelerometer");
  State::registerStateType<AgentFrameState>("AgentFrame");
  State::registerStateType<AudioPowerSpectrumState>("AudioPowerSpectrum");
  State::registerStateType<BalanceState>("Balance");
  State::registerStateType<BehaviourControlState>("BehaviourControl");
  State::registerStateType<BodyControlState>("BodyControl");
  State::registerStateType<BodyState>("Body");
  State::registerStateType<CameraFrameState>("CameraFrame");
  State::registerStateType<DrawingState>("Drawing");
  State::registerStateType<FieldEdgeState>("FieldEdge");
  State::registerStateType<FSRState>("FSR");
  State::registerStateType<GameState>("Game");
  State::registerStateType<GyroCalibrationState>("GyroCalibrationState");
  State::registerStateType<HardwareState>("Hardware");
  State::registerStateType<LabelCountState>("LabelCount");
  State::registerStateType<LabelTeacherState>("LabelTeacher");
  State::registerStateType<LEDState>("LED");
  State::registerStateType<MessageCountState>("MessageCount");
  State::registerStateType<MotionTaskState>("MotionTask");
  State::registerStateType<MotionTimingState>("MotionTiming");
  State::registerStateType<OdometryState>("Odometry");
  State::registerStateType<TeamState>("Team");
  State::registerStateType<OptionTreeState>("OptionTree");
  State::registerStateType<OrientationState>("Orientation");
  State::registerStateType<ParticleState>("Particle");
  State::registerStateType<StabilityState>("Stability");
  State::registerStateType<StationaryMapState>("StationaryMap");
  State::registerStateType<StaticHardwareState>("StaticHardware");
  State::registerStateType<ThinkTimingState>("ThinkTiming");
  State::registerStateType<RobotisWalkState>("RobotisWalk");
  State::registerStateType<WorldFrameState>("WorldFrame");
}

void Agent::run()
{
  if (d_motionLoop->isRunning())
    throw runtime_error("Motion loop already running");
  if (isRunning())
    throw runtime_error("Think loop already running");

  if (d_voice)
  {
    ostringstream announcement;
    announcement <<
                 "Player " << (int)d_uniformNumber <<
                 " on team " << (int)d_teamNumber <<
                 ", " <<
                 (d_teamColour == TeamColour::Cyan ? "cyan" : "magenta");
    d_voice->say(announcement.str());
  }

  // Start the motion loop
  if (!d_motionLoop->start())
  {
    Log::error("Agent::run") << "Unable to start motion loop";
    throw runtime_error("Unable to start motion loop");
  }

  //
  // Become the think loop...
  //

  // Wait until the motion loop has read a hardware value
  Log::info("Agent::run") << "Waiting for sufficient Motion Loop cycles";
  while (d_motionLoop->getCycleNumber() < 5)
    usleep(8000);

  Log::info("Agent::run") << "Starting think loop";

  // Start the think loop
  start();

  while (isRunning() || d_motionLoop->isRunning())
    usleep(100 * 1000);

  Log::info("Agent::run") << "Stopped";
}

unsigned Agent::getThinkCycleNumber() const {
  return getCycleNumber();
}

void Agent::setOptionTree(shared_ptr<OptionTree> tree) {
  Log::verbose("Agent::setOptionTree") << "Setting OptionTree";
  d_optionTree = tree;
  d_roundTableServer->setOptionTree(tree);
}

void Agent::requestShutdown(bool forceOngoingShutdown) {
  if (!isRunning())
    throw runtime_error("Not running");

  if (d_isShutdownRequested && forceOngoingShutdown) {
    Log::warning("Agent::requestShutdown") << "Forcing shutdown";
    exit(EXIT_FAILURE);
  }

  Log::info("Agent::requestShutdown");

  // The option tree picks up this flag and performs a shutdown sequence
  d_isShutdownRequested = true;

  d_voice->say("Shutting down");
}

void Agent::onStopped() {
  d_roundTableServer->stop();

  if (d_motionLoop)
    d_motionLoop->stop();

  if (d_voice)
    d_voice->stop();

  Log::info("Agent::stop") << "Stopped";
}
