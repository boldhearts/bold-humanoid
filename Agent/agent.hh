// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <memory>

#include "Clock/clock.hh"
#include "util/Loop/loop.hh"
#include "util/TeamColour/teamcolour.hh"

namespace bold {
  class BehaviourControl;

  class BodyModel;

  class ButtonObserver;

  class Debugger;

  class Draw;

  class DrawBridgeComms;

  class FallDetector;

  class GameStateReceiver;

  class GyroCalibrator;

  class HealthAndSafety;

  class JamDetector;

  class MessageCounter;

  class MotionLoop;

  class Odometer;

  class OptionTree;

  class OpenTeamCommunicator;

  class OrientationTracker;

  class RemoteControl;

  class RoleDecider;

  class Spatialiser;

  class StabilityObserver;

  class SuicidePill;

  class Vocaliser;

  class Voice;

  namespace motion {
    namespace core {
      class MotionTaskScheduler;
    }
    namespace modules {
      class MotionScriptModule;

      class HeadModule;

      class StandModule;

      class WalkModule;
    }
    namespace scripts {
      class MotionScriptLibrary;
    }
  }

  namespace roundtable {
    class RoundTableServer;
  }

  namespace vision {
    class Camera;

    class CameraModel;

    class CameraController;

    class VisualCortex;
  }

  class Agent : public util::Loop {
  public:
    static void registerStateTypes();

    Agent(std::shared_ptr<Clock> clock, std::shared_ptr<Draw> draw,
          std::unique_ptr<vision::Camera> camera, std::unique_ptr<vision::CameraController> cameraController);

    std::shared_ptr<ButtonObserver> getButtonObserver() const { return d_buttonObserver; }

    std::shared_ptr<vision::CameraModel> getCameraModel() const { return d_cameraModel; }

    std::shared_ptr<BehaviourControl> getBehaviourControl() const { return d_behaviourControl; }

    std::shared_ptr<motion::modules::HeadModule> getHeadModule() const { return d_headModule; }

    std::shared_ptr<motion::modules::WalkModule> getWalkModule() const { return d_walkModule; }

    std::shared_ptr<motion::modules::MotionScriptModule> getMotionScriptModule() const { return d_motionScriptModule; }

    std::shared_ptr<motion::modules::StandModule> getStandModule() const { return d_standModule; }

    std::shared_ptr<FallDetector const> getFallDetector() const { return d_fallDetector; }

    std::shared_ptr<Voice> getVoice() const { return d_voice; }

    std::shared_ptr<roundtable::RoundTableServer> getRoundTableServer() const { return d_roundTableServer; }

    void setOptionTree(std::shared_ptr<OptionTree> tree);

    Agent(Agent const &) = delete;

    Agent &operator=(Agent const &) = delete;

    void run();

    void requestShutdown(bool forceOngoingShutdown);

    bool isShutdownRequested() const { return d_isShutdownRequested; }

    unsigned getThinkCycleNumber() const;

    double getUptimeSeconds() const { return d_clock->getSecondsSince(d_startTime); }

  private:
    void onLoopStart() override;

    void onStep(unsigned cycleNumber) override;

    void onStopped() override;

    bool d_isShutdownRequested;

    uint8_t const d_teamNumber;
    uint8_t const d_uniformNumber;
    util::TeamColour const d_teamColour;
    std::string const d_model;

    std::shared_ptr<Clock> d_clock;
    std::shared_ptr<Draw> d_draw;
    std::shared_ptr<BodyModel> d_bodyModel;

    // Motion
    std::shared_ptr<MotionLoop> d_motionLoop;
    std::shared_ptr<motion::core::MotionTaskScheduler> d_motionSchedule;
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    std::shared_ptr<motion::modules::MotionScriptModule> d_motionScriptModule;
    std::shared_ptr<motion::modules::StandModule> d_standModule;
    std::shared_ptr<motion::scripts::MotionScriptLibrary> d_motionScriptLibrary;

    // State observers

    std::shared_ptr<BehaviourControl> d_behaviourControl;
    std::shared_ptr<ButtonObserver> d_buttonObserver;
    std::shared_ptr<FallDetector const> d_fallDetector;
    std::shared_ptr<GyroCalibrator> d_gyroCalibrator;
    std::shared_ptr<HealthAndSafety> d_healthAndSafety;
    std::shared_ptr<JamDetector> d_jamTracker;
    std::shared_ptr<Odometer> d_odometer;
    std::shared_ptr<OpenTeamCommunicator> d_teamCommunicator;
    std::shared_ptr<OrientationTracker> d_orientationTracker;
    std::shared_ptr<RoleDecider> d_roleDecider;
    std::shared_ptr<StabilityObserver> d_stabilityObserver;
    std::shared_ptr<SuicidePill> d_suicidePill;
    std::shared_ptr<Vocaliser> d_vocaliser;

    // Components

    std::unique_ptr<vision::Camera> d_camera;
    std::shared_ptr<vision::CameraModel> d_cameraModel;
    std::unique_ptr<vision::CameraController> d_cameraController;

    std::shared_ptr<roundtable::RoundTableServer> d_roundTableServer;
    std::shared_ptr<Debugger> d_debugger;
    std::shared_ptr<DrawBridgeComms> d_drawBridgeComms;
    std::shared_ptr<GameStateReceiver> d_gameStateReceiver;
    std::shared_ptr<MessageCounter> d_messageCounter;
    std::shared_ptr<OptionTree> d_optionTree;
    std::shared_ptr<RemoteControl> d_remoteControl;
    std::shared_ptr<Spatialiser> d_spatialiser;
    std::shared_ptr<vision::VisualCortex> d_visualCortex;
    std::shared_ptr<Voice> d_voice;

    unsigned d_cycleNumber;

    Clock::Timestamp d_startTime;
  };
}
