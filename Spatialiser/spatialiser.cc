// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "spatialiser.hh"

#include "Math/math.hh"
#include "state/State/state.hh"
#include "state/StateObject/AgentFrameState/agentframestate.hh"
#include "state/StateObject/BodyState/bodystate.hh"
#include "state/StateObject/CameraFrameState/cameraframestate.hh"
#include "state/StateObject/WorldFrameState/worldframestate.hh"

#include "FieldMap/fieldmap.hh"
#include "geometry2/Operator/Predicate/Contains/contains.hh"
#include "vision/CameraModel/cameramodel.hh"

using namespace bold;
using namespace bold::config;
using namespace bold::geometry2;
using namespace bold::state;
using namespace bold::util;
using namespace bold::vision;
using namespace Eigen;
using namespace std;

Spatialiser::Spatialiser(shared_ptr<CameraModel> cameraModel)
    : d_cameraModel(std::move(cameraModel)) {
  d_lineJunctionFinder = make_shared<LineJunctionFinder>();
}

Matrix3d Spatialiser::findGroundPixelTransform(double groundZ) const {
  return findGroundPixelTransform(State::get<BodyState>(StateTime::CameraImage)->getAgentCameraTransform(), groundZ);
}

Matrix3d Spatialiser::findGroundPixelTransform(Eigen::Affine3d const &agentCameraTr, double groundZ) const {
  // point in agent frame p to pixel i: i = T * p =

  // | t11 t12 t13 t14 |   | p1 |   | u * w |
  // | t21 t22 t23 t24 |   | p2 |   | v * w |
  // | t31 t32 t33 t34 | * | p3 | = | w     |
  // | t41 t42 t43 t44 |   | 1  |   | 1     |

  // We have fixed p3 = 0, and t41 and t42 are always 0, so
  // simplification:

  // | t11 t12 t14 |   | p1 |   | u * w |
  // | t21 t22 t24 |   | p2 |   | v * w |
  // | t31 t32 t34 | * | 1  | = | w     |

  // Bringing w to other side:

  // | t11 t12 t14 |   | p1 / w |   | u |
  // | t21 t22 t24 |   | p2 / w |   | v |
  // | t31 t32 t34 | * | 1  / w | = | 1 |

  // Note u and v are in pixels wrt center of image
  auto groundCameraTr = Translation3d(0, 0, -groundZ) * agentCameraTr;

  // from camera frame to image frame
  auto imageCameraTr = d_cameraModel->getImageCameraTransform();

  // from agent frame to camera frame
  auto cameraGroundTr = groundCameraTr.inverse();

  // from agent frame to image frame
  auto imageGroundTr = imageCameraTr * cameraGroundTr;

  auto Tm = imageGroundTr.matrix();
  auto Tp = (Matrix3d{} <<
                        Tm(0, 0), Tm(0, 1), Tm(0, 3),
      Tm(1, 0), Tm(1, 1), Tm(1, 3),
      Tm(2, 0), Tm(2, 1), Tm(2, 3)).finished();

  auto groundPixelTr = Matrix3d{Tp.inverse()};

  return groundPixelTr;
}

Maybe<Point3d> Spatialiser::findGroundPointForPixel(Vector2d const &pixel, double groundZ) const {
  return findGroundPointForPixel(pixel, groundZ == 0 ?
                                        d_zeroGroundPixelTr :
                                        findGroundPixelTransform(
                                            State::get<BodyState>(StateTime::CameraImage)->getAgentCameraTransform(),
                                            groundZ));
}

Maybe<Point3d> Spatialiser::findGroundPointForPixel(Vector2d const &pixel,
                                                    Affine3d const &agentCameraTr,
                                                    double groundZ) const {
  auto groundPixelTr = findGroundPixelTransform(agentCameraTr, groundZ);
  return findGroundPointForPixel(pixel, groundPixelTr);
}

Maybe<Point3d> Spatialiser::findGroundPointForPixel(Vector2d const &pixel, Matrix3d const &groundPixelTr) const {
  auto b = Vector3d{pixel(0) - d_cameraModel->imageWidth() / 2.0,
                    pixel(1) - d_cameraModel->imageHeight() / 2.0,
                    1};

  auto pt = groundPixelTr * b;

  if (pt(2) < 0)
    return util::Maybe<Point3d>::empty();
  else {
    auto point = Point3d{pt(0) / pt(2), pt(1) / pt(2), 0.0};
    return util::Maybe<Point3d>{point};
  }
}


pair<MatrixXd, VectorXi> Spatialiser::findGroundPointsForPixels(MatrixXd const &pixels,
                                                                double groundZ) const {
  return findGroundPointsForPixels(pixels, groundZ == 0 ?
                                           d_zeroGroundPixelTr :
                                           findGroundPixelTransform(
                                               State::get<BodyState>(StateTime::CameraImage)->getAgentCameraTransform(),
                                               groundZ));
}

pair<MatrixXd, VectorXi> Spatialiser::findGroundPointsForPixels(MatrixXd const &pixels,
                                                                Affine3d const &agentCameraTr,
                                                                double groundZ) const {
  auto groundPixelTr = findGroundPixelTransform(agentCameraTr, groundZ);
  return findGroundPointsForPixels(pixels, groundPixelTr);
}

pair<MatrixXd, VectorXi> Spatialiser::findGroundPointsForPixels(MatrixXd const &pixels,
                                                                Matrix3d const &groundPixelTr) const {
  auto b = (MatrixXd{3, pixels.cols()} << pixels, VectorXd::Zero(pixels.cols())).finished();
  b.row(0) = (b.row(0).array() - d_cameraModel->imageWidth()) / 2.0;
  b.row(1) = (b.row(1).array() - d_cameraModel->imageHeight()) / 2.0;

  auto pts = MatrixXd{groundPixelTr * b};

  auto valid = VectorXi{(pts.row(2).array() >= 0).cast<int>()};

  return make_pair(pts, valid);
  //pts.colwise()
}

int Spatialiser::findHorizonForColumn(int column) {
  return findHorizonForColumn(column, State::get<BodyState>(StateTime::CameraImage)->getCameraAgentTransform());
}

int Spatialiser::findHorizonForColumn(int column, Affine3d const &cameraAgentTr) {
  // TODO: can we derive directly from perspective transform?

  //
  // Equation of horizon line:
  // http://mi.eng.cam.ac.uk/~cipolla/lectures/4F12/Examples/old/solutions2.pdf
  //
  // | x |
  // | y | . n = 0
  // | f |
  //
  // Where n is normal of plane parallel to ground (in image frame).
  //
  // x nx + f ny + z nz = 0
  // y = -1/ny (x nx + f nz)

  ASSERT(d_cameraModel);
  ASSERT(d_cameraModel->imageWidth() > 1);

  // x on projection plane
  // Todo: should be column + 0.5 to match convention elsewhere?
  double x = (2.0 * column / (d_cameraModel->imageWidth() - 1.0)) - 1.0;

  // From camera to clip space frame (intermediate of camera to image transform)
  // TODO: cache
  Eigen::Affine3d clipCameraTr;
  clipCameraTr.matrix() <<
                        -1, 0, 0, 0,
      0, 0, 1, 0,
      0, 1, 0, 0,
      0, 0, 0, 1;

  auto clipAgentTr = clipCameraTr * cameraAgentTr;

  // Normal to ground plane, in clip frame
  Vector3d up = clipAgentTr.matrix().col(2).head<3>();

  // In this case, we're looking straight up or down
  // TODO: handle better
  ASSERT(up.y() != 0);

  double f = d_cameraModel->focalLength();

  // Solution to | x y f |^T . n = 0
  double y = -1 / up.y() * (x * up.x() + f * up.z());

  double r = f * tan(d_cameraModel->rangeVerticalRads() / 2);

  // TODO: check if matches convention
  int py = ((y / r + 1.0) * (d_cameraModel->imageHeight() - 1.0) / 2.0) + .5;
  return py;
}

Maybe<Vector2d> Spatialiser::findPixelForAgentPoint(Point3d const &agentPoint) const {
  return findPixelForAgentPoint(agentPoint, State::get<BodyState>(StateTime::CameraImage)->getCameraAgentTransform());
}

Maybe<Vector2d> Spatialiser::findPixelForAgentPoint(Point3d const &agentPoint, Affine3d const &cameraAgentTr) const {
  return d_cameraModel->pixelForDirection(cameraAgentTr * (agentPoint - Point3d::ORIGIN));
}

Vector2d Spatialiser::getCameraDown() const {
  return getCameraDown(State::get<BodyState>(StateTime::CameraImage)->getCameraAgentTransform());
}

Vector2d Spatialiser::getCameraDown(Affine3d const &cameraAgentTr) const {
  // TODO: this replicates some code from horizon method
  Eigen::Affine3d clipCameraTr;
  clipCameraTr.matrix() <<
                        -1, 0, 0, 0,
      0, 0, 1, 0,
      0, 1, 0, 0,
      0, 0, 0, 1;

  auto clipAgentTr = clipCameraTr * cameraAgentTr;

  // 3rd column has up direction of agent in clip frame
  return -clipAgentTr.matrix().col(2).head<2>().normalized();
}

void Spatialiser::updateAgentToWorld(AgentPosition position) {
  auto agentFrame = State::get<AgentFrameState>();

  //
  // Transform from agent to world space
  //

  Affine3d worldAgentTransform = position.worldAgentTransform();

  // TODO: Think of nice way to transform points, or at least measure preformance
  auto transformPoint = [](Affine3d const &transform, Point3d const &point) {
    return Point3d::ORIGIN + transform * (point - Point3d::ORIGIN);
  };

  // Project ball observation
  Maybe<Point3d> ball;
  for (auto const &ballAgent : agentFrame->getBallObservation())
    ball = make_maybe(transformPoint(worldAgentTransform, ballAgent));

  // Project goal observations
  Point3d::Vector goals;
  for (auto const &goalPos : agentFrame->getGoalObservations()) {
    goals.emplace_back(transformPoint(worldAgentTransform, goalPos));
  }

  // Project observed lines
  vector<LineSegment3d> lineSegments;
  for (auto const &lineSegmentAgent : agentFrame->getObservedLineSegments()) {
    auto lineSegment = LineSegment3d{
        transformPoint(worldAgentTransform, lineSegmentAgent.p1()),
        transformPoint(worldAgentTransform, lineSegmentAgent.p2())
    };
    lineSegments.emplace_back(lineSegment);
  }

  // Project occlusion rays
  OcclusionRay<double>::Vector occlusionRays;
  // TODO perform the rotation/translation in 2D (where z == 0), not in 3D
  for (auto const &ray : agentFrame->getOcclusionRays()) {
    auto near = ray.near().toDim<3>();
    auto far = ray.far().toDim<3>();

    occlusionRays.emplace_back(transformPoint(worldAgentTransform, near).template toDim<2>(),
                               transformPoint(worldAgentTransform, far).template toDim<2>());
  }

  // Determine observed field area polygon
  Point2d::Vector vertices;
  for (auto poly : agentFrame->getVisibleFieldPoly())
    for (auto const &vertex : poly) {
      auto vertex3 = Point3d{vertex.x(), vertex.y(), 0};
      vertices.emplace_back(transformPoint(worldAgentTransform, vertex3).template toDim<2>());
    }

  Maybe<Polygon2d> visibleFieldPoly =
      vertices.size() == 4 ? Maybe<Polygon2d>(Polygon2d(vertices)) : Maybe<Polygon2d>::empty();

  State::make<WorldFrameState>(ball, goals, lineSegments, visibleFieldPoly, occlusionRays, position);
}

void Spatialiser::updateCameraToAgent() {
  auto cameraFrame = State::get<CameraFrameState>();

  static double ballRadius = FieldMap::getBallRadius();
  double goalieMarkerHeight = Config::getValue<double>("vision.player-detection.goalie-marker-height");

  // Project ball observation
  util::Maybe<Point3d> ball;
  for (auto const &ballObs : cameraFrame->getBallObservation())
    ball = findGroundPointForPixel(ballObs, ballRadius);

  // Project goal observations
  Point3d::Vector goals;
  for (auto const &goal : cameraFrame->getGoalObservations()) {
    auto const &pos3d = findGroundPointForPixel(goal);
    if (pos3d.hasValue())
      goals.emplace_back(*pos3d);
  }

  // Project occlusion rays
  auto cameraOcclusionRays = cameraFrame->getOcclusionRays();
  OcclusionRay<double>::Vector occlusionRays;
  for (uint i = 0; i < cameraOcclusionRays.size(); i++) {
    auto const &ray = cameraOcclusionRays[i];
    auto const &p1 = findGroundPointForPixel(ray.near().cast<double>() + Vector2d(0.5, 0.5));
    auto const &p2 = findGroundPointForPixel(ray.far().cast<double>() + Vector2d(0.5, 0.5));
    if (p1.hasValue() && p2.hasValue())
      occlusionRays.emplace_back(p1->head<2>(), p2->head<2>());
  }

  bool enableOcclusion = Config::getSetting<bool>("vision.player-detection.enable-occlusion-check")->getValue();
  auto occlusionPoly = AgentFrameState::getOcclusionPoly(occlusionRays);

  // Project team mate observations
  Point3d::Vector teamMates;
  for (auto const &teamMate : cameraFrame->getTeamMateObservations()) {
    auto const &pos3d = findGroundPointForPixel(teamMate, goalieMarkerHeight);
    if (pos3d.hasValue()) {
      bool validPlayer = true;
      if (enableOcclusion)
        if (occlusionPoly && !contains(*occlusionPoly, pos3d->toDim<2>()))
          validPlayer = false;

      if (validPlayer)
        teamMates.emplace_back(*pos3d);
    }
  }

  // Project observed lines
  LineSegment3d::Vector lineSegments;
  for (LineSegment2i const &lineSegment : cameraFrame->getObservedLineSegments()) {
    auto const &p1 = findGroundPointForPixel(lineSegment.p1().cast<double>() + Vector2d(0.5, 0.5));
    auto const &p2 = findGroundPointForPixel(lineSegment.p2().cast<double>() + Vector2d(0.5, 0.5));
    if (p1.hasValue() && p2.hasValue())
      lineSegments.emplace_back(*p1, *p2);
  }

  // Find line junctions
  auto lineJunctions = d_lineJunctionFinder->findLineJunctions(lineSegments);

  // Determine observed field area polygon
  Point2d::Vector vertices;

  static int width = d_cameraModel->imageWidth();
  static int height = d_cameraModel->imageHeight();

  int horiz1 = min(height - 1, findHorizonForColumn(0));
  int horiz2 = min(height - 1, findHorizonForColumn(width - 1));

  auto const &p1 = findGroundPointForPixel(Vector2d(0, 0) + Vector2d(0.5, 0.5));
  if (p1)
    vertices.emplace_back(p1->toDim<2>());

  auto const &p2 = findGroundPointForPixel(Vector2d(width - 3, 0) + Vector2d(0.5, 0.5));
  if (p2)
    vertices.emplace_back(p2->toDim<2>());

  if (horiz2 >= 0 && horiz2 < height) {
    auto const &p3 = findGroundPointForPixel(Vector2d(width - 3, horiz2) + Vector2d(0.5, 0.5));
    if (p3)
      vertices.emplace_back(p3->toDim<2>());
  }

  if (horiz1 >= 0 && horiz1 < height) {
    auto const &p4 = findGroundPointForPixel(Vector2d(0, horiz1) + Vector2d(0.5, 0.5));
    if (p4)
      vertices.emplace_back(p4->toDim<2>());
  }

  auto visibleFieldPoly =
      vertices.size() == 4 ? make_maybe(Polygon2d{vertices}) : Maybe<Polygon2d>::empty();

  State::make<AgentFrameState>(ball, goals, teamMates,
                               lineSegments, lineJunctions,
                               visibleFieldPoly, occlusionRays,
                               cameraFrame->getThinkCycleNumber());
}

void Spatialiser::updateZeroGroundPixelTransform() {
  updateZeroGroundPixelTransform(State::get<BodyState>(StateTime::CameraImage)->getAgentCameraTransform());
}

void Spatialiser::updateZeroGroundPixelTransform(Eigen::Affine3d const &agentCameraTr) {
  d_zeroGroundPixelTr = findGroundPixelTransform(agentCameraTr, 0.0);
}
