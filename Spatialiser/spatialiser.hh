// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "src/geometry2/Point/point.hh"
#include "AgentPosition/agentposition.hh"
#include "src/util/Maybe/maybe.hh"

namespace bold
{
  namespace vision {
    class CameraModel;
  }

  class LineJunctionFinder;

  class Spatialiser
  {
  public:
    Spatialiser(std::shared_ptr<vision::CameraModel> cameraModel);

    void updateZeroGroundPixelTransform();

    void updateZeroGroundPixelTransform(Eigen::Affine3d const& agentCameraTr);

    Eigen::Matrix3d getZeroGroundPixelTransform() const { return d_zeroGroundPixelTr; }

    Eigen::Matrix3d findGroundPixelTransform(double groundZ = 0.0) const;

    Eigen::Matrix3d findGroundPixelTransform(Eigen::Affine3d const& agentCameraTr,
                                             double groundZ = 0.0) const;

    // TODO rename these two functions to indicate they work in agent space

    /** Returns the ground-plane location, in agent space, for a given
     * camera pixel.
     *
     * @param pixel The x/y pixel location
     * @param groundZ The z coordinate of the ground plane that is used; default is 0
     */
    util::Maybe<geometry2::Point3d> findGroundPointForPixel(Eigen::Vector2d const& pixel, double groundZ = 0.0) const;

    /** Returns the ground-plane location, in agent space, for a given
     * camera pixel and camera transformation.
     *
     * @param pixel The x/y pixel location
     * @param agentCameraTr The transformation from camera to agent frame
     * @param groundZ The z coordinate of the ground plane that is used; default is 0
     */
    util::Maybe<geometry2::Point3d> findGroundPointForPixel(Eigen::Vector2d const& pixel,
                                                            Eigen::Affine3d const& agentCameraTr,
                                                            double groundZ = 0) const;


    /** Returns the ground-plane locations, in agent space, for a
     * given set of camera pixels.
     *
     * @param pixels A 2xN matrix, where each column denotes a pixel coordinate
     * @param groundZ The z coordinate of the ground plane that is used; default is 0
     * @returns a pair containing a 3xN matrix of ground locations and
     * an N-dimensional vector that denotes whether these are valid
     */
    std::pair<Eigen::MatrixXd, Eigen::VectorXi> findGroundPointsForPixels(Eigen::MatrixXd const& pixels,
                                                                          double groundZ = 0.0) const;

    /** Returns the ground-plane locations, in agent space, for a
     * given set of camera pixels and camera transfrmation.
     *
     * @param pixels A 2xN matrix, where each column denotes a pixel coordinate
     * @param agentCameraTr The transformation from camera to agent frame
     * @param groundZ The z coordinate of the ground plane that is used; default is 0
     */
    std::pair<Eigen::MatrixXd, Eigen::VectorXi> findGroundPointsForPixels(Eigen::MatrixXd const& pixels,
                                                                          Eigen::Affine3d const& agentCameraTr,
                                                                          double groundZ = 0.0) const;

    util::Maybe<Eigen::Vector2d> findPixelForAgentPoint(geometry2::Point3d const& agentPoint) const;

    util::Maybe<Eigen::Vector2d> findPixelForAgentPoint(geometry2::Point3d const& agentPoint,
                                                        Eigen::Affine3d const& agentCameraTr) const;

    int findHorizonForColumn(int column);
    int findHorizonForColumn(int column, Eigen::Affine3d const& cameraAgentTr);

    Eigen::Vector2d getCameraDown(Eigen::Affine3d const& cameraAgentTr) const;
    Eigen::Vector2d getCameraDown() const;
    
    void updateCameraToAgent();

    void updateAgentToWorld(AgentPosition position);

    std::shared_ptr<vision::CameraModel> getCameraModel() const { return d_cameraModel; }


  private:
    util::Maybe<geometry2::Point3d> findGroundPointForPixel(Eigen::Vector2d const& pixel,
                                                            Eigen::Matrix3d const& groundPixelTr) const;
    std::pair<Eigen::MatrixXd, Eigen::VectorXi> findGroundPointsForPixels(Eigen::MatrixXd const& pixels,
                                                                          Eigen::Matrix3d const& groundPixelTr) const;

    std::shared_ptr<vision::CameraModel> d_cameraModel;
    std::shared_ptr<LineJunctionFinder> d_lineJunctionFinder;

    Eigen::Matrix3d d_zeroGroundPixelTr;
  };
}
