%module(directors="1") glatisant

// Can not create objects, unless we tell you to
%nodefaultctor;
// Create a lot of docs
%feature("autodoc", "3");

// C++ director exception handling
// Added to wrapping code
%feature("director:except") {
  if ($error != NULL) {
    PyErr_Print();
    throw Swig::DirectorMethodException();
  }
}

%feature("flatnested");

// Include std library interfaces
%include <stdint.i>
%include <stl.i>
%include <std_shared_ptr.i>
%include "eigen.i"

// Have to list all classes of which a shared_ptr is used (plus their
// (grand)parent classes, just to be sure). Must be listed before any
// use
%shared_ptr(bold::motion::scripts::MotionScript)
%shared_ptr(bold::motion::scripts::MotionScript::Stage)

// List special STL container instantiations
%template() std::vector<PyObject*>;
%template() std::vector<std::shared_ptr<bold::Option> >;
%template() std::vector<std::shared_ptr<bold::motion::scripts::MotionScript::Stage> >;

%feature("kwargs");

// Now define all interfaces that we want to be available in Python In
// theory we can also %include all header files, but that often breaks
// (eg C++1y stuff, and usually inlined and private stuff)

%include "colour/HSVRange/hsvrange.i"
%include "util/Range/range.i"
%include "config/Config/config.i"
%include "motion/scripts/MotionScript/motionscript.i"
