import unittest

import os

from .context import glatisant
from glatisant import Config, MotionScript

class TestConfig(unittest.TestCase):

    def setUp(self):
        if Config.isInitialising():
            Config.initialise("../configuration-metadata.json", "../configuration-hatfield.json")
            Config.initialisationCompleted();
            
    def test_getSetting(self):
        # int
        self.assertTrue(Config.getIntValue("team-number") > 0)

        # double
        self.assertTrue(Config.getDoubleValue("mitecom.send-period-seconds") > 0.0)
        
        # bool
        ignore_above_horizon = Config.getBoolValue("vision.ignore-above-horizon")
        self.assertTrue(ignore_above_horizon == True or ignore_above_horizon == False)

        # string
        self.assertEqual("boldie", Config.getStringValue("player-name"))

        # HSVRange
        ball_label_range = Config.getHSVRangeValue("vision.pixel-labels.ball")
        self.assertTrue(ball_label_range.sMin < ball_label_range.sMax)
        self.assertTrue(ball_label_range.vMin < ball_label_range.vMax)

        # DoubleRange
        head_tilt_range = Config.getDoubleRangeValue("hardware.limits.angle-limits.head-tilt")
        self.assertTrue(head_tilt_range.min() < head_tilt_range.max())

    def test_getSettingWrongType(self):
        type_error_thrown = False
        try:
            Config.getDoubleValue("team-number")
        except TypeError as e:
            type_error_thrown = True
        self.assertTrue(type_error_thrown)

    def test_getSettingUnknown(self):
        key_error_thrown = False
        try:
            Config.getIntValue("foo.bar")
        except KeyError as e:
            key_error_thrown = True
        self.assertTrue(key_error_thrown)


class TestMotionScript(unittest.TestCase):

    def test_loading(self):
        motion_script = MotionScript.fromFile('../motionscripts/zero.json')

        self.assertEqual("zero", motion_script.getName())
        self.assertEqual(1, len(motion_script.getStages()))
