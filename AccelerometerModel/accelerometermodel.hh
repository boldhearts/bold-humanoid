// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include "config/Config/config.hh"
#include "Math/math.hh"
#include "src/util/Log/log.hh"

namespace bold
{
  /** Accelerometer model
   */
  class AccelerometerModel
  {
  public:
    /** Create an accelerometermodel from configuration
     */
    AccelerometerModel()
      : AccelerometerModel(config::Config::getStaticValue<double>("hardware.accelerometer.mgs-per-digit"),
                           config::Config::getStaticValue<double>("hardware.accelerometer.g-range"),
                           config::Config::getStaticValue<int>("hardware.accelerometer.digital-range"))
    {}

    AccelerometerModel(double mGs_per_digit, double gRange, unsigned digitalRange)
      : d_mGs_per_digit(mGs_per_digit),
        d_gRange(gRange),
        d_digitalRange(digitalRange),
        d_zeroRateLevel(digitalRange / 2)
    {}

    double valueToGs(uint16_t value) const;

  private:
    double d_mGs_per_digit;
    double d_gRange;
    int d_digitalRange;
    uint16_t d_zeroRateLevel;
  };

  inline double AccelerometerModel::valueToGs(uint16_t value) const
  {
    ASSERT(value < d_digitalRange);
    return Math::clamp((int{value} - d_zeroRateLevel) * d_mGs_per_digit / 1000,
                       -d_gRange, d_gRange);
  }

}
