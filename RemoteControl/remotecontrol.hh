// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <iosfwd>

class Joystick;

namespace bold
{
  namespace motion
  {
    namespace modules
    {
      class HeadModule;
      class MotionScriptModule;
      class WalkModule;
    }
  }
  
  class Agent;

  namespace config
  {
    template<typename> class Setting;
  }

  class RemoteControl
  {
  public:
    RemoteControl(Agent* agent);

    void update();

    std::shared_ptr<Joystick> getJoystick() const { return d_joystick; }

  private:
    void openJoystick();

    Agent* d_agent;
    std::shared_ptr<Joystick> d_joystick;
    std::shared_ptr<motion::modules::MotionScriptModule> d_motionScriptModule;
    std::shared_ptr<motion::modules::HeadModule> d_headModule;
    std::shared_ptr<motion::modules::WalkModule> d_walkModule;

    config::Setting<bool>* d_joystickEnabled;
    config::Setting<std::string>* d_joystickDevicePath;
    config::Setting<double>* d_joystickHeadSpeed;
    config::Setting<double>* d_joystickXAmpMax;
    config::Setting<double>* d_joystickYAmpMax;
    config::Setting<double>* d_joystickAAmpMax;
  };
}
