// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <map>
#include <memory>

#include "Clock/clock.hh"

namespace bold {
  class GameStateDecoder;

  class MessageCounter;

  class UDPSocket;

  class Voice;

  namespace state {
    class GameState;
  }

  namespace config {
    template<typename>
    class Setting;
  }

  /**
  * Listens for and integrates data sent according to the game controller protocol.
   *
   * This involves two types of message:
   *
   *  - GameState
   *  - RobotStatus
   *
   * In 'normal' usage, we listen for GameState and send RobotStatus, however it is possible to listen
   * for status messages from other team mates and potentially even the other team.
   */
  class GameStateReceiver {
  public:
    GameStateReceiver(std::shared_ptr<MessageCounter> messageCounter, std::shared_ptr<Voice> voice,
                      std::shared_ptr<Clock> clock);

    /** Add a decoder for received messages
     *
     * Multiple decoders can be added, to support different protocols. If multiple decoders for the same protocol
     * version are added, only the first one is used
     *
     * @param decoder Decoder to add
     */
    void addDecoder(std::unique_ptr<GameStateDecoder> decoder);

    /** Read and process all pending messages
     */
    void receive();

  private:
    void processGameState(std::shared_ptr<state::GameState const> gameState);

    std::map<uint8_t, std::unique_ptr<GameStateDecoder>> d_decoderByVersion;

    std::shared_ptr<MessageCounter> d_messageCounter;
    std::shared_ptr<UDPSocket> d_socket;
    std::shared_ptr<UDPSocket> d_socket_out;
    std::shared_ptr<Voice> d_voice;
    std::shared_ptr<Clock> d_clock;

    config::Setting<bool> *d_sendResponseMessages;
    uint16_t d_gameControllerPort;
    uint16_t d_gameControllerReturnPort;

    bool d_receivedGameStateRecently;
    Clock::Timestamp d_gameStateReceivedAt;
    uint8_t d_activeGameStateVersion;
    unsigned d_maxMessageSize;
  };
}
