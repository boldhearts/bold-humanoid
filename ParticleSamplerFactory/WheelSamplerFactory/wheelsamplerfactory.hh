// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iterator>
#include <vector>
#include <functional>

#include "Filter/ParticleFilter/particlefilter.hh"
#include "Math/math.hh"
#include "ParticleSamplerFactory/particlesamplerfactory.hh"

namespace bold
{
  template<int DIM>
  class WheelSamplerFactory : public ParticleSamplerFactory<DIM>
  {
  public:
    WheelSamplerFactory()
    {
      d_rnd = Math::createUniformRng(0, 1);
    }

    typename ParticleFilter<DIM>::ParticleSampler create(std::shared_ptr<std::vector<typename ParticleFilter<DIM>::Particle>> const& particles) override
    {
      d_index = d_rnd() * particles->size();
      d_beta = 0.0;

      d_maxWeight = std::max_element(
        particles->begin(), particles->end(),
        [](typename ParticleFilter<DIM>::Particle const& p1, typename ParticleFilter<DIM>::Particle const& p2) {
          return p1.second < p2.second;
        })->second;

      return [&] {
        d_beta += d_rnd() * 2 * d_maxWeight;
        double weight = (*particles)[d_index].second;
        while (d_beta > weight) {
          d_beta -= weight;
          d_index = (d_index + 1) % particles->size();
          weight = (*particles)[d_index].second;
        }
        return (*particles)[d_index];
      };
    }

  private:
    int d_index;
    double d_beta;
    double d_maxWeight;
    std::function<double()> d_rnd;
  };
}
