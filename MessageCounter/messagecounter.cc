// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "messagecounter.hh"

#include "state/State/state.hh"
#include "state/StateObject/MessageCountState/messagecountstate.hh"

using namespace bold;
using namespace bold::state;

void MessageCounter::updateStateObject()
{
  State::make<MessageCountState>(
    d_gameControllerMessageCount,
    d_ignoredMessageCount,
    d_sentTeamMessageCount,
    d_receivedTeamMessageCount,
    d_sentDrawbridgeMessageCount
  );

  // clear accumulators for next cycle
  d_gameControllerMessageCount = 0;
  d_ignoredMessageCount = 0;
  d_sentTeamMessageCount = 0;
  d_receivedTeamMessageCount = 0;
  d_sentDrawbridgeMessageCount = 0;
}
