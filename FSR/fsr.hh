// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>

namespace bold
{
  enum class FSRTable : uint8_t
  {
    MODEL_NUMBER_L         = 0,
    MODEL_NUMBER_H         = 1,
    VERSION                = 2,
    ID                     = 3,
    BAUD_RATE              = 4,
    RETURN_DELAY_TIME      = 5,
    RETURN_LEVEL           = 16,
    OPERATING_MODE         = 19,
    LED                    = 25,
    FSR1_L                 = 26,
    FSR1_H                 = 27,
    FSR2_L                 = 28,
    FSR2_H                 = 29,
    FSR3_L                 = 30,
    FSR3_H                 = 31,
    FSR4_L                 = 32,
    FSR4_H                 = 33,
    FSR_X                  = 34,
    FSR_Y                  = 35,
    PRESENT_VOLTAGE        = 42,
    REGISTERED_INSTRUCTION = 44,
    LOCK                   = 47,
    MAXNUM_ADDRESS
  };

  /** Helper functions and constants related to the FSR foot sensor. */
  class FSR
  {
  public:
    static constexpr uint8_t ID_FSR_L = 111;
    static constexpr uint8_t ID_FSR_R = 112;

    static constexpr uint8_t MAX_VALUE = 254;
  };
}
