// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkengine.ih"

void RobotisWalkEngine::reset()
{
  Log::info("WalkEngine::reset");

  d_xMoveTargetAmplitude = 0;
  d_yMoveTargetAmplitude = 0;
  d_aMoveTargetAmplitude = 0;
  d_hipPitchOffset = 13.0;

  d_bodySwingY = 0;
  d_bodySwingZ = 0;

  d_time = 0;
  updateTimeParams();
  updatePhase();
  updateMovementParams();

  // Copy the current pose to the output value array, in case the applyLegs etc
  // are called before a successful step completes.
  auto hw = State::get<HardwareState>();
  if (hw)
  {
    d_outValue[0]  = hw->getMX28State((uint8_t)JointId::R_HIP_YAW).presentPositionValue;
    d_outValue[1]  = hw->getMX28State((uint8_t)JointId::R_HIP_ROLL).presentPositionValue;
    d_outValue[2]  = hw->getMX28State((uint8_t)JointId::R_HIP_PITCH).presentPositionValue;
    d_outValue[3]  = hw->getMX28State((uint8_t)JointId::R_KNEE).presentPositionValue;
    d_outValue[4]  = hw->getMX28State((uint8_t)JointId::R_ANKLE_PITCH).presentPositionValue;
    d_outValue[5]  = hw->getMX28State((uint8_t)JointId::R_ANKLE_ROLL).presentPositionValue;
    d_outValue[6]  = hw->getMX28State((uint8_t)JointId::L_HIP_YAW).presentPositionValue;
    d_outValue[7]  = hw->getMX28State((uint8_t)JointId::L_HIP_ROLL).presentPositionValue;
    d_outValue[8]  = hw->getMX28State((uint8_t)JointId::L_HIP_PITCH).presentPositionValue;
    d_outValue[9]  = hw->getMX28State((uint8_t)JointId::L_KNEE).presentPositionValue;
    d_outValue[10] = hw->getMX28State((uint8_t)JointId::L_ANKLE_PITCH).presentPositionValue;
    d_outValue[11] = hw->getMX28State((uint8_t)JointId::L_ANKLE_ROLL).presentPositionValue;
    d_outValue[12] = hw->getMX28State((uint8_t)JointId::R_SHOULDER_PITCH).presentPositionValue;
    d_outValue[13] = hw->getMX28State((uint8_t)JointId::L_SHOULDER_PITCH).presentPositionValue;
  }
}
