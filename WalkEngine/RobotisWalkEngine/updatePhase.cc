// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkengine.ih"

void RobotisWalkEngine::updatePhase()
{
  bool canStop = false;

  if (d_time == 0)
  {
    updateTimeParams();
    d_phase = WalkPhase::LeftUp;
    canStop = true;
  }
  else if (fabs(d_time - d_phaseTime1) <= s_timeUnit/2)
  {
    updateMovementParams();
    d_phase = WalkPhase::LeftDown;
  }
  else if (fabs(d_time - d_phaseTime2) <= s_timeUnit/2)
  {
    updateTimeParams();
    d_time = d_phaseTime2;
    d_phase = WalkPhase::RightUp;
    canStop = true;
  }
  else if (fabs(d_time - d_phaseTime3) <= s_timeUnit/2)
  {
    updateMovementParams();
    d_phase = WalkPhase::RightDown;
  }

  d_canStopNow = canStop;
}
