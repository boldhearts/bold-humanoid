// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkengine.ih"

void RobotisWalkEngine::updateMovementParams()
{
  // Forward/Back
  d_xMoveAmplitude = d_xMoveTargetAmplitude;
  d_xMovePatternL.setAmplitude(d_xMoveAmplitude);
  d_xSwapPattern.setAmplitude(d_xMoveAmplitude * STEP_FB_RATIO->getValue());

  // Right/Left
  d_yMoveAmplitude = d_yMoveTargetAmplitude / 2;
  d_yMovePatternL.setAmplitude(d_yMoveAmplitude / 2);

  if (d_yMoveAmplitude > 0)
  {
    d_yMovePatternL.setAmplitudeShift(d_yMoveAmplitude);
    d_yMoveAmplitudeShift = d_yMoveAmplitude;
  }
  else
  {
    d_yMovePatternL.setAmplitudeShift(-d_yMoveAmplitude);
    d_yMoveAmplitudeShift = -d_yMoveAmplitude;
  }
  d_ySwapPattern.setAmplitude(Y_SWAP_AMPLITUDE->getValue() + d_yMoveAmplitudeShift * 0.04);

  d_zMoveAmplitude = Z_MOVE_AMPLITUDE->getValue() / 2;
  d_zMoveAmplitudeShift = d_zMoveAmplitude / 2;

  d_zMovePatternL.setAmplitude(d_zMoveAmplitude);
  d_zMovePatternL.setAmplitudeShift(d_zMoveAmplitudeShift);

  double zSwapAmplitude = Z_SWAP_AMPLITUDE->getValue();
  d_zSwapPattern.setAmplitude(zSwapAmplitude);
  d_zSwapPattern.setAmplitudeShift(zSwapAmplitude);

  // Direction
  d_aMoveAmplitude = Math::degToRad(d_aMoveTargetAmplitude) / 2.0;
  if (d_aMoveAmplitude > 0)
    d_aMoveAmplitudeShift = d_aMoveAmplitude;
  else
    d_aMoveAmplitudeShift = -d_aMoveAmplitude;
}
