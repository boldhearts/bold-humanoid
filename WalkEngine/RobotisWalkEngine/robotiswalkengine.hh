// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string.h>
#include <cmath>
#include <memory>
#include <array>
#include <Eigen/Core>

#include "WalkEngine/walkengine.hh"
#include "PatternGenerator/SineGenerator/sinegenerator.hh"
#include "PatternGenerator/ClampedSquashedPatternGenerator/clampedsquashedpatterngenerator.hh"

namespace bold
{
  namespace config
  {
    template <typename> class Setting;
  }

  using ClampedSquashedSineGenerator = ClampedSquashedPatternGenerator<SineGenerator>;

  /** Computes body angles for walking.
   */
  class RobotisWalkEngine : public WalkEngine
  {
  public:
    enum class WalkPhase
    {
      LeftUp = 0,
      LeftDown = 1,
      RightUp = 2,
      RightDown = 3
    };

    /** Initialise a new WalkEngine. */
    RobotisWalkEngine();

    void reset() override;

    void step() override;

    void balance(double ratio);

    void applyHead(motion::core::HeadSectionControl& head) override;
    void applyArms(motion::core::ArmSectionControl& arms) override;
    void applyLegs(motion::core::LegSectionControl& legs) override;

    bool canStopNow() const override;

    WalkPhase getCurrentPhase() const { return d_phase; }
    double getBodySwingY() const { return d_bodySwingY; }
    double getBodySwingZ() const { return d_bodySwingZ; }

  private:
    static constexpr unsigned s_timeUnit = 8; //msec

//    static constexpr double THIGH_LENGTH = 93.0; // mm
//    static constexpr double CALF_LENGTH = 93.0; // mm
//    static constexpr double ANKLE_LENGTH = 33.5; // mm
//    static constexpr double LEG_LENGTH = THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH;

    double THIGH_LENGTH;
    double CALF_LENGTH;
    double ANKLE_LENGTH;
    double LEG_LENGTH;

    static double wsin(double time, double period, double periodShift, double mag, double magShift);

    void updatePhase();
    void updateTimeParams();
    void updateMovementParams();
    void updateStandardOffsets();

    double d_periodTime;
    /// Double support phase ratio.
    double d_dspRatio;
    /// Single support phase ratio.
    double d_sspRatio;
    double d_xSwapPeriodTime;
    double d_xMovePeriodTime;
    double d_ySwapPeriodTime;
    double d_yMovePeriodTime;
    double d_zSwapPeriodTime;
    double d_zMovePeriodTime;
    double d_aMovePeriodTime;
    double d_sspTimeStartL;
    double d_sspTimeEndL;
    double d_sspTimeStartR;
    double d_sspTimeEndR;
    double d_phaseTime1;
    double d_phaseTime2;
    double d_phaseTime3;

    Eigen::Vector3d d_linOffsets;
    Eigen::Vector3d d_rotOffsets;

    SineGenerator d_xSwapPattern;
    SineGenerator d_ySwapPattern;
    SineGenerator d_zSwapPattern;

    ClampedSquashedSineGenerator d_xMovePatternL;
    ClampedSquashedSineGenerator d_yMovePatternL;
    ClampedSquashedSineGenerator d_zMovePatternL;

    ClampedSquashedSineGenerator d_xMovePatternR;
    ClampedSquashedSineGenerator d_yMovePatternR;
    ClampedSquashedSineGenerator d_zMovePatternR;

    SineGenerator d_lArmPattern;
    SineGenerator d_rArmPattern;

    static constexpr double d_xMovePhaseShift = M_PI / 2;
    static constexpr double d_xMoveAmplitudeShift = 0;
    static constexpr double d_yMovePhaseShift = M_PI / 2;
    static constexpr double d_zMovePhaseShift = M_PI / 2;
    static constexpr double d_aMovePhaseShift = M_PI / 2;

    double d_xMoveAmplitude;
    double d_yMoveAmplitude;
    double d_yMoveAmplitudeShift;
    double d_zMoveAmplitude;
    double d_zMoveAmplitudeShift;
    double d_aMoveAmplitude;
    double d_aMoveAmplitudeShift;

    double d_pelvisOffset;
    double d_pelvisSwing;
    double d_armSwingGain;

    double d_time;

    WalkPhase d_phase;
    double d_bodySwingY;
    double d_bodySwingZ;

    bool d_canStopNow;

    std::array<int,14> d_outValue;

    // TODO divide these settings up by path

    // balance params
    config::Setting<double>* X_OFFSET;
    config::Setting<double>* Y_OFFSET;
    config::Setting<double>* Z_OFFSET;
    config::Setting<double>* YAW_OFFSET;
    config::Setting<double>* PITCH_OFFSET;
    config::Setting<double>* ROLL_OFFSET;

    // time params
    config::Setting<double>* PERIOD_TIME;
    config::Setting<double>* DSP_RATIO;
    config::Setting<double>* STEP_FB_RATIO;

    // movement params
    config::Setting<double>* Z_MOVE_AMPLITUDE;
    config::Setting<double>* Y_SWAP_AMPLITUDE;
    config::Setting<double>* Z_SWAP_AMPLITUDE;
    config::Setting<double>* ARM_SWING_GAIN;
    config::Setting<double>* PELVIS_OFFSET;

    // motor gains
    config::Setting<int>* d_legGainP;
    config::Setting<int>* d_legGainI;
    config::Setting<int>* d_legGainD;
    config::Setting<int>* d_armGainP;
    config::Setting<int>* d_armGainI;
    config::Setting<int>* d_armGainD;
    config::Setting<int>* d_headGainP;
    config::Setting<int>* d_headGainI;
    config::Setting<int>* d_headGainD;

    // robot model type 
    std::string d_model;
  };
}
