// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkengine.ih"

void RobotisWalkEngine::updateTimeParams()
{
  d_periodTime = PERIOD_TIME->getValue();
  d_dspRatio = DSP_RATIO->getValue();
  d_sspRatio = 1 - d_dspRatio;

  d_xSwapPattern.setPeriod(d_periodTime / 2.0);
  d_ySwapPattern.setPeriod(d_periodTime);
  d_zSwapPattern.setPeriod(d_periodTime / 2.0);

  d_xMovePeriodTime = d_periodTime * d_sspRatio;
  d_yMovePeriodTime = d_periodTime * d_sspRatio;
  d_zMovePeriodTime = d_periodTime * d_sspRatio / 2;
  d_aMovePeriodTime = d_periodTime * d_sspRatio;

  d_xMovePatternL.setPeriod(d_periodTime);
  d_yMovePatternL.setPeriod(d_periodTime);
  d_zMovePatternL.setPeriod(d_periodTime / 2);

  d_xMovePatternL.setPatternRatio(d_sspRatio, true);
  d_yMovePatternL.setPatternRatio(d_sspRatio, true);
  d_zMovePatternL.setPatternRatio(d_sspRatio, false);

  d_xMovePatternR.setPeriod(d_periodTime);
  d_yMovePatternR.setPeriod(d_periodTime);
  d_zMovePatternR.setPeriod(d_periodTime / 2);

  d_xMovePatternR.setPatternRatio(d_sspRatio, true);
  d_yMovePatternR.setPatternRatio(d_sspRatio, true);
  d_zMovePatternR.setPatternRatio(d_sspRatio, false);

  /*
    |----|--------|--------|----|----|--------|--------|----|
    0    a       1T        b   2T   c        3T        d   4T

    T = period / 4
    a = ssp start L
    b = ssp end L
    c = ssp start R
    d = ssp end R
   */
  d_sspTimeStartL = (1 - d_sspRatio) * d_periodTime / 4;
  d_sspTimeEndL   = (1 + d_sspRatio) * d_periodTime / 4;
  d_sspTimeStartR = (3 - d_sspRatio) * d_periodTime / 4;
  d_sspTimeEndR   = (3 + d_sspRatio) * d_periodTime / 4;

  d_phaseTime1 = (d_sspTimeEndL + d_sspTimeStartL) / 2; // = 1T
  d_phaseTime2 = (d_sspTimeStartR + d_sspTimeEndL) / 2; // = 2T
  d_phaseTime3 = (d_sspTimeEndR + d_sspTimeStartR) / 2; // = 3T

  d_pelvisOffset = PELVIS_OFFSET->getValue()*MX28::RATIO_DEGS2VALUE;
  d_pelvisSwing = d_pelvisOffset * 0.35;
  d_armSwingGain = ARM_SWING_GAIN->getValue();

  d_lArmPattern.setPeriod(d_periodTime);
  d_lArmPattern.setAmplitude(d_xMoveAmplitude * d_armSwingGain);

  d_rArmPattern.setPeriod(d_periodTime);
  d_rArmPattern.setAmplitude(-d_xMoveAmplitude * d_armSwingGain);
}
