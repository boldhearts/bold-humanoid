// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "robotiswalkengine.ih"

RobotisWalkEngine::RobotisWalkEngine()
  : d_canStopNow{true}
{
  X_OFFSET          = Config::getSetting<double>("walk-engine.params.x-offset");
  Y_OFFSET          = Config::getSetting<double>("walk-engine.params.y-offset");
  Z_OFFSET          = Config::getSetting<double>("walk-engine.params.z-offset");
  ROLL_OFFSET       = Config::getSetting<double>("walk-engine.params.roll-offset");
  PITCH_OFFSET      = Config::getSetting<double>("walk-engine.params.pitch-offset");
  YAW_OFFSET        = Config::getSetting<double>("walk-engine.params.yaw-offset");
  PERIOD_TIME       = Config::getSetting<double>("walk-engine.params.period-time");
  DSP_RATIO         = Config::getSetting<double>("walk-engine.params.dsp-ratio");
  STEP_FB_RATIO     = Config::getSetting<double>("walk-engine.params.step-fb-ratio");
  Z_MOVE_AMPLITUDE  = Config::getSetting<double>("walk-engine.params.foot-height");
  Y_SWAP_AMPLITUDE  = Config::getSetting<double>("walk-engine.params.swing-right-left");
  Z_SWAP_AMPLITUDE  = Config::getSetting<double>("walk-engine.params.swing-top-down");
  PELVIS_OFFSET     = Config::getSetting<double>("walk-engine.params.pelvis-offset");
  ARM_SWING_GAIN    = Config::getSetting<double>("walk-engine.params.arm-swing-gain");

  d_legGainP = Config::getSetting<int>("walk-engine.gains.leg-p-gain");
  d_legGainI = Config::getSetting<int>("walk-engine.gains.leg-i-gain");
  d_legGainD = Config::getSetting<int>("walk-engine.gains.leg-d-gain");
  d_armGainP = Config::getSetting<int>("walk-engine.gains.arm-p-gain");
  d_armGainI = Config::getSetting<int>("walk-engine.gains.arm-i-gain");
  d_armGainD = Config::getSetting<int>("walk-engine.gains.arm-d-gain");
  d_headGainP = Config::getSetting<int>("walk-engine.gains.head-p-gain");
  d_headGainI = Config::getSetting<int>("walk-engine.gains.head-i-gain");
  d_headGainD = Config::getSetting<int>("walk-engine.gains.head-d-gain");

  d_model = Config::getStaticValue<string>("model"); //Config::getSetting<string>("model");

  if (d_model=="boldhearts")
  {
    THIGH_LENGTH = 143.0; //mm
    CALF_LENGTH = 143.0; //mm
    ANKLE_LENGTH = 33.5; //mm
  }else{
    THIGH_LENGTH = 93.0; //mm
    CALF_LENGTH = 93.0; //mm
    ANKLE_LENGTH = 33.5; //mm 
  }
  LEG_LENGTH = THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH;

  d_xSwapPattern.setPeriodShift(M_PI);
  d_xSwapPattern.setAmplitudeShift(0.0);

  d_ySwapPattern.setPeriodShift(0.0);
  d_ySwapPattern.setAmplitudeShift(0.0);

  d_zSwapPattern.setPeriodShift(M_PI * 3.0 / 2.0);

  d_lArmPattern.setPeriodShift(M_PI * 1.5);
  d_rArmPattern.setPeriodShift(M_PI * 1.5);

  d_xMovePatternL.setPeriodShift(M_PI / 2);
  d_yMovePatternL.setPeriodShift(M_PI / 2);
  d_zMovePatternL.setPeriodShift(M_PI / 2);

  d_xMovePatternR.setPeriodShift(M_PI / 2);
  d_yMovePatternR.setPeriodShift(M_PI / 2);
  d_zMovePatternR.setPeriodShift(M_PI / 2);

  reset();
}

//constexpr double RobotisWalkEngine::THIGH_LENGTH;
//constexpr double RobotisWalkEngine::CALF_LENGTH;
//constexpr double RobotisWalkEngine::ANKLE_LENGTH;
//constexpr double RobotisWalkEngine::LEG_LENGTH;

double RobotisWalkEngine::wsin(double time, double period, double period_shift, double mag, double mag_shift)
{
  return mag * sin(2 * M_PI / period * time - period_shift) + mag_shift;
}

bool RobotisWalkEngine::canStopNow() const
{
  return d_canStopNow;
}

void RobotisWalkEngine::applyHead(HeadSectionControl& head)
{
  // Ensure we have our standard PID values
  head.visitJoints([this](JointControl& joint)
                   {
                     joint.setPidGains((uint8_t)d_headGainP->getValue(),
                                       (uint8_t)d_headGainI->getValue(),
                                       (uint8_t)d_headGainD->getValue());
                   });

  // Head pan follows turn, if any
  head.pan().setDegrees(d_aMoveTargetAmplitude);
}

void RobotisWalkEngine::applyArms(ArmSectionControl& arms)
{
  // Arms move with a low P value of 8
  arms.visitJoints([this](JointControl& joint)
                    {
                      joint.setPidGains((uint8_t)d_armGainP->getValue(),
                                        (uint8_t)d_armGainI->getValue(),
                                        (uint8_t)d_armGainD->getValue());
                    });

  arms.shoulderPitchRight().setValue(MX28::clampValue(d_outValue[12]));
  arms.shoulderPitchLeft().setValue(MX28::clampValue(d_outValue[13]));

  arms.shoulderRollRight().setDegrees(-17);
  arms.shoulderRollLeft().setDegrees(17);
  arms.elbowRight().setDegrees(29);
  arms.elbowLeft().setDegrees(-29);
}

void RobotisWalkEngine::applyLegs(LegSectionControl& legs)
{
  // Ensure we have our standard PID values
  legs.visitJoints([this](JointControl& joint)
                   {
                     joint.setPidGains((uint8_t)d_legGainP->getValue(),
                                       (uint8_t)d_legGainI->getValue(),
                                       (uint8_t)d_legGainD->getValue());
                   });

  legs.hipYawRight().setValue(MX28::clampValue(d_outValue[0]));
  legs.hipRollRight().setValue(MX28::clampValue(d_outValue[1]));
  legs.hipPitchRight().setValue(MX28::clampValue(d_outValue[2]));
  legs.kneeRight().setValue(MX28::clampValue(d_outValue[3]));
  legs.anklePitchRight().setValue(MX28::clampValue(d_outValue[4]));
  legs.ankleRollRight().setValue(MX28::clampValue(d_outValue[5]));
  legs.hipYawLeft().setValue(MX28::clampValue(d_outValue[6]));
  legs.hipRollLeft().setValue(MX28::clampValue(d_outValue[7]));
  legs.hipPitchLeft().setValue(MX28::clampValue(d_outValue[8]));
  legs.kneeLeft().setValue(MX28::clampValue(d_outValue[9]));
  legs.anklePitchLeft().setValue(MX28::clampValue(d_outValue[10]));
  legs.ankleRollLeft().setValue(MX28::clampValue(d_outValue[11]));
}
