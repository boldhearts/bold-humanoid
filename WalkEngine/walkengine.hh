// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/core/PoseProvider/poseprovider.hh"

namespace bold
{
  class WalkEngine : public motion::core::PoseProvider
  {
  public:
    virtual ~WalkEngine() {}

    virtual void reset() = 0;

    virtual void step() = 0;

    /** Whether the body would be stable if walking would cease now.
     *
     * Only true twice each period.
     */
    virtual bool canStopNow() const = 0;

    double getXMoveAmplitude() const { return d_xMoveTargetAmplitude; }
    void setXMoveAmplitude(double amp) { d_xMoveTargetAmplitude = amp; }

    double getYMoveAmplitude() const { return d_yMoveTargetAmplitude; }
    void setYMoveAmplitude(double amp) { d_yMoveTargetAmplitude = amp; }

    double getAMoveAmplitude() const { return d_aMoveTargetAmplitude; }
    void setAMoveAmplitude(double amp) { d_aMoveTargetAmplitude = amp; }

    double getHipPitchOffset() const { return d_hipPitchOffset; }
    void setHipPitchOffset(double offset) { d_hipPitchOffset = offset; }

  protected:

    double d_xMoveTargetAmplitude;
    double d_yMoveTargetAmplitude;
    double d_aMoveTargetAmplitude;

    double d_hipPitchOffset;
  };
}
