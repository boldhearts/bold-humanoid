// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <opencv2/core/core.hpp>

namespace bold
{
  namespace geometry2
  {
    template<typename T, int DIM>
    class LineSegment;

    using LineSegment2i = LineSegment<int, 2>;
  }

  class Painter
  {
  public:
    static void draw(geometry2::LineSegment2i const& segment, cv::Mat& img, cv::Scalar const& colour, unsigned thickness);
  };
}
