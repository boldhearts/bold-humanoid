// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "motion/scripts/MotionScript/motionscript.hh"
#include "motion/core/PoseProvider/poseprovider.hh"

#include <memory>

namespace bold
{
  namespace motion
  {
    namespace core
    {
      class BodySectionControl;
      class JointSelection;
    }
  }

  class MotionScriptRunner : public motion::core::PoseProvider
  {
  public:
    enum class Status
    {
      Pending,
      Running,
      Finished
    };

    static std::string getStatusName(Status const& state);

    static int getMaxDeltaFromFinalPose(std::shared_ptr<motion::scripts::MotionScript const> const& script,
                                        bool includeHead = false,
                                        bool includeArms = false);
    
    static bool isInFinalPose(std::shared_ptr<motion::scripts::MotionScript const> const& script,
                              bool includeHead = false, bool includeArms = false,
                              unsigned valueTolerance = 100);

    MotionScriptRunner(std::shared_ptr<motion::scripts::MotionScript const> const& script);

    std::shared_ptr<motion::scripts::MotionScript const> getScript() const { return d_script; }

    Status getStatus() const { return d_state; }

    bool step(motion::core::JointSelection const& selectedJoints);

    void applyHead(motion::core::HeadSectionControl& head) override;
    void applyArms(motion::core::ArmSectionControl& arms) override;
    void applyLegs(motion::core::LegSectionControl& legs) override;

    unsigned getValue(uint8_t jointId) const { return d_values[jointId]; }
    uint8_t getPGain(uint8_t jointId) const { return d_pGains[jointId]; }

    unsigned getCurrentStageIndex() const { return d_currentStageIndex; }
    int getCurrentKeyFrameIndex() const { return d_currentKeyFrameIndex; }

  private:
    void applySection(motion::core::BodySectionControl& section) const;

    bool progressToNextSection(motion::core::JointSelection const& selectedJoints);
    void continueCurrentSection(motion::core::JointSelection const& selectedJoints);
    bool startKeyFrame(motion::core::JointSelection const& selectedJoints);

    enum class Section : uint8_t { PRE = 1, MAIN = 2, POST = 3, PAUSE = 4 };
    enum class FinishSpeed : uint8_t { ZERO = 1, NON_ZERO = 2 };

    std::shared_ptr<motion::scripts::MotionScript const> d_script;

    std::shared_ptr<motion::scripts::MotionScript::Stage const> d_currentStage;

    unsigned d_currentStageIndex;
    /// Zero-based index of the current keyframe; can be set to -1 at start of stage
    int d_currentKeyFrameIndex;
    /// The number of times that the current stage must be replayed, including the current execution
    unsigned d_repeatCurrentStageCount;

    // TODO can this be replaced by d_state in Finished? or new Finishing value?
    bool d_isPlayingFinished;

    uint8_t d_pGains[21];
    uint16_t d_values[21];

    static const int JOINT_ARRAY_LENGTH = 22;

    /// Position values as at start of current keyframe
    uint16_t d_sectionStartAngles[JOINT_ARRAY_LENGTH];

    /// Target position values for the end of the current keyframe
    uint16_t d_keyFrameTargetAngles[JOINT_ARRAY_LENGTH];

    /// The total delta in position value for the current keyframe (keyframe target - keyframe start angle)
    short d_keyFrameDeltaValue[JOINT_ARRAY_LENGTH];
    short d_mainAngles1024[JOINT_ARRAY_LENGTH];
    short d_accelAngles1024[JOINT_ARRAY_LENGTH];

    short d_mainSpeeds1024[JOINT_ARRAY_LENGTH];
    /// The goal speed value at the start of the section
    short d_sectionStartGoalSpeeds[JOINT_ARRAY_LENGTH];
    short d_goalSpeeds[JOINT_ARRAY_LENGTH];

    /// Whether a joint's speed must be zero at the end of the keyframe or not
    FinishSpeed d_finishSpeeds[JOINT_ARRAY_LENGTH];

    uint16_t d_sectionStepIndex;
    uint16_t d_sectionStepCount;

    /// The number of steps to pause motion for at the end of the keyframe (zero denotes no pause)
    uint16_t d_keyFramePauseStepCount;

    /// The number of steps motion occurs for within this keyframe, excluding pause steps
    uint16_t d_keyFrameMotionStepCount;

    /// The length of the acceleration/deceleration periods in the current keyframe
    uint16_t d_accelStepCount;

    /// The current section within the current keyframe
    Section d_section;

    Status d_state;
  };
}
