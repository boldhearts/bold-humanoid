// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "src/colour/colour.hh"
#include "src/colour/BGR/bgr.hh"
#include "src/geometry2/Geometry/LineSegment/linesegment.hh"
#include "src/geometry2/Geometry/MultiPoint/Polygon/polygon.hh"

#include <memory>
#include <utility>
#include <vector>
#include <Eigen/Core>

namespace bold
{
  enum class DrawingItemType
  {
    Line = 1,
    Circle = 2,
    Polygon = 3,
  };

  enum class Frame
  {
    Camera = 1,
    Agent = 2,
    World = 3
  };

  struct DrawingItem
  {
    Frame frame;
    DrawingItemType type;
  };

  struct LineDrawing : public DrawingItem
  {
    LineDrawing() : DrawingItem{}
    {
      type = DrawingItemType::Line;
    }

    geometry2::LineSegment2d line;
    colour::BGR colour{0, 0, 0};
    double lineWidth{0.0};
    double alpha{1.0};

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  struct CircleDrawing : public DrawingItem
  {
    CircleDrawing() : DrawingItem{} {
      type = DrawingItemType::Circle;
    }

    geometry2::Point2d centre;
    double radius{0.0};
    colour::BGR fillColour{0, 0, 0};
    colour::BGR strokeColour{0, 0, 0};
    double fillAlpha{0.0};
    double strokeAlpha{1.0};
    double lineWidth{1.0};

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  struct PolygonDrawing : public DrawingItem
  {
    explicit PolygonDrawing(geometry2::Polygon2d polygon)
      : DrawingItem{},
        polygon(std::move(polygon))
    {
      type = DrawingItemType::Polygon;
    }

    geometry2::Polygon2d polygon;
    colour::BGR fillColour{0, 0, 0};
    colour::BGR strokeColour{0, 0, 0};
    double fillAlpha{0.0};
    double strokeAlpha{1.0};
    double lineWidth{1.0};

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

  class Draw
  {
  public:
    using ItemVector = std::vector<std::unique_ptr<DrawingItem>>;
    
    void circle(Frame frame,
                geometry2::Point2d const& centre, double radius,
                colour::BGR const& colour, double alpha = 1.0,
                double lineWidth = 1.0);
    
    void fillCircle(Frame frame,
                    geometry2::Point2d const& centre, double radius,
                    colour::BGR const& fillColour, double fillAlpha,
                    colour::BGR const& strokeColour, double strokeAlpha,
                    double lineWidth = 1.0);

    void polygon(Frame frame,
                 geometry2::Polygon2d const& polygon,
                 colour::BGR const& strokeColour, double strokeAlpha,
                 double lineWidth = 1.0);

    void fillPolygon(Frame frame,
                     geometry2::Polygon2d const& polygon,
                     colour::BGR const& fillColour, double fillAlpha,
                     colour::BGR const& strokeColour, double strokeAlpha,
                     double lineWidth = 1.0);

    void line(Frame frame,
              geometry2::LineSegment2d const& line,
              colour::BGR const& colour, double alpha = 1.0,
              double lineWidth = 1.0);
    
    void lineAtAngle(Frame frame,
                     geometry2::Point2d const& p1, double angle, double length,
                     colour::BGR const& colour, double alpha = 1.0,
                     double lineWidth = 1.0);

    ItemVector const& getDrawingItems() const { return d_drawingItems; }
    
    void flushToStateObject();

  private:
    ItemVector d_drawingItems;
  };
}
