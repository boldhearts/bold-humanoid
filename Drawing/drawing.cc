// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "drawing.hh"

#include "state/State/state.hh"
#include "state/StateObject/DrawingState/drawingstate.hh"

using namespace bold;
using namespace bold::colour;
using namespace bold::geometry2;
using namespace bold::state;
using namespace Eigen;
using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Draw::line(Frame frame,
                LineSegment2d const& line,
                BGR const& colour, double alpha,
                double lineWidth)
{
  auto lineDrawing = make_unique<LineDrawing>();

  lineDrawing->type = DrawingItemType::Line;
  lineDrawing->frame = frame;
  lineDrawing->line = line;
  lineDrawing->colour = colour;
  lineDrawing->alpha = alpha;
  lineDrawing->lineWidth = lineWidth;

  d_drawingItems.push_back(move(lineDrawing));
}

void Draw::lineAtAngle(Frame frame,
                       Point2d const& p1, double angle, double length,
                       BGR const& colour, double alpha, double lineWidth)
{
  Point2d p2 = p1;
  p2.x() += cos(angle) * length;
  p2.y() += sin(angle) * length;

  line(frame, LineSegment2d{p1, p2}, colour, alpha, lineWidth);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Draw::circle(Frame frame, Point2d const& centre, double radius, BGR const& strokeColour, double strokeAlpha, double lineWidth)
{
  fillCircle(frame, centre, radius, BGR::black(), 0.0, strokeColour, strokeAlpha, lineWidth);
}

void Draw::fillCircle(Frame frame, Point2d const& centre, double radius, BGR const& fillColour, double fillAlpha, BGR const& strokeColour, double strokeAlpha, double lineWidth)
{
  auto circle = make_unique<CircleDrawing>();

  circle->type = DrawingItemType::Circle;
  circle->frame = frame;
  circle->centre = centre;
  circle->radius = radius;
  circle->strokeColour = strokeColour;
  circle->strokeAlpha = strokeAlpha;
  circle->fillColour = fillColour;
  circle->fillAlpha = fillAlpha;
  circle->lineWidth = lineWidth;

  d_drawingItems.emplace_back(move(circle));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Draw::polygon(Frame frame, Polygon2d const& polygon, colour::BGR const& strokeColour, double strokeAlpha, double lineWidth)
{
  fillPolygon(frame, polygon, BGR::black(), 0.0, strokeColour, strokeAlpha, lineWidth);
}

void Draw::fillPolygon(Frame frame, Polygon2d const& polygon, BGR const& fillColour, double fillAlpha, BGR const& strokeColour, double strokeAlpha, double lineWidth)
{
  auto poly = make_unique<PolygonDrawing>(polygon);

  poly->type = DrawingItemType::Polygon;
  poly->frame = frame;
  poly->fillColour = fillColour;
  poly->strokeColour = strokeColour;
  poly->fillAlpha = fillAlpha;
  poly->strokeAlpha = strokeAlpha;
  poly->lineWidth = lineWidth;

  d_drawingItems.push_back(move(poly));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Draw::flushToStateObject()
{
  State::make<DrawingState>(std::move(d_drawingItems));
  d_drawingItems.clear();
}
