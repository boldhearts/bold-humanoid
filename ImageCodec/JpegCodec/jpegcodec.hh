// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ImageCodec/imagecodec.hh"
#include "src/util/assert.hh"
#include "src/colour/colour.hh"

#include <opencv2/core/core.hpp>

struct jpeg_compress_struct;
struct jpeg_common_struct;

namespace bold
{
  class JpegCodec : public ImageCodec
  {
  public:
    JpegCodec();

    bool encode(cv::Mat const& image, std::vector<unsigned char>& buffer);

    void setQualityLevel(int quality) { ASSERT(quality >= 0 && quality <= 100); d_quality = quality; }

  private:
    static constexpr unsigned BufferSize = 4096 * 5;

    static void onError(jpeg_common_struct* cinfo);
    static void growBuffer(jpeg_compress_struct* cinfo);

    std::vector<unsigned char*> rowPointers;
    int d_quality;
  };
}
