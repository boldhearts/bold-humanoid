// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ImageCodec/imagecodec.hh"
#include "util/assert.hh"
#include "colour/BGR/bgr.hh"
#include <opencv2/core/core.hpp>
#include <map>

struct png_struct_def;

namespace bold
{
  namespace colour { struct bgr; }

  enum class CompressionStrategy
  {
    Filtered = 1,
    HuffmanOnly = 2,
    RLE = 3,
    Fixed = 4
  };

  class PngCodec : public ImageCodec
  {
  public:
    PngCodec(bool useBGR = true, bool allowAlpha = false);

    bool encode(cv::Mat const& image, std::vector<unsigned char>& buffer, std::map<uint8_t, colour::BGR> const* palette);

    void setCompressionLevel(int level) { ASSERT(level >= 0 && level <= 9); d_compressionLevel = level; }
    void setCompressionStrategy(CompressionStrategy strategy) { d_compressionStrategy = strategy; }

    void setFilterSub(bool enabled) { d_filterSub = enabled; }
    void setFilterUp(bool enabled) { d_filterUp = enabled; }
    void setFilterAvg(bool enabled) { d_filterAvg = enabled; }
    void setFilterPaeth(bool enabled) { d_filterPaeth = enabled; }

    bool read(std::string filePath, cv::Mat& img);

  private:
    static void writeDataToBuf(png_struct_def* png_ptr, uint8_t* src, size_t size);
    static void onError(png_struct_def* png_ptr, const char* message);
    static void onWarning(png_struct_def* png_ptr, const char* message);

    std::vector<unsigned char*> rowPointers;

    bool d_useBGR;
    bool d_allowAlpha;
    
    int d_compressionLevel;
    CompressionStrategy d_compressionStrategy;
    bool d_filterSub;
    bool d_filterUp;
    bool d_filterAvg;
    bool d_filterPaeth;
  };
}
