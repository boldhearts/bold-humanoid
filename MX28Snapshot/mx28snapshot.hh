// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "MX28Alarm/mx28alarm.hh"

namespace bold
{
  class BulkReadTable;

  // http://support.robotis.com/en/techsupport_eng.htm#product/dynamixel/mx_series/mx-28.htm

  // TODO rename as MX28State

  /** Models frequently changing data table entries from an MX28 dynamixel device.
   */
  class MX28Snapshot
  {
  public:
    /// The ID of this MX28 dynamixel device.
    uint8_t id;

    /// The present angle, in radians.
    double presentPosition;
    /// The present angle, in encoder units.
    uint16_t presentPositionValue;
    /// The present speed, in revolutions per minute.
    double presentSpeedRPM;
    /// The present load.
    double presentLoad;
    /// The present voltage.
    double presentVoltage;
    /// The present temperature, in Celsius.
    uint8_t presentTemp;

    MX28Snapshot(uint8_t mx28ID) : id(mx28ID) {}

    MX28Snapshot(uint8_t mx28ID, BulkReadTable const& data, int jointOffset);
  };

  /** Models infrequently changing data table entries from an MX28 dynamixel device.
   */
  class StaticMX28State
  {
  public:
    unsigned short modelNumber;
    uint8_t firmwareVersion;
    uint8_t id;
    unsigned int baudBPS;
    unsigned int returnDelayTimeMicroSeconds;
    double angleLimitCW;
    double angleLimitCCW;
    uint8_t tempLimitHighCelsius;
    double voltageLimitLow;
    double voltageLimitHigh;
    unsigned short maxTorque;

    /**
    * Controls when a status packet is returned.
    *
    * 0 - only for PING command
    * 1 - only for READ command
    * 2 - for all commands
    *
    * Never returned if instruction is a broadcast packet.
    */
    uint8_t statusRetLevel;

    /// Specifies conditions for the alarm LED to be turned on
    MX28Alarm alarmLed;
    /// Specifies conditions for the device to shut down
    MX28Alarm alarmShutdown;

    // RAM AREA

    bool torqueEnable;
//     bool led;
//     double gainP;
//     double gainI;
//     double gainD;
//     double goalPositionRads;
//     double movingSpeedRPM;
//     double torqueLimit;
//     bool isInstructionRegistered;
//     bool isMoving;
    bool isEepromLocked;
//  uint8_t punch; // apparently this value is unused
//  double goalAcceleration; // TODO introduce this from the read

    StaticMX28State(uint8_t mx28ID, BulkReadTable const& data);

    StaticMX28State(uint8_t mx28ID) : id(mx28ID) {}
  };
}
