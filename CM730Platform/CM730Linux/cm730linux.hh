// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "CM730Platform/cm730platform.hh"

#include <memory>

namespace bold
{
  class Clock;

  class CM730Linux : public CM730Platform
  {
  public:
    CM730Linux(std::string name, std::shared_ptr<Clock> clock);
    ~CM730Linux();

    void setPortName(std::string name) { d_portName = name; }
    std::string getPortName() const { return d_portName; }

    unsigned long getReceivedByteCount() const override { return d_rxByteCount; }
    unsigned long getTransmittedByteCount() const override { return d_txByteCount; }
    void resetByteCounts() override { d_rxByteCount = d_txByteCount = 0; }

    bool openPort() override;
    bool setBaud(unsigned baud) override;
    bool closePort() override;
    bool clearPort() override;
    bool isPortOpen() const override;
    int writePort(uint8_t const* packet, std::size_t numPacket) override;
    int readPort(uint8_t* packet, std::size_t numPacket) override;

    void setPacketTimeout(std::size_t lenPacket) override;
    bool isPacketTimeout() override;
    double getPacketTime() override;
    double getPacketTimeoutMillis() const override;

    void sleep(double msec) override;

  private:
    std::shared_ptr<Clock> d_clock;

    /// The FD for the socket connected to the CM730
    int d_socket;

    /// Timestamp of when we start waiting for a response (status) packet, in millis
    double d_packetStartTimeMillis;

    /// Amount of time to wait for a packet, relative to d_PacketStartTime, in millis
    double d_packetWaitTimeMillis;

    /// The estimated amount of time required to send a single byte, in millis. Calculated from baud rate.
    double d_byteTransferTimeMillis;

    /// A string that defines the port of the CM730
    std::string d_portName;

    unsigned long d_txByteCount;
    unsigned long d_rxByteCount;
  };
}
