// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>

namespace bold
{
  class LinearSmoother;

  namespace config
  {
    template<typename> class Setting;
  }

  namespace motion
  {
    namespace modules
    {
      enum class WalkStatus : uint8_t;
    }
  }

  /** Calculates torso attitude during walking to maintain stability. */
  class WalkAttitude
  {
  public:
    WalkAttitude();

    void update(motion::modules::WalkStatus walkStatus, LinearSmoother const& xAmp, LinearSmoother const& yAmp, LinearSmoother const& turnAmp);

    void reset();

    double getHipPitch() const { return d_hipPitch; }

  private:
    double d_hipPitch;

    // hip pitch calculation parameters
    config::Setting<double>* d_stableHipPitch;
    config::Setting<double>* d_maxFwdHipPitch;
    config::Setting<double>* d_maxBwdHipPitch;
    config::Setting<double>* d_maxHipPitchAtFwdSpeed;
    config::Setting<double>* d_maxHipPitchAtBwdSpeed;
    config::Setting<double>* d_fwdAccHipPitchDelta;
    config::Setting<double>* d_bwdAccHipPitchDelta;
    config::Setting<double>* d_turnHipPitch;
    config::Setting<double>* d_maxHipPitchAtTurnSpeed;
  };
}
