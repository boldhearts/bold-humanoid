#!/bin/bash

file="/home/darwin/network"

if [ ! -f "$file" ]; then
    echo wlan0-boldhearts-$HOSTNAME
    exit
fi

echo `cat $file`-$HOSTNAME
