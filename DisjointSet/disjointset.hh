// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <map>
#include <set>
#include <vector>
#include <algorithm>

namespace bold
{
  template <typename T>
  class DisjointSet
  {
  public:
    DisjointSet() {}

    void insert(T const& el)
    {
      std::size_t idx = d_equivList.size();
      d_elementIdxMap[el] = idx;
      d_equivList.push_back(idx);
    }

    std::size_t find(T const& el)
    {
      return find(d_elementIdxMap[el]);
    }

    void merge(T const& el1, T const& el2)
    {
      merge(d_elementIdxMap[el1], d_elementIdxMap[el2]);
    }

    std::set<std::set<T> > getSubSets()
    {
      flattenEquivList();
      std::map<std::size_t, std::set<T> > subSetsM;

      for (auto elIdxPair : d_elementIdxMap)
      {
        // Find subset of this element
        std::size_t ssId = find(elIdxPair.second);

        // If we haven't seen this one yet, add it
        if (subSetsM.find(ssId) == subSetsM.end())
          subSetsM[ssId] = std::set<T>();
        // Insert element
        subSetsM[ssId].insert(elIdxPair.first);
      }

      std::set<std::set<T> > subSets;

      std::transform(subSetsM.begin(), subSetsM.end(),
                     std::inserter(subSets, subSets.begin()),
                     [](std::pair<const size_t, std::set<T>> const& kv) {
                       return kv.second;
                     });

      return subSets;
    }

    std::size_t size() const;

  private:
    std::size_t find(std::size_t idx)
    {
      if (d_equivList[idx] != idx)
        d_equivList[idx] = find(d_equivList[idx]);
      return d_equivList[idx];
    }

    std::size_t merge(std::size_t idx1, std::size_t idx2)
    {
      std::size_t ss1 = find(idx1);
      std::size_t ss2 = find(idx2);

      if (ss1 < ss2)
      {
        d_equivList[ss2] = ss1;
        return ss2;
      }
      else
      {
        d_equivList[ss1] = ss2;
        return ss1;
      }
    }

    void flattenEquivList()
    {
      for (std::size_t idx = 0; idx < d_equivList.size(); ++idx)
        d_equivList[idx] = d_equivList[d_equivList[idx]];
    }

    std::map<T, std::size_t> d_elementIdxMap;
    std::vector<std::size_t> d_equivList;
  };
}
