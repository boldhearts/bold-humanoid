// Copyright 2021 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "jointid.hh"

using namespace bold;

bool bold::isHeadJoint(JointId id)
{
  return id == JointId::HEAD_PAN || id == JointId::HEAD_TILT;
}

bool bold::isArmJoint(JointId id)
{
  return id >= JointId::R_SHOULDER_PITCH && id <= JointId::L_ELBOW;
}

bool bold::isLegJoint(JointId id)
{
  return id >= JointId::R_HIP_YAW && id <= JointId::L_ANKLE_ROLL;
}
